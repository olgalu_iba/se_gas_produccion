﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParametrosSubastaUVLP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parametros Subasta UVLP";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string sRutaArc = ConfigurationManager.AppSettings["RutaIMG"].ToString();
    string gsTabla = "m_parametros_uvlp";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    //private void EstablecerPermisosSistema()
    //{
    //    Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
    //    imbCrear.Visible = (Boolean)permisos["INSERT"];
    //    imbActualiza.Visible = (Boolean)permisos["UPDATE"];
    //}
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtFechaIniDecInf.Text = lLector["fecha_ini_dec_inf"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_dec_inf"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_dec_inf"].ToString().Substring(0, 2);
                    TxtFechaFinDecInf.Text = lLector["fecha_fin_dec_inf"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_dec_inf"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_dec_inf"].ToString().Substring(0, 2);
                    TxtFechaPubCeIni.Text = lLector["fecha_pub_ce"].ToString().Substring(6, 4) + "/" + lLector["fecha_pub_ce"].ToString().Substring(3, 2) + "/" + lLector["fecha_pub_ce"].ToString().Substring(0, 2);
                    TxtFechaPubCeFin.Text = lLector["fecha_fin_pub_ce"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_pub_ce"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_pub_ce"].ToString().Substring(0, 2);
                    TxtFechaModIni.Text = lLector["fecha_ini_mod_dec"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_mod_dec"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_mod_dec"].ToString().Substring(0, 2);
                    TxtFechaModFin.Text = lLector["fecha_fin_mod_dec"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_mod_dec"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_mod_dec"].ToString().Substring(0, 2);
                    TxtFechaMaxSum.Text = lLector["fecha_max_reg_sum"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_reg_sum"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_reg_sum"].ToString().Substring(0, 2);
                    TxtHorMaxSum.Text = lLector["hora_max_reg_sum"].ToString();
                    TxtFechaMaxTra.Text = lLector["fecha_max_reg_trans"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_reg_trans"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_reg_trans"].ToString().Substring(0, 2);
                    TxtHorMaxTra.Text = lLector["hora_max_reg_trans"].ToString();
                    ddlContraos.SelectedValue = lLector["contratos_base"].ToString(); // Ajuste 20151027
                    TxtPorcMaxTot.Text = lLector["porc_max_tot_demand"].ToString();
                    TxtCntMonSub.Text = lLector["cantidad_min_sub"].ToString();
                    TxtPorcMaxExc.Text = lLector["porc_max_capa_exc"].ToString();
                    //20170929 rq048-17
                    if (lLector["fecha_ini_entrega"].ToString().Length >= 10)
                        TxtFechaEntIni.Text = lLector["fecha_ini_entrega"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_entrega"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_entrega"].ToString().Substring(0, 2);  
                    if (lLector["fecha_fin_entrega"].ToString().Length >= 10)
                        TxtFechaEntFin.Text = lLector["fecha_fin_entrega"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_entrega"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_entrega"].ToString().Substring(0, 2);
                    //20170929  fin rq048-17
                    //TxtFechaIniNegMp.Text = lLector["fecha_ini_neg_mp"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_neg_mp"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_neg_mp"].ToString().Substring(0, 2);
                    //TxtFechaFinNegMp.Text = lLector["fecha_fin_neg_mp"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_neg_mp"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_neg_mp"].ToString().Substring(0, 2);
                    //TxtFechaIniEntMp.Text = lLector["fecha_ini_entrega_mp"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_entrega_mp"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_entrega_mp"].ToString().Substring(0, 2);
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado.";
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_ini_dec_inf","@P_fecha_fin_dec_inf","@P_fecha_pub_ce","@P_fecha_fin_pub_ce","@P_fecha_ini_mod_dec","@P_fecha_fin_mod_dec","@P_fecha_max_reg_sum","@P_hora_max_reg_sum","@P_fecha_max_reg_trans","@P_hora_max_reg_trans",
                                          "@P_contratos_base", "@P_porc_max_tot_demand","@P_cantidad_min_sub", "@P_porc_max_capa_exc","@P_observaciones","@P_accion",
                                          "@P_fecha_ini_entrega","@P_fecha_fin_entrega"  //20170929 rq048-17
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                                          SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                                            SqlDbType.VarChar,SqlDbType.VarChar}; //20170929 rq048-17
        string[] lValorParametros = { "", "", "", "", "", "", "", "", "", "", ddlContraos.SelectedValue, "0", "0", "0", "", "2", "", "" }; //20170929 rq048-17
        lblMensaje.Text = "";
        string sRuta = sRutaArc;
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        int liDato;

        try
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniDecInf.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Inicial Declaración Información Posterior Subasta.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinDecInf.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Final Declaración Información Posterior Subasta.<br>";

            }
            if (lblMensaje.Text == "")
                if (ldFechaI > ldFechaF)
                    lblMensaje.Text += "La Fecha Final de Declaración  de Información debe ser mayor o igual que la fecha inicial.<br>";

            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaPubCeIni.Text.Trim());
                if (ldFechaF > ldFechaI)
                    lblMensaje.Text += "La Fecha de publicación inicial de la capacidad excedentaria debe ser mayor o igual que la fecha final de declaración.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Publicación Inicial de Capacidad Excedentaria.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaModIni.Text.Trim());
                if (ldFechaF <= ldFechaI)
                    lblMensaje.Text += "La Fecha Inicial de modificación de la declaración debe ser mayor que la fecha de publicación inicial de la capacidad excedentaria.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Inicial de modificación de declaración.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaModFin.Text.Trim());
                if (ldFechaF > ldFechaI)
                    lblMensaje.Text += "La Fecha Final de modificación de la declaración debe ser mayor o igual que la fecha de modificación inicial.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha final de modificación de declaración.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaPubCeFin.Text.Trim());
                if (ldFechaI > ldFechaF)
                    lblMensaje.Text += "La Fecha de publicación final de la capacidad excedentaria debe ser mayor o igual que la fecha final de modificación de declaración.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Publicación Final de Capacidad Excedentaria.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaMaxSum.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Máxima de Registro de Contratos de Suministro.<br>";
            }
            if(TxtHorMaxSum.Text =="")
                lblMensaje.Text += "Debe digitar la hora máxima de Registro de Contratos de Suministro.<br>";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaMaxTra.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Máxima de Registro de Contratos de Transporte.<br>";
            }
            if (TxtHorMaxTra.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de Registro de Contratos de Transporte.<br>";

            if (TxtPorcMaxTot.Text == "")
                lblMensaje.Text += "Debe digitar el porcentaje Máximo de energía demandada total.<br>";
            else
            {
                try
                {
                    liDato = Convert.ToInt32(TxtPorcMaxTot.Text);
                    if (liDato <0 )
                        lblMensaje.Text += "El porcentaje Máximo de energía demandada total debe ser mayor o igual que cero.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor inválido en el porcentaje Máximo de energía demandada total.<br>";
                }
            }
            if (TxtCntMonSub.Text == "")
                lblMensaje.Text += "Debe digitar la cantidad mínima de producto a subastar.<br>";
            if (TxtPorcMaxExc.Text == "")
                lblMensaje.Text += "Debe digitar el porcentaje máximo de capacidad excedentaria.<br>";
            else
            {
                try
                {
                    liDato = Convert.ToInt32(TxtPorcMaxExc.Text);
                    if (liDato <0 || liDato >100 )
                        lblMensaje.Text += "El porcentaje máximo de capacidad excedentaria debe estar entre 0 y 100.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor inválido en el porcentaje máximo de capacidad excedentaria.<br>";
                }
            }

            //20170929 rq048-17
            string lsError ="N";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaEntIni.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Inicial de entrega.<br>";
                lsError ="S";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaEntFin.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha final de entrega.<br>";
                lsError ="S";
            }
            if (lsError == "N")
                if (ldFechaI > ldFechaF)
                    lblMensaje.Text += "La Fecha Final de entrega debe ser mayor o igual que la fecha inicial.<br>";
            //20170929 fin rq048-17

            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaIniDecInf.Text.Trim();
                lValorParametros[1] = TxtFechaFinDecInf.Text.Trim();
                lValorParametros[2] = TxtFechaPubCeIni.Text.Trim();
                lValorParametros[3] = TxtFechaPubCeFin.Text.Trim();
                lValorParametros[4] = TxtFechaModIni.Text.Trim();
                lValorParametros[5] = TxtFechaModFin.Text.Trim();
                lValorParametros[6] = TxtFechaMaxSum.Text.Trim();
                lValorParametros[7] = TxtHorMaxSum.Text.Trim();
                lValorParametros[8] = TxtFechaMaxTra.Text.Trim();
                lValorParametros[9] = TxtHorMaxTra.Text.Trim();
                lValorParametros[11] = TxtPorcMaxTot.Text.Trim();
                lValorParametros[12] = TxtCntMonSub.Text.Trim();
                lValorParametros[13] = TxtPorcMaxExc.Text.Trim();
                lValorParametros[14] = TxtObservacion.Text.Trim();
                lValorParametros[16] = TxtFechaEntIni.Text.Trim();  //20170929 rq048-17
                lValorParametros[17] = TxtFechaEntFin.Text.Trim();  //20170929 rq048-17

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosUvlp", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de Parámetros UVLP.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_ini_dec_inf","@P_fecha_fin_dec_inf","@P_fecha_pub_ce","@P_fecha_fin_pub_ce","@P_fecha_ini_mod_dec","@P_fecha_fin_mod_dec","@P_fecha_max_reg_sum","@P_hora_max_reg_sum","@P_fecha_max_reg_trans","@P_hora_max_reg_trans",
                                          "@P_contratos_base", "@P_porc_max_tot_demand","@P_cantidad_min_sub", "@P_porc_max_capa_exc","@P_observaciones","@P_accion",
                                          "@P_fecha_ini_entrega", "@P_fecha_fin_entrega"  //20170929 rq048-17
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, 
                                          SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int,
                                      SqlDbType.VarChar,SqlDbType.VarChar}; //20170929 rq048-17
        string[] lValorParametros = { "", "", "", "", "", "", "", "", "", "", ddlContraos.SelectedValue, "0", "0", "0", "", "2", "", "" }; //20170929 rq048-17
        lblMensaje.Text = "";
        string sRuta = sRutaArc;
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        int liDato;
        try
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniDecInf.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Inicial Declaración Información Posterior Subasta.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinDecInf.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Final Declaración Información Posterior Subasta.<br>";

            }
            if (lblMensaje.Text == "")
                if (ldFechaI > ldFechaF)
                    lblMensaje.Text += "La Fecha Final de Declaración  de Información debe ser mayor o igual que la fecha inicial.<br>";

            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaPubCeIni.Text.Trim());
                if (ldFechaF > ldFechaI)
                    lblMensaje.Text += "La Fecha de publicación inicial de la capacidad excedentaria debe ser mayor o igual que la fecha final de declaración.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Publicación Inicial de Capacidad Excedentaria.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaModIni.Text.Trim());
                if (ldFechaF <= ldFechaI)
                    lblMensaje.Text += "La Fecha Inicial de modificación de la declaración debe ser mayor que la fecha de publicación inicial de la capacidad excedentaria.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Inicial de modificación de declaración.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaModFin.Text.Trim());
                if (ldFechaF > ldFechaI)
                    lblMensaje.Text += "La Fecha Final de modificación de la declaración debe ser mayor o igual que la fecha de modificación inicial.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha final de modificación de declaración.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaPubCeFin.Text.Trim());
                if (ldFechaI > ldFechaF)
                    lblMensaje.Text += "La Fecha de publicación final de la capacidad excedentaria debe ser mayor o igual que la fecha final de modificación de declaración.<br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Publicación Final de Capacidad Excedentaria.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaMaxSum.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Máxima de Registro de Contratos de Suministro.<br>";
            }
            if (TxtHorMaxSum.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de Registro de Contratos de Suministro.<br>";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaMaxTra.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Máxima de Registro de Contratos de Transporte.<br>";
            }
            if (TxtHorMaxTra.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de Registro de Contratos de Transporte.<br>";
            if (TxtPorcMaxTot.Text == "")
                lblMensaje.Text += "Debe digitar el porcentaje Máximo de energía demandada total.<br>";
            else
            {
                try
                {
                    liDato = Convert.ToInt32(TxtPorcMaxTot.Text);
                    if (liDato < 0 )
                        lblMensaje.Text += "El porcentaje Máximo de energía demandada total debe ser mayor o igual que cero.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor inválido en el porcentaje Máximo de energía demandada total.<br>";
                }
            }
            if (TxtCntMonSub.Text == "")
                lblMensaje.Text += "Debe digitar la cantidad mínima de producto a subastar.<br>";
            if (TxtPorcMaxExc.Text == "")
                lblMensaje.Text += "Debe digitar el porcentaje máximo de capacidad excedentaria.<br>";
            else
            {
                try
                {
                    liDato = Convert.ToInt32(TxtPorcMaxExc.Text);
                    if (liDato < 0 || liDato > 100)
                        lblMensaje.Text += "El porcentaje máximo de capacidad excedentaria debe estar entre 0 y 100.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor inválido en el porcentaje máximo de capacidad excedentaria.<br>";
                }
            }
            //20170929 rq048-17
            string lsError = "N";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaEntIni.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha Inicial de entrega.<br>";
                lsError = "S";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaEntFin.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha final de entrega.<br>";
                lsError = "S";
            }
            if (lsError == "N")
                if (ldFechaI > ldFechaF)
                    lblMensaje.Text += "La Fecha Final de entrega debe ser mayor o igual que la fecha inicial.<br>";
            //20170929 fin rq048-17

            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaIniDecInf.Text.Trim();
                lValorParametros[1] = TxtFechaFinDecInf.Text.Trim();
                lValorParametros[2] = TxtFechaPubCeIni.Text.Trim();
                lValorParametros[3] = TxtFechaPubCeFin.Text.Trim();
                lValorParametros[4] = TxtFechaModIni.Text.Trim();
                lValorParametros[5] = TxtFechaModFin.Text.Trim();
                lValorParametros[6] = TxtFechaMaxSum.Text.Trim();
                lValorParametros[7] = TxtHorMaxSum.Text.Trim();
                lValorParametros[8] = TxtFechaMaxTra.Text.Trim();
                lValorParametros[9] = TxtHorMaxTra.Text.Trim();
                lValorParametros[11] = TxtPorcMaxTot.Text.Trim();
                lValorParametros[12] = TxtCntMonSub.Text.Trim();
                lValorParametros[13] = TxtPorcMaxExc.Text.Trim();
                lValorParametros[14] = TxtObservacion.Text.Trim();
                lValorParametros[16] = TxtFechaEntIni.Text.Trim();  //20170929 rq048-17
                lValorParametros[17] = TxtFechaEntFin.Text.Trim();  //20170929 rq048-17

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosUvlp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de los Parámetros UVLP .!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistenciaSib", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_uvlp' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}