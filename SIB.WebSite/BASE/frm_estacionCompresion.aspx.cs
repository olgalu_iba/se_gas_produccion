﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_estacionCompresion : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Estaciones de Compresión";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTramo, "m_pozo", " estado = 'A' order by descripcion", 0, 10);
            LlenarControles1(lConexion.gObjConexion, ddlDepartamento, "m_divipola", " estado = 'A' and codigo_ciudad ='0' order by nombre_ciudad", 1, 2); //20180228 rq032-17
            lConexion.Cerrar();

            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_punto_salida_snt");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[11].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[12].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoPunto.Visible = false;
        LblCodigoPunto.Visible = true;
        LblCodigoPunto.Text = "Automático";
        ddlEstado.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_estacion_compresion", " codigo_estacion = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoPunto.Text = lLector["codigo_estacion"].ToString();
                        TxtCodigoPunto.Text = lLector["codigo_estacion"].ToString();
                        TxtDescripcion.Text = lLector["descripcion"].ToString();
                        try
                        {
                            ddlDepartamento.SelectedValue = lLector["codigo_departamento"].ToString();
                            ddlDepartamento_SelectedIndexChanged(null, null);
                            ddlCiudad.SelectedValue = lLector["codigo_ciudad"].ToString();
                            ddlCiudad_SelectedIndexChanged(null, null);
                            ddlCentro.SelectedValue = lLector["codigo_centro"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text = "El centro poblado no existe o está inactivo";
                        }
                        try
                        {
                            ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tramo no existe o está inactivo<br>";
                        }
                        ddlTelemetria.SelectedValue = lLector["maneja_telemetria"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();

                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoPunto.Visible = false;
                        LblCodigoPunto.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro porque está bloqueado. Codigo estación " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
            //ddlEstado.Enabled = false;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_estacion", "@P_descripcion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "" };

        try
        {
            if (TxtBusCodPunto.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodPunto.Text.Trim();
            if (TxtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetEstacionComp", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_estacion", "@P_descripcion", "@P_codigo_departamento", "@P_codigo_ciudad", "@P_codigo_centro", "@P_codigo_tramo", "@P_maneja_telemetria", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "", "", "", "0", "", "", "1" };
        lblMensaje.Text = "";

        try
        {
            if (TxtDescripcion.Text.Trim() == "")
                lblMensaje.Text += " Debe ingresar la descripción de la estación de compresión <br>";
            if (VerificarExistencia(" descripcion = '" + TxtDescripcion.Text.Trim() + "'"))
                lblMensaje.Text += " La Descripción de la estáción de compresión YA existe <br>";
            if (ddlDepartamento.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el departamento. <br>";
            if (ddlCiudad.SelectedValue == "0" || ddlCiudad.SelectedValue == "")
                lblMensaje.Text += " Debe Seleccionar la ciudad. <br>";
            if (ddlCentro.SelectedValue == "0" || ddlCentro.SelectedValue == "")
                lblMensaje.Text += " Debe Seleccionar el centro poblado. <br>";
            if (ddlTramo.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el tramo. <br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlDepartamento.SelectedValue;
                lValorParametros[3] = ddlCiudad.SelectedValue;
                lValorParametros[4] = ddlCentro.SelectedValue;
                lValorParametros[5] = ddlTramo.SelectedValue;
                lValorParametros[6] = ddlTelemetria.SelectedValue;
                lValorParametros[7] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEstacionComp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la estación de compresión.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_estacion", "@P_descripcion", "@P_codigo_departamento", "@P_codigo_ciudad", "@P_codigo_centro", "@P_codigo_tramo", "@P_maneja_telemetria", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "", "", "", "0", "", "", "2" };
        lblMensaje.Text = "";
        lblMensaje.Text = "";
        try
        {
            if (TxtDescripcion.Text.Trim() == "")
                lblMensaje.Text += " Debe ingresar la descripción de la estación de compresión <br>";
            if (VerificarExistencia(" descripcion = '" + TxtDescripcion.Text.Trim() + "'And codigo_estacion != " + LblCodigoPunto.Text))
                lblMensaje.Text += " La Descripción de la estación YA existe <br>";
            if (ddlDepartamento.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el departamento. <br>";
            if (ddlCiudad.SelectedValue == "0" || ddlCiudad.SelectedValue == "")
                lblMensaje.Text += " Debe Seleccionar la ciudad. <br>";
            if (ddlCentro.SelectedValue == "0" || ddlCentro.SelectedValue == "")
                lblMensaje.Text += " Debe Seleccionar el centro poblado. <br>";
            if (ddlTramo.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el tramo. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoPunto.Text;
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlDepartamento.SelectedValue;
                lValorParametros[3] = ddlCiudad.SelectedValue;
                lValorParametros[4] = ddlCentro.SelectedValue;
                lValorParametros[5] = ddlTramo.SelectedValue;
                lValorParametros[6] = ddlTelemetria.SelectedValue;
                lValorParametros[7] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEstacionComp", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la estación de compresión.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoPunto.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoPunto.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoPunto.Text != "")
            manejo_bloqueo("E", LblCodigoPunto.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_estacion_compresion", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["desc_tramo"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_estacion_compresion' and llave_registro='codigo_estacion=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_estacion=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_estacion_compresion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_estacion_compresion", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodPunto.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodPunto.Text.Trim();
                lsParametros += " Código Estación: " + TxtBusCodPunto.Text;

            }
            if (TxtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
                lsParametros += " - Descripción: " + TxtBusDescripcion.Text;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetEstacionComp&nombreParametros=@P_codigo_estacion*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_estacion*descripcion&titulo_informe=Listado estaciones de compresión&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_estacion <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_estacion_compresion&procedimiento=pa_ValidarExistencia&columnas=codigo_estacion*descripcion*codigo_tramo*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlCiudad.Items.Clear();
        LlenarControles1(lConexion.gObjConexion, ddlCiudad, "m_divipola", " estado = 'A' and codigo_departamento =" + ddlDepartamento.SelectedValue + " and codigo_ciudad <>'0' and codigo_centro='0' order by nombre_ciudad", 3, 4);
        lConexion.Cerrar();

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlCiudad_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlCentro.Items.Clear();
        LlenarControles1(lConexion.gObjConexion, ddlCentro, "m_divipola", " estado = 'A' and codigo_departamento ='" + ddlDepartamento.SelectedValue + "' and codigo_ciudad='" + ddlCiudad.SelectedValue + "'  and codigo_centro<>'0' order by nombre_centro", 5, 6);
        lConexion.Cerrar();
    }
}