﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
public partial class BASE_frm_TiposRueda : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Tipos de Rueda";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Controlador util = new Controlador();
            /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
             *   lsIndica = N -> Nuevo (Creacion)
             *   lsIndica = L -> Listar Registros (Grilla)
             *   lsIndica = M -> Modidificar
             *   lsIndica = B -> Buscar
             * */

            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            lConexion = new clConexion(goInfo);

            if (!IsPostBack)
            {
                /// Llenar controles
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlBusTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
                lConexion.Cerrar();

                // Carga informacion de combos
                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
                {
                    lsIndica = this.Request.QueryString["lsIndica"].ToString();
                }
                if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                {
                    Listar();
                }
                else if (lsIndica == "N")
                {
                    Nuevo();
                }
                else if (lsIndica == "B")
                {
                    Buscar();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas en la carga de la página. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_tipos_rueda");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[9].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[10].Visible = (Boolean)permisos["DELETE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoTipoRuedas.Visible = false;
        LblCodigoTipoRueda.Visible = true;
        LblCodigoTipoRueda.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_tipos_rueda", " codigo_tipo_rueda = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        TrModCont.Visible = false;
                        TrUvcp01.Visible = false;
                        TrUvcp02.Visible = false;
                        TrUvcp03.Visible = false;
                        TrSmpsi01.Visible = false;
                        TrSmpsi02.Visible = false;
                        TrSmpsi03.Visible = false;
                        TrSmpsi04.Visible = false;
                        //TrUvlp01.Visible = false;
                        TrSci01.Visible = false;
                        TrSci02.Visible = false;
                        TrSci03.Visible = false;
                        TrSci04.Visible = false;
                        TrSci05.Visible = false;
                        TrSci06.Visible = false;
                        TrSnd01.Visible = false;
                        TrSci07.Visible = false;
                        TrSci08.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                        TrSci09.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                        TrMes.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                        TrMes1.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                        TrUVLP01.Visible = false;  //20160921 UVLP
                        TrUVLP02.Visible = false;  //20160921 UVLP
                        TrUVLP03.Visible = false;  //20160921 UVLP
                        TrUVLP04.Visible = false;  //20160921 UVLP
                        txtHoraIniCont.Text = "";
                        txtHoraFinCont.Text = "";
                        /// Campos Nuevos Req. 005-2021 20200120 Subasta Transporte
                        TrStra01.Visible = false;
                        TrStra02.Visible = false;
                        TrStra03.Visible = false;
                        TrStra04.Visible = false;
                        /// Hasta Aqui
                        lLector.Read();
                        LblCodigoTipoRueda.Text = lLector["codigo_tipo_rueda"].ToString();
                        TxtCodigoTipoRuedas.Text = lLector["codigo_tipo_rueda"].ToString();
                        TxtDescripcion.Text = lLector["descripcion"].ToString();
                        /////
                        try
                        {
                            ddlTipoSubasta.SelectedValue = lLector["codigo_tipo_subasta"].ToString();
                            ddlTipoSubasta_SelectedIndexChanged(null, null);
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El tipo de subasta del registro no existe o esta inactivo<br>";
                        }
                        ddlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                        ddlDestino_SelectedIndexChanged(null, null); //20201207
                        TxtHoraApertura.Text = lLector["hora_apertura"].ToString();
                        try
                        {
                            ddlTipoMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El tipo de mercado del registro no existe o esta inactivo<br>";
                        }
                        ddlModifContratos.SelectedValue = lLector["permite_modif_manual_cont"].ToString();
                        try
                        {
                            ddlTipoSubasta.SelectedValue = lLector["codigo_tipo_subasta"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El tipo de subasta del registro no existe o esta inactivo<br>";
                        }
                        txtHoraIniCont.Text = lLector["hora_ini_modif_ctr"].ToString();
                        txtHoraFinCont.Text = lLector["hora_fin_modif_ctr"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        /// Campos Subasta Uselo o Vendalo Cotro Plazo
                        if (ddlTipoSubasta.SelectedValue == "2")
                        {
                            TrUvcp01.Visible = true;
                            TrUvcp02.Visible = true;
                            TrUvcp03.Visible = true;
                            //TrHora.Visible = false;
                            TrModCont.Visible = true;
                            txtHoraIniDecInf.Text = lLector["hora_ini_publi_v_sci"].ToString();
                            txtHoraFinDecInf.Text = lLector["hora_fin_publi_v_sci"].ToString();
                            txtHoraIniPre.Text = lLector["hora_ini_ctrl_v"].ToString();
                            txtHoraFinPre.Text = lLector["hora_fin_ctrl_v"].ToString();
                            txtHoraIniPub.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString();
                            txtHoraFinPub.Text = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                            txtHoraIniComp.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString();
                            txtHoraFinComp.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                            txtHoraIniCalce.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                            txtHoraFinCalce.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                            TxtNoPostCompra.Text = lLector["no_posturas_compra"].ToString();
                            //20201207
                            try
                            {
                                ddlTipoRuedaTra.SelectedValue = lLector["ind_tipo_rueda"].ToString();
                                ddlTipoRuedaTra_SelectedIndexChanged(null, null);
                            }
                            catch (Exception)
                            {
                                ddlTipoRuedaTra.SelectedValue = "R";
                            }
                        }
                        /// Campos Subasta Suministro con Interrpciones
                        if (ddlTipoSubasta.SelectedValue == "1")
                        {
                            TrSmpsi01.Visible = true;
                            TrSmpsi02.Visible = true;
                            TrSmpsi03.Visible = true;
                            TrSmpsi04.Visible = true;
                            TxtMesPrevVendSmpsi.Text = lLector["mes_prev_vend_smpsi"].ToString();
                            TxtDuraRondaSmpsi.Text = lLector["duracion_ronda_smpsi"].ToString();
                            TxtDiasDecOfVSmpsi.Text = lLector["dias_declara_ofer_v_smpsi"].ToString();
                            TxtDiasDecOfCSmpsi.Text = lLector["dias_declara_ofer_c_smpsi"].ToString();
                            TxtNHorModOfVSmpsi.Text = lLector["hora_modif_ofer_v_smpsi"].ToString();
                            TxtNHorModOfCSmpsi.Text = lLector["hora_modif_ofer_c_smpsi"].ToString();
                            TxtNoSemSmpsi.Text = lLector["no_semana_rev_inf_smpsi"].ToString();
                        }
                        /// 20160921 UVL
                        if (ddlTipoSubasta.SelectedValue == "3")
                        {
                            TrUVLP01.Visible = true;
                            TrUVLP02.Visible = true;
                            TrUVLP03.Visible = true;
                            TrUVLP04.Visible = true;
                            TxtHoraIniPubUvlp.Text = lLector["hora_ini_publi_v_sci"].ToString();
                            TxtHoraFinPubUvlp.Text = lLector["hora_fin_publi_v_sci"].ToString();
                            TxtHoraIniCompUvlp.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString();
                            TxtHoraFinCompUvlp.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                            TxtHoraIniCalceUvlp.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                            TxtHoraFinCalceUvlp.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                            ddlTpoNEgUvlp.SelectedValue = lLector["ind_tipo_rueda"].ToString();
                        }
                        /// Campos Subasta Suministro con Interrpciones
                        if (ddlTipoSubasta.SelectedValue == "4" || ddlTipoSubasta.SelectedValue == "6")
                        {
                            TrSci01.Visible = true;
                            TrSci02.Visible = true;
                            TrSci03.Visible = true;
                            TrSci04.Visible = true;
                            TrSci05.Visible = true;
                            TrSci06.Visible = true;
                            TxtDiaAntCi.Text = lLector["dias_habil_ant_sub_sci"].ToString();
                            TxtNoPosCCi.Text = lLector["no_posturas_compra"].ToString();
                            TxtHorIniPubVCi.Text = lLector["hora_ini_publi_v_sci"].ToString();
                            TxtHorFinPubVCi.Text = lLector["hora_fin_publi_v_sci"].ToString();
                            TxtHorIniPubCantDCi.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString();
                            TxtHorFinPubCantDCi.Text = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                            TxtHorIniRecCantCCi.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString();
                            TxtHorFinRecCantCCi.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                            TxtHorIniNegociaCi.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                            TxtHorFinNegociaCi.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                            TxtMinModV.Text = lLector["min_modif_ofert_sci_v"].ToString();
                            TxtMinModC.Text = lLector["min_modif_ofert_sci_c"].ToString();
                            // Campo nuevo ajuste subastas bimetrales por contingencia 20151105
                            TxtNoMesIniPer.Text = "0";
                            if (ddlTipoSubasta.SelectedValue == "6")
                            {
                                TrSci07.Visible = true;
                                TxtNoMesIniPer.Text = lLector["mes_prev_vend_smpsi"].ToString();
                                /////////////////////////////////////////////////////////////
                                //// Campos Nuevos Req.007-17 Subasta Bimestral 20170209 ////
                                /////////////////////////////////////////////////////////////
                                TrSci08.Visible = true; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                                TrSci09.Visible = true; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                                TrMes.Visible = true; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                                TrMes1.Visible = true; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                                TxtDiaHabDeclaraInf.Text = lLector["dias_declara_ofer_v_smpsi"].ToString();
                                TxtDiaHabPublicaInf.Text = lLector["dias_declara_ofer_c_smpsi"].ToString();
                                TxtHoraIniModDecSb.Text = lLector["hora_ini_ctrl_v"].ToString();
                                TxtHoraFinModDecSb.Text = lLector["hora_fin_ctrl_v"].ToString();
                                if (lLector["lunes"].ToString() == "S")
                                    ChkEnero.Checked = true;
                                else
                                    ChkEnero.Checked = false;
                                if (lLector["martes"].ToString() == "S")
                                    ChkFebrero.Checked = true;
                                else
                                    ChkFebrero.Checked = false;
                                if (lLector["miercoles"].ToString() == "S")
                                    ChkMarzo.Checked = true;
                                else
                                    ChkMarzo.Checked = false;
                                if (lLector["jueves"].ToString() == "S")
                                    ChkAbril.Checked = true;
                                else
                                    ChkAbril.Checked = false;
                                if (lLector["viernes"].ToString() == "S")
                                    ChkMayo.Checked = true;
                                else
                                    ChkMayo.Checked = false;
                                if (lLector["Sabado"].ToString() == "S")
                                    ChkJunio.Checked = true;
                                else
                                    ChkJunio.Checked = false;
                                if (lLector["Domingo"].ToString() == "S")
                                    ChkJulio.Checked = true;
                                else
                                    ChkJulio.Checked = false;
                                if (lLector["mes08"].ToString() == "S")
                                    ChkAgosto.Checked = true;
                                else
                                    ChkAgosto.Checked = false;
                                if (lLector["mes09"].ToString() == "S")
                                    ChkSeptiembre.Checked = true;
                                else
                                    ChkSeptiembre.Checked = false;
                                if (lLector["mes10"].ToString() == "S")
                                    ChkOctubre.Checked = true;
                                else
                                    ChkOctubre.Checked = false;
                                if (lLector["mes11"].ToString() == "S")
                                    ChkNoviembre.Checked = true;
                                else
                                    ChkNoviembre.Checked = false;
                                if (lLector["mes12"].ToString() == "S")
                                    ChkDicimebre.Checked = true;
                                else
                                    ChkDicimebre.Checked = false;
                                /////////////////////////////////////////////////////////////
                            }
                        }
                        if (ddlTipoSubasta.SelectedValue == "5")
                        {
                            TrSnd01.Visible = true;
                            TxtHorInPosNd.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                            TxtHorFiPosNd.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                        }
                        /// Campos nuevos Req. 005-2021 20210120 Subasta Transporte
                        if (ddlTipoSubasta.SelectedValue == "11")
                        {
                            TrStra01.Visible = true;
                            TrStra02.Visible = true;
                            TrStra03.Visible = true;
                            TrStra04.Visible = true;
                            ddlTipoRueTra11.SelectedValue = lLector["ind_tipo_rueda"].ToString();
                            txtNoDiaInTriNeg11.Text = lLector["dias_habil_ant_sub_sci"].ToString();
                            txtHoraIniPubOfr11.Text = lLector["hora_ini_publi_v_sci"].ToString();
                            txtHoraFinPubOfr11.Text = lLector["hora_fin_publi_v_sci"].ToString();
                            txtHoraIniDecPosCom11.Text = lLector["hora_ini_ctrl_v"].ToString();
                            txtHoraFinDecPosCom11.Text = lLector["hora_fin_ctrl_v"].ToString();
                            txtHoraIniDesSub11.Text = lLector["hora_ini_negociacioni_sci"].ToString();
                            txtHoraFinDesSub11.Text = lLector["hora_fin_negociacioni_sci"].ToString();
                            txtHoraIniRecAdj11.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString();
                            txtHoraFinRecAdj11.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString();
                            txtHoraIniPubRes11.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString();
                            txtHoraFinPubRes11.Text = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString();
                            txtHoraIniFinSub11.Text = lLector["hora_ini_modif_ctr"].ToString();
                            txtHoraFinFinSub11.Text = lLector["hora_fin_modif_ctr"].ToString();
                            txtHoraIniCont.Text = "";
                            txtHoraFinCont.Text = "";
                        }
                        /// Hasta Aqui
                        ddlRuedaAut.SelectedValue = lLector["creacion_automatica"].ToString();
                        ddlRuedaAut_SelectedIndexChanged(null, null);
                        if (ddlRuedaAut.SelectedValue == "S")
                        {
                            if (lLector["lunes"].ToString() == "S")
                                chkLun.Checked = true;
                            else
                                chkLun.Checked = false;
                            if (lLector["martes"].ToString() == "S")
                                chkMar.Checked = true;
                            else
                                chkMar.Checked = false;
                            if (lLector["miercoles"].ToString() == "S")
                                chkMie.Checked = true;
                            else
                                chkMie.Checked = false;
                            if (lLector["jueves"].ToString() == "S")
                                chkJue.Checked = true;
                            else
                                chkJue.Checked = false;
                            if (lLector["viernes"].ToString() == "S")
                                chkVie.Checked = true;
                            else
                                chkVie.Checked = false;
                            if (lLector["Sabado"].ToString() == "S")
                                chkSab.Checked = true;
                            else
                                chkSab.Checked = false;
                            if (lLector["Domingo"].ToString() == "S")
                                chkDom.Checked = true;
                            else
                                chkDom.Checked = false;
                        }
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoTipoRuedas.Visible = false;
                        LblCodigoTipoRueda.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Código Tipo Rueda " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_tipo_rueda", "@P_descripcion", "@P_destino", "@P_codigo_tipo_subasta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "S", "0" };

        try
        {
            if (TxtBusTipoRueda.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusTipoRueda.Text.Trim();
            if (TxtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
            lValorParametros[2] = ddlBusDestino.SelectedValue;
            if (ddlBusTipoSubasta.SelectedValue != "0")
                lValorParametros[3] = ddlBusTipoSubasta.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTipoRueda", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_tipo_rueda", "@P_descripcion",  "@P_destino_rueda", "@P_hora_apertura",
                                        "@P_minutos_oferta_venta","@P_minutos_control_venta","@P_minutos_publica_venta","@P_minutos_oferta_compra","@P_minutos_calce",
                                        "@P_minutos_modif_cont","@P_no_posturas_compra","@P_tipo_mercado","@P_permite_modif_manual_cont", "@P_estado", "@P_accion",
                                        "@P_codigo_tipo_subasta","@P_mes_prev_vend_smpsi","@P_duracion_ronda_smpsi","@P_dias_declara_ofer_v_smpsi", "@P_dias_declara_ofer_c_smpsi",
                                        "@P_hora_modif_ofer_v_smpsi","@P_hora_modif_ofer_c_smpsi","@P_duracion_ronda_suvlp","@P_dias_habil_ant_sub_sci","@P_hora_ini_publi_v_sci",
                                        "@P_hora_fin_publi_v_sci","@P_hora_ini_publ_cnt_dispi_v_sci","@P_hora_fin_publ_cnt_dispi_v_sci","@P_hora_ini_rec_solicitud_c_sci",
                                        "@P_hora_fin_rec_solicitud_c_sci","@P_hora_ini_negociacioni_sci","@P_hora_fin_negociacioni_sci","@P_no_semana_rev_inf_Smpsi",
                                        "@P_hora_ini_ctrl_v","@P_hora_fin_ctrl_v", "@P_hora_ini_modif_ctr", "@P_hora_fin_modif_ctr",
                                        "@P_min_modif_ofert_sci_v", "@P_min_modif_ofert_sci_c" ,//datos adicionales de SCI
                                        "@P_creacion_automatica", "@P_lunes", "@P_martes", "@P_miercoles", "@P_jueves", "@P_viernes", "@P_sabado", "@P_domingo", //Ruedas automaticas
                                        "@P_ind_tipo_rueda",  // 20160921 UVLP
                                        "@P_mes08","@P_mes09","@P_mes10","@P_mes11","@P_mes12" // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char, //20160921 UVLP
                                        SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char,SqlDbType.Char // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                        };
        string[] lValorParametros = { "0", "", "", "", "0", "0", "0", "0", "0", "0", "0", "", "", "", "1",
                                      "0","0","0","0","0","0","0","0","0","","","","","","","","","0","","","","","0","0","N","N","N","N","N","N","N","N",
                                      ddlTpoNEgUvlp.SelectedValue ,//20160921 UVLP
                                      "N","N","N","N","N" // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                    };
        lblMensaje.Text = "";

        try
        {
            if (TxtDescripcion.Text.Trim().Length < 3)
                lblMensaje.Text += " Debe ingresar la descripción del tipo de rueda <br>";
            else
                if (VerificarExistencia(" descripcion = '" + TxtDescripcion.Text.Trim() + "'"))
                lblMensaje.Text += " La Descripción del Tipo de Rueda  YA existe " + TxtDescripcion.Text + " <br>";
            if (ddlTipoSubasta.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo de Subasta. <br>";
            if (ddlModifContratos.SelectedValue == "S" && (txtHoraIniCont.Text == "" || txtHoraFinCont.Text == ""))
                lblMensaje.Text += "Seleccionó Si en Permite Modificación Manual de Contratos, pero No definió las horas para hacerlo. <br>";
            if (ddlModifContratos.SelectedValue == "N" && (txtHoraIniCont.Text != "" || txtHoraFinCont.Text != ""))
                lblMensaje.Text += "Seleccionó No en Permite Modificación Manual de Contratos, pero ingreso las horas para hacerlo. <br>";
            if (ddlRuedaAut.SelectedValue == "S" && !chkLun.Checked && !chkMar.Checked && !chkMie.Checked && !chkJue.Checked && !chkVie.Checked && !chkSab.Checked && !chkDom.Checked)
                lblMensaje.Text += "Debe seleccionar al menos un día de creación de ruedas automáticas. <br>";
            //// Validaciones Subasta Uselo o Vendalo Corto Plazo
            if (ddlTipoSubasta.SelectedValue == "2")
            {
                string sVlidaHora = "S";
                if (txtHoraIniDecInf.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de declaración de información. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDecInf.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de declaración de información. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDecInf.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDecInf.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de declaración de información debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de declaración de información no válido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniPre.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de declaración de precio de reserva y cantidad no disponible. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPre.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de declaración de precio y cantidad no disponible. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPre.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPre.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de declaración precio de reserva debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinDecInf.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPre.Text.Trim()))
                            lblMensaje.Text += "La hora final de declaración precio de reserva debe ser mayor que la hora final de declaración de información. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de declaración de precio de reserva no válido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniPub.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPub.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPub.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPub.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de publicación debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinPre.Text.Trim()) > Convert.ToDateTime(txtHoraIniPub.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de publicación debe ser mayor o igual que la hora final de declaración de precio de reserva. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de publicación no válido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniComp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinComp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniComp.Text.Trim()) >= Convert.ToDateTime(txtHoraFinComp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinPub.Text.Trim()) > Convert.ToDateTime(txtHoraIniComp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser mayor o igual que la hora final de publicación. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de ingreso de posturas de compra no válido<br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniCalce.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial calce. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinCalce.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de calce. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniCalce.Text.Trim()) >= Convert.ToDateTime(txtHoraFinCalce.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinComp.Text.Trim()) > Convert.ToDateTime(txtHoraIniCalce.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser mayor o igual que la hora final de ingreso de posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de calce no válido<br>";
                    }
                }
                if (txtHoraIniCont.Text.Trim() != "" && txtHoraFinCont.Text.Trim() != "")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniCont.Text.Trim()) >= Convert.ToDateTime(txtHoraFinCont.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de modificación de contratos debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinCalce.Text.Trim()) > Convert.ToDateTime(txtHoraIniCont.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de modificación de contratos debe ser mayor o igual que la hora final de calce . <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de calce no válido<br>";
                    }
                }

                if (TxtNoPostCompra.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar el No. de Posturas de Compra Uselo Corto Plazo. <br>";
                else
                {
                    if (TxtNoPostCompra.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Posturas de Compra Uselo Corto Plazo no Pueden ser iguales a 0. <br>";
                }

            }
            //// Validaciones Subasta de Suministro Sin Interrupciones
            if (ddlTipoSubasta.SelectedValue == "1")
            {
                if (TxtMesPrevVendSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. del Mes Previo Ingreso de Vendedores Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtMesPrevVendSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. del Mes Previo Ingreso de Vendedores Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtDuraRondaSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el Tiempo de Duración de las Rondas de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtDuraRondaSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El Tiempo de Duración de las Rondas de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtDiasDecOfVSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No.  Dias Declara Oferta Vendedor de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtDiasDecOfVSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No.  Días Declara Oferta Vendedor de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtDiasDecOfCSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Días Declara Oferta Comprador de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtDiasDecOfCSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Días Declara Oferta Comprador de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNHorModOfVSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Horas Modifica Oferta Vendedor de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtNHorModOfVSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Horas Modifica Oferta Vendedor de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNHorModOfCSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Horas Modifica Oferta Comprador de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtNHorModOfCSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Horas Modifica Oferta Comprador de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNoSemSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Semanas Revelación Información de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtNoSemSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Semandas Revelación Información de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
            }
            // 20160921 UVLP
            if (ddlTipoSubasta.SelectedValue == "3")
            {
                string sVlidaHora = "S";
                if (TxtHoraIniPubUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinPubUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniPubUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinPubUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de publicación debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de publicación<br>";
                    }
                }
                sVlidaHora = "S";
                if (TxtHoraIniCompUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinCompUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniCompUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinCompUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(TxtHoraIniCompUvlp.Text.Trim()) < Convert.ToDateTime(TxtHoraFinPubUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser mayor o igual que la hora final de publicación. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato inválido de hora de ingreso de posturas de compra<br>";
                    }
                }
                sVlidaHora = "S";
                if (TxtHoraIniCalceUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de calce. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinCalceUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de calce. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniCalceUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinCalceUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(TxtHoraIniCalceUvlp.Text.Trim()) < Convert.ToDateTime(TxtHoraFinCompUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser mayor o igual que la hora final de ingreso de posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato inválido de hora de calce<br>";
                    }
                }
            }
            //// Validaciones Subasta de Contrtos con Interrupciones
            if (ddlTipoSubasta.SelectedValue == "4" || ddlTipoSubasta.SelectedValue == "6")
            {
                if (TxtDiaAntCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Dias Hábiles Anteriores para Subasta Contrato con Interrupciones. <br>";
                else
                {
                    if (TxtDiaAntCi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Días Hábiles Anteriores para Subasta Contrato con Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNoPosCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar el No. de Posturas de Compra Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtNoPosCCi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Posturas de Compra Subasta Contrato con interrupciones no Puden ser iguales a 0. <br>";
                }
                if (TxtHorIniPubVCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Publicación Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniPubVCi.Text.Trim() == "0")
                        lblMensaje.Text += "La Hora Inicial Publicación Venta de Subasta Contrato con interrupciones no Puede se igual a 0. <br>";
                }
                if (TxtHorFinPubVCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Publicación Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinPubVCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Publicación Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniPubCantDCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniPubCantDCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinPubCantDCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinPubCantDCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniRecCantCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Recibo Cantidad Compra de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniRecCantCCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Recibo Cantidad Compra de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinRecCantCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Recibo Cantidad Compra de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinRecCantCCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Recibo Cantidad Compra de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniNegociaCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Negociación de Subasta Contrato con interupciones. <br>";
                else
                {
                    if (TxtHorIniNegociaCi.Text.Trim() == "0")
                        lblMensaje.Text += "La Hora Inicial Negociación de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinNegociaCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Negociación de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinNegociaCi.Text.Trim() == "0")
                        lblMensaje.Text += "La Hora Final Negociación de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtMinModV.Text == "")
                    lblMensaje.Text += "Debe ingresar los minutos para modificar ofertas de venta<br>";
                if (TxtMinModC.Text == "")
                    lblMensaje.Text += "Debe ingresar los minutos para modificar ofertas de compra<br>";
                try
                {
                    if (Convert.ToDateTime(TxtHorIniPubVCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de publicación de venta debe ser menor que la hora final. <br>";
                    /// Se Comentarea validacion Req. 007-17 Subasta Bimestral 20170209 ///
                    //if (Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniPubCantDCi.Text.Trim()))
                    //    lblMensaje.Text += "La hora inicial de publicación de cantidad disponible debe ser mayor o igual que la hora final de publicación de venta. <br>";
                    if (Convert.ToDateTime(TxtHorIniPubCantDCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinPubCantDCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de publicación de cantidad disponible debe ser menor que la hora final<br>";
                    if (Convert.ToDateTime(TxtHorFinPubCantDCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniRecCantCCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de recibo de cantidad compra debe ser mayor o igual que la hora final de publicación de cantidad disponible<br>";
                    if (Convert.ToDateTime(TxtHorIniRecCantCCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinRecCantCCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de recibo de cantidad compra debe ser menor que la hora final <br>";
                    if (Convert.ToDateTime(TxtHorFinRecCantCCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniNegociaCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial negociación debe ser mayor o igual que la hora final  de recibo de cantidad de compra<br>";
                    if (Convert.ToDateTime(TxtHorIniNegociaCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinNegociaCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial negociación debe ser menor que la hora final <br>";
                    ///////////////////////////////////////////////////////////////////////
                    /// Se Comentarea validacion Req. 007-17 Subasta Bimestral 20170209 ///
                    ///////////////////////////////////////////////////////////////////////
                    //DateTime dateValue = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorFinPubVCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorFinPubVCi.Text.Trim().Substring(3, 2)), 0);
                    //dateValue = dateValue.AddMinutes(Convert.ToInt16(TxtMinModV.Text));
                    //DateTime dateValue1 = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorIniPubCantDCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorIniPubCantDCi.Text.Trim().Substring(3, 2)), 0);
                    //if (dateValue > dateValue1)
                    //    lblMensaje.Text += "El tiempo de modificación de venta supera la hora de inicio de publicación <br>";
                    ///////////////////////////////////////////////////////////////////////
                    DateTime dateValue = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(3, 2)), 0); //Req. 007-17 Subasta Bimestral 20170209 /
                    dateValue = dateValue.AddMinutes(Convert.ToInt16(TxtMinModC.Text));
                    DateTime dateValue1 = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(3, 2)), 0); //Req. 007-17 Subasta Bimestral 20170209 /
                    if (dateValue > dateValue1)
                        lblMensaje.Text += "El tiempo de modificación de compra supera la hora de inicio de negociación <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Error en el formato de horas. <br>";
                }
                // Campo nuevo subasta bimestral por contingencia 20151105
                if (ddlTipoSubasta.SelectedValue == "6")
                {
                    string sVlidaHora = "S"; //Req. 007-07 Subasta Bimestral 20170209
                    if (TxtNoMesIniPer.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingesar El No. Mes Adelante Def Inicio Periodo subasta Bimestral. <br>";
                    else
                    {
                        if (TxtNoMesIniPer.Text.Trim() == "0")
                            lblMensaje.Text += "El No. Mes Adelante Def Inicio Periodo subasta Bimestral no puede ser igual a 0. <br>";
                    }
                    // Validacion que para el mismo tipo de subasta no exista mas de un tipo de rueda con el mismo día habil de subasta
                    if (DelegadaBase.Servicios.ValidarExistencia("m_tipos_rueda", " codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue + " And estado = 'A' And dias_habil_ant_sub_sci = " + TxtDiaAntCi.Text.Trim(), goInfo))
                        lblMensaje.Text += "No pueden existir dos tipos de Rueda bimestrales que tengan el mismo Día habil del mes para apertura de rueda. <br>";
                    /////////////////////////////////////////////////////////////
                    /// Validaciones Req. 007-07 Subasta Bimestral 20170209 /////
                    /////////////////////////////////////////////////////////////
                    if (TxtDiaHabDeclaraInf.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Día hábil del Mes para Declaración de Información de la subasta Bimestral no puede ser igual a 0. <br>";
                    if (TxtDiaHabPublicaInf.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Día hábil del Mes para Publicación de Información de la subasta Bimestral no puede ser igual a 0. <br>";
                    if (TxtHoraIniModDecSb.Text.Trim().Length <= 0)
                    {
                        lblMensaje.Text += "Debe ingresar la Hora Inicial Modificación Declaración Subasta Bimestral. <br>";
                        sVlidaHora = "N";
                    }
                    if (TxtHoraFinModDecSb.Text.Trim().Length <= 0)
                    {
                        lblMensaje.Text += "Debe ingresar la Hora Final Modificación Declaración Subasta Bimestral. <br>";
                        sVlidaHora = "N";
                    }
                    if (sVlidaHora == "S")
                    {
                        try
                        {
                            if (Convert.ToDateTime(TxtHoraIniModDecSb.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinModDecSb.Text.Trim()))
                                lblMensaje.Text += "La hora inicial de Modificación Declaración Subasta Bimestral debe ser menor que la hora final. <br>";

                            if (Convert.ToDateTime(TxtHoraIniModDecSb.Text.Trim()) < Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()))
                                lblMensaje.Text += "La hora inicial de Modificación Declaración Subasta Bimestral debe ser mayor o igual que la hora Final de Publicación Venta Contratos en Firme Bimestrales. <br>";
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "Formato de hora de Modificación Declaración Subasta Bimestral no valido <br>";
                        }
                    }
                    /////////////////////////////////////////////////////////////
                }
            }
            //// Validaciones Subasta Negociacion Directa
            if (ddlTipoSubasta.SelectedValue == "5")
            {
                if (TxtHorInPosNd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar la Hora Inicial de Ingreso de Posturas Negociación Directa. <br>";
                if (TxtHorFiPosNd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar la Hora Final de Ingreso de Posturas Negociación Directa. <br>";
                try
                {
                    if (Convert.ToDateTime(TxtHorInPosNd.Text.Trim()) >= Convert.ToDateTime(TxtHorFiPosNd.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de ingreso de posturas debe ser menor que la hora final. <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Error en el formato de horas. <br>";
                }

            }
            /// Validaciones Subasta Transporte Req. 005-2021 20210120
            if (ddlTipoSubasta.SelectedValue == "11")
            {
                string sVlidaHora = "S";
                if (ddlTipoRueTra11.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el tipo de rueda. <br>";
                if (txtNoDiaInTriNeg11.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Días hábiles desde inicio de trimestre de negociación para el desarrollo de la subasta. <br>";
                else
                {
                    if (txtNoDiaInTriNeg11.Text.Trim() == "0")
                        lblMensaje.Text += "Los Días hábiles desde inicio de trimestre de negociación para el desarrollo de la subasta no Puden ser iguales a 0. <br>";
                }
                if (txtHoraIniPubOfr11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Publicación de oferta . <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPubOfr11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Publicación de oferta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPubOfr11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPubOfr11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación de oferta debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación de oferta no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniDecPosCom11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Declaración posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDecPosCom11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Declaración posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDecPosCom11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDecPosCom11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Declaración posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniDecPosCom11.Text.Trim()) < Convert.ToDateTime(txtHoraFinPubOfr11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Declaración posturas de compra debe ser mayor o igual que la hora Final para Publicación de oferta. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Declaración posturas de compra no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniDesSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Desarrollo subasta. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDesSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Desarrollo subasta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDesSub11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDesSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Desarrollo subasta debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniDesSub11.Text.Trim()) < Convert.ToDateTime(txtHoraFinDecPosCom11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Desarrollo subasta debe ser mayor o igual que la hora Final para Declaración posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Desarrollo subasta no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniRecAdj11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Rechazo de adjudicaciones. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinRecAdj11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Rechazo de adjudicaciones. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniRecAdj11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinRecAdj11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Rechazo de adjudicaciones debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniRecAdj11.Text.Trim()) < Convert.ToDateTime(txtHoraFinDesSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Rechazo de adjudicaciones debe ser mayor o igual que la hora Final para Desarrollo subasta. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Rechazo de adjudicaciones no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniPubRes11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Publicación resultados. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPubRes11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Publicación resultados. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPubRes11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPubRes11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación resultados debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniPubRes11.Text.Trim()) < Convert.ToDateTime(txtHoraFinRecAdj11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación resultados debe ser mayor o igual que la hora Final para Rechazo de adjudicaciones. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación resultados no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniFinSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Finalización de la subasta. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinFinSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Finalización de la subasta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniFinSub11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinFinSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Finalización de la subasta debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniFinSub11.Text.Trim()) < Convert.ToDateTime(txtHoraFinPubRes11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Finalización de la subasta debe ser mayor o igual que la hora Final para Publicación resultados. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación resultados no valido <br>";
                    }
                }
            }
            ///  Hasta Aqui

            if (ddlTipoSubasta.SelectedValue != "2" && ddlRuedaAut.SelectedValue == "S")
                lblMensaje.Text += "Las ruedas automáticas diarias solo se pueden definir para la subasta UVCP. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlDestino.SelectedValue;
                lValorParametros[3] = TxtHoraApertura.Text.Trim();
                if (ddlTipoSubasta.SelectedValue == "2")
                {
                    lValorParametros[24] = txtHoraIniDecInf.Text.Trim();
                    lValorParametros[25] = txtHoraFinDecInf.Text.Trim();
                    lValorParametros[26] = txtHoraIniPub.Text.Trim();
                    lValorParametros[27] = txtHoraFinPub.Text.Trim();
                    lValorParametros[28] = txtHoraIniComp.Text.Trim();
                    lValorParametros[29] = txtHoraFinComp.Text.Trim();
                    lValorParametros[30] = txtHoraIniCalce.Text.Trim();
                    lValorParametros[31] = txtHoraFinCalce.Text.Trim();
                    lValorParametros[33] = txtHoraIniPre.Text.Trim();
                    lValorParametros[34] = txtHoraFinPre.Text.Trim();
                    lValorParametros[35] = txtHoraIniCont.Text.Trim();
                    lValorParametros[36] = txtHoraFinCont.Text.Trim();

                    //lValorParametros[4] = TxtMinOferVenta.Text.Trim();
                    //lValorParametros[5] = TxtMinConOfVenta.Text.Trim();
                    //lValorParametros[6] = TxtMinPubOfVenta.Text.Trim();
                    //lValorParametros[7] = TxtMinOfCompra.Text.Trim();
                    //lValorParametros[8] = TxtMinCalce.Text.Trim();
                    lValorParametros[10] = TxtNoPostCompra.Text.Trim();
                    lValorParametros[47] = ddlTipoRuedaTra.SelectedValue;  //20201207
                }
                //lValorParametros[9] = TxtMinModCont.Text.Trim();
                lValorParametros[11] = ddlTipoMercado.SelectedValue;
                lValorParametros[12] = ddlModifContratos.SelectedValue;
                lValorParametros[13] = ddlEstado.SelectedValue;
                lValorParametros[15] = ddlTipoSubasta.SelectedValue;
                if (ddlTipoSubasta.SelectedValue == "1")
                {
                    lValorParametros[16] = TxtMesPrevVendSmpsi.Text.Trim();
                    lValorParametros[17] = TxtDuraRondaSmpsi.Text.Trim();
                    lValorParametros[18] = TxtDiasDecOfVSmpsi.Text.Trim();
                    lValorParametros[19] = TxtDiasDecOfCSmpsi.Text.Trim();
                    lValorParametros[20] = TxtNHorModOfVSmpsi.Text.Trim();
                    lValorParametros[21] = TxtNHorModOfCSmpsi.Text.Trim();
                    lValorParametros[32] = TxtNoSemSmpsi.Text.Trim();
                }
                //20160921 UVLP
                if (ddlTipoSubasta.SelectedValue == "3")
                {
                    lValorParametros[24] = TxtHoraIniPubUvlp.Text.Trim();
                    lValorParametros[25] = TxtHoraFinPubUvlp.Text.Trim();
                    lValorParametros[28] = TxtHoraIniCompUvlp.Text.Trim();
                    lValorParametros[29] = TxtHoraFinCompUvlp.Text.Trim();
                    lValorParametros[30] = TxtHoraIniCalceUvlp.Text.Trim();
                    lValorParametros[31] = TxtHoraFinCalceUvlp.Text.Trim();
                }
                if (ddlTipoSubasta.SelectedValue == "4" || ddlTipoSubasta.SelectedValue == "6")
                {
                    lValorParametros[10] = TxtNoPosCCi.Text.Trim();
                    lValorParametros[23] = TxtDiaAntCi.Text.Trim();
                    lValorParametros[24] = TxtHorIniPubVCi.Text.Trim();
                    lValorParametros[25] = TxtHorFinPubVCi.Text.Trim();
                    lValorParametros[26] = TxtHorIniPubCantDCi.Text.Trim();
                    lValorParametros[27] = TxtHorFinPubCantDCi.Text.Trim();
                    lValorParametros[28] = TxtHorIniRecCantCCi.Text.Trim();
                    lValorParametros[29] = TxtHorFinRecCantCCi.Text.Trim();
                    lValorParametros[30] = TxtHorIniNegociaCi.Text.Trim();
                    lValorParametros[31] = TxtHorFinNegociaCi.Text.Trim();
                    lValorParametros[37] = TxtMinModV.Text.Trim();
                    lValorParametros[38] = TxtMinModC.Text.Trim();
                    // Campo nuevo ajuste subasta bimestrales por contingencia 20151105
                    if (ddlTipoSubasta.SelectedValue == "6")
                    {
                        lValorParametros[16] = TxtNoMesIniPer.Text.Trim();
                    }
                }
                if (ddlTipoSubasta.SelectedValue == "5")
                {
                    lValorParametros[30] = TxtHorInPosNd.Text.Trim();
                    lValorParametros[31] = TxtHorFiPosNd.Text.Trim();
                }
                /// Campos Nueva Subasta Tramsporte Req. 005-2021 20210120
                if (ddlTipoSubasta.SelectedValue == "11")
                {
                    lValorParametros[47] = ddlTipoRueTra11.SelectedValue;
                    lValorParametros[23] = txtNoDiaInTriNeg11.Text.Trim();
                    lValorParametros[24] = txtHoraIniPubOfr11.Text.Trim();
                    lValorParametros[25] = txtHoraFinPubOfr11.Text.Trim();
                    lValorParametros[33] = txtHoraIniDecPosCom11.Text.Trim();
                    lValorParametros[34] = txtHoraFinDecPosCom11.Text.Trim();
                    lValorParametros[30] = txtHoraIniDesSub11.Text.Trim();
                    lValorParametros[31] = txtHoraFinDesSub11.Text.Trim();
                    lValorParametros[28] = txtHoraIniRecAdj11.Text.Trim();
                    lValorParametros[29] = txtHoraFinRecAdj11.Text.Trim();
                    lValorParametros[26] = txtHoraIniPubRes11.Text.Trim();
                    lValorParametros[27] = txtHoraFinPubRes11.Text.Trim();
                    lValorParametros[35] = txtHoraIniFinSub11.Text.Trim();
                    lValorParametros[36] = txtHoraFinFinSub11.Text.Trim();
                }
                /// Hasta Aqui
                lValorParametros[39] = ddlRuedaAut.SelectedValue;
                if (chkLun.Checked)
                    lValorParametros[40] = "S";
                if (chkMar.Checked)
                    lValorParametros[41] = "S";
                if (chkMie.Checked)
                    lValorParametros[42] = "S";
                if (chkJue.Checked)
                    lValorParametros[43] = "S";
                if (chkVie.Checked)
                    lValorParametros[44] = "S";
                if (chkSab.Checked)
                    lValorParametros[45] = "S";
                if (chkDom.Checked)
                    lValorParametros[46] = "S";
                //////////////////////////////////////////////////////////////
                /// Campos Nuevos Req. 007-07 Subasta Bimestral 20170209 /////
                //////////////////////////////////////////////////////////////
                if (ddlTipoSubasta.SelectedValue == "6")
                {
                    lValorParametros[18] = TxtDiaHabDeclaraInf.Text.Trim();
                    lValorParametros[19] = TxtDiaHabPublicaInf.Text.Trim();
                    lValorParametros[33] = TxtHoraIniModDecSb.Text.Trim();
                    lValorParametros[34] = TxtHoraFinModDecSb.Text.Trim();
                    if (ChkEnero.Checked)
                        lValorParametros[40] = "S";
                    if (ChkFebrero.Checked)
                        lValorParametros[41] = "S";
                    if (ChkMarzo.Checked)
                        lValorParametros[42] = "S";
                    if (ChkAbril.Checked)
                        lValorParametros[43] = "S";
                    if (ChkMayo.Checked)
                        lValorParametros[44] = "S";
                    if (ChkJunio.Checked)
                        lValorParametros[45] = "S";
                    if (ChkJulio.Checked)
                        lValorParametros[46] = "S";
                    if (ChkAgosto.Checked)
                        lValorParametros[48] = "S";
                    if (ChkSeptiembre.Checked)
                        lValorParametros[49] = "S";
                    if (ChkOctubre.Checked)
                        lValorParametros[50] = "S";
                    if (ChkNoviembre.Checked)
                        lValorParametros[51] = "S";
                    if (ChkDicimebre.Checked)
                        lValorParametros[52] = "S";
                }
                //////////////////////////////////////////////////////////////

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTipoRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del Tipo de Rueda.! " + goInfo.mensaje_error;
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_tipo_rueda", "@P_descripcion",  "@P_destino_rueda", "@P_hora_apertura",
                                        "@P_minutos_oferta_venta","@P_minutos_control_venta","@P_minutos_publica_venta","@P_minutos_oferta_compra","@P_minutos_calce",
                                        "@P_minutos_modif_cont","@P_no_posturas_compra","@P_tipo_mercado","@P_permite_modif_manual_cont", "@P_estado", "@P_accion",
                                        "@P_codigo_tipo_subasta","@P_mes_prev_vend_smpsi","@P_duracion_ronda_smpsi","@P_dias_declara_ofer_v_smpsi", "@P_dias_declara_ofer_c_smpsi",
                                        "@P_hora_modif_ofer_v_smpsi","@P_hora_modif_ofer_c_smpsi","@P_duracion_ronda_suvlp","@P_dias_habil_ant_sub_sci","@P_hora_ini_publi_v_sci",
                                        "@P_hora_fin_publi_v_sci","@P_hora_ini_publ_cnt_dispi_v_sci","@P_hora_fin_publ_cnt_dispi_v_sci","@P_hora_ini_rec_solicitud_c_sci",
                                        "@P_hora_fin_rec_solicitud_c_sci","@P_hora_ini_negociacioni_sci","@P_hora_fin_negociacioni_sci","@P_no_semana_rev_inf_Smpsi",
                                        "@P_hora_ini_ctrl_v","@P_hora_fin_ctrl_v", "@P_hora_ini_modif_ctr", "@P_hora_fin_modif_ctr",
                                        "@P_min_modif_ofert_sci_v", "@P_min_modif_ofert_sci_c", // datos adicionales de SCI
                                        "@P_creacion_automatica", "@P_lunes", "@P_martes", "@P_miercoles", "@P_jueves", "@P_viernes", "@P_sabado", "@P_domingo", //Ruedas automaticas
                                        "@P_ind_tipo_rueda",  // 20160921 UVLP
                                        "@P_mes08","@P_mes09","@P_mes10","@P_mes11","@P_mes12" // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.Int, SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int, SqlDbType.Int,
                                        SqlDbType.Char, SqlDbType.Char,SqlDbType.Char, SqlDbType.Char,SqlDbType.Char, SqlDbType.Char,SqlDbType.Char, SqlDbType.Char,SqlDbType.Char, // 20160921 UVLP
                                        SqlDbType.Char, SqlDbType.Char,SqlDbType.Char, SqlDbType.Char,SqlDbType.Char // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                        };
        string[] lValorParametros = { "0", "", "", "", "0", "0", "0", "0", "0", "0", "0", "", "", "", "2",
                                      "0","0","0","0","0","0","0","0","0","","","","","","","","","0","","","","","0","0","N","N","N","N","N","N","N","N",
                                      ddlTpoNEgUvlp.SelectedValue ,// 20160921 UVLP
                                      "N","N","N","N","N" // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                    };
        lblMensaje.Text = "";
        try
        {
            if (TxtDescripcion.Text.Trim().Length < 3)
                lblMensaje.Text += " Debe digitar la descripción del tipo de rueda<br>";
            else
                if (VerificarExistencia(" descripcion = '" + this.TxtDescripcion.Text.Trim() + "' And codigo_tipo_rueda != " + LblCodigoTipoRueda.Text))
                lblMensaje.Text += " La Descripción de la Unidad de Medida  YA existe " + TxtDescripcion.Text + " <br>";
            if (ddlTipoSubasta.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Tipo de Subasta. <br>";
            if (ddlModifContratos.SelectedValue == "S" && (txtHoraIniCont.Text == "" || txtHoraFinCont.Text == ""))
                lblMensaje.Text += "Seleccionó Si en Permite Modificación Manual de Contratos, pero no definió las horas para hacerlo. <br>";
            if (ddlModifContratos.SelectedValue == "N" && (txtHoraIniCont.Text != "" || txtHoraFinCont.Text != ""))
                lblMensaje.Text += "Seleccionó No en Permite Modificación Manual de Contratos, pero definió horas para hacerlo. <br>";
            if (ddlRuedaAut.SelectedValue == "S" && !chkLun.Checked && !chkMar.Checked && !chkMie.Checked && !chkJue.Checked && !chkVie.Checked && !chkSab.Checked && !chkDom.Checked)
                lblMensaje.Text += "Debe seleccionar al menos un día de creación de ruedas automáticas. <br>";

            //// Validaciones Subasta Uselo o Vendalo Corto Plazo

            if (ddlTipoSubasta.SelectedValue == "2")
            {
                string sVlidaHora = "S";
                if (txtHoraIniDecInf.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de declaración de información. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDecInf.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de declaración de información. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDecInf.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDecInf.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de declaración de información debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de declaración de información no válido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniPre.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de declaración de precio de reserva y cantidad no disponible. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPre.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de declaración de precio y cantidad no disponible. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPre.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPre.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de declaración precio de reserva debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinDecInf.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPre.Text.Trim()))
                            lblMensaje.Text += "La hora final de declaración precio de reserva debe ser mayor que la hora final de declaración de información. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de declaración de precio de reserva no válido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniPub.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPub.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPub.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPub.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de declaración publicación debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinPre.Text.Trim()) > Convert.ToDateTime(txtHoraIniPub.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de publicación debe ser mayor o igual que la hora final de declaración de precio de reserva. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de publicación no válido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniComp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinComp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniComp.Text.Trim()) >= Convert.ToDateTime(txtHoraFinComp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinPub.Text.Trim()) > Convert.ToDateTime(txtHoraIniComp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser mayor o igual que la hora final de publicación. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de ingreso de posturas de compra no válido<br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniCalce.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial calce. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinCalce.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de calce. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniCalce.Text.Trim()) >= Convert.ToDateTime(txtHoraFinCalce.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinComp.Text.Trim()) > Convert.ToDateTime(txtHoraIniCalce.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser mayor o igual que la hora final de ingreso de posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de calce no válido<br>";
                    }
                }
                if (txtHoraIniCont.Text.Trim() != "" && txtHoraFinCont.Text.Trim() != "")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniCont.Text.Trim()) >= Convert.ToDateTime(txtHoraFinCont.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de modificación de contratos debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraFinCalce.Text.Trim()) > Convert.ToDateTime(txtHoraIniCont.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de modificación de contratos debe ser mayor o igual que la hora final de calce . <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de calce no válido<br>";
                    }
                }

                if (TxtNoPostCompra.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar el No. de Posturas de Compra. <br>";
                else
                {
                    if (TxtNoPostCompra.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Posturas de Compra no Puede ser igual a 0. <br>";
                }
            }
            //// Validaciones Subasta de Suministro Sin Interrupciones
            if (ddlTipoSubasta.SelectedValue == "1")
            {
                if (TxtMesPrevVendSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. del Mes Previo Ingreso de Vendedores Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtMesPrevVendSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. del Mes Previo Ingreso de Vendedores Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtDuraRondaSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el Tiempo de Duración de las Rondas de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtDuraRondaSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El Tiempo de Duración de las Rondas de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtDiasDecOfVSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Días Declara Oferta Vendedor de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtDiasDecOfVSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Días Declara Oferta Vendedor de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtDiasDecOfCSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Días Declara Oferta Comprador de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtDiasDecOfCSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Días Declara Oferta Comprador de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNHorModOfVSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Horas Modifica Oferta Vendedor de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtNHorModOfVSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Horas Modifica Oferta Vendedor de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNHorModOfCSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Horas Modifica Oferta Comprador de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtNHorModOfCSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Horas Modifica Oferta Comprador de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNoSemSmpsi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Semanas Revelación Información de Subasta sin Interrupciones. <br>";
                else
                {
                    if (TxtNoSemSmpsi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Semanas Revelación Información de Subasta sin Interrupciones no puede ser igual a 0. <br>";
                }
            }
            // 20160921 UVLP
            if (ddlTipoSubasta.SelectedValue == "3")
            {
                string sVlidaHora = "S";
                if (TxtHoraIniPubUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinPubUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniPubUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinPubUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de publicación debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora de publicación<br>";
                    }
                }
                sVlidaHora = "S";
                if (TxtHoraIniCompUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinCompUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniCompUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinCompUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(TxtHoraIniCompUvlp.Text.Trim()) < Convert.ToDateTime(TxtHoraFinPubUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser mayor o igual que la hora final de publicación. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato inválido de hora de ingreso de posturas de compra<br>";
                    }
                }
                sVlidaHora = "S";
                if (TxtHoraIniCalceUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de calce. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinCalceUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de calce. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniCalceUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinCalceUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(TxtHoraIniCalceUvlp.Text.Trim()) < Convert.ToDateTime(TxtHoraFinCompUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser mayor o igual que la hora final de ingreso de posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato inválido de hora de calce<br>";
                    }
                }
            }
            if (ddlTipoSubasta.SelectedValue == "4" || ddlTipoSubasta.SelectedValue == "6")
            {
                if (TxtDiaAntCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar el No. Dias Hábiles Anteriores para Subasta Contrato con Interrupciones. <br>";
                else
                {
                    if (TxtDiaAntCi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. Días Hábiles Anteriores para Subasta Contrato con Interrupciones no puede ser igual a 0. <br>";
                }
                if (TxtNoPosCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar el No. de Posturas de Compra Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtNoPosCCi.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Posturas de Compra Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniPubVCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Publicación Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniPubVCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Publicación Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinPubVCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Publicación Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinPubVCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Publicación Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniPubCantDCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniPubCantDCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Publicación Cantidad Disponible Venta de Subasta Contrato con interupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinPubCantDCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinPubCantDCi.Text.Trim() == "0")
                        lblMensaje.Text += "La Hora Final Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniRecCantCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Recibo Cantidad Compra de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniRecCantCCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Recibo Cantidad Compra de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinRecCantCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Recibo Cantidad Compra de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinRecCantCCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Recibo Cantidad Compra de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniNegociaCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Negociación de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniNegociaCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Negociación de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinNegociaCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Negociación de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinNegociaCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Negociación de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtMinModV.Text == "")
                    lblMensaje.Text += "Debe ingresar los minutos para modificación ofertas de venta. <br>";
                if (TxtMinModC.Text == "")
                    lblMensaje.Text += "Debe ingresar los minutos para modificación ofertas de compra. <br>";

                try
                {
                    if (Convert.ToDateTime(TxtHorIniPubVCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de publicación de venta debe ser menor que la hora final. <br>";
                    /// Se Comentarea validacion Req. 007-17 Subasta Bimestral 20170209 ///
                    //if (Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniPubCantDCi.Text.Trim()))
                    //    lblMensaje.Text += "La hora inicial de publicación de cantidad disponible debe ser mayor o igual que la hora final de publicación de venta. <br>";
                    if (Convert.ToDateTime(TxtHorIniPubCantDCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinPubCantDCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de publicación de cantidad disponible debe ser menor que la hora final<br>";
                    if (Convert.ToDateTime(TxtHorFinPubCantDCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniRecCantCCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de recibo de cantidad compra debe ser mayor o igual que la hora final de publicación de cantidad disponible<br>";
                    if (Convert.ToDateTime(TxtHorIniRecCantCCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinRecCantCCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de recibo de cantidad compra debe ser menor que la hora final <br>";
                    if (Convert.ToDateTime(TxtHorFinRecCantCCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniNegociaCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial negociación debe ser mayor o igual que la hora final de recibo de cantidad de compra<br>";
                    if (Convert.ToDateTime(TxtHorIniNegociaCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinNegociaCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial negociación debe ser menor que la hora final <br>";
                    ///////////////////////////////////////////////////////////////////////
                    /// Se Comentarea validacion Req. 007-17 Subasta Bimestral 20170209 ///
                    ///////////////////////////////////////////////////////////////////////
                    //DateTime dateValue = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorFinPubVCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorFinPubVCi.Text.Trim().Substring(3, 2)), 0);
                    //dateValue = dateValue.AddMinutes(Convert.ToInt16(TxtMinModV.Text));
                    //DateTime dateValue1 = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorIniPubCantDCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorIniPubCantDCi.Text.Trim().Substring(3, 2)), 0);
                    //if (dateValue > dateValue1)
                    //    lblMensaje.Text += "El tiempo de modificación de venta supera la hora de inicio de publicación <br>";
                    ///////////////////////////////////////////////////////////////////////
                    /// Se Comentarea validacion Req. 007-17 Subasta Bimestral 20170209 ///
                    ///////////////////////////////////////////////////////////////////////
                    //dateValue = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(3, 2)), 0);
                    //dateValue = dateValue.AddMinutes(Convert.ToInt16(TxtMinModC.Text));
                    //dateValue1 = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(3, 2)), 0);
                    //if (dateValue > dateValue1)
                    //    lblMensaje.Text += "El tiempo de modificación de compra supera la hora de inicio de negociación <br>";
                    DateTime dateValue = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(3, 2)), 0);
                    dateValue = dateValue.AddMinutes(Convert.ToInt16(TxtMinModC.Text));
                    DateTime dateValue1 = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(3, 2)), 0);
                    if (dateValue > dateValue1)
                        lblMensaje.Text += "El tiempo de modificación de compra supera la hora de inicio de negociación <br>";

                }
                catch (Exception)
                {
                    lblMensaje.Text += "Error en el formato de horas. <br>";
                }
                // Campo nuevo subasta bimestral por contingencia 20151105
                if (ddlTipoSubasta.SelectedValue == "6")
                {
                    string sVlidaHora = "S"; //007-07 Subasta Bimestral 20170209 
                    if (TxtNoMesIniPer.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingesar El No. Mes Adelante Def Inicio Periodo subasta Bimestral. <br>";
                    else
                    {
                        if (TxtNoMesIniPer.Text.Trim() == "0")
                            lblMensaje.Text += "El No. Mes Adelante Def Inicio Periodo subasta Bimestral no puede ser igual a 0. <br>";
                    }
                    // Validacion que para el mismo tipo de subasta no exista mas de un tipo de rueda con el mismo día habil de subasta
                    if (DelegadaBase.Servicios.ValidarExistencia("m_tipos_rueda", " codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue + " And estado = 'A' And dias_habil_ant_sub_sci = " + TxtDiaAntCi.Text.Trim() + " And codigo_tipo_rueda != " + LblCodigoTipoRueda.Text, goInfo))
                        lblMensaje.Text += "No pueden existir dos tipos de Rueda bimestrales que tengan el mismo Día habil del mes para apertura de rueda. <br>";
                    /////////////////////////////////////////////////////////////
                    /// Validaciones Req. 007-07 Subasta Bimestral 20170209 /////
                    /////////////////////////////////////////////////////////////
                    if (TxtDiaHabDeclaraInf.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Día hábil del Mes para Declaración de Información de la subasta Bimestral no puede ser igual a 0. <br>";
                    if (TxtDiaHabPublicaInf.Text.Trim() == "0")
                        lblMensaje.Text += "El No. de Día hábil del Mes para Publicación de Información de la subasta Bimestral no puede ser igual a 0. <br>";
                    if (TxtHoraIniModDecSb.Text.Trim().Length <= 0)
                    {
                        lblMensaje.Text += "Debe ingresar la Hora Inicial Modificación Declaración Subasta Bimestral. <br>";
                        sVlidaHora = "N";
                    }
                    if (TxtHoraFinModDecSb.Text.Trim().Length <= 0)
                    {
                        lblMensaje.Text += "Debe ingresar la Hora Final Modificación Declaración Subasta Bimestral. <br>";
                        sVlidaHora = "N";
                    }
                    if (sVlidaHora == "S")
                    {
                        try
                        {
                            if (Convert.ToDateTime(TxtHoraIniModDecSb.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinModDecSb.Text.Trim()))
                                lblMensaje.Text += "La hora inicial de Modificación Declaración Subasta Bimestral debe ser menor que la hora final. <br>";
                            if (Convert.ToDateTime(TxtHoraIniModDecSb.Text.Trim()) < Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()))
                                lblMensaje.Text += "La hora inicial de Modificación Declaración Subasta Bimestral debe ser mayor o igual que la hora Final de Publicación Venta Contratos en Firme Bimestrales. <br>";
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "Formato de hora de Modificación Declaración Subasta Bimestral no valido <br>";
                        }
                    }
                    /////////////////////////////////////////////////////////////

                }
            }
            //// Validaciones Subasta Negociacion Directa
            if (ddlTipoSubasta.SelectedValue == "5")
            {
                if (TxtHorInPosNd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar la Hora Inicial de Ingreso de Posturas Negociación Directa. <br>";
                if (TxtHorFiPosNd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingesar la Hora Final de Ingreso de Posturas Negociación Directa. <br>";
                try
                {
                    if (Convert.ToDateTime(TxtHorInPosNd.Text.Trim()) >= Convert.ToDateTime(TxtHorFiPosNd.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de ingreso de posturas debe ser menor que la hora final. <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Error en el formato de horas. <br>";
                }
            }
            /// Validaciones Subasta Transporte Req. 005-2021 20210120
            if (ddlTipoSubasta.SelectedValue == "11")
            {
                string sVlidaHora = "S";
                if (ddlTipoRueTra11.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el tipo de rueda. <br>";
                if (txtNoDiaInTriNeg11.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Días hábiles desde inicio de trimestre de negociación para el desarrollo de la subasta. <br>";
                else
                {
                    if (txtNoDiaInTriNeg11.Text.Trim() == "0")
                        lblMensaje.Text += "Los Días hábiles desde inicio de trimestre de negociación para el desarrollo de la subasta no Puden ser iguales a 0. <br>";
                }
                if (txtHoraIniPubOfr11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Publicación de oferta . <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPubOfr11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Publicación de oferta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPubOfr11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPubOfr11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación de oferta debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación de oferta no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniDecPosCom11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Declaración posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDecPosCom11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Declaración posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDecPosCom11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDecPosCom11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Declaración posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniDecPosCom11.Text.Trim()) < Convert.ToDateTime(txtHoraFinPubOfr11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Declaración posturas de compra debe ser mayor o igual que la hora Final para Publicación de oferta. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Declaración posturas de compra no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniDesSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Desarrollo subasta. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDesSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Desarrollo subasta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDesSub11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDesSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Desarrollo subasta debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniDesSub11.Text.Trim()) < Convert.ToDateTime(txtHoraFinDecPosCom11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Desarrollo subasta debe ser mayor o igual que la hora Final para Declaración posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Desarrollo subasta no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniRecAdj11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Rechazo de adjudicaciones. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinRecAdj11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Rechazo de adjudicaciones. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniRecAdj11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinRecAdj11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Rechazo de adjudicaciones debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniRecAdj11.Text.Trim()) < Convert.ToDateTime(txtHoraFinDesSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Rechazo de adjudicaciones debe ser mayor o igual que la hora Final para Desarrollo subasta. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Rechazo de adjudicaciones no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniPubRes11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Publicación resultados. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPubRes11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Publicación resultados. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPubRes11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPubRes11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación resultados debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniPubRes11.Text.Trim()) < Convert.ToDateTime(txtHoraFinRecAdj11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación resultados debe ser mayor o igual que la hora Final para Rechazo de adjudicaciones. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación resultados no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniFinSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Finalización de la subasta. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinFinSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Finalización de la subasta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniFinSub11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinFinSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Finalización de la subasta debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniFinSub11.Text.Trim()) < Convert.ToDateTime(txtHoraFinPubRes11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Finalización de la subasta debe ser mayor o igual que la hora Final para Publicación resultados. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación resultados no valido <br>";
                    }
                }
            }
            ///  Hasta Aqui
            if (ddlTipoSubasta.SelectedValue != "2" && ddlRuedaAut.SelectedValue == "S")
                lblMensaje.Text += "Las ruedas automáticas diarias solo se pueden definir para la subasta UVCP. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoTipoRueda.Text;
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlDestino.SelectedValue;
                lValorParametros[3] = TxtHoraApertura.Text.Trim();
                if (ddlTipoSubasta.SelectedValue == "2")
                {
                    lValorParametros[24] = txtHoraIniDecInf.Text.Trim();
                    lValorParametros[25] = txtHoraFinDecInf.Text.Trim();
                    lValorParametros[26] = txtHoraIniPub.Text.Trim();
                    lValorParametros[27] = txtHoraFinPub.Text.Trim();
                    lValorParametros[28] = txtHoraIniComp.Text.Trim();
                    lValorParametros[29] = txtHoraFinComp.Text.Trim();
                    lValorParametros[30] = txtHoraIniCalce.Text.Trim();
                    lValorParametros[31] = txtHoraFinCalce.Text.Trim();
                    lValorParametros[33] = txtHoraIniPre.Text.Trim();
                    lValorParametros[34] = txtHoraFinPre.Text.Trim();
                    lValorParametros[35] = txtHoraIniCont.Text.Trim();
                    lValorParametros[36] = txtHoraFinCont.Text.Trim();


                    //lValorParametros[4] = TxtMinOferVenta.Text.Trim();
                    //lValorParametros[5] = TxtMinConOfVenta.Text.Trim();
                    //lValorParametros[6] = TxtMinPubOfVenta.Text.Trim();
                    //lValorParametros[7] = TxtMinOfCompra.Text.Trim();
                    //lValorParametros[8] = TxtMinCalce.Text.Trim();
                    lValorParametros[10] = TxtNoPostCompra.Text.Trim();
                    lValorParametros[47] = ddlTipoRuedaTra.SelectedValue; //20201207
                }
                //lValorParametros[9] = TxtMinModCont.Text.Trim();
                lValorParametros[11] = ddlTipoMercado.SelectedValue;
                lValorParametros[12] = ddlModifContratos.SelectedValue;
                lValorParametros[13] = ddlEstado.SelectedValue;
                lValorParametros[15] = ddlTipoSubasta.SelectedValue;
                if (ddlTipoSubasta.SelectedValue == "1")
                {
                    lValorParametros[16] = TxtMesPrevVendSmpsi.Text.Trim();
                    lValorParametros[17] = TxtDuraRondaSmpsi.Text.Trim();
                    lValorParametros[18] = TxtDiasDecOfVSmpsi.Text.Trim();
                    lValorParametros[19] = TxtDiasDecOfCSmpsi.Text.Trim();
                    lValorParametros[20] = TxtNHorModOfVSmpsi.Text.Trim();
                    lValorParametros[21] = TxtNHorModOfCSmpsi.Text.Trim();
                    lValorParametros[32] = TxtNoSemSmpsi.Text.Trim();
                }
                //20160921 UVLP
                if (ddlTipoSubasta.SelectedValue == "3")
                {
                    lValorParametros[24] = TxtHoraIniPubUvlp.Text.Trim();
                    lValorParametros[25] = TxtHoraFinPubUvlp.Text.Trim();
                    lValorParametros[28] = TxtHoraIniCompUvlp.Text.Trim();
                    lValorParametros[29] = TxtHoraFinCompUvlp.Text.Trim();
                    lValorParametros[30] = TxtHoraIniCalceUvlp.Text.Trim();
                    lValorParametros[31] = TxtHoraFinCalceUvlp.Text.Trim();
                }
                if (ddlTipoSubasta.SelectedValue == "4" || ddlTipoSubasta.SelectedValue == "6")
                {
                    lValorParametros[10] = TxtNoPosCCi.Text.Trim();
                    lValorParametros[23] = TxtDiaAntCi.Text.Trim();
                    lValorParametros[24] = TxtHorIniPubVCi.Text.Trim();
                    lValorParametros[25] = TxtHorFinPubVCi.Text.Trim();
                    lValorParametros[26] = TxtHorIniPubCantDCi.Text.Trim();
                    lValorParametros[27] = TxtHorFinPubCantDCi.Text.Trim();
                    lValorParametros[28] = TxtHorIniRecCantCCi.Text.Trim();
                    lValorParametros[29] = TxtHorFinRecCantCCi.Text.Trim();
                    lValorParametros[30] = TxtHorIniNegociaCi.Text.Trim();
                    lValorParametros[31] = TxtHorFinNegociaCi.Text.Trim();
                    lValorParametros[37] = TxtMinModV.Text.Trim();
                    lValorParametros[38] = TxtMinModC.Text.Trim();
                    // Campo nuevo ajuste subasta bimestrales por contingencia 20151105
                    if (ddlTipoSubasta.SelectedValue == "6")
                    {
                        lValorParametros[16] = TxtNoMesIniPer.Text.Trim();
                    }
                }
                if (ddlTipoSubasta.SelectedValue == "5")
                {
                    lValorParametros[30] = TxtHorInPosNd.Text.Trim();
                    lValorParametros[31] = TxtHorFiPosNd.Text.Trim();
                }
                /// Campos Nueva Subasta Tramsporte Req. 005-2021 20210120
                if (ddlTipoSubasta.SelectedValue == "11")
                {
                    lValorParametros[47] = ddlTipoRueTra11.SelectedValue;
                    lValorParametros[23] = txtNoDiaInTriNeg11.Text.Trim();
                    lValorParametros[24] = txtHoraIniPubOfr11.Text.Trim();
                    lValorParametros[25] = txtHoraFinPubOfr11.Text.Trim();
                    lValorParametros[33] = txtHoraIniDecPosCom11.Text.Trim();
                    lValorParametros[34] = txtHoraFinDecPosCom11.Text.Trim();
                    lValorParametros[30] = txtHoraIniDesSub11.Text.Trim();
                    lValorParametros[31] = txtHoraFinDesSub11.Text.Trim();
                    lValorParametros[28] = txtHoraIniRecAdj11.Text.Trim();
                    lValorParametros[29] = txtHoraFinRecAdj11.Text.Trim();
                    lValorParametros[26] = txtHoraIniPubRes11.Text.Trim();
                    lValorParametros[27] = txtHoraFinPubRes11.Text.Trim();
                    lValorParametros[35] = txtHoraIniFinSub11.Text.Trim();
                    lValorParametros[36] = txtHoraFinFinSub11.Text.Trim();
                }
                /// Hasta Aqui
                lValorParametros[39] = ddlRuedaAut.SelectedValue;
                if (chkLun.Checked)
                    lValorParametros[40] = "S";
                if (chkMar.Checked)
                    lValorParametros[41] = "S";
                if (chkMie.Checked)
                    lValorParametros[42] = "S";
                if (chkJue.Checked)
                    lValorParametros[43] = "S";
                if (chkVie.Checked)
                    lValorParametros[44] = "S";
                if (chkSab.Checked)
                    lValorParametros[45] = "S";
                if (chkDom.Checked)
                    lValorParametros[46] = "S";
                //////////////////////////////////////////////////////////////
                /// Campos Nuevos Req. 007-07 Subasta Bimestral 20170209 /////
                //////////////////////////////////////////////////////////////
                if (ddlTipoSubasta.SelectedValue == "6")
                {
                    lValorParametros[18] = TxtDiaHabDeclaraInf.Text.Trim();
                    lValorParametros[19] = TxtDiaHabPublicaInf.Text.Trim();
                    lValorParametros[33] = TxtHoraIniModDecSb.Text.Trim();
                    lValorParametros[34] = TxtHoraFinModDecSb.Text.Trim();
                    if (ChkEnero.Checked)
                        lValorParametros[40] = "S";
                    if (ChkFebrero.Checked)
                        lValorParametros[41] = "S";
                    if (ChkMarzo.Checked)
                        lValorParametros[42] = "S";
                    if (ChkAbril.Checked)
                        lValorParametros[43] = "S";
                    if (ChkMayo.Checked)
                        lValorParametros[44] = "S";
                    if (ChkJunio.Checked)
                        lValorParametros[45] = "S";
                    if (ChkJulio.Checked)
                        lValorParametros[46] = "S";
                    if (ChkAgosto.Checked)
                        lValorParametros[48] = "S";
                    if (ChkSeptiembre.Checked)
                        lValorParametros[49] = "S";
                    if (ChkOctubre.Checked)
                        lValorParametros[50] = "S";
                    if (ChkNoviembre.Checked)
                        lValorParametros[51] = "S";
                    if (ChkDicimebre.Checked)
                        lValorParametros[52] = "S";
                }
                //////////////////////////////////////////////////////////////


                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTipoRueda", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización del Tipo de Rueda.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoTipoRueda.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoTipoRueda.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoTipoRueda.Text != "")
            manejo_bloqueo("E", LblCodigoTipoRueda.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_tipos_rueda", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='dbo.m_tipos_rueda' and llave_registro='codigo_tipo_rueda=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_tipo_rueda=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_tipos_rueda";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_tipos_rueda", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "", "S", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusTipoRueda.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusTipoRueda.Text.Trim();
                lsParametros += " Código Tipo Rueda : " + TxtBusTipoRueda.Text;

            }
            if (TxtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
                lsParametros += " - Descripción: " + TxtBusDescripcion.Text;
            }
            if (ddlBusDestino.SelectedValue != "S")
            {
                lValorParametros[2] = ddlBusDestino.SelectedValue;
                lsParametros += " - Destino Rueda: " + ddlBusDestino.SelectedItem.ToString();
            }
            if (ddlBusTipoSubasta.SelectedValue != "0")
            {
                lValorParametros[3] = ddlBusTipoSubasta.SelectedValue.Trim();
                lsParametros += " - Tipo Subasta: " + ddlBusTipoSubasta.SelectedItem.ToString();
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetTipoRueda&nombreParametros=@P_codigo_tipo_rueda*@P_descripcion*@P_destino*@P_codigo_tipo_subasta&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=Producto*valor_negocio*dias_plazo*valor_tasa_venta*valor_tasa_compra*valor_tasa_cierre&titulo_informe=Listado de Tipos de Rueda&TituloParametros=" + lsParametros);
        }
        catch (Exception)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }
    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_tipo_rueda <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_tipos_rueda&procedimiento=pa_ValidarExistencia&columnas=codigo_tipo_rueda*descripcion*destino_rueda*hora_apertura*tipo_mercado*codigo_tipo_subasta*permite_modif_manual_cont*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoSubasta_SelectedIndexChanged(object sender, EventArgs e)
    {
        TrUvcp01.Visible = false;
        TrUvcp02.Visible = false;
        TrUvcp03.Visible = false;
        TrSmpsi01.Visible = false;
        TrSmpsi02.Visible = false;
        TrSmpsi03.Visible = false;
        TrSmpsi04.Visible = false;
        //TrUvlp01.Visible = false;
        TrSci01.Visible = false;
        TrSci02.Visible = false;
        TrSci03.Visible = false;
        TrSci04.Visible = false;
        TrSci05.Visible = false;
        TrSci06.Visible = false;
        TrHora.Visible = false;
        TrModCont.Visible = false;
        txtHoraIniCont.Text = "";
        txtHoraFinCont.Text = "";
        TrSnd01.Visible = false;
        TrSci07.Visible = false;
        TrUVLP01.Visible = false; //20160921 UVLP
        TrUVLP02.Visible = false; //20160921 UVLP
        TrUVLP03.Visible = false; //20160921 UVLP
        TrUVLP04.Visible = false; //20160921 UVLP
        TrSci08.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
        TrSci09.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
        TrMes.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
        TrMes1.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
        // Campos Nueva Subasta Transporte Req. 005-2021 20210120
        TrStra01.Visible = false;
        TrStra02.Visible = false;
        TrStra03.Visible = false;
        TrStra04.Visible = false;
        // Hasta Aqui
        /// Subasta de Suministro sin Interrupciones        
        if (ddlTipoSubasta.SelectedValue == "1")
        {
            TrSmpsi01.Visible = true;
            TrSmpsi02.Visible = true;
            TrSmpsi03.Visible = true;
            TrSmpsi04.Visible = true;
        }
        /// Subasta Uselo o Vendalo de Cotro Plazo        
        if (ddlTipoSubasta.SelectedValue == "2")
        {
            TrUvcp01.Visible = true;
            TrUvcp02.Visible = true;
            TrUvcp03.Visible = true;
            TrModCont.Visible = true;
        }
        else
        {
            ddlModifContratos.SelectedValue = "N";
            txtHoraIniCont.Text = "";
            txtHoraFinCont.Text = "";
        }
        //20160921 UVLP
        if (ddlTipoSubasta.SelectedValue == "3")
        {
            TrUVLP01.Visible = true;
            TrUVLP02.Visible = true;
            TrUVLP03.Visible = true;
            TrUVLP04.Visible = true;
        }
        /// Subasta de COntratos con Interrupciones
        if (ddlTipoSubasta.SelectedValue == "4" || ddlTipoSubasta.SelectedValue == "6")
        {
            if (ddlTipoSubasta.SelectedValue == "4")
            {
                lblDiasSCI.Text = "No. Dias Hábiles Anteriores para Contratos Con Interrupciones";
                lblSub01.Text = "No. Posturas de Compra Contratos con Interrupciones";
                lblSub02.Text = "Hora Inicial Publicación Venta Contratos con Interrupciones";
                lblSub03.Text = "Hora Final Publicación Venta Contratos con Interrupciones";
                lblSub04.Text = "Hora Inicial Publicación Cantidad Dispinible Venta Contratos con Interrupciones";
                lblSub05.Text = "Hora Final Publicación Cantidad Disponible Venta Contratos con Interrupciones";
                lblSub06.Text = "Hora Inicial Recibo Cantidad Compra Contratos con Interrupciones";
                lblSub07.Text = "Hora Final Recibo Cantidad Compra Contratos con Interrupciones";
                lblSub08.Text = "Hora Inicial Negociación Contratos con Interrupciones";
                lblSub09.Text = "Hora Final Negociación Contratos con Interrupciones";
            }
            else
            {
                lblDiasSCI.Text = "No Días Hábiles del mes para apertura de rueda";
                lblSub01.Text = "No. Posturas de Compra Contratos en firme bimestrales";
                lblSub02.Text = "Hora Inicial Publicación Venta Contratos en firme bimestrales";
                lblSub03.Text = "Hora Final Publicación Venta Contratos en firme bimestrales";
                lblSub04.Text = "Hora Inicial Publicación Cantidad Dispinible Venta Contratos en firme bimestrales";
                lblSub05.Text = "Hora Final Publicación Cantidad Dispinible Venta Contratos en firme bimestrales";
                lblSub06.Text = "Hora Inicial Recibo Cantidad Compra Contratos en firme bimestrales";
                lblSub07.Text = "Hora Final Recibo Cantidad Compra Contratos en firme bimestrales";
                lblSub08.Text = "Hora Inicial Negociación Contratos en firme bimestrales";
                lblSub09.Text = "Hora Final Negociación Contratos en firme bimestrales";
                // Campo nuevo para ajuste de subastas bimestrales por tema de ocntingencia 20151105
                lblSub10.Text = "No. Mes Adelante Definición Inicio Periodo bimestrales";
                TrSci07.Visible = true;
                /////////////////////////////////////////////////////////////
                //// Campos Nuevos Req.007-17 Subasta Bimestral 20170209 ////
                /////////////////////////////////////////////////////////////
                TrSci08.Visible = true;
                TrSci09.Visible = true;
                TrMes.Visible = true;
                TrMes1.Visible = true;
                lblSub11.Text = "Día hábil para la Declaración de Información";
                lblSub12.Text = "Día hábil para la Publicación de Información";
                /////////////////////////////////////////////////////////////
            }
            TrSci01.Visible = true;
            TrSci02.Visible = true;
            TrSci03.Visible = true;
            TrSci04.Visible = true;
            TrSci05.Visible = true;
            TrSci06.Visible = true;
        }
        /// Subasta Negociacion Directa
        if (ddlTipoSubasta.SelectedValue == "5")
        {
            TrSnd01.Visible = true;
        }
        /// Nueva Subasta Transporte Req. 005-2021 20210121
        if (ddlTipoSubasta.SelectedValue == "11")
        {
            TrStra01.Visible = true;
            TrStra02.Visible = true;
            TrStra03.Visible = true;
            TrStra04.Visible = true;
        }
        // Hasta Aqui
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlRuedaAut_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRuedaAut.SelectedValue == "S")
        {
            trDia.Visible = true;
        }
        else
        {
            trDia.Visible = false;
            chkLun.Checked = false;
            chkMar.Checked = false;
            chkMie.Checked = false;
            chkJue.Checked = false;
            chkVie.Checked = false;
            chkSab.Checked = false;
            chkDom.Checked = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20201207
    protected void ddlDestino_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTipoSubasta.SelectedValue == "2" && ddlDestino.SelectedValue == "T")
        {
            TrUvcp00.Visible = true;
            ddlTipoRuedaTra.SelectedValue = "R";
        }
        else
        {
            TrUvcp00.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20201207
    protected void ddlTipoRuedaTra_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTipoRuedaTra.SelectedValue == "R")
            TrUvcp01.Visible = true;
        else
        {
            TrUvcp01.Visible = false;
            txtHoraIniDecInf.Text = "00:00";
            txtHoraFinDecInf.Text = "00:01";
            txtHoraIniPre.Text = "00:02";
            txtHoraFinPre.Text = "00:03";
        }
    }
}