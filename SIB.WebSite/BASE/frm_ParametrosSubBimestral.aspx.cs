﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParametrosSubBimestral : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Subasta Bimestral";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string gsTabla = "m_parametros_sub_bim";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtFechaIniProCom.Text = lLector["fecha_ini_comer"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_comer"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_comer"].ToString().Substring(0, 2);
                    TxtFechaFinProCom.Text = lLector["fecha_fin_comer"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_comer"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_comer"].ToString().Substring(0, 2);
                    TxtFechaIniDeclPrev.Text = lLector["fecha_ini_dec_prev"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_dec_prev"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_dec_prev"].ToString().Substring(0, 2);
                    TxtFechaFinDecPrev.Text = lLector["fecha_fin_dec_prev"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_dec_prev"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_dec_prev"].ToString().Substring(0, 2);
                    TxtAnoDeclara.Text = lLector["año_dec_prev"].ToString();
                    TxtNoSubBloqueo.Text = lLector["no_sub_bloq_gar"].ToString();
                    TxtValAdicPrecRese.Text = lLector["valor_adc_precio"].ToString();
                    TxtFechaPubPrev.Text = lLector["fecha_pub_prev"].ToString().Substring(6, 4) + "/" + lLector["fecha_pub_prev"].ToString().Substring(3, 2) + "/" + lLector["fecha_pub_prev"].ToString().Substring(0, 2);
                    TxtAnoDecPrev.Text = lLector["año_dec_prev_nv"].ToString();  //20170714 ajuste de año de declaracion
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado.";

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_ini_comer","@P_fecha_fin_comer","@P_fecha_ini_dec_prev","@P_fecha_fin_dec_prev","@P_año_dec_prev","@P_no_sub_bloq_gar",
                                        "@P_valor_adc_precio", "@P_observaciones","@P_accion", "@P_fecha_pub_prev", "@P_año_dec_prev_nv" };  //20170714 ajuste de año de declaracion
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, 
                                        SqlDbType.Decimal, SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int }; //20170714 ajuste de año de declaracion
        string[] lValorParametros = { "", "", "", "", "0", "0", "0", "", "1", "", "0" }; //20170714 ajuste de año de declaracion
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;

        try
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniProCom.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Inicial Proceso de Comercialización.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinProCom.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Final Proceso de Comercialización.<br>";

            }
            if (lblMensaje.Text == "")
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final Proceso de Comercialización no puede ser menor a la Fecha Inicial.<br>";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniDeclPrev.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Inicial Declaración Previa.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinDecPrev.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Final Declaración Previa.<br>";

            }
            if (lblMensaje.Text == "")
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final Declaración Previa no puede ser menor a la Fecha Inicial.<br>";

            try
            {
                if (Convert.ToInt32(TxtAnoDeclara.Text) < DateTime.Now.Year)
                    lblMensaje.Text += "El Año de gas de las subastas No debe ser menor al Año Actual. <br>"; //20170714 ajuste de ño de declaración
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Año de gas de las subastas digitado no es válido <br>"; //20170714 ajuste de ño de declaración
            }

            try
            {
                if (Convert.ToInt32(TxtNoSubBloqueo.Text) < 0)
                    lblMensaje.Text += "El Número de Subastas a bloquear por No Constitución de Garantías No debe ser menor a 0. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Número de Subastas a bloquear por No Constitución de Garantías digitado no es válido <br>";
            }
            string sOferta = TxtValAdicPrecRese.Text.Replace(",", "");
            int iPos = sOferta.IndexOf(".");
            if (iPos > 0)
                if (sOferta.Length - iPos > 3)
                    lblMensaje.Text += "Se permiten máximo 2 decimales en el Valor Adicional Precio de Reserva <br>";
            try
            {
                if (Convert.ToDouble(sOferta) < 0)
                    lblMensaje.Text += "El Valor Adicional Precio de Reserva debe ser mayor que cero <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Valor Adicional Precio de Reserva digitado no es válido <br>";
            }
            try
            {
                Convert.ToDateTime(TxtFechaPubPrev.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha de publicación previa.<br>";

            }
            //20170714 ajuste de ño de declaración
            try
            {
                if (Convert.ToInt32(TxtAnoDecPrev.Text) < DateTime.Now.Year)
                    lblMensaje.Text += "El Año para el cual se realiza la Declaración previa No debe ser menor al Año Actual. <br>"; 
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Año para el cual se realiza la Declaración previa digitado no es válido <br>"; 
            }
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaIniProCom.Text.Trim();
                lValorParametros[1] = TxtFechaFinProCom.Text.Trim();
                lValorParametros[2] = TxtFechaIniDeclPrev.Text.Trim();
                lValorParametros[3] = TxtFechaFinDecPrev.Text.Trim();
                lValorParametros[4] = TxtAnoDeclara.Text.Trim();
                lValorParametros[5] = TxtNoSubBloqueo.Text.Trim();
                lValorParametros[6] = TxtValAdicPrecRese.Text.Trim();
                lValorParametros[7] = TxtObservacion.Text.Trim();
                lValorParametros[9] = TxtFechaPubPrev.Text.Trim();
                lValorParametros[10] = TxtAnoDecPrev.Text.Trim(); //20170714 ajuste de año de declaración

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosSubBimestral", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación de Parametros Subasta Bimestral.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_ini_comer","@P_fecha_fin_comer","@P_fecha_ini_dec_prev","@P_fecha_fin_dec_prev","@P_año_dec_prev","@P_no_sub_bloq_gar",
                                        "@P_valor_adc_precio", "@P_observaciones","@P_accion", "@P_fecha_pub_prev", "@P_año_dec_prev_nv" };  //20170714 ajuste de año de declaracion
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, 
                                        SqlDbType.Decimal, SqlDbType.VarChar,SqlDbType.Int, SqlDbType.VarChar,SqlDbType.Int}; //20170714 ajuste de año de declaracion
        string[] lValorParametros = { "", "", "", "", "0", "0", "0", "", "2", "", "0" }; //20170714 ajuste de año de declaracion
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        try
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniProCom.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Inicial Proceso de Comercialización.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinProCom.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Final Proceso de Comercialización.<br>";

            }
            if (lblMensaje.Text == "")
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final Proceso de Comercialización no puede ser menor a la Fecha Inicial.<br>";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniDeclPrev.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Inicial Declaración Previa.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinDecPrev.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo Fecha Final Declaración Previa.<br>";

            }
            if (lblMensaje.Text == "")
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final Declaración Previa no puede ser menor a la Fecha Inicial.<br>";
            
            try
            {
                if (Convert.ToInt32(TxtAnoDeclara.Text) < DateTime.Now.Year)
                    lblMensaje.Text += "El Año de gas de las subastas No debe ser menor al Año Actual. <br>"; //20170714 ajuste de año de declaracion
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Año de gas de las subatas digitado no es válido <br>"; //20170714 ajuste de año de declaracion
            }

            try
            {
                if (Convert.ToInt32(TxtNoSubBloqueo.Text) < 0)
                    lblMensaje.Text += "El Número de Subastas a bloquear por No Constitución de Garantías No debe ser menor a 0. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Número de Subastas a bloquear por No Constitución de Garantías digitado no es válido <br>";
            }
            string sOferta = TxtValAdicPrecRese.Text.Replace(",", "");
            int iPos = sOferta.IndexOf(".");
            if (iPos > 0)
                if (sOferta.Length - iPos > 3)
                    lblMensaje.Text += "Se permiten máximo 2 decimales en el Valor Adicional Precio de Reserva <br>";
            try
            {
                if (Convert.ToDouble(sOferta) < 0)
                    lblMensaje.Text += "El Valor Adicional Precio de Reserva debe ser mayor que cero <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Valor Adicional Precio de Reserva digitado no es válido <br>";
            }
            try
            {
                Convert.ToDateTime(TxtFechaPubPrev.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en la fecha de publicación Previa.<br>";

            }
            //20170714 ajuste de año de declaracion
            try
            {
                if (Convert.ToInt32(TxtAnoDecPrev.Text) < DateTime.Now.Year)
                    lblMensaje.Text += "El Año para el cual se realiza la Declaración previa No debe ser menor al Año Actual. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "El Año para el cual se realiza la Declaración previa digitado no es válido <br>";
            }

            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaIniProCom.Text.Trim();
                lValorParametros[1] = TxtFechaFinProCom.Text.Trim();
                lValorParametros[2] = TxtFechaIniDeclPrev.Text.Trim();
                lValorParametros[3] = TxtFechaFinDecPrev.Text.Trim();
                lValorParametros[4] = TxtAnoDeclara.Text.Trim();
                lValorParametros[5] = TxtNoSubBloqueo.Text.Trim();
                lValorParametros[6] = TxtValAdicPrecRese.Text.Trim();
                lValorParametros[7] = TxtObservacion.Text.Trim();
                lValorParametros[9] = TxtFechaPubPrev.Text.Trim();
                lValorParametros[10] = TxtAnoDecPrev.Text.Trim(); //20170714 ajuste de año de declaracion

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosSubBimestral", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de los Parametros Subasta Bimestral .!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// </summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_sub_bim' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}