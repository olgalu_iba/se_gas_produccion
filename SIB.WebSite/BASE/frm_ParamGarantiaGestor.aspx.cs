﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParamGarantiaGestor : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Garantías servicios gestor";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string sRutaArc = ConfigurationManager.AppSettings["RutaIMG"].ToString();
    string gsTabla = "m_param_garant_gestor";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    //private void EstablecerPermisosSistema()
    //{
    //    Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
    //    imbCrear.Visible = (Boolean)permisos["INSERT"];
    //    imbActualiza.Visible = (Boolean)permisos["UPDATE"];
    //}
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtFechaPago.Text = lLector["fecha_pago"].ToString().Substring(6, 4) + "/" + lLector["fecha_pago"].ToString().Substring(3, 2) + "/" + lLector["fecha_pago"].ToString().Substring(0, 2); //20170126 rq002 ajutes fact y gar
                    TxtFechaCont.Text = lLector["fecha_contrato"].ToString().Substring(6, 4) + "/" + lLector["fecha_contrato"].ToString().Substring(3, 2) + "/" + lLector["fecha_contrato"].ToString().Substring(0, 2); //20170126 rq002 ajutes fact y gar
                    TxtFechaCons.Text = lLector["fecha_lim_cons_gar"].ToString().Substring(6, 4) + "/" + lLector["fecha_lim_cons_gar"].ToString().Substring(3, 2) + "/" + lLector["fecha_lim_cons_gar"].ToString().Substring(0, 2); //20170126 rq002 ajutes fact y gar
                    TxtFechaAprob.Text = lLector["fecha_aprob_gar"].ToString().Substring(6, 4) + "/" + lLector["fecha_aprob_gar"].ToString().Substring(3, 2) + "/" + lLector["fecha_aprob_gar"].ToString().Substring(0, 2); //20170126 rq002 ajutes fact y gar
                    TxtFechaLib.Text = lLector["fecha_lib_gar"].ToString().Substring(6, 4) + "/" + lLector["fecha_lib_gar"].ToString().Substring(3, 2) + "/" + lLector["fecha_lib_gar"].ToString().Substring(0, 2); //20170126 rq002 ajutes fact y gar
                    TxtParraf1.Text = lLector["texto_carta_1"].ToString(); //20170126 rq002 ajutes fact y gar
                    TxtParraf2.Text = lLector["texto_carta_2"].ToString(); //20170126 rq002 ajutes fact y gar
                    TxtParraf3.Text = lLector["texto_carta_3"].ToString(); //20170126 rq002 ajutes fact y gar
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado.";

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_pago", "@P_fecha_contrato", "@P_fecha_lim_cons_gar", "@P_fecha_aprob_gar","@P_fecha_lib_gar","@P_texto_carta_1","@P_texto_carta_2","@P_texto_carta_3", "@P_observaciones"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar ,SqlDbType.VarChar 
                                      };
        string[] lValorParametros = { "", "", "", "", "", "", "", "", "", }; 
        lblMensaje.Text = "";
        try
        {
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaPago.Text.Trim();  
                lValorParametros[1] = TxtFechaCont.Text.Trim();  
                lValorParametros[2] = TxtFechaCons.Text.Trim();  
                lValorParametros[3] = TxtFechaAprob.Text.Trim(); 
                lValorParametros[4] = TxtFechaLib.Text.Trim();  
                lValorParametros[5] = TxtParraf1.Text.Trim();  
                lValorParametros[6] = TxtParraf2.Text.Trim();  
                lValorParametros[7] = TxtParraf3.Text.Trim();  
                lValorParametros[8] = TxtObservacion.Text.Trim();
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParamGarGestor", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de Parámetros de garantías del gestor.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_pago", "@P_fecha_contrato", "@P_fecha_lim_cons_gar", "@P_fecha_aprob_gar","@P_fecha_lib_gar","@P_texto_carta_1","@P_texto_carta_2","@P_texto_carta_3", "@P_observaciones"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar ,SqlDbType.VarChar 
                                      };
        string[] lValorParametros = { "", "", "", "", "", "", "", "", "", };
        lblMensaje.Text = "";
        try
        {
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaPago.Text.Trim();
                lValorParametros[1] = TxtFechaCont.Text.Trim();
                lValorParametros[2] = TxtFechaCons.Text.Trim();
                lValorParametros[3] = TxtFechaAprob.Text.Trim();
                lValorParametros[4] = TxtFechaLib.Text.Trim();
                lValorParametros[5] = TxtParraf1.Text.Trim();
                lValorParametros[6] = TxtParraf2.Text.Trim();
                lValorParametros[7] = TxtParraf3.Text.Trim();
                lValorParametros[8] = TxtObservacion.Text.Trim();
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParamGarGestor", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de los Parámetros de garantías del gestor.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_param_garant_gestor' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}