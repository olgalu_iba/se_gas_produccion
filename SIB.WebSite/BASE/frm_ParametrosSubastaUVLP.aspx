﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosSubastaUVLP.aspx.cs"
    Inherits="BASE_frm_ParametrosSubastaUVLP" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Fecha Inicial Declaración Información Posterior Subasta {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniDecInf" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaCamFac" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicial Declaración Información Posterior Subasta"
                    ControlToValidate="TxtFechaIniDecInf" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Fecha Final Declaración Información Posterior Subasta {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFinDecInf" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaFinDecInf" runat="server" ErrorMessage="Debe Ingresar la Fecha Final Declaración Información Posterior Subasta"
                    ControlToValidate="TxtFechaFinDecInf" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Inicial Modificación Declaración de Información {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaModIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaModIni" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicial de Modificación de declaración de Información"
                    ControlToValidate="TxtFechaModIni" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Fecha Final Modificación Declaración de Información {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaModFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaModFin" runat="server" ErrorMessage="Debe Ingresar la Fecha Final de Modificación de declaración de Información"
                    ControlToValidate="TxtFechaModFin" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Publicación Inicial Capacidad Excedentaria {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaPubCeIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaPubCeIni" runat="server" ErrorMessage="Debe Ingresar la Fecha de Publicación Inicial de Capacidad Excedentaria"
                    ControlToValidate="TxtFechaPubCeIni" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Fecha Publicación Final Capacidad Excedentaria {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaPubCeFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaPubCeFin" runat="server" ErrorMessage="Debe Ingresar la Fecha Final de Publicación de Capacidad Excedentaria"
                    ControlToValidate="TxtFechaPubCeFin" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Máxima Registro Contratos Suministro
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaMaxSum" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaMaxSum" runat="server" ErrorMessage="Debe Ingresar la Fecha Máxima Registro Contratos Suministro"
                    ControlToValidate="TxtFechaMaxSum" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Hora Máxima Registro Contratos Suministro
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtHorMaxSum" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtHorMaxSum" runat="server" ErrorMessage="Debe Ingresar la Hora Máxima Registro Contratos Suministro"
                    ControlToValidate="TxtHorMaxSum" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevTxtHorMaxSum" ControlToValidate="TxtHorMaxSum"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Maxima Declaracion Operativa"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Máxima Registro Contratos Transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaMaxTra" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaMaxTra" runat="server" ErrorMessage="Debe Ingresar la Fecha Máxima Registro Contratos de transporte"
                    ControlToValidate="TxtFechaMaxTra" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Hora Máxima Registro Contratos Transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtHorMaxTra" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtHorMaxTra" runat="server" ErrorMessage="Debe Ingresar la Hora Máxima Registro Contratos de transporte"
                    ControlToValidate="TxtHorMaxTra" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevTxtHorMaxTra" ControlToValidate="TxtHorMaxTra"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Maxima Declaracion Operativa"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Contratos base Declaracion UVLP
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlContraos" runat="server">
                    <asp:ListItem Value="A" Text="Ambos"></asp:ListItem>
                    <asp:ListItem Value="P" Text="Primario"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Secundario"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">
                Porcentaje Máximo de energía demandada total
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtPorcMaxTot" runat="server" MaxLength="4" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtPorcMaxTot" runat="server" ErrorMessage="Debe Ingresar el porcentaje máximo de energía demandada"
                    ControlToValidate="TxtPorcMaxTot" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="fteTxtPorcMaxTot" runat="server" TargetControlID="TxtPorcMaxTot"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad Mínima de producto a subastar
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCntMonSub" runat="server" MaxLength="6" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtCntMonSub" runat="server" ErrorMessage="Debe Ingresar la cantidad mínima a subastar"
                    ControlToValidate="TxtCntMonSub" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="fteTxtCntMonSub" runat="server" TargetControlID="TxtCntMonSub"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">
                Porcentaje máximo de capacidad excedentaria
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtPorcMaxExc" runat="server" MaxLength="6" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtPorcMaxExc" runat="server" ErrorMessage="Debe Ingresar el porcentaje máximo de capacidad excedentaria"
                    ControlToValidate="TxtPorcMaxExc" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FteTxtPorcMaxExc" runat="server" TargetControlID="TxtPorcMaxExc"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--<tr>
            <td class="td1">
                Fecha Inicial Negociación Mercado Primario {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniNegMp" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaIniNegMp" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicial Negociación Mercado Primario"
                    ControlToValidate="TxtFechaIniNegMp" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:CalendarExtender ID="CeTxtFechaIniNegMp" runat="server" TargetControlID="TxtFechaIniNegMp"
                    Format="yyyy/MM/dd">
                </cc1:CalendarExtender>
            </td>
            <td class="td1">
                Fecha Final Negociación Mercado Primario {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFinNegMp" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaFinNegMp" runat="server" ErrorMessage="Debe Ingresar la Fecha Final Negociación Mercado Primario"
                    ControlToValidate="TxtFechaFinNegMp" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:CalendarExtender ID="CeTxtFechaFinNegMp" runat="server" TargetControlID="TxtFechaFinNegMp"
                    Format="yyyy/MM/dd">
                </cc1:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Inicial Entrega Subasta Mercado Primario {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniEntMp" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaIniEntMp" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicial Entrega Subasta Mercado Primario"
                    ControlToValidate="TxtFechaIniEntMp" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:CalendarExtender ID="CeTxtFechaIniEntMp" runat="server" TargetControlID="TxtFechaIniEntMp"
                    Format="yyyy/MM/dd">
                </cc1:CalendarExtender>
            </td>
        </tr>--%>
        <%--20170929 rq048-17--%>
        <tr>
            <td class="td1">
                Fecha de entrega Inicial {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaEntIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtFechaEntIni" runat="server" ErrorMessage="Debe Ingresar la Fecha de entrega inicial"
                    ControlToValidate="TxtFechaEntIni" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Fecha de entrega Final {YYYY/MM/DD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaEntFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtFechaEntFin" runat="server" ErrorMessage="Debe Ingresar la Fecha de entrega final"
                    ControlToValidate="TxtFechaEntFin" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Observación Cambio
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    </asp:Content>