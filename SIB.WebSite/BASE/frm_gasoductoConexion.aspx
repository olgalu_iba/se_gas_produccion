﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_gasoductoConexion.aspx.cs" Inherits="BASE_frm_gasoductoConexion" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <%--Captura--%>
            <div class="kt-portlet__body" runat="server" >
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Código Gasoducto Conexión</label>
                            <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBETxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Gasoducto de conexión</label>
                            <asp:DropDownList ID="ddlBusGasoducto" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Operador</label>
                            <asp:DropDownList ID="ddlBusOperador" runat="server" CssClass="form-control selectpicker">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True" PagerStyle-HorizontalAlign="Center"
                                OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged" Width="100%" CssClass="table-bordered">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_gasoducto" HeaderText="Código Gasoducto"
                                        ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="descripcion" HeaderText="Descripción" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tipo_conexion" HeaderText="Tipo de Conexión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_ini" HeaderText="Punto Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_departamento_ini" HeaderText="Departamento Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_ciudad_ini" HeaderText="Ciudad Inicial" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tipo_punto_fin" HeaderText="Tipo Punto Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_punto_fin" HeaderText="Punto Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="conexion_snt" HeaderText="Conexión SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_tipo_punto_fin" HeaderText="Tipo Punto Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_departamento_fin" HeaderText="Departamento Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_ciudad_fin" HeaderText="Ciudad Final" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="aplica_control_cmmp" HeaderText="Aplica control CMMP" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                </li>
                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>


    <%--Modals--%>
    <div class="modal fade" id="gasoductoConexion" tabindex="-1" role="dialog" aria-labelledby="mdlgasoductoConexionLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="gasoductoConexionInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlgasoductoConexionLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Código Gasoducto Conexión</label>
                                        <asp:TextBox ID="TxtCodigo" runat="server" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        <asp:Label ID="LblCodigo" runat="server" Visible="False" CssClass="form-control"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Descripción</label>
                                        <asp:TextBox ID="TxtDescripcion" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Operador</label>
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Tipo Gasoducto Conexión</label>
                                        <asp:DropDownList ID="ddlTipo" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlTipo_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="N">Conectado desde un campo nacional</asp:ListItem>
                                            <asp:ListItem Value="E">Procedencia extranjera</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Punto inicial</label>
                                        <asp:DropDownList ID="ddlPuntoIni" runat="server" CssClass="form-control selectpicker">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Departamento Punto Inicial</label>
                                        <asp:DropDownList ID="ddlDepartamentoIni" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlDepartamentoIni_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Ciudad Punto Inicial </label>
                                        <asp:DropDownList ID="ddlCiudadIni" runat="server" CssClass="form-control selectpicker">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Conectado al SNT</label>
                                        <asp:DropDownList ID="ddlConectado" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlConectado_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Tipo Punto Final</label>
                                        <asp:DropDownList ID="ddlTipoPuntoFin" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlTipoPuntoFin_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Punto Final</label>
                                        <asp:DropDownList ID="ddlPuntoFin" runat="server" CssClass="form-control selectpicker">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Departamento Punto Salida</label>
                                        <asp:DropDownList ID="ddlDepartamentoFin" runat="server" CssClass="form-control selectpicker" OnSelectedIndexChanged="ddlDepartamentoFin_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Ciudad Punto Salida</label>
                                        <asp:DropDownList ID="ddlCiudadFin" runat="server" CssClass="form-control selectpicker">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Valida CMMP</label>
                                        <asp:DropDownList ID="ddlCmmp" runat="server" CssClass="form-control selectpicker">
                                            <asp:ListItem Value="S">Si</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker">
                                            <asp:ListItem Value="A">Activo</asp:ListItem>
                                            <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="consulta" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="imbActualiza_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbCrearOpe" runat="server" class="btn btn-primary" Text="Agregar Operador" CausesValidation="false" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbCrearOpe_Click1" />
                            <asp:Button ID="imbCrearFuente" runat="server" class="btn btn-primary" Text="Agregar Fuente" CausesValidation="false" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbCrearFuente_Click1" />
                            <asp:Button ID="imbModFuente" runat="server" CssClass="btn btn-primary" Text="Modificar Fuente" OnClick="imbModFuente_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbSalir" runat="server" CssClass="btn btn-secondary" Text="Salir" OnClick="imbSalir_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>

                        <%--Detalle--%>
                        <div id="tblOperador" runat="server">
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5>Detalle de Operadores</h5>
                                </div>
                                <div class="panel-body">
                                    <div class="table table-responsive">
                                        <asp:DataGrid ID="dtgOperador" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" OnItemCommand="dtgOperador_EditCommand"
                                            Width="100%" CssClass="table-bordered" >
                                            <Columns>
                                                <asp:BoundColumn DataField="codigo_detalle_ope" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="codigo_gasoducto" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="nombre" HeaderText="Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <div class="dropdown dropdown-inline">
                                                            <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="flaticon-more-1"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                <!--begin::Nav-->
                                                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <ul class="kt-nav">
                                                                            <li class="kt-nav__item">
                                                                                <asp:LinkButton ID="lkbEliminar2" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                            </li>
                                                                        </ul>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                <!--end::Nav-->
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--Detalle--%>
                        <div id="tblDetalle" visible="false" runat="server">
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5>Detalle de Fuentes</h5>
                                </div>
                                <div class="panel-body">
                                    <div class="table table-responsive">
                                        <asp:DataGrid ID="dtgDetalle" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" OnItemCommand="dtgDetalle_EditCommand"
                                            Width="100%" CssClass="table-bordered" runat="server" >
                                            <Columns>
                                                <asp:BoundColumn DataField="codigo_detalle_fte" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="codigo_gasoducto" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="codigo_fuente" HeaderText="Código Fuente" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="desc_fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="tipo_fuente" HeaderText="Tipo Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="departamento_ini" HeaderText="Código Departamenteo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="nombre_departamento" HeaderText="Departamenteo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="municipio_ini" HeaderText="Código Ciudad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Ciudad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <div class="dropdown dropdown-inline">
                                                            <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="flaticon-more-1"></i>
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                                <!--begin::Nav-->
                                                                <asp:UpdatePanel runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <ul class="kt-nav">
                                                                            <li class="kt-nav__item">
                                                                                <asp:LinkButton ID="lkbModificar1" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                            </li>
                                                                            <li class="kt-nav__item">
                                                                                <asp:LinkButton ID="lkbEliminar1" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                                    </asp:LinkButton>
                                                                            </li>
                                                                        </ul>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                                <!--end::Nav-->
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
