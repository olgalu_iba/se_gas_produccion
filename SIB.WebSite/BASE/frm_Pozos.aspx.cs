﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_Pozos : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Punto SNT";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlTipoCampo, "m_tipo_campo", " estado ='A' order by descripcion", 0, 1);
            LlenarControles2(lConexion.gObjConexion, ddlDepartamento, "m_divipola", " estado = 'A' and codigo_ciudad ='0' order by nombre_ciudad", 1, 2); //20190306 rq013-19
            LlenarControles2(lConexion.gObjConexion, ddlCiudad, "m_divipola", "  1=2 order by nombre_ciudad", 3, 4); //20190306 rq013-19
            LlenarControles2(lConexion.gObjConexion, ddlCentro, "m_divipola", "  1=2 order by nombre_ciudad", 5, 6); //20190306 rq013-19
            LlenarControles1(lConexion.gObjConexion, ddlTramo, "m_tramo", " estado = 'A' order by descripcion", 0, 10); //20190306 rq013-19
            LlenarControles(lConexion.gObjConexion, ddlMercado, "m_mercado_relevante", " estado = 'A' order by descripcion", 0, 1); //20190306 rq013-19
            lConexion.Cerrar();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_pozo");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[22].Visible = (Boolean)permisos["UPDATE"]; //20160726 ajuste parametrizacion pozoz   //Campo nuevo Req. 007-17 Subasta Bimestral 20170209  //20190306 rq013-19 //20190404 rq018-19 fase II //20190621 rq039-19
        dtgMaestro.Columns[23].Visible = false; //20160726 ajuste parametrizacion pozoz //Campo nuevo Req. 007-17 Subasta Bimestral 20170209 //20190306 rq013-19 //20190404 rq018-19 fase II //20190621 rq039-19
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        tblDetalle.Visible = false; //20190306 rq013-19
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoPozo.Visible = false;
        LblCodigoPozo.Visible = true;
        LblCodigoPozo.Text = "Automático"; //20190306 rq013-19
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        tblDetalle.Visible = false; //20190306 rq013-19
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo", " codigo_pozo = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoPozo.Text = lLector["codigo_pozo"].ToString();
                        TxtCodigoPozo.Text = lLector["codigo_pozo"].ToString();
                        TxtDescripcion.Text = lLector["descripcion"].ToString();
                        DdlTipoCampo.SelectedValue = lLector["codigo_tipo_campo"].ToString();
                        DdlCampoPto.SelectedValue = lLector["ind_campo_pto"].ToString();
                        DdlCampoPto_SelectedIndexChanged(null, null); //Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                        ddlTrasf.SelectedValue = lLector["ind_trasferencia"].ToString(); //20160726 ajuste parametrizacion pozoz
                        ddlIntermedio.SelectedValue = lLector["ind_intermedio"].ToString(); //20160726 ajuste parametrizacion pozoz
                        ddlEstandar.SelectedValue = lLector["ind_estandar"].ToString(); //20160726 ajuste parametrizacion pozoz
                        ddlEntrada.SelectedValue = lLector["ind_entrada"].ToString(); //20160726 ajuste parametrizacion pozoz
                        ddlCampoDeclinacion.SelectedValue = lLector["campo_declinacion"].ToString(); //Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                        ddlPublicaMapa.SelectedValue = lLector["ind_pub_bec_mapa"].ToString(); //Campo nuevo Req. 079 Mapa Interactivo 20170314
                        //20190306 rq013-19
                        try
                        {
                            ddlDepartamento.SelectedValue = lLector["codigo_departamento"].ToString();
                            ddlDepartamento_SelectedIndexChanged(null, null);
                            ddlCiudad.SelectedValue = lLector["codigo_ciudad"].ToString();
                            ddlCiudad_SelectedIndexChanged(null, null);
                            ddlCentro.SelectedValue = lLector["codigo_centro"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text = "El centro poblado no existe o está inactivo";
                        }
                        try
                        {
                            ddlTramo.SelectedValue = lLector["codigo_tramo_poz"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tramo no existe o está inactivo<br>";
                        }
                        ddlTelemetria.SelectedValue = lLector["maneja_telemetria"].ToString();
                        //20190306 fin rq013-19
                        ddlPrecio.SelectedValue = lLector["formacion_precio"].ToString();//20190404 rq018-19 fase II
                        ddlImporta.SelectedValue = lLector["ind_importacion"].ToString();//20190621 rq039-19 
                        ddlRegasifica.SelectedValue = lLector["ind_regasificacion"].ToString();//20190621 rq039-19 
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoPozo.Visible = false;
                        LblCodigoPozo.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro porque está bloqueado. Codigo Pozo " + modificar.ToString(); //20190306 rq013-19

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }

    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    /// 20190306 rq013-19
    private void Detalle(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_pozo", " codigo_pozo = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        ddlPotencial.SelectedValue = lLector["pot_produccion"].ToString();
                        ddlFuente.SelectedValue = lLector["fuente_existente"].ToString();
                        ddlInterconexion.SelectedValue = lLector["interconexion"].ToString();
                        ddlEstadoProd.SelectedValue = lLector["estado_produccion"].ToString();
                        ddlTipoExp.SelectedValue = lLector["tipo_explotacion"].ToString();
                        ddlDestino.SelectedValue = lLector["destino_inyeccion"].ToString();
                        ddlEntrega.SelectedValue = lLector["ent_merc_relev"].ToString();
                        ddlEntrega_SelectedIndexChanged(null,null);
                        try
                        {
                            ddlMercado.SelectedValue = lLector["cod_mercado_relev"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        ddlRegion.SelectedValue = lLector["region"].ToString();
                        txtLongitud.Text = lLector["longitud"].ToString(); //2010621 rq039-19
                        txtLatitud.Text = lLector["latitud"].ToString(); //2010621 rq039-19
                        ddlPtdvfMp.SelectedValue = lLector["control_ptdvf_mp"].ToString(); //20211119 control ptdvf mp
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro porque está bloqueado. Código Fuente " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = false;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            tblDetalle.Visible = true;
            lblTitulo.Text = "Detalle Fuente";
        }
    }

    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_pozo", "@P_descripcion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "" };

        try
        {
            if (TxtBusCodigoPozo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigoPozo.Text.Trim();
            if (TxtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetPozo", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_pozo", "@P_descripcion", "@P_estado", "@P_accion", "@P_codigo_tipo_campo", "@P_ind_campo_pto", "@P_ind_trasferencia", "@P_ind_intermedio", "@P_ind_estandar", "@P_ind_entrada" ,  //20160726 ajuste parametrizacion pozoz
                                          "@P_campo_declinacion", // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          "@P_ind_pub_bec_mapa", // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                          "@P_codigo_departamento","@P_codigo_ciudad","@P_codigo_centro","@P_codigo_tramo_poz","@P_maneja_telemetria", // 20190306 rq013-19
                                          "@P_formacion_precio", //20190404 rq018-19 fase II
                                          "@P_ind_importacion","@P_ind_regasificacion" //20190621 rq039-19
    };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, //20160726 ajuste parametrizacion pozoz
                                          SqlDbType.VarChar, // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          SqlDbType.VarChar, // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                          SqlDbType.VarChar,SqlDbType.VarChar ,SqlDbType.VarChar ,SqlDbType.Int ,SqlDbType.VarChar,  // 20190306 rq013-19
                                          SqlDbType.VarChar, //20190404 rq018-19 fase II
                                          SqlDbType.VarChar ,SqlDbType.VarChar //20190621 rq039-19
                                        };
        string[] lValorParametros = { "0", "", "", "1", "0", "", "", "", "", "" , //20160726 ajuste parametrizacion pozoz
                                        ddlCampoDeclinacion.SelectedValue, // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                        ddlPublicaMapa.SelectedValue, // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                        "", "", "", "0", "",// 20190306 rq013-19
                                        "" , //20190404 rq018-19 fase II
                                        "","" //20190621 rq039-19
                                    };
        lblMensaje.Text = "";

        try
        {
            if (TxtDescripcion.Text.Trim() == "")
                lblMensaje.Text += " Debe digitar la Descripción del Pozo<br>"; //20190306 rq013-19
            else
                if (VerificarExistencia(" descripcion = '" + TxtDescripcion.Text.Trim() + "'"))
                lblMensaje.Text += " La Descripción del Pozo YA existe " + TxtDescripcion.Text + " <br>"; //20190306 rq013-19
            //20190306 rq013-19
            //20190607 rq036-19
            //if (ddlDepartamento.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar el departamento<br>";
            //if (ddlCiudad.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar la ciudad<br>";
            //if (ddlCentro.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar el departamento<br>";
            //if (ddlTramo.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar el tramo<br>";
            //20190607 fin rq036-19
            //20190306 fin rq013-19
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlEstado.SelectedValue;
                lValorParametros[4] = DdlTipoCampo.SelectedValue;
                lValorParametros[5] = DdlCampoPto.SelectedValue;
                lValorParametros[6] = ddlTrasf.SelectedValue;  //20160726 ajuste parametrizacion pozoz
                lValorParametros[7] = ddlIntermedio.SelectedValue; //20160726 ajuste parametrizacion pozoz
                lValorParametros[8] = ddlEstandar.SelectedValue; //20160726 ajuste parametrizacion pozoz
                lValorParametros[9] = ddlEntrada.SelectedValue; //20160726 ajuste parametrizacion pozoz
                lValorParametros[12] = ddlDepartamento.SelectedValue; // 20190306 rq013-19
                lValorParametros[13] = ddlCiudad.SelectedValue; // 20190306 rq013-19
                lValorParametros[14] = ddlCentro.SelectedValue; // 20190306 rq013-19
                lValorParametros[15] = ddlTramo.SelectedValue; // 20190306 rq013-19
                lValorParametros[16] = ddlTelemetria.SelectedValue; // 20190306 rq013-19
                lValorParametros[17] = ddlPrecio.SelectedValue;//20190404 rq018-19 fase II
                lValorParametros[18] = ddlImporta.SelectedValue;//20190621 rq039-19
                lValorParametros[19 ] = ddlRegasifica.SelectedValue;//20190621 rq039-19
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetPozo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un problema en la Creación del Pozo.!"; //20190306 rq013-19
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_pozo", "@P_descripcion", "@P_estado", "@P_accion", "@P_codigo_tipo_campo", "@P_ind_campo_pto", "@P_ind_trasferencia", "@P_ind_intermedio", "@P_ind_estandar", "@P_ind_entrada" ,  //20160726 ajuste parametrizacion pozoz
                                          "@P_campo_declinacion", // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          "@P_ind_pub_bec_mapa", // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                          "@P_codigo_departamento","@P_codigo_ciudad","@P_codigo_centro","@P_codigo_tramo_poz","@P_maneja_telemetria", // 20190306 rq013-19
                                          "@P_formacion_precio",//20190404 rq018-19 fase II
                                          "@P_ind_importacion","@P_ind_regasificacion" //20190621 rq039-19
    };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, //20160726 ajuste parametrizacion pozoz
                                          SqlDbType.VarChar,   // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          SqlDbType.VarChar,   // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                          SqlDbType.VarChar,SqlDbType.VarChar ,SqlDbType.VarChar ,SqlDbType.Int ,SqlDbType.VarChar,  // 20190306 rq013-19
                                          SqlDbType.VarChar,//20190404 rq018-19 fase II
                                          SqlDbType.VarChar,SqlDbType.VarChar//20190621 rq039-19
                                      };
        string[] lValorParametros = { "0", "", "", "2", "0", "", "", "", "", "" , //20160726 ajuste parametrizacion pozoz
                                        ddlCampoDeclinacion.SelectedValue,    // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                        ddlPublicaMapa.SelectedValue,    // Campo nuevo Req. 079 Mapa Interactivo 20170314
                                        "", "", "", "0", "",// 20190306 rq013-19
                                        "",// 20190306 rq013-19
                                        "","" //20190621 rq039-19
                                    };
        lblMensaje.Text = "";
        try
        {
            if (VerificarExistencia(" descripcion = '" + this.TxtDescripcion.Text.Trim() + "' And codigo_pozo != " + LblCodigoPozo.Text))
                lblMensaje.Text += " La Descripción del pozo YA existe " + TxtDescripcion.Text + " <br>"; //20190306 rq013-19
            //20190306 rq013-19
            //20190607 rq036-19
            //if (ddlDepartamento.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar el departamento<br>";
            //if (ddlCiudad.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar la ciudad<br>";
            //if (ddlCentro.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar el centro poblado<br>";
            //if (ddlTramo.SelectedValue == "0")
            //    lblMensaje.Text += "Debe seleccionar el tramo<br>";
            //20190607 fin rq036-19
            //20190306 fin rq013-19
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoPozo.Text;
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlEstado.SelectedValue;
                lValorParametros[4] = DdlTipoCampo.SelectedValue;
                lValorParametros[5] = DdlCampoPto.SelectedValue;
                lValorParametros[6] = ddlTrasf.SelectedValue;  //20160726 ajuste parametrizacion pozoz
                lValorParametros[7] = ddlIntermedio.SelectedValue; //20160726 ajuste parametrizacion pozoz
                lValorParametros[8] = ddlEstandar.SelectedValue; //20160726 ajuste parametrizacion pozoz
                lValorParametros[9] = ddlEntrada.SelectedValue; //20160726 ajuste parametrizacion pozoz
                lValorParametros[12] = ddlDepartamento.SelectedValue; // 20190306 rq013-19
                lValorParametros[13] = ddlCiudad.SelectedValue; // 20190306 rq013-19
                lValorParametros[14] = ddlCentro.SelectedValue; // 20190306 rq013-19
                lValorParametros[15] = ddlTramo.SelectedValue; // 20190306 rq013-19
                lValorParametros[16] = ddlTelemetria.SelectedValue; // 20190306 rq013-19
                lValorParametros[17] = ddlPrecio.SelectedValue; // 20190306 rq013-19
                lValorParametros[18] = ddlImporta.SelectedValue;//20190621 rq039-19
                lValorParametros[19] = ddlRegasifica.SelectedValue;//20190621 rq039-19
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetPozo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un problema en la Actualización del Pozo.!"; //20190306 rq013-19
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoPozo.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoPozo.Text);
            lblMensaje.Text = ex.Message;
        }
    }

    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20190603 rq013-19
    protected void imbDetalle_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_pozo", "@P_pot_produccion", "@P_fuente_existente", "@P_interconexion", "@P_estado_produccion", "@P_tipo_explotacion", "@P_destino_inyeccion", "@P_ent_merc_relev", "@P_cod_mercado_relev", "@P_region" ,
                        "@P_longitud","@P_latitud", //20190621 rq039-19
                        "@P_control_ptdvf_mp" //20211119 control ptdvf mp
                        };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar,
                    SqlDbType.VarChar,SqlDbType.VarChar,//20190621 rq039-19
                    SqlDbType.VarChar //20211119 control ptvf mp
        };
        string[] lValorParametros = { "0", "", "", "", "", "", "", "", "0", "" ,
                    "","",//20190621 rq039-19
                    ddlPtdvfMp.SelectedValue //20211119 control ptdvf mp
        };
        lblMensaje.Text = "";
        try
        {
            if (ddlPotencial.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el potencial de producción.<br>";
            if (ddlFuente.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el tipo de existencia de la fuente.<br>";
            if (ddlInterconexion.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar la interconexión.<br>";
            if (ddlEstadoProd.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el estado de producción.<br>";
            if (ddlTipoExp.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el tipo de explotación.<br>";
            if (ddlDestino.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el destino de inyección.<br>";
            if (ddlMercado.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar si entrega a mercado relevante.<br>";
            if (ddlEntrega.SelectedValue == "Si" && ddlMercado.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el mercado relevante.<br>";
            if (ddlRegion.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar la región.<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = lblFuente.Text;
                lValorParametros[1] = ddlPotencial.SelectedValue;
                lValorParametros[2] = ddlFuente.SelectedValue;
                lValorParametros[3] = ddlInterconexion.SelectedValue;
                lValorParametros[4] = ddlEstadoProd.SelectedValue;
                lValorParametros[5] = ddlTipoExp.SelectedValue;
                lValorParametros[6] = ddlDestino.SelectedValue;
                lValorParametros[7] = ddlEntrega.SelectedValue;
                lValorParametros[8] = ddlMercado.SelectedValue;
                lValorParametros[9] = ddlRegion.SelectedValue;
                lValorParametros[10] = txtLongitud.Text;//20190621 rq039-19
                lValorParametros[11] = txtLatitud.Text;//20190621 rq039-19
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetPozoDatFte", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un problema en la Actualización de la Fuente.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", lblFuente.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", lblFuente.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoPozo.Text != "")
            manejo_bloqueo("E", LblCodigoPozo.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
        //20190306 rq013-19
        if (((LinkButton)e.CommandSource).Text == "Detalle")
        {
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[3].Text == "S")
            {
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[7].Text == "S")
                {
                    trMercado.Visible = true;
                    trMercado1.Visible = true;
                }
                else
                {
                    trMercado.Visible = false;
                    trMercado1.Visible = false;
                    ddlEntrega.SelectedValue = "No Aplica";
                    ddlMercado.SelectedValue = "0";
                }
                lblFuente.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                lblDescripcion.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                Detalle(lCodigoRegistro);
            }
            else
                lblMensaje.Text = "El detalle solo se define para Fuentes";
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_pozo", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20190607 rq036-19
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        ListItem lItem2 = new ListItem();
        lItem2.Value = "-1";
        lItem2.Text = "No Aplica";
        lDdl.Items.Add(lItem2);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// 20190306 rq013-19
    protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", null, null, null);
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);
        //20190607 rq36-19
        ListItem lItem2 = new ListItem();
        lItem2.Value = "-1";
        lItem2.Text = "No Aplica";
        lDdl.Items.Add(lItem2);
        //20190607 fin rq36-19
        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector["desc_tramo"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_pozo' and llave_registro='codigo_pozo=" + lscodigo_registro + "'"; //20211119 control ptdvf mo
        string lsCondicion1 = "codigo_pozo=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_pozo";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_pozo", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigoPozo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigoPozo.Text.Trim();
                lsParametros += " Código Punto SNT: " + TxtBusCodigoPozo.Text;

            }
            if (TxtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
                lsParametros += " - Descripción: " + TxtBusDescripcion.Text; //20190306 rq013-19
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetPozo&nombreParametros=@P_codigo_pozo*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_pozo*descripcion*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado Puntos SNT&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!"; //20190306 rq013-19
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// /// //20190621 rq039-19
    //protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    try
    //    {
    //        string lsCondicion = " codigo_pozo <> '0'";
    //        Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_pozo&procedimiento=pa_ValidarExistencia&columnas=codigo_pozo*descripcion*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
    //    }
    //    catch (Exception ex)
    //    {
    //        lblMensaje.Text = "No se Pudo Generar el Informe.!"; //20190306 rq013-19
    //    }

    //}
    /// <summary>
    /// Metodo Nuevo para habilitar la captura del Campo en Declinación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// //Campo nuevo Req. 007-17 Subasta Bimestral 20170209
    protected void DdlCampoPto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdlCampoPto.SelectedValue == "C")
        {
            TrCampo.Visible = true;
            TrCampo1.Visible = true; // Campo Req. 079 Mapa Interactivo 20170314
        }
        else
        {
            TrCampo.Visible = false;
            ddlCampoDeclinacion.SelectedValue = "N";
            TrCampo1.Visible = false; // Campo Req. 079 Mapa Interactivo 20170314
            ddlPublicaMapa.SelectedValue = "N"; // Campo Req. 079 Mapa Interactivo 20170314
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20190306 rq013-19
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlCiudad.Items.Clear();
        LlenarControles2(lConexion.gObjConexion, ddlCiudad, "m_divipola", " estado = 'A' and codigo_departamento =" + ddlDepartamento.SelectedValue + " and codigo_ciudad <>'0' and codigo_centro='0' order by nombre_ciudad", 3, 4); //20190607 rq036-19
        lConexion.Cerrar();

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20190306 rq013-19
    protected void ddlCiudad_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlCentro.Items.Clear();
        LlenarControles2(lConexion.gObjConexion, ddlCentro, "m_divipola", " estado = 'A' and codigo_departamento ='" + ddlDepartamento.SelectedValue + "' and codigo_ciudad='" + ddlCiudad.SelectedValue + "'  and codigo_centro<>'0' order by nombre_centro", 5, 6); //20190607 rq036-19
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20190306 rq013-19
    protected void ddlEntrega_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEntrega.SelectedValue == "Si")
            trMercado1.Visible = true;
        else
        {
            trMercado1.Visible = false;
            ddlMercado.SelectedValue = "0";
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20211119 control ptdvf 
    protected void imbSalirD_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (lblFuente.Text != "")
            manejo_bloqueo("E", lblFuente.Text);
        Listar();
    }

}