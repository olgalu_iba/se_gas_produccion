﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_Festivo.aspx.cs"
    Inherits="BASE_frm_Festivo" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_Festivo.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_Festivo.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_Festivo.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Fecha
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:Label ID="LblFecha" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RfvFecha" runat="server" ErrorMessage="La Fecha es Requerida"
                    ControlToValidate="TxtFecha" Display="None" ValidationGroup="captura"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Día Hábil
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlDiaHabil" runat="server">
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">A</asp:ListItem>
                    <asp:ListItem Value="I">I</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="captura" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="captura" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ValidationSummary ID="VSFestivo" runat="server" ValidationGroup="captura" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblAutomatico">
        <tr>
            <td class="td1">
                Año
            </td>
            <td>
                <asp:TextBox ID="TxtAno" runat="server" MaxLength="4"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtAno" runat="server" ErrorMessage="Debe Ingresar el Año "
                    ControlToValidate="TxtAno"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtAno" runat="server" ControlToValidate="TxtAno" Type="Integer"
                    ErrorMessage="El Año debe ser Numérico" Operator="DataTypeCheck" ValidationGroup="CapturaAutomatica"> * </asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbAutomatico" runat="server" ImageUrl="~/Images/Crear.png"
                    OnClick="imbAutomatico_Click" ValidationGroup="CapturaAutomatica" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ValidationSummary ID="VsAutomatico" runat="server" ValidationGroup="CapturaAutomatica" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Año
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusAno" runat="server">
                    <asp:ListItem Value="=">Igual</asp:ListItem>
                    <asp:ListItem Value="&gt;">Mayor que</asp:ListItem>
                    <asp:ListItem Value="&lt;">Menor que</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td2">
                <asp:TextBox ID="txtBusAno" runat="server" autocomplete="off"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Debe Ingresar el Año "
                    ControlToValidate="TxtAno"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvtxtBusAno" runat="server" ControlToValidate="txtBusAno"
                    Type="Integer" ErrorMessage="El Año debe ser Numérico" Operator="DataTypeCheck"
                    ValidationGroup="Busqueda"> * </asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Día Hábil
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusDiaHabil" runat="server">
                    <asp:ListItem Value="Like">Contiene</asp:ListItem>
                    <asp:ListItem Value="=">Igual</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBuscaDiaHabil" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:LinkButton ID="lkbConsultar" runat="server" OnClick="lkbConsultar_Click" ValidationGroup="Busqueda">Buscar</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Busqueda" />
            </td>
        </tr>
    </table>
    <br />
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="85%">
        <tr>
            <td colspan="2" align="center">
                <div>
                    <asp:DataGrid ID="dtgFestivo" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgFestivo_EditCommand" OnPageIndexChanged="dtgFestivo_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="15">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="Fecha" HeaderText="Fecha" ItemStyle-HorizontalAlign="Left"
                                DataFormatString="{0: yyyy/MM/dd}" ItemStyle-Width="150px">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ano" HeaderText="Año" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="mes" HeaderText="MEs" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="dia" HeaderText="Día" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ind_dia_habil" HeaderText="Día Hábil" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>