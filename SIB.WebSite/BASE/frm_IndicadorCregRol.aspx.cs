﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_IndicadorCregRol : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Indicadores Creg por Rol";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlIndicadorCreg, "m_indicadores_creg", " 1=1 order by codigo_indicador", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusIndicador, "m_indicadores_creg", " 1=1 order by codigo_indicador", 0, 1);
            lConexion.Cerrar();

            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_indicador_rol");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[7].Visible = (Boolean)permisos["UPDATE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoIndicadorRol.Visible = false;
        LblCodigoIndicadorRol.Visible = true;
        LblCodigoIndicadorRol.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    ddlIndicadorCreg.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlIndicadorCreg, "m_indicadores_creg", " 1=1 order by descripcion", 0, 1);
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_indicador_rol", " codigo_indicador_rol = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoIndicadorRol.Text = lLector["codigo_indicador_rol"].ToString();
                        TxtCodigoIndicadorRol.Text = lLector["codigo_indicador_rol"].ToString();
                        try
                        {
                            ddlIndicadorCreg.SelectedValue = lLector["codigo_indicador"].ToString();
                            ddlIndicadorCreg_SelectedIndexChanged(null, null);
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El Indicador del registro no existe o esta inactivo";
                        }
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        try
                        {
                            ddlVista.SelectedValue = lLector["vista_desagregacion"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "La vista del indicador no existe o no está activa";  //20170705 rq025-17 indicadores MP fase III
                        }
                        ddlSistema.SelectedValue = lLector["sistema"].ToString();
                        ddlSistema_SelectedIndexChanged(null, null);
                        try
                        {
                            ddlRol.SelectedValue = lLector["rol"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El Rol del Indicador Rol No existe o está inactivo";
                        }
                        ddlIndicadorCreg.Enabled = false;
                        ddlVista.Enabled = false;
                        ddlSistema.Enabled = false;
                        ddlRol.Enabled = false;
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoIndicadorRol.Visible = false;
                        LblCodigoIndicadorRol.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Indicador Rol " + modificar.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_indicador_rol", "@P_codigo_indicador", "@P_sistema" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "0" };

        try
        {
            if (TxtBusIndicadorRol.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusIndicadorRol.Text.Trim();
            if (ddlBusIndicador.SelectedValue != "0")
                lValorParametros[1] = ddlBusIndicador.SelectedValue;
            if (ddlBusSistema.SelectedValue != "0")
                lValorParametros[2] = ddlBusSistema.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetIndicadorCregRol", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_indicador_rol", "@P_codigo_indicador", "@P_vista_desagregacion", "@P_sistema", "@P_rol", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "", "", "0", "", "1" };
        lblMensaje.Text = "";

        try
        {
            if (ddlIndicadorCreg.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Indicador Creg. <br>";
            if (ddlVista.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar la Vista Desagregación. <br>";
            if (ddlSistema.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Sistema. <br>";
            if (ddlSistema.SelectedValue == "S")
            {
                if (ddlRol.SelectedValue == "0")
                    lblMensaje.Text += " Debe Seleccionar el Rol. <br>";
            }
            if (VerificarExistencia(" vista_desagregacion = '" + ddlVista.SelectedValue + "' And codigo_indicador = '" + ddlIndicadorCreg.SelectedValue + "' And sistema = '" + ddlSistema.SelectedValue + "' And rol = '" + ddlRol.SelectedValue + "' "))
                lblMensaje.Text += " Ya Existe un Registro para el Indicador, Vista, Sistema y Rol <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = ddlIndicadorCreg.SelectedValue.Trim();
                lValorParametros[2] = ddlVista.SelectedValue.Trim();
                lValorParametros[3] = ddlSistema.SelectedValue.Trim();
                lValorParametros[4] = ddlRol.SelectedValue;
                lValorParametros[5] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetIndicadorCregRol", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación del Indicador Creg Rol.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_indicador_rol", "@P_codigo_indicador", "@P_vista_desagregacion", "@P_sistema", "@P_rol", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "", "", "0", "", "2" };
        lblMensaje.Text = "";
        try
        {
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoIndicadorRol.Text;
                lValorParametros[1] = ddlIndicadorCreg.SelectedValue.Trim();
                lValorParametros[2] = ddlVista.SelectedValue.Trim();
                lValorParametros[3] = ddlSistema.SelectedValue.Trim();
                lValorParametros[4] = ddlRol.SelectedValue;
                lValorParametros[5] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetIndicadorCregRol", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del Indicador Creg Rol.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoIndicadorRol.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoIndicadorRol.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoIndicadorRol.Text != "")
            manejo_bloqueo("E", LblCodigoIndicadorRol.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_indicador_rol", lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            if (lsTabla == "m_operador")
            {
                lItem1.Value = lLector["codigo_operador"].ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            else
            {
                if (lsTabla == "t_rueda")
                {
                    lItem1.Value = lLector["numero_rueda"].ToString();
                    lItem1.Text = lLector["numero_rueda"].ToString() + "-" + lLector["descripcion"].ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
            }
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_indicador_rol' and llave_registro='codigo_indicador_rol=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_indicador_rol=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_indicador_rol";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_indicador_rol", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusIndicadorRol.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusIndicadorRol.Text.Trim();
                lsParametros += " Codigo Indicador Rol : " + TxtBusIndicadorRol.Text;

            }
            if (ddlBusIndicador.SelectedValue != "S")
            {
                lValorParametros[1] = ddlBusIndicador.SelectedValue;
                lsParametros += " - Indicador : " + ddlBusIndicador.SelectedItem.ToString();
            }
            if (ddlBusSistema.SelectedValue != "0")
            {
                lValorParametros[2] = ddlBusSistema.SelectedValue.Trim();
                lsParametros += " - Sistema: " + ddlBusSistema.SelectedItem.ToString();
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetIndicadorCregRol&nombreParametros=@P_codigo_indicador_rol*@P_codigo_indicador*@P_sistema&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_operador_subasta&titulo_informe=Listado de Indicadores Creg por Rol&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_indicador_rol <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_indicador_rol&procedimiento=pa_ValidarExistencia&columnas=codigo_indicador_rol*codigo_indicador*vista_desagregacion*sistema*rol*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlIndicadorCreg_SelectedIndexChanged(object sender, EventArgs e)
    {
        //20170705 rq025 indicar creg fase III
        ddlVista.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlVista, "m_indicador_vista", " codigo_indicador = " + ddlIndicadorCreg.SelectedValue + " and estado ='A'", 2, 3);
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSistema_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ddlRol.Items.Clear();
            if (ddlSistema.SelectedValue == "B")
            {
                ListItem lItem3 = new ListItem();
                lItem3.Value = "0";
                lItem3.Text = "Todos";
                ddlRol.Items.Add(lItem3);
            }
            else
            {
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlRol, "a_grupo_usuario", " estado = 'A' order by descripcion", 0, 1);
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {

        }
    }
}