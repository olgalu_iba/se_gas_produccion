﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_Rueda.aspx.cs"
    Inherits="BASE_frm_Rueda" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_Rueda.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_Rueda.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_Rueda.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=3">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br /><%--20210824--%>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlSubasta" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubasta_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:HiddenField ID="hdfNumeroRueda" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="td1">Tipo de Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoRueda" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trFechaRue" runat="server">
            <td class="td1" colspan="1">Fecha Rueda
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFechaRueda" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaRueda" runat="server" ErrorMessage="Debe Ingresar la Fecha de la Rueda"
                    ControlToValidate="TxtFechaRueda" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <%--<tr runat="server" id="TrHora">
            <td class="td1" colspan="1">
                Hora de Apertura
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraApertura" runat="server" MaxLength="5" Width="100px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtHoraApertura" runat="server" ErrorMessage="Debe Ingresar la Hora de apertura"
                    ControlToValidate="TxtHoraApertura" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevTxtHoraApertura" ControlToValidate="TxtHoraApertura"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de apertura de la rueda"> * </asp:RegularExpressionValidator>
            </td>
        </tr>--%>
        <tr runat="server" id="TrSIVLP" visible="false">
            <td class="td1" colspan="1">Año y Mes de Consumo
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtAnoCons" runat="server" MaxLength="5" Width="100px"></asp:TextBox>-
                <cc1:FilteredTextBoxExtender ID="FTBETxtAnoCons" runat="server" TargetControlID="TxtAnoCons"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
                <asp:TextBox ID="TxtMesCons" runat="server" MaxLength="5" Width="100px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtMesCons" runat="server" TargetControlID="TxtMesCons"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--Campo nuevo Subasta Transporte Req. 005-2021 20210121--%>
        <tr runat="server" id="TrTrans" visible="false">
            <td class="td1" colspan="1">Año y Trimestre de Negociación
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtAñoNeg" runat="server" MaxLength="5" Width="100px"></asp:TextBox>-
                <cc1:FilteredTextBoxExtender ID="FeTxtAñoNeg" runat="server" TargetControlID="TxtAñoNeg"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
                <asp:DropDownList ID="ddlTrimestre" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblModifica" visible="false">
        <tr>
            <td class="td1">Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlActSubasta" runat="server">
                </asp:DropDownList>
            </td>
            <td class="td1">Tipo de Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlActTipoRueda" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">Fecha Rueda
            </td>
            <td class="td2" colspan="1">
                <asp:Label ID="lblFechaRueda" runat="server"></asp:Label>
            </td>
            <td class="td1" colspan="1">Estado
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlEstado" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trHorasMod" runat="server" visible="false">
            <td class="td1" colspan="1">Hora Apertura
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraAperturaAct" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraAperturaAct" ControlToValidate="TxtHoraAperturaAct"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de apertura de la rueda"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Cierre
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraCierre" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraCierre" ControlToValidate="TxtHoraCierre"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de cierre de la rueda"> * </asp:RegularExpressionValidator>
            </td>
        </tr>

        <tr runat="server" id="TrSuvcp01">
            <td class="td1" colspan="1">Hora Declaración de información
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniOfVenSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniOfVenSuvcp" ControlToValidate="TxtHorIniOfVenSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial de declaracion de informacion"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="TxtHorFinOfVenSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtHorFinOfVenSuvcp" ControlToValidate="TxtHorFinOfVenSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora final de declaracion de informacion"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Declaración precio reserva y cantidad no disponible
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniContVenSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniContVenSuvcp" ControlToValidate="TxtHorIniContVenSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial de delcaracion de precio de reserva"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="TxtHorFinContVenSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtHorFinContVenSuvcp" ControlToValidate="TxtHorFinContVenSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora final de declaracion de precio de reserv"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSuvcp02">
            <td class="td1" colspan="1">Hora Publicación posturas de Venta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniPubVenSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniPubVenSuvcp" ControlToValidate="TxtHorIniPubVenSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Publica Venta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="TxtHorFinPubVenSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtHorFinPubVenSuvcp" ControlToValidate="TxtHorFinPubVenSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora final Publica Venta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Ingreso Oferta Compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniOfComSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniOfComSuvcp" ControlToValidate="TxtHorIniOfComSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial de ingreso Oferta Compra"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="TxtHorFinOfComSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtHorFinOfComSuvcp" ControlToValidate="TxtHorFinOfComSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora final de ingreso Oferta Compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSuvcp03">
            <td class="td1" colspan="1">Hora Calce
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniCalceSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniCalceSuvcp" ControlToValidate="TxtHorIniCalceSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Calce"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="TxtHorFinCalceSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtHorFinCalceSuvcp" ControlToValidate="TxtHorFinCalceSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora Final Calce"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Modificación Contratos
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniModContSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniModContSuvcp" ControlToValidate="TxtHorIniModContSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Modificacion de Contratos"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="TxtHorFinModContSuvcp" runat="server" Width="40" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtHorFinModContSuvcp" ControlToValidate="TxtHorFinModContSuvcp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la Hora final Modificacion de Contratos"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSspci01">
            <td class="td1" colspan="1">Fecha Máxima Ingreso Participación Vendedores
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFecMaxParVendSci" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td1" colspan="1">Fecha Máxima Ingreso Declaración de Vendedores
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFecMaxDecVendSci" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="TrSspci02">
            <td class="td1" colspan="1">Fecha Máxima Revelación de Información
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFecMaxRevInfSci" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>

            </td>
            <td class="td1" colspan="1">Fecha Máxima Ingreso Declaración de Compradores
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFecMaxDecCompdSci" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="TrSspci03">
            <td class="td1" colspan="1">Horas Para Modificación de Declaración de Venta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNoHoraModVenSci" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtNoHoraModVenSci" runat="server" TargetControlID="TxtNoHoraModVenSci"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">Horas Para Modificación de Declaración de Compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNoHoraModComSci" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtNoHoraModComSci" runat="server" TargetControlID="TxtNoHoraModComSci"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSnd01" visible="false">
            <td class="td1" colspan="1">Hora Inicial Ingreso Posturas Negociación Directa
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorInPosNd" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorInPosNd" ControlToValidate="TxtHorInPosNd"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Ingreso Posturas Negociacion Directa"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Final Ingreso Posturas Negociación Directa
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFiPosNd" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFiPosNd" ControlToValidate="TxtHorFiPosNd"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Ingreso Posturas Negociación Directa"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci02" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub01" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniPubVCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniPubVCi" ControlToValidate="TxtHorIniPubVCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Publicación Venta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub02" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinPubVCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinPubVCi" ControlToValidate="TxtHorFinPubVCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Publicación Venta"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci03" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub03" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniPubCantDCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniPubCantDCi" ControlToValidate="TxtHorIniPubCantDCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Publicación Cantidad Dispinible Venta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub04" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinPubCantDCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinPubCantDCi" ControlToValidate="TxtHorFinPubCantDCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Publicación Cantidad Dispinible Venta Contratos"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci04" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub05" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniRecCantCCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniRecCantCCi" ControlToValidate="TxtHorIniRecCantCCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Recibo Cantidad Compra"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub06" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinRecCantCCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinRecCantCCi" ControlToValidate="TxtHorFinRecCantCCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Recibo Cantidad Compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci05" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub07" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniNegociaCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniNegociaCi" ControlToValidate="TxtHorIniNegociaCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Negociación"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub08" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinNegociaCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinNegociaCi" ControlToValidate="TxtHorFinNegociaCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Negociación"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci06" visible="false">
            <td class="td1" colspan="1">Minutos para modificar ofertas de venta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtMinModV" runat="server" MaxLength="5"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="cvTxtMinModV" runat="server" TargetControlID="TxtMinModV"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">Minutos para modificar ofertas de compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtMinModC" runat="server" MaxLength="5"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="cvTxtMinModC" runat="server" TargetControlID="TxtMinModC"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSci07" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub10" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNoMesIniPer" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtNoMesIniPer" runat="server" TargetControlID="TxtNoMesIniPer"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--Campos Nuevos Req. 007-17 Subasta Bimestral 20170209--%>
        <tr runat="server" id="TrSci08" visible="false">
            <td class="td1" colspan="1">Fecha Para Declaración de Información Subasta Bimestral
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFecDeclaSubBimes" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td1" colspan="1">Fecha Para Publicacíón de Información Subasta Bimestral
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFecPubliSubBimes" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>

            </td>
        </tr>
        <tr runat="server" id="TrSci09" visible="false">
            <td class="td1" colspan="1">Hora Inicial Modificación Declaración Subasta Bimestral
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniModDecSb" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraIniModDecSb" ControlToValidate="TxtHoraIniModDecSb"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Modificación Declaración Subasta Bimestral"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Final Modificación Declaración Subasta Bimestral
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinModDecSb" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinModDecSb" ControlToValidate="TxtHoraFinModDecSb"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Modificación Declaración Subasta Bimestral"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--hasta Aqui--%>

        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP01" visible="false">
            <td class="td1" colspan="1">Hora Inicio Fase de Publicación
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniPubUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraIniPubUvlp" ControlToValidate="TxtHoraIniPubUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial de publicación"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Fin Fase de Publicación
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinPubUvlp" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinPubUvlp" ControlToValidate="TxtHoraFinPubUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final de publicación"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP02" visible="false">
            <td class="td1" colspan="1">Hora Inicio Fase de Ingreso de Posturas de Compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniCompUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RfvTxtHoraIniCompUvlp" ControlToValidate="TxtHoraIniPubUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial de ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Fin Fase de Ingreso de Posturas de Compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinCompUvlp" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinCompUvlp" ControlToValidate="TxtHoraFinCompUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final de ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP03" visible="false">
            <td class="td1" colspan="1">Hora Inicio Fase de Calce
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniCalceUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraIniCalceUvlp" ControlToValidate="TxtHoraIniCalceUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de inicio de calce"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Fin Fase de Calce
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinCalceUvlp" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinCalceUvlp" ControlToValidate="TxtHoraFinCalceUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final de calce"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP04" visible="false">
            <td class="td1" colspan="1">Tipo de negociación
            </td>
            <td class="td2" colspan="3">
                <asp:DropDownList ID="ddlTpoNEgUvlp" runat="server" Enabled="false">
                    <asp:ListItem Value="P">Principal</asp:ListItem>
                    <asp:ListItem Value="D">Derivada</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--Campos Subasta Transporte Req. 005-2021 20210120--%>
        <tr runat="server" id="TrStra01" visible="false">
            <td class="td1">Tipo Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoRueTra11" runat="server" Enabled="false">
                    <asp:ListItem Value="0">Seleccione</asp:ListItem>
                    <asp:ListItem Value="1">Beneficiarios Por Rutas</asp:ListItem>
                    <asp:ListItem Value="2">Beneficiarios Por Tramos</asp:ListItem>
                    <asp:ListItem Value="3">No Beneficiarios Por rutas</asp:ListItem>
                    <asp:ListItem Value="4">No beneficiarios por Tramos</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="TrStra02" visible="false">
            <td class="td1" colspan="1">Horas Para Publicación de oferta 
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniPubOfr11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniPubOfr11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Publicación de oferta"
                    ControlToValidate="txtHoraIniPubOfr11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniPubOfr11" ControlToValidate="txtHoraIniPubOfr11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Publicación de oferta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinPubOfr11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinPubOfr11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Publicación de oferta"
                    ControlToValidate="txtHoraFinPubOfr11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinPubOfr11" ControlToValidate="txtHoraFinPubOfr11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Publicación de oferta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Horas Para Declaración posturas de compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniDecPosCom11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniDecPosCom11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Declaración posturas de compra"
                    ControlToValidate="txtHoraIniDecPosCom11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniDecPosCom11" ControlToValidate="txtHoraIniDecPosCom11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Declaración posturas de compra"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinDecPosCom11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinDecPosCom11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Declaración posturas de compra"
                    ControlToValidate="txtHoraFinDecPosCom11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinDecPosCom11" ControlToValidate="txtHoraFinDecPosCom11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Declaración posturas de compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrStra03" visible="false">
            <td class="td1" colspan="1">Horas Para Desarrollo subasta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniDesSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniDesSub11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Desarrollo subasta"
                    ControlToValidate="txtHoraIniDesSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniDesSub11" ControlToValidate="txtHoraIniDesSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Desarrollo subasta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinDesSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinDesSub11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Desarrollo subasta"
                    ControlToValidate="txtHoraFinDesSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinDesSub11" ControlToValidate="txtHoraFinDesSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Desarrollo subasta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Horas Para Rechazo de adjudicaciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniRecAdj11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniRecAdj11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Rechazo de adjudicaciones"
                    ControlToValidate="txtHoraIniRecAdj11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniRecAdj11" ControlToValidate="txtHoraIniRecAdj11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Rechazo de adjudicaciones"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinRecAdj11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinRecAdj11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Rechazo de adjudicaciones"
                    ControlToValidate="txtHoraFinRecAdj11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinRecAdj11" ControlToValidate="txtHoraFinRecAdj11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Rechazo de adjudicaciones"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrStra04" visible="false">
            <td class="td1" colspan="1">Horas Para Publicación resultados
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniPubRes11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniPubRes11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Publicación resultados"
                    ControlToValidate="txtHoraIniPubRes11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniPubRes11" ControlToValidate="txtHoraIniPubRes11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Publicación resultados"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinPubRes11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinPubRes11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Publicación resultados"
                    ControlToValidate="txtHoraFinPubRes11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinPubRes11" ControlToValidate="txtHoraFinPubRes11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Publicación resultados"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Horas Para Finalización de la subasta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniFinSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniFinSub11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Finalización de la subasta"
                    ControlToValidate="txtHoraIniFinSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniFinSub11" ControlToValidate="txtHoraIniFinSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Finalización de la subasta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinFinSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinFinSub11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Finalización de la subasta"
                    ControlToValidate="txtHoraFinFinSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinFinSub11" ControlToValidate="txtHoraFinFinSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Finalización de la subasta"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--Hsta Aqui--%>
        <tr id="tr1" runat="server">
            <td class="td1" colspan="1">Fecha Próxima Apertura
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFechaProx" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtFechaProx" runat="server" ErrorMessage="Debe Ingresar la Fecha próxima de apertura"
                    ControlToValidate="TxtFechaProx" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="ImgbSalir" runat="server" ImageUrl="~/Images/salir.png" CausesValidation="false"
                    Height="40" OnClick="ImgbSalir_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Numero Rueda
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusNumeroRueda" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtBusNumeroRueda" runat="server" TargetControlID="TxtBusNumeroRueda"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Rueda
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusFechaRueda" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Año Rueda
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusANoRueda" runat="server" autocomplete="off" MaxLength="4"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtBusANoRueda" runat="server" TargetControlID="TxtBusANoRueda"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="ano_rueda" HeaderText="Ano Rueda" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numero_rueda" HeaderText="Número Rueda" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_rueda" HeaderText="Tipo Rueda" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_apertura" HeaderText="Hora Apertura" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_cierre" HeaderText="Hora Cierre" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion_estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                            <asp:BoundColumn DataField="estado" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_subasta" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
