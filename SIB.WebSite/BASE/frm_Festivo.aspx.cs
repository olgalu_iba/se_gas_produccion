﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_Festivo : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Calendario";
    m_festivo loFestivoBuscar = new m_festivo();
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    string gsTabla = "m_festivo";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                ddlBusAno.SelectedValue = "&gt;";
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_festivo");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgFestivo.Columns[6].Visible = (Boolean)permisos["UPDATE"];
        dtgFestivo.Columns[7].Visible = (Boolean)permisos["DELETE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblAutomatico.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtFecha.Visible = true;
        LblFecha.Visible = false;
        RfvFecha.Enabled = true;

    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblAutomatico.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null)
        {
            try
            {
                lblMensaje.Text = "";
                tblAutomatico.Visible = false;
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar.Trim()))
                {
                    m_festivo FestivoBuscar = new m_festivo();
                    FestivoBuscar.fecha = Convert.ToDateTime(modificar);
                    m_festivo Festivo = DelegadaBase.Servicios.traer_m_festivo(goInfo, FestivoBuscar);
                    if (Festivo != null)
                    {
                        string[] lsFechas = modificar.Split('/');
                        LblFecha.Text = lsFechas[2].ToString() + "/" + lsFechas[1].ToString() + "/" + lsFechas[0].ToString();
                        ddlDiaHabil.SelectedValue = Festivo.ind_dia_habil.ToString();
                        ddlEstado.SelectedValue = Festivo.estado.ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtFecha.Visible = false;
                        LblFecha.Visible = true;
                        RfvFecha.Enabled = false;
                    }
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar.Trim());
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Fecha Festivo =  " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblAutomatico.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        try
        {
            //FLLG - Agrega Validacion para controlar texto txtBusCodigoFestivo.Text vacio
            if (txtBusAno.Text != "" && txtBusAno.Text != null)
            {
                loFestivoBuscar.ano = Convert.ToInt32(txtBusAno.Text);
            }
            if (ddlBuscaDiaHabil.SelectedValue != "")
                loFestivoBuscar.ind_dia_habil = Char.Parse(ddlBuscaDiaHabil.SelectedValue.Trim().Substring(0, 1));
            List<m_festivo> listaFestivo = new List<m_festivo>();
            listaFestivo.Add(loFestivoBuscar);
            List<String> llFiltros = new List<string>();
            //FLLG - Agrega Validacion para controlar busqueda cuando codigo sea cero - cuando no es postback
            llFiltros.Add("");
            llFiltros.Add(ddlBusAno.SelectedValue);
            llFiltros.Add("");
            llFiltros.Add("");
            llFiltros.Add(ddlBusDiaHabil.SelectedValue);
            string lsCondicion = "";
            dtgFestivo.DataSource = DelegadaBase.Servicios.consultarTabla<m_festivo>(goInfo, listaFestivo, llFiltros, lsCondicion);
            dtgFestivo.DataBind();
            /////////////////////////////////////////////////////////////////////////////////////
            /// Para Llenar una Grilla Con la Clase clProcesosSql
            ////////////////////////////////////////////////////////////////////////////////////
            //lConexion = new clConexion(goInfo);
            //lConexion.Abrir();
            //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
            //dtgActividadEconomica.DataBind();
            //lConexion.Cerrar();
            /////////////////////////////////////////////////////////////////////////////////////
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lkbConsultar_Click(object sender, EventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!VerificarExistencia(gsTabla, "fecha = '" + TxtFecha.Text + "'"))
            {
                m_festivo Festivo = new m_festivo();
                Festivo.fecha = Convert.ToDateTime(TxtFecha.Text);
                Festivo.ano = Convert.ToInt32(TxtFecha.Text.Substring(0, 4));
                Festivo.mes = Convert.ToInt32(TxtFecha.Text.Substring(5, 2));
                Festivo.dia = Convert.ToInt32(TxtFecha.Text.Substring(8, 2));
                Festivo.ind_dia_habil = Char.Parse(ddlDiaHabil.SelectedValue.Trim().Substring(0, 1));
                Festivo.estado = Char.Parse(ddlEstado.SelectedValue.Trim().Substring(0, 1));
                DelegadaBase.Servicios.guardar_m_festivo(goInfo, Festivo);
                lblMensaje.Text = "";
                Listar();
            }
            else
            {
                lblMensaje.Text = "Fecha de Festivo YA existe";
            }

        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsFechas = LblFecha.Text.Split('/');
        string lsFecha = lsFechas[2].ToString().Trim() + "/" + lsFechas[1].ToString().Trim() + "/" + lsFechas[0].ToString().Trim();

        try
        {
            m_festivo Festivo = new m_festivo();
            Festivo.fecha = Convert.ToDateTime(LblFecha.Text);
            Festivo.ano = Festivo.fecha.Year;
            Festivo.mes = Festivo.fecha.Month;
            Festivo.dia = Festivo.fecha.Day;
            Festivo.ind_dia_habil = Char.Parse(ddlDiaHabil.SelectedValue.Trim().Substring(0, 1));
            Festivo.estado = Char.Parse(ddlEstado.SelectedValue.Trim().Substring(0, 1));
            DelegadaBase.Servicios.modificar_m_festivo(goInfo, Festivo);
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", lsFecha);
            Listar();

        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", lsFecha);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblFecha.Text.Trim() != "")
        {
            string[] lsFechas = LblFecha.Text.Split('/');
            string lsFecha = lsFechas[2].ToString().Trim() + "/" + lsFechas[1].ToString().Trim() + "/" + lsFechas[0].ToString().Trim();
            manejo_bloqueo("E", lsFecha);
        }
        Listar();
    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgFestivo_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgFestivo.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgActividadEconomica_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgFestivo_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoFestivo = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoFestivo = this.dtgFestivo.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoFestivo);
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", this.dtgFestivo.Items[e.Item.ItemIndex].Cells[0].Text.Trim()))
            {
                lCodigoFestivo = this.dtgFestivo.Items[e.Item.ItemIndex].Cells[0].Text;
                try
                {
                    DelegadaBase.Servicios.borrar_m_festivo(goInfo, Convert.ToDateTime(lCodigoFestivo));
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "No se pudo eliminar el día porque tiene registros relacionados";
                }
            }
            else
            {
                lblMensaje.Text = "No se Puede Eliminar el Registro por que esta Bloqueado. Fecha Festivo =  " + this.dtgFestivo.Items[e.Item.ItemIndex].Cells[0].Text.ToString();
            }
            Listar();
        }

    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTabla, string lsCondicion)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lsCondicion, goInfo);
    }


    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgActividadEconomica.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgActividadEconomica.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lsFecha)
    {
        string lsCondicion = "nombre_tabla='m_festivo' and llave_registro='fecha=" + lsFecha.ToString() + "'";
        string lsCondicion1 = "fecha=" + lsFecha.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_festivo";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_festivo", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " ano > 0";
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=1&procedimiento=pa_ValidarExistencia&columnas=fecha*ano*mes*dia*ind_dia_habil*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion + "&nombre_tabla=m_festivo&titulo_informe=Listado Tabla");
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }
    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " ano > 0";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_festivo&procedimiento=pa_ValidarExistencia&columnas=fecha*ano*mes*dia*ind_dia_habil*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbAutomatico_Click(object sender, ImageClickEventArgs e)
    {
        if (TxtAno.Text == "")
            lblMensaje.Text = "Debe Digitar el año";
        else
        {
            //      string[] lsNombreParametros = "@P_ano";
            ////           Int32[] lTipoparametros ;
            //        object[] lValorParametros ;

            string[] lsNombreParametros = { "@P_ano" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
            object[] lValorParametros = { TxtAno.Text };


            lConexion = new clConexion(goInfo);
            lConexion.Abrir();
            clProcesosSql lProcesosSql = new clProcesosSql("pa_insertar_dias_habiles", lsNombreParametros, lTipoparametros, lValorParametros);
            if (lProcesosSql.EjecutarProcedimiento(lConexion.gObjConexion))
                lblMensaje.Text = "Se crearon exitosamente los días para el año " + TxtAno.Text;
            else
                lblMensaje.Text = "No se pudieron crear los días del año " + TxtAno.Text;
            lConexion.Cerrar();
            Listar();

        }

    }

}