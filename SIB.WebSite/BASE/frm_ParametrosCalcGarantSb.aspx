﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosCalcGarantSb.aspx.cs"
    Inherits="BASE_frm_ParametrosCalcGarantSb" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Precio de Venta por Defecto
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtPerioVentaDef" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtPerioVentaDef" runat="server" ErrorMessage="Debe Ingresar el Precio de Venta por Defecto"
                    ControlToValidate="TxtPerioVentaDef" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtPerioVentaDef" runat="server" ControlToValidate="TxtPerioVentaDef"
                    Type="Double" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo de Precio de Venta por Defecto debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">
                Precio de Compra por Defecto
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtPrecioCompraDef" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtPrecioCompraDef" runat="server" ErrorMessage="Debe Ingresar el Precio de Compra por Defecto"
                    ControlToValidate="TxtPrecioCompraDef" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtPrecioCompraDef" runat="server" ControlToValidate="TxtPrecioCompraDef"
                    Type="Double" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo de Precio de Compra por Defecto debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cantidad de Venta por Defecto
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCantidadVentaDef" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtCantidadVentaDef" runat="server" ErrorMessage="Debe Ingresar la Cantidad de Venta por Defecto"
                    ControlToValidate="TxtCantidadVentaDef" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TxtPerioVentaDef"
                    Type="Double" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo de Cantidad de Venta por Defecto debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">
                Cantidad de Compra por Defecto
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCantidadCompraDef" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtCantidadCompraDef" runat="server" ErrorMessage="Debe Ingresar la Cantidad de Compra por Defecto"
                    ControlToValidate="TxtCantidadCompraDef" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtCantidadCompraDef" runat="server" ControlToValidate="TxtCantidadCompraDef"
                    Type="Double" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo de Cantidad de Compra por Defecto debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Constante No. 1
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtConstante1" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtConstante1" runat="server" ErrorMessage="Debe Ingresar la Contante No. 1"
                    ControlToValidate="TxtConstante1" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtConstante1" runat="server" ControlToValidate="TxtConstante1"
                    Type="Double" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo de Constante 2 debe Ser numérico">*</asp:CompareValidator>                   
            </td>
            <td class="td1">
                Constante No. 2
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtConstante2" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtConstante2" runat="server" ErrorMessage="Debe Ingresar la Contante No. 2"
                    ControlToValidate="TxtConstante2" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtConstante2" runat="server" ControlToValidate="TxtConstante2"
                    Type="Double" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo de Constante 2 debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Constante No. 3
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtConstante3" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtConstante3" runat="server" ErrorMessage="Debe Ingresar la Contante No. 3"
                    ControlToValidate="TxtConstante3" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtConstante3" runat="server" ControlToValidate="TxtConstante3"
                    Type="Double" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo de Constante 3 debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                No. Mes para Obtener Ofertas y Demandas de Compra
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoMesOyDCompra" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoMesOyDCompra" runat="server" ErrorMessage="Debe Ingresar No. Mes para Obtener Ofertas y Demandas de Compra"
                    ControlToValidate="TxtNoMesOyDCompra" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtNoMesOyDCompra" runat="server" ControlToValidate="TxtCantidadCompraDef"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo No. Mes para Obtener Ofertas y Demandas de Compra debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">
                No. Días hábiles Publicación Información Garantía Participación inicial luego Subasta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiasParticipa" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoDiasParticipa" runat="server" ErrorMessage="Debe Ingresar No. Días hábiles Publicación Información Garantía Participación inicial luego Subasta"
                    ControlToValidate="TxtNoDiasParticipa" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtNoDiasParticipa" runat="server" ControlToValidate="TxtNoDiasParticipa"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo No. Días hábiles Publicación Información Garantía Participación inicial luego Subasta debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                No. Días hábiles Publicación Información Garantía Cumplimiento Primer Mes luego Subasta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiasCumpliMes1" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoDiasCumpliMes1" runat="server" ErrorMessage="Debe Ingresar No. Días hábiles Publicación Información Garantía Cumplimiento Primer Mes luego Subasta"
                    ControlToValidate="TxtNoDiasCumpliMes1" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtNoDiasCumpliMes1" runat="server" ControlToValidate="TxtNoDiasCumpliMes1"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo No. Días hábiles Publicación Información Garantía Cumplimiento Primer Mes luego Subasta debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">
                No. Días hábiles Publicación Información Garantía Cumplimiento Segundo Mes luego
                Subasta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiasCumpliMes2" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoDiasCumpliMes2" runat="server" ErrorMessage="Debe Ingresar No. Días hábiles Publicación Información Garantía Cumplimiento Segundo Mes luego Subasta"
                    ControlToValidate="TxtNoDiasCumpliMes2" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtNoDiasCumpliMes2" runat="server" ControlToValidate="TxtNoDiasCumpliMes2"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo No. Días hábiles Publicación Información Garantía Cumplimiento Segundo Mes luego Subasta debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Año Indice
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtAnoIndice" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtAnoIndice" runat="server" ErrorMessage="Debe Ingresar Año Indice"
                    ControlToValidate="TxtAnoIndice" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtAnoIndice" runat="server" ControlToValidate="TxtAnoIndice"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo Mes Indice debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">
                Mes Indice
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtMesIndice" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtMesIndice" runat="server" ErrorMessage="Debe Ingresar Año Indice"
                    ControlToValidate="TxtMesIndice" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtMesIndice" runat="server" ControlToValidate="TxtMesIndice"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="VsParametros" ErrorMessage="El Campo Mes Indice debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Observación Cambio
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    </asp:Content>
