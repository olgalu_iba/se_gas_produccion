﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosSubBimestral.aspx.cs"
    Inherits="BASE_frm_ParametrosSubBimestral" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Fecha Inicial Proceso de Comercialización {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniProCom" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaIniProCom" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicial Proceso de Comercialización"
                    ControlToValidate="TxtFechaIniProCom" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Fecha Final Proceso de Comercialización {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFinProCom" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaFinProCom" runat="server" ErrorMessage="Debe Ingresar la Fecha Final Proceso de Comercialización"
                    ControlToValidate="TxtFechaFinProCom" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Fecha Inicial Declaración Previa {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniDeclPrev" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaIniDeclPrev" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicial Declaración Previa"
                    ControlToValidate="TxtFechaIniDeclPrev" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Fecha Final Declaración Previa {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFinDecPrev" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaFinDecPrev" runat="server" ErrorMessage="Debe Ingresar la Fecha Final Declaración Previa"
                    ControlToValidate="TxtFechaFinDecPrev" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Año de gas de las subastas  <%--20170714 ajuste año declaracion--%>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtAnoDeclara" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtAnoDeclara" runat="server" ErrorMessage="Debe Ingresar el Año para el cual realizan las subastas"
                    ControlToValidate="TxtAnoDeclara" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Número de Subastas a bloquear por No Constitución de Garantías
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoSubBloqueo" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoSubBloqueo" runat="server" ErrorMessage="Debe Ingresar el Número de Subastas a bloquear por No Constitución de Garantías"
                    ControlToValidate="TxtNoSubBloqueo" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Valor Adicional Precio de Reserva
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtValAdicPrecRese" runat="server" MaxLength="6" Width="150px" onkeyup="return formatoMilesPre(this,event,2);"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FteTxtValAdicPrecRese" runat="server" Enabled="True"
                    FilterType="Custom, Numbers" TargetControlID="TxtValAdicPrecRese" ValidChars=",.">
                </cc1:FilteredTextBoxExtender>
                <asp:RequiredFieldValidator ID="RfvTxtValAdicPrecRese" runat="server" ErrorMessage="Debe Ingresar el Valor Adicional Precio de Reserva"
                    ControlToValidate="TxtValAdicPrecRese" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Fecha publicación declaración previa
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaPubPrev" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtFechaPubPrev" runat="server" ErrorMessage="Debe Ingresar la Fecha de publicación de la declaración previa"
                    ControlToValidate="TxtFechaPubPrev" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <%--20170714 ajuste año declaracion--%>
        <tr>
            <td class="td1">
                Año de declaración previa
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtAnoDecPrev" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtAnoDecPrev" runat="server" ErrorMessage="Debe Ingresar el Año para el cual se realiza la Declaración Previa"
                    ControlToValidate="TxtAnoDecPrev" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Observación Cambio
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsParam" runat="server" ValidationGroup="VsParametrosBna" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>