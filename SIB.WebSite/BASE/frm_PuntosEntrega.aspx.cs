﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_PuntosEntrega : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Puntos de Entrega";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPozoIni, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPozoFin, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();

            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_puntos_entrega");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[7].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[8].Visible = (Boolean)permisos["DELETE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoPuntoEntre.Visible = false;
        LblCodigoPuntoEntre.Visible = true;
        LblCodigoPuntoEntre.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_puntos_entrega", " codigo_punto_entrega = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoPuntoEntre.Text = lLector["codigo_punto_entrega"].ToString();
                        TxtCodigoPuntoEntre.Text = lLector["codigo_punto_entrega"].ToString();
                        TxtDescripcion.Text = lLector["descripcion"].ToString();
                        ddlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                        try
                        {
                            ddlPozoIni.SelectedValue = lLector["codigo_pozo_ini"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El punto inicial del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlPozoFin.SelectedValue = lLector["codigo_pozo_fin"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El punto final del registro no existe o esta inactivo<br>";
                        }
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        try
                        {
                            ddlTipoSubasta.SelectedValue = lLector["codigo_tipo_subasta"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tipo de subasta registro no existe o esta inactivo<br>";
                        }
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoPuntoEntre.Visible = false;
                        LblCodigoPuntoEntre.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Punto Entrega " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_punto_entrega", "@P_descripcion", "@P_destino", "@P_codigo_tipo_subasta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "S", "0" };

        try
        {
            if (TxtBusPuntoEntre.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusPuntoEntre.Text.Trim();
            if (TxtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
            lValorParametros[2] = ddlBusDestino.SelectedValue;
            if (ddlBusTipoSubasta.SelectedValue != "0")
                lValorParametros[3] = ddlBusTipoSubasta.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetPuntoEntrega", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_punto_entrega", "@P_descripcion", "@P_destino_rueda", "@P_estado", "@P_codigo_pozo_ini", "@P_codigo_pozo_fin", "@P_accion", "@P_codigo_tipo_subasta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "", "", "0", "0", "1", "0" };
        lblMensaje.Text = "";

        try
        {
            if (TxtDescripcion.Text.Trim() == "")
                lblMensaje.Text += " Debe digitar la Descripcion del Punto de Entrega <br>";
            else
                if (VerificarExistencia("m_puntos_entrega", " descripcion = '" + TxtDescripcion.Text.Trim() + "' And destino_rueda = '" + ddlDestino.SelectedValue + "' And codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue))
                    lblMensaje.Text += " La Descripcion del Punto de Entrega  YA existe " + TxtDescripcion.Text + " <br>";
            if (ddlTipoSubasta.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Subasta. <br>";
            if (ddlDestino.SelectedValue == "T")
            {
                if (ddlPozoIni.SelectedValue == "0")
                    lblMensaje.Text += " Debe Seleccionar el pozo inicial. <br>";
                if (ddlPozoIni.SelectedValue == "0")
                    lblMensaje.Text += " Debe Seleccionar el pozo final. <br>";
                if (ddlPozoIni.SelectedValue != "0" && ddlPozoIni.SelectedValue == ddlPozoFin.SelectedValue)
                    lblMensaje.Text += " EL pozo inicial debe ser diferente al pozo final. <br>";
            }

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlDestino.SelectedValue;
                lValorParametros[3] = ddlEstado.SelectedValue;
                if (ddlDestino.SelectedValue == "T")
                {
                    lValorParametros[4] = ddlPozoIni.SelectedValue;
                    lValorParametros[5] = ddlPozoFin.SelectedValue;
                }
                else
                {
                    lValorParametros[4] = "0";
                    lValorParametros[5] = "0";
                }
                lValorParametros[7] = ddlTipoSubasta.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetPuntoEntrega", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación del Punto de Entrega.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_punto_entrega", "@P_descripcion", "@P_destino_rueda", "@P_estado", "@P_codigo_pozo_ini", "@P_codigo_pozo_fin", "@P_accion", "@P_codigo_tipo_subasta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "", "", "", "", "2", "0" };
        lblMensaje.Text = "";
        try
        {
            if (TxtDescripcion.Text.Trim() == "")
                lblMensaje.Text += " Debe digitar la Descripcion del Punto de Entrega <br>";
            else
                if (VerificarExistencia("m_puntos_entrega", " descripcion = '" + this.TxtDescripcion.Text.Trim() + "' And destino_rueda = '" + ddlDestino.SelectedValue + "' And codigo_punto_entrega != " + LblCodigoPuntoEntre.Text + " And codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue))
                    lblMensaje.Text += " La Descripcion del Punto de Entrega  YA existe " + TxtDescripcion.Text + " <br>";
            if (ddlTipoSubasta.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Subasta. <br>";
            if (ddlDestino.SelectedValue == "G")
            {
                if (VerificarExistencia("m_puntos_entrega pto, m_ruta rut", " pto.codigo_punto_entrega = " + LblCodigoPuntoEntre.Text + " and rut.codigo_punto_entrega =" + LblCodigoPuntoEntre.Text))
                    lblMensaje.Text += " El punto de entrega no se puede cambiar a gas porque tiene tramos definidos<br>";
            }
            else
            {
                if (VerificarExistencia("m_puntos_entrega pto, m_ruta rut", " pto.codigo_punto_entrega = " + LblCodigoPuntoEntre.Text + " and rut.codigo_punto_entrega =" + LblCodigoPuntoEntre.Text))
                {
                    if (!VerificarExistencia("m_puntos_entrega pto", " pto.codigo_punto_entrega = " + LblCodigoPuntoEntre.Text + " and (pto.codigo_pozo_ini <>" + ddlPozoIni.SelectedValue + " or pto.codigo_pozo_fin <> " + ddlPozoFin.SelectedValue + ")"))
                        lblMensaje.Text += " No se pueden cambiar los pozos inicial o final porque ya está defiida la ruta<br>";
                }
            }

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoPuntoEntre.Text;
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = ddlDestino.SelectedValue;
                lValorParametros[3] = ddlEstado.SelectedValue;
                if (ddlDestino.SelectedValue == "T")
                {
                    lValorParametros[4] = ddlPozoIni.SelectedValue;
                    lValorParametros[5] = ddlPozoFin.SelectedValue;
                }
                else
                {
                    lValorParametros[4] = "0";
                    lValorParametros[5] = "0";
                }
                lValorParametros[7] = ddlTipoSubasta.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetPuntoEntrega", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del Punto de Entrega.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoPuntoEntre.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoPuntoEntre.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        /// Desbloquea el Registro Actualizado
        if (LblCodigoPuntoEntre.Text != "")
            manejo_bloqueo("E", LblCodigoPuntoEntre.Text);
        Listar();

    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[2].Text == "G")
            {
                trPozoIni.Visible = false;
                trPozoFin.Visible = false;
            }
            else
            {
                trPozoIni.Visible = true;
                trPozoFin.Visible = true;
            }
            Modificar(lCodigoRegistro);
        }
        if (((LinkButton)e.CommandSource).Text == "Rutas")
        {
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[2].Text != "T")
                lblMensaje.Text = "Los tramos solo se definen para puntos de entrega de trasporte";
            if (lblMensaje.Text == "")
            {
                lblCodPunt.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                lblDescPunto.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
                lblCodPozI.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text;
                lblDescPozI.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[11].Text;
                lblCodPozF.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[12].Text;
                lblDescPozF.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[13].Text;
                LlenarTramo(lblCodPunt.Text);
                Tramos(lblCodPunt.Text);
            }
        }

    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_puntos_entrega' and llave_registro='codigo_punto_entrega=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_punto_entrega=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_puntos_entrega";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_puntos_entrega", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "", "S", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusPuntoEntre.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusPuntoEntre.Text.Trim();
                lsParametros += " Codigo Punto Entrega : " + TxtBusPuntoEntre.Text;

            }
            if (TxtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
                lsParametros += " - Descripcion: " + TxtBusDescripcion.Text;
            }
            if (ddlBusDestino.SelectedValue != "S")
            {
                lValorParametros[2] = ddlBusDestino.SelectedValue;
                lsParametros += " - Destino Rueda: " + ddlBusDestino.SelectedItem.ToString();
            }
            if (ddlBusTipoSubasta.SelectedValue != "0")
            {
                lValorParametros[3] = ddlBusTipoSubasta.SelectedValue.Trim();
                lsParametros += " - Tipo Subasta: " + ddlBusTipoSubasta.SelectedItem.ToString();
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetPuntoEntrega&nombreParametros=@P_codigo_punto_entrega*@P_descripcion*@P_destino*@P_codigo_tipo_subasta&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=Producto*valor_negocio*dias_plazo*valor_tasa_venta*valor_tasa_compra*valor_tasa_cierre&titulo_informe=Listado de Puntos de Entrega&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_punto_entrega <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_puntos_entrega&procedimiento=pa_ValidarExistencia&columnas=codigo_punto_entrega*descripcion*destino_rueda*codigo_tipo_subasta*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: tramos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Tramos(string codigo)
    {
        if (codigo != null && codigo != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", codigo))
                {
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", codigo);
                    //Actualiza la grilla de tramos
                    listar_tramo();

                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Pueden modificar los tramos porque el registro esta Bloqueado. Codigo Punto Entrega " + codigo;

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblTramo.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "DEIFNICIO DE RUTAS " + lsTitulo;
            imbActualizaTra.Visible = false;
            imbCrearTra.Visible = true;
        }
    }
    /// <summary>
    /// Nombre: imbCrearTra_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrearTra_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";

        try
        {
            if (ddlTramo.SelectedValue == "0")
                lblMensaje.Text = "Debe seleccionar el tramo<br>";
            if (VerificarExistencia("m_ruta", "codigo_punto_entrega=" + lblCodPunt.Text + " and codigo_tramo=" + ddlTramo.SelectedValue))
                lblMensaje.Text += "Ya existe el tramo seleccionado para la ruta<br>";
            if (lblCodPozF.Text == hndPozoFin.Value)
                lblMensaje.Text += "No puede agregar mas tramos porque ya llego al pozo final<br>";
            if (lblMensaje.Text == "")
            {
                string[] lsNombreParametros = { "@P_codigo_punto_entrega", "@P_codigo_tramo" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { lblCodPunt.Text, ddlTramo.SelectedValue };

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRuta", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la ruta.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    listar_tramo();
                    LlenarTramo(lblCodPunt.Text);
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void listar_tramo()
    {
        string[] lsNombreParametros = { "@P_codigo_punto_entrega" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { lblCodPunt.Text };

        lConexion.Abrir();
        dtgTramo.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRuta", lsNombreParametros, lTipoparametros, lValorParametros);
        dtgTramo.DataBind();
        lConexion.Cerrar();
        hndCuenta.Value = this.dtgTramo.Items.Count.ToString();
        foreach (DataGridItem Grilla in this.dtgTramo.Items)
        {
            hndOrden.Value = Grilla.Cells[6].Text;
            hndPozoFin.Value = Grilla.Cells[4].Text;
        }
    }
    /// <summary>
    /// Nombre: dtgTramo_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgTramo_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        //if (((LinkButton)e.CommandSource).Text == "Modificar")
        //{
        //    lblCodTramo.Text = this.dtgTramo.Items[e.Item.ItemIndex].Cells[0].Text;
        //    txtPtoIni.Text = this.dtgTramo.Items[e.Item.ItemIndex].Cells[2].Text; ;
        //    txtPtoFin.Text = this.dtgTramo.Items[e.Item.ItemIndex].Cells[3].Text; ;
        //    imbActualizaTra.Visible = true;
        //    imbCrearTra.Visible = false;
        //}
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            if (hndOrden.Value == this.dtgTramo.Items[e.Item.ItemIndex].Cells[6].Text)
            {
                string[] lsNombreParametros = { "@P_codigo_ruta" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { this.dtgTramo.Items[e.Item.ItemIndex].Cells[0].Text };

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelRuta", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la eliminación de la ruta.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    listar_tramo();
                    LlenarTramo(lblCodPunt.Text);
                }
            }
            else
                lblMensaje.Text = "No se puede eliminar una ruta intermedia";
        }
    }
    /// <summary>
    /// Nombre: imbActualizaTra_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void imbActualizaTra_Click(object sender, ImageClickEventArgs e)
    //{
    //    lblMensaje.Text = "";
    //    try
    //    {
    //        if (txtPtoIni.Text == "")
    //            lblMensaje.Text = "Debe digitar el punto inicial<br>";
    //        if (txtPtoFin.Text == "")
    //            lblMensaje.Text += "Debe digitar el punto final<br>";
    //        if (VerificarExistencia("m_tramo", "codigo_punto_entrega=" + lblCodPunt.Text + " and punto_inicio='" + txtPtoIni.Text + "' and codigo_tramo<>" + lblCodTramo.Text))
    //            lblMensaje.Text += "Ya existe un tramo con el punto inicial digitado<br>";
    //        if (VerificarExistencia("m_tramo", "codigo_punto_entrega=" + lblCodPunt.Text + " and punto_fin='" + txtPtoFin.Text + "' and codigo_tramo<>" + lblCodTramo.Text))
    //            lblMensaje.Text += "Ya existe un tramo con el punto inicial digitado<br>";
    //        if (lblMensaje.Text == "")
    //        {
    //            string[] lsNombreParametros = { "@P_codigo_tramo", "@P_punto_ini", "@P_punto_fin" };
    //            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
    //            string[] lValorParametros = { lblCodTramo.Text, txtPtoIni.Text, txtPtoFin.Text };

    //            lConexion.Abrir();
    //            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_UptTramo", lsNombreParametros, lTipoparametros, lValorParametros))
    //            {
    //                lblMensaje.Text = "Se presentó un Problema en la Actualizacion del tramo.!";
    //                lConexion.Cerrar();
    //            }
    //            else
    //            {
    //                lConexion.Cerrar();
    //                listar_tramo();
    //                imbCrearTra.Visible = true;
    //                imbActualizaTra.Visible = false;
    //                txtPtoIni.Text = "";
    //                txtPtoFin.Text = "";
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        /// Desbloquea el Registro Actualizado
    //        lblMensaje.Text = ex.Message;
    //    }
    //}
    /// <summary>
    /// Nombre: imbSalirTra_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalirTra_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        if (hndCuenta.Value != "0" && lblCodPozF.Text != hndPozoFin.Value)
            lblMensaje.Text += "No puede salir porque no ha llegado al punto final <br>";
        else
        {

            /// Desbloquea el Registro Actualizado
            if (lblCodPunt.Text != "")
                manejo_bloqueo("E", lblCodPunt.Text);
            tblTramo.Visible = false;
            Listar();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDestino_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlTipoSubasta.SelectedValue != "0")
        //{
        //    lConexion.Abrir();
        //    ddlPozoIni.Items.Clear();
        //    LlenarControles(lConexion.gObjConexion, ddlPozoIni, "m_pozo", " estado = 'A' And codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue + " order by descripcion", 0, 1);
        //    lConexion.Cerrar();
        //}
        if (ddlDestino.SelectedValue == "G")
        {
            trPozoIni.Visible = false;
            trPozoFin.Visible = false;
        }
        else
        {
            trPozoIni.Visible = true;
            trPozoFin.Visible = true;
        }
    }

    /// <summary>
    /// Nombre: LlenarTramo
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarTramo(string lsCodPtoEntrega)
    {
        ddlTramo.Items.Clear();
        string[] lsNombreParametros = { "@P_codigo_punto_entrega" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { lsCodPtoEntrega };

        lConexion.Abrir();
        SqlDataReader lLector;
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        ddlTramo.Items.Add(lItem);

        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramoPto", lsNombreParametros, lTipoparametros, lValorParametros);
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {

                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(0).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                ddlTramo.Items.Add(lItem1);
            }
        }
        lLector.Close();
        lConexion.Cerrar();
    }


}
