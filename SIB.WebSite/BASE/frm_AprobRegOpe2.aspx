﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_AprobRegOpe2.aspx.cs"
    Inherits="BASE_frm_AprobRegOpe2" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" width="100%">
        <tr>
            <td class="th1" align="center">
                <asp:Button ID="btnAprobar" runat="server" Text="Aprobar" autoPostback="true" OnClick="btnAprobar_Click" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="td1"
                    ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1" OnEditCommand="dtgMaestro_EditCommand">
                    <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                    <ItemStyle CssClass="td2"></ItemStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Apr">
                            <ItemTemplate>
                                <asp:CheckBox ID="ChkAprobar" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod" ItemStyle-HorizontalAlign="Center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="no_documento" HeaderText="No. doc" ItemStyle-HorizontalAlign="Center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="digito_verif" HeaderText="DV" ItemStyle-HorizontalAlign="Center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="razon_social" HeaderText="Razon social" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_tipo" HeaderText="tpo operador" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <asp:EditCommandColumn HeaderText="Comunicado" EditText="Comunicado" ItemStyle-HorizontalAlign="Center">
                        </asp:EditCommandColumn>
                        <asp:EditCommandColumn HeaderText="Sarlaf" EditText="Sarlaf" ItemStyle-HorizontalAlign="Center">
                        </asp:EditCommandColumn>
                        <asp:BoundColumn DataField="ruta_archivo_comunicado" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ruta_archivo_sarlaf" Visible="false"></asp:BoundColumn>
                        <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" ItemStyle-HorizontalAlign="Center">
                        </asp:EditCommandColumn>
                        <asp:BoundColumn DataField="tiene_usuario" Visible="false"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                    <HeaderStyle CssClass="th1"></HeaderStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>