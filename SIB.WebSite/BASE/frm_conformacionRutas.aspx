﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_conformacionRutas.aspx.cs"
    Inherits="BASE_frm_conformacionRutas" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" Text="CONFORMACION AUTOMATICA DE RUTAS DEL SNT" ForeColor ="White" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" class="td1">
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center" class="th3">
                <asp:Button ID="btnProcesar" OnClick="BtnProcesar_Click" runat="server" Text="Procesar" />
            </td>
        </tr>
        <tr>
            <td align="center" class="td1">
                <br />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>