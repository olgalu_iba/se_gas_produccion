﻿using System;
using System.Data;
using System.Data.SqlClient;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
public partial class BASE_frm_ParametrosTrasnporte : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Trasnporte";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;
    string gsTabla = "m_parametros_sub_transporte";

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        Master.Titulo = "Parámetros de Transporte";
        /// Activacion de los Botones
        var botones = new[] { EnumBotones.Modificar, EnumBotones.Salir };
        buttons.Inicializar(botones: botones);
        buttons.ModificarOnclick += btnModifica;
        buttons.SalirOnclick += btnSalir;
        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            Modificar();
        }
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtIngCmmp.Text = lLector["dia_hab_notif_cmmp_tras"].ToString();
                    TxtIngIpat.Text = lLector["dia_hab_ing_cmmp_tra"].ToString();
                    TxtPubIpat.Text = lLector["dia_hab_pub_cmmp"].ToString();
                    TxtSolRem.Text = lLector["dia_sol_remitente"].ToString();
                    TxtPubSolIni.Text = lLector["dia_pub_sol_rem"].ToString();
                    TxtModSolRem.Text = lLector["dia_mod_sol_rem"].ToString();
                    TxtPubSolFin.Text = lLector["dia_pub_mod_sol_rem"].ToString();
                    TxtOtroSi.Text = lLector["dia_otrosi_excede"].ToString();
                    txtInvPub.Text = lLector["dia_inv_publ"].ToString();
                    txtCapAmp.Text = lLector["dia_sol_amp"].ToString();
                    txtPubAmp.Text = lLector["dia_pub_amp"].ToString();
                    txtCntDisp.Text = lLector["dia_cnt_disp"].ToString();
                    txtPubDisp.Text = lLector["dia_pub_disp"].ToString();
                    txtPubPag.Text = lLector["dia_pub_pag"].ToString();
                    txtRemPag.Text = lLector["dia_rem_pag"].ToString();
                    txtAsigPag.Text = lLector["dia_asigna_pag"].ToString();
                    txtPubAsigPag.Text = lLector["dia_pub_asig_pag"].ToString();
                    txtRegPag.Text = lLector["dia_reg_cont_pag"].ToString();
                    txtIaeCop.Text = lLector["iae_cop"].ToString();
                    txtIaeUsd.Text = lLector["iae_usd"].ToString();
                    ////////////////////////////////////////////
                    /// Campos Nuevos Req. 005-2021 20210120 ///
                    ////////////////////////////////////////////
                    txtDiaIniComDecla.Text = lLector["dia_ini_com_decla"].ToString();
                    txtDiaFinComDecla.Text = lLector["dia_fin_com_decla"].ToString();
                    txtDiaDesPubDecla.Text = lLector["dia_des_publi_decla"].ToString();
                    txtDiaDesHabParti.Text = lLector["dia_des_habil_parti"].ToString();
                    txtDiaDesDeclaOfer.Text = lLector["dia_des_hacer_decla"].ToString();
                    txtHoraIniDecOfer.Text = lLector["hora_ini_decla_ofer"].ToString();
                    txtHoraFinDecOfer.Text = lLector["hora_fin_decla_ofer"].ToString();
                    txtDiaDesPubPrec.Text = lLector["dia_des_publi_prec"].ToString();
                    txtDiaDesAsigProy.Text = lLector["dia_des_asig_proye"].ToString();
                    txtCantMInPostSub.Text = lLector["cant_min_ofer_suba"].ToString();
                    ////////////////////////////////////////////
                    txtDiasTrimReg.Text = lLector["dia_hab_trim_reg"].ToString(); //20220106 rq001-22
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
                divDatos.Visible = true;
            }
            else
            {
                divDatos.Visible = false;
                var botones = new[] { EnumBotones.Salir };
                buttons.Inicializar(botones: botones);
                buttons.SalirOnclick += btnSalir;
                Toastr.Error(this, "No se puede modificar el registro porque está bloqueado");
            }
        }
        catch (Exception ex)
        {
            divDatos.Visible = false;
            Toastr.Error(this, "Error al traer los datos. " + ex.Message.ToString()); //20220106 rq001-22
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnModifica(object sender, EventArgs e)
    {
        Modal.Abrir(this, registroAdicion.ID, registroAdicionInside.ID);
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <summary>
    /// 
    /// </summary>
    protected void btnSalir(object sender, EventArgs e)
    {
        manejo_bloqueo("E", "");
        Response.Redirect("~/WebForms/Parametros/frm_parametros.aspx?idParametros=3");
    }

    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// </summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_sub_transporte' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Cancel_OnClick(object sender, EventArgs e)
    {
        Modal.Cerrar(this, registroAdicion.ID);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        Modal.Cerrar(this, registroAdicion.ID);



        string[] lsNombreParametros = { "@P_dia_hab_notif_cmmp_tras", "@P_dia_hab_ing_cmmp_tra", "@P_dia_hab_pub_cmmp", "@P_dia_sol_remitente", "@P_dia_pub_sol_rem", "@P_dia_mod_sol_rem", "@P_dia_pub_mod_sol_rem", "@P_dia_otrosi_excede",
            "@P_dia_inv_publ","@P_dia_sol_amp","@P_dia_pub_amp","@P_dia_cnt_disp","@P_dia_pub_disp","@P_dia_pub_pag","@P_dia_rem_pag","@P_dia_asigna_pag","@P_dia_pub_asig_pag","@P_dia_reg_cont_pag","@P_iae_cop","@P_iae_usd","@P_observaciones",
            /// Campos nuevos Req. 005-2021 20210120
            "@P_dia_ini_com_decla","@P_dia_fin_com_decla","@P_dia_des_publi_decla","@P_dia_des_habil_parti","@P_dia_des_hacer_decla","@P_hora_ini_decla_ofer","@P_hora_fin_decla_ofer",
            "@P_dia_des_publi_prec","@P_dia_des_asig_proye","@P_cant_min_ofer_suba",
            /// Hasta Aqui
            "@P_dia_hab_trim_reg" //20220106 rq001-22
        };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar,
                                        SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, /// Campos nuevos Req. 005-2021 20210120
                                        SqlDbType.Int //20220106 rq001-22
                                      };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "",
                                      "0", "0", "0", "0", "0", "", "", "0", "0", "0", // Campos nuevos Req. 005-2021 20210120
                                      "0"//20220106 rq001-22
                                    };
        string lsmensaje = "";
        decimal ldValor = 0;

        try
        {
            if (TxtIngCmmp.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles previos a la finalización del trimestre para enviar notificación de ingreso de información CMMP de proyectos IPAT<br>";
            else
            {
                if (Convert.ToInt32(TxtIngCmmp.Text) <= 0)
                    lsmensaje += "Los Días hábiles previos a la finalización del trimestre para enviar notificación de ingreso de información CMMP de proyectos IPAT deben ser mayores que cero<br>";
            }
            if (TxtIngIpat.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles máximos previos al trimestre de negociación para ingresar información CMMP de proyectos IPAT<br>";
            else
            {
                if (Convert.ToInt32(TxtIngIpat.Text) <= 0)
                    lsmensaje += "Los Días hábiles máximos previos al trimestre de negociación para ingresar información CMMP de proyectos IPAT deben ser mayores que cero<br>";
            }
            if (TxtPubIpat.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles de inicio del trimestre de negociación para publicación de información CMMP de proyectos IPAT<br>";
            else
            {
                if (Convert.ToInt32(TxtPubIpat.Text) <= 0)
                    lsmensaje += "Los Días hábiles de inicio del trimestre de negociación para publicación de información CMMP de proyectos IPAT deben ser mayores que cero<br>";
            }
            if (TxtSolRem.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles para el ingreso de solicitudes de remitentes en el inicio de trimestre de negociación<br>";
            else
            {
                if (Convert.ToInt32(TxtSolRem.Text) <= 0)
                    lsmensaje += "Los Días hábiles para el ingreso de solicitudes de remitentes en el inicio de trimestre de negociación deben ser mayores que cero<br>";
            }
            if (TxtPubSolIni.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles para la publicación de solicitudes de remitentes en el inicio de trimestre de negociación<br>";
            else
            {
                if (Convert.ToInt32(TxtPubSolIni.Text) <= 0)
                    lsmensaje += "Los Días hábiles para la publicación de solicitudes de remitentes en el inicio de trimestre de negociación deben ser mayores que cero<br>";
            }
            if (TxtModSolRem.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles para la modificación de solicitudes de remitentes en el inicio de trimestre de negociación<br>";
            else
            {
                if (Convert.ToInt32(TxtModSolRem.Text) <= 0)
                    lsmensaje += "Los Días hábiles para la modificación de solicitudes de remitentes en el inicio de trimestre de negociación deben ser mayores que cero<br>";
            }
            if (TxtPubSolFin.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles para la publicación de la modificación de solicitudes de remitentes en el inicio de trimestre de negociación<br>";
            else
            {
                if (Convert.ToInt32(TxtPubSolFin.Text) <= 0)
                    lsmensaje += "Los Días hábiles para la publicación de la modificación de solicitudes de remitentes en el inicio de trimestre de negociación deben ser mayores que cero<br>";
            }
            if (TxtOtroSi.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles otrosí contrato que excede capacidad contratada<br>";
            else
            {
                if (Convert.ToInt32(TxtOtroSi.Text) <= 0)
                    lsmensaje += "Los Días hábiles otrosí contrato que excede capacidad contratada deben ser mayores que cero<br>";
            }
            if (txtInvPub.Text == "")
                lsmensaje += "Debe ingresar los días hábiles trimestre de negociación para carga de invitación pública a los remitentes interesados en capacidad asociada a ampliación<br>";
            else
            {
                if (Convert.ToInt32(txtInvPub.Text) <= 0)
                    lsmensaje += "Los Días hábiles días hábiles trimestre de negociación para carga de invitación pública a los remitentes interesados en capacidad asociada a ampliación deben ser mayores que cero<br>";
            }
            if (txtCapAmp.Text == "")
                lsmensaje += "Debe ingresar los días hábiles del tercer mes del trimestre de negociación  para declarar las capacidades de ampliación solicitadas por remitente<br>";
            else
            {
                if (Convert.ToInt32(txtCapAmp.Text) <= 0)
                    lsmensaje += "Los Días hábiles del tercer mes del trimestre de negociación  para declarar las capacidades de ampliación solicitadas por remitente deben ser mayores que cero<br>";
            }
            if (txtPubAmp.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles del tercer mes del trimestre de negociación  para publicar las capacidades de ampliación solicitadas por remitente<br>";
            else
            {
                if (Convert.ToInt32(txtPubAmp.Text) <= 0)
                    lsmensaje += "Los Días hábiles del tercer mes del trimestre de negociación  para publicar las capacidades de ampliación solicitadas por remitente deben ser mayores que cero<br>";
            }
            if (txtCntDisp.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles previos  a finalización de semana para cargar cantidades  disponibles para la venta<br>";
            else
            {
                if (Convert.ToInt32(txtCntDisp.Text) <= 0)
                    lsmensaje += "Los Días hábiles previos  a finalización de semana para cargar cantidades  disponibles para la venta deben ser mayores que cero<br>";
            }
            if (txtPubDisp.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles previos  a finalización de semana para publicación de cantidades  disponibles para la venta<br>";
            else
            {
                if (Convert.ToInt32(txtPubDisp.Text) <= 0)
                    lsmensaje += "Los Días hábiles previos  a finalización de semana para publicación de cantidades  disponibles para la venta deben ser mayores que cero<br>";
            }

            if (txtPubPag.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles del trimestre de negociación para publicar la CMMP de proyectos PAG<br>";
            else
            {
                if (Convert.ToInt32(txtPubPag.Text) <= 0)
                    lsmensaje += "Los Días hábiles del trimestre de negociación para publicar la CMMP de proyectos PAG deben ser mayores que cero<br>";
            }
            if (txtRemPag.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles previos a la finalización del segundo mes del trimestre de negociación para cargar solicitudes de remitentes PAG<br>";
            else
            {
                if (Convert.ToInt32(txtRemPag.Text) <= 0)
                    lsmensaje += "Los Días hábiles previos a la finalización del segundo mes del trimestre de negociación para cargar solicitudes de remitentes PAG deben ser mayores que cero<br>";
            }
            if (txtAsigPag.Text == "")
                lsmensaje += "Debe Días hábiles del tercer mes del trimestre de negociación para asignar las solicitudes ce los remitentes de proyectos PAG<br>";
            else
            {
                if (Convert.ToInt32(txtAsigPag.Text) <= 0)
                    lsmensaje += "Los Días hábiles del tercer mes del trimestre de negociación para asignar las solicitudes ce los remitentes de proyectos PAG deben ser mayores que cero<br>";
            }
            if (txtPubAsigPag.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles del tercer mes del trimestre de negociación para  publicar las asignaciones de las solicitudes ce los remitentes de proyectos PAG<br>";
            else
            {
                if (Convert.ToInt32(txtPubAsigPag.Text) <= 0)
                    lsmensaje += "Los Días hábiles del tercer mes del trimestre de negociación para  publicar las asignaciones de las solicitudes ce los remitentes de proyectos PAG deben ser mayores que cero<br>";
            }
            if (txtRegPag.Text == "")
                lsmensaje += "Debe ingresar los Días hábiles previos a la finalización del trimestre de negociación para registrar los contratos PAG resultantes de la asignación<br>";
            else
            {
                if (Convert.ToInt32(txtRegPag.Text) <= 0)
                    lsmensaje += "Los Días hábiles previos a la finalización del trimestre de negociación para registrar los contratos PAG resultantes de la asignación deben ser mayores que cero<br>";
            }
            if (txtIaeCop.Text == "")
                lsmensaje += "Debe ingresar el Promedio simple del ingreso anual en pesos de la remuneración del proyecto del PAG<br>";
            else
            {
                try
                {
                    if (Convert.ToDecimal(txtIaeCop.Text) <= 0)
                        lsmensaje += "El Promedio simple del ingreso anual en pesos de la remuneración del proyecto del PAG debe ser mayor que cero<br>";
                    ldValor = Convert.ToDecimal(txtIaeCop.Text.Trim());
                    string sOferta = ldValor.ToString();
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 4)
                            lsmensaje += "El Promedio simple del ingreso anual en pesos de la remuneración del proyecto del PAG tiene mas de 3 decimales<br>";
                }
                catch (Exception)
                {
                    lsmensaje += "Valor inválido en el Promedio simple del ingreso anual en pesos de la remuneración del proyecto del PAG<br>";
                }
            }
            if (txtIaeUsd.Text == "")
                lsmensaje += "Debe ingresar el Promedio simple del ingreso anual en dólares de la remuneración del proyecto del PAG<br>";
            else
            {
                try
                {
                    if (Convert.ToDecimal(txtIaeCop.Text) <= 0)
                        lsmensaje += "El Promedio simple del ingreso anual en dólares de la remuneración del proyecto del PAG debe ser mayor que cero<br>";
                    ldValor = Convert.ToDecimal(txtIaeCop.Text.Trim());
                    string sOferta = ldValor.ToString();
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 4)
                            lsmensaje += "El Promedio simple del ingreso anual en dólares de la remuneración del proyecto del PAG tiene mas de 3 decimales<br>";
                }
                catch (Exception)
                {
                    lsmensaje += "Valor inválido en el Promedio simple del ingreso anual en dólares de la remuneración del proyecto del PAG<br>";
                }
            }
            //////////////////////////////////////////////////////////
            /// Validaciones Campos nuevos Req. 005-2021 20210120 ////
            //////////////////////////////////////////////////////////
            if (txtDiaIniComDecla.Text == "")
                lsmensaje += "Debe ingresar Día hábil inicial desde el comienzo del trimestre de negociación para hacer la declaración de los comercializadores<br>";
            else
            {
                if (Convert.ToInt32(txtDiaIniComDecla.Text) <= 0)
                    lsmensaje += "El Día hábil inicial desde el comienzo del trimestre de negociación para hacer la declaración de los comercializadores debe ser mayor que cero<br>";
            }
            if (txtDiaFinComDecla.Text == "")
                lsmensaje += "Debe ingresar Día hábil final desde el comienzo del trimestre de negociación para hacer la declaración de los comercializadores<br>";
            else
            {
                if (Convert.ToInt32(txtDiaFinComDecla.Text) <= 0)
                    lsmensaje += "El Día hábil final desde el comienzo del trimestre de negociación para hacer la declaración de los comercializadores debe ser mayor que cero<br>";
            }
            if (txtDiaDesPubDecla.Text == "")
                lsmensaje += "Debe ingresar Día hábil desde el comienzo del trimestre de negociación para publicar la declaración de los comercializadores<br>";
            else
            {
                if (Convert.ToInt32(txtDiaDesPubDecla.Text) <= 0)
                    lsmensaje += "El Día hábil desde el comienzo del trimestre de negociación para publicar la declaración de los comercializadores debe ser mayor que cero<br>";
            }
            if (txtDiaDesHabParti.Text == "")
                lsmensaje += "Debe ingresar Día hábil desde el comienzo del trimestre de negociación para habilitar los participantes de la subasta<br>";
            else
            {
                if (Convert.ToInt32(txtDiaDesHabParti.Text) <= 0)
                    lsmensaje += "El Día hábil desde el comienzo del trimestre de negociación para habilitar los participantes de la subasta debe ser mayor que cero<br>";
            }
            if (txtDiaDesDeclaOfer.Text == "")
                lsmensaje += "Debe ingresar Día hábil desde el comienzo del trimestre de negociación para hacer la declaración de oferta<br>";
            else
            {
                if (Convert.ToInt32(txtDiaDesDeclaOfer.Text) <= 0)
                    lsmensaje += "El Día hábil desde el comienzo del trimestre de negociación para hacer la declaración de oferta debe ser mayor que cero<br>";
            }
            if (txtHoraIniDecOfer.Text == "")
                lsmensaje += "Debe ingresar Hora inicial para hacer la declaración de la oferta<br>";
            if (txtHoraFinDecOfer.Text == "")
                lsmensaje += "Debe ingresar Hora final para hacer la declaración de la oferta<br>";
            else
            {
                try
                {
                    if (Convert.ToDateTime(txtHoraIniDecOfer.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDecOfer.Text.Trim()))
                        lsmensaje += "La Hora inicial para hacer la declaración de la oferta debe ser menor que la hora final. <br>";
                }
                catch (Exception)
                {
                    lsmensaje += "Formato de Hora para hacer la declaración de la oferta no válido <br>";
                }
            }
            if (txtDiaDesPubPrec.Text == "")
                lsmensaje += "Debe ingresar Día hábil desde el comienzo del trimestre de negociación para hacer la publicación de precios de reserva<br>";
            else
            {
                if (Convert.ToInt32(txtDiaDesPubPrec.Text) <= 0)
                    lsmensaje += "El Día hábil desde el comienzo del trimestre de negociación para hacer la publicación de precios de reserva debe ser mayor que cero<br>";
            }
            if (txtDiaDesAsigProy.Text == "")
                lsmensaje += "Debe ingresar Día hábil desde el comienzo del trimestre de negociación para hacer la asignación de proyectos PAG de demanda regulada<br>";
            else
            {
                if (Convert.ToInt32(txtDiaDesAsigProy.Text) <= 0)
                    lsmensaje += "El Día hábil desde el comienzo del trimestre de negociación para hacer la asignación de proyectos PAG de demanda regulada debe ser mayor que cero<br>";
            }
            if (txtCantMInPostSub.Text == "")
                lsmensaje += "Debe ingresar Cantidad mínima postura subasta<br>";
            else
            {
                if (Convert.ToInt32(txtCantMInPostSub.Text) <= 0)
                    lsmensaje += "La Cantidad mínima postura subasta debe ser mayor que cero<br>";
            }
            //20220106 rq001-22
            if (txtDiasTrimReg.Text == "")
                lsmensaje += "Debe ingresar los días hábiles del trimestre para el registro de los contratos<br>";
            else
            {
                if (Convert.ToInt32(txtDiasTrimReg.Text) <= 0)
                    lsmensaje += "Los días hábiles del trimestre para el registro de los contratos deben ser mayor que cero<br>";
            }
            //////////////////////////////////////////////////////////
            if (TxtObservacion.Text == "")
                lsmensaje += "Debe ingresar las observaciones<br>";

            if (lsmensaje == "")
            {
                lValorParametros[0] = TxtIngCmmp.Text;
                lValorParametros[1] = TxtIngIpat.Text;
                lValorParametros[2] = TxtPubIpat.Text;
                lValorParametros[3] = TxtSolRem.Text;
                lValorParametros[4] = TxtPubSolIni.Text;
                lValorParametros[5] = TxtModSolRem.Text;
                lValorParametros[6] = TxtPubSolFin.Text;
                lValorParametros[7] = TxtOtroSi.Text;
                lValorParametros[8] = txtInvPub.Text;
                lValorParametros[9] = txtCapAmp.Text;
                lValorParametros[10] = txtPubAmp.Text;
                lValorParametros[11] = txtCntDisp.Text;
                lValorParametros[12] = txtPubDisp.Text;
                lValorParametros[13] = txtPubPag.Text;
                lValorParametros[14] = txtRemPag.Text;
                lValorParametros[15] = txtAsigPag.Text;
                lValorParametros[16] = txtPubAsigPag.Text;
                lValorParametros[17] = txtRegPag.Text;
                lValorParametros[18] = txtIaeCop.Text;
                lValorParametros[19] = txtIaeUsd.Text;
                lValorParametros[20] = TxtObservacion.Text;
                // Campos nuevos Req. 005-2021 20210120
                lValorParametros[21] = txtDiaIniComDecla.Text;
                lValorParametros[22] = txtDiaFinComDecla.Text;
                lValorParametros[23] = txtDiaDesPubDecla.Text;
                lValorParametros[24] = txtDiaDesHabParti.Text;
                lValorParametros[25] = txtDiaDesDeclaOfer.Text;
                lValorParametros[26] = txtHoraIniDecOfer.Text;
                lValorParametros[27] = txtHoraFinDecOfer.Text;
                lValorParametros[28] = txtDiaDesPubPrec.Text;
                lValorParametros[29] = txtDiaDesAsigProy.Text;
                lValorParametros[30] = txtCantMInPostSub.Text;
                // Hasta Aqui
                lValorParametros[31] = txtDiasTrimReg.Text; //20220106 rq001-22
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosTrans", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    Toastr.Error(this, "Se presentó un Problema en la Actualizacion de los Parametros adicionales.!" + goInfo.mensaje_error.ToString());
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    Toastr.Error(this, "Parámetros actualizados correctamente");
                    Response.Redirect("~/WebForms/Parametros/frm_parametros.aspx?idParametros=3");
                }
            }
            else
            {
                Toastr.Error(this, lsmensaje);
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            Toastr.Error(this, ex.Message);
        }

    }
}