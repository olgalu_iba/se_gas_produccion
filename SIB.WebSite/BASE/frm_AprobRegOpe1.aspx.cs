﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;

public partial class BASE_frm_AprobRegOpe1 : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Primera Aprobación Registro de Operadores";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;
    String strRuta;
    string[] lsCarperta;
    string Carpeta;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo;
        strRuta = ConfigurationManager.AppSettings["RutaOpera"].ToString();
        lsCarperta = strRuta.Split('\\');
        Carpeta = lsCarperta[lsCarperta.Length - 2];
        //Titulo
        Master.Titulo = "Primera Aprobación Registro de Operadores";
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            CargarDatos();
        }
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Char };
        string[] lValorParametros = { "I" };

        try
        {
            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetOperador", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
            foreach (DataGridItem Grilla in this.dtgMaestro.Items)
            {

                if (Grilla.Cells[8].Text == "&nbsp;")
                    Grilla.Cells[6].Enabled = false;
                if (Grilla.Cells[9].Text == "&nbsp;")
                    Grilla.Cells[7].Enabled = false;
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }

    /// Nombre: btnAprobar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo que recorre toda la grilla y llama el procedimiento de verificacion para los items marcados
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected void btnAprobar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        int oContador;
        string[] lsNombreParametros = { "@P_codigo_operador", "@P_estado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char };
        Object[] lValorParametros = { "0", "P" };
        try
        {
            oContador = 0;
            lConexion.Abrir();
            foreach (DataGridItem Grilla in this.dtgMaestro.Items)
            {
                CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
                if (Checkbox.Checked == true)
                {
                    lValorParametros[0] = Grilla.Cells[1].Text;
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetAprobOpe", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        lblMensaje.Text += "Se presento un Problema al aprobar el operador " + lValorParametros[0] + ".! " + goInfo.mensaje_error.ToString() + "<br>";
                        lConexion.Cerrar();
                    }
                    oContador += 1;
                }
            }
            if (oContador > 0)
            {
                CargarDatos();
                lblMensaje.Text += "Se Aprobaron " + oContador.ToString() + " registros";
            }
            else
                lblMensaje.Text = "No se seleccionó ningun registro para aprobar";
        }
        catch (Exception E)
        {
            lblMensaje.Text = "Error en la aprobacion de los operadores seleccionados, " + E.Message.ToString();
        }
    }
    /// <summary>
    /// Nombre: dtgReqHab_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos Visualizacion de Imagen de la Grilla de Requisitos
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Comunicado")
        {
            try
            {
                string lsRuta = "../" + Carpeta + "/" + this.dtgMaestro.Items[e.Item.ItemIndex].Cells[8].Text.Replace(@"\", "/");
                string lsArchivo = strRuta + this.dtgMaestro.Items[e.Item.ItemIndex].Cells[8].Text.Trim();
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[8].Text.Trim() != "" && this.dtgMaestro.Items[e.Item.ItemIndex].Cells[8].Text.Trim() != "&nbsp;")
                {
                    if (File.Exists(lsArchivo))
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                    else
                        lblMensaje.Text = "El Archivo NO ha sido Cargado al Servidor.!";
                }
                else
                    lblMensaje.Text = "No han Realizado la Carga del Archivo de comunicado.!";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperae el Archivo de comunicado.! " + ex.Message.ToString();
            }
        }

        if (((LinkButton)e.CommandSource).Text == "Sarlaf")
        {
            try
            {
                string lsRuta = "../" + Carpeta + "/" + this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text.Replace(@"\", "/");
                string lsArchivo = strRuta + this.dtgMaestro.Items[e.Item.ItemIndex].Cells[8].Text.Trim();
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text.Trim() != "" && this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text.Trim() != "&nbsp;")
                {
                    if (File.Exists(lsArchivo))
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                    else
                        lblMensaje.Text = "El Archivo NO ha sido Cargado al Servidor.!";
                }
                else
                    lblMensaje.Text = "No han Realizado la Carga del Archivo de Sarlaf.!";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperae el Archivo de Sarlaf.! " + ex.Message.ToString();
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string lsOperador = "";
            try
            {
                lConexion.Abrir();
                string[] lsNombreParametros = { "@P_codigo_operador", "@P_estado", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.Int };
                Object[] lValorParametros = { "0", "I", "3" };

                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[11].Text.Trim() == "N")
                {
                    lsOperador = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text.Trim();
                    if (DelegadaBase.Servicios.ValidarExistencia("m_operador", " codigo_operador = " + lsOperador + " And estado = 'I' ", goInfo))
                    {
                        lValorParametros[0] = lsOperador;
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetOperador", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                        if (goInfo.mensaje_error != "")
                        {
                            lblMensaje.Text += "El Operador NO se puede Eliminar YA que tiene registros relacionados.!";
                            lConexion.Cerrar();
                        }
                        else
                        {
                            lblMensaje.Text = "Registro Eliminado Correctamente.!";
                            lConexion.Cerrar();
                            CargarDatos();
                        }
                    }
                    else
                        lblMensaje.Text = "El Operador NO puede ser eliminado porque Ya  fue Aprobado.!";
                }
                else
                    lblMensaje.Text += "El Operador NO se puede Eliminar YA que tiene registros relacionados.!";

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperae el Archivo de Sarlaf.! " + ex.Message.ToString();
            }
        }

    }
}