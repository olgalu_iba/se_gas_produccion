﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_TramoCostoUvlp : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Costos por Tramo 100% Uvlp";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarTramo(lConexion.gObjConexion, ddlBusTramo);
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and tipo_operador ='T' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddbBusOperador, "m_operador", " estado = 'A' and tipo_operador ='T'  order by razon_social", 0, 4);
            lConexion.Cerrar();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_tramo_costo_100");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[10].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[11].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoCos.Visible = false;
        LblCodigoCos.Visible = true;
        LblCodigoCos.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_tramo_costo_100", " codigo_costo_100= " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoCos.Text = lLector["codigo_costo_100"].ToString();
                        TxtCodigoCos.Text = lLector["codigo_costo_100"].ToString();
                        try
                        {
                            ddlOperador.SelectedValue = lLector["codigo_transportador"].ToString();
                            ddlOperador_SelectedIndexChanged(null, null);
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El operador del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tramo del registro no existe o esta inactivo<br>";
                        }
                        TxtCargoFij.Text = lLector["costo_fijo_100"].ToString();
                        TxtCargoVar.Text = lLector["costo_variable_100"].ToString();
                        TxtCargoAdm.Text = lLector["costo_adm"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoCos.Visible = false;
                        LblCodigoCos.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Costo " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_costo", "@P_codigo_tramo", "@P_codigo_operador" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0" };

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
            if (ddlBusTramo.SelectedValue != "0")
                lValorParametros[1] = ddlBusTramo.SelectedValue;
            if (ddbBusOperador.SelectedValue != "0")
                lValorParametros[2] = ddbBusOperador.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTramoCostoUvlp", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_costo", "@P_codigo_tramo", "@P_codigo_operador", "@P_cargo_fijo", "@P_cargo_variable", "@P_cargo_fijo_adm", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "", "1" };
        lblMensaje.Text = "";
        decimal ldValor = 0;
        int liValor = 0;
        try
        {
            if (ddlTramo.SelectedValue == "0" || ddlTramo.SelectedValue == "")
                lblMensaje.Text += "debe seleccionar el tramo<br>";
            if (ddlOperador.SelectedValue == "0")
                lblMensaje.Text += "debe seleccionar el operador<br>";
            if (VerificarExistencia(" codigo_tramo= " + ddlTramo.SelectedValue + " and codigo_transportador=" + ddlOperador.SelectedValue))
                lblMensaje.Text += " El costo para el tramo  ya esta definido <br>";
            if (TxtCargoFij.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Cargo Fijo Inversión (US$ / Kpcd - año)<br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtCargoFij.Text.Trim());
                    string sOferta = ldValor.ToString();
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 4)
                            lblMensaje.Text += "Valor Campo Cargo Fijo Inversión (US$ / Kpcd - año) tiene mas de 3 decimales<br>";

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Cargo Fijo Inversión (US$ / Kpcd - año)<br>";
                }
            }
            if (TxtCargoVar.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Cargo Variable Inversión (US$ / Kpc)<br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtCargoVar.Text.Trim());
                    string sOferta = ldValor.ToString();
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 4)
                            lblMensaje.Text += "Valor Campo Cargo Variable Inversión (US$ / Kpc) tiene mas de 3 decimales<br>";

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Cargo Variable Inversión (US$ / Kpc)<br>";
                }
            }
            if (TxtCargoAdm.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Cargo Fijo Administración ($ / Kpcd - año)<br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCargoAdm.Text.Trim());

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Cargo Fijo Administración ($ / Kpcd - año)<br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = ddlTramo.SelectedValue;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = TxtCargoFij.Text;
                lValorParametros[4] = TxtCargoVar.Text;
                lValorParametros[5] = TxtCargoAdm.Text;
                lValorParametros[6] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTramoCostoUvlp", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación del costo del tramo.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_costo", "@P_cargo_fijo", "@P_cargo_variable", "@P_cargo_fijo_adm", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0", "0", "", "2" };
        lblMensaje.Text = "";
        decimal ldValor = 0;
        int liValor = 0;
        try
        {
            if (TxtCargoFij.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Cargo Fijo Inversión (US$ / Kpcd - año)<br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtCargoFij.Text.Trim());
                    string sOferta = ldValor.ToString();
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 4)
                            lblMensaje.Text += "Valor Campo Cargo Fijo Inversión (US$ / Kpcd - año) tiene mas de 3 decimales<br>";

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Cargo Fijo Inversión (US$ / Kpcd - año)<br>";
                }
            }
            if (TxtCargoVar.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Cargo Variable Inversión (US$ / Kpc)<br>";
            else
            {
                try
                {
                    ldValor = Convert.ToDecimal(TxtCargoVar.Text.Trim());
                    string sOferta = ldValor.ToString();
                    int iPos = sOferta.IndexOf(".");
                    if (iPos > 0)
                        if (sOferta.Length - iPos > 4)
                            lblMensaje.Text += "Valor Campo Cargo Variable Inversión (US$ / Kpc) tiene mas de 3 decimales<br>";

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Cargo Variable Inversión (US$ / Kpc)<br>";
                }
            }
            if (TxtCargoAdm.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Cargo Fijo Administración ($ / Kpcd - año)<br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtCargoAdm.Text.Trim());

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Cargo Fijo Administración ($ / Kpcd - año)<br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoCos.Text;
                lValorParametros[1] = TxtCargoFij.Text;
                lValorParametros[2] = TxtCargoVar.Text;
                lValorParametros[3] = TxtCargoAdm.Text;
                lValorParametros[4] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTramoCostoUvlp", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del costo del tramo.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoCos.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoCos.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoCos.Text != "")
            manejo_bloqueo("E", LblCodigoCos.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlTramo.Enabled = false;
            ddlOperador.Enabled = false;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_tramo_costo_100", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarTramo(SqlConnection lConn, DropDownList lDdl)
    {
        lDdl.Items.Clear();
        string[] lsNombreParametros = { "@P_codigo_trasportador" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { "0" };

        if (ddlOperador.SelectedValue != "")
            lValorParametros[0] = ddlOperador.SelectedValue;
        SqlDataReader lLector;
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConn, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {

                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(0).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_tramo_costo_100' and llave_registro='codigo_costo_100=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_costo_100=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_tramo_costo_100";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_tramo_costo_100", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Codigo Costo : " + TxtBusCodigo.Text;
            }
            if (ddlBusTramo.SelectedValue != "0")
            {
                lValorParametros[1] = ddlBusTramo.SelectedValue;
                lsParametros += " Tramo: " + ddlBusTramo.SelectedItem;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetTramoCostoUvlp&nombreParametros=@P_codigo_costo*@P_codigo_tramo*@P_codigo_operador&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_costo*codigo_tramo*desc_tramo*codigo_trasportador*nombre_trasportador*cargo_fijo*cargo_variable*desc_pozo_fin*numero_resolucion*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de costos por tramo 100% Uvlp&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_costo <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_tramo_costo_100&procedimiento=pa_ValidarExistencia&columnas=codigo_costo_100*codigo_tramo*codigo_transportador*costo_fijo_100*costo_variable_100*costo_adm*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlOperador.SelectedValue != "0")
        {
            lConexion.Abrir();
            ddlTramo.Items.Clear();
            LlenarTramo(lConexion.gObjConexion, ddlTramo);
            lConexion.Cerrar();
        }
    }
}