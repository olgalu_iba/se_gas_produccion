﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.Globalization;
using System.IO;

public partial class BASE_frm_conformacionRutas : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaCarga;
    String strRutaFTP;
    SqlDataReader lLector;

    //string fechaRueda;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BtnProcesar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        lConexion = new clConexion(goInfo);
        try
        {
            lConexion.Abrir();
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_SetRutaAut";
            lComando.CommandTimeout = 3600;
            lLector = lComando.ExecuteReader();
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    lblMensaje.Text = lblMensaje.Text + lLector["Mensaje"].ToString() + "<br>";
                }
            }
            else
            {
                lblMensaje.Text = "Proceso ejecutado Satisfactoriamente.!";
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Se presento un Problema Al ejecutar el proceso.! <br>" + ex.Message.ToString();
        }
    }
}
