﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace BASE
{
    // ReSharper disable once IdentifierTypo
    // ReSharper disable once InconsistentNaming
    public partial class frm_CostoEstampillaRuta : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Costos Estampilla por Ruta";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        private SqlDataReader lLector;
        private string gsTabla = "m_costo_estampa_rut";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;

            //Activacion de los Botones
            buttons.Inicializar(ruta: gsTabla);
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;
            buttons.SalirOnclick += imbSalir_Click;
            lConexion = new clConexion(goInfo);

            if (IsPostBack) return;
            //Establese los permisos del sistema

            //Titulo
            Master.Titulo = "Parámetros";
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and tipo_operador ='T' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and tipo_operador ='T'  order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusRuta, "m_ruta_snt", " estado = 'A' Order by descripcion ", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlRuta, "m_ruta_snt", " codigo_ruta = -1 ", 0, 4);
            lConexion.Cerrar();

            Inicializar();
            Listar();

        }

        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf, EnumBotones.Salir };
            // Activacion de los Botones
            buttons.Inicializar(gsTabla, botones: botones);
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];
                var lkbEliminar = (LinkButton)Grilla.FindControl("lnkEliminar");
                lkbEliminar.Visible = false;

                if (!lkbModificar.Visible && !lkbEliminar.Visible)
                    dtgMaestro.Columns[10].Visible = false;
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = lsTitulo;
            TxtCodigoCos.Visible = false;
            LblCodigoCos.Visible = true;
            LblCodigoCos.Text = "Automático"; 
            imbActualiza.Visible = false;
            imbCrear.Visible = true;
            ddlOperador.Enabled = true;
            ddlRuta.Enabled = true;
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            dtgMaestro.Visible = true;
            lblTitulo.Text = lsTitulo;
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " codigo_estampa_rut = " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCos.Text = lLector["codigo_estampa_rut"].ToString();
                            TxtCodigoCos.Text = lLector["codigo_estampa_rut"].ToString();
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_transportador"].ToString();
                                ddlOperador_SelectedIndexChanged(null, null);
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El Transportador del registro no existe o está inactivo<br>");
                            }
                            try
                            {
                                ddlRuta.SelectedValue = lLector["codigo_ruta"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("La Ruta del registro no existe o está inactivo<br>");
                            }
                            TxtCargoFijo.Text = lLector["costo_fijo_100"].ToString();
                            TxtCargoVariable.Text = lLector["costo_variable_100"].ToString();
                            TxtCargoAom.Text = lLector["costo_adm"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCos.Visible = false;
                            LblCodigoCos.Visible = true;
                            ddlOperador.Enabled = false;
                            ddlRuta.Enabled = false;

                            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. Código Estampilla Ruta " + modificar);

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_estampa_rut", "@P_codigo_transportador", "@P_codigo_ruta" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0" };

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[1] = ddlBusOperador.SelectedValue;
                if (ddlBusRuta.SelectedValue != "0")
                    lValorParametros[2] = ddlBusRuta.SelectedValue;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCostoEstampillaRuta", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: btnConsultar_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }
        /// <summary>
        /// Nombre: Validaciones
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para validar campos de Entrada
        ///              en el Boton Crear.
        /// </summary>
        /// <returns></returns>
        protected StringBuilder Validaciones(string indicador)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                decimal ldValor = 0;
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el Transportador<br>");
                if (ddlRuta.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar la Ruta<br>");
                if (TxtCargoFijo.Text.Trim() != "")
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoFijo.Text.Trim());
                        string sOferta = ldValor.ToString();
                        int iPos = sOferta.IndexOf(".");
                        if (iPos > 0)
                            if (sOferta.Length - iPos > 5)
                                lblMensaje.Append("El Cargo fijo:(USD/KPCD-año) tiene mas de 4 decimales<br>");

                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en el Cargo fijo:(USD/KPCD-año) <br>");
                    }
                }
                else
                    lblMensaje.Append("Debe Ingresar el Cargo fijo:(USD/KPCD-año)<br>");
                if (TxtCargoVariable.Text.Trim() != "")
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtCargoVariable.Text.Trim());
                        string sOferta = ldValor.ToString();
                        int iPos = sOferta.IndexOf(".");
                        if (iPos > 0)
                            if (sOferta.Length - iPos > 5)
                                lblMensaje.Append("El Cargo variable: (USD/KPC) tiene mas de 4 decimales<br>");

                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Valor Inválido en el Cargo variable: (USD/KPC) <br>");
                    }
                }
                else
                    lblMensaje.Append("Debe Ingresar el Cargo variable: (USD/KPC)<br>");
                if (TxtCargoAom.Text.Trim() == "")
                    lblMensaje.Append("Debe Ingresar el Cargo AOM: (COP/KPCD/A)<br>");
                if (indicador == "C")
                {
                    if (VerificarExistencia(" codigo_ruta= " + ddlRuta.SelectedValue))
                        lblMensaje.Append("Ya se definió el Costo de Estampilla para la ruta<br>");
                }
                else
                {
                    if (VerificarExistencia(" codigo_ruta= " + ddlRuta.SelectedValue + " And codigo_estampa_rut !=" + LblCodigoCos.Text))
                        lblMensaje.Append("Ya se definió el Costo de Estampilla para la ruta<br>");
                }

                return lblMensaje;
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Error en las Validaciones de la Información. " + ex.Message.ToString());
                return lblMensaje;
            }
        }
        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_estampa_rut", "@P_codigo_transportador", "@P_codigo_ruta", "@P_costo_fijo_100", "@P_costo_variable_100", "@P_costo_adm", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "", "1" };
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("C");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = ddlRuta.SelectedValue;
                    lValorParametros[3] = TxtCargoFijo.Text;
                    lValorParametros[4] = TxtCargoVariable.Text;
                    lValorParametros[5] = TxtCargoAom.Text;
                    lValorParametros[6] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEstampillaRuta", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación del Costo Estampilla por Ruta.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Modal.Cerrar(this, CrearRegistro.ID); //20200727
                        limpiarCampos();
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_estampa_rut", "@P_codigo_transportador", "@P_codigo_ruta", "@P_costo_fijo_100", "@P_costo_variable_100", "@P_costo_adm", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "", "2" };
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("M");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = LblCodigoCos.Text;
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = ddlRuta.SelectedValue;
                    lValorParametros[3] = TxtCargoFijo.Text;
                    lValorParametros[4] = TxtCargoVariable.Text;
                    lValorParametros[5] = TxtCargoAom.Text;
                    lValorParametros[6] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetEstampillaRuta", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Actualización del Costo Estampilla por Ruta.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCos.Text);
                        Toastr.Success(this, "El registro se actualizó con éxito!.");
                        Modal.Cerrar(this, CrearRegistro.ID);
                        limpiarCampos();
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCos.Text);
                Toastr.Error(this, ex.Message);
            }
        }
        /// <summary>
        /// Nombre: imbSalir_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoCos.Text != "")
                manejo_bloqueo("E", LblCodigoCos.Text);
            //Cierra el modal de Agregar
            //Modal.Cerrar(this, CrearRegistro.ID);
            //Listar();
            Response.Redirect("../WebForms/Parametros/frm_parametros.aspx?idParametros=3");
        }
        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName == "Modificar")
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                Modificar(lCodigoRegistro);
                imbActualiza.Visible = true;
                imbCrear.Visible = false;
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Enero 22 de 2021
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='" + gsTabla + "' and llave_registro='codigo_estampa_rut=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_estampa_rut=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = gsTabla;
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0", "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código Estampa: " + TxtBusCodigo.Text;
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusOperador.SelectedValue;
                    lsParametros += " Transportador: " + ddlBusOperador.SelectedItem.ToString();
                }
                if (ddlBusRuta.SelectedValue != "0")
                {
                    lValorParametros[2] = ddlBusRuta.SelectedValue;
                    lsParametros += " Ruta: " + ddlBusRuta.SelectedItem.ToString();
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetCostoEstampillaRuta&nombreParametros=@P_codigo_estampa_rut*@P_codigo_transportador*@P_codigo_ruta&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_estampa_rut*codigo_transportador*nombre_operador*codigo_ruta*desc_ruta*costo_fijo_100*costo_variable_100*costo_adm*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Costos Estampilla por Ruta&TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_estampa_rut <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=" + gsTabla + "&procedimiento=pa_ValidarExistencia&columnas=codigo_estampa_rut*codigo_transportador*codigo_ruta*costo_fijo_100*costo_variable_100*costo_adm*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// </summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="liIndiceLlave"></param>
        /// <param name="liIndiceDescripcion"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Listar();
        }
        /// <summary>
        /// Nombre: ddlOperador_SelectedIndexChanged
        /// Fecha: Enero 22 de 2021
        /// Creador: German Eduardo Guarnizo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOperador.SelectedValue != "0")
            {
                ddlRuta.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlRuta, "m_ruta_snt", " codigo_trasportador ="+ddlOperador.SelectedValue + " and estado = 'A' Order by descripcion ", 0, 4);
                lConexion.Cerrar();
            }
            else
            {
                ddlRuta.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlRuta, "m_ruta_snt", " codigo_ruta = -1 ", 0, 4);
                lConexion.Cerrar();
            }
        }
        /// <summary>
        /// Nombre: limpiarCampos
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para limpiar campos
        /// </summary>
        protected void limpiarCampos()
        {
            ddlOperador.SelectedValue = "0";
            ddlRuta.SelectedValue = "0";
            TxtCargoAom.Text = "";
            TxtCargoFijo.Text = "";
            TxtCargoVariable.Text = "";
        }
        /// <summary>
        /// Nombre: imbCancelar_Click
        /// Fecha: Enero 25 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para Cancelar la creacion o modificacion de registros
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCancelar_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoCos.Text != "")
                manejo_bloqueo("E", LblCodigoCos.Text);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, CrearRegistro.ID);
            Listar();
        }
    }
}