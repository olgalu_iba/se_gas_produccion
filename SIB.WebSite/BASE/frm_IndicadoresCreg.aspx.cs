﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;

public partial class BASE_frm_IndicadoresCreg : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Indicadores CREG";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    string sRutaArc = ConfigurationManager.AppSettings["RutaIMG"].ToString();

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_indicadores_creg");
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[8].Visible = (Boolean)permisos["UPDATE"];  //20170705 rq025-17 indicadores MP fase III
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    imbActualiza.Visible = true;
                    TxtCodigoIndicador.Visible = false;
                    LblCodigoIndicador.Visible = true;
                    ddlTipoMercado.Enabled = false;
                    ddlDestinoRueda.Enabled = false;
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //// Activo los Campos de las Formulas de acuerdo al indicador Req. 009-17 Indicadores Fase II 20170307 ///
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                    switch (LblCodigoIndicador.Text)
                    {
                        //case "1":
                        //case "3":
                        //    TrFr01.Visible = true;
                        //    TrFr02.Visible = true;
                        //    TrFr03.Visible = true;
                        //    TrFr04.Visible = false;
                        //    break;
                        case "2":
                            TrFr01.Visible = true;
                            TrFr02.Visible = true;
                            TrFr03.Visible = true;
                            TrFr04.Visible = true;
                            break;
                        case "4":
                        case "5":
                        case "8":
                        case "10":
                            TrFr01.Visible = true;
                            TrFr02.Visible = false;
                            TrFr03.Visible = true;
                            TrFr04.Visible = true;
                            break;
                        case "1":
                        case "3":
                        case "6":
                        case "7":
                        case "9":
                        case "11":
                        case "12":
                        case "13":
                        case "14":
                        case "15":
                        case "16":
                        case "17":
                        case "18":
                        case "19":
                        case "20":
                        case "21":
                        case "22":
                            TrFr01.Visible = true;
                            TrFr02.Visible = false;
                            TrFr03.Visible = true;
                            TrFr04.Visible = false;
                            break;
                        case "23":
                            TrFr01.Visible = true;
                            TrFr02.Visible = false;
                            TrFr03.Visible = false;
                            TrFr04.Visible = false;
                            break;
                    }
                    switch (LblCodigoIndicador.Text)
                    {
                        case "7":
                        case "8":
                        case "9":
                        case "10":
                        case "11":
                        case "12":
                        case "13":
                        case "14":
                        case "15":
                        case "17":
                        case "18":
                        case "21":
                        case "23":
                            trFechaIniCont.Visible = true;
                            trFechaFinCont.Visible = true;
                            break;
                        default:
                            trFechaIniCont.Visible = false;
                            trFechaFinCont.Visible = false;
                            TxtFechaIniCont.Text = "";
                            TxtFechaFinCont.Text = "";
                            break;
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Indicador " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_indicador", "@P_descripcion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "" };
        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
            if (TxtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetIndicadorCreg", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_indicador", "@P_descripcion", "@P_fecha_publicacion_prev", "@P_fecha_publicacion_defi", "@P_accion",
                                        "@P_ruta_archivo_formula","@P_numerador_1","@P_numerador_2", // Campos nuevos Req. 009-17 Indicadores Fase II 20170307 
                                        "@P_denominador_1","@P_denominador_2","@P_periodicidad", "@P_fecha_cont_ini", "@P_fecha_cont_fin", // Campos nuevos Req. 009-17 Indicadores Fase II 20170307 
                                        "@P_tipo_publicacion"  //20170705 rq025-17 indicadres MP fase III
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, // Campos nuevos Req. 009-17 Indicadores Fase II 20170307
                                        SqlDbType.VarChar //20170705 rq025-17 indicadres MP fase III
                                      };
        string[] lValorParametros = { LblCodigoIndicador.Text, "", "", "", "2",
                                      "", "", "", "", "", "", "","", // Campos nuevos Req. 009-17 Indicadores Fase II 20170307
                                      ddlPublica.SelectedValue //20170705 rq025-17 indicadres MP fase III
                                    };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        try
        {

            if (VerificarExistencia(" descripcion = '" + this.TxtDescripcion.Text.Trim() + "' And codigo_indicador != " + LblCodigoIndicador.Text))
                lblMensaje.Text += " La Descripcion del Indicador  YA existe " + TxtDescripcion.Text + " <br>";
            if (TxtFechaPubPrev.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaPubPrev.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor inválido en la Fecha de Publicación Previa <br>";
                }
            }
            else
                lblMensaje.Text += " Debe Ingresar la Fecha de Publicación Previa <br>";
            if (TxtFechaPubDefi.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaPubDefi.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor inválido en la Fecha de Publicación Definitiva <br>";
                }
            }
            else
                lblMensaje.Text += " Debe Ingresar la Fecha de Publicación Definitiva <br>";
            if (lblMensaje.Text == "")
            {
                if (ldFechaI > ldFechaF)
                    lblMensaje.Text += " La fecha de publicación Previa debe ser menor o igual que la fecha de Publicación Definitiva <br>";
            }
            if (trFechaIniCont.Visible == true)
            {
                if (TxtFechaIniCont.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFechaI = Convert.ToDateTime(TxtFechaIniCont.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += " Valor inválido en la Fecha inicial de selección de contratos <br>";
                    }
                }
                else
                    lblMensaje.Text += " Debe Ingresar la Fecha inicial de selección de contratos<br>";
                if (TxtFechaFinCont.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFechaF = Convert.ToDateTime(TxtFechaFinCont.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += " Valor inválido en la Fecha final de selección de contratos <br>";
                    }
                }
                else
                    lblMensaje.Text += " Debe Ingresar la Fecha final de selección de contratos<br>";
                if (lblMensaje.Text == "")
                {
                    if (ldFechaI > ldFechaF)
                        lblMensaje.Text += " La fecha inicial de selección de contratos debe ser menor o igual que la fecha final <br>";
                }
            }
            ////////////////////////////////////////////////////////////////////
            /// Nuevas Validaciones Req. 009-17 Indicadores Fase II 20170307 ///
            ////////////////////////////////////////////////////////////////////
            if (hdfNomArchivo.Value.Trim() == "")
            {
                if (FuArchivo.FileName.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Seleccionar el Archivo de la Fórmula.<br>";
                else
                {
                    if (File.Exists(sRutaArc + FuArchivo.FileName))
                        lblMensaje.Text += "El archivo seleccionado Ya existe en el servidor.<br>";
                    else
                        hdfNomArchivo.Value = FuArchivo.FileName;
                }
            }
            else
            {
                if (FuArchivo.FileName.Trim().Length > 0)
                {
                    if (File.Exists(sRutaArc + FuArchivo.FileName))
                        lblMensaje.Text += "El archivo seleccionado Ya existe en el servidor.<br>";
                    else
                        hdfNomArchivo.Value = FuArchivo.FileName;
                }
            }
            if (TxtPeriodicidad.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe ingresar la periocidad.<br>";
            switch (LblCodigoIndicador.Text)
            {

                //if (TxtNumeraUno.Text.Trim().Length <= 0)
                //    lblMensaje.Text += "Debe ingresar el numerador uno.<br>";
                //if (TxtNumeraDos.Text.Trim().Length <= 0)
                //    lblMensaje.Text += "Debe ingresar el numerador dos.<br>";
                //if (TxtDenominaUno.Text.Trim().Length <= 0)
                //    lblMensaje.Text += "Debe ingresar el denominador uno.<br>";
                //break;
                case "2":
                    if (TxtNumeraUno.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el numerador uno.<br>";
                    if (TxtNumeraDos.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el numerador dos.<br>";
                    if (TxtDenominaUno.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el denominador uno.<br>";
                    if (TxtDenominaDos.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el denominador dos.<br>";
                    break;
                case "4":
                case "5":
                case "8":
                case "10":
                    if (TxtNumeraUno.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el numerador uno.<br>";
                    if (TxtDenominaUno.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el denominador uno.<br>";
                    if (TxtDenominaDos.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el denominador dos.<br>";
                    break;
                case "1":
                case "3":
                case "6":
                case "7":
                case "9":
                case "11":
                case "12":
                case "13":
                case "14":
                case "15":
                case "16":
                case "17":
                case "18":
                case "19":
                case "20":
                case "21":
                case "22":
                    if (TxtNumeraUno.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el numerador uno.<br>";
                    if (TxtDenominaUno.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el denominador uno.<br>";
                    break;
                case "23":
                    if (TxtNumeraUno.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe ingresar el numerador uno.<br>";
                    break;
            }
            ////////////////////////////////////////////////////////////////////
            if (lblMensaje.Text == "")
            {
                if (FuArchivo.FileName.Trim().Length > 0)
                    FuArchivo.SaveAs(sRutaArc + FuArchivo.FileName);

                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = TxtFechaPubPrev.Text.Trim();
                lValorParametros[3] = TxtFechaPubDefi.Text.Trim();
                //////////////////////////////////////////////////////////////
                /// Campos nuevos Req. 009-17 Indicadores Fase II 20170307 /// 
                //////////////////////////////////////////////////////////////
                lValorParametros[5] = hdfNomArchivo.Value;
                lValorParametros[6] = TxtNumeraUno.Text.Trim();
                lValorParametros[7] = TxtNumeraDos.Text.Trim();
                lValorParametros[8] = TxtDenominaUno.Text.Trim();
                lValorParametros[9] = TxtDenominaDos.Text.Trim();
                lValorParametros[10] = TxtPeriodicidad.Text.Trim(); 
                lValorParametros[11] = TxtFechaIniCont.Text.Trim();
                lValorParametros[12] = TxtFechaFinCont.Text.Trim();
                //////////////////////////////////////////////////////////////
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetIndicadorCreg", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del Indicador.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoIndicador.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoIndicador.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoIndicador.Text != "")
            manejo_bloqueo("E", LblCodigoIndicador.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
        LblCodigoIndicador.Text = lCodigoRegistro;

        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            TxtDescripcion.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
            ddlTipoMercado.SelectedValue = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text;  //20170705 rq025-17 indicadores MP fase III
            ddlDestinoRueda.SelectedValue = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text;  //20170705 rq025-17 indicadores MP fase III
            TxtFechaPubPrev.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[4].Text; 
            TxtFechaPubDefi.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[5].Text;
            //////////////////////////////////////////////////////////////
            /// Campos Nuevos Req. 009-17 Indicadores Fase II 20170307 ///
            //////////////////////////////////////////////////////////////
            hdfNomArchivo.Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[11].Text.Replace("&nbsp;", "");  //20170705 rq025-17 indicadores MP fase III
            TxtPeriodicidad.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[16].Text.Replace("&nbsp;", ""); //20170705 rq025-17 indicadores MP fase III
            TxtNumeraUno.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[12].Text.Replace("&nbsp;", ""); //20170705 rq025-17 indicadores MP fase III
            TxtNumeraDos.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[13].Text.Replace("&nbsp;", ""); //20170705 rq025-17 indicadores MP fase III
            TxtDenominaUno.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[14].Text.Replace("&nbsp;", ""); //20170705 rq025-17 indicadores MP fase III
            TxtDenominaDos.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[15].Text.Replace("&nbsp;", ""); //20170705 rq025-17 indicadores MP fase III
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[18].Text == "&nbsp;") //20170705 rq025-17 indicadores MP fase III
                TxtFechaIniCont.Text = "";
            else
                TxtFechaIniCont.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[18].Text;  //20170705 rq025-17 indicadores MP fase III
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[19].Text == "&nbsp;")  //20170705 rq025-17 indicadores MP fase III
                TxtFechaFinCont.Text = "";
            else
                TxtFechaFinCont.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[19].Text;  //20170705 rq025-17 indicadores MP fase III
            ddlPublica.SelectedValue = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[20].Text;  //20170705 rq025-17 indicadores MP fase III
            //////////////////////////////////////////////////////////////
            Modificar(lCodigoRegistro);
        }
        ////////////////////////////////////////////////////////////////////
        /// Visualización Nueva Req. 009-17 Indicadores Fase II 20170307 ///
        ////////////////////////////////////////////////////////////////////
        if (((LinkButton)e.CommandSource).Text == "Ver")
        {
            try
            {
                string lsRuta = "../Images/" + this.dtgMaestro.Items[e.Item.ItemIndex].Cells[11].Text.Replace(@"\", "/");  //20170705 rq025-17 indicadores MP fase III
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[11].Text.Trim() != "&nbsp;" && this.dtgMaestro.Items[e.Item.ItemIndex].Cells[11].Text.Trim() != "") //20170705 rq025-17 indicadores MP fase III
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                else
                    lblMensaje.Text = "No han Realizado la Carga del Archivo.!";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Recuperae el Archivo.! " + ex.Message.ToString();
            }
        }
        ////////////////////////////////////////////////////////////////////
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_indicadores_creg", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_indicadores_creg' and llave_registro='codigo_indicador=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_indicador=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_indicadores_creg";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_indicadores_creg", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Código Indicador: " + TxtBusCodigo.Text;
            }
            if (TxtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
                lsParametros += " - Descripción: " + TxtBusDescripcion.Text.Trim();
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetIndicadorCreg&nombreParametros=@P_codigo_indicador*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=Producto*valor_negocio*dias_plazo*valor_tasa_venta*valor_tasa_compra*valor_tasa_cierre&titulo_informe=Listado de Indicadores Creg&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_indicador <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_indicadores_creg&procedimiento=pa_ValidarExistencia&columnas=codigo_indicador*descripcion*tipo_mercado*destino_rueda*fecha_publicacion_prev*fecha_publicacion_defi*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
}