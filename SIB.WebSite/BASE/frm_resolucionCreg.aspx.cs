﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;
using System.IO;
using System.Configuration;

namespace BASE
{
    // ReSharper disable once IdentifierTypo
    // ReSharper disable once InconsistentNaming
    public partial class frm_resolucionCreg : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Resolución CREG"; //20170929 rq048-17
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null; //20200727
        private SqlDataReader lLector;
        private string gsTabla = "m_resolucion_creg";

        public string FileUpload_Temp
        {
            get { return Session["FileUpload_Temp"] != null && !string.IsNullOrEmpty(Session["FileUpload_Temp"].ToString()) ? Session["FileUpload_Temp"].ToString() : string.Empty; }
            set { Session["FileUpload_Temp"] = value; }
        }
        public string ArchivoOriginal
        {
            get { return Session["ArchivoOriginal"] != null && !string.IsNullOrEmpty(Session["ArchivoOriginal"].ToString()) ? Session["ArchivoOriginal"].ToString() : string.Empty; }
            set { Session["ArchivoOriginal"] = value; }
        }
        public string ExtensionArch
        {
            get { return Session["ExtensionArch"] != null && !string.IsNullOrEmpty(Session["ExtensionArch"].ToString()) ? Session["ExtensionArch"].ToString() : string.Empty; }
            set { Session["ExtensionArch"] = value; }
        }
        public string rutaCarga
        {
            get { return Session["rutaCarga"] != null && !string.IsNullOrEmpty(Session["rutaCarga"].ToString()) ? Session["rutaCarga"].ToString() : string.Empty; }
            set { Session["rutaCarga"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;

            rutaCarga = ConfigurationManager.AppSettings["RutaResolucion"].ToString();

            //Activacion de los Botones
            buttons.Inicializar(ruta: gsTabla);
            //buttons.CrearOnclick += btnNuevo; //20200924 ajsue componente
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.CrearOnclick += btnNuevo;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;
            lConexion = new clConexion(goInfo); //20200727

            if (IsPostBack) return;
            //Establese los permisos del sistema

            //Titulo
            Master.Titulo = "Parametros";
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlBusResolucion, "m_resolucion_creg", " 1=1 order by numero_resolucion", 0, 1);
            lConexion.Cerrar();

            Inicializar();
            Listar();
            EstablecerPermisosSistema();

        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }


        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf };

            // Activacion de los Botones
            buttons.Inicializar(gsTabla, botones: botones);
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];

            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = lsTitulo;
            TxtCodigoRes.Visible = false;
            LblCodigoRes.Visible = true;
            TxtResolucion.Enabled = true;
            TxtArticulo.Enabled = true;
            TxtFechaMax.Enabled = true;
            ddlEstado.Enabled = true;
            LblCodigoRes.Text = "Automático";
            imbActualiza.Visible = false;
            imbCrear.Visible = true;
            TxtResolucion.Text = "";
            TxtArticulo.Text = "";
            TxtFechaMax.Text = "";
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            dtgMaestro.Visible = true;
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " codigo_resolucion= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoRes.Text = lLector["codigo_resolucion"].ToString();
                            TxtCodigoRes.Text = lLector["codigo_resolucion"].ToString();
                            TxtResolucion.Text = lLector["numero_resolucion"].ToString();
                            TxtArticulo.Text = lLector["articulo"].ToString();
                            TxtFechaMax.Text = Convert.ToDateTime(lLector["fecha_maxima"].ToString()).ToString("yyyy/MM/dd");
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoRes.Visible = false;
                            LblCodigoRes.Visible = true;
                            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. Código Resolución " + modificar);

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_resolucion", "@P_numero_resolucion", "@P_fecha_max_inicial", "@P_fecha_max_final" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "", "" };
            DateTime ldFechaI = DateTime.Now;
            DateTime ldFechaF = DateTime.Now;
            bool Error = false;
            if (TxtFechaIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIni.Text);
                }
                catch (Exception)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Inicial. <br>");
                    Error = true;
                }
            }
            if (TxtFechaFin.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFin.Text);
                    if (TxtFechaIni.Text.Trim().Length > 0 && ldFechaF < ldFechaI)
                    {
                        Toastr.Warning(this, "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>");
                        Error = true;
                    }
                }
                catch (Exception)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Fecha Final. <br>");
                    Error = true;
                }
            }
            if (TxtFechaIni.Text.Trim().Length > 0 && TxtFechaFin.Text.Trim().Length > 0)
            {
                if (ldFechaI > ldFechaF)
                {
                    Toastr.Warning(this, "La fecha inicial debe ser menor o igual que la fecha final. <br>");
                    Error = true;
                }
            }
            if (!Error)
            {
                try
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (ddlBusResolucion.SelectedValue != "0")
                        lValorParametros[1] = ddlBusResolucion.SelectedItem.ToString();
                    if (TxtFechaIni.Text.Trim().Length > 0)
                    {
                        lValorParametros[2] = TxtFechaIni.Text;
                        if (TxtFechaFin.Text.Trim().Length > 0)
                            lValorParametros[3] = TxtFechaFin.Text;
                        else
                            lValorParametros[3] = TxtFechaIni.Text;
                    }

                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetResolucionCreg", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_resolucion", "@P_numero_resolucion", "@P_articulo", "@P_fecha_maxima", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "", "", "", "1" };
            var lblMensaje = new StringBuilder();
            try
            {
                if (TxtResolucion.Text == "")
                    lblMensaje.Append("Debe ingresar el número de la resolución<br>");
                if (TxtArticulo.Text == "")
                    lblMensaje.Append("Debe ingresar el artículo de la resolución<br>");
                if (lblMensaje.ToString() == "")
                {
                    if (VerificarExistencia(" numero_resolucion= '" + TxtResolucion.Text + "' and articulo = '" + TxtArticulo.Text + "'"))
                        lblMensaje.Append("Ya se definió al resolución y el artículo<br>");
                }
                if (TxtFechaMax.Text == "")
                    lblMensaje.Append("Debe digitar la fecha máxima de implementación<br>");
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaMax.Text);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Fecha máxima de implementación no válida<br>");
                    }
                }

                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = TxtResolucion.Text;
                    lValorParametros[2] = TxtArticulo.Text;
                    lValorParametros[3] = TxtFechaMax.Text;
                    lValorParametros[4] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetResolucionCreg", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación de la resolución.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Modal.Cerrar(this, CrearRegistro.ID);
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_resolucion", "@P_numero_resolucion", "@P_articulo", "@P_fecha_maxima", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "", "", "", "2" };
            var lblMensaje = new StringBuilder();
            decimal ldValor = 0;

            try
            {
                if (TxtResolucion.Text == "")
                    lblMensaje.Append("Debe ingresar el número de la resolución<br>");
                if (TxtArticulo.Text == "")
                    lblMensaje.Append("Debe ingresar el artículo de la resolución<br>");
                if (lblMensaje.ToString() == "")
                {
                    if (VerificarExistencia(" numero_resolucion= '" + TxtResolucion.Text + "' and articulo = '" + TxtArticulo.Text + "' and codigo_resolucion <> " + LblCodigoRes.Text))
                        lblMensaje.Append("Ya se definió al resolución y el artículo<br>");
                }
                if (TxtFechaMax.Text == "")
                    lblMensaje.Append("Debe digitar la fecha máxima de implementación<br>");
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaMax.Text);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Fecha máxima de implementación no válida<br>");
                    }
                }

                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = LblCodigoRes.Text;
                    lValorParametros[1] = TxtResolucion.Text;
                    lValorParametros[2] = TxtArticulo.Text;
                    lValorParametros[3] = TxtFechaMax.Text;
                    lValorParametros[4] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetResolucionCreg", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Actualización de la resolución.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoRes.Text);
                        Toastr.Success(this, "El registro se actualizó con éxito!.");
                        Modal.Cerrar(this, CrearRegistro.ID);
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoRes.Text);
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoRes.Text != "")
                manejo_bloqueo("E", LblCodigoRes.Text);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, CrearRegistro.ID);
            Listar();
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName == "Modificar")
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                Modificar(lCodigoRegistro);
                imbActualiza.Visible = true;
                imbCrear.Visible = false;
            }
            if (e.CommandName == "Implementar")
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                ImplementarRes(lCodigoRegistro);
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='m_resolucion_creg' and llave_registro='codigo_resolucion=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_resolucion=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "m_resolucion_creg";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_resolucion_creg", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "", "", "" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código resolución: " + TxtBusCodigo.Text;
                }
                if (ddlBusResolucion.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusResolucion.SelectedItem.ToString();
                    lsParametros += " Número Resolución: " + ddlBusResolucion.SelectedItem.ToString();
                }
                if (TxtFechaIni.Text.Trim().Length > 0)
                {
                    lValorParametros[2] = TxtFechaIni.Text;
                    if (TxtFechaFin.Text.Trim().Length > 0)
                        lValorParametros[3] = TxtFechaFin.Text;
                    else
                        lValorParametros[3] = TxtFechaIni.Text;
                }

                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetResolucionCreg&nombreParametros=@P_codigo_resolucion*@P_numero_resolucion*@P_fecha_max_inicial*@P_fecha_max_final&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=codigo_resolucion*numero_resolucion*artiuclo*fecha_maxima*fecha_implementacion*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de resolución CREG&TituloParametros=" + lsParametros);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_resolucion <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_resolucion_creg&procedimiento=pa_ValidarExistencia&columnas=codigo_resolucion*numero_resolucion*articulo*fecha_maxima*fecha_implementacion*estado&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Listar();
        }
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelarImp_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (TxtCodResImp.Text != "")
                manejo_bloqueo("E", TxtCodResImp.Text);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, Implementar.ID);
            Listar();
        }
        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void ImplementarRes(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " codigo_resolucion= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            TxtCodResImp.Text = lLector["codigo_resolucion"].ToString();
                            TxtNumResImp.Text = lLector["numero_resolucion"].ToString();
                            TxtArticuloImp.Text = lLector["articulo"].ToString();
                            TxtFechaMaxImp.Text = Convert.ToDateTime(lLector["fecha_maxima"].ToString()).ToString("yyyy/MM/dd");
                            if (lLector["fecha_implementacion"].ToString() != "")
                                TxtFechaImp.Text = Convert.ToDateTime(lLector["fecha_implementacion"].ToString()).ToString("yyyy/MM/dd");
                            else
                                TxtFechaImp.Text = "";
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoRes.Visible = false;
                            LblCodigoRes.Visible = true;
                            listarDet();
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro porque está Bloqueado. Código Resolución " + modificar);

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, Implementar.ID, ImplementarInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActualizarImp_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_resolucion", "@P_fecha_implementacion", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { TxtCodResImp.Text, "", "3" };
            var lblMensaje = new StringBuilder();

            try
            {
                if (TxtFechaImp.Text == "")
                    lblMensaje.Append("Debe digitar la fecha real de implementación<br>");
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaImp.Text);
                    }
                    catch (Exception)
                    {
                        lblMensaje.Append("Fecha real de implementación no válida<br>");
                    }
                }
                if (dtgDetalle.Items.Count <= 0)
                    lblMensaje.Append("Debe cargar al menos una archivo de evidencia<br>");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = TxtFechaImp.Text;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetResolucionCreg", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Actualización de la resolución.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoRes.Text);
                        Toastr.Success(this, "El registro se actualizó con éxito!.");
                        Modal.Cerrar(this, Implementar.ID);
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoRes.Text);
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCargarEvi_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_resolucion", "@P_nombre_original", "@P_nombre_archivo", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "", "", "1" };
            var lblMensaje = new StringBuilder();

            try
            {
                if (string.IsNullOrEmpty(FileUpload_Temp))
                {
                    lblMensaje.Append("Debe seleccionr el archivo de evidencia<br>");
                }

                if (!(Directory.Exists(rutaCarga)))
                    lblMensaje.Append("No existe el directorio para grabar los archivos<br>");

                string lsNumero = "";
                string lsArchivo = "";
                int liCOnta = 0;
                while (lsArchivo == "" && liCOnta < 100)
                {
                    lsNumero = DateTime.Now.Millisecond.ToString();
                    lsArchivo = "Evidencia_" + TxtCodResImp.Text + DateTime.Now.ToString("_yyyy_MM_dd_") + liCOnta.ToString() + ExtensionArch;
                    if (File.Exists(rutaCarga + lsArchivo))
                    {
                        lsArchivo = "";
                    }
                    liCOnta++;
                }
                if (lsArchivo == "")
                    lblMensaje.Append("No Se puede cargar el archivo. Intente de nuevo<br>");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = TxtCodResImp.Text;
                    lValorParametros[1] = ArchivoOriginal;
                    lValorParametros[2] = lsArchivo;
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_setResolucionCregDet", lsNombreParametros, lTipoparametros, lValorParametros);
                    if (lLector.HasRows)
                    {
                        while (lLector.Read())
                            lblMensaje.Append(lLector["error"].ToString() + "<br>");
                    }
                    else
                    {
                        var fi1 = new FileInfo(FileUpload_Temp);
                        File.Copy(FileUpload_Temp, rutaCarga + lsArchivo, true);
                        fi1.Delete();

                        Toastr.Success(this, "Archivo creado correctamente");
                    }
                    if (lblMensaje.ToString() != "")
                        Toastr.Error(this, lblMensaje.ToString());
                    else
                    {
                        listarDet();
                    }

                }
                else
                    Toastr.Error(this, lblMensaje.ToString());

            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Error: " + ex.Message.ToString());
            }
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void listarDet()
        {
            string[] lsNombreParametros = { "@P_codigo_resolucion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { TxtCodResImp.Text };

            try
            {
                lConexion.Abrir();
                dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetResolucionCregDet", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgDetalle.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgDetalle_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName == "Eliminar")
            {
                string[] lsNombreParametros = { "@P_codigo_resolucion_det", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                string[] lValorParametros = { "0", "3" };
                var lblMensaje = new StringBuilder();

                try
                {
                    if (lblMensaje.ToString() == "")
                    {
                        lValorParametros[0] = this.dtgDetalle.Items[e.Item.ItemIndex].Cells[0].Text;
                        lConexion.Abrir();
                        if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetResolucionCregDet", lsNombreParametros, lTipoparametros, lValorParametros))
                        {
                            lblMensaje.Append("Se presentó un Problema en la eliminación de la evidencia.!");
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Success(this, "El registro se eliminó con éxito!.");
                            listarDet();
                        }
                    }

                    if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                        Toastr.Error(this, lblMensaje.ToString());
                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    Toastr.Error(this, ex.Message);
                }
            }
            if (e.CommandName == "Ver")
            {
                try
                {
                    string lsCarpetaAnt = "";
                    string[] lsCarperta;
                    lsCarperta = rutaCarga.Split('\\');
                    lsCarpetaAnt = lsCarperta[lsCarperta.Length - 2];
                    var lsRuta = "../" + lsCarpetaAnt + '/' + e.Item.Cells[2].Text.Replace(@"\", "/");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "Error al visualizar el archivo. " + ex.Message);
                }
            }
        }

        /// <summary>
        /// Evento que guarda temporalmente el archivo original o modificado en el servidor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FileUpload_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                // Se valida si se cargo el archivo original  
                if (!string.IsNullOrEmpty(FuArchivo.FileName))
                {
                    Directory.CreateDirectory(rutaCarga + "temp\\");
                    string fileUploadPath;
                    fileUploadPath = Path.Combine(rutaCarga, "temp\\", FuArchivo.FileName);
                    ExtensionArch = Path.GetExtension(FuArchivo.FileName);
                    ArchivoOriginal = FuArchivo.FileName;
                    FuArchivo.SaveAs(fileUploadPath);
                    FileUpload_Temp = fileUploadPath;
                }

            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }


    }
}