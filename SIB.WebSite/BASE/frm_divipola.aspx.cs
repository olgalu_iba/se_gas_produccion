﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_divipola : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Codificación Divipola";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_divipola");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[8].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[9].Visible = (Boolean)permisos["DELETE"]; 
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigo.Visible = false;
        LblCodigo.Visible = true;
        LblCodigo.Text = "Automatico";
        trDescripcion.Visible = false;
        // Carga informacion de combos
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlDpto, "m_divipola", " codigo_ciudad ='0' and codigo_centro ='0' and estado ='A'  order by nombre_departamento ", 1, 2);
        LlenarControles(lConexion.gObjConexion, ddlCiudad, "m_divipola", " codigo_departamento ='0' and codigo_ciudad ='0' and codigo_centro ='0' and estado ='A'  order by nombre_departamento ", 3, 4);
        lConexion.Cerrar();

    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_divipola", " codigo_divipola = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigo.Text = lLector["codigo_divipola"].ToString();
                        TxtCodigo.Text = lLector["codigo_divipola"].ToString();
                        ddlDpto.Items.Clear();
                        ListItem lItemD = new ListItem();
                        lItemD.Value = lLector["codigo_departamento"].ToString();
                        lItemD.Text = lLector["nombre_departamento"].ToString();
                        ddlDpto.Items.Add(lItemD);
                        ddlDpto.SelectedValue = lLector["codigo_departamento"].ToString();
                        TxtNombreDepto.Text = lLector["nombre_departamento"].ToString();
                        ddlCiudad.Items.Clear();
                        ListItem lItemC = new ListItem();
                        lItemC.Value = lLector["codigo_ciudad"].ToString();
                        lItemC.Text = lLector["nombre_ciudad"].ToString();
                        ddlCiudad.Items.Add(lItemC);
                        ddlCiudad.SelectedValue = lLector["codigo_ciudad"].ToString();
                        TxtCodCiudad.Visible = false;
                        TxtNombreCiudad.Text = lLector["nombre_ciudad"].ToString();
                        TxtCodCentro.Text = lLector["codigo_centro"].ToString();
                        TxtNombreCentro.Text = lLector["nombre_centro"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigo.Visible = false;
                        LblCodigo.Visible = true;
                        trCiudad.Visible = false;
                        trDescripcion.Visible = true;
                        TxtDescripcion.Text = "";
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado";

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_divipola", "@P_nombre_departamento", "@P_nombre_ciudad" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "" , ""};

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
            if (TxtBusDepto.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDepto.Text.Trim();
            if (TxtBusCiudad.Text.Trim().Length > 0)
                lValorParametros[2] = TxtBusCiudad.Text.Trim();
            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDivipola", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_divipola", "@P_codigo_departamento", "@P_nombre_departamento", "@P_codigo_ciudad", "@P_nombre_ciudad","@P_codigo_centro", "@P_nombre_centro", "@P_estado", "@P_accion"};
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { "0", "", "", "", "", "", "", "", "1"};
        lblMensaje.Text = "";

        try
        {
            if (ddlDpto.SelectedValue =="0" && txtCodDpto.Text =="")
                lblMensaje.Text += "Debe Seleccionar el código del departamento<br>";
            else
                if (txtCodDpto.Text !="" && TxtNombreDepto.Text =="")
                    lblMensaje.Text += "Debe digitar la descripción del nuevo departamento <br>";
            if (txtCodDpto.Text != "")
            {
                if (this.txtCodDpto.Text.Trim().Length != 2)
                    lblMensaje.Text += "El código del departamento debe tener 2 dígitos<br>";
                if (VerificarExistencia(" codigo_departamento = '" + this.txtCodDpto.Text.Trim() + "' And codigo_ciudad = '0'"))
                    lblMensaje.Text += "El código del nuevo departamento ya existe<br>";
                if (TxtNombreDepto.Text != "")
                {
                    if (VerificarExistencia(" nombre_departamento  = '" + this.TxtNombreDepto.Text.Trim() + "' And codigo_ciudad = '0'"))
                        lblMensaje.Text += "El nombre del nuevo departamento ya existe<br>";
                }
            }

            if (ddlCiudad.SelectedValue == "0" && TxtCodCiudad.Text == "")
                lblMensaje.Text += "Debe seleccionar el código del municipio<br>";
            else
                if (TxtCodCiudad.Text != "" && TxtNombreCiudad.Text == "")
                    lblMensaje.Text += "Debe digitar la descripción del nuevo municipio<br>";
            if (TxtCodCiudad.Text != "")
            {
                if (this.TxtCodCiudad.Text.Trim().Length != 5)
                    lblMensaje.Text += "El código del municipio debe tener 5 dígitos<br>";
                else
                {
                    if (ddlDpto.SelectedValue != "0")
                    {
                        if (this.TxtCodCiudad.Text.Trim().Substring(0, 2) != ddlDpto.SelectedValue)
                            lblMensaje.Text += "Los 2 primeros dígitos del municipio deben corresponder al código del departamento<br>";
                    }
                    else
                    {
                        if (this.TxtCodCiudad.Text.Trim().Substring(0, 2) != txtCodDpto.Text.Trim())
                            lblMensaje.Text += "Los 2 primeros dígitos del municipio deben corresponder al código del departamento<br>";
                    }
                }
                if (VerificarExistencia(" codigo_ciudad = '" + this.TxtCodCiudad.Text.Trim() + "' and codigo_centro ='0'"))
                    lblMensaje.Text += "El código del municipio ya existe<br>";
            }

            if (TxtNombreCiudad.Text != "")
            {
                if (VerificarExistencia(" codigo_departamento = '"+ddlDpto.SelectedValue +"'  and nombre_ciudad = '" + this.TxtNombreCiudad.Text.Trim() + "' And codigo_centro = '0'"))
                    lblMensaje.Text += "El nombre del nuevo municipio ya existe<br>";
            }

            if (TxtCodCentro.Text == "" )
                lblMensaje.Text += "Debe digitar el código del centro poblado<br>";
            else
            {
                if (this.TxtCodCentro.Text.Trim().Length != 8)
                    lblMensaje.Text += "El código del centro poblado debe tener 8 dígitos<br>";
                else
                {
                    if (ddlCiudad.SelectedValue != "0")
                    {
                        if (this.TxtCodCentro.Text.Trim().Substring(0, 5) != ddlCiudad.SelectedValue)
                            lblMensaje.Text += "Los 5 primeros dígitos del centro poblado deben corresponder al código del municipio<br>";
                    }
                    else
                    {
                        if (this.TxtCodCentro.Text.Trim().Substring(0, 5) != TxtCodCiudad.Text.Trim())
                            lblMensaje.Text += "Los 5 primeros dígitos del centro poblado deben corresponder al código del municipio<br>";
                    }
                }
                if (VerificarExistencia(" codigo_centro = '" + this.TxtCodCentro.Text.Trim() + "'"))
                    lblMensaje.Text += "El código del centro poblado  ya existe<br>";
            }
            if (TxtNombreCentro.Text =="")
                lblMensaje.Text += "Debe digitar el nombre del centro poblado<br>";
            if (TxtNombreCentro.Text != "")
            {
                if (VerificarExistencia(" codigo_ciudad = '"+ddlCiudad.SelectedValue  +"' and nombre_centro= '" + this.TxtNombreCentro.Text.Trim() + "' And codigo_centro<> '0'"))
                    lblMensaje.Text += "El nombre del nuevo centro poblado ya existe<br>";
            }
            
            if (lblMensaje.Text == "")
            {
                if (ddlDpto.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlDpto.SelectedValue;
                    string[] lsDepto = ddlDpto.SelectedItem.ToString().Split('-');
                    lValorParametros[2] = lsDepto[1];
                }
                else
                {
                    lValorParametros[1] = txtCodDpto.Text;
                    lValorParametros[2] = TxtNombreDepto.Text;
                }
                if (ddlCiudad.SelectedValue != "0")
                {
                    lValorParametros[3] = ddlCiudad.SelectedValue;
                    string[] lsCiudad = ddlCiudad.SelectedItem.ToString().Split('-');
                    lValorParametros[4] = lsCiudad[1];
                }
                else
                {
                    lValorParametros[3] = TxtCodCiudad.Text;
                    lValorParametros[4] = TxtNombreCiudad.Text;
                }
                lValorParametros[5] = TxtCodCentro.Text;
                lValorParametros[6] = TxtNombreCentro.Text;
                lValorParametros[7] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetDivipola", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del municipio.!";
                    lConexion.Cerrar();
                }
                else
                    ddlDpto.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlDpto, "m_divipola", " codigo_ciudad ='0' and estado ='A'  order by nombre_departamento ", 1, 2);
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_divipola", "@P_descripcion", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int,  SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { "0",TxtDescripcion.Text,  "", "2" };
        lblMensaje.Text = "";
        try
        {
            if (TxtDescripcion.Text != "")
                if (VerificarExistencia1("m_divipola div, m_divipola div1", " div.codigo_divipola = " + LblCodigo.Text + " and (div.codigo_ciudad = '0' and div1.codigo_departamento <> div.codigo_departamento and div1.nombre_departamento = '" + TxtDescripcion.Text + "' or div.codigo_ciudad <>'0'  and div.codigo_centro ='0' and div1.codigo_departamento = div.codigo_departamento  and div1.codigo_ciudad <> div.codigo_ciudad and div1.nombre_ciudad= '" + TxtDescripcion.Text + "' or div.codigo_centro <>'0' and div1.codigo_ciudad = div.codigo_ciudad and  div1.codigo_centro <> div.codigo_centro and div1.nombre_centro= '" + TxtDescripcion.Text + "')"))
                    lblMensaje.Text += "La nueva descripción ya existe<br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigo.Text;
                lValorParametros[2] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetDivipola", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización del Municipio.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigo.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigo.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigo.Text != "")
            manejo_bloqueo("E", LblCodigo.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            txtCodDpto.Text = "";
            txtCodDpto.Visible = false;
            trDepto.Visible = false;
            trCiudad.Visible = false;
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlDpto.Visible = true;
            ddlDpto.Enabled = false;
            ddlCiudad.Visible = true;
            ddlCiudad.Enabled = false;
            TxtCodCentro.Enabled = false;
            TxtNombreCentro.Enabled = false;
            Modificar(lCodigoRegistro);
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            string lsCodigo = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            string[] lsNombreParametros = { "@P_codigo_divipola", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char };
            string[] lValorParametros = { lsCodigo, "3" };
            lblMensaje.Text = "";
            try
            {
                if (VerificarExistencia1("m_divipola div, m_divipola div1", " div.codigo_divipola =" + lsCodigo + " and div1.codigo_divipola <> " + lsCodigo + " and  div.codigo_departamento  =div1.codigo_departamento and case when div.codigo_ciudad ='0' then '0' else div1.codigo_ciudad end = div.codigo_ciudad and case when div.codigo_centro <> '0' then 'S' else 'N' end  = 'N'"))
                        lblMensaje.Text += "El registro a eliminar tiene registros derivados que deben ser eliminados previamente.<br>";

                if (lblMensaje.Text == "")
                {
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetDivipola", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Eliminación del Registro.!";
                        lConexion.Cerrar();
                    }
                    else
                        CargarDatos();
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                lblMensaje.Text = ex.Message;
            }

        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia1(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_divipola", lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='dbo.m_divipola' and llave_registro='codigo_divipola=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_divipola=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_divipola";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_divipola", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Código Divipola: " + TxtBusCodigo.Text;

            }
            if (TxtBusDepto.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDepto.Text.Trim();
                lsParametros += " - Depto: " + TxtBusDepto.Text;
            }
            if (TxtBusDepto.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDepto.Text.Trim();
                lsParametros += " - Municipio: " + TxtBusCiudad.Text;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetDivipola1&nombreParametros=@P_codigo_divipola*@P_nombre_departamento*@P_nombre_ciudad&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_divipola*codigo_departamento*codigo_ciudad*nombre_ciudad*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado Codificación Divipola&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_ciudad <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_divipola&procedimiento=pa_ValidarExistencia&columnas=codigo_divipola*codigo_departamento*nombre_departamentpo*codigo_ciudad*nombre_ciudad*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Metodo Nuevo para habilitar la captura del Campo en Declinación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtCodDpto_textChanged(object sender, EventArgs e)
    {
        if (txtCodDpto.Text == "")
        {
            ddlDpto.Visible = true;
            TxtNombreDepto.Text = "";
            trDepto.Visible = false;
        }
        else
        {
            ddlDpto.SelectedValue = "0";
            ddlDpto.Visible = false;
            trDepto.Visible = true;
            ddlCiudad.SelectedValue = "0";
            ddlCiudad.Visible = false;
        }
    }
    /// <summary>
    /// Metodo Nuevo para habilitar la captura del Campo en Declinación
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TxtCodCiudad_textChanged(object sender, EventArgs e)
    {
        if (TxtCodCiudad.Text == "")
        {
            ddlCiudad.Visible = true;
            TxtNombreCiudad.Text = "";
            trCiudad.Visible = false;
        }
        else
        {
            ddlCiudad.SelectedValue = "0";
            ddlCiudad.Visible = false;
            trCiudad.Visible = true;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlDpto_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCiudad.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlCiudad, "m_divipola", " codigo_departamento ='" + ddlDpto.SelectedValue + "' and  codigo_ciudad <>'0' and codigo_centro ='0' and estado ='A'  order by nombre_ciudad ", 3, 4);
        lConexion.Cerrar();
        if (ddlDpto.SelectedValue != "0")
        {
            txtCodDpto.Visible = false;
            trDepto.Visible = false;
        }
        else
        {
            txtCodDpto.Visible = true;
            trDepto.Visible = true;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCiudad_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCiudad.SelectedValue != "0")
        {
            TxtCodCiudad.Visible = false;
            trCiudad.Visible = false;
        }
        else
        {
            TxtCodCiudad.Visible = true;
            trCiudad.Visible = true;
        }
    }

}