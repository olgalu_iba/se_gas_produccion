﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_Operador.aspx.cs"
    Inherits="BASE_frm_Operador" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_Operador.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_Operador.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_Operador.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">Código Operador
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoOperador" runat="server" MaxLength="3"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtCodigoOperador" runat="server" TargetControlID="TxtCodigoOperador"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
                <asp:Label ID="LblCodigoOperador" runat="server" Visible="False"></asp:Label>
                <asp:HiddenField ID="hdfEstadoAct" runat="server" />
            </td>
            <td class="td1">Tipo Persona
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoPersona" runat="server" Width="200" OnSelectedIndexChanged="ddlTipoPersona_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="J" Text="Jurídico"></asp:ListItem>
                    <%--20170530 divipola--%>
                    <asp:ListItem Value="N" Text="Natural"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1"><%--20170530 divipola--%>
                Código Tipo Documento
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlCodigoTipoDocumento" runat="server" Width="200">
                    <asp:ListItem Value="1" Text="Nit"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Cédula de Ciudadanía"></asp:ListItem>
                    <%--20170530 divipola--%>
                </asp:DropDownList>
            </td>
            <td class="td1"><%--20170530 divipola--%>
                Número Identificación
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDocumento" runat="server" MaxLength="14"></asp:TextBox>
                <%--20170530 divipola--%>
                <asp:RequiredFieldValidator ID="RfvNoDocumento" runat="server" ErrorMessage="Debe Ingresar el Número de Documento"
                    ControlToValidate="TxtNoDocumento" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtNoDocumento" runat="server" TargetControlID="TxtNoDocumento"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
                <asp:TextBox ID="TxtDigitoVerificacion" runat="server" MaxLength="1" Width="20px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtDigitoVerificacion" runat="server" TargetControlID="TxtDigitoVerificacion"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrJuridico">
            <td class="td1" colspan="1"><%--20170530 divipola--%>
                Razón Social
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtRazonSocial" runat="server" MaxLength="100" Width="280px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtRazonSocial" runat="server" ErrorMessage="Debe Ingresar la razón social del operador"
                    ControlToValidate="TxtRazonSocial" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr runat="server" id="TrNatural" visible="false">
            <td class="td1" colspan="1">Nombres
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNombre" runat="server" MaxLength="100" Width="280px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtNombre" runat="server" ErrorMessage="Debe Ingresar el nombre del operador"
                    ControlToValidate="TxtNombre" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1" colspan="1">Apellidos
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtApellido" runat="server" MaxLength="100" Width="280px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtApellido" runat="server" ErrorMessage="Debe Ingresar el apellido del operador"
                    ControlToValidate="TxtApellido" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Tipo Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipo" runat="server" OnSelectedIndexChanged="ddlTipo_SelectedIndexChanged" AutoPostBack="true">
                    <%--20180228 rq032-17--%>
                </asp:DropDownList>
                <asp:CompareValidator ID="cvddlTipo" runat="server" ControlToValidate="ddlTipo" ValidationGroup="comi"
                    ErrorMessage="Debe seleccionar el tipo de operador" Operator="NotEqual" ValueToCompare="0">*</asp:CompareValidator>
            </td>
            <td class="td1">Posición
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPosicion" runat="server">
                    <asp:ListItem Value="P">Cuenta Propia</asp:ListItem>
                    <asp:ListItem Value="T">Cuenta de Terceros</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1"><%--20170530 divipola--%>
                Autorretenedor
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlAutoretenedor" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1"><%--20170530 divipola--%>
                Régimen
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlRegimen" runat="server">
                    <asp:ListItem Value="C">Común</asp:ListItem>
                    <%--20170530 divipola--%>
                    <asp:ListItem Value="S">Simplificado</asp:ListItem>
                    <asp:ListItem Value="E">Especial</asp:ListItem>
                    <%--20161121--%>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Contribuyente
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlContribuyente" runat="server">
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Gran Contribuyente
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlGranContribuyente" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Nombre del Representante
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNombreRepresentante" runat="server" MaxLength="50" Width="180px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtNombreRepresentante" runat="server" ErrorMessage="Debe Ingresar el nombre del representante"
                    ControlToValidate="TxtNombreRepresentante" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">E-Mail corporativo 1 <%--20180228 rq032-17--%>
            </td>
            <td class="td2"><%--20220602--%>
                <asp:TextBox ID="TxtEmail" runat="server" MaxLength="100" Width="180px"></asp:TextBox>
                <%--20170530 divipola--%>
                <asp:RegularExpressionValidator ID="RevEmail" ControlToValidate="TxtEmail" ValidationGroup="comi"
                    runat="server" ValidationExpression="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                    ErrorMessage="Valor inválido en el campo Mail 1"> * </asp:RegularExpressionValidator>
                <%--20180228 q032-17--%>
                <asp:RequiredFieldValidator ID="rfvTxtEmail" runat="server" ErrorMessage="Debe Ingresar el mail de contacto"
                    ControlToValidate="TxtEmail" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <%--20180228 rq032-17--%>
        <tr>
            <td class="td1">E-Mail corporativo 2
            </td>
            <td class="td2"> <%--20220602--%>
                <asp:TextBox ID="TxtEmail2" runat="server" MaxLength="100" Width="180px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtEmail2" ControlToValidate="TxtEmail2" ValidationGroup="comi"
                    runat="server" ValidationExpression="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                    ErrorMessage="Valor inválido en el campo Mail 2"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1">E-Mail corporativo 3
            </td>
            <td class="td2"><%--20220602--%>
                <asp:TextBox ID="TxtEmail3" runat="server" MaxLength="100" Width="180px"></asp:TextBox>
                <%--20170530 divipola--%>
                <asp:RegularExpressionValidator ID="revTxtEmail3" ControlToValidate="TxtEmail3" ValidationGroup="comi"
                    runat="server" ValidationExpression="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                    ErrorMessage="Valor inválido en el campo Mail 3"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1"><%--20170530 divipola--%>
                Dirección
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDireccion" runat="server" MaxLength="60" Width="180px"></asp:TextBox>
                <%--20170530 divipola--%>
                <asp:RequiredFieldValidator ID="RevDireccion" runat="server" ErrorMessage="Debe Ingresar la Dirección"
                    ControlToValidate="TxtDireccion" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1"><%--20170530 divipola--%>
                Teléfono
            </td>
            <td class="td2">
                <%--20180228 rq032-17--%>
                <asp:TextBox ID="TxtTelefono" runat="server" MaxLength="100"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtTelefono" runat="server" TargetControlID="TxtTelefono"
                    FilterType="Custom, Numbers" ValidChars="() ;"></cc1:FilteredTextBoxExtender>
                <%--20170530 divipola--%>
                <asp:RequiredFieldValidator ID="rfvTxtTelefono" runat="server" ErrorMessage="Debe Ingresar el teléfono de contacto"
                    ControlToValidate="TxtTelefono" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Teléfono 2 <%--20180228 rq032-17--%>
            </td>
            <td class="td2">
                <%--20180228 rq032-17--%>
                <asp:TextBox ID="TxtFax" runat="server" MaxLength="100"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtFax" runat="server" TargetControlID="TxtFax"
                    FilterType="Custom, Numbers" ValidChars="() ;"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">No. Celular
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCelular" runat="server" MaxLength="100"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBTxtCelular" runat="server" TargetControlID="TxtCelular"
                    FilterType="Custom, Numbers" ValidChars=";"><%--20180228 rq032-17--%>
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">Lugar Ubicación <%--20170530 divipola--%>
            </td>
            <td class="td2" colspan="3">
                <%--20170530--%>
                <%--<asp:DropDownList ID="ddlPais" runat="server" Width="200px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvDdlPais" runat="server" ControlToValidate="DdlPais"
                    ErrorMessage="Debe selecionar el pais">*</asp:RequiredFieldValidator>
                <cc1:CascadingDropDown ID="CddlPais" runat="server" Category="Pais" LoadingText="Cargando Paises..."
                    PromptText="Seleccione el Pais" ServiceMethod="GetListaPais" ServicePath="~/WebService/AutoComplete.asmx"
                    TargetControlID="ddlPais">
                </cc1:CascadingDropDown>--%>
                <%--20180228 rq032-17--%>
                <asp:DropDownList ID="ddlDepartamento" runat="server" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvddlDepartamento" runat="server" ControlToValidate="ddlDepartamento"
                    ErrorMessage="Debe selecionar el Departamento">*</asp:RequiredFieldValidator>
                <%--<cc1:CascadingDropDown ID="CddlDepartamento" runat="server" Category="Dpto" LoadingText="Cargando Departamentos..."
                    PromptText="Seleccione el Departamento" ServiceMethod="GetListaDepartamento"
                    ServicePath="~/WebService/AutoComplete.asmx" TargetControlID="ddlDepartamento">--%>
                <%--20170530 divipola--%>
                <%-- </cc1:CascadingDropDown>--%>
                <asp:DropDownList ID="ddlCiudad" runat="server" Width="200px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvddlCiudad" runat="server" ControlToValidate="ddlCiudad"
                    ErrorMessage="Debe selecionar la Ciudad">*</asp:RequiredFieldValidator>
                <%--<cc1:CascadingDropDown ID="CddlCiudad" runat="server" Category="Ciudad" LoadingText="Cargando Ciudades..."
                    PromptText="Seleccione la Ciudad" PromptValue="0" ServiceMethod="GetListaCiudad"
                    ServicePath="~/WebService/AutoComplete.asmx" TargetControlID="ddlCiudad" ParentControlID="ddlDepartamento">
                </cc1:CascadingDropDown>--%>
                <asp:CompareValidator ID="cvddlCiudad" runat="server" ControlToValidate="ddlCiudad"
                    ValidationGroup="comi" ErrorMessage="Debe seleccionar la ciudad" Operator="NotEqual"
                    ValueToCompare="0">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1"><%--20170530 divipola--%>
                Código Actividad
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoActividad" runat="server" MaxLength="10"></asp:TextBox>
                <%--20170530 divipola--%>
                <asp:RequiredFieldValidator ID="rfvTxtCodigoActividad" runat="server" ErrorMessage="Debe Ingresar el código de la actividad económica"
                    ControlToValidate="TxtCodigoActividad" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1"><%--20170530 divipola--%>
                Presta Servicios Públicos
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlServPublico" runat="server" OnSelectedIndexChanged="ddlServPublico_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="TrServPub" visible="false">
            <td class="td1">No. Registro RUPS
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoRegRups" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                <%--20180228 rq032-17--%>
                <cc1:FilteredTextBoxExtender ID="ftebTxtNoRegRups" runat="server" TargetControlID="TxtNoRegRups"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Fecha Elaboración Registro
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaRegRups" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="TrServPub1" visible="false">
            <td class="td1">Fecha Inicio Actividad
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniAct" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td2" colspan="2"></td>
        </tr>
         <%--20201207--%>
        <tr>
            <td class="td1">Banco Cuenta para pagos
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBanco" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
            </td>
            <td class="td1">Tipo Cuenta para pagos
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoCta" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="Corriente">Corriente</asp:ListItem>
                    <asp:ListItem Value="Ahorros">Ahorros</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
         <%--20201207--%>
        <tr>
            <td class="td1">Cuenta para pagos
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCuenta" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
            </td>
            <td class="td1" colspan="2">
            </td>
        </tr>
        <tr>
            <td class="td1">Dirección Web
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDireccionWeb" runat="server" MaxLength="200" Width="250px"></asp:TextBox>
            </td>
            <td class="td1">Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--20190411 rq019-19--%>
        <tr>
            <td class="td1">Tratamiento de datos
            </td>
            <td class="td2">
                <asp:Label ID="lblPolitica" runat="server">NO autorizado</asp:Label>
            </td>
            <td class="td2" colspan="2"></td>
        </tr>
       
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="HdnLoginCreacion" runat="server" />
    <asp:HiddenField ID="HdnFechaCreacion" runat="server" />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">Nit
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusNit" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusNit" runat="server" TargetControlID="TxtBusNit"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1"><%--20170530 divipola--%>
                Razón Social
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusRazonSocial" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Tipo Operador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusTipoOperador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="85%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgOperador" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgOperador_EditCommand" OnPageIndexChanged="dtgOperador_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="Codigo Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_persona" HeaderText="Tipo Persona" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="no_documento" HeaderText="Numero Documento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="Razon Social" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo" HeaderText="Tipo Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="direccion" HeaderText="Direccion" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="200px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <asp:BoundColumn DataField="tipo_operador" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                    <%--20170530--%>
                    <asp:DataGrid ID="dtgExcel" runat="server" AutoGenerateColumns="False" Visible="false">
                        <Columns>
                            <asp:BoundColumn DataField="codigo_operador" HeaderText="codigo_operador"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_doc" HeaderText="codigo_tipo_doc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_doc" HeaderText="desc_tipo_doc"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_persona" HeaderText="tipo_persona"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_persona" HeaderText="desc_tipo_persona"></asp:BoundColumn>
                            <asp:BoundColumn DataField="no_documento" HeaderText="no_documento"></asp:BoundColumn>
                            <asp:BoundColumn DataField="digito_verif" HeaderText="digito_verif"></asp:BoundColumn>
                            <asp:BoundColumn DataField="razon_social" HeaderText="razon_social"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombres" HeaderText="nombres"></asp:BoundColumn>
                            <asp:BoundColumn DataField="apellidos" HeaderText="apellidos"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_operador" HeaderText="nombre_operador"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_operador" HeaderText="tipo_operador"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo" HeaderText="desc_tipo"></asp:BoundColumn>
                            <asp:BoundColumn DataField="posicion" HeaderText="posicion"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_posicion" HeaderText="desc_posicion"></asp:BoundColumn>
                            <asp:BoundColumn DataField="autoretenedor" HeaderText="autoretenedor"></asp:BoundColumn>
                            <asp:BoundColumn DataField="regimen" HeaderText="regimen"></asp:BoundColumn>
                            <asp:BoundColumn DataField="contribuyente" HeaderText="contribuyente"></asp:BoundColumn>
                            <asp:BoundColumn DataField="gran_contribuyente" HeaderText="gran_contribuyente"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_representante" HeaderText="nombre_representante"></asp:BoundColumn>
                            <asp:BoundColumn DataField="no_celular" HeaderText="no_celular"></asp:BoundColumn>
                            <asp:BoundColumn DataField="e_mail" HeaderText="Mail 1 "></asp:BoundColumn>
                            <%--20180228 rq032-17--%>
                            <%--20180228 rq032-17--%>
                            <asp:BoundColumn DataField="e_mail2" HeaderText="Mail 2"></asp:BoundColumn>
                            <%--20180228 rq032-17--%>
                            <asp:BoundColumn DataField="e_mail3" HeaderText="Mail 3"></asp:BoundColumn>
                            <asp:BoundColumn DataField="direccion" HeaderText="direccion"></asp:BoundColumn>
                            <asp:BoundColumn DataField="telefono" HeaderText="telefono"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fax" HeaderText="telefono 2"></asp:BoundColumn>
                            <%--20180228 rq032-17--%>
                            <asp:BoundColumn DataField="codigo_departamento" HeaderText="codigo_departamento"></asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_departamento" HeaderText="nombre_departamento"></asp:BoundColumn>
                            <%--20170530 divipola--%>
                            <asp:BoundColumn DataField="codigo_ciudad" HeaderText="codigo_municipio"></asp:BoundColumn>
                            <%--20170530 divipola--%>
                            <asp:BoundColumn DataField="nombre_ciudad" HeaderText="nombre_municipio"></asp:BoundColumn>
                            <%--20170530 divipola--%>
                            <asp:BoundColumn DataField="codigo_actividad" HeaderText="codigo_actividad"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="estado"></asp:BoundColumn>
                            <asp:BoundColumn DataField="usuario_creacion" HeaderText="usuario_creacion"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_hora_creacion" HeaderText="fecha_hora_creacion"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="login_usuario"></asp:BoundColumn>
                            <asp:BoundColumn DataField="fecha_hora_actual" HeaderText="fecha_hora_actual"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ruta_archivo_comunicado" HeaderText="ruta_archivo_comunicado"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ruta_archivo_sarlaf" HeaderText="ruta_archivo_sarlaf"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tiene_usuario" HeaderText="tiene_usuario"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
