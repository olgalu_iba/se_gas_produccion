﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_divipola.aspx.cs"
    Inherits="BASE_frm_divipola" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_divipola.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_divipola.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_divipola.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Divipola
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigo" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigo" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Código Departamento
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlDpto" runat="server" OnSelectedIndexChanged= "ddlDpto_SelectedIndexChanged" autopostback ="true">
                </asp:DropDownList>
                <asp:TextBox ID="txtCodDpto" runat="server" MaxLength="2" Width="50px" OnTextChanged="txtCodDpto_textChanged" AutoPostBack="true"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTEBtxtCodDpto" runat="server" TargetControlID="txtCodDpto"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr id="trDepto" runat ="server" visible="false">
            <td class="td1" colspan="1">
                Nombre Departamento
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNombreDepto" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Código Municipio
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlCiudad" runat="server" OnSelectedIndexChanged= "ddlCiudad_SelectedIndexChanged" autopostback ="true">
                </asp:DropDownList>
                <asp:TextBox ID="TxtCodCiudad" runat="server" MaxLength="5" Width="50px" OnTextChanged="TxtCodCiudad_textChanged" AutoPostBack="true"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtCodCiudad" runat="server" TargetControlID="TxtCodCiudad"
                    FilterType="Custom, Numbers" >
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr id="trCiudad" runat ="server">
            <td class="td1" colspan="1">
                Nombre Municipio
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNombreCiudad" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Código Centro Poblado
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtCodCentro" runat="server" MaxLength="8" Width="50px" ></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FtevTxtCentro" runat="server" TargetControlID="TxtCodCentro"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr id="trCentro" runat ="server">
            <td class="td1" colspan="1">
                Nombre Centro Poblado
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNombreCentro" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr id="trDescripcion" runat ="server" visible ="false"> 
            <td class="td1" colspan="1">
                Nueva Descripción
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Código Divipola
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCodigo" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodigo" runat="server" TargetControlID="TxtBusCodigo"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Nombre Departamento
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusDepto" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Nombre Municipio
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCiudad" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%" >
        <tr>
            <td colspan="2" align="center" >
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_divipola" visible ="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_departamento" HeaderText="Cod Depto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_departamento" HeaderText="Departamento" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Cod Mcpio" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Municipio" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_centro" HeaderText="Cod Centro Poblado" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_centro" HeaderText="Centro Poblado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="estado" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>