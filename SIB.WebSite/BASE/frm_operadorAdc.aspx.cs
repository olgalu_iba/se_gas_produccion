﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace BASE
{
    // ReSharper disable once IdentifierTypo
    // ReSharper disable once InconsistentNaming
    public partial class frm_operadorAdc : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Datos adicionales de operadores para facturación"; //20170929 rq048-17
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null; //20200727
        private SqlDataReader lLector;
        private string gsTabla = "m_operador_adc";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;

            //Activacion de los Botones
            buttons.Inicializar(ruta: gsTabla);
            //buttons.CrearOnclick += btnNuevo; //20200924 ajsue componente
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.CrearOnclick += btnNuevo;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;
            lConexion = new clConexion(goInfo); //20200727

            if (IsPostBack) return;
            //Establese los permisos del sistema

            //Titulo
            Master.Titulo = "Datos Operadores Facturación";
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " tipo_operador in ('P', 'I') and estado ='A' and codigo_operador >0", 2, 4);
            lConexion.Cerrar();

            Inicializar();
            Listar();
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// <summary>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["no_documento"].ToString() + "-" + lLector["razon_social"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }



        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf };

            // Activacion de los Botones
            buttons.Inicializar(gsTabla, botones: botones);
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla );
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];
                
                if (!lkbModificar.Visible )
                    dtgMaestro.Columns[10].Visible = false;
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = lsTitulo;
            ddlOperador.Enabled = true;
            ddlOperador.SelectedValue = "0";
            TxtNombre.Text= "";
            TxtApellido.Text = "";
            TxtTrato.Text = "";
            TxtTrato2.Text = "";
            TxtCargo.Text = "";
            TxtMail.Text = "";
            ddlEstado.SelectedValue ="0";
            imbActualiza.Visible = false;
            imbCrear.Visible = true;
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            dtgMaestro.Visible = true;
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla , " nit_operador= '" + modificar + "'");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            try
                            {
                                ddlOperador.SelectedValue = modificar;
                            }
                            catch (Exception ex)
                            {
                            }
                            TxtNombre.Text = lLector["nombres"].ToString();
                            TxtApellido.Text = lLector["apellidos"].ToString();
                            TxtTrato.Text = lLector["trato"].ToString();
                            TxtTrato2.Text = lLector["trato2"].ToString();
                            TxtCargo.Text = lLector["cargo"].ToString();
                            TxtMail.Text = lLector["mail_facturacion"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            ddlOperador.Enabled = false;
                            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. Nit oeprador: " + modificar );

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_nit_operador", "@P_razon_social" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar , SqlDbType.VarChar };
            string[] lValorParametros = { "", ""};

            try
            {
                if (TxtBusNit.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusNit.Text.Trim();
                if (TxtBusRazon.Text != "")
                    lValorParametros[1] = TxtBusRazon.Text;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetOperadorAdc", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_nit_operador", "@P_nombres", "@P_apellidos", "@P_trato", "@P_trato2", "@P_cargo", "@P_mail_facturacion", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "", "", "", "", "", "", "", "", "1" };
            var lblMensaje = new StringBuilder();
            decimal ldValor = 0;
            try
            {
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el operador<br>");
                else
                {
                    if (VerificarExistencia(" nit_operador= '" + ddlOperador.SelectedValue + "'"))
                        lblMensaje.Append("Ya se definieron los datos adicionales para el operador<br>");
                }
                if (TxtNombre.Text == "")
                    lblMensaje.Append("Debe digitar el nombre<br>");
                if (TxtApellido.Text == "")
                    lblMensaje.Append("Debe digitar el apellido<br>");
                if (TxtTrato.Text == "")
                    lblMensaje.Append("Debe digitar el trato<br>");
                if (TxtTrato2.Text == "")
                    lblMensaje.Append("Debe digitar el trato 2<br>");
                if (TxtCargo.Text == "")
                    lblMensaje.Append("Debe digitar el cargo<br>");
                if (TxtMail.Text == "")
                    lblMensaje.Append("Debe digitar el mail<br>");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = ddlOperador.SelectedValue;
                    lValorParametros[1] = TxtNombre.Text;
                    lValorParametros[2] = TxtApellido.Text;
                    lValorParametros[3] = TxtTrato.Text;
                    lValorParametros[4] = TxtTrato2.Text;
                    lValorParametros[5] = TxtCargo.Text;
                    lValorParametros[6] = TxtMail.Text;
                    lValorParametros[7] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetOperadorAdc", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación de los datos del operador.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Success(this, "El registro se creó correctamente!.");
                        Modal.Cerrar(this, CrearRegistro.ID); 
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_nit_operador", "@P_nombres", "@P_apellidos", "@P_trato", "@P_trato2", "@P_cargo", "@P_mail_facturacion", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "", "", "", "", "", "", "", "", "2" };
            var lblMensaje = new StringBuilder();
            decimal ldValor = 0;
            try
            {
                if (TxtNombre.Text == "")
                    lblMensaje.Append("Debe digitar el nombre<br>");
                if (TxtApellido.Text == "")
                    lblMensaje.Append("Debe digitar el apellido<br>");
                if (TxtTrato.Text == "")
                    lblMensaje.Append("Debe digitar el trato<br>");
                if (TxtTrato2.Text == "")
                    lblMensaje.Append("Debe digitar el trato 2<br>");
                if (TxtCargo.Text == "")
                    lblMensaje.Append("Debe digitar el cargo<br>");
                if (TxtMail.Text == "")
                    lblMensaje.Append("Debe digitar el mail<br>");

                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = ddlOperador.SelectedValue;
                    lValorParametros[1] = TxtNombre.Text;
                    lValorParametros[2] = TxtApellido.Text;
                    lValorParametros[3] = TxtTrato.Text;
                    lValorParametros[4] = TxtTrato2.Text;
                    lValorParametros[5] = TxtCargo.Text;
                    lValorParametros[6] = TxtMail.Text;
                    lValorParametros[7] = ddlEstado.SelectedValue;

                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetOperadorAdc", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la actualización de los datos.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", ddlOperador.SelectedValue );
                        Toastr.Success(this, "El registro se actualizo con éxito!.");
                        Modal.Cerrar(this, CrearRegistro.ID);
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", ddlOperador.SelectedValue );
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (ddlOperador.SelectedValue != "0")
                manejo_bloqueo("E", ddlOperador.SelectedValue);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, CrearRegistro.ID);
            Listar();
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Modificar")
            {
                Modificar(this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text);
                imbActualiza.Visible = true;
                imbCrear.Visible = false;
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(gsTabla , lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='"+gsTabla +"' and llave_registro='nit_operador=" + lscodigo_registro +"'";
            string lsCondicion1 = "nit_operador=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = gsTabla ;
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla , lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "", ""};
            string lsParametros = "";

            try
            {
                if (TxtBusNit.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusNit.Text.Trim();
                    lsParametros += " Nit: " + TxtBusNit.Text;
                }
                if (TxtBusRazon.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusRazon.Text.Trim();
                    lsParametros += " Nit: " + TxtBusRazon.Text;
                }

                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetOperadorAdc&nombreParametros=@P_nit_operador*@P_razon_social&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1]  + "&columnas=nit_operador*razon_social*nombrs*apellidos*trato*trato2*cargo*mail_facturacion*estado*fecha_hora_actual&titulo_informe=Listado de datos adicionales de operadores&TituloParametros=" + lsParametros); 
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " estado = 'A'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla="+gsTabla + "&procedimiento=pa_ValidarExistencia&columnas=nit_operador*nombres*apellidos*trato*trato2*cargo*mail_facturacion*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Listar();
        }
    }
}