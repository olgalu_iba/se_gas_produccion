﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace BASE
{
    // ReSharper disable once IdentifierTypo
    // ReSharper disable once InconsistentNaming
    public partial class frm_homologaTramo : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Homologación de Tramos";
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null;
        private SqlDataReader lLector;
        private string gsTabla = "m_homologa_tramo";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;

            //Activacion de los Botones
            buttons.Inicializar(ruta: gsTabla);
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;
            buttons.SalirOnclick += imbSalir_Click;
            lConexion = new clConexion(goInfo);

            if (IsPostBack) return;
            //Establese los permisos del sistema

            //Titulo
            Master.Titulo = "Parámetros";
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and tipo_operador ='T' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador", " estado = 'A' and tipo_operador ='T'  order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlTramo, "m_tramo", " codigo_tramo = -1 ", 0, 10);
            lConexion.Cerrar();

            Inicializar();
            Listar();

        }

        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf, EnumBotones.Salir };
            // Activacion de los Botones
            buttons.Inicializar(gsTabla, botones: botones);
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];

                if (!lkbModificar.Visible )
                    dtgMaestro.Columns[7].Visible = false;
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = lsTitulo;
            TxtCodigoCos.Visible = false;
            LblCodigoCos.Visible = true;
            LblCodigoCos.Text = "Automático"; 
            imbActualiza.Visible = false;
            imbCrear.Visible = true;
            ddlOperador.Enabled = true;
            ddlTramo.Enabled = true;
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            dtgMaestro.CurrentPageIndex = 0;
            CargarDatos();
            dtgMaestro.Visible = true;
            lblTitulo.Text = lsTitulo;
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " codigo_homologa = " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigoCos.Text = lLector["codigo_homologa"].ToString();
                            TxtCodigoCos.Text = lLector["codigo_homologa"].ToString();
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                                ddlOperador_SelectedIndexChanged(null, null);
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El Transportador del registro no existe o está inactivo<br>");
                            }
                            try
                            {
                                ddlTramo.SelectedValue = lLector["codigo_tramo"].ToString();
                            }
                            catch (Exception)
                            {
                                lblMensaje.Append("El Tramo del registro no existe o está inactivo<br>");
                            }
                            TxtDescripcion.Text = lLector["descripcion_participante"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoCos.Visible = false;
                            LblCodigoCos.Visible = true;
                            ddlOperador.Enabled = true;
                            ddlTramo.Enabled = true;

                            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. Código Homologación " + modificar);

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_codigo_homologa", "@P_codigo_operador", "@P_descripcion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "" };

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[1] = ddlBusOperador.SelectedValue;
                if (TxtBusDescripcion.Text != "")
                    lValorParametros[2] = TxtBusDescripcion.Text;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTramoHomologa", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: btnConsultar_Click
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }
        /// <summary>
        /// Nombre: Validaciones
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para validar campos de Entrada
        ///              en el Boton Crear.
        /// </summary>
        /// <returns></returns>
        protected StringBuilder Validaciones(string indicador)
        {
            var lblMensaje = new StringBuilder();
            try
            {
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el Transportador<br>");
                if (TxtDescripcion.Text =="")
                    lblMensaje.Append("Debe digitar la descripción del participante<br>");
                if (ddlTramo.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el Tramo<br>");
                if (indicador == "C")
                {
                    if (VerificarExistencia(" descripcion_participante = '" + TxtDescripcion.Text + "'"))
                        lblMensaje.Append("Ya se homologó la descripción digitada<br>");
                }
                else
                {
                    if (VerificarExistencia(" descripcion_participante= '" + ddlTramo.SelectedValue + "' And codigo_homologa !=" + LblCodigoCos.Text))
                        lblMensaje.Append("Ya se homologó la descripción digitada<br>");
                }

                return lblMensaje;
            }
            catch (Exception ex)
            {
                lblMensaje.Append("Error en las Validaciones de la Información. " + ex.Message.ToString());
                return lblMensaje;
            }
        }
        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_homologa", "@P_codigo_operador", "@P_descripcion_participante", "@P_codigo_tramo", "@P_estado", "@P_accion"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int};
            string[] lValorParametros = { "0", "0", "", "0", "", "1"};
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("C");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = TxtDescripcion.Text;
                    lValorParametros[3] = ddlTramo.SelectedValue;
                    lValorParametros[4] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTramoHomologa", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación de la homologación del tramo.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Success(this, "El registro se creó con éxito!.");
                        Modal.Cerrar(this, CrearRegistro.ID); 
                        limpiarCampos();
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                Toastr.Warning(this,"Error al grabar el registro. "+ ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_homologa", "@P_codigo_operador", "@P_descripcion_participante", "@P_codigo_tramo", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "0", "", "0", "", "2" };
            var lblMensaje = new StringBuilder();
            try
            {
                lblMensaje = Validaciones("M");
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = LblCodigoCos.Text;
                    lValorParametros[1] = ddlOperador.SelectedValue;
                    lValorParametros[2] = TxtDescripcion.Text;
                    lValorParametros[3] = ddlTramo.SelectedValue;
                    lValorParametros[4] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTramoHomologa", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Actualización del Cargo Fijo Subasta Transporte.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigoCos.Text);
                        Toastr.Success(this, "El registro se actualizó con éxito!.");
                        Modal.Cerrar(this, CrearRegistro.ID);
                        limpiarCampos();
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigoCos.Text);
                Toastr.Warning(this,"Error al actualizar el registro. "+ ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbSalir_Click
        /// Fecha: Enero 22 de 2021
        /// Creador: German Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoCos.Text != "")
                manejo_bloqueo("E", LblCodigoCos.Text);
            //Cierra el modal de Agregar
            //Modal.Cerrar(this, CrearRegistro.ID);
            //Listar();
            Response.Redirect("../WebForms/Parametros/frm_parametros.aspx?idParametros=3");
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro = "";
            if (e.CommandName == "Modificar")
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                Modificar(lCodigoRegistro);
                imbActualiza.Visible = true;
                imbCrear.Visible = false;
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='" + gsTabla + "' and llave_registro='codigo_homologa=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_homologa=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = gsTabla;
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0", "" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código homologación: " + TxtBusCodigo.Text;
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusOperador.SelectedValue;
                    lsParametros += " Transportador: " + ddlBusOperador.SelectedItem.ToString();
                }
                if (TxtBusDescripcion.Text != "")
                {
                    lValorParametros[2] = TxtBusDescripcion.Text;
                    lsParametros += " Descripción: " + TxtBusDescripcion.Text;
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_getTramoHomologa&nombreParametros=@P_codigo_homologa*@P_codigo_operador*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_homologa*codigo_operador*razon_social*descripcion_participante*codigo_tramo*desc_tramo*estado&titulo_informe=Listado de tramos homologados&TituloParametros=" + lsParametros);
            }
            catch (Exception)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_homologa <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=" + gsTabla + "&procedimiento=pa_ValidarExistencia&columnas=codigo_homologa*codigo_operador*nombre_operador*descripcion_participante*codigo_tramo*descripcion_segas*estado&condicion=" + lsCondicion);
            }
            catch (Exception)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// </summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// <param name="lsTabla"></param>
        /// <param name="lsCondicion"></param>
        /// <param name="liIndiceLlave"></param>
        /// <param name="liIndiceDescripcion"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                if (lsTabla != "m_operador")
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                }
                else
                {
                    lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                    lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
                }
                lDdl.Items.Add(lItem1);

            }
            lLector.Dispose();
            lLector.Close();
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Listar();
        }
        /// <summary>
        /// Nombre: ddlOperador_SelectedIndexChanged
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlOperador.SelectedValue != "0")
            {
                ddlTramo.Items.Clear();
                lConexion.Abrir();
                LlenarControles1(lConexion.gObjConexion, ddlTramo, "m_tramo", " estado = 'A' ", 0, 10);
                lConexion.Cerrar();
            }
            else
            {
                ddlTramo.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlTramo, "m_tramo", " codigo_tramo=0 ", 0, 10);
                lConexion.Cerrar();
            }
        }
        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            string[] lsNombreParametros = { "@P_codigo_trasportador" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int };
            string[] lValorParametros = { ddlOperador.SelectedValue };
            if (ddlOperador.SelectedValue == "")
                lValorParametros[0] = "0";

            lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramo", lsNombreParametros, lTipoparametros, lValorParametros);
            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);
            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }
        /// <summary>
        /// Nombre: limpiarCampos
        /// Fecha: Enero 21 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para limpiar campos
        /// </summary>
        protected void limpiarCampos()
        {
            ddlOperador.SelectedValue = "0";
            ddlTramo.SelectedValue = "0";
            TxtDescripcion.Text = "";
        }
        /// <summary>
        /// Nombre: imbCancelar_Click
        /// Fecha: Enero 25 de 2021
        /// Creador: German Eduardo Guarnizo
        /// Metodo para Cancelar la creacion o modificacion de registros
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCancelar_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigoCos.Text != "")
                manejo_bloqueo("E", LblCodigoCos.Text);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, CrearRegistro.ID);
            Listar();
        }
    }
}