﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_AutorizacionContingencia : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Autorización Activación Contingencia";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            ////////////////////////////////////////////
            //// Ajuste COmbos Req. 006-17 20170303 ////
            ////////////////////////////////////////////
            //LlenarControles(lConexion.gObjConexion, DdlUsuario, "a_usuario", " estado ='A' And codigo_grupo_usuario = 40 order by nombre", 1, 4);
            //LlenarControles(lConexion.gObjConexion, DdlBusUsuario, "a_usuario", " estado ='A' And codigo_grupo_usuario = 40 order by nombre", 1, 4);
            LlenarControles(lConexion.gObjConexion, DdlUsuario, "a_usuario a, a_grupo_usuario b", " a.estado ='A' And a.codigo_grupo_usuario = b.codigo_grupo_usuario And b.habilitado_contingencia = 'S' order by nombre", 1, 4);
            LlenarControles(lConexion.gObjConexion, DdlBusUsuario, "a_usuario a, a_grupo_usuario b", " a.estado ='A' And a.codigo_grupo_usuario = b.codigo_grupo_usuario And b.habilitado_contingencia = 'S' order by nombre", 1, 4);
            ////////////////////////////////////////////
            lConexion.Cerrar();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_autorizado_contingencia");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[7].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[8].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoAutoriza.Visible = false;
        LblCodigoAutoriza.Visible = true;
        LblCodigoAutoriza.Text = "Automatico";
        DdlUsuario.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            DdlUsuario.Enabled = false;
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_autorizado_contingencia", " codigo_autoriza_contngencia = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoAutoriza.Text = lLector["codigo_autoriza_contngencia"].ToString();
                        TxtCodigoAutoriza.Text = lLector["codigo_autoriza_contngencia"].ToString();
                        try
                        {
                            DdlUsuario.SelectedValue = lLector["login_autorizado"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text = "El Usuario a Modificar NO pertenece al Perfil Operador BMC.!";

                        }
                        if (lblMensaje.Text == "")
                        {
                            TxtFechaInicial.Text = lLector["fecha_inicial"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial"].ToString().Substring(0, 2);
                            TxtFechaFinal.Text = lLector["fecha_final"].ToString().Substring(6, 4) + "/" + lLector["fecha_final"].ToString().Substring(3, 2) + "/" + lLector["fecha_final"].ToString().Substring(0, 2);
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigoAutoriza.Visible = false;
                            LblCodigoAutoriza.Visible = true;
                            DdlUsuario.Enabled = false;
                        }
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    if (lblMensaje.Text == "")
                    {
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Autorización " + modificar.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_autoriza_contngencia", "@P_login_autorizado" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0" };

        try
        {
            if (TxtBusCodigoCampo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigoCampo.Text.Trim();
            lValorParametros[1] = DdlBusUsuario.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetAutorizaContingencia", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_autoriza_contngencia", "@P_login_autorizado", "@P_fecha_inicial", "@P_fecha_final", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "", "", "", "1" };
        lblMensaje.Text = "";
        DateTime ldFecha;

        try
        {
            if (DdlUsuario.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el usuario<br>";
            if (VerificarExistencia(" login_autorizado = '" + DdlUsuario.SelectedValue + "' "))
                lblMensaje.Text += " Ya se ha creado la autorización del Usuario Seleccionado<br>";
            if (TxtFechaInicial.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Fecha Inicial<br>";
                }
            }
            else
                lblMensaje.Text += " Debe Ingresar la  Fecha Inicial<br>";
            if (TxtFechaFinal.Text.Trim().Length > 0)
            {

                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFinal.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtFechaInicial.Text.Trim()))
                        lblMensaje.Text += " La Fecha FInal debe ser mayor que la fecha inicial<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Fecha Final<br>";
                }
            }
            else
                lblMensaje.Text += " Debe Ingresar la Fecha Final<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = DdlUsuario.SelectedValue;
                lValorParametros[2] = TxtFechaInicial.Text.Trim();
                lValorParametros[3] = TxtFechaFinal.Text.Trim();
                lValorParametros[4] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetAutorizaContingencia", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación del registro.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_autoriza_contngencia", "@P_login_autorizado", "@P_fecha_inicial", "@P_fecha_final", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "", "", "", "2" };
        lblMensaje.Text = "";
        DateTime ldFecha;
        try
        {
            if (TxtFechaInicial.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaInicial.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Fecha Inicial<br>";
                }
            }
            else
                lblMensaje.Text += " Debe Ingresar la  Fecha Inicial<br>";
            if (TxtFechaFinal.Text.Trim().Length > 0)
            {

                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFinal.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtFechaInicial.Text.Trim()))
                        lblMensaje.Text += " La Fecha FInal debe ser mayor que la fecha inicial<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Fecha Final<br>";
                }
            }
            else
                lblMensaje.Text += " Debe Ingresar la Fecha Final<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoAutoriza.Text;
                lValorParametros[1] = DdlUsuario.SelectedValue;
                lValorParametros[2] = TxtFechaInicial.Text.Trim();
                lValorParametros[3] = TxtFechaFinal.Text.Trim();
                lValorParametros[4] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetAutorizaContingencia", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del registro.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoAutoriza.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoAutoriza.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoAutoriza.Text != "")
            manejo_bloqueo("E", LblCodigoAutoriza.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_autorizado_contingencia", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='dbo.m_autorizado_contingencia' and llave_registro='codigo_autoriza_contngencia=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_autoriza_contngencia=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_autorizado_contingencia";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_autorizado_contingencia", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigoCampo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigoCampo.Text.Trim();
                lsParametros += " Codigo Autorizacion : " + TxtBusCodigoCampo.Text;

            }
            if (DdlBusUsuario.SelectedValue != "0")
            {
                lValorParametros[1] = DdlBusUsuario.SelectedValue;
                lsParametros += " - Usuario: " + DdlBusUsuario.SelectedItem;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetAutorizaContingencia&nombreParametros=@P_codigo_autoriza_contngencia*@P_login_autorizado&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_campo_periodo*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado Autorizacion Contingencia&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_autoriza_contngencia<> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_autorizado_contingencia&procedimiento=pa_ValidarExistencia&columnas=codigo_autoriza_contngencia*login_autorizado*fecha_inicial*fecha_final*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
}