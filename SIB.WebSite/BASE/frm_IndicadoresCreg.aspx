﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_IndicadoresCreg.aspx.cs"
    Inherits="BASE_frm_IndicadoresCreg" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_IndicadoresCreg.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_IndicadoresCreg.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">Código Indicador
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoIndicador" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoIndicador" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">Descripción
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtDescripcion" ControlToValidate="TxtDescripcion"
                    runat="server" ValidationExpression="^([0-9a-zA-Z]{3})([0-9a-zA-Z .-_&])*$" ErrorMessage="Valor muy corto en el Campo Descripcion"
                    ValidationGroup="comi"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Tipo Mercado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoMercado" runat="server" Width="250px">
                    <asp:ListItem Value="P">Primario</asp:ListItem>
                    <asp:ListItem Value="S">Secundario</asp:ListItem>
                    <asp:ListItem Value="O">Otras Transacciones del Mercado Mayorista</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Destino Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlDestinoRueda" runat="server">
                    <asp:ListItem Value="G">Suministro de Gas</asp:ListItem>
                    <asp:ListItem Value="T">Capacidad de Transporte</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">Fecha de Publicación Previa
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFechaPubPrev" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">Fecha de Publicación Definitiva
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFechaPubDefi" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr id="trFechaIniCont" runat="server">
            <td class="td1" colspan="1">Fecha inicial de negociación para selección de contratos
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFechaIniCont" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr id="trFechaFinCont" runat="server">
            <td class="td1" colspan="1">Fecha final de negociación para selección de contratos
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtFechaFinCont" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <%--Campos Nuevos Req. 009-17 Indicadores Fase II 20170307--%>
        <tr>
            <td class="td1" colspan="1">Archivo Fórmula Indicador
            </td>
            <td class="td2" colspan="1">
                <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" Width="500px" />
                <asp:HiddenField ID="hdfNomArchivo" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">Periodicidad
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtPeriodicidad" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="TrFr01" visible="false">
            <td class="td1" colspan="1">Numerador Uno
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNumeraUno" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="TrFr02" visible="false">
            <td class="td1" colspan="1">Numerador Dos
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNumeraDos" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="TrFr03" visible="false">
            <td class="td1" colspan="1">Denominador Uno
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDenominaUno" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="TrFr04" visible="false">
            <td class="td1" colspan="1">Denominador Dos
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDenominaDos" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <%--hasta Aqui--%>
        <%--20170705 rq025-17--%>
        <tr>
            <td class="td1" colspan="1">Tipo Publicación
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlPublica" runat="server">
                    <asp:ListItem Value="A">Anual</asp:ListItem>
                    <asp:ListItem Value="M">Mensual</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">Código Indicador
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCodigo" runat="server" MaxLength="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Descripción
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusDescripcion" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <%--0--%><asp:BoundColumn DataField="codigo_indicador" HeaderText="Código Indicador"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--1--%><asp:BoundColumn DataField="descripcion" HeaderText="Descripción" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--2--%><asp:BoundColumn DataField="desc_tipo_mercado" HeaderText="Tipo Mercado"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <%--3--%><asp:BoundColumn DataField="desc_destino_rueda" HeaderText="Destino Rueda"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--4--%><asp:BoundColumn DataField="fecha_publicacion_prev" HeaderText="Fecha Publicación Previa"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--5--%><asp:BoundColumn DataField="fecha_publicacion_defi" HeaderText="Fecha Publicación Definitiva"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--20170705 rq025-17--%>
                            <%--6--%><asp:BoundColumn DataField="desc_tipo_pub" HeaderText="Tipo Publicación"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--7--%><asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px"></asp:BoundColumn>
                            <%--8--%><asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            <%--9--%><asp:BoundColumn DataField="tipo_mercado" Visible="false"></asp:BoundColumn>
                            <%--10--%><asp:BoundColumn DataField="destino_rueda" Visible="false"></asp:BoundColumn>
                            <%--Campos nuevos Req. 009-17 Indicadores Fase II 20170307--%>
                            <%--11--%><asp:BoundColumn DataField="ruta_archivo_formula" Visible="false"></asp:BoundColumn>
                            <%--12--%><asp:BoundColumn DataField="numerador_1" Visible="false"></asp:BoundColumn>
                            <%--13--%><asp:BoundColumn DataField="numerador_2" Visible="false"></asp:BoundColumn>
                            <%--14--%><asp:BoundColumn DataField="denominador_1" Visible="false"></asp:BoundColumn>
                            <%--15--%><asp:BoundColumn DataField="denominador_2" Visible="false"></asp:BoundColumn>
                            <%--16--%><asp:BoundColumn DataField="periodicidad" Visible="false"></asp:BoundColumn>
                            <%--17--%><asp:EditCommandColumn HeaderText="Ver Archivo" EditText="Ver" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            <%--18--%><asp:BoundColumn DataField="fecha_cont_ini" Visible="false"></asp:BoundColumn>
                            <%--19--%><asp:BoundColumn DataField="fecha_cont_fin" Visible="false"></asp:BoundColumn>
                            <%--20170705 rq025-17--%>
                            <%--20--%><asp:BoundColumn DataField="tipo_publicacion" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
