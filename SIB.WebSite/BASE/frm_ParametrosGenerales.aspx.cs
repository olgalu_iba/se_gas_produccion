﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;

public partial class BASE_frm_ParametrosGenerales : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parametros Generales";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string sRutaArc = ConfigurationManager.AppSettings["RutaIMG"].ToString();
    string gsTabla = "m_parametros_generales";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                /// Camo nuevo Reg.58 20160701
                ddlMenuVisualizaArchivos.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlMenuVisualizaArchivos, "a_menu", " estado = 'A' And visualizar = 'S' And ruta = '' order by menu", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlMesfact, "m_mes", " 1=1", 0, 1);
                lConexion.Cerrar();
                /////
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    //private void EstablecerPermisosSistema()
    //{
    //    Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
    //    imbCrear.Visible = (Boolean)permisos["INSERT"];
    //    imbActualiza.Visible = (Boolean)permisos["UPDATE"];
    //}
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtValIncOferta.Text = lLector["valor_incremento_ofertas"].ToString();
                    TxtCantMinNego.Text = lLector["cantidad_minima_negociacion"].ToString();
                    TxtValHumExcDema.Text = lLector["valor_umbral_exceso_demanda"].ToString();
                    TxtValVigMinis.Text = lLector["valor_vig_ptdv_cidv_min_minas"].ToString();
                    TxtDiasCont.Text = lLector["dias_caducidad_contrasena"].ToString();
                    ddlMostAgreTod.SelectedValue = lLector["mostrar_agresores_todos"].ToString();
                    ddlMostAgreOfe.SelectedValue = lLector["mostrar_agresores_oferentes"].ToString();
                    TxtDiaMaxTras.Text = lLector["dias_max_reg_capacidad_tra"].ToString();
                    hdfNomArchivo.Value = lLector["archivo_firma"].ToString();
                    TxtNomFirma.Text = lLector["nombre_firma"].ToString();
                    TxtCarFirma.Text = lLector["cargo_firma"].ToString();
                    //// Camps Nuevos de Facturacion y Garantias
                    if (lLector["fecha_cam_pro_fac"].ToString().Trim().Length >= 10)
                        TxtFechaCamFac.Text = lLector["fecha_cam_pro_fac"].ToString().Substring(6, 4) + "/" + lLector["fecha_cam_pro_fac"].ToString().Substring(3, 2) + "/" + lLector["fecha_cam_pro_fac"].ToString().Substring(0, 2);
                    TxtDiaCortFact.Text = lLector["dia_corte_fact"].ToString();
                    TxtAnobasLiq.Text = lLector["ano_base_liq_fact"].ToString();
                    TxtDiaHabVecnFac.Text = lLector["dia_hab_venc_fact"].ToString();
                    TxtDiaCalGar.Text = lLector["dia_cal_calculo_gar"].ToString();
                    TxtNoMesCalGar.Text = lLector["meses_fac_calc_gar"].ToString();
                    TxtDiaMaxConsGar.Text = lLector["dia_max_const_gar"].ToString();
                    TxtDiaMinLibGar.Text = lLector["dia_min_const_gar"].ToString();
                    TxtNoCuentaConsGar.Text = lLector["no_cuenta_consi_gar"].ToString();
                    TxtHorMaxDecOpera.Text = lLector["hora_max_declara_opera"].ToString();
                    /// Ajustes Verificacion 20150211
                    TxtNoHorasCorContPrim.Text = lLector["no_horas_correcion_primario"].ToString();
                    TxtNoDiasCorContPrim.Text = lLector["no_dias_cal_correc_secundario"].ToString();
                    ddlPermiteRegCont.SelectedValue = lLector["permite_ing_cont_ext"].ToString();
                    /// Ajustes Verificacion 20150410
                    TxtNoHorasActualCont.Text = lLector["no_horas_actualiza_contrato"].ToString();
                    TxtHoraIniExt.Text = lLector["hora_ini_inf_ope_ext"].ToString();
                    TxtHoraFinExt.Text = lLector["hora_fin_inf_ope_ext"].ToString();
                    txtDiaUNR.Text = lLector["dias_reg_usr_no_reg"].ToString();
                    ddlAprob.SelectedValue = lLector["requiere_aprob_cargue"].ToString();
                    ddlVerifMan.SelectedValue = lLector["verifica_manual"].ToString();
                    /// Ajustes mercado mayorista 20150616
                    ddlVerifManMayo.SelectedValue = lLector["verifica_manual_cont_mayor"].ToString();
                    TxtDiaHabRegMayor.Text = lLector["no_dias_hab_regi_cont_mayor"].ToString();
                    TxtDiaHabCorrMayor.Text = lLector["no_dias_hab_corr_cont_mayor"].ToString();
                    //// Parametrizacion del Mail del Gestor 20150715
                    TxtMailGestor.Text = lLector["mail_gestor"].ToString();
                    //// parametrod de mercado mayorista 2015/09/24
                    TxtDiaPubMercMay.Text = lLector["dia_hab_publica_inf_mayor"].ToString();
                    // Campos nuevos mercado mayorista 20150616
                    txtHoraSum.Text = lLector["hora_max_prog_sum"].ToString();
                    txtHoraSumPost.Text = lLector["hora_max_prog_sum_post"].ToString();
                    //20190502 rq024-19
                    //if (lLector["fecha_max_prog_sum"].ToString().Trim().Length >= 10)
                    //    txtFechaSum.Text = lLector["fecha_max_prog_sum"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_prog_sum"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_prog_sum"].ToString().Substring(0, 2);
                    txtHoraTrans.Text = lLector["hora_max_prog_trans"].ToString();
                    txtHoraTransPost.Text = lLector["hora_max_prog_trans_post"].ToString();
                    //20190502 rq024-19
                    //if (lLector["fecha_max_prog_trans"].ToString().Trim().Length >= 10)
                    //    txtFechaTrans.Text = lLector["fecha_max_prog_trans"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_prog_trans"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_prog_trans"].ToString().Substring(0, 2);
                    // Campos nuevos parametros cambio precios indexador 20151211
                    ddlMercadoActPrecio.SelectedValue = lLector["mercado_act_precio"].ToString();
                    TxtNoDiaAproCambioPrec.Text = lLector["no_dias_apro_cambio_precio"].ToString();
                    TxtDiaPubOpe.Text = lLector["dia_hab_ejec_inf"].ToString();
                    if (lLector["fecha_ini_act_precio"].ToString().Trim().Length >= 10)
                        TxtFechaIniCamPrecio.Text = lLector["fecha_ini_act_precio"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_act_precio"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_act_precio"].ToString().Substring(0, 2);
                    if (lLector["fecha_fin_act_precio"].ToString().Trim().Length >= 10)
                        TxtFechaFinCamPrecio.Text = lLector["fecha_fin_act_precio"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_act_precio"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_act_precio"].ToString().Substring(0, 2);
                    if (lLector["fecha_ini_vigencia_precio"].ToString().Trim().Length >= 10)
                        TxtFechaIniVigPrecioIndexa.Text = lLector["fecha_ini_vigencia_precio"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_vigencia_precio"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_vigencia_precio"].ToString().Substring(0, 2);
                    /// Campo nuevo ajustes facturacion 20160104
                    TxtNoDiasModifFactura.Text = lLector["no_dias_modifica_factura"].ToString().Trim();
                    if (lLector["fecha_max_corr_usr_fin"].ToString().Trim().Length >= 10)
                        TxtFechaUsrFin.Text = lLector["fecha_max_corr_usr_fin"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_corr_usr_fin"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_corr_usr_fin"].ToString().Substring(0, 2);
                    //Cambio para fechas de curva
                    if (lLector["fecha_ini_curva"].ToString().Trim().Length >= 10)
                        TxtFechaIniCurva.Text = lLector["fecha_ini_curva"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_curva"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_curva"].ToString().Substring(0, 2);
                    if (lLector["fecha_fin_curva"].ToString().Trim().Length >= 10)
                        TxtFechaFinCurva.Text = lLector["fecha_fin_curva"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_curva"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_curva"].ToString().Substring(0, 2);
                    //Cambio Fecha maxima de modificacion fuente 20160607
                    if (lLector["fecha_max_fuente"].ToString().Trim().Length >= 10)
                        TxtFechaFuente.Text = lLector["fecha_max_fuente"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_fuente"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_fuente"].ToString().Substring(0, 2);
                    //oferta comprometida 20160608
                    TxtDiaOferta.Text = lLector["dia_hab_pub_ofe_comp"].ToString().Trim();
                    //Alcance PTDV 20160721
                    if (lLector["fecha_max_ptdv"].ToString().Trim().Length >= 10)
                        TxtFechaPtdv.Text = lLector["fecha_max_ptdv"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_ptdv"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_ptdv"].ToString().Substring(0, 2);
                    // Campo Nuevo Req. 079 Mapa Interactivo 20170314
                    TxtNoDiaPubMapa.Text = lLector["dia_pub_map_int"].ToString();
                    /////////////////////////////////////////////////////////////
                    /// Campos nuevos Req.009-17 Indicadores Fase II 20170307 ///
                    /////////////////////////////////////////////////////////////
                    if (lLector["fecha_ini_reg_demanda"].ToString().Trim().Length >= 10)
                        TxtFecIniDema.Text = lLector["fecha_ini_reg_demanda"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_reg_demanda"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_reg_demanda"].ToString().Substring(0, 2);
                    TxtHoraIniDema.Text = lLector["hora_ini_reg_demanda"].ToString();
                    if (lLector["fecha_fin_reg_demanda"].ToString().Trim().Length >= 10)
                        TxtFecFinDema.Text = lLector["fecha_fin_reg_demanda"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_reg_demanda"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_reg_demanda"].ToString().Substring(0, 2);
                    TxtHoraFinDema.Text = lLector["hora_fin_reg_demanda"].ToString();
                    /////////////////////////////////////////////////////////////
                    ///20170804 rq037-17
                    TxtOperPre.Text = lLector["contratos_precio"].ToString();
                    ///20170803 rq034-17
                    if (lLector["fecha_pub_ptdvf"].ToString().Trim().Length >= 10)
                        TxtFechaPtdvf.Text = lLector["fecha_pub_ptdvf"].ToString().Substring(6, 4) + "/" + lLector["fecha_pub_ptdvf"].ToString().Substring(3, 2) + "/" + lLector["fecha_pub_ptdvf"].ToString().Substring(0, 2);
                    ///20171017 rq026-17
                    txtDiaRegMod.Text = lLector["dias_hab_mod_cont"].ToString();
                    ///20171017 rq026-17
                    txtDiaRegModApr.Text = lLector["dias_hab_aprob_mod"].ToString();
                    TxtMaxSolPrec.Text = lLector["no_max_sol_pre"].ToString();  //20180302 rq004-18
                    TxtHoraVigTok.Text = lLector["hora_vig_token"].ToString();  //20181210 rq046-18
                    TxtDiaVigFirma.Text = lLector["dia_vig_firma"].ToString();  //20181210 rq046-18
                    TxtNombreFirmaBmc.Text = lLector["nombre_firma_qr_bmc"].ToString();  //20181210 rq046-18
                    TxtDiaImpCert.Text = lLector["dia_vig_impresion"].ToString();  //20181210 rq046-18
                    ddlEjecucion.SelectedValue = lLector["dia_ingreso_ejecucion"].ToString(); //20190322 rq018-19
                    txtHoraEjec.Text = lLector["hora_max_ejecucion"].ToString();  //20190322 rq018-19
                    ddlMesfact.SelectedValue = lLector["mes_base_fact"].ToString();  //20210422 ajsute factruacion
                    txtDiaConsGar.Text = lLector["dia_cons_gar_crt"].ToString();  //20210422 ajsute factruacion
                    txtDiaLibGar.Text = lLector["dia_lib_gar_crt"].ToString();  //20210422 ajsute factruacion
                    TxtObservacion.Text = "";
                    // Campo nuevo de Menu Visualizacion Archivos Reg. 58 20160701
                    try
                    {
                        ddlMenuVisualizaArchivos.SelectedValue = lLector["codigo_menu_visual_archivos"].ToString().Trim();
                    }
                    catch (Exception ex)
                    {

                    }
                    //20190318 rq017-19 ajsue nominaciones
                    ddlDiaNomSum.SelectedValue = lLector["dia_prog_sum"].ToString();
                    ddlDiaPostSum.SelectedValue = lLector["dia_prog_sum_post"].ToString();
                    txtHoraSumDia.Text = lLector["hora_max_prog_sum_dia"].ToString();
                    ddlDiaNomDiaSum.SelectedValue = lLector["dia_prog_sum_dia"].ToString();
                    ddlDiaNomTrans.Text = lLector["dia_prog_trans"].ToString();
                    ddlDiaPostTrans.SelectedValue = lLector["dia_prog_trans_post"].ToString();
                    txtHoraDiaTrans.Text = lLector["hora_max_prog_trans_dia"].ToString();
                    ddlDiaNomDiaTrans.SelectedValue = lLector["dia_prog_trans_dia"].ToString();
                    //20190318 rq017-19 fin ajsue nominaciones
                    //  20190502 rq024-19
                    ddlReplicaNomTra.SelectedValue = lLector["replica_nom_tra"].ToString();
                    txtRegSumInt.Text = lLector["dia_habil_reg_int"].ToString();  //20190404 rq018-19 fase II
                    txtMesDiaInt.Text = lLector["dia_mes_ini_int"].ToString();  //20190404 rq018-19 fase II
                    txtMesDiaIniReg.Text = lLector["dia_mes_int_ini"].ToString();  //20190404 rq018-19 fase II
                    txtMesDiaFinReg.Text = lLector["dia_mes_int_fin"].ToString();  //20190404 rq018-19 fase II                }
                    txtDiaPrevEjec.Text = lLector["dia_prev_ing_ejec"].ToString();  //20190524 rq029-19
                    ddlReplicaNomSum.SelectedValue = lLector["replica_nom_sum"].ToString(); //20190712  
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", "");
                }   
                else
                {
                    tblCaptura.Visible = false;
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado.";
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_valor_incremento_ofertas", "@P_cantidad_minima_negociacion", "@P_valor_umbral_exceso_demanda", "@P_valor_vig_ptdv_cidv_min_minas", 
                                        "@P_dias_caducidad_contrasena", "@P_observacion_cambio", "@P_accion", "@P_mostrar_agresores_todos", "@P_mostrar_agresores_oferentes", "@P_dias_max_reg_capacidad_tra",
                                        "@P_nombre_firma","@P_cargo_firma","@P_archivo_firma",
                                        "@P_fecha_cam_pro_fac","@P_dia_corte_fact","@P_ano_base_liq_fact","@P_dia_hab_venc_fact","@P_dia_cal_calculo_gar",
                                        "@P_meses_fac_calc_gar","@P_no_cuenta_consi_gar","@P_dia_max_const_gar","@P_dia_min_const_gar","@P_hora_max_declara_opera",
                                        "@P_no_horas_correcion_primario", "@P_no_dias_cal_correc_secundario", "@P_permite_ing_cont_ext","@P_no_horas_actualiza_contrato",
                                        "@P_hora_ini_inf_ope_ext","@P_hora_fin_inf_ope_ext","@P_dias_reg_usr_no_reg","@P_requiere_aprob_cargue", "@P_verifica_manual",
                                        "@P_no_dias_hab_regi_cont_mayor","@P_no_dias_hab_corr_cont_mayor","@P_verifica_manual_cont_mayor", // Campos nuevos mercado mayorista 20150616
                                        "@P_mail_gestor", "@P_dia_hab_publica_inf_mayor",
                                        "@P_hora_max_prog_sum","@P_hora_max_prog_sum_post","@P_fecha_max_prog_sum","@P_hora_max_prog_trans","@P_hora_max_prog_trans_post","@P_fecha_max_prog_trans", // campos de programacion definivita 20151123
                                        "@P_mercado_act_precio","@P_no_dias_apro_cambio_precio","@P_fecha_ini_act_precio","@P_fecha_fin_act_precio","@P_fecha_ini_vigencia_precio", // Campos nuevos cambio precio indexador 20151211
                                        "@P_dia_hab_ejec_inf", //dia de publicacion de infromacion operativa 20160108
                                        "@P_no_dias_modifica_factura", // Campo nuevo ajustes facturacion 20160104
                                        "@P_fecha_max_corr_usr_fin", // campo para Sector de consumo 20160118
                                        "@P_fecha_ini_curva", "@P_fecha_fin_curva", // cambio para fecha de curva
                                        "@P_fecha_max_fuente", //cambio para fecha actualización fuente 20160607
                                        "@P_dia_hab_pub_ofe_comp", //oferta comprometida 20160608
                                        "@P_codigo_menu_visual_archivos", //Campo nuevo Reg.58 20160701
                                        "@P_fecha_max_ptdv", //alcance PTDV 20160721
                                        "@P_dia_pub_map_int", //Req. 079 Mapa Intetractivo 20170314
                                        "@P_fecha_ini_reg_demanda","@P_hora_ini_reg_demanda","@P_fecha_fin_reg_demanda","@P_hora_fin_reg_demanda", // Campos nuevos Req.009-17 Indicadores Fase II 20170307
                                        "@P_contratos_precio",  //20170804 rq037-17
                                         "@P_fecha_pub_ptdvf",  //20170803 rq034-17
                                        "@P_dias_hab_mod_cont" , "@P_dias_hab_aprob_mod", //20171017 rq026-17
                                        "@P_no_max_sol_pre", //20180302 rq004-18
                                        "@P_hora_vig_token","@P_dia_vig_firma","@P_nombre_firma_qr_bmc","@P_dia_vig_impresion", //20181210 rq046-18
                                        "@P_dia_prog_sum","@P_dia_prog_sum_post","@P_hora_max_prog_sum_dia","@P_dia_prog_sum_dia","@P_dia_prog_trans","@P_dia_prog_trans_post","@P_hora_max_prog_trans_dia","@P_dia_prog_trans_dia", //20190318 rq017-19
                                        "@P_dia_ingreso_ejecucion","@P_hora_max_ejecucion",//20190322 rq018-19 ajuste transaccioonal
                                        "@P_replica_nom_tra",//20190502 rq024-19
                                        "@P_dia_habil_reg_int","@P_dia_mes_ini_int","@P_dia_mes_int_ini","@P_dia_mes_int_fin",//20190404 rq018-19 fase II
                                        "@P_dia_prev_ing_ejec",//20190524 rq029-19
                                        "@P_replica_nom_sum",//20190712
                                        "@P_mes_base_fact", "@P_dia_cons_gar_crt", "@P_dia_lib_gar_crt", //20210422 ajsute factiracion
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Char ,SqlDbType.Char,SqlDbType.Int,SqlDbType.Int,SqlDbType.Char, SqlDbType.VarChar , SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, // Campos nuevos mercado mayorista 20150616
                                        SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, // Campos nuevos cambio precio indexador 20151211
                                        SqlDbType.Int, //dia de publicacion de infromacion operativa 20160108
                                        SqlDbType.Int, // Campo nuevo ajustes facturacion 20160104
                                        SqlDbType.VarChar, // Campo nuevo ajustes facturacion 20160104
                                        SqlDbType.VarChar, SqlDbType.VarChar, // Fecha de curva
                                        SqlDbType.VarChar,// Fecha de máxima fuente 20160607
                                        SqlDbType.Int, // Oferta comprometida 20160608
                                        SqlDbType.Int, //Campo nuevo Reg.58 20160701
                                        SqlDbType.VarChar,// alcance PTDV 20160721
                                        SqlDbType.Int, //Req. 079 Mapa Interactivo 20170314
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, // Campos nuevos Req.009-17 Indicadores Fase II 20170307
                                        SqlDbType.Int, //20170804 rq037-17
                                        SqlDbType.VarChar, //20170803 rq034-17
                                        SqlDbType.Int,SqlDbType.Int, //20171017 rq026-17
                                        SqlDbType.Int, //20180302 rq004-18
                                        SqlDbType.Int ,SqlDbType.Int ,SqlDbType.VarChar,SqlDbType.Int, //20181210 rq046-18
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, //20180318 rq017-19
                                        SqlDbType.VarChar,SqlDbType.VarChar,//20190322 rq018-19 ajuste transaccioonal
                                        SqlDbType.VarChar,//20190502 rq024-19
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, //20190404 rq018-19 fase II
                                        SqlDbType.Int,//20190524 rq029-19
                                        SqlDbType.VarChar,//20190712
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,//20210422 ajsute factiracion
                                        };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "", "1", "", "", "0", "", "", "", "", "0", "0", "0", "0", "0", "", "0", "0", "", "0", "0", "", "0", "", "", "0", "S", "S", "0", "0", "N", "", "0", "", "", "", "", "", "", "", "0", "", "", "", "", "0", "", "", "", "", "0", "0", "", // Fecha de máxima fuente 20160607 // Oferta comprometida 20160608 //alcance PTDV 20160721
                                      "0",  //Req. 079 Mapa Interactivo 20170314
                                      "","","","",  // Campos nuevos Req.009-17 Indicadores Fase II 20170307
                                      "0",  //20170804 rq037-17
                                      "",  //20170803 rq034-17
                                      "0", "0", //20171017 rq026-17
                                      "0",//20180302 rq004-18
                                      "0","0","","0", //20181210 rq046-18
                                      "","","","","","","","",//20180318 rq017-19
                                      "","",//20190322 rq018-19 ajuste transaccioonal
                                      "",//20190502 rq024-19
                                      "0","","","",//20190404 rq018-19 fase II
                                      "0",//20190524 rq029-19
                                      "N", //20190712
                                      "0","0","0",//20210422 ajsute factiracion
                                    };
        lblMensaje.Text = "";
        string sRuta = sRutaArc;
        DateTime ldFecha;
        int liValor = 0;

        try
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaCamFac.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha Cambio Proceso Facturación.<br>";
            }
            if (FuArchivo.FileName.Trim().Length <= 0)
                lblMensaje.Text += "Debe Seleccionar el Archivo de la Firma.<br>";
            if (TxtNoHorasCorContPrim.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Horas Correción Contrato Primario. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoHorasCorContPrim.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Horas Correción Contrato Primario. <br>";
                }
            }
            if (TxtNoDiasCorContPrim.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Dias Calendario Correción Contrato Secundario. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoDiasCorContPrim.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Dias Calendario Correción Contrato Secundario. <br>";
                }
            }
            if (TxtNoHorasActualCont.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Horas Actualización Contrato Verificación. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoHorasActualCont.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Horas Actualización Contrato Verificación. <br>";
                }
            }
            if (TxtHoraIniExt.Text == "")
                lblMensaje.Text += " Debe digitar la hora inicial de registro extemporáneo de información operativa. <br>";
            if (TxtHoraFinExt.Text == "")
                lblMensaje.Text += " Debe digitar la hora final de registro extemporáneo de información operativa. <br>";
            if (txtDiaUNR.Text == "")
                lblMensaje.Text += " Debe digitar dìas para registro de información operativa del mes anterior de usuarios no regulados. <br>";
            // Validaciones campos nuevos mercado mayorista 20150616
            if (TxtDiaHabRegMayor.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Dias Habiles Registro Contrato Mayorista. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtDiaHabRegMayor.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Dias Habiles  Registro Contrato Mayorisya. <br>";
                }
            }
            if (TxtDiaHabCorrMayor.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Dias Habiles Corección Contrato Mayorista. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtDiaHabCorrMayor.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Dias Habiles  Corección Contrato Mayorisya. <br>";
                }
            }
            // Validacion del Mail del Gestor 20150715
            if (TxtMailGestor.Text == "")
                lblMensaje.Text += " Debe Ingresar el Mail del Gestor. <br>";

            if (TxtMailGestor.Text == "")
                lblMensaje.Text += " Debe Ingresar el Mail del Gestor. <br>";

            if (TxtDiaPubMercMay.Text == "")
                lblMensaje.Text += " Debe digitar los días de publicación de mercado mayorista. <br>";
            else
            {
                if (Convert.ToInt16(TxtDiaPubMercMay.Text) <= 0)
                    lblMensaje.Text += " Los días de publicación de mercado mayorista deben se mayores que cero. <br>";
            }
            // Validaciones campos nuevos paametros precio indexador 20151211
            if (TxtNoDiaAproCambioPrec.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo No. Días Calendario Aprobación Cambio Precio Indexador. <br>";
            if (TxtDiaPubOpe.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar los días hábiles de publicación de información operativa. <br>";
            if (TxtFechaIniCamPrecio.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo Fecha Inicial Permite Cambio Precio Indexador. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniCamPrecio.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial Permite Cambio Precio Indexador.<br>";
                }
            }
            if (TxtFechaFinCamPrecio.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo Fecha Final Permite Cambio Precio Indexador. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFinCamPrecio.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Final Permite Cambio Precio Indexador.<br>";
                }
            }
            if (TxtFechaIniVigPrecioIndexa.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo Fecha Inicial Vigencia Nuevo Precio Indexador. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniVigPrecioIndexa.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial Vigencia Nuevo Precio Indexador.<br>";
                }
            }
            /// Campo nuevo ajustes facturacion 20160104
            if (TxtNoDiasModifFactura.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo No. Días Calendario Modificación Facturación. <br>";

            //Control de fechas para consulta de curvas
            string lsFecha = "N";
            ldFecha = DateTime.Now;
            if (TxtFechaIniCurva.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la fecha inicial para la consulta de curvas. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniCurva.Text.Trim());
                    lsFecha = "S";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial de consulta de curvas.<br>";
                }
            }
            if (TxtFechaFinCurva.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la fecha final para la consulta de curvas. <br>";
            else
            {
                try
                {
                    if (lsFecha == "S")
                    {
                        if (ldFecha > Convert.ToDateTime(TxtFechaFinCurva.Text.Trim()))
                            lblMensaje.Text += "La fecha inicial de consulta de curvas debe ser menor o igual que la fecha final.<br>";
                    }
                    else
                        ldFecha = Convert.ToDateTime(TxtFechaFinCurva.Text.Trim());

                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Final de consulta de curvas.<br>";
                }
            }
            //valida fecha para modificación de fuente 20160607
            if (TxtFechaFuente.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la fecha máxima para actualización de fuente de contratos registrados. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFechaFuente.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha máxima de actualización de fuente de contratos registrados.<br>";
                }
            }
            //oferta comprometida 20160608
            if (TxtDiaOferta.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el día hábil de publicación de la oferta comprometida. <br>";
            else
                try
                {
                    if (Convert.ToInt16(TxtDiaOferta.Text) <= 0)
                        lblMensaje.Text += "El día hábil de publicación de la oferta comprometida debe ser mayor que cero. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor inválido para el día hábil de publicación de la oferta comprometida. <br>";
                }
            // Validacion Menu Visualiza Archivos Reg. 58 20160701
            if (ddlMenuVisualizaArchivos.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Menú de Visualización de Archivos. <br>";
            //alcance PTDV 20160721
            if (TxtFechaPtdv.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la fecha máxima de registro de información PTDV. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFechaFuente.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha máxima de actualización de fuente de contratos registrados.<br>";
                }
            }
            ///////////////////////////////////////////////////////////////////
            /// Validaciones nuevas Req.009-17 Indicadores Fase II 20170307 ///
            ///////////////////////////////////////////////////////////////////
            if (TxtFecIniDema.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Inicial Registro Demanda Regulada. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFecIniDema.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial Registro Demanda Regulada.<br>";
                }
            }
            if (TxtHoraIniDema.Text == "")
                lblMensaje.Text += "Debe digitar la hora inicial registro demanda regulada. <br>";
            if (TxtFecFinDema.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Final Registro Demanda Regulada. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFecFinDema.Text.Trim());
                    if (Convert.ToDateTime(TxtFecFinDema.Text.Trim()) < Convert.ToDateTime(TxtFecIniDema.Text.Trim()))
                        lblMensaje.Text += "La Fecha Final Registro Demanda Regulada debe ser mayor o igual a la Fecha Inicial.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Final Registro Demanda Regulada.<br>";
                }
            }
            if (TxtHoraFinDema.Text == "")
                lblMensaje.Text += "Debe digitar la hora final registro demanda regulada. <br>";
            ///FIn indicadores CREG rq009-17

            //20170804 rq037-17
            if (TxtOperPre.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el número mínimo de contratos para publicación de precio en el BEC. <br>";
            //20170803 rq034-17
            if (TxtFechaPtdvf.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha de publicación de la PTDVF. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFechaPtdvf.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha de publicación de la PTDVF.<br>";
                }
            }
            //20171017 rq026-17
            if (txtDiaRegMod.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar los días hábiles para la  aporbación de las modificaciones pr parte de la contraparte. <br>";
            //20171017 rq026-17
            if (txtDiaRegModApr.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar los días hábiles para la  aporbación de las modificaciones pr parte del gestor. <br>";
            //20180302rq004-18
            if (TxtMaxSolPrec.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el número máximo de solicitudes de cambio de precio por año. <br>";
            //20181210 rq046-18
            if (TxtHoraVigTok.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las horas de vigencia del token. <br>";
            //20181210 rq046-18
            if (TxtDiaVigFirma.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las días de vigencia de la firma. <br>";
            //20181210 rq046-18
            if (TxtNombreFirmaBmc.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el nombre del usuario de la BMC que firma el certificado. <br>";
            //20181210 rq046-18
            if (TxtDiaImpCert.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las días de vigencia impresión del ceritificado. <br>";
            //20190318 rq017-19
            if (ddlDiaNomSum.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de suministro. <br>";
            if (txtHoraSum.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de suministro. <br>";
            if (ddlDiaNomDiaSum.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación de nominación del día de gas de suministro. <br>";
            if (txtHoraSumDia.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación de nominación del día de gas de suministro. <br>";
            if (ddlDiaPostSum.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de suministro posterior a la re-nominación. <br>";
            if (txtHoraSumPost.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de suministro posterior a la re-nominación. <br>";
            if (ddlDiaNomTrans.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de transporte. <br>";
            if (txtHoraTrans.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de transporte. <br>";
            if (ddlDiaNomDiaTrans.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación de nominación del día de gas de transporte. <br>";
            if (txtHoraDiaTrans.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación de nominación del día de gas de transporte. <br>";
            if (ddlDiaPostTrans.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de transporte posterior a la re-nominación. <br>";
            if (txtHoraTransPost.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de transporte posterior a la re-nominación. <br>";
            //20190502 rq024-19
            //if (txtFechaSum.Text == "")
            //    lblMensaje.Text += "Debe digitar la fecha máxima de reporte histórico de programación definitiva de suministro posterior a la re-nominación. <br>";
            //20190502 rq024-19
            //if (txtFechaTrans.Text == "")
            //    lblMensaje.Text += "Debe digitar la fecha máxima de reporte histórico de programación definitiva de transporte posterior a la re-nominación. <br>";
            //20190318 Fin rq017-19
            //20190322 rq018-19 ajuste transaccional
            if (ddlEjecucion.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día de ingreso de la ejecución del contrato. <br>";
            if (txtHoraEjec.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de ingreso de la ejecución del contrato. <br>";
            //20190322  rq018-19  fin ajuste transaccional
            //20190524 rq029-19
            if (txtDiaPrevEjec.Text == "")
                lblMensaje.Text += "Debe digitar los días previos para el ingreso de ejecución del contrato. <br>";
            //20190404 rq018-19 fase II
            if (txtRegSumInt.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las días de registro de los contratos de suministro con interrupciones. <br>";
            if (txtMesDiaInt.Text == "")
                lblMensaje.Text += "Debe digitar el mes/día de inicio para control de negociaciones de suministro con interrupciones de mercado secundario. <br>";
            else
                try
                {
                    Convert.ToDateTime("2000/" + txtMesDiaInt.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El mes/día de inicio para control de negociaciones de suministro con interrupciones de mercado secundario no es válido. <br>";
                }
            if (txtMesDiaIniReg.Text == "")
                lblMensaje.Text += "Debe digitar el mes/día inicial de registro negociaciones de suministro con interrupciones de mercado secundario. <br>";
            else
                try
                {
                    Convert.ToDateTime("2000/" + txtMesDiaIniReg.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El mes/día inicial de registro negociaciones de suministro con interrupciones de mercado secundario no es válido. <br>";
                }
            if (txtMesDiaFinReg.Text == "")
                lblMensaje.Text += "Debe digitar el mes/día final de registro negociaciones de suministro con interrupciones de mercado secundario. <br>";
            else
                try
                {
                    Convert.ToDateTime("2000/" + txtMesDiaFinReg.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El mes/día final de registro negociaciones de suministro con interrupciones de mercado secundario no es válido. <br>";
                }
            //20190404 fin rq018-19 fase II
            //20210422 ajuste factruacion
            if (ddlMesfact.SelectedValue =="0")
                lblMensaje.Text += "Debe digitar el mes base para la facturación . <br>";
            if (txtDiaConsGar.Text=="")
                lblMensaje.Text += "Debe digitar los días para constitución de garantías. <br>";
            if (txtDiaLibGar.Text == "")
                lblMensaje.Text += "Debe digitar los días para liberacíón de garantías. <br>";
            //20210422 ajuste fin factruacion
            if (lblMensaje.Text == "")
            {
                FuArchivo.SaveAs(sRuta + FuArchivo.FileName);

                lValorParametros[0] = TxtValIncOferta.Text.Trim();
                lValorParametros[1] = TxtCantMinNego.Text.Trim();
                lValorParametros[2] = TxtValHumExcDema.Text.Trim();
                lValorParametros[3] = TxtValVigMinis.Text.Trim();
                lValorParametros[4] = TxtDiasCont.Text.Trim();
                lValorParametros[5] = TxtObservacion.Text.Trim();
                lValorParametros[7] = ddlMostAgreTod.SelectedValue;
                lValorParametros[8] = ddlMostAgreOfe.SelectedValue;
                lValorParametros[9] = TxtDiaMaxTras.Text.Trim();
                lValorParametros[10] = TxtNomFirma.Text.Trim();
                lValorParametros[11] = TxtCarFirma.Text.Trim();
                lValorParametros[12] = FuArchivo.FileName.Trim();
                lValorParametros[13] = TxtFechaCamFac.Text.Trim();
                lValorParametros[14] = TxtDiaCortFact.Text.Trim();
                lValorParametros[15] = TxtAnobasLiq.Text.Trim();
                lValorParametros[16] = TxtDiaHabVecnFac.Text.Trim();
                lValorParametros[17] = TxtDiaCalGar.Text.Trim();
                lValorParametros[18] = TxtNoMesCalGar.Text.Trim();
                lValorParametros[19] = TxtNoCuentaConsGar.Text.Trim();
                lValorParametros[20] = TxtDiaMaxConsGar.Text.Trim();
                lValorParametros[21] = TxtDiaMinLibGar.Text.Trim();
                lValorParametros[22] = TxtHorMaxDecOpera.Text.Trim();
                lValorParametros[23] = TxtNoHorasCorContPrim.Text.Trim();
                lValorParametros[24] = TxtNoDiasCorContPrim.Text.Trim();
                lValorParametros[25] = ddlPermiteRegCont.SelectedValue;
                lValorParametros[26] = TxtNoHorasActualCont.Text.Trim();
                lValorParametros[27] = TxtHoraIniExt.Text.Trim();
                lValorParametros[28] = TxtHoraFinExt.Text.Trim();
                lValorParametros[29] = txtDiaUNR.Text.Trim();
                lValorParametros[30] = ddlAprob.SelectedValue;
                lValorParametros[31] = ddlVerifMan.SelectedValue;
                // Campos nuevos contrato mayorista 20150616
                lValorParametros[32] = TxtDiaHabRegMayor.Text.Trim();
                lValorParametros[33] = TxtDiaHabCorrMayor.Text.Trim();
                lValorParametros[34] = ddlVerifManMayo.SelectedValue;
                // Campos nuevos validacoin del mail del gestor 20150715
                lValorParametros[35] = TxtMailGestor.Text;
                lValorParametros[36] = TxtDiaPubMercMay.Text;
                // Campso paar programacion definitia 20151123
                lValorParametros[37] = txtHoraSum.Text;
                lValorParametros[38] = txtHoraSumPost.Text; 
                //lValorParametros[39] = txtFechaSum.Text; //20190502 rq024-19
                lValorParametros[40] = txtHoraTrans.Text;
                lValorParametros[41] = txtHoraTransPost.Text;
                //lValorParametros[42] = txtFechaTrans.Text; //20190502 rq024-19
                // Campos nuevos precio indexador 20151211
                lValorParametros[43] = ddlMercadoActPrecio.SelectedValue;
                lValorParametros[44] = TxtNoDiaAproCambioPrec.Text.Trim();
                lValorParametros[45] = TxtFechaIniCamPrecio.Text.Trim();
                lValorParametros[46] = TxtFechaFinCamPrecio.Text.Trim();
                lValorParametros[47] = TxtFechaIniVigPrecioIndexa.Text.Trim();
                lValorParametros[48] = TxtDiaPubOpe.Text.Trim();
                // Campo nuevo ajustes facturacion 20160104
                lValorParametros[49] = TxtNoDiasModifFactura.Text.Trim();
                // Campo nuevo cambio sector de consumo 20160118
                lValorParametros[50] = TxtFechaUsrFin.Text.Trim();
                lValorParametros[51] = TxtFechaIniCurva.Text.Trim();
                lValorParametros[52] = TxtFechaFinCurva.Text.Trim();
                // Campo nuevo fuente 20160607
                lValorParametros[53] = TxtFechaFuente.Text.Trim();
                // oferta comprometida 20160608
                lValorParametros[54] = TxtDiaOferta.Text.Trim();
                // Campo nuevo Reg. 58 20160701
                lValorParametros[55] = ddlMenuVisualizaArchivos.SelectedValue;
                //Alcance PTDV
                lValorParametros[56] = TxtFechaPtdv.Text.Trim();
                // Req. 079 Mapa Interactivo 20170314
                lValorParametros[57] = TxtNoDiaPubMapa.Text.Trim();
                //////////////////////////////////////////////////////////////
                /// Campos nuevos Req. 009-17 Indicadores Fase II 20170307 ///
                //////////////////////////////////////////////////////////////
                lValorParametros[58] = TxtFecIniDema.Text.Trim();
                lValorParametros[59] = TxtHoraIniDema.Text.Trim();
                lValorParametros[60] = TxtFecFinDema.Text.Trim();
                lValorParametros[61] = TxtHoraFinDema.Text.Trim();
                //////////////////////////////////////////////////////////////
                //20170804 rq037-17
                lValorParametros[62] = TxtOperPre.Text.Trim();
                //20170803 rq034-17
                lValorParametros[63] = TxtFechaPtdvf.Text.Trim();
                //20171017 rq026-17
                lValorParametros[64] = txtDiaRegMod.Text.Trim();
                //20171017 rq026-17
                lValorParametros[65] = txtDiaRegModApr.Text.Trim();
                lValorParametros[66] = TxtMaxSolPrec.Text; //20180302 rq004-18
                lValorParametros[67] = TxtHoraVigTok.Text; //20181210 rq046-18
                lValorParametros[68] = TxtDiaVigFirma.Text; //20181210 rq046-18
                lValorParametros[69] = TxtNombreFirmaBmc.Text; //20181210 rq046-18
                lValorParametros[70] = TxtDiaImpCert.Text; //20181210 rq046-18
                //20190318 rq017-19
                lValorParametros[71] = ddlDiaNomSum.SelectedValue;
                lValorParametros[72] = ddlDiaPostSum.SelectedValue;
                lValorParametros[73] = txtHoraSumDia.Text;
                lValorParametros[74] = ddlDiaNomDiaSum.SelectedValue;
                lValorParametros[75] = ddlDiaNomTrans.SelectedValue;
                lValorParametros[76] = ddlDiaPostTrans.SelectedValue;
                lValorParametros[77] = txtHoraDiaTrans.Text;
                lValorParametros[78] = ddlDiaNomDiaTrans.SelectedValue;
                lValorParametros[79] = ddlEjecucion.SelectedValue; //20190322 rq018-19 ajsute transaccional
                lValorParametros[80] = txtHoraEjec.Text;//20190322 rq018-19 ajsute transaccional
                lValorParametros[81] = ddlReplicaNomTra.SelectedValue;//20190502 rq024-19
                lValorParametros[82] = txtRegSumInt.Text;//20190404 rq018-19 fase II
                lValorParametros[83] = txtMesDiaInt.Text;//20190404 rq018-19 fase II
                lValorParametros[84] = txtMesDiaIniReg.Text;//20190404 rq018-19 fase II
                lValorParametros[85] = txtMesDiaFinReg.Text;//20190404 rq018-19 fase II
                lValorParametros[86] = txtDiaPrevEjec.Text;//20190524 rq029-19
                lValorParametros[87] = ddlReplicaNomSum.SelectedValue;//20190712
                lValorParametros[88] = ddlMesfact.SelectedValue;//20210422 ajsute factruacion
                lValorParametros[89] = txtDiaConsGar.Text;//20210422 ajsute factruacion
                lValorParametros[90] = txtDiaLibGar.Text;//20210422 ajsute factruacion
                //20190318 fin rq017-19
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosGenerales", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación de Parametros Generales.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_valor_incremento_ofertas", "@P_cantidad_minima_negociacion", "@P_valor_umbral_exceso_demanda", "@P_valor_vig_ptdv_cidv_min_minas", 
                                        "@P_dias_caducidad_contrasena", "@P_observacion_cambio", "@P_accion", "@P_mostrar_agresores_todos", "@P_mostrar_agresores_oferentes", "@P_dias_max_reg_capacidad_tra",
                                        "@P_nombre_firma","@P_cargo_firma","@P_archivo_firma",
                                        "@P_fecha_cam_pro_fac","@P_dia_corte_fact","@P_ano_base_liq_fact","@P_dia_hab_venc_fact","@P_dia_cal_calculo_gar",
                                        "@P_meses_fac_calc_gar","@P_no_cuenta_consi_gar","@P_dia_max_const_gar","@P_dia_min_const_gar","@P_hora_max_declara_opera",
                                        "@P_no_horas_correcion_primario", "@P_no_dias_cal_correc_secundario", "@P_permite_ing_cont_ext", "@P_no_horas_actualiza_contrato",
                                        "@P_hora_ini_inf_ope_ext","@P_hora_fin_inf_ope_ext","@P_dias_reg_usr_no_reg","@P_requiere_aprob_cargue","@P_verifica_manual",
                                        "@P_no_dias_hab_regi_cont_mayor","@P_no_dias_hab_corr_cont_mayor","@P_verifica_manual_cont_mayor", // Campos nuevos mercado mayorista 20150616
                                        "@P_mail_gestor", "@P_dia_hab_publica_inf_mayor",
                                        "@P_hora_max_prog_sum","@P_hora_max_prog_sum_post","@P_fecha_max_prog_sum","@P_hora_max_prog_trans","@P_hora_max_prog_trans_post","@P_fecha_max_prog_trans", // campos de programacion definivita 20151123
                                        "@P_mercado_act_precio","@P_no_dias_apro_cambio_precio","@P_fecha_ini_act_precio","@P_fecha_fin_act_precio","@P_fecha_ini_vigencia_precio", // Campos nuevos cambio precio indexador 20151211
                                        "@P_dia_hab_ejec_inf", //dia de publicacion de infromacion operativa 20160108
                                        "@P_no_dias_modifica_factura", // Campo nuevo ajustes facturacion 20160104
                                        "@P_fecha_max_corr_usr_fin", // Campo nuevo sector de consumo 20160118
                                        "@P_fecha_ini_curva","@P_fecha_fin_curva",  // fechas para curvas 20160523
                                        "@P_fecha_max_fuente", //fecha fuente 20160607
                                        "@P_dia_hab_pub_ofe_comp", //oferta comprometida 20160608
                                        "@P_codigo_menu_visual_archivos", //Campo nuevo Reg.58 20160701
                                        "@P_fecha_max_ptdv", // alcande PTDV 20160721
                                        "@P_dia_pub_map_int", //Req. 079 Mapa Intetractivo 20170314
                                        "@P_fecha_ini_reg_demanda","@P_hora_ini_reg_demanda","@P_fecha_fin_reg_demanda","@P_hora_fin_reg_demanda", // Campos nuevos Req.009-17 Indicadores Fase II 20170307
                                        "@P_contratos_precio" , //20170804 rq037-17
                                        "@P_fecha_pub_ptdvf",  //20170803 rq034-17
                                        "@P_dias_hab_mod_cont", "@P_dias_hab_aprob_mod",  //20171017 rq026-17
                                        "@P_no_max_sol_pre",  //20180302 rq004-18
                                        "@P_hora_vig_token","@P_dia_vig_firma","@P_nombre_firma_qr_bmc","@P_dia_vig_impresion", //20181210 rq046-18
                                        "@P_dia_prog_sum","@P_dia_prog_sum_post","@P_hora_max_prog_sum_dia","@P_dia_prog_sum_dia","@P_dia_prog_trans","@P_dia_prog_trans_post","@P_hora_max_prog_trans_dia","@P_dia_prog_trans_dia", //20190318 rq017-19
                                        "@P_dia_ingreso_ejecucion","@P_hora_max_ejecucion", //20190322 rq018-19 ajuste transaccional
                                        "@P_replica_nom_tra",//20190502 rq024-19
                                        "@P_dia_habil_reg_int","@P_dia_mes_ini_int","@P_dia_mes_int_ini","@P_dia_mes_int_fin",//20190404 rq018-19 fase II
                                        "@P_dia_prev_ing_ejec",//20190524 rq029-19
                                        "@P_replica_nom_sum",//20190712
                                        "@P_mes_base_fact", "@P_dia_cons_gar_crt", "@P_dia_lib_gar_crt" //20210422 ajsute factiracion
                                      };

        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar,
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int,SqlDbType.Int,SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.Char,SqlDbType.Char,SqlDbType.Int,SqlDbType.Int,SqlDbType.Char, SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, // Campos nuevos mercado mayorista 20150616
                                        SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, // Campos nuevos cambio precio indexador 20151211
                                        SqlDbType.Int, //dia de publicacion de infromacion operativa 20160108
                                        SqlDbType.Int, // Campo nuevo ajustes facturacion 20160104
                                        SqlDbType.VarChar, // Campo nuevo sector de consumo 20160118
                                        SqlDbType.VarChar,SqlDbType.VarChar,  // fecha de curvas 20160523
                                        SqlDbType.VarChar,  // fecha actualiación fuente 20160607
                                        SqlDbType.Int, // oferta comprometida 20160608
                                        SqlDbType.Int, //Campo nuevo Reg.58 20160701
                                        SqlDbType.VarChar,  // alcance PTDV 20160721
                                        SqlDbType.Int, //Req. 079 Mapa Interactivo 20170314
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, // Campos nuevos Req.009-17 Indicadores Fase II 20170307
                                        SqlDbType.Int,  //20170804 rq037-17
                                        SqlDbType.VarChar,  //20170803 rq034-17
                                        SqlDbType.Int, SqlDbType.Int , //20171017 rq026-17
                                        SqlDbType.Int, //20180302 rq004-18
                                        SqlDbType.Int ,SqlDbType.Int ,SqlDbType.VarChar,SqlDbType.Int, //20181210 rq046-18
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, //20180318 rq017-19
                                        SqlDbType.VarChar,SqlDbType.VarChar,//20190322 rq018-19 ajuste transaccional
                                        SqlDbType.VarChar,//20190502 rq024-19
                                        SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//20190404 rq018-19 fase II
                                        SqlDbType.Int,//20190524 rq029-19
                                        SqlDbType.VarChar,//20190712
                                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int//20210422 ajsute factiracion
                                      };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "", "2", "", "", "0", "", "", "", "", "0", "0", "0", "0", "0", "", "0", "0", "", "0", "0", "", "0", "", "", "0", "S", "S", "0", "0", "N", "", "0", "", "", "", "", "", "", "", "0", "", "", "", "", "0", "", "", "", "", "0", "0", "",  //fecha fuente 20160607 //oferta comprometida 20160608 //alcance PTDV 20160721
                                      "0",  //Req. 079 Mapa Interactivo 20170314
                                      "","","","", // Campos nuevos Req.009-17 Indicadores Fase II 20170307
                                      "0", //20170804 rq037-17
                                      "", //20170803 rq034-17
                                      "0", "0", //20171017 rq026-17
                                      "0", //20180302 rq004-18
                                      "0","0","", "0",//20181210 rq046-18
                                      "","","","","","","","",//20180318 rq017-19
                                      "","",//20190322 rq018-19 ajuste transaccional
                                      "",//20190502 rq024-19
                                      "0","","","",//20190404 rq018-19 fase II
                                      "",//20190524 rq029-19
                                      "",//20190712
                                      "0","0","0"//20210422 ajsute factiracion
                                    };
        lblMensaje.Text = "";
        string sRuta = sRutaArc;
        DateTime ldFecha;
        int liValor = 0;
        try
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaCamFac.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha Cambio Proceso Facturación.<br>";
            }
            if (TxtNoHorasCorContPrim.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Horas Correción Contrato Primario. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoHorasCorContPrim.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Horas Correción Contrato Primario. <br>";
                }
            }
            if (TxtNoDiasCorContPrim.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Dias Calendario Correción Contrato Secundario. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoDiasCorContPrim.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Dias Calendario Correción Contrato Secundario. <br>";
                }
            }
            if (TxtNoHorasActualCont.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Horas Actualización Contrato Verificación. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtNoHorasActualCont.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Horas Actualización Contrato Verificación. <br>";
                }
            }
            if (TxtHoraIniExt.Text == "")
                lblMensaje.Text += " Debe digitar la hora inicial de registro extemporáneo de información operativa. <br>";
            if (TxtHoraFinExt.Text == "")
                lblMensaje.Text += " Debe digitar la hora final de registro extemporáneo de información operativa. <br>";
            try
            {
                if (Convert.ToDateTime(TxtHoraIniExt.Text) >= Convert.ToDateTime(TxtHoraFinExt.Text))
                    lblMensaje.Text += " La hora final de registro extemporáneo de información operativa debe ser mayor que la inicial. <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += " Las horas de registro extemporáneo de información operativa n son válidas. <br>";
            }
            if (txtDiaUNR.Text == "")
                lblMensaje.Text += " Debe digitar dìas para registro de información operativa del mes anterior de usuarios no regulados. <br>";
            // Validaciones campos nuevos mercado mayorista 20150616
            if (TxtDiaHabRegMayor.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Dias Habiles Registro Contrato Mayorista. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtDiaHabRegMayor.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Dias Habiles  Registro Contrato Mayorisya. <br>";
                }
            }
            if (TxtDiaHabCorrMayor.Text.Trim().Length <= 0)
                lblMensaje.Text += " Debe Ingresar No. Dias Habiles Corección Contrato Mayorista. <br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtDiaHabCorrMayor.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += " Valor Inválido en Campo No. Dias Habiles  Corección Contrato Mayorisya. <br>";
                }
            }
            // Validacion del Mail del Gestor 20150715
            if (TxtMailGestor.Text == "")
                lblMensaje.Text += " Debe Ingresar el Mail del Gestor. <br>";

            if (TxtDiaPubMercMay.Text == "")
                lblMensaje.Text += " Debe digitar los días de publicación de mercado mayorista. <br>";
            else
            {
                if (Convert.ToInt16(TxtDiaPubMercMay.Text) <= 0)
                    lblMensaje.Text += " Los días de publicación de mercado mayorista deben se mayores que cero. <br>";
            }
            
            // Validaciones campos nuevos paametros precio indexador 20151211
            if (TxtNoDiaAproCambioPrec.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo No. Días Calendario Aprobación Cambio Precio Indexador. <br>";
            if (TxtDiaPubOpe.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar los días de hábiles de publicación de información operativa. <br>";
            if (TxtFechaIniCamPrecio.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo Fecha Inicial Permite Cambio Precio Indexador. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniCamPrecio.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial Permite Cambio Precio Indexador.<br>";
                }
            }
            if (TxtFechaFinCamPrecio.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo Fecha Final Permite Cambio Precio Indexador. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFinCamPrecio.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtFechaIniCamPrecio.Text.Trim()))
                        lblMensaje.Text += "La Fecha Final Prmite Cambio Precio No puede ser menor a la Fecha Inicial.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Final Permite Cambio Precio Indexador.<br>";
                }
            }
            if (TxtFechaIniVigPrecioIndexa.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo Fecha Inicial Vigencia Nuevo Precio Indexador. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniVigPrecioIndexa.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial Vigencia Nuevo Precio Indexador.<br>";
                }
            }
            /// Campo nuevo ajustes facturacion 20160104
            if (TxtNoDiasModifFactura.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar campo No. Días Calendario Modificación Facturación. <br>";
            //validacion de fecha de curvas 20160523
            string lsFecha = "N";
            ldFecha = DateTime.Now;
            if (TxtFechaIniCurva.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la fecha inicial de consulta de curvas. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniCurva.Text.Trim());
                    lsFecha = "S";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el Campo Fecha inicial de consulta de curvas.<br>";
                }
            }
            if (TxtFechaFinCurva.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la fecha final de consulta de curvas. <br>";
            else
            {
                try
                {
                    if (lsFecha == "S")
                    {
                        if (ldFecha > Convert.ToDateTime(TxtFechaFinCurva.Text.Trim()))
                            lblMensaje.Text += "La fecha inicial de consulta de curvas debe ser menor o igual que la fecha final.<br>";
                    }
                    else
                        ldFecha = Convert.ToDateTime(TxtFechaFinCurva.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el Campo Fecha final de consulta de curvas.<br>";
                }
            }
            //valida fecha maxima de actualizaciond e fuentes
            if (TxtFechaFuente.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la fecha máxima de actualización de fuente de contratos registrados. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFuente.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el Campo Fecha màxima de actualización de fuente en contratos registrados.<br>";
                }
            }
            //Oferta comprometida 20160608
            if (TxtDiaOferta.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe ingresar el día hábil de publicación de la oferta comprometida. <br>";
            else
            {
                try
                {
                    if (Convert.ToInt16(TxtDiaOferta.Text) <= 0)
                        lblMensaje.Text += "El día hábil de publicación de la oferta comprometida debe ser mayor que cero.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el día de publicación de la oferta comprometida.<br>";
                }
            }
            // Validacion Menu Visualiza Archivos Reg. 58 20160701
            if (ddlMenuVisualizaArchivos.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el Menú de Visualización de Archivos. <br>";
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para la modificación. <br>";
            //alcance PTDV 20160721
            if (TxtFechaPtdv.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe ingresar la fecha màxima para el registro de la información PTDV. <br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaPtdv.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "valor inválido en el campo fecha màxima de registro de información PTDV.<br>";
                }
            }
            ///////////////////////////////////////////////////////////////////
            /// Validaciones nuevas Req.009-17 Indicadores Fase II 20170307 ///
            ///////////////////////////////////////////////////////////////////
            if (TxtFecIniDema.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Inicial Registro Demanda Regulada. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFecIniDema.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial Registro Demanda Regulada.<br>";
                }
            }
            if (TxtHoraIniDema.Text == "")
                lblMensaje.Text += "Debe digitar la hora inicial registro demanda regulada. <br>";
            if (TxtFecFinDema.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Final Registro Demanda Regulada. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFecFinDema.Text.Trim());
                    if (Convert.ToDateTime(TxtFecFinDema.Text.Trim()) < Convert.ToDateTime(TxtFecIniDema.Text.Trim()))
                        lblMensaje.Text += "La Fecha Final Registro Demanda Regulada debe ser mayor o igual a la Fecha Inicial.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha Final Registro Demanda Regulada.<br>";
                }
            }
            if (TxtHoraFinDema.Text == "")
                lblMensaje.Text += "Debe digitar la hora final registro demanda regulada. <br>";
            ///FIn indidaes CREG rq009-17
            //20170804 rq037-17
            if (TxtOperPre.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el número mínimo de contratos para publicación de precio en el BEC. <br>";
            //20170803 rq034-17
            if (TxtFechaPtdvf.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha de publicación de la PTDVF. <br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFechaPtdvf.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en Campo Fecha de publicación de la PTDVF.<br>";
                }
            }
            //20171017 rq026-17
            if (txtDiaRegMod.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar los días hábiles para aprobación de las modificaciones por parte de la contraparte. <br>";
            else
                try
                {
                    if (Convert.ToInt32(txtDiaRegMod.Text.Trim()) <= 1)
                        lblMensaje.Text += "Los días hábiles para aprobación de las modificaciones por parte de la contraparte deben ser mayores que 1. <br>";
                }
                catch { }
            //20171017 rq026-17
            if (txtDiaRegMod.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar los días hábiles para aprobación de las modificaciones por parte del gestor. <br>";
            try
            {
                if (Convert.ToInt32(txtDiaRegMod.Text.Trim()) <= 1)
                    lblMensaje.Text += "Los días hábiles para aprobación de las modificaciones por parte del gestor deben ser mayores que 1. <br>";
            }
            catch { }
            //20180302 rq004-18
            if (TxtMaxSolPrec.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el número máximo de solicitudes de cambio de precio por año<br>";
            //20181210 rq046-18
            if (TxtHoraVigTok.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las horas de vigencia del token. <br>";
            //20181210 rq046-18
            if (TxtDiaVigFirma.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las días de vigencia de la firma. <br>";
            //20181210 rq046-18
            if (TxtNombreFirmaBmc.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el nombre del usuario de la BMC que firma el certificado. <br>";
            //20181210 rq046-18
            if (TxtDiaImpCert.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las días de vigencia de impresión del certificado. <br>";
            //20190318 rq017-19
            if (ddlDiaNomSum.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de suministro. <br>";
            if (txtHoraSum.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de suministro. <br>";
            if (ddlDiaNomDiaSum.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación de nominación del día de gas de suministro. <br>";
            if (txtHoraSumDia.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación de nominación del día de gas de suministro. <br>";
            if (ddlDiaPostSum.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de suministro posterior a la re-nominación. <br>";
            if (txtHoraSumPost.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de suministro posterior a la re-nominación. <br>";
            if (ddlDiaNomTrans.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de transporte. <br>";
            if (txtHoraTrans.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de transporte. <br>";
            if (ddlDiaNomDiaTrans.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación de nominación del día de gas de transporte. <br>";
            if (txtHoraDiaTrans.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación de nominación del día de gas de transporte. <br>";
            if (ddlDiaPostTrans.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día del reporte programación definitiva de transporte posterior a la re-nominación. <br>";
            if (txtHoraTransPost.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de reporte programación definitiva de transporte posterior a la re-nominación. <br>";
            //20190502 rq024-19
            //if (txtFechaSum.Text == "")
            //    lblMensaje.Text += "Debe digitar la fecha máxima de reporte histórico de programación definitiva de suministro posterior a la re-nominación. <br>";
            //20190502 rq024-19
            //if (txtFechaTrans.Text == "")
            //    lblMensaje.Text += "Debe digitar la fecha máxima de reporte histórico de programación definitiva de transporte posterior a la re-nominación. <br>";
            //20190318 Fin rq017-19
            //20190322 rq018-19 ajuste transaccional
            if (ddlEjecucion.SelectedValue == "")
                lblMensaje.Text += "Debe seleccionar el día de ingreso de la ejecución del contrato. <br>";
            if (txtHoraEjec.Text == "")
                lblMensaje.Text += "Debe digitar la hora máxima de ingreso de la ejecución del contrato. <br>";
            //20190322  rq018-19  fin ajuste transaccional  
            //20190524 rq029-19
            if (txtDiaPrevEjec.Text == "")
                lblMensaje.Text += "Debe digitar los días previos para el ingreso de la ejecución del contrato. <br>";
            //20190404 rq018-19
            if (txtRegSumInt.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar las días de registro de contratos de suministro con interrupciones. <br>";
            if (txtMesDiaInt.Text == "")
                lblMensaje.Text += "Debe digitar el mes/día de inicio para control de negociaciones de suministro con interrupciones de mercado secundario. <br>";
            else
                try
                {
                    Convert.ToDateTime("2000/" + txtMesDiaInt.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El mes/día de inicio para control de negociaciones de suministro con interrupciones de mercado secundario no es válido. <br>";
                }
            if (txtMesDiaIniReg.Text == "")
                lblMensaje.Text += "Debe digitar el mes/día inicial de registro negociaciones de suministro con interrupciones de mercado secundario. <br>";
            else
                try
                {
                    Convert.ToDateTime("2000/" + txtMesDiaIniReg.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El mes/día inicial de registro negociaciones de suministro con interrupciones de mercado secundario no es válido. <br>";
                }
            if (txtMesDiaFinReg.Text == "")
                lblMensaje.Text += "Debe digitar el mes/día final de registro negociaciones de suministro con interrupciones de mercado secundario. <br>";
            else
                try
                {
                    Convert.ToDateTime("2000/" + txtMesDiaFinReg.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El mes/día final de registro negociaciones de suministro con interrupciones de mercado secundario no es válido. <br>";
                }
            //20190404 fin rq018-19 fase II
            //20210422 ajuste factruacion
            if (ddlMesfact.SelectedValue == "0")
                lblMensaje.Text += "Debe digitar el mes base para la facturación . <br>";
            if (txtDiaConsGar.Text == "")
                lblMensaje.Text += "Debe digitar los días para constitución de garantías. <br>";
            if (txtDiaLibGar.Text == "")
                lblMensaje.Text += "Debe digitar los días para liberacíón de garantías. <br>";
            //20210422 ajuste fin factruacion
            if (lblMensaje.Text == "")
            {
                if (FuArchivo.FileName.Trim().Length > 0)
                    FuArchivo.SaveAs(sRuta + FuArchivo.FileName);

                lValorParametros[0] = TxtValIncOferta.Text.Trim();
                lValorParametros[1] = TxtCantMinNego.Text.Trim();
                lValorParametros[2] = TxtValHumExcDema.Text.Trim();
                lValorParametros[3] = TxtValVigMinis.Text.Trim();
                lValorParametros[4] = TxtDiasCont.Text.Trim();
                lValorParametros[5] = TxtObservacion.Text.Trim();
                lValorParametros[7] = ddlMostAgreTod.SelectedValue;
                lValorParametros[8] = ddlMostAgreOfe.SelectedValue;
                lValorParametros[9] = TxtDiaMaxTras.Text.Trim();
                lValorParametros[10] = TxtNomFirma.Text.Trim();
                lValorParametros[11] = TxtCarFirma.Text.Trim();
                if (FuArchivo.FileName.Trim().Length > 0)
                    lValorParametros[12] = FuArchivo.FileName.Trim();
                else
                    lValorParametros[12] = hdfNomArchivo.Value;
                lValorParametros[13] = TxtFechaCamFac.Text.Trim();
                lValorParametros[14] = TxtDiaCortFact.Text.Trim();
                lValorParametros[15] = TxtAnobasLiq.Text.Trim();
                lValorParametros[16] = TxtDiaHabVecnFac.Text.Trim();
                lValorParametros[17] = TxtDiaCalGar.Text.Trim();
                lValorParametros[18] = TxtNoMesCalGar.Text.Trim();
                lValorParametros[19] = TxtNoCuentaConsGar.Text.Trim();
                lValorParametros[20] = TxtDiaMaxConsGar.Text.Trim();
                lValorParametros[21] = TxtDiaMinLibGar.Text.Trim();
                lValorParametros[22] = TxtHorMaxDecOpera.Text.Trim();
                lValorParametros[23] = TxtNoHorasCorContPrim.Text.Trim();
                lValorParametros[24] = TxtNoDiasCorContPrim.Text.Trim();
                lValorParametros[25] = ddlPermiteRegCont.SelectedValue;
                lValorParametros[26] = TxtNoHorasActualCont.Text.Trim();
                lValorParametros[27] = TxtHoraIniExt.Text.Trim();
                lValorParametros[28] = TxtHoraFinExt.Text.Trim();
                lValorParametros[29] = txtDiaUNR.Text.Trim();
                lValorParametros[30] = ddlAprob.SelectedValue;
                lValorParametros[31] = ddlVerifMan.SelectedValue;
                // Campos nuevos contrato mayorista 20150616
                lValorParametros[32] = TxtDiaHabRegMayor.Text.Trim();
                lValorParametros[33] = TxtDiaHabCorrMayor.Text.Trim();
                lValorParametros[34] = ddlVerifManMayo.SelectedValue;
                // Campos nuevos validacoin del mail del gestor 20150715
                lValorParametros[35] = TxtMailGestor.Text;
                lValorParametros[36] = TxtDiaPubMercMay.Text;
                // Campso paar programacion definitia 20151123
                lValorParametros[37] = txtHoraSum.Text;
                lValorParametros[38] = txtHoraSumPost.Text;
                //lValorParametros[39] = txtFechaSum.Text; //20190502 rq024-19
                lValorParametros[40] = txtHoraTrans.Text;
                lValorParametros[41] = txtHoraTransPost.Text;
                //lValorParametros[42] = txtFechaTrans.Text; //20190502 rq024-19
                // Campos nuevos precio indexador 20151211
                lValorParametros[43] = ddlMercadoActPrecio.SelectedValue;
                lValorParametros[44] = TxtNoDiaAproCambioPrec.Text.Trim();
                lValorParametros[45] = TxtFechaIniCamPrecio.Text.Trim();
                lValorParametros[46] = TxtFechaFinCamPrecio.Text.Trim();
                lValorParametros[47] = TxtFechaIniVigPrecioIndexa.Text.Trim();
                lValorParametros[48] = TxtDiaPubOpe.Text.Trim();
                // Campo nuevo ajustes facturacion 20160104
                lValorParametros[49] = TxtNoDiasModifFactura.Text.Trim();
                // Campo nuevo ajustes sector de consumo 20160108
                lValorParametros[50] = TxtFechaUsrFin.Text.Trim();
                lValorParametros[51] = TxtFechaIniCurva.Text.Trim();
                lValorParametros[52] = TxtFechaFinCurva.Text.Trim();
                //Cambio fuente 20160607
                lValorParametros[53] = TxtFechaFuente.Text.Trim();
                //oferta comprometida 20160608
                lValorParametros[54] = TxtDiaOferta.Text.Trim();
                // Campo nuevo Reg. 58 20160701
                lValorParametros[55] = ddlMenuVisualizaArchivos.SelectedValue;
                // alcance PTDV 20160721
                lValorParametros[56] = TxtFechaPtdv.Text.Trim();
                // Req. 079 Mapa Interactivo 20170314
                lValorParametros[57] = TxtNoDiaPubMapa.Text.Trim();
                //////////////////////////////////////////////////////////////
                /// Campos nuevos Req. 009-17 Indicadores Fase II 20170307 ///
                //////////////////////////////////////////////////////////////
                lValorParametros[58] = TxtFecIniDema.Text.Trim();
                lValorParametros[59] = TxtHoraIniDema.Text.Trim();
                lValorParametros[60] = TxtFecFinDema.Text.Trim();
                lValorParametros[61] = TxtHoraFinDema.Text.Trim();
                //20170804 rq037-17
                lValorParametros[62] = TxtOperPre.Text.Trim();
                //20170803 rq034-17
                lValorParametros[63] = TxtFechaPtdvf.Text.Trim();
                //20171017 rq026-17
                lValorParametros[64] = txtDiaRegMod.Text.Trim();
                //20171017 rq026-17
                lValorParametros[65] = txtDiaRegModApr.Text.Trim();
                lValorParametros[66] = TxtMaxSolPrec.Text;  //20180302 rq004-18
                lValorParametros[67] = TxtHoraVigTok.Text; //20181210 rq046-18
                lValorParametros[68] = TxtDiaVigFirma.Text; //20181210 rq046-18
                lValorParametros[69] = TxtNombreFirmaBmc.Text; //20181210 rq046-18
                lValorParametros[70] = TxtDiaImpCert.Text; //20181210 rq046-18
                //20190318 rq017-19
                lValorParametros[71] = ddlDiaNomSum.SelectedValue;
                lValorParametros[72] = ddlDiaPostSum.SelectedValue;
                lValorParametros[73] = txtHoraSumDia.Text;
                lValorParametros[74] = ddlDiaNomDiaSum.SelectedValue;
                lValorParametros[75] = ddlDiaNomTrans.SelectedValue;
                lValorParametros[76] = ddlDiaPostTrans.SelectedValue;
                lValorParametros[77] = txtHoraDiaTrans.Text;
                lValorParametros[78] = ddlDiaNomDiaTrans.SelectedValue;
                //20190318 fin rq017-19
                lValorParametros[79] = ddlEjecucion.SelectedValue; //20190322 rq018-19 ajuste transaccional
                lValorParametros[80] = txtHoraEjec.Text; //20190322 rq018-19 ajuste transaccional
                lValorParametros[81] = ddlReplicaNomTra.SelectedValue;//20190502 rq024-19
                lValorParametros[82] = txtRegSumInt.Text; //20190404 rq018-19 fase II
                lValorParametros[83] = txtMesDiaInt.Text;//20190404 rq018-19 fase II
                lValorParametros[84] = txtMesDiaIniReg.Text;//20190404 rq018-19 fase II
                lValorParametros[85] = txtMesDiaFinReg.Text;//20190404 rq018-19 fase II
                lValorParametros[86] = txtDiaPrevEjec.Text;//20190524 rq029-19
                lValorParametros[87] = ddlReplicaNomSum.SelectedValue;//20190712
                lValorParametros[88] = ddlMesfact.SelectedValue;//20210422 ajsute factruacion
                lValorParametros[89] = txtDiaConsGar.Text;//20210422 ajsute factruacion
                lValorParametros[90] = txtDiaLibGar.Text;//20210422 ajsute factruacion

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosGenerales", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de los Parómetros Generales .!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_generales' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
    protected void bntRuedas_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_ind_proceso" };

        SqlDbType[] lTipoparametros = { SqlDbType.Char };
        string[] lValorParametros = { "M" };
        lConexion.Abrir();
        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetRuedaAut", lsNombreParametros, lTipoparametros, lValorParametros);
        if (lLector.HasRows)
        {
            lLector.Read();
            lblMensaje.Text = lLector["correo"].ToString();
        }
        else
            lblMensaje.Text = "NO se encontraron ruedas para crear";
    }
}