﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_rutaEsp : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Rutas Especiales del SNT";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlPozoIni, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPozoFin, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();

            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_ruta_snt");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[6].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[7].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoRuta.Visible = false;
        LblCodigoRuta.Visible = true;
        LblCodigoRuta.Text = "Automatico";
        ddlEstado.Enabled = false;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_ruta_esp", " codigo_ruta_esp = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoRuta.Text = lLector["codigo_ruta_esp"].ToString();
                        TxtCodigoRuta.Text = lLector["codigo_ruta_esp"].ToString();
                        try
                        {
                            ddlPozoIni.SelectedValue = lLector["codigo_pozo_ini"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El punto inicial del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlPozoFin.SelectedValue = lLector["codigo_pozo_fin"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El punto final del registro no existe o esta inactivo<br>";
                        }
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoRuta.Visible = false;
                        LblCodigoRuta.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    ddlPozoIni.Enabled = false;
                    ddlPozoFin.Enabled = false;
                    ddlEstado.Enabled = true;
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado. Codigo Ruta Especial" + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_ruta_esp", "@P_descripcion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar};
        string[] lValorParametros = { "0", ""};

        try
        {
            if (TxtBusCodigoRuta.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigoRuta.Text.Trim();
            if (TxtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRutaEsp", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_ruta_esp", "@P_codigo_pozo_ini", "@P_codigo_pozo_fin", "@P_descripcion", "@P_costo_actual", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Decimal, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { "0", "0", "0",  "", "0", "P", "1" };
        lblMensaje.Text = "";

        try
        {
            if (ddlPozoIni.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el punto del SNT inicial. <br>";
            if (ddlPozoFin.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el punto del SNT final. <br>";
            if (VerificarExistencia("m_ruta_snt", " codigo_pozo_ini =" + ddlPozoIni.SelectedValue + " and codigo_pozo_fin=" + ddlPozoFin.SelectedValue))
                lblMensaje.Text += " La ruta seleccionada ya existe en el sistema. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = ddlPozoIni.SelectedValue;
                lValorParametros[2] = ddlPozoFin.SelectedValue;
                lValorParametros[3] = ddlPozoIni.SelectedItem + " - " + ddlPozoFin.SelectedItem;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRutaEsp", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la Ruta Especial.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_ruta_esp", "@P_estado", "@P_accion"};
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar};
        string[] lValorParametros = { LblCodigoRuta.Text , ddlEstado.SelectedValue, "2" };
        lblMensaje.Text = "";
        try
        {
            if (ddlEstado.SelectedValue == "A" && !VerificarExistencia("m_ruta_esp_det", " codigo_ruta_esp = " + LblCodigoRuta.Text ))
                lblMensaje.Text += "No se puede activar la ruta porque no tiene tramos definidos. <br>";
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRutaEsp", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la ruta.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoRuta.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoRuta.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        /// Desbloquea el Registro Actualizado
        if (LblCodigoRuta.Text != "")
            manejo_bloqueo("E", LblCodigoRuta.Text);
        Listar();

    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
            lblMensaje.Text = "";
            if (((LinkButton)e.CommandSource).Text == "Modificar")
            {
                lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                Modificar(lCodigoRegistro);
            }
            if (((LinkButton)e.CommandSource).Text == "Tramos")
            {
                lblCodRuta1.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                lblDescPunto.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[3].Text;
                lblCodPozF.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[2].Text;
                hndPozoIni.Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
                hndPozoFin.Value = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
                LlenarTramo(lblCodRuta1.Text);
                Tramos(lblCodRuta1.Text);
            }

    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_ruta_esp' and llave_registro='codigo_ruta_esp=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_ruta_esp=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_ruta_esp";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_ruta_esp", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigoRuta.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigoRuta.Text.Trim();
                lsParametros += " Código Ruta Especial: " + TxtBusCodigoRuta.Text;

            }
            if (TxtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
                lsParametros += " - Descripcion: " + TxtBusDescripcion.Text;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetRutaEsp&nombreParametros=@P_codigo_ruta_esp*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_ruta*codigo_trasportador*nombre_trasportador*codigo_pozo_ini*codigo_pozo_fin*descripcion*costo_actual&titulo_informe=Listado de rutas del SNT&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_ruta_esp <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_ruta_snt&procedimiento=pa_ValidarExistencia&columnas=codigo_ruta*codigo_trasportador*codigo_pozo_ini*codigo_pozo_fin*descripcion*costo_actual*estado&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: tramos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Tramos(string codigo)
    {
        if (codigo != null && codigo != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", codigo))
                {
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", codigo);
                    //Actualiza la grilla de tramos
                    listar_tramo();

                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Pueden modificar los tramos porque el registro está Bloqueado. Codigo Ruta Especial " + codigo;

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblTramo.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "DEFINICION DE TRAMOS ESPECIALES A " + lsTitulo;
            imbActualizaTra.Visible = false;
            imbCrearTra.Visible = true;
        }
    }
    /// <summary>
    /// Nombre: imbCrearTra_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrearTra_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";

        try
        {
            if (ddlTramo.SelectedValue == "0")
                lblMensaje.Text = "Debe seleccionar el tramo<br>";
            if (VerificarExistencia("m_ruta_esp_det", "codigo_ruta_esp=" + lblCodRuta1.Text + " and codigo_tramo=" + ddlTramo.SelectedValue))
                lblMensaje.Text += "Ya existe el tramo seleccionado para la ruta especial<br>";
            if (lblCodPozF.Text == hndPozoFin.Value)
                lblMensaje.Text += "No puede agregar mas tramos porque ya llegó al pozo final<br>";
            if (lblMensaje.Text == "")
            {
                string[] lsNombreParametros = { "@P_codigo_ruta_esp", "@P_codigo_tramo" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { lblCodRuta1.Text, ddlTramo.SelectedValue };

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRutaEspDet", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de la ruta especial.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    listar_tramo();
                    LlenarTramo(lblCodRuta1.Text);
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void listar_tramo()
    {
        string[] lsNombreParametros = { "@P_codigo_ruta_esp" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { lblCodRuta1.Text };

        lConexion.Abrir();
        dtgTramo.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRutaEspDet", lsNombreParametros, lTipoparametros, lValorParametros);
        dtgTramo.DataBind();
        lConexion.Cerrar();
        hndCuenta.Value = this.dtgTramo.Items.Count.ToString();
        hndPozoFin.Value = hndPozoIni.Value;
        foreach (DataGridItem Grilla in this.dtgTramo.Items)
        {
            hndOrden.Value = Grilla.Cells[6].Text;
            hndPozoFin.Value = Grilla.Cells[4].Text;
        }
    }
    /// <summary>
    /// Nombre: dtgTramo_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgTramo_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        //if (((LinkButton)e.CommandSource).Text == "Modificar")
        //{
        //    lblCodTramo.Text = this.dtgTramo.Items[e.Item.ItemIndex].Cells[0].Text;
        //    txtPtoIni.Text = this.dtgTramo.Items[e.Item.ItemIndex].Cells[2].Text; ;
        //    txtPtoFin.Text = this.dtgTramo.Items[e.Item.ItemIndex].Cells[3].Text; ;
        //    imbActualizaTra.Visible = true;
        //    imbCrearTra.Visible = false;
        //}
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            if (hndOrden.Value == this.dtgTramo.Items[e.Item.ItemIndex].Cells[6].Text)
            {
                string[] lsNombreParametros = { "@P_codigo_ruta_esp_det" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                string[] lValorParametros = { this.dtgTramo.Items[e.Item.ItemIndex].Cells[0].Text };

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_DelRutaEspDet", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la eliminación del tramo.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    listar_tramo();
                    LlenarTramo(lblCodRuta1.Text);
                }
            }
            else
                lblMensaje.Text = "No se puede eliminar un tramo intermedia";
        }
    }
    /// <summary>
    /// Nombre: imbActualizaTra_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void imbActualizaTra_Click(object sender, ImageClickEventArgs e)
    //{
    //    lblMensaje.Text = "";
    //    try
    //    {
    //        if (txtPtoIni.Text == "")
    //            lblMensaje.Text = "Debe digitar el punto inicial<br>";
    //        if (txtPtoFin.Text == "")
    //            lblMensaje.Text += "Debe digitar el punto final<br>";
    //        if (VerificarExistencia("m_tramo", "codigo_ruta=" + lblCodRuta1.Text + " and punto_inicio='" + txtPtoIni.Text + "' and codigo_tramo<>" + lblCodTramo.Text))
    //            lblMensaje.Text += "Ya existe un tramo con el punto inicial digitado<br>";
    //        if (VerificarExistencia("m_tramo", "codigo_ruta=" + lblCodRuta1.Text + " and punto_fin='" + txtPtoFin.Text + "' and codigo_tramo<>" + lblCodTramo.Text))
    //            lblMensaje.Text += "Ya existe un tramo con el punto inicial digitado<br>";
    //        if (lblMensaje.Text == "")
    //        {
    //            string[] lsNombreParametros = { "@P_codigo_tramo", "@P_punto_ini", "@P_punto_fin" };
    //            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar };
    //            string[] lValorParametros = { lblCodTramo.Text, txtPtoIni.Text, txtPtoFin.Text };

    //            lConexion.Abrir();
    //            if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_UptTramo", lsNombreParametros, lTipoparametros, lValorParametros))
    //            {
    //                lblMensaje.Text = "Se presentó un Problema en la Actualizacion del tramo.!";
    //                lConexion.Cerrar();
    //            }
    //            else
    //            {
    //                lConexion.Cerrar();
    //                listar_tramo();
    //                imbCrearTra.Visible = true;
    //                imbActualizaTra.Visible = false;
    //                txtPtoIni.Text = "";
    //                txtPtoFin.Text = "";
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        /// Desbloquea el Registro Actualizado
    //        lblMensaje.Text = ex.Message;
    //    }
    //}
    /// <summary>
    /// Nombre: imbSalirTra_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalirTra_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        if (hndCuenta.Value != "0" && lblCodPozF.Text != hndPozoFin.Value)
            lblMensaje.Text += "No puede salir porque no ha llegado al punto final <br>";
        else
        {

            /// Desbloquea el Registro Actualizado
            if (lblCodRuta1.Text != "")
                manejo_bloqueo("E", lblCodRuta1.Text);
            tblTramo.Visible = false;
            Listar();
        }
    }
    /// <summary>
    /// Nombre: LlenarTramo
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarTramo(string lsCodRuta)
    {
        ddlTramo.Items.Clear();

        lConexion.Abrir();
        SqlDataReader lLector;
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        ddlTramo.Items.Add(lItem);

        string[] lsNombreParametros = { "@P_codigo_pozo_ini" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { hndPozoFin.Value  };


        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetTramoEsp1", lsNombreParametros, lTipoparametros, lValorParametros);
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {

                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(0).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                ddlTramo.Items.Add(lItem1);
            }
        }
        lLector.Close();
        lConexion.Cerrar();
    }

}
