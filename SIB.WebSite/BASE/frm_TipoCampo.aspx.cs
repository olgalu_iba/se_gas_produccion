﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_TipoCampo : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Tipos de Campo";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlMesIni, "m_mes", " 1=1 order by mes", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlMesFin, "m_mes", " 1=1 order by mes", 0, 1);
            lConexion.Cerrar();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_tipo_campo");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[11].Visible = (Boolean)permisos["UPDATE"]; //Req. 007-17 Subasta Bimestral 20170209 //rq009-17 //20170705 rq025-17 indicadores MP fase III
        dtgMaestro.Columns[12].Visible = false; //Req. 007-17 Subasta Bimestral 20170209  //rq009-17 //20170705 rq025-17 indicadores MP fase III
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoCampo.Visible = false;
        LblCodigoCampo.Visible = true;
        LblCodigoCampo.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_tipo_campo", " codigo_tipo_campo = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoCampo.Text = lLector["codigo_tipo_campo"].ToString();
                        TxtCodigoCampo.Text = lLector["codigo_tipo_campo"].ToString();
                        TxtDescripcion.Text = lLector["descripcion"].ToString();
                        DdlMesIni.SelectedValue = lLector["mes_inicio"].ToString();
                        TxtDiaIni.Text = lLector["dia_inicio"].ToString();
                        DdlMesFin.SelectedValue = lLector["mes_fin"].ToString();
                        TxtDiaFin.Text = lLector["dia_fin"].ToString();
                        DdlComer.SelectedValue = lLector["proceso_comer_ind_mp"].ToString(); //20170705 rq025-17 indicadores MP fase III
                        ddlPermiteDecAntiSb.SelectedValue = lLector["permite_decla_anti_sb"].ToString(); // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                        ddlUsadoIndMp.SelectedValue = lLector["usa_ind_mp"].ToString(); // Campo nuevo Indicadores 20160620
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoCampo.Visible = false;
                        LblCodigoCampo.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo tipo campo" + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_tipo_campo", "@P_descripcion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "" };

        try
        {
            if (TxtBusCodigoCampo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigoCampo.Text.Trim();
            if (TxtBusDescripcion.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetTipoCampo", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_tipo_campo", "@P_descripcion", "@P_mes_inicio", "@P_dia_inicio", "@P_mes_fin", "@P_dia_fin", "@P_estado", "@P_accion" ,
                                          "@P_permite_decla_anti_sb", // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          "@P_usa_ind_mp", // Campo nuevo indicadores mp 20160620
                                          "@P_proceso_comer_ind_mp" //20170705 rq025-17 indicadores MP fase III
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int ,
                                          SqlDbType.VarChar, // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          SqlDbType.VarChar, // Campo nuevo indicadores mp 20160620
                                          SqlDbType.VarChar //20170705 rq025-17 indicadores MP fase III
                                      };
        string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "1" ,
                                        ddlPermiteDecAntiSb.SelectedValue, // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                        ddlUsadoIndMp.SelectedValue, // Campo nuevo indicadores mp 20160620
                                        DdlComer.SelectedValue //20170705 rq025-17 indicadores MP fase III
                                    };
        lblMensaje.Text = "";

        try
        {
            if (TxtDescripcion.Text.Trim() == "")
                lblMensaje.Text += " Debe digitar la Descripcion del tipo de campo<br>";
            else
                if (VerificarExistencia(" descripcion = '" + TxtDescripcion.Text.Trim() + "'"))
                    lblMensaje.Text += " La Descripcion del tipo e campo YA existe " + TxtDescripcion.Text + " <br>";
            if (DdlMesIni.SelectedValue == "0" && TxtDiaIni.Text != "" && TxtDiaIni.Text != "0")
                lblMensaje.Text += " Si selecciona día de inicio, debe seleccionar el mes de inicio<br>";
            if (DdlMesIni.SelectedValue != "0" && (TxtDiaIni.Text == "" || TxtDiaIni.Text == "0"))
                lblMensaje.Text += " Si selecciona mes de inicio, debe seleccionar el día de inicio<br>";
            if (DdlMesFin.SelectedValue == "0" && TxtDiaFin.Text != "" && TxtDiaFin.Text != "0")
                lblMensaje.Text += " Si selecciona día de finalización, debe seleccionar el mes de finalización<br>";
            if (DdlMesFin.SelectedValue != "0" && (TxtDiaFin.Text == "" || TxtDiaFin.Text == "0"))
                lblMensaje.Text += " Si selecciona mes de finalización, debe seleccionar el día de finalización<br>";
            if (DdlMesIni.SelectedValue != "0" && TxtDiaIni.Text != "" && TxtDiaIni.Text != "0")
                try
                {
                    Convert.ToDateTime("2015/" + DdlMesIni.SelectedValue + "/" + TxtDiaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Mes-Día de inicio no válido<br>";
                }

            if (DdlMesFin.SelectedValue != "0" && TxtDiaFin.Text != "" && TxtDiaFin.Text != "0")
                try
                {
                    Convert.ToDateTime("2015/" + DdlMesFin.SelectedValue + "/" + TxtDiaFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Mes-Día de finalización no válido<br>";
                }

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = DdlMesIni.SelectedValue;
                if (TxtDiaIni.Text != "")
                    lValorParametros[3] = TxtDiaIni.Text;
                lValorParametros[4] = DdlMesFin.SelectedValue;
                if (TxtDiaFin.Text != "")
                    lValorParametros[5] = TxtDiaFin.Text;
                lValorParametros[6] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTipoCampo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación del tipo de campo.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_tipo_campo", "@P_descripcion", "@P_mes_inicio", "@P_dia_inicio", "@P_mes_fin", "@P_dia_fin", "@P_estado", "@P_accion" ,
                                          "@P_permite_decla_anti_sb", // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          "@P_usa_ind_mp", // Campo nuevo indicadores mp 20160620
                                          "@P_proceso_comer_ind_mp"//20170705 rq025-17 indicadores MP fase III
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, 
                                          SqlDbType.VarChar, // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                          SqlDbType.VarChar, // Campo nuevo indicadores mp 20160620
                                          SqlDbType.VarChar//20170705 rq025-17 indicadores MP fase III
                                      };
        string[] lValorParametros = { "0", "", "0", "0", "0", "0", "", "2" ,
                                        ddlPermiteDecAntiSb.SelectedValue, // Campo nuevo Req. 007-17 Subasta Bimestral 20170209
                                        ddlUsadoIndMp.SelectedValue, // Campo nuevo indicadores mp 20160620
                                        DdlComer.SelectedValue //20170705 rq025-17 indicadores MP fase III
                                    };

        lblMensaje.Text = "";
        try
        {
            if (VerificarExistencia(" descripcion = '" + this.TxtDescripcion.Text.Trim() + "' And codigo_tipo_campo != " + LblCodigoCampo.Text))
                lblMensaje.Text += " La Descripcion del tipo de campo YA existe " + TxtDescripcion.Text + " <br>";
            if (DdlMesIni.SelectedValue == "0" && TxtDiaIni.Text != "" && TxtDiaIni.Text != "0")
                lblMensaje.Text += " Si selecciona día de inicio, debe seleccionar el mes de inicio<br>";
            if (DdlMesIni.SelectedValue != "0" && (TxtDiaIni.Text == "" || TxtDiaIni.Text == "0"))
                lblMensaje.Text += " Si selecciona mes de inicio, debe seleccionar el día de inicio<br>";
            if (DdlMesFin.SelectedValue == "0" && TxtDiaFin.Text != "" && TxtDiaFin.Text != "0")
                lblMensaje.Text += " Si selecciona día de finalización, debe seleccionar el mes de finalización<br>";
            if (DdlMesFin.SelectedValue != "0" && (TxtDiaFin.Text == "" || TxtDiaFin.Text == "0"))
                lblMensaje.Text += " Si selecciona mes de finalización, debe seleccionar el día de finalización<br>";
            if (DdlMesIni.SelectedValue != "0" && TxtDiaIni.Text != "" && TxtDiaIni.Text != "0")
                try
                {
                    Convert.ToDateTime("2015/" + DdlMesIni.SelectedValue + "/" + TxtDiaIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Mes-Día de inicio no válido<br>";
                }

            if (DdlMesFin.SelectedValue != "0" && TxtDiaFin.Text != "" && TxtDiaFin.Text != "0")
                try
                {
                    Convert.ToDateTime("2015/" + DdlMesFin.SelectedValue + "/" + TxtDiaFin.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Mes-Día de finalización no válido<br>";
                }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoCampo.Text;
                lValorParametros[1] = TxtDescripcion.Text.Trim();
                lValorParametros[2] = DdlMesIni.SelectedValue;
                if (TxtDiaIni.Text != "")
                    lValorParametros[3] = TxtDiaIni.Text;
                lValorParametros[4] = DdlMesFin.SelectedValue;
                if (TxtDiaFin.Text != "")
                    lValorParametros[5] = TxtDiaFin.Text;
                lValorParametros[6] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetTipoCampo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del tipo de campo.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoCampo.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoCampo.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoCampo.Text != "")
            manejo_bloqueo("E", LblCodigoCampo.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_tipo_campo", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='dbo.m_tipo_campo' and llave_registro='codigo_tipo_campo=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_tipo_campo=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_tipo_campo";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_tipo_campo", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigoCampo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigoCampo.Text.Trim();
                lsParametros += " Codigo tipo campo : " + TxtBusCodigoCampo.Text;

            }
            if (TxtBusDescripcion.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusDescripcion.Text.Trim();
                lsParametros += " - Descripcion: " + TxtBusDescripcion.Text;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetTipoCampo&nombreParametros=@P_codigo_tipo_campo*@P_descripcion&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_tipo_campo*descripcion*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de tipos de campo&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_tipo_campo <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_tipo_campo&procedimiento=pa_ValidarExistencia&columnas=codigo_tipo_campo*descripcion*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
}