﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_precioUsoConexion.aspx.cs" Inherits="BASE.Frm_precioUsoConexion" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" Text="Precio de Uso Gasoducto de Conexión" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Código Precio Uso gasoducto de conexión" AssociatedControlID="TxtBusCodigo" runat="server" />
                            <asp:TextBox ID="TxtBusCodigo" autocomplete="off" CssClass="form-control" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlBusOperador" runat="server" />
                            <asp:DropDownList ID="ddlBusOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" OnSelectedIndexChanged="ddlBusOperador_SelectedIndexChanged" AutoPostBack="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Gasoducto de Conexión" AssociatedControlID="ddlBusGasoducto" runat="server" />
                            <asp:DropDownList ID="ddlBusGasoducto" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered"
                                OnItemCommand="dtgMaestro_EditCommand" AllowPaging="true" PageSize="10" OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                                <Columns>
                                    <asp:BoundColumn DataField="codigo_precio_uso" HeaderText="Código" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="fecha_vigencia" HeaderText="Fecha Vigencia" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="codigo_gasoducto" HeaderText="Código Gasoducto" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="desc_gasoducto" HeaderText="Gasoducto de Conexión" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numero_operacion" HeaderText="Operación" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="precio_uso" HeaderText="Precio uso" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,###,###,###,###,##.00}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <ul class="kt-nav">
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                             <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                        <li class="kt-nav__item">
                                                            <asp:LinkButton ID="lkbEliminar" CssClass="kt-nav__link" CommandName="Eliminar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon-delete"></i>
                                                            <span class="kt-nav__link-text">Eliminar</span>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <%--Registro--%>
    <div class="modal fade" id="mdlRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlRegistroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlRegistroLabel">Registro</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Código Precio Uso Gasoducto de Conexión" AssociatedControlID="TxtCodigo" runat="server" />
                                        <asp:TextBox ID="TxtCodigo" runat="server" MaxLength="3" CssClass="form-control" />
                                        <asp:Label ID="LblCodigo" runat="server" Visible="False" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Fecha Vigencia" AssociatedControlID="TxtFecha" runat="server" />
                                        <asp:TextBox ID="TxtFecha" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                                        <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlOperador_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Gasoducto de Conexión" AssociatedControlID="ddlGasoducto" runat="server" />
                                        <asp:DropDownList ID="ddlGasoducto" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Número de Operación" AssociatedControlID="TxtOperacion" runat="server" />
                                        <asp:TextBox ID="TxtOperacion" type="number" onkeydown="javascript: return event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" runat="server" CssClass="form-control" MaxLength="100" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Precio Uso" AssociatedControlID="TxtPrecio" runat="server" />
                                        <asp:TextBox ID="TxtPrecio" type="number" step="any" onkeydown="javascript: return event.keyCode === 188 || event.keyCode === 190 ||event.keyCode === 8 || event.keyCode === 46 ? true : !isNaN(Number(event.key))" CssClass="form-control" MaxLength="100" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-4">
                                    <div class="form-group">
                                        <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                                        <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="A">Activo</asp:ListItem>
                                            <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Cancelar" CssClass="btn btn-secondary" OnClick="Cancel_OnClick" runat="server" />
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="imbCrear_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-primary" Text="Actualizar" OnClick="imbActualiza_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
