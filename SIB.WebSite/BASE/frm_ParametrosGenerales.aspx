﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosGenerales.aspx.cs"
    Inherits="BASE_frm_ParametrosGenerales" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">Valor Incremento Ofertas
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtValIncOferta" runat="server" MaxLength="4" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtValIncOferta" runat="server" ErrorMessage="Debe Ingresar el Valor Incremento Ofertas"
                    ControlToValidate="TxtValIncOferta" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtValIncOferta" runat="server" ControlToValidate="TxtValIncOferta"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Valor Incremento Ofertas debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">Cantidad Minima Negociación
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCantMinNego" runat="server" MaxLength="4" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtCantMinNego" runat="server" ErrorMessage="Debe Ingresar la Cantidad Minima Negociación"
                    ControlToValidate="TxtCantMinNego" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TxtCantMinNego"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Cantidad Minima Negociacion debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Valor Umbral Exceso Demanda
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtValHumExcDema" runat="server" MaxLength="4" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtValHumExcDema" runat="server" ErrorMessage="Debe Ingresar el Valor del Umbral Exceso Demanda"
                    ControlToValidate="TxtValHumExcDema" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtValHumExcDema" runat="server" ControlToValidate="TxtValHumExcDema"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Valor Umbral Exceso Demanda debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">Valor Vigente de PTDV o CIDV Ministerio Minas
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtValVigMinis" runat="server" MaxLength="8" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtValVigMinis" runat="server" ErrorMessage="Debe Ingresar el Valor Vigente de PTDV o CIDV Ministerio Minas"
                    ControlToValidate="TxtValVigMinis" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtValVigMinis" runat="server" ControlToValidate="TxtValVigMinis"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Valor Vigente de PTDV o CIDV Ministerio Minas debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Dias Caducidad Contraseña
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiasCont" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtDiasCont" runat="server" ErrorMessage="Debe Ingresar Dias Caducidad Contraseña"
                    ControlToValidate="TxtDiasCont" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtDiasCont" runat="server" ControlToValidate="TxtDiasCont"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Dias Caducidad Contraseña debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">Dia Maximo Para Registro Capacidad Transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaMaxTras" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtDiaMaxTras" runat="server" ErrorMessage="Debe Ingresar Dia Maximo Para Registro Capacidad Transporte"
                    ControlToValidate="TxtDiaMaxTras" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtDiaMaxTras" runat="server" ControlToValidate="TxtDiaMaxTras"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Dia Maximo Para Registro Capacidad Transporte debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Mostrar Agresores a Todos
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMostAgreTod" runat="server">
                    <asp:ListItem Value="N" Text="No"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Mostrar Agresores al Oferente
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMostAgreOfe" runat="server">
                    <asp:ListItem Value="N" Text="No"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Nombre Firma Certificado
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNomFirma" runat="server" MaxLength="1000" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Debe Ingresar el Nombre de la Firma del Certificado"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">Cargo Firma Certificado
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCarFirma" runat="server" MaxLength="1000" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Debe Ingresar el Cargo de la Firma del Certificado"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Archivo Firma (El tamaño del archivo debe ser de 200 Ancho x 90 de Alto)
            </td>
            <td class="td2" colspan="3">
                <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" Width="500px" />
                <asp:HiddenField ID="hdfNomArchivo" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Cambio Proceso Facturación {YYYY/MM/DDDD}
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaCamFac" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaCamFac" runat="server" ErrorMessage="Debe Ingresar la Fecha de Cambio de Facturacion"
                    ControlToValidate="TxtFechaCamFac" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">Dia Corte para Facturacion
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaCortFact" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtDiaCortFact" runat="server" ErrorMessage="Debe Ingresar Dia Corte Para Facturación"
                    ControlToValidate="TxtDiaCortFact" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtDiaCortFact" runat="server" ControlToValidate="TxtDiaCortFact"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Dia Corte para Facturación debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Día Hábil Vencimiento Factura
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtDiaHabVecnFac" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfdvTxtDiaHabVecnFac" runat="server" ErrorMessage="Debe Ingresar Día Hábil Vencimiento Factura"
                    ControlToValidate="TxtDiaHabVecnFac" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtDiaHabVecnFac" runat="server" ControlToValidate="TxtDiaHabVecnFac"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Día Hábil Vencimiento Factura debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Dia Calendario Anterior para Calculo Garantías
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaCalGar" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtDiaCalGar" runat="server" ErrorMessage="Debe Ingresar Dia Calendario Anterior para Calculo Garantías"
                    ControlToValidate="TxtDiaCalGar" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtDiaCalGar" runat="server" ControlToValidate="TxtDiaCalGar"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Dia Calendario Anterior para Calculo Garantías debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">No. Meses Selección Factura para calculo Garantías
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoMesCalGar" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoMesCalGar" runat="server" ErrorMessage="Debe Ingresar No. Meses Selección Factura para calculo Garantías"
                    ControlToValidate="TxtNoMesCalGar" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtNoMesCalGar" runat="server" ControlToValidate="TxtNoMesCalGar"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo No. Meses Selección Factura para calculo Garantías debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Dias Hábiles Maximnos para Constitución Garantías
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaMaxConsGar" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtDiaMaxConsGar" runat="server" ErrorMessage="Debe Ingresar Dias Hábiles Maximnos para Constitución Garantías"
                    ControlToValidate="TxtDiaMaxConsGar" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtDiaMaxConsGar" runat="server" ControlToValidate="TxtDiaMaxConsGar"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Dias Hábiles Maximnos para Constitución Garantías debe Ser numérico">*</asp:CompareValidator>
            </td>
            <td class="td1">Dias Hábiles Mínimos para Libreación Garantías
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaMinLibGar" runat="server" MaxLength="3" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtDiaMinLibGar" runat="server" ErrorMessage="Debe Ingresar Dias Hábiles Mínimos para Libreación Garantías"
                    ControlToValidate="TxtDiaMinLibGar" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtDiaMinLibGar" runat="server" ControlToValidate="TxtDiaMinLibGar"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Dias Hábiles Mínimos para Libreación Garantías debe Ser numérico">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">No. Cuenta de Constitución de Garantías
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoCuentaConsGar" runat="server" MaxLength="30" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoCuentaConsGar" runat="server" ErrorMessage="Debe Ingresar No. Cuenta de Constitución de Garantías"
                    ControlToValidate="TxtNoCuentaConsGar" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1" colspan="1">Hora Maxima Declaracion Operativa
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorMaxDecOpera" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorMaxDecOpera" ControlToValidate="TxtHorMaxDecOpera"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Maxima Declaracion Operativa"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">No. Horas Correción Contrato Primario
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoHorasCorContPrim" runat="server" MaxLength="3"></asp:TextBox>
            </td>
            <td class="td1">No. Dias Calendario Correción Contrato Secundario
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiasCorContPrim" runat="server" MaxLength="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Permite Registro de Contratos Antes del Gestor
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPermiteRegCont" runat="server">
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">No. Horas Actualización Contrato Verificación
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoHorasActualCont" runat="server" MaxLength="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Hora Inicial registro informacion operativa extemporánea
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtHoraIniExt" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RfvTxtHoraIniExt" ControlToValidate="TxtHoraIniExt"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora inicial de registro de información operativa extemporánea"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1">Hora Final registro informacion operativa extemporánea
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtHoraFinExt" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="rgvTxtHoraFinExt" ControlToValidate="TxtHoraFinExt"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora final de registro de información operativa extemporánea"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Días hábiles registro información operativa del mes anterior de usuario no regulado
            </td>
            <td class="td2">
                <asp:TextBox ID="txtDiaUNR" runat="server" MaxLength="3"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftetxtDiaUNR" runat="server" Enabled="True" FilterType="Custom, Numbers"
                    TargetControlID="txtDiaUNR"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Requiere aprobación carga de Ids Negocíación directa
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlAprob" runat="server">
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Requiere verificación manual de contratos registrados
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlVerifMan" runat="server">
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Requiere verificación manual de contratos mayoristas
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlVerifManMayo" runat="server">
                    <asp:ListItem Value="S">Si</asp:ListItem>
                    <asp:ListItem Value="N">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">No. Días hábiles máximo Registro Contrato Mayorista
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaHabRegMayor" runat="server" MaxLength="3"></asp:TextBox>
            </td>
            <td class="td1">No. Días hábiles máximo Corección Contrato Mayorista
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaHabCorrMayor" runat="server" MaxLength="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Mail Gestor
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtMailGestor" runat="server" MaxLength="200" Width="350px"></asp:TextBox>
            </td>
            <td class="td1">No. Días hábiles publicación mercado mayorista
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaPubMercMay" runat="server" MaxLength="3"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtDiaPubMercMay" runat="server" TargetControlID="TxtDiaPubMercMay"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>

        <tr>
            <td class="td1">Mercado para permitir Actualización de Precios Indexador
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlMercadoActPrecio" runat="server">
                    <asp:ListItem Value="P">Primario</asp:ListItem>
                    <asp:ListItem Value="S">Secundario</asp:ListItem>
                    <asp:ListItem Value="O">Otras Transacciones del Mercado Mayorista</asp:ListItem>
                    <asp:ListItem Value="T">Todos</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">No. Días Calendario Aprobación Cambio Precio Indexador
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiaAproCambioPrec" runat="server" MaxLength="3"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FtbeTxtNoDiaAproCambioPrec" runat="server" TargetControlID="TxtNoDiaAproCambioPrec"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Inicial Permite Cambio Precio Indexador
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniCamPrecio" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td1">Fecha Final Permite Cambio Precio Indexador
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFinCamPrecio" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Inicial Vigencia Nuevo Precio Indexador
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniVigPrecioIndexa" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td1">No. Días hábiles Publicación Información operativa
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaPubOpe" runat="server" MaxLength="3"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtDiaPubOpe" runat="server" TargetControlID="TxtDiaPubOpe"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">No. Días Calendario Modificación Facturación
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiasModifFactura" runat="server" MaxLength="3"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtNoDiasModifFactura"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Fecha Límite cambio mercado relevante
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaUsrFin" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaUsrFin" runat="server" ErrorMessage="Debe Ingresar la Fecha límite de cambio mercado relevante"
                    ControlToValidate="TxtFechaUsrFin" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Inicial para consulta de curvas
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaIniCurva" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaIniCurva" runat="server" ErrorMessage="Debe Ingresar la Fecha inicial de consulta de curvas"
                    ControlToValidate="TxtFechaIniCurva" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">Fecha Final para consulta de curvas
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFinCurva" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaFinCurva" runat="server" ErrorMessage="Debe Ingresar la Fecha final de consulta de curvas"
                    ControlToValidate="TxtFechaFinCurva" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Máxima para Actualización de fuente en contratos registrados
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaFuente" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtFechaFuente" runat="server" ErrorMessage="Debe Ingresar la Fecha máxima de actualización de fuente en contratos registrados"
                    ControlToValidate="TxtFechaFuente" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">Día hábil de publicación de Oferta Comprometida
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaOferta" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtDiaOferta" runat="server" ErrorMessage="Debe Ingresar el día hábil de publicación de oferta comprometida"
                    ControlToValidate="TxtDiaOferta" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="fteTxtDiaOferta" runat="server" TargetControlID="TxtDiaOferta"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--Campo Nuevo Req 58 - Cargue Archivos 20160701--%>
        <tr>
            <td class="td1">Menú Visualización Archivos
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlMenuVisualizaArchivos" runat="server" Width="200px">
                </asp:DropDownList>
            </td>
            <td class="td1">Fecha Máxima de registro de información PTDV
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaPtdv" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtFechaPtdv" runat="server" ErrorMessage="Debe Ingresar la Fecha máxima rgistro de información PTDV"
                    ControlToValidate="TxtFechaPtdv" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <%--Campo Nuevo Req 079 - Mapa Interactivo 20170314 --%>
        <tr>
            <td class="td1">No. Día hábil Publicación Mapa Interactivo
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiaPubMapa" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoDiaPubMapa" runat="server" ErrorMessage="Debe Ingresar el No. Días hábil para Publicación Información Mapa Interactivo"
                    ControlToValidate="TxtNoDiaPubMapa" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FteTxtNoDiaPubMapa" runat="server" TargetControlID="TxtNoDiaPubMapa"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <%--rq037-17--%>
            <td class="td1">Número mínino de operaciones registradas para mostrar precio en el BEC
            </td>
            <%--rq037-17--%>
            <td class="td2">
                <asp:TextBox ID="TxtOperPre" runat="server" MaxLength="3"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FteTxtOperPre" runat="server" TargetControlID="TxtOperPre"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--Hasta Aqui--%>
        <%--Campos Nuevos Req. 009-17 Indicadores Fase II 20170307--%>
        <tr>
            <td class="td1">Fecha Inicial Registro Demanda Regulada
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecIniDema" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td1">Hora Inicial Registro Demanda Regulada
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtHoraIniDema" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="FRevTxtHoraIniDema" ControlToValidate="TxtHoraIniDema"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Registro Demanda Regulada"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha Final Registro Demanda Regulada
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecFinDema" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td1">Hora Final Registro Demanda Regulada
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtHoraFinDema" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinDema" ControlToValidate="TxtHoraFinDema"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Registro Demanda Regulada"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20170803 rq034-17--%>
        <tr>
            <td class="td1">Fecha Publicación PTDVF 
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaPtdvf" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <%--20171017 rq026-17--%>
        <tr>
            <td class="td1">Días hábiles para aprobación de la solicitud de modificación por parte de la contraparte
            </td>
            <td class="td2">
                <asp:TextBox ID="txtDiaRegMod" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftetxtDiaRegMod" runat="server" TargetControlID="txtDiaRegMod"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Días hábiles para aprobación de la solicitud de modificación por parte del gestor
            </td>
            <td class="td2">
                <asp:TextBox ID="txtDiaRegModApr" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftetxtDiaRegModApr" runat="server" TargetControlID="txtDiaRegModApr"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <%--20180302 rq004-18--%>
            <td class="td1">Número máximo de solicitudes de cambio de precio por año
            </td>
            <%--20180302 rq004-18--%>
            <td class="td2">
                <asp:TextBox ID="TxtMaxSolPrec" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RqfvTxtMaxSolPrec" runat="server" ErrorMessage="Debe Ingresar el No. Días de la fecha de negociación"
                    ControlToValidate="TxtMaxSolPrec" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FtebTxtMaxSolPrec" runat="server" TargetControlID="TxtMaxSolPrec"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <%--20181210 rq046-18--%>
            <td class="td1">Horas vigencia token para firma de certificado
            </td>
            <%--20181210 rq046-18--%>
            <td class="td2">
                <asp:TextBox ID="TxtHoraVigTok" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtHoraVigTok" runat="server" ErrorMessage="Debe Ingresar las horas de vigencia del token"
                    ControlToValidate="TxtHoraVigTok" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftebTxtHoraVigTok" runat="server" TargetControlID="TxtHoraVigTok"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <%--20181210 rq046-18--%>
            <td class="td1">Días vigencia del proceso de firma de certificado
            </td>
            <%--20181210 rq046-18--%>
            <td class="td2">
                <asp:TextBox ID="TxtDiaVigFirma" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtDiaVigFirma" runat="server" ErrorMessage="Debe Ingresar los días de vigencia de la firma de certificado"
                    ControlToValidate="TxtDiaVigFirma" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftebTxtDiaVigFirma" runat="server" TargetControlID="TxtDiaVigFirma"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <%--20181210 rq046-18--%>
            <td class="td1">Días vigencia impresión certificado
            </td>
            <%--20181210 rq046-18--%>
            <td class="td2">
                <asp:TextBox ID="TxtDiaImpCert" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtDiaImpCert" runat="server" ErrorMessage="Debe Ingresar los días de vigencia de impresión de certificado"
                    ControlToValidate="TxtDiaImpCert" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftebTxtDiaImpCert" runat="server" TargetControlID="TxtDiaImpCert"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <%--20181210 rq046-18--%>
            <td class="td1">Nombre usuario que firma certificados de la BMC
            </td>
            <%--20181210 rq046-18--%>
            <td class="td2">
                <asp:TextBox ID="TxtNombreFirmaBmc" runat="server" MaxLength="100" Width="350px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTxtNombreFirmaBmc" runat="server" ErrorMessage="Debe Ingresar el nombre del usuario de la BMC que firma los ceritificados"
                    ControlToValidate="TxtNombreFirmaBmc" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <%--20190318 fin rq017-19--%>
        <tr>
            <td class="td1">Día reporte programación de nominaciones de suministro
            </td>
            <td class="td1">
                <asp:DropDownList ID="ddlDiaNomSum" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="A">D - 1</asp:ListItem>
                    <asp:ListItem Value="D">D</asp:ListItem>
                    <asp:ListItem Value="P">D + 1</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Hora máxima reporte programación nominaciones suministro
            </td>
            <td class="td2">
                <asp:TextBox ID="txtHoraSum" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revtxtHoraSum" ControlToValidate="txtHoraSum"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de reporte de programación definitiva de suministro"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Día reporte programación de nominaciones del día de gas de suministro
            </td>
            <td class="td1">
                <asp:DropDownList ID="ddlDiaNomDiaSum" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="A">D - 1</asp:ListItem>
                    <asp:ListItem Value="D">D</asp:ListItem>
                    <asp:ListItem Value="P">D + 1</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Hora máxima reporte programación nominaciones del día de gas de suministro
            </td>
            <td class="td2">
                <asp:TextBox ID="txtHoraSumDia" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revtxtHoraSumDia" ControlToValidate="txtHoraSumDia"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de reporte de programación de nominaciones del día de gas de suministro"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Día reporte programación definitiva suministro posterior a re-nominación
            </td>
            <td class="td1">
                <asp:DropDownList ID="ddlDiaPostSum" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="A">D - 1</asp:ListItem>
                    <asp:ListItem Value="D">D</asp:ListItem>
                    <asp:ListItem Value="P">D + 1</asp:ListItem>
                </asp:DropDownList>
            </td>

            <td class="td1">Hora máxima reporte programación definitiva suministro posterior a re-nominación
            </td>
            <td class="td2">
                <asp:TextBox ID="txtHoraSumPost" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revtxtHoraSumPost" ControlToValidate="txtHoraSumPost"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de reporte de programación definitiva de suministro posterior a re-nominación"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>

            <td class="td1">Día reporte programación de nominaciones de transporte
            </td>
            <td class="td1">
                <asp:DropDownList ID="ddlDiaNomTrans" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="A">D - 1</asp:ListItem>
                    <asp:ListItem Value="D">D</asp:ListItem>
                    <asp:ListItem Value="P">D + 1</asp:ListItem>
                </asp:DropDownList>
            </td>

            <td class="td1">Hora máxima reporte programación definitiva transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="txtHoraTrans" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtHoraTrans"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de reporte de programación definitiva de transporte"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Día reporte programación de nominaciones del día de gas de transporte
            </td>
            <td class="td1">
                <asp:DropDownList ID="ddlDiaNomDiaTrans" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="A">D - 1</asp:ListItem>
                    <asp:ListItem Value="D">D</asp:ListItem>
                    <asp:ListItem Value="P">D + 1</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Hora máxima reporte programación de nominaciones del día de gas de transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="txtHoraDiaTrans" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtHoraDiaTrans"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de reporte de programación de nominaciones del día de gas de transporte"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Día reporte programación definitiva de transporte posterior a re-nominación
            </td>
            <td class="td1">
                <asp:DropDownList ID="ddlDiaPostTrans" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="A">D - 1</asp:ListItem>
                    <asp:ListItem Value="D">D</asp:ListItem>
                    <asp:ListItem Value="P">D + 1</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Hora máxima reporte programación definitiva transporte porterior a re-nominación
            </td>
            <td class="td2">
                <asp:TextBox ID="txtHoraTransPost" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtHoraTransPost"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de reporte de programación definitiva de transporte posterior a re-nominación"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%-- 20190502 rq024-19--%>
        <%--<tr>
                <td class="td1">Fecha máxima de reporte histórico de programación definitiva de suministro posterior a re-nominación
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFechaSum" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                    <cc1:CalendarExtender ID="cetxtFechaSum" runat="server" TargetControlID="txtFechaSum"
                        Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                </td>
                <td class="td1">Fecha máxima de reporte histórico de programación definitiva de transporte posterior a re-nominación
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFechaTrans" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaTrans"
                        Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                </td>
            </tr>--%>
        <%--20190318 fin rq017-19--%>
        <%-- 20190502 rq024-19--%>
        <tr>
            <td class="td1">Replica información de nominación de transporte D-1 para nominación del día de gas 
            </td>
            <td class="td1"><%--20190712--%>
                <asp:DropDownList ID="ddlReplicaNomTra" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
            <%--20190712--%>
            <td class="td1">Replica información de nominación de suministro D-1 para nominación del día de gas 
            </td>
            <%--20190712--%>
            <td class="td1">
                <asp:DropDownList ID="ddlReplicaNomSum" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--20190322 rq018-19--%>
        <tr>
            <td class="td1">Día límite máximo para ingreso ejecución contrato <%--20190524 rq020-19--%> 
            </td>
            <td class="td1">
                <asp:DropDownList ID="ddlEjecucion" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="A">D - 1</asp:ListItem>
                    <asp:ListItem Value="D">D</asp:ListItem>
                    <asp:ListItem Value="P">D + 1</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Hora máxima ingreso ejecución contrato
            </td>
            <td class="td2">
                <asp:TextBox ID="txtHoraEjec" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revtxtHoraEjec" ControlToValidate="txtHoraEjec"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de ingreso de ejecución de contrato"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20190524 rq020-19--%>
        <tr>
            <td class="td1">Días previos ingreso de ejecución de contratos
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="txtDiaPrevEjec" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtDiaPrevEjec" runat="server" ErrorMessage="Debe Ingresar los días previos para el ingreso de ejecución de contratos"
                    ControlToValidate="txtDiaPrevEjec" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftebtxtDiaPrevEjec" runat="server" TargetControlID="txtDiaPrevEjec"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--20190404 rq018-19 fase II--%>
        <tr>
            <td class="td1">Días hábiles registro contratos suministro con interrupciones mercado secundario
            </td>
            <td class="td2">
                <asp:TextBox ID="txtRegSumInt" runat="server" MaxLength="4" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtRegSumInt" runat="server" ErrorMessage="Debe Ingresar los días para registro de contratos de suministro con interrupciones"
                    ControlToValidate="txtRegSumInt" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="ftebtxtRegSumInt" runat="server" TargetControlID="txtRegSumInt"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Mes/Día inicio control de registro de negociaciones de contratos de suministro con interrupciones de mercado secundario (mm/dd)

            </td>
            <td class="td2">
                <asp:TextBox ID="txtMesDiaInt" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revtxtMesDiaInt" ControlToValidate="txtMesDiaInt"
                    ValidationGroup="comi" runat="server" ValidationExpression="^(0[1-9]|1[0-2])/([0-2][0-9]|3[01])"
                    ErrorMessage="Formato Incorrecto en el mes/día de negociaciones de contratos de suministro con interrupciones"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20190404 rq018-19 fase II--%>
        <tr>
            <td class="td1">Mes/Día inicial de registro de negociaciones de contratos de suministro con interrupciones de mercado secundario (mm/dd)
            </td>
            <td class="td2">
                <asp:TextBox ID="txtMesDiaIniReg" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revtxtMesDiaIniReg" ControlToValidate="txtMesDiaIniReg"
                    ValidationGroup="comi" runat="server" ValidationExpression="^(0[1-9]|1[0-2])/([0-2][0-9]|3[01])"
                    ErrorMessage="Formato Incorrecto en el mes/día inicial de registro  negociaciones de contratos de suministro con interrupciones"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1">Mes/Día final de registro de negociaciones de contratos de suministro con interrupciones de mercado secundario (mm/dd)
            </td>
            <td class="td2">
                <asp:TextBox ID="txtMesDiaFinReg" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revtxtMesDiaFinReg" ControlToValidate="txtMesDiaIniReg"
                    ValidationGroup="comi" runat="server" ValidationExpression="^(0[1-9]|1[0-2])/([0-2][0-9]|3[01])"
                    ErrorMessage="Formato Incorrecto en el mes/día final de registro  negociaciones de contratos de suministro con interrupciones"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20210422 ajuste factruacion--%>
        <tr>
            <td class="td1">Año Base de selección IPP para Facturación
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtAnobasLiq" runat="server" MaxLength="4" Width="70px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtAnobasLiq" runat="server" ErrorMessage="Debe Ingresar Año Base de Liquidación Para Facturación"
                    ControlToValidate="TxtAnobasLiq" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CvTxtAnobasLiq" runat="server" ControlToValidate="TxtAnobasLiq"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Campo Año Base Liquidación para Facturación debe Ser numérico">*</asp:CompareValidator>
            </td>

            <td class="td1">Mes Base selección IPP para Facturación
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMesfact" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <%--20210422 ajuste factruacion--%>
        <tr>
            <td class="td1">Días hábiles para cálculo de fecha de constitución de garantías en carta
            </td>
            <td class="td2">
                <asp:TextBox ID="txtDiaConsGar" runat="server" MaxLength="5"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebtxtDiaConsGar" runat="server" TargetControlID="txtDiaConsGar"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Días hábiles para cálculo de fecha de liberación de garantías en carta
            </td>
            <td class="td2">
                <asp:TextBox ID="txtDiaLibGar" runat="server" MaxLength="5"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebtxtDiaLibGar" runat="server" TargetControlID="txtDiaLibGar"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">Observación Cambio
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
                <asp:Button ID="bntRuedas" runat="server" OnClick="bntRuedas_Click" Text="Ruedas Automaticas"
                    Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
