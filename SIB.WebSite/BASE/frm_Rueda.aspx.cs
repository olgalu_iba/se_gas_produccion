﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
public partial class BASE_frm_Rueda : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Ruedas";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            //Controlador util = new Controlador();
            /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
             *   lsIndica = N -> Nuevo (Creacion)
             *   lsIndica = L -> Listar Registros (Grilla)
             *   lsIndica = M -> Modidificar
             *   lsIndica = B -> Buscar
             * */

            //Establese los permisos del sistema
            EstablecerPermisosSistema();
            lConexion = new clConexion(goInfo);
            lConexion1 = new clConexion(goInfo);

            if (!IsPostBack)
            {
                lConexion.Abrir();
                ddlBusSubasta.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlBusSubasta, "m_tipos_subasta", "estado = 'A' and codigo_tipo_subasta != 0 Order by descripcion", 0, 1);
                lConexion.Cerrar();
                // Carga informacion de combos
                //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
                if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
                {
                    lsIndica = this.Request.QueryString["lsIndica"].ToString();
                }
                if (lsIndica == null || lsIndica == "" || lsIndica == "L")
                {
                    Listar();
                }
                else if (lsIndica == "N")
                {
                    Nuevo();
                }
                else if (lsIndica == "B")
                {
                    Buscar();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al Cargar la Página. " + ex.Message.ToString();

        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_rueda");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[8].Visible = (Boolean)permisos["DELETE"];
        dtgMaestro.Columns[9].Visible = (Boolean)permisos["UPDATE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        lblTitulo.Text = "Crear " + lsTitulo;
        lConexion.Abrir();
        ddlSubasta.Items.Clear();
        LlenarControles(lConexion.gObjConexion, ddlSubasta, "m_tipos_subasta", "estado = 'A' and codigo_tipo_subasta != 0 Order by descripcion", 0, 1);
        lConexion.Cerrar();
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblModifica.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    ddlActSubasta.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlActSubasta, "m_tipos_subasta", "estado = 'A' and codigo_tipo_subasta != 0 Order by descripcion", 0, 1);
                    LlenarControles(lConexion.gObjConexion, ddlEstado, "m_estado_gas", " tipo_estado = 'R'  Order by descripcion_estado", 2, 3);
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " numero_rueda = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        TrSuvcp01.Visible = false;
                        TrSuvcp02.Visible = false;
                        TrSuvcp03.Visible = false;

                        TrSspci01.Visible = false;
                        TrSspci02.Visible = false;
                        TrSspci03.Visible = false;

                        TrSci02.Visible = false;
                        TrSci03.Visible = false;
                        TrSci04.Visible = false;
                        TrSci05.Visible = false;
                        TrSci06.Visible = false;
                        TrSci07.Visible = false;
                        TrSci08.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209
                        TrSci09.Visible = false; // Campo Nuevo Req. 007-17 Subasta Bimestral 20170209

                        TrUVLP01.Visible = false;  //20160921 UVLP
                        TrUVLP02.Visible = false;  //20160921 UVLP
                        TrUVLP03.Visible = false;  //20160921 UVLP
                        TrUVLP04.Visible = false;  //20160921 UVLP
                        /// Nueva Subasta Transporte Req. 005-2021 20210121
                        TrStra01.Visible = false;
                        TrStra02.Visible = false;
                        TrStra03.Visible = false;
                        TrStra04.Visible = false;
                        /// Hasta Aqui
                        lLector.Read();
                        hdfNumeroRueda.Value = lLector["numero_rueda"].ToString();
                        try
                        {
                            ddlActSubasta.SelectedValue = lLector["codigo_tipo_subasta"].ToString();
                            lConexion1.Abrir();
                            ddlActTipoRueda.Items.Clear();
                            LlenarControles(lConexion1.gObjConexion, ddlActTipoRueda, "m_tipos_rueda", "estado = 'A' and codigo_tipo_rueda != 0 And codigo_tipo_subasta = " + ddlActSubasta.SelectedValue + " Order by descripcion", 0, 1);
                            lConexion1.Cerrar();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El tipo de subasta del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlActTipoRueda.SelectedValue = lLector["codigo_tipo_rueda"].ToString();
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "El tipo de rueda del registro no existe o esta inactivo<br>";
                        }
                        ddlActSubasta.Enabled = false;
                        ddlActTipoRueda.Enabled = false;
                        lblFechaRueda.Text = lLector["fecha_rueda"].ToString().Substring(0, 10);
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        ddlEstado.Enabled = false;
                        TxtHoraAperturaAct.Text = lLector["hora_apertura"].ToString();
                        TxtHoraCierre.Text = lLector["hora_cierre"].ToString();

                        /// Campos Subasta Uselo o Vendalo Cotro Plazo
                        if (ddlActSubasta.SelectedValue == "2")
                        {
                            if (lLector["ind_tipo_rueda"].ToString() == "R") //20201207
                                TrSuvcp01.Visible = true;
                            TrSuvcp02.Visible = true;
                            TrSuvcp03.Visible = true;
                            TxtHorIniOfVenSuvcp.Text = lLector["hora_ini_oferta_venta"].ToString().Trim();
                            TxtHorFinOfVenSuvcp.Text = lLector["hora_fin_publi_v_sci"].ToString().Trim();
                            TxtHorIniContVenSuvcp.Text = lLector["hora_ini_cont_venta"].ToString().Trim();
                            TxtHorFinContVenSuvcp.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString().Trim();
                            TxtHorIniPubVenSuvcp.Text = lLector["hora_ini_publica_venta"].ToString().Trim();
                            TxtHorFinPubVenSuvcp.Text = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString().Trim();
                            TxtHorIniOfComSuvcp.Text = lLector["hora_ini_oferta_compra"].ToString().Trim();
                            TxtHorFinOfComSuvcp.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString().Trim();
                            TxtHorIniCalceSuvcp.Text = lLector["hora_ini_calce"].ToString().Trim();
                            TxtHorFinCalceSuvcp.Text = lLector["hora_fin_negociacioni_sci"].ToString().Trim();
                            TxtHorIniModContSuvcp.Text = lLector["hora_ini_modif_cont"].ToString().Trim();
                            TxtHorFinModContSuvcp.Text = lLector["hora_ini_negociacioni_sci"].ToString().Trim();
                        }
                        /// Campos Subasta Suministro con Interrpciones
                        if (ddlActSubasta.SelectedValue == "1")
                        {
                            TrSspci01.Visible = true;
                            TrSspci02.Visible = true;
                            TrSspci03.Visible = true;
                            TxtFecMaxParVendSci.Text = lLector["fecha_ing_vend_smpsi"].ToString().Substring(6, 4) + "/" + lLector["fecha_ing_vend_smpsi"].ToString().Substring(3, 2) + "/" + lLector["fecha_ing_vend_smpsi"].ToString().Substring(0, 2);
                            TxtFecMaxDecVendSci.Text = lLector["fecha_max_ofe_v_smpsi"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_ofe_v_smpsi"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_ofe_v_smpsi"].ToString().Substring(0, 2);
                            TxtFecMaxRevInfSci.Text = lLector["fecha_min_rev_inf_smpsi"].ToString().Substring(6, 4) + "/" + lLector["fecha_min_rev_inf_smpsi"].ToString().Substring(3, 2) + "/" + lLector["fecha_min_rev_inf_smpsi"].ToString().Substring(0, 2);
                            TxtFecMaxDecCompdSci.Text = lLector["fecha_max_ofe_c_smpsi"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_ofe_c_smpsi"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_ofe_c_smpsi"].ToString().Substring(0, 2);
                            TxtNoHoraModVenSci.Text = lLector["hora_modif_ofer_v_smpsi"].ToString().Trim();
                            TxtNoHoraModComSci.Text = lLector["hora_modif_ofer_c_smpsi"].ToString().Trim();
                        }
                        /// Campos Subasta suminsitro con interrupciones
                        if (ddlActSubasta.SelectedValue == "4" || ddlActSubasta.SelectedValue == "6")
                        {
                            TrSci02.Visible = true;
                            TrSci03.Visible = true;
                            TrSci04.Visible = true;
                            TrSci05.Visible = true;
                            TrSci06.Visible = true;
                            TxtHorIniPubVCi.Text = lLector["hora_ini_publi_v_sci"].ToString().Trim();
                            TxtHorFinPubVCi.Text = lLector["hora_fin_publi_v_sci"].ToString().Trim();
                            TxtHorIniPubCantDCi.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString().Trim();
                            TxtHorFinPubCantDCi.Text = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString().Trim();
                            TxtHorIniRecCantCCi.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString().Trim();
                            TxtHorFinRecCantCCi.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString().Trim();
                            TxtHorIniNegociaCi.Text = lLector["hora_ini_negociacioni_sci"].ToString().Trim();
                            TxtHorFinNegociaCi.Text = lLector["hora_fin_negociacioni_sci"].ToString().Trim();
                            TxtMinModV.Text = lLector["min_modif_ofert_sci_v"].ToString();
                            TxtMinModC.Text = lLector["min_modif_ofert_sci_c"].ToString();
                            if (ddlActSubasta.SelectedValue == "4")
                            {
                                lblSub01.Text = "Hora Inicial Publicación Venta Contratos con Interrupciones";
                                lblSub02.Text = "Hora Final Publicación Venta Contratos con Interrupciones";
                                lblSub03.Text = "Hora Inicial Publicación Cantidad Dispinible Venta Contratos con Interrupciones";
                                lblSub04.Text = "Hora Final Publicación Cantidad Dispinible Venta Contratos con Interrupciones";
                                lblSub05.Text = "Hora Inicial Recibo Cantidad Compra Contratos con Interrupciones";
                                lblSub06.Text = "Hora Final Recibo Cantidad Compra Contratos con Interrupciones";
                                lblSub07.Text = "Hora Inicial Negociación Contratos con Interrupciones";
                                lblSub08.Text = "Hora Final Negociación Contratos con Interrupciones";
                            }
                            else
                            {
                                lblSub01.Text = "Hora Inicial Para Declaración de Información Subasta Bimestral";
                                lblSub02.Text = "Hora Final Para Declaración de Información Subasta Bimestral";
                                lblSub03.Text = "Hora Inicial Publicación Cantidad Dispinible Venta Contratos en firme bimestrales";
                                lblSub04.Text = "Hora Final Publicación Cantidad Dispinible Venta Contratos en firme bimestrales";
                                lblSub05.Text = "Hora Inicial Recibo Cantidad Compra Contratos en firme bimestrales";
                                lblSub06.Text = "Hora Final Recibo Cantidad Compra Contratos en firme bimestrales";
                                lblSub07.Text = "Hora Inicial Negociación Contratos en firme bimestrales";
                                lblSub08.Text = "Hora Final Negociación Contratos en firme bimestrales";
                            }
                            // Campo nuevo ajuste subastas bimetrales por contingencia 20151105
                            TxtNoMesIniPer.Text = "0";
                            if (ddlActSubasta.SelectedValue == "6")
                            {
                                // Campo nuevo para ajuste de subastas bimestrales por tema de ocntingencia 20151105
                                lblSub10.Text = "No. Mes Adelante Definición Inicio Periodo bimestrales";
                                TrSci07.Visible = true;
                                TxtNoMesIniPer.Text = lLector["mes_prev_vend_smpsi"].ToString();
                                /////////////////////////////////////////////////////////////
                                //// Campos Nuevos Req.007-17 Subasta Bimestral 20170209 ////
                                /////////////////////////////////////////////////////////////
                                TrSci08.Visible = true;
                                TrSci09.Visible = true;
                                TxtFecDeclaSubBimes.Text = lLector["fecha_max_ofe_v_smpsi"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_ofe_v_smpsi"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_ofe_v_smpsi"].ToString().Substring(0, 2);
                                TxtFecPubliSubBimes.Text = lLector["fecha_max_ofe_c_smpsi"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_ofe_c_smpsi"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_ofe_c_smpsi"].ToString().Substring(0, 2);
                                TxtHoraIniModDecSb.Text = lLector["hora_ini_modif_decla_sb"].ToString().Trim();
                                TxtHoraFinModDecSb.Text = lLector["hora_fin_modif_decla_sb"].ToString().Trim();
                                /////////////////////////////////////////////////////////////
                            }
                        }
                        /// Campos Subasta Negociacion Directa
                        if (ddlActSubasta.SelectedValue == "5")
                        {
                            TrSnd01.Visible = true;
                            TxtHorInPosNd.Text = lLector["hora_ini_negociacioni_sci"].ToString().Trim();
                            TxtHorFiPosNd.Text = lLector["hora_fin_negociacioni_sci"].ToString().Trim();
                        }
                        /// 20160921 UVL
                        if (ddlActSubasta.SelectedValue == "3")
                        {
                            TrUVLP01.Visible = true;
                            TrUVLP02.Visible = true;
                            TrUVLP03.Visible = true;
                            TrUVLP04.Visible = true;
                            TxtHoraIniPubUvlp.Text = lLector["hora_ini_publi_v_sci"].ToString().Trim();
                            TxtHoraFinPubUvlp.Text = lLector["hora_fin_publi_v_sci"].ToString().Trim();
                            TxtHoraIniCompUvlp.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString().Trim();
                            TxtHoraFinCompUvlp.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString().Trim();
                            TxtHoraIniCalceUvlp.Text = lLector["hora_ini_negociacioni_sci"].ToString().Trim();
                            TxtHoraFinCalceUvlp.Text = lLector["hora_fin_negociacioni_sci"].ToString().Trim();
                            ddlTpoNEgUvlp.SelectedValue = lLector["ind_tipo_rueda"].ToString();
                        }
                        /// Campos nuevos Req. 005-2021 20210120 Subasta Transporte
                        if (ddlActSubasta.SelectedValue == "11")
                        {
                            TrStra01.Visible = true;
                            TrStra02.Visible = true;
                            TrStra03.Visible = true;
                            TrStra04.Visible = true;
                            ddlTipoRueTra11.SelectedValue = lLector["ind_tipo_rueda"].ToString();
                            txtHoraIniPubOfr11.Text = lLector["hora_ini_publi_v_sci"].ToString().Trim();
                            txtHoraFinPubOfr11.Text = lLector["hora_fin_publi_v_sci"].ToString().Trim();
                            txtHoraIniDecPosCom11.Text = lLector["hora_ini_modif_decla_sb"].ToString().Trim();
                            txtHoraFinDecPosCom11.Text = lLector["hora_fin_modif_decla_sb"].ToString().Trim();
                            txtHoraIniDesSub11.Text = lLector["hora_ini_negociacioni_sci"].ToString().Trim();
                            txtHoraFinDesSub11.Text = lLector["hora_fin_negociacioni_sci"].ToString().Trim();
                            txtHoraIniRecAdj11.Text = lLector["hora_ini_rec_solicitud_c_sci"].ToString().Trim();
                            txtHoraFinRecAdj11.Text = lLector["hora_fin_rec_solicitud_c_sci"].ToString().Trim();
                            txtHoraIniPubRes11.Text = lLector["hora_ini_publ_cnt_dispi_v_sci"].ToString().Trim();
                            txtHoraFinPubRes11.Text = lLector["hora_fin_publ_cnt_dispi_v_sci"].ToString().Trim();
                            txtHoraIniFinSub11.Text = lLector["hora_ini_publica_venta"].ToString().Trim();
                            txtHoraFinFinSub11.Text = lLector["hora_ini_oferta_compra"].ToString().Trim();
                        }
                        /// Hasta Aqui
                        TxtFechaProx.Text = lLector["fecha_prox_apertura"].ToString().Substring(6, 4) + lLector["fecha_prox_apertura"].ToString().Substring(2, 4) + lLector["fecha_prox_apertura"].ToString().Substring(0, 2);

                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Código Rueda " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = false;
            tblModifica.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_fecha_rueda", "@P_ano_rueda", "@P_codigo_tipo_subasta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "0" };

        try
        {
            if (TxtBusNumeroRueda.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusNumeroRueda.Text.Trim();
            if (TxtBusFechaRueda.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusFechaRueda.Text.Trim();
            if (TxtBusANoRueda.Text.Trim().Length > 0)
                lValorParametros[2] = TxtBusANoRueda.Text.Trim();
            if (ddlBusSubasta.SelectedValue != "0")
                lValorParametros[3] = ddlBusSubasta.SelectedValue
                    ;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRueda", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_fecha_rueda", "@P_codigo_tipo_rueda", "@P_hora_apertura", "@P_accion", "@P_codigo_tipo_subasta", "@P_ano_consumo", "@P_mes_consumo",
                                        "@P_trimestre_nego" // Campo nueva subasta transporte Req. 005-2021 20210121
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,
                                        SqlDbType.Int // Campo nueva subasta transporte Req. 005-2021 20210121
                                      };
        string[] lValorParametros = { "0", "", "0", "", "1", ddlSubasta.SelectedValue, "0", "0",
                                      ddlTrimestre.SelectedValue // Campo nueva subasta transporte Req. 005-2021 20210121
                                    };
        DateTime lsFecha;
        int liValor = 0;
        int liValor1 = 0;
        lblMensaje.Text = "";
        string lsRutaArchivo = ConfigurationManager.AppSettings["RutaArchivos"].ToString();
        try
        {
            if (ddlSubasta.SelectedValue != "4" && ddlSubasta.SelectedValue != "6")
            {
                // Validaciones nueva substa transporte Req. 005-2021 20210121 //
                if (ddlSubasta.SelectedValue == "11")
                {
                    if (ddlTrimestre.SelectedValue == "0")
                        lblMensaje.Text += "Debe seleccionar el Trimestre de Negociación.<br>";
                    if (TxtAñoNeg.Text.Trim() == "")
                        lblMensaje.Text += "Debe Ingresar Año de Negociación.<br>";
                    else
                    {
                        try
                        {
                            liValor1 = Convert.ToInt32(TxtAñoNeg.Text.Trim());
                            lValorParametros[6] = TxtAñoNeg.Text.Trim();
                            if (liValor1 < DateTime.Now.Year)
                                lblMensaje.Text += "Valor Inválido en Año de Negociación.<br>";
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "Valor Inválido en Año de Negociación.<br>";
                        }
                    }
                }
                else
                {
                    try
                    {
                        lsFecha = Convert.ToDateTime(TxtFechaRueda.Text.Trim());
                        if (lsFecha < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                            lblMensaje.Text = "La fecha de la rueda no puede ser anterior a la fecha actual. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text = "Valor Inválido para el Campo Fecha Rueda. <br>";
                    }
                }
            }
            if (ddlSubasta.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar la Subasta.<br>";
            else
            {
                /*
                if (ddlSubasta.SelectedValue == "2")
                {
                    if (DelegadaBase.Servicios.ValidarExistencia("t_rueda", " convert(varchar(10),fecha_rueda,111) = " + TxtFechaRueda.Text + " And codigo_tipo_subasta = 2 ", goInfo))
                        lblMensaje.Text += "YA existe una Rueda para la Subasta y Fecha Ingresada.<br>";
                }
                if (ddlSubasta.SelectedValue == "1")
                {
                    if (DelegadaBase.Servicios.ValidarExistencia("t_rueda", " year(fecha_rueda) = " + TxtFechaRueda.Text.Substring(0, 4) + " And codigo_tipo_subasta = 1 ", goInfo))
                        lblMensaje.Text += "YA existe una Rueda para la Subasta y Ano Ingresados.<br>";
                }
                */
            }
            if (DelegadaBase.Servicios.ValidarExistencia("t_rueda", " codigo_tipo_rueda = " + ddlTipoRueda.SelectedValue + " and estado not in ('F','Z')", goInfo))
                lblMensaje.Text += "Ya existe una Rueda para el tipo seleccionado que no ha sido finalizada.<br>";
            if (ddlTipoRueda.SelectedValue == "0")
                lblMensaje.Text += "Debe seleccionar el Tipo de Rueda.<br>";
            if (ddlSubasta.SelectedValue == "4" || ddlSubasta.SelectedValue == "6")
            {
                try
                {
                    liValor = Convert.ToInt32(TxtAnoCons.Text.Trim());
                    lValorParametros[6] = TxtAnoCons.Text.Trim();

                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido en Año de Consumo.<br>";
                }
                try
                {
                    liValor1 = Convert.ToInt32(TxtMesCons.Text.Trim());
                    lValorParametros[7] = TxtMesCons.Text.Trim();
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido en Mes de Consumo.<br>";
                }
                if (lblMensaje.Text == "")
                {
                    if (ddlSubasta.SelectedValue == "4")
                    {
                        if (liValor < DateTime.Now.Year || liValor == DateTime.Now.Year && liValor1 <= DateTime.Now.Month)
                            lblMensaje.Text += "El Año-Mes no puede ser menor o igual que el actual.<br>";
                    }
                    else
                    {
                        if (liValor < DateTime.Now.Year || liValor == DateTime.Now.Year && liValor1 < DateTime.Now.Month)
                            lblMensaje.Text += "El Año-Mes no puede ser menor que el actual.<br>";
                    }
                }
            }
            if (lblMensaje.Text == "")
            {
                goInfo.mensaje_error = "";
                lValorParametros[1] = TxtFechaRueda.Text.Trim();
                lValorParametros[2] = ddlTipoRueda.SelectedValue;
                lValorParametros[3] = "";
                // Campo nueva subasta transporte Req. 005-2021 20210121
                if (lValorParametros[8] =="")
                    lValorParametros[8] = "0";
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error == "")
                {
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        // Obgtengo el mensaje de error del procedimiento
                        if (lLector["Error"].ToString() == "")
                        {

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Rueda No. " + lLector["NumeroRueda"].ToString() + " Creada Correctamente." + "');", true);
                            /// Creao el Archivo de la Rueda
                            lsRutaArchivo += "Subasta-" + lLector["NumeroRueda"].ToString() + ".txt";
                            File.Create(lsRutaArchivo).Close();
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                            Listar();
                        }
                        else
                            lblMensaje.Text = lLector["Error"].ToString();
                    }
                    else
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Creación de la Rueda.!";
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                }
                else
                {
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        lblMensaje.Text = "";
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lNoRueda = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text == "C" || this.dtgMaestro.Items[e.Item.ItemIndex].Cells[11].Text == "5")
            {
                lNoRueda = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
                Modificar(lNoRueda);
            }
            else
                lblMensaje.Text = "No se puede modificar la rueda porque ya está abierta";
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            lNoRueda = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text;
            if (!manejo_bloqueo("V", this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text))
            {
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_rueda", " numero_rueda = " + lNoRueda + " And estado = 'C' ");
                if (lLector.HasRows)
                {
                    lLector.Close();
                    lLector.Dispose();
                    string[] lsNombreParametros = { "@P_numero_rueda", "@P_fecha_rueda", "@P_codigo_tipo_rueda", "@P_hora_apertura", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
                    string[] lValorParametros = { "0", "", "0", "", "3" };
                    lValorParametros[0] = lNoRueda;
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRueda", lsNombreParametros, lTipoparametros, lValorParametros))
                        lblMensaje.Text = "No se puede eliminar la rueda porque tiene registros asociados!";
                    else
                        lblMensaje.Text = "Rueda No. " + lNoRueda + " Eliminada Correctamente.!";
                }
                else
                    lblMensaje.Text = "No se Puede Eliminar el Registro por que la Rueda debe estar en estado C=Creada. Número Rueda =  " + lNoRueda;
            }
            else
                lblMensaje.Text = "No se Puede Eliminar el Registro por que esta Bloqueado. Número Rueda =  " + lNoRueda;

            lConexion.Cerrar();
            Listar();
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("t_rueda", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='t_rueda' and llave_registro='numero_rueda=" + lscodigo_registro + "'";
        string lsCondicion1 = "numero_rueda=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "t_rueda";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_rueda", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "", "0", "0" };
        string lsParametros = "";
        if (TxtBusANoRueda.Text.Trim().Length > 0)
            lValorParametros[2] = TxtBusANoRueda.Text.Trim();

        try
        {
            if (TxtBusNumeroRueda.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusNumeroRueda.Text.Trim();
                lsParametros += " Número Rueda : " + TxtBusNumeroRueda.Text;

            }
            if (TxtBusFechaRueda.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusFechaRueda.Text.Trim();
                lsParametros += " - Fecha Rueda: " + TxtBusFechaRueda.Text;
            }
            if (TxtBusANoRueda.Text.Trim().Length > 0)
            {
                lValorParametros[2] = TxtBusANoRueda.Text;
                lsParametros += " - Ano Rueda: " + TxtBusANoRueda.Text;
            }
            if (ddlBusSubasta.SelectedValue != "0")
            {
                lValorParametros[3] = ddlBusSubasta.SelectedValue;
                lsParametros += " - Año Subasta: " + ddlBusSubasta.SelectedItem.ToString();
            }

            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetRueda&nombreParametros=@P_numero_rueda*@P_fecha_rueda*@P_ano_rueda*@P_codigo_tipo_subasta&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "*" + lValorParametros[3] + "&columnas=Producto*valor_negocio*dias_plazo*valor_tasa_venta*valor_tasa_compra*valor_tasa_cierre&titulo_informe=Listado de Ruedas&TituloParametros=" + lsParametros);
        }
        catch (Exception)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " numero_rueda <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_rueda&procedimiento=pa_ValidarExistencia&columnas=ano_rueda*numero_rueda*fecha_rueda*codigo_tipo_rueda*hora_apertura*hora_cierre*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void ddlTipoRueda_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlTipoRueda.SelectedValue != "0")
    //    {
    //        lConexion.Abrir();
    //        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_tipos_rueda", " codigo_tipo_rueda = " + ddlTipoRueda.SelectedValue + " ");
    //        if (lLector.HasRows)
    //        {
    //            lLector.Read();
    //            TxtHoraApertura.Text = lLector["hora_apertura"].ToString();
    //        }
    //        lLector.Close();
    //        lLector.Dispose();
    //        lConexion.Cerrar();
    //    }

    //}
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlSubasta_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSubasta.SelectedValue != "0")
        {
            lConexion.Abrir();
            ddlTipoRueda.Items.Clear();
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", "estado = 'A' and codigo_tipo_rueda != 0 And codigo_tipo_subasta = " + ddlSubasta.SelectedValue + " Order by descripcion", 0, 1);
            lConexion.Cerrar();
            if (ddlSubasta.SelectedValue == "4" || ddlSubasta.SelectedValue == "6")
            {
                TrTrans.Visible = false; // Campo nuevo nueva Subasta transporte Req. 005-2021 20210121
                TrSIVLP.Visible = true;
                trFechaRue.Visible = false;

            }
            else
            {
                // Campo nuevo nueva Subasta transporte Req. 005-2021 20210121 //
                if (ddlSubasta.SelectedValue == "11")
                {
                    lConexion.Abrir();
                    ddlTrimestre.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlTrimestre, "m_trimestre", " 1=1  order by numero_trimestre", 0, 1);
                    lConexion.Cerrar();
                    trFechaRue.Visible = false;
                    TrTrans.Visible = true;
                    TrSIVLP.Visible = false;
                }
                else
                {
                    TrTrans.Visible = false; // Campo nuevo nueva Subasta transporte Req. 005-2021 20210121
                    TrSIVLP.Visible = false;
                    trFechaRue.Visible = true;
                }
            }
            //if (ddlSubasta.SelectedValue != "2")
            //    TrHora.Visible = false;
            //else
            //    TrHora.Visible = true;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_numero_rueda", "@P_codigo_tipo_subasta", "@P_estado", "@P_hora_apertura", "@P_hora_cierre",
                                        "@P_hora_ini_oferta_venta","@P_hora_ini_cont_venta","@P_hora_ini_publica_venta","@P_hora_ini_oferta_compra","@P_hora_ini_calce","@P_hora_ini_modif_cont", //// Variables de Subasta Uselo Cotro Plazo
                                        "@P_fecha_ing_vend_smpsi","@P_fecha_max_ofe_v_smpsi","@P_fecha_min_rev_inf_smpsi","@P_fecha_max_ofe_c_smpsi","@P_hora_modif_ofer_v_smpsi","@P_hora_modif_ofer_c_smpsi", /// Variables Subasta Suministro Sin Interupciones
                                        "@P_hora_ini_negociacioni_sci","@P_hora_fin_negociacioni_sci", /// Variables Negociacion Directa
                                        "@P_hora_ini_publi_v_sci","@P_hora_fin_publi_v_sci","@P_hora_ini_publ_cnt_dispi_v_sci","@P_hora_fin_publ_cnt_dispi_v_sci","@P_hora_ini_rec_solicitud_c_sci","@P_hora_fin_rec_solicitud_c_sci","@P_hora_ini_negociacion_sci","@P_hora_fin_negociacion_sci",/// Variables para subasta suministro con interrupciones
                                        "@P_fecha_prox_apertura", "@p_min_modif_ofert_sci_v", "@p_min_modif_ofert_sci_c",
                                        "@P_mes_prev_vend_smpsi", // Campp nuevo ajuste subasta bimestral por contingencia 20151105
                                        "@P_ind_tipo_rueda", // 20160921 UVLP
                                        "@P_hora_ini_modif_decla_sb","@P_hora_fin_modif_decla_sb"  // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar ,
                                        SqlDbType.VarChar, SqlDbType.VarChar ,
                                        SqlDbType.VarChar, SqlDbType.VarChar ,SqlDbType.VarChar, SqlDbType.VarChar ,SqlDbType.VarChar, SqlDbType.VarChar ,SqlDbType.VarChar, SqlDbType.VarChar ,
                                        SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int , SqlDbType.Int , SqlDbType.Char, // 20160921 UVLP
                                        SqlDbType.VarChar,SqlDbType.VarChar // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                      };
        string[] lValorParametros = { hdfNumeroRueda.Value, ddlActSubasta.SelectedValue, ddlEstado.SelectedValue, "", TxtHoraCierre.Text.Trim(),
                                      "", "", "", "", "", "", "", "", "", "", "", "" ,"","","","","","","","","","","","0","0","0",ddlTpoNEgUvlp.SelectedValue,// 20160921 UVLP
                                      "","" // Campos nuevos Req. 007-17 Subasta Bimestral 20170209
                                    };
        lblMensaje.Text = "";
        DateTime lsFecha;
        DateTime lsFecha1;
        int liValor = 0;
        try
        {
            if (ddlEstado.SelectedValue == "0")
                lblMensaje.Text += "Debe Seleccionar el estado de la Rueda.<br>";
            //if (ddlActSubasta.SelectedValue == "1" && ddlActSubasta.SelectedValue != "3")
            //{
            //    if (TxtHoraAperturaAct.Text.Trim().Length <= 0)
            //        lblMensaje.Text += "Debe Ingresar la Hora de Apertura.<br>";
            //    if (TxtHoraCierre.Text.Trim().Length <= 0)
            //        lblMensaje.Text += "Debe Ingresar la Hora de Cierre.<br>";
            //}
            try
            {
                lsFecha1 = Convert.ToDateTime(TxtFechaProx.Text.Trim());
                if (lsFecha1 < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                    lblMensaje.Text = "La fecha de la próxima apertura no puede ser anterior a la fecha actual. <br>";
            }
            catch (Exception)
            {
                lsFecha1 = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                lblMensaje.Text = "Valor Inválido para el Campo Fecha de próxima apertura. <br>";
            }


            if (ddlActSubasta.SelectedValue == "2")
            {
                /// Validaciones Modificacion campos subasta uselo o  vendalo cortro plazo
                if (TrSuvcp01.Visible) //20201207
                {
                    if (TxtHorIniOfVenSuvcp.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingresar la Hora Inicial de declaración de información.<br>";
                    else
                    {
                        lsFecha1 = Convert.ToDateTime(TxtFechaProx.Text.Trim());
                        if (lsFecha1 == Convert.ToDateTime(DateTime.Now.ToShortDateString()) && Convert.ToDateTime(TxtHorIniOfVenSuvcp.Text) < Convert.ToDateTime(DateTime.Now.ToShortTimeString()))
                            lblMensaje.Text = "La Hora inicial de declaración de información no puede ser menor a la actual. <br>";
                    }
                    if (TxtHorFinOfVenSuvcp.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingresar la Hora Final de declaración de información.<br>";
                    if (TxtHorIniContVenSuvcp.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingresar la Hora Inicial de declaración de precio de reserva.<br>";
                    if (TxtHorFinContVenSuvcp.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingresar la Hora Final de declaración de precio de reserva.<br>";
                }
                if (TxtHorIniPubVenSuvcp.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Inicial de Publicación Venta.<br>";
                if (TxtHorFinPubVenSuvcp.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Final de Publicación Venta.<br>";
                if (TxtHorIniOfComSuvcp.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Inicial Oferta Compra.<br>";
                if (TxtHorFinOfComSuvcp.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Final Oferta Compra.<br>";
                if (TxtHorIniCalceSuvcp.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Inicial Calce.<br>";
                if (TxtHorFinCalceSuvcp.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Final Calce.<br>";
                //20201207
                //if (TxtHorIniModContSuvcp.Text.Trim().Length <= 0)
                //    lblMensaje.Text += "Debe Ingresar la Hora Inicial Modificación Contratos.<br>";
                try
                {
                    if (Convert.ToDateTime(TxtHorIniOfVenSuvcp.Text.Trim()) >= Convert.ToDateTime(TxtHorFinOfVenSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de Declaración de información debe ser menor que la final. <br>";
                    if (Convert.ToDateTime(TxtHorIniContVenSuvcp.Text.Trim()) >= Convert.ToDateTime(TxtHorFinContVenSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de declaración de precio de reserva  debe ser menor que la final. <br>";
                    if (Convert.ToDateTime(TxtHorIniOfVenSuvcp.Text.Trim()) > Convert.ToDateTime(TxtHorIniContVenSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de declaración de información debe ser menor o igual que la hora inicio de declaración de precio de reserva. <br>";
                    if (Convert.ToDateTime(TxtHorFinOfVenSuvcp.Text.Trim()) > Convert.ToDateTime(TxtHorFinContVenSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora final de declaración de información debe ser menor o igual que la hora final de declaración de precio de reserva. <br>";
                    if (Convert.ToDateTime(TxtHorIniContVenSuvcp.Text.Trim()) > Convert.ToDateTime(TxtHorIniPubVenSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora final de declaración de precio de reserva venta debe ser menor o igual que la hora inicial de publicación. <br>";
                    if (Convert.ToDateTime(TxtHorIniPubVenSuvcp.Text.Trim()) >= Convert.ToDateTime(TxtHorFinPubVenSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de publicación debe ser menor que la hora final<br>";
                    if (Convert.ToDateTime(TxtHorFinPubVenSuvcp.Text.Trim()) > Convert.ToDateTime(TxtHorIniOfComSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora final de publicación debe ser menor o igual  que la hora inicial de ingreso de ofertas de compra<br>";
                    if (Convert.ToDateTime(TxtHorIniOfComSuvcp.Text.Trim()) >= Convert.ToDateTime(TxtHorFinOfComSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de ingreso de ofertas de compra debe ser menor que la final<br>";
                    if (Convert.ToDateTime(TxtHorFinOfComSuvcp.Text.Trim()) > Convert.ToDateTime(TxtHorIniCalceSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora final de ingreso de ofertas de compra debe ser menor o igual que la hora de inicio de calce<br>";
                    if (Convert.ToDateTime(TxtHorIniCalceSuvcp.Text.Trim()) >= Convert.ToDateTime(TxtHorFinCalceSuvcp.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de calce debe ser menor que la final<br>";
                    if (TxtHorIniModContSuvcp.Text.Trim().Length > 0) //20201207
                    {
                        if (Convert.ToDateTime(TxtHorFinCalceSuvcp.Text.Trim()) > Convert.ToDateTime(TxtHorIniModContSuvcp.Text.Trim()))
                            lblMensaje.Text += "La hora final de calce debe ser menor o igual que la hora inicial de modificación de contratos<br>";
                        if (TxtHorIniModContSuvcp.Text.Trim() != "" && Convert.ToDateTime(TxtHorIniCalceSuvcp.Text.Trim()) > Convert.ToDateTime(TxtHorIniModContSuvcp.Text.Trim()))
                            lblMensaje.Text += "La hora de inicio de calce debe ser menor o igual que la hora final de modificación de contratos<br>";
                        if (TxtHorIniModContSuvcp.Text.Trim() != "" && TxtHorFinModContSuvcp.Text.Trim() != "" && Convert.ToDateTime(TxtHorIniModContSuvcp.Text.Trim()) >= Convert.ToDateTime(TxtHorFinModContSuvcp.Text.Trim()))
                            lblMensaje.Text += "La hora de incio de modificación de contratos debe ser menor que la final<br>";
                    }
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Error en el formato de horas. <br>";
                }

            }
            if (ddlActSubasta.SelectedValue == "1")
            {
                /// Validaciones Modificacion campos subasta suministro sin interupciones
                try
                {
                    lsFecha = Convert.ToDateTime(TxtFecMaxParVendSci.Text.Trim());
                    if (lsFecha1 < lsFecha)
                        lblMensaje.Text += "La Fecha Máxima Ingreso Participación Vendedores no puede ser mayor a la fecha de apertura de la rueda. <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido para el Campo Fecha Máxima Ingreso Participación Vendedores. <br>";
                }
                try
                {
                    lsFecha = Convert.ToDateTime(TxtFecMaxDecVendSci.Text.Trim());
                    if (lsFecha1 < lsFecha)
                        lblMensaje.Text += "La Fecha Máxima Ingreso Declaración de Vendedores no puede ser mayor a la fecha de apertura de la rueda. <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido para el Campo Fecha Máxima Ingreso Declaración de Vendedores. <br>";
                }
                try
                {
                    lsFecha = Convert.ToDateTime(TxtFecMaxRevInfSci.Text.Trim());
                    if (lsFecha1 < lsFecha)
                        lblMensaje.Text += "La Fecha Máxima Revelación de Información no puede ser mayor a la fecha de apertura de la rueda. <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido para el Campo Fecha Máxima Revelación de Información. <br>";
                }
                try
                {
                    lsFecha = Convert.ToDateTime(TxtFecMaxDecCompdSci.Text.Trim());
                    if (lsFecha1 < lsFecha)
                        lblMensaje.Text += "La Fecha Máxima Ingreso Declaración de Compradores no puede ser mayor a la fecha de apertura de la rueda. <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido para el Campo Fecha Máxima Ingreso Declaración de Compradores. <br>";
                }
                try
                {
                    liValor = Convert.ToInt32(TxtNoHoraModVenSci.Text.Trim());
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido para el Campo Horas Para Modificación de Declaración de Venta. <br>";
                }
                try
                {
                    liValor = Convert.ToInt32(TxtNoHoraModComSci.Text.Trim());
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Valor Inválido para el Campo Horas Para Modificación de Declaración de Compra. <br>";
                }
            }
            //// Validaciones Subasta de Contrtos con Interrupciones
            if (ddlActSubasta.SelectedValue == "4" || ddlActSubasta.SelectedValue == "6")
            {
                if (TxtHorIniPubVCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Publicación Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniPubVCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Publicación Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinPubVCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Publicación Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinPubVCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Publicación Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniPubCantDCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniPubCantDCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Publicación Cantidad Disponible Venta de Subasta Contrato con interupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinPubCantDCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinPubCantDCi.Text.Trim() == "0")
                        lblMensaje.Text += "La Hora Final Publicación Cantidad Disponible Venta de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniRecCantCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Recibo Cantidad Compra de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniRecCantCCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Recibo Cantidad Compra de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinRecCantCCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Recibo Cantidad Compra de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorFinRecCantCCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Recibo Cantidad Compra de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorIniNegociaCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Inicial Negociación de Subasta Contrato con interrupciones. <br>";
                else
                {
                    if (TxtHorIniNegociaCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Inicial Negociación de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtHorFinNegociaCi.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar Hora Final Negociación de Subasta Contrato con interupciones. <br>";
                else
                {
                    if (TxtHorFinNegociaCi.Text.Trim() == "0")
                        lblMensaje.Text += "La  Hora Final Negociación de Subasta Contrato con interrupciones no Puede ser igual a 0. <br>";
                }
                if (TxtMinModV.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe digitar los minutos de modificación de posturas de venta. <br>";
                if (TxtMinModC.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe digitar los minutos de modificación de posturas de compra. <br>";
                try
                {
                    if (Convert.ToDateTime(TxtHorIniPubVCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de publicación de venta debe ser menor que la hora final. <br>";
                    /// Se Comentarea validacion Req. 007-17 Subasta Bimestral 20170209 ///
                    //if (Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniPubCantDCi.Text.Trim()))
                    //    lblMensaje.Text += "La hora inicial de publicación de cantidad disponible debe ser mayor o igual que la hora final de publicación de venta. <br>";
                    if (Convert.ToDateTime(TxtHorIniPubCantDCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinPubCantDCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de publicación de cantidad disponible debe ser menor que la hora final<br>";
                    if (Convert.ToDateTime(TxtHorFinPubCantDCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniRecCantCCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de recibo de cantidad compra debe ser mayor o igual que la hora final de publicación de cantidad disponible<br>";
                    if (Convert.ToDateTime(TxtHorIniRecCantCCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinRecCantCCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de recibo de cantidad compra debe ser menor que la hora final <br>";
                    if (Convert.ToDateTime(TxtHorFinRecCantCCi.Text.Trim()) > Convert.ToDateTime(TxtHorIniNegociaCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial negociación debe ser mayor o igual que la hora final de recibo de cantidad de compra<br>";
                    if (Convert.ToDateTime(TxtHorIniNegociaCi.Text.Trim()) >= Convert.ToDateTime(TxtHorFinNegociaCi.Text.Trim()))
                        lblMensaje.Text += "La hora inicial negociación debe ser menor que la hora final <br>";
                    ///////////////////////////////////////////////////////////////////////
                    /// Se Comentarea validacion Req. 007-17 Subasta Bimestral 20170209 ///
                    /////////////////////////////////////////////////////////////////////////
                    //DateTime dateValue = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorFinPubVCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorFinPubVCi.Text.Trim().Substring(3, 2)), 0);
                    //dateValue = dateValue.AddMinutes(Convert.ToInt16(TxtMinModV.Text));
                    //DateTime dateValue1 = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorIniPubCantDCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorIniPubCantDCi.Text.Trim().Substring(3, 2)), 0);
                    //if (dateValue > dateValue1)
                    //    lblMensaje.Text += "El tiempo de modificación de venta supera la hora de inicio de publicación <br>";
                    DateTime dateValue = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorFinRecCantCCi.Text.Trim().Substring(3, 2)), 0); //Req. 007-17 Subasta Bimestral 20170209
                    dateValue = dateValue.AddMinutes(Convert.ToInt16(TxtMinModC.Text));
                    DateTime dateValue1 = new DateTime(1900, 1, 1, Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(0, 2)), Convert.ToInt16(TxtHorIniNegociaCi.Text.Trim().Substring(3, 2)), 0);  //Req. 007-17 Subasta Bimestral 20170209
                    if (dateValue > dateValue1)
                        lblMensaje.Text += "El tiempo de modificación de compra supera la hora de inicio de negociación <br>";

                }
                catch (Exception)
                {
                    lblMensaje.Text += "Error en el formato de horas. <br>";
                }
                // Campo nuevo subasta bimestral por contingencia 20151105
                if (ddlActSubasta.SelectedValue == "6")
                {
                    string sVlidaHora = "S";  //Req. 007-07 Subasta Bimestral 20170209
                    if (TxtNoMesIniPer.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingesar El No. Mes Adelante Def Inicio Periodo subasta Bimestral. <br>";
                    else
                    {
                        if (TxtNoMesIniPer.Text.Trim() == "0")
                            lblMensaje.Text += "El No. Mes Adelante Def Inicio Periodo subasta Bimestral no puede ser igual a 0. <br>";
                    }
                    // Validacion que para el mismo tipo de subasta no exista mas de un tipo de rueda con el mismo día habil de subasta
                    if (DelegadaBase.Servicios.ValidarExistencia("t_rueda", " codigo_tipo_subasta = " + ddlActSubasta.SelectedValue + " And estado NOT IN ('Z','F') And  convert(varchar(10),fecha_prox_apertura,111) = '" + TxtFechaProx.Text.Trim() + "'  and numero_rueda <> " + hdfNumeroRueda.Value, goInfo))
                        lblMensaje.Text += "No pueden existir dos Ruedas bimestrales para la misma fecha de apertura. <br>";
                    /////////////////////////////////////////////////////////////
                    /// Validaciones Req. 007-07 Subasta Bimestral 20170209 /////
                    /////////////////////////////////////////////////////////////
                    if (TxtFecDeclaSubBimes.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingresar la Fecha de Declaración de Información de la Subasta Bimestral.<br>";
                    if (TxtFecPubliSubBimes.Text.Trim().Length <= 0)
                        lblMensaje.Text += "Debe Ingresar la Fecha de Publicación de Información de la Subasta Bimestral. <br>";
                    if (TxtHoraIniModDecSb.Text.Trim().Length <= 0)
                    {
                        lblMensaje.Text += "Debe ingresar la Hora Inicial Modificación Declaración Subasta Bimestral. <br>";
                        sVlidaHora = "N";
                    }
                    if (TxtHoraFinModDecSb.Text.Trim().Length <= 0)
                    {
                        lblMensaje.Text += "Debe ingresar la Hora Final Modificación Declaración Subasta Bimestral. <br>";
                        sVlidaHora = "N";
                    }
                    if (sVlidaHora == "S")
                    {
                        try
                        {
                            if (Convert.ToDateTime(TxtHoraIniModDecSb.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinModDecSb.Text.Trim()))
                                lblMensaje.Text += "La hora inicial de Modificación Declaración Subasta Bimestral debe ser menor que la hora final. <br>";

                            if (Convert.ToDateTime(TxtHoraIniModDecSb.Text.Trim()) < Convert.ToDateTime(TxtHorFinPubVCi.Text.Trim()))
                                lblMensaje.Text += "La hora inicial de Modificación Declaración Subasta Bimestral debe ser mayor o igual que la hora Final de Publicación Venta Contratos en Firme Bimestrales. <br>";
                        }
                        catch (Exception)
                        {
                            lblMensaje.Text += "Formato de hora de Modificación Declaración Subasta Bimestral no valido <br>";
                        }
                    }
                    /////////////////////////////////////////////////////////////
                }
            }

            if (ddlActSubasta.SelectedValue == "5")
            {
                /// Validaciones Modificacion campos subasta Negociacion Directa
                if (TxtHorInPosNd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Inicial de Ingreso de Posturas Negociación Directa.<br>";
                if (TxtHorFiPosNd.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar la Hora Final de Ingreso de Posturas Negociación Directa.<br>";
                try
                {
                    if (Convert.ToDateTime(TxtHorInPosNd.Text.Trim()) >= Convert.ToDateTime(TxtHorFiPosNd.Text.Trim()))
                        lblMensaje.Text += "La hora inicial de negociación debe ser menor que la hora final. <br>";
                }
                catch (Exception)
                {
                    lblMensaje.Text += "Formato Inválido de las horas <br>";
                }
            }
            // 20160921 UVLP
            if (ddlActSubasta.SelectedValue == "3")
            {
                string sVlidaHora = "S";
                if (TxtHoraIniPubUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinPubUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de publicación. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniPubUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinPubUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de publicación debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato inválido de hora de publicación<br>";
                    }
                }
                sVlidaHora = "S";
                if (TxtHoraIniCompUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinCompUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de ingreso de posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniCompUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinCompUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(TxtHoraIniCompUvlp.Text.Trim()) < Convert.ToDateTime(TxtHoraFinPubUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de ingreso de posturas de compra debe ser mayor o igual que la hora final de publicación. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato inválido de hora de ingreso de posturas de compra<br>";
                    }
                }
                sVlidaHora = "S";
                if (TxtHoraIniCalceUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora inicial de calce. <br>";
                    sVlidaHora = "N";
                }
                if (TxtHoraFinCalceUvlp.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la hora final de calce. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(TxtHoraIniCalceUvlp.Text.Trim()) >= Convert.ToDateTime(TxtHoraFinCalceUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(TxtHoraIniCalceUvlp.Text.Trim()) < Convert.ToDateTime(TxtHoraFinCompUvlp.Text.Trim()))
                            lblMensaje.Text += "La hora inicial de calce debe ser mayor o igual que la hora final de ingreso de posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato inválido de hora de calce<br>";
                    }
                }
            }
            /// Validaciones Subasta Transporte Req. 005-2021 20210121
            if (ddlActSubasta.SelectedValue == "11")
            {
                string sVlidaHora = "S";
                if (ddlTipoRueTra11.SelectedValue == "0")
                    lblMensaje.Text += "Debe Seleccionar el tipo de rueda. <br>";
                if (txtHoraIniPubOfr11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Publicación de oferta . <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPubOfr11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Publicación de oferta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPubOfr11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPubOfr11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación de oferta debe ser menor que la hora final. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación de oferta no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniDecPosCom11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Declaración posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDecPosCom11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Declaración posturas de compra. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDecPosCom11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDecPosCom11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Declaración posturas de compra debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniDecPosCom11.Text.Trim()) < Convert.ToDateTime(txtHoraFinPubOfr11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Declaración posturas de compra debe ser mayor o igual que la hora Final para Publicación de oferta. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Declaración posturas de compra no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniDesSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Desarrollo subasta. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinDesSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Desarrollo subasta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniDesSub11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinDesSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Desarrollo subasta debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniDesSub11.Text.Trim()) < Convert.ToDateTime(txtHoraFinDecPosCom11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Desarrollo subasta debe ser mayor o igual que la hora Final para Declaración posturas de compra. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Desarrollo subasta no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniRecAdj11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Rechazo de adjudicaciones. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinRecAdj11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Rechazo de adjudicaciones. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniRecAdj11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinRecAdj11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Rechazo de adjudicaciones debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniRecAdj11.Text.Trim()) < Convert.ToDateTime(txtHoraFinDesSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Rechazo de adjudicaciones debe ser mayor o igual que la hora Final para Desarrollo subasta. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Rechazo de adjudicaciones no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniPubRes11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Publicación resultados. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinPubRes11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Publicación resultados. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniPubRes11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinPubRes11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación resultados debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniPubRes11.Text.Trim()) < Convert.ToDateTime(txtHoraFinRecAdj11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Publicación resultados debe ser mayor o igual que la hora Final para Rechazo de adjudicaciones. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación resultados no valido <br>";
                    }
                }
                sVlidaHora = "S";
                if (txtHoraIniFinSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Inicial para Finalización de la subasta. <br>";
                    sVlidaHora = "N";
                }
                if (txtHoraFinFinSub11.Text.Trim().Length <= 0)
                {
                    lblMensaje.Text += "Debe ingresar la Hora Final para Finalización de la subasta. <br>";
                    sVlidaHora = "N";
                }
                if (sVlidaHora == "S")
                {
                    try
                    {
                        if (Convert.ToDateTime(txtHoraIniFinSub11.Text.Trim()) >= Convert.ToDateTime(txtHoraFinFinSub11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Finalización de la subasta debe ser menor que la hora final. <br>";
                        if (Convert.ToDateTime(txtHoraIniFinSub11.Text.Trim()) < Convert.ToDateTime(txtHoraFinPubRes11.Text.Trim()))
                            lblMensaje.Text += "La hora inicial para Finalización de la subasta debe ser mayor o igual que la hora Final para Publicación resultados. <br>";
                    }
                    catch (Exception)
                    {
                        lblMensaje.Text += "Formato de hora para Publicación resultados no valido <br>";
                    }
                }
            }
            ///  Hasta Aqui
            if (lblMensaje.Text == "")
            {
                goInfo.mensaje_error = "";
                lValorParametros[5] = TxtHorIniOfVenSuvcp.Text.Trim();
                lValorParametros[6] = TxtHorIniContVenSuvcp.Text.Trim();
                lValorParametros[7] = TxtHorIniPubVenSuvcp.Text.Trim();
                lValorParametros[8] = TxtHorIniOfComSuvcp.Text.Trim();
                lValorParametros[9] = TxtHorIniCalceSuvcp.Text.Trim();
                lValorParametros[10] = TxtHorIniModContSuvcp.Text.Trim();
                lValorParametros[11] = TxtFecMaxParVendSci.Text.Trim();
                lValorParametros[12] = TxtFecMaxDecVendSci.Text.Trim();
                lValorParametros[13] = TxtFecMaxRevInfSci.Text.Trim();
                lValorParametros[14] = TxtFecMaxDecCompdSci.Text.Trim();
                lValorParametros[15] = TxtNoHoraModVenSci.Text.Trim();
                lValorParametros[16] = TxtNoHoraModComSci.Text.Trim();
                lValorParametros[17] = TxtHorInPosNd.Text.Trim();
                lValorParametros[18] = TxtHorFiPosNd.Text.Trim();
                lValorParametros[19] = TxtHorIniPubVCi.Text.Trim();
                lValorParametros[20] = TxtHorFinPubVCi.Text.Trim();
                lValorParametros[21] = TxtHorIniPubCantDCi.Text.Trim();
                lValorParametros[22] = TxtHorFinPubCantDCi.Text.Trim();
                lValorParametros[23] = TxtHorIniRecCantCCi.Text.Trim();
                lValorParametros[24] = TxtHorFinRecCantCCi.Text.Trim();
                lValorParametros[25] = TxtHorIniNegociaCi.Text.Trim();
                lValorParametros[26] = TxtHorFinNegociaCi.Text.Trim();
                lValorParametros[27] = TxtFechaProx.Text.Trim();
                if (TxtMinModV.Text == "")
                    lValorParametros[28] = "0";
                else
                    lValorParametros[28] = TxtMinModV.Text.Trim();
                if (TxtMinModC.Text == "")
                    lValorParametros[29] = "0";
                else
                    lValorParametros[29] = TxtMinModC.Text.Trim();
                if (ddlActSubasta.SelectedValue == "2")
                {
                    lValorParametros[20] = TxtHorFinOfVenSuvcp.Text.Trim();
                    lValorParametros[21] = TxtHorFinContVenSuvcp.Text.Trim();
                    lValorParametros[22] = TxtHorFinPubVenSuvcp.Text.Trim();
                    lValorParametros[24] = TxtHorFinOfComSuvcp.Text.Trim();
                    lValorParametros[26] = TxtHorFinCalceSuvcp.Text.Trim();
                    lValorParametros[25] = TxtHorFinModContSuvcp.Text.Trim();
                }
                // Campo nuevo ajuste subasta bimestrales por contingencia 20151105
                if (ddlActSubasta.SelectedValue == "6")
                {
                    lValorParametros[30] = TxtNoMesIniPer.Text.Trim();
                    //////////////////////////////////////////////////////////
                    // Campos nuevos Req. 007-17 Subasta Bimestral 20170209 //
                    //////////////////////////////////////////////////////////
                    lValorParametros[12] = TxtFecDeclaSubBimes.Text.Trim();
                    lValorParametros[14] = TxtFecPubliSubBimes.Text.Trim();
                    lValorParametros[32] = TxtHoraIniModDecSb.Text.Trim();
                    lValorParametros[33] = TxtHoraFinModDecSb.Text.Trim();
                    //////////////////////////////////////////////////////////
                }
                // 20160921 UVLP
                if (ddlActSubasta.SelectedValue == "3")
                {
                    lValorParametros[19] = TxtHoraIniPubUvlp.Text.Trim();
                    lValorParametros[20] = TxtHoraFinPubUvlp.Text.Trim();
                    lValorParametros[23] = TxtHoraIniCompUvlp.Text.Trim();
                    lValorParametros[24] = TxtHoraFinCompUvlp.Text.Trim();
                    lValorParametros[17] = TxtHoraIniCalceUvlp.Text.Trim();
                    lValorParametros[18] = TxtHoraFinCalceUvlp.Text.Trim();
                }
                /// Campos Nueva Subasta Tramsporte Req. 005-2021 20210121
                if (ddlActSubasta.SelectedValue == "11")
                {
                    lValorParametros[31] = ddlTipoRueTra11.SelectedValue;
                    lValorParametros[19] = txtHoraIniPubOfr11.Text.Trim();
                    lValorParametros[20] = txtHoraFinPubOfr11.Text.Trim();
                    lValorParametros[32] = txtHoraIniDecPosCom11.Text.Trim();
                    lValorParametros[33] = txtHoraFinDecPosCom11.Text.Trim();
                    lValorParametros[25] = txtHoraIniDesSub11.Text.Trim();
                    lValorParametros[26] = txtHoraFinDesSub11.Text.Trim();
                    lValorParametros[23] = txtHoraIniRecAdj11.Text.Trim();
                    lValorParametros[24] = txtHoraFinRecAdj11.Text.Trim();
                    lValorParametros[21] = txtHoraIniPubRes11.Text.Trim();
                    lValorParametros[22] = txtHoraFinPubRes11.Text.Trim();
                    lValorParametros[7] = txtHoraIniFinSub11.Text.Trim();
                    lValorParametros[8] = txtHoraFinFinSub11.Text.Trim();
                }
                /// Hasta Aqui
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_UptRueda", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje.Text = lLector["error"].ToString();
                }
                else
                {
                    lblMensaje.Text = "Rueda Actualizada Correctamente.!";
                    manejo_bloqueo("E", hdfNumeroRueda.Value);
                    Listar();
                }
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        manejo_bloqueo("E", hdfNumeroRueda.Value);
        lblMensaje.Text = "";
        Listar();
    }
}