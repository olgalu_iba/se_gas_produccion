﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_tramos.aspx.cs"
    Inherits="BASE_frm_tramos" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_tramos.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_tramos.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_tramos.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=3">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Tramo
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoTramo" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoTramo" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Trasportador
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlTrasportador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Punto Inicial
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlPozoIni" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Punto Final
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlPozoFin" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Excento Conformación de rutas Capacidad Excedentaria UVLP
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlExcentoUvlp" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--Campo nuevo Req. 079 Mapa Interactivo  20170314 --%>
        <tr>
            <td class="td1" colspan="1">
                Publíca Bec Mapa Interactivo
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlPublicaMapa" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--Hasta Aqui --%>
        <%--20170705 rq025-17--%>
        <tr>
            <td class="td1" colspan="1">
                Flujo del Tramo
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlFlujo" runat="server">
                    <asp:ListItem Value="F">FLUJO</asp:ListItem>
                    <asp:ListItem Value="C">CONTRAFLUJO</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--Campo Nuevo Req. 005-2021 20210120--%>
        <tr>
            <td class="td1" colspan="1">
                Tipo Proyecto
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlTipoProyecto" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <%--20221012--%>
        <tr>
            <td class="td1" colspan="1">
                Moneda Vigente
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlMoneda" runat="server">
                    <asp:ListItem Value="USD">Dólares de los Estados Unidos (USD)</asp:ListItem>
                    <asp:ListItem Value="COP">Pesos colombianos (COP)</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Codigo Tramo
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCodigoTramo" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodigoTramo" runat="server" TargetControlID="TxtBusCodigoTramo"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Trasportador
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusTrasportador" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Punto Inicial
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusPozoIni" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Punto Final
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusPozoFin" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_tramo" HeaderText="Codigo Tramo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_trasportador" HeaderText="Cod Tras" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="nombre_trasportador" HeaderText="nombre Tras" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_pozo_ini" HeaderText="Cod Punto ini" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_pozo_ini" HeaderText="Punto ini" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_pozo_fin" HeaderText="Cod Punto fin" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_pozo_fin" HeaderText="punto fin" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--Campo nuevo Req. 079 Mapa Interactivo 20170314 --%>
                            <asp:BoundColumn DataField="ind_pub_bec_mapa" HeaderText="Publíca Bec Mapa Interactivo"
                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            <%--20170705 rq025-17--%>
                            <asp:BoundColumn DataField="tipo_flujo" HeaderText="tipo flujo" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--Campo Nuevo Req. 005-2021 20210120--%>
                            <asp:BoundColumn DataField="tipo_proyecto" HeaderText="Tipo Proyecto" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--20221012--%>
                            <asp:BoundColumn DataField="tipo_moneda" HeaderText="Moneda Vigente" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    </asp:Content>