﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_PuntosEntrega.aspx.cs"
    Inherits="BASE_frm_PuntosEntrega" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_PuntosEntrega.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_PuntosEntrega.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_PuntosEntrega.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Punto Entrega
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoPuntoEntre" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoPuntoEntre" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Descripción
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
                <%--<asp:RegularExpressionValidator ID="RevTxtDescripcion" ControlToValidate="TxtDescripcion"
                    runat="server" ValidationExpression="^([0-9a-zA-Z]{3})([0-9a-zA-Z .-_&])*$" ErrorMessage="Valor muy corto en el Campo Descripcion"
                    ValidationGroup="comi"> * </asp:RegularExpressionValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Destino Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlDestino" runat="server" OnSelectedIndexChanged="ddlDestino_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="G">Gas</asp:ListItem>
                    <asp:ListItem Value="T">Transporte</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trPozoIni" visible="false">
            <td class="td1">
                Código Pozo Ini
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPozoIni" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trPozoFin" visible="false">
            <td class="td1">
                Código Pozo Fin
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPozoFin" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Codigo Punto Entrega
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusPuntoEntre" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusPuntoEntre" runat="server" TargetControlID="TxtBusPuntoEntre"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Descripción
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusDescripcion" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Destino Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusDestino" runat="server">
                    <asp:ListItem Value="S">Seleccione</asp:ListItem>
                    <asp:ListItem Value="G">Gas</asp:ListItem>
                    <asp:ListItem Value="T">Transporte</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusTipoSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Codigo Punto Entrega"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="350px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="destino_rueda" HeaderText="Destino Rueda" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_subasta" HeaderText="Código Tipo Subasta"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Desc. Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Rutas" EditText="Rutas"></asp:EditCommandColumn>
                            <asp:BoundColumn DataField="codigo_pozo_ini" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_pozo_ini" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_pozo_fin" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_pozo_fin" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTramo" visible="false">
        <tr>
            <td class="td1">
                Punto Entrega
            </td>
            <td class="td2" colspan="3">
                <asp:Label ID="lblCodPunt" runat="server"></asp:Label>
                -
                <asp:Label ID="lblDescPunto" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Pozo Inicial
            </td>
            <td class="td2">
                <asp:Label ID="lblCodPozI" runat="server"></asp:Label>
                -
                <asp:Label ID="lblDescPozI" runat="server"></asp:Label>
            </td>
            <td class="td1">
                Pozo Final
            </td>
            <td class="td2">
                <asp:Label ID="lblCodPozF" runat="server"></asp:Label>
                -
                <asp:Label ID="lblDescPozF" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tramo
            </td>
            <td class="td2" colspan="3">
                <asp:DropDownList ID="ddlTramo" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrearTra" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrearTra_Click"
                    Height="40" />
                <asp:ImageButton ID="imbActualizaTra" runat="server" ImageUrl="~/Images/Actualizar.png"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalirTra" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalirTra_Click"
                    Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:DataGrid ID="dtgTramo" runat="server" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="td1"
                    ItemStyle-CssClass="td2" OnEditCommand="dtgTramo_EditCommand" HeaderStyle-CssClass="th1">
                    <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                    <ItemStyle CssClass="td2"></ItemStyle>
                    <Columns>
                        <asp:BoundColumn DataField="codigo_ruta" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_punto_entrega" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_pozo_ini" HeaderText="Cod Ini" ItemStyle-HorizontalAlign="center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_pozo_ini" HeaderText="desc pozo ini" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_pozo_fin" HeaderText="Cod Fin" ItemStyle-HorizontalAlign="center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_pozo_fin" HeaderText="desc pozo fin" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="orden" HeaderText="orden" ItemStyle-HorizontalAlign="Center">
                        </asp:BoundColumn>
                        <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                    </Columns>
                    <HeaderStyle CssClass="th1"></HeaderStyle>
                </asp:DataGrid>
                <asp:HiddenField ID="hndOrden" runat="server" />
                <asp:HiddenField ID="hndPozoFin" runat="server" />
                <asp:HiddenField ID="hndCuenta" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>