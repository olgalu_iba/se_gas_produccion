﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using Segas.Web.Elements;

public partial class BASE_frm_gasoductoConexion : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    public string HdnCodDetalle
    {
        get { return ViewState["HdnCodDetalle"] != null && !string.IsNullOrEmpty(ViewState["HdnCodDetalle"].ToString()) ? ViewState["HdnCodDetalle"].ToString() : string.Empty; }
        set { ViewState["HdnCodDetalle"] = value; }
    }

    InfoSessionVO goInfo = null;
    static string lsTitulo = "Gasoductos de Conexión";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);
        Master.Titulo = lsTitulo ;

        /// Activacion de los Botones
        buttons.Inicializar(ruta: "t_capacidad_inyectada");
        buttons.CrearOnclick += btnNuevo;
        buttons.FiltrarOnclick += btnConsultar_Click;
        buttons.ExportarExcelOnclick += ImgExcel_Click;
        //buttons.ExportarPdfOnclick += ImgPdf_Click;

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles2(lConexion.gObjConexion, ddlOperador, "m_operador ope", " estado = 'A' and exists (select 1 from m_operador_conexion con where ope.tipo_operador = con.tipo_operador and con.estado ='A' ) order by razon_social", 0, 4);
            LlenarControles2(lConexion.gObjConexion, ddlBusOperador, "m_operador ope", " estado = 'A' and exists (select 1 from m_operador_conexion con where ope.tipo_operador = con.tipo_operador and con.estado ='A' ) order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlDepartamentoIni, "m_divipola", " estado = 'A' and codigo_ciudad ='0' and codigo_centro='0' order by nombre_departamento", 1, 2);
            LlenarControles(lConexion.gObjConexion, ddlDepartamentoFin, "m_divipola", " estado = 'A' and codigo_ciudad ='0' and codigo_centro='0' order by nombre_departamento", 1, 2);
            LlenarControles(lConexion.gObjConexion, ddlPuntoIni, "m_pozo", " estado = 'A' and ind_campo_pto='C' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusGasoducto, "m_gasoducto_conexion ", " estado = 'A'  order by descripcion", 0, 1);

            //if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            //{
            //    lsIndica = this.Request.QueryString["lsIndica"].ToString();
            //}
            //if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            //{
            //    Listar();
            //}
            //else if (lsIndica == "N")
            //{
            //    Nuevo();
            //}
            //else if (lsIndica == "B")
            //{
            //    Buscar();
            //}
            Listar();
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {

        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo,"m_gasoducto_conexion" );
        foreach (DataGridItem Grilla in dtgMaestro.Items)
        {
            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
            lkbModificar.Visible = (bool)permisos["UPDATE"];  //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
            lkbEliminar.Visible = (bool)permisos["DELETE"];  //20170131 Modif Inf Operativa Participantes Req.003-17 //20190306 rq013-19
        }
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    protected void btnNuevo(object sender, EventArgs e)
    {
        mdlgasoductoConexionLabel.InnerText = "Agregar";
        // Abre el modal de Agregar
        Modal.Abrir(this, gasoductoConexion.ID, gasoductoConexionInside.ID);
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigo.Visible = false;
        LblCodigo.Visible = true;
        LblCodigo.Text = "Automatico";
        ddlEstado.Enabled = false;
        ddlEstado.SelectedValue = "A";
        ddlTipo_SelectedIndexChanged(null, null);
        ddlConectado_SelectedIndexChanged(null, null);
        imbCrearFuente.Visible = false;
        imbModFuente.Visible = false;
        activaCampos();
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        var lblMensaje = "";
        if (modificar != null && modificar != "")
        {
            try
            {
                string lsPunto = "0";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_gasoducto_conexion", " codigo_gasoducto= " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigo.Text = lLector["codigo_gasoducto"].ToString();
                        TxtCodigo.Text = lLector["codigo_gasoducto"].ToString();
                        TxtDescripcion.Text = lLector["descripcion"].ToString();
                        //20190822 rq054-19
                        //try
                        //{
                        //    ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                        //}
                        //catch (Exception ex)
                        //{
                        //    lblMensaje.Text += "El operador del registro no existe o está inactivo<br>";
                        //}
                        ddlTipo.SelectedValue = lLector["tipo_conexion"].ToString();
                        ddlTipo_SelectedIndexChanged(null, null);
                        if (ddlTipo.SelectedValue == "N")
                        {
                            tblDetalle.Visible = true; //20190822 rq054-19
                            imbCrearFuente.Visible = true;
                            dtgDetalle.Visible = true;
                            ddlPuntoIni.SelectedValue = "0";
                            ddlDepartamentoIni.SelectedValue = "0";
                            ddlDepartamentoIni_SelectedIndexChanged(null, null);
                            ddlCiudadIni.SelectedValue = "0";
                        }
                        else
                        {
                            tblDetalle.Visible = false; //20190822 rq054-19
                            imbCrearFuente.Visible = false;
                            dtgDetalle.Visible = false;
                            ddlPuntoIni.SelectedValue = lLector["punto_inicial"].ToString();
                            ddlDepartamentoIni.SelectedValue = lLector["departamento_ini"].ToString();
                            ddlDepartamentoIni_SelectedIndexChanged(null, null);
                            ddlCiudadIni.SelectedValue = lLector["municipio_ini"].ToString();
                        }

                        imbModFuente.Visible = false;
                        ddlConectado.SelectedValue = lLector["conexion_snt"].ToString();
                        ddlConectado_SelectedIndexChanged(null, null);
                        ddlTipoPuntoFin.SelectedValue = lLector["tipo_punto_fin"].ToString();
                        ddlTipoPuntoFin_SelectedIndexChanged(null, null);
                        ddlPuntoFin.SelectedValue = lLector["punto_final"].ToString();
                        ddlDepartamentoFin.SelectedValue = lLector["departamento_fin"].ToString();
                        ddlDepartamentoFin_SelectedIndexChanged(null, null);
                        ddlCiudadFin.SelectedValue = lLector["municipio_fin"].ToString();
                        ddlCmmp.SelectedValue = lLector["aplica_control_cmmp"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        lLector.Close();
                        lLector.Dispose();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigo.Visible = false;
                        LblCodigo.Visible = true;
                        ddlOperador.Enabled = true;
                        ddlEstado.Enabled = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    if (lblMensaje == "") //20160809 carga ptdv
                        manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje = "No se Puede editar el Registro porque está Bloqueado. Código gasoducto de conexión" + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje = ex.Message;
            }
        }
        if (lblMensaje == "")
        {
            lblTitulo.Text = "Modificar " + lsTitulo;
            //Abre el modal de Agregar
            Modal.Abrir(this, gasoductoConexion.ID, gasoductoConexionInside.ID);
            mdlgasoductoConexionLabel.InnerHtml = "Modificar " + lsTitulo;
        }
        else
            Toastr.Warning(this, lblMensaje);
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        lblTitulo.Text = "Consultar " + lsTitulo;
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_gasoducto", "@P_descripcion", "@P_codigo_operador" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0" };
        var lblMensaje = "";

        try
        {
            if (lblMensaje == "")
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                if (ddlBusGasoducto.SelectedValue != "0")
                    lValorParametros[0] = ddlBusGasoducto.SelectedValue;
                if (ddlBusOperador.SelectedValue != "0")
                    lValorParametros[2] = ddlBusOperador.SelectedValue;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetGasoductoConexion", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();

                //HyperLink imbControl;
                //foreach (DataGridItem Grilla in this.dtgMaestro.Items)
                //{
                //    imbControl = (HyperLink)Grilla.Cells[19].Controls[1];
                //    imbControl.Attributes.Add("onClick", "javascript:Popup=window.open('frm_EntregaUsuarioFinalDet.aspx?codigo_ent=" + Grilla.Cells[0].Text + "','Contratos','width=450,height=250,left=350,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no');");
                //}

            }
        }
        catch (Exception ex)
        {
            lblMensaje = ex.Message;
        }
        if (lblMensaje != "") 
            Toastr.Error(this, lblMensaje);
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected string Validaciones(string lsAccion) //20161222 rq112 entega usr final
    {
        string lsError = "";

        if (TxtDescripcion.Text.Trim() == "")
            lsError += " Debe digitar la descripción del gasoducto<br>";
        else
        {
            if (lsAccion == "C" && VerificarExistencia("m_gasoducto_conexion", "descripcion = '" + TxtDescripcion.Text + "'"))
                lsError += " La descripción del gasoducto ya existe<br>";
            if (lsAccion == "M" && VerificarExistencia("m_gasoducto_conexion", "descripcion = '" + TxtDescripcion.Text + "' and codigo_gasoducto <>" + LblCodigo.Text))
                lsError += " La descripción del gasoducto ya existe<br>";
        }

        //20190822 rq054-19
        //if (ddlOperador.SelectedValue == "0")
        //    lsError += "Debe seleccionar el operador<br>";
        if (lsAccion == "C" || ddlTipo.SelectedValue == "E")
        {
            //20190822 rq054-19
            if (lsAccion == "C" && ddlOperador.SelectedValue == "0")
                lsError += "Debe seleccionar el operador<br>";
            if (ddlPuntoIni.SelectedValue == "0")
                lsError += "Debe seleccionar el punto inicial<br>";
            if (ddlDepartamentoIni.SelectedValue != "0" && ddlCiudadIni.SelectedValue == "0")
                lsError += "Debe seleccionar tanto el departamento como la ciudad inicial o ninguno de los dos<br>";
        }
        if (lsAccion == "M")
        {
            //20190822 rq054-19
            if (!VerificarExistencia("m_gasoducto_conexion_ope", "codigo_gasoducto = " + LblCodigo.Text))
                lsError += "Debe seleccionar al menos un operador<br>";
            if (ddlTipo.SelectedValue == "N" && !VerificarExistencia("m_gasoducto_conexion_det", "codigo_gasoducto = " + LblCodigo.Text))
                lsError += "Debe seleccionar al menos un punto inicial<br>";
        }
        if (ddlPuntoFin.SelectedValue == "0")
            lsError += "Debe seleccionar el Punto Final<br>";
        if (ddlDepartamentoFin.SelectedValue != "0" && ddlCiudadFin.SelectedValue == "0")
            lsError += "Debe seleccionar tanto el departamento como la ciudad final o ninguno de los dos<br>";

        return lsError;
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        var lblMensaje = "";
        lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
        LblCodigo.Text = lCodigoRegistro; //20190822 rq054-19
        ///////////////////////////////////////////////////////////////////////////////////
        ///// Control Nuevo para Modif Inf Operativa Participantes Req. 003-17 20170216 ///
        ///////////////////////////////////////////////////////////////////////////////////
        if (e.CommandName.Equals("Modificar"))
        {
            CargarDatosDet(); //20190822 rq054-19
            CargarDatosOpe(); //20190822 rq054-19
            activaCampos();
            Modificar(lCodigoRegistro);
        }
        // Evento Eliminar para los Participantes Modif Inf Operativa Participantes Req. 003-17 20170216
        if (e.CommandName.Equals("Eliminar"))
        {
            string[] lsNombreParametros = { "@P_codigo_gasoducto", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
            string[] lValorParametros = { lCodigoRegistro, "3" };
            lblMensaje = "";
            try
            {
                if (lblMensaje == "")
                {
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoCOnexion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                    {
                        lblMensaje = "Se presentó un Problema en la Eliminación del Registro.! " + goInfo.mensaje_error.ToString();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Eliminado Correctamente" + "');", true);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                lblMensaje = ex.Message;
            }
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje);
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTable, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTable, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

    /// <summary>



    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    /// // 20180126 rq107-16
    protected void LlenarControles2(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector["codigo_operador"].ToString();
            lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Dispose();
        lLector.Close();
    }

    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_gasoducto_conexion' and llave_registro='codigo_gasoducto=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_gasoducto=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_gasoducto_conexion";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_gasoducto_conexion", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, EventArgs e)
    {
        string[] lValorParametros = { "0", "", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Código Gasoducto: " + TxtBusCodigo.Text;
            }
            if (ddlBusGasoducto.SelectedValue != "0")
            {
                lValorParametros[0] = ddlBusGasoducto.SelectedValue;
                lsParametros = " Gasoducto: " + ddlBusGasoducto.SelectedItem;
            }
            if (ddlBusOperador.SelectedValue != "0")
            {
                lValorParametros[2] = ddlBusOperador.SelectedValue;
                lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetGasoductoConexionExc&nombreParametros=@P_codigo_gasoducto*@P_descripcion*@P_codigo_operador&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_gasoducto*descripcion*codigo_operador*nombre_operador&titulo_informe=Listado de Gasoductos de Conexión&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!");
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_gasoducto<> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_gasoducto_conexion&procedimiento=pa_ValidarExistencia&columnas=codigo_gasoducto*descripcion*codigo_operador*tipo_conexion*conexion_snt*tipo_punto_fin*departamento_fin*municipio_fin*aplica_control_cmmp*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            Toastr.Warning(this, "No se Pudo Generar el Informe.!");
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlPuntoIni.Items.Clear();
        if (ddlTipo.SelectedValue == "N")
        {
            LlenarControles(lConexion.gObjConexion, ddlPuntoIni, "m_pozo", " ind_campo_pto='C' and estado = 'A' ", 0, 1);
            imbCrearFuente.Visible = true;
            dtgDetalle.Visible = true;
            tblDetalle.Visible = true; //2019/0822 rq054-19
        }
        else
        {
            LlenarControles(lConexion.gObjConexion, ddlPuntoIni, "m_pozo", " (ind_importacion='S' or ind_regasificacion ='S') and estado = 'A' ", 0, 1);
            imbCrearFuente.Visible = false;
            imbModFuente.Visible = false;
            dtgDetalle.Visible = false;
            tblDetalle.Visible = false; //2019/0822 rq054-19
        }
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlConectado_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlTipoPuntoFin.Items.Clear();

        if (ddlConectado.SelectedValue == "S")
        {
            System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
            lItem.Value = "S";
            lItem.Text = "Punto de Salida";
            ddlTipoPuntoFin.Items.Add(lItem);
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = "T";
            lItem1.Text = "Punto de Trasferencia";
            ddlTipoPuntoFin.Items.Add(lItem1);
            System.Web.UI.WebControls.ListItem lItem2 = new System.Web.UI.WebControls.ListItem();
            lItem2.Value = "E";
            lItem2.Text = "Punto de Entrada";
            ddlTipoPuntoFin.Items.Add(lItem2);
        }

        else
        {
            System.Web.UI.WebControls.ListItem lItem3 = new System.Web.UI.WebControls.ListItem();
            lItem3.Value = "R";
            lItem3.Text = "Mercado Relevante";
            ddlTipoPuntoFin.Items.Add(lItem3);
            System.Web.UI.WebControls.ListItem lItem4 = new System.Web.UI.WebControls.ListItem();
            lItem4.Value = "G";
            lItem4.Text = "Gasoducto Dedicado";
            ddlTipoPuntoFin.Items.Add(lItem4);
        }
        ddlTipoPuntoFin_SelectedIndexChanged(null, null);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlTipoPuntoFin_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlPuntoFin.Items.Clear();
        if (ddlTipoPuntoFin.SelectedValue == "S")
            LlenarControles(lConexion.gObjConexion, ddlPuntoFin, "m_punto_salida_snt", "  estado = 'A' ", 0, 2);
        if (ddlTipoPuntoFin.SelectedValue == "T")
            LlenarControles(lConexion.gObjConexion, ddlPuntoFin, "m_pozo", " ind_trasferencia='S' and estado = 'A' ", 0, 1);
        if (ddlTipoPuntoFin.SelectedValue == "E")
            LlenarControles(lConexion.gObjConexion, ddlPuntoFin, "m_pozo", " ind_entrada='S' and estado = 'A' ", 0, 1);
        if (ddlTipoPuntoFin.SelectedValue == "R")
            LlenarControles(lConexion.gObjConexion, ddlPuntoFin, "m_mercado_relevante", " estado = 'A' ", 0, 1);
        if (ddlTipoPuntoFin.SelectedValue == "G")
            LlenarControles(lConexion.gObjConexion, ddlPuntoFin, "m_gasoducto_dedicado", " estado = 'A' ", 0, 1);
        lConexion.Cerrar();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlDepartamentoIni_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlCiudadIni.Items.Clear();
        LlenarControles(lConexion.gObjConexion, ddlCiudadIni, "m_divipola", " estado = 'A' and codigo_departamento =" + ddlDepartamentoIni.SelectedValue + " and codigo_ciudad <>'0' and codigo_centro='0' order by nombre_ciudad", 3, 4);
        lConexion.Cerrar();

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlDepartamentoFin_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlCiudadFin.Items.Clear();
        LlenarControles(lConexion.gObjConexion, ddlCiudadFin, "m_divipola", " estado = 'A' and codigo_departamento =" + ddlDepartamentoFin.SelectedValue + " and codigo_ciudad <>'0' and codigo_centro='0' order by nombre_ciudad", 3, 4);
        lConexion.Cerrar();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_gasoducto", "@P_descripcion", "@P_codigo_operador", "@P_tipo_conexion", "@P_punto_inicial", "@P_departamento_ini", "@P_municipio_ini", "@P_conexion_snt", "@P_tipo_punto_fin", "@P_punto_final", "@P_departamento_fin", "@P_municipio_fin", "@P_aplica_control_cmmp", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Char, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "", "0", "0", "0", "", "", "0", "0", "0", "", "", "1" };
        var lblMensaje = "";

        try
        {
            lblMensaje = Validaciones("C");
            if (lblMensaje == "")
            {
                lValorParametros[1] = TxtDescripcion.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlTipo.SelectedValue;
                lValorParametros[4] = ddlPuntoIni.SelectedValue;
                lValorParametros[5] = ddlDepartamentoIni.SelectedValue;
                lValorParametros[6] = ddlCiudadIni.SelectedValue;
                lValorParametros[7] = ddlConectado.SelectedValue;
                lValorParametros[8] = ddlTipoPuntoFin.SelectedValue;
                lValorParametros[9] = ddlPuntoFin.SelectedValue;
                lValorParametros[10] = ddlDepartamentoFin.SelectedValue;
                lValorParametros[11] = ddlCiudadFin.SelectedValue;
                lValorParametros[12] = ddlCmmp.SelectedValue;
                lValorParametros[13] = ddlEstado.SelectedValue;
                lConexion.Abrir();

                SqlDataReader lLector;
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetGasoductoConexion", lsNombreParametros, lTipoparametros, lValorParametros);
                if (!lLector.HasRows)
                {
                    lblMensaje = "Se presentó un Problema en la Creación del gasoducto de conexión.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lLector.Read();
                    LblCodigo.Text = lLector["codigo_gasoducto"].ToString();
                    lLector.Close();
                    imbCrear.Visible = false;
                    inactivaCampos();
                    //20190822 rq054-19
                    if (ddlTipo.SelectedValue == "N")
                    {
                        imbCrearFuente.Visible = true;
                        ddlPuntoIni.SelectedValue = "0";
                        ddlDepartamentoIni.SelectedValue = "0";
                        ddlDepartamentoIni_SelectedIndexChanged(null, null);
                        CargarDatosDet();
                    }
                    else
                    {
                        ddlPuntoIni.Enabled = false;
                        ddlDepartamentoIni.Enabled = false;
                        ddlCiudadIni.Enabled = false;
                    }
                    lConexion.Cerrar();
                    ddlOperador.SelectedValue = "0"; //20190822 rq054-19
                    CargarDatosOpe(); //20190822 rq054-19

                }
                lLector.Close();
            }
        }
        catch (Exception ex)
        {
            lblMensaje = ex.Message;
        }
        if (lblMensaje !="")
            Toastr.Warning(this, lblMensaje.ToString());
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_gasoducto", "@P_descripcion", "@P_codigo_operador", "@P_tipo_conexion", "@P_punto_inicial", "@P_departamento_ini", "@P_municipio_ini", "@P_conexion_snt", "@P_tipo_punto_fin", "@P_punto_final", "@P_departamento_fin", "@P_municipio_fin", "@P_aplica_control_cmmp", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Char, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "", "0", "0", "0", "", "", "0", "0", "0", "", "", "2" };
        var lblMensaje = "";
        try
        {

            lblMensaje = Validaciones("M");

            if (lblMensaje == "")
            {
                lValorParametros[0] = LblCodigo.Text;
                lValorParametros[1] = TxtDescripcion.Text;
                lValorParametros[2] = ddlOperador.SelectedValue;
                lValorParametros[3] = ddlTipo.SelectedValue;
                lValorParametros[4] = ddlPuntoIni.SelectedValue;
                lValorParametros[5] = ddlDepartamentoIni.SelectedValue;
                lValorParametros[6] = ddlCiudadIni.SelectedValue;
                lValorParametros[7] = ddlConectado.SelectedValue;
                lValorParametros[8] = ddlTipoPuntoFin.SelectedValue;
                lValorParametros[9] = ddlPuntoFin.SelectedValue;
                lValorParametros[10] = ddlDepartamentoFin.SelectedValue;
                lValorParametros[11] = ddlCiudadFin.SelectedValue;
                lValorParametros[12] = ddlCmmp.SelectedValue;
                lValorParametros[13] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoConexion", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje = "Se presentó un Problema en la Actualización del gasoducto de conexión.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigo.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigo.Text);
            lblMensaje = ex.Message;
        }
        if (lblMensaje != "")
            Toastr.Warning(this, lblMensaje.ToString());
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click1(object sender, EventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigo.Text != "")
            manejo_bloqueo("E", LblCodigo.Text);
        //Cierra el modal de Agregar
        Modal.Cerrar(this, gasoductoConexion.ID);
        Listar();
    }
    /// <summary>
    /// Nombre: CargarDatosDet
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    // 20161222 rq112 entrega usuario final
    private void CargarDatosDet()
    {
        string[] lsNombreParametros = { "@P_codigo_gasoducto" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { "0" };
        var lblMensaje = "";
        try
        {
            if (LblCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = LblCodigo.Text;
            }
            lConexion.Abrir();
            dtgDetalle.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetGasoductoConexionDet", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgDetalle.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje = ex.Message;
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje );

    }
    /// <summary>
    /// Nombre: CargarDatosDet
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    // 20190822 rq054-19
    private void CargarDatosOpe()
    {
        string[] lsNombreParametros = { "@P_codigo_gasoducto" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int };
        string[] lValorParametros = { "0" };
        var lblMensaje = "";
        try
        {
            if (LblCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = LblCodigo.Text;
            }
            lConexion.Abrir();
            dtgOperador.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetGasoductoConexionOpe", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgOperador.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje = ex.Message;
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje);
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    // 20161222 rq112 entega usuario final
    protected void dtgDetalle_EditCommand(object source, DataGridCommandEventArgs e)
    {
        var lblMensaje = "";
        if (e.CommandName.Equals("Modificar"))
        {
            HdnCodDetalle = this.dtgDetalle.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlPuntoIni.SelectedValue = this.dtgDetalle.Items[e.Item.ItemIndex].Cells[2].Text;
            ddlDepartamentoIni.SelectedValue = this.dtgDetalle.Items[e.Item.ItemIndex].Cells[5].Text;
            ddlDepartamentoIni_SelectedIndexChanged(null, null);
            ddlCiudadIni.SelectedValue = this.dtgDetalle.Items[e.Item.ItemIndex].Cells[7].Text;
            imbCrearFuente.Visible = false;
            imbModFuente.Visible = true;
        }
        if (e.CommandName.Equals("Eliminar"))
        {
            string[] lsNombreParametros = { "@P_codigo_detalle_fte", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { this.dtgDetalle.Items[e.Item.ItemIndex].Cells[0].Text, "3" };
            lblMensaje = "";
            try
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoConexionDet", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje = "Se presentó un Problema en la Eliminación de la fuente.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    CargarDatosDet();
                }
            }
            catch (Exception ex)
            {
                lblMensaje = ex.Message;
            }
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje);
    }
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    // 20190822 rq054-19
    protected void dtgOperador_EditCommand(object source, DataGridCommandEventArgs e)
    {
        var lblMensaje = "";
        if (e.CommandName.Equals("Eliminar"))
        {
            string[] lsNombreParametros = { "@P_codigo_detalle_ope", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
            string[] lValorParametros = { this.dtgOperador.Items[e.Item.ItemIndex].Cells[0].Text, "3" };
            lblMensaje = "";
            try
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoConexionOpe", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje = "Se presentó un Problema en la Eliminación del operador.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lConexion.Cerrar();
                    CargarDatosOpe();
                }
            }
            catch (Exception ex)
            {
                lblMensaje = ex.Message;
            }
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje);
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    protected void imbCrearFuente_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_detalle_fte", "@P_codigo_gasoducto", "@P_codigo_fuente", "@P_departamento_ini", "@P_municipio_ini", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, };
        string[] lValorParametros = { "0", LblCodigo.Text, ddlPuntoIni.SelectedValue, ddlDepartamentoIni.SelectedValue, ddlCiudadIni.SelectedValue, "1" };
        var lblMensaje = "";
        //20190822 rq054-19
        try
        {
            Convert.ToInt32(LblCodigo.Text);
        }
        catch (Exception ex)
        {
            lblMensaje += "Debe crear el gasoducto<br>";
        }
        if (ddlPuntoIni.SelectedValue == "0")
            lblMensaje += " Debe Seleccionar la fuente<br>";
        if (ddlDepartamentoIni.SelectedValue != "0" && ddlCiudadIni.SelectedValue == "0")
            lblMensaje += " Debe Seleccionar tanto el departamento como el municipio<br>";
        try
        {
            if (lblMensaje == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoConexionDet", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje = "Se presentó un Problema en la Creación de la fuente.!";
                    lConexion.Cerrar();
                }
                else
                {
                    ddlPuntoIni.SelectedValue = "0";
                    ddlDepartamentoIni.SelectedValue = "0";
                    ddlDepartamentoIni_SelectedIndexChanged(null, null);
                    lConexion.Cerrar();
                    CargarDatosDet();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje = ex.Message;
            lConexion.Cerrar();
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje);
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// 20190822 rq054-19
    protected void imbCrearOpe_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_detalle_ope", "@P_codigo_gasoducto", "@P_codigo_operador", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, };
        string[] lValorParametros = { "0", LblCodigo.Text, ddlOperador.SelectedValue, "1" };
        var lblMensaje = "";
        if (ddlOperador.SelectedValue == "0")
            lblMensaje += " Debe Seleccionar el operador<br>";
        try
        {
            Convert.ToInt32(LblCodigo.Text);
            if (VerificarExistencia("m_gasoducto_conexion_ope", "codigo_gasoducto = " + LblCodigo.Text + " and codigo_operador = " + ddlOperador.SelectedValue))
                lblMensaje += " El operador ya está asociado al gasoducto<br>";
        }
        catch (Exception ex)
        {
            lblMensaje  += "Debe crear el gasoducto<br>";
        }
        try
        {
            if (lblMensaje == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoConexionOpe", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje = "Se presentó un Problema en la Creación del operador.!";
                    lConexion.Cerrar();
                }
                else
                {
                    ddlOperador.SelectedValue = "0";
                    lConexion.Cerrar();
                    CargarDatosOpe();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje = ex.Message;
            lConexion.Cerrar();
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje);
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    protected void imbModFuente_Click1(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_detalle_fte", "@P_codigo_gasoducto", "@P_codigo_fuente", "@P_departamento_ini", "@P_municipio_ini", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, };
        string[] lValorParametros = { HdnCodDetalle , "0", ddlPuntoIni.SelectedValue, ddlDepartamentoIni.SelectedValue, ddlCiudadIni.SelectedValue, "2" };
        var lblMensaje= "";
        if (ddlPuntoIni.SelectedValue == "0")
            lblMensaje += " Debe Seleccionar la fuente<br>";
        if (ddlDepartamentoIni.SelectedValue != "0" && ddlCiudadIni.SelectedValue == "0")
            lblMensaje += " Debe Seleccionar tanto el departamento como el municipio o ninguno de los dos<br>";
        try
        {
            if (lblMensaje == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoConexionDet", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje = "Se presentó un Problema en la Modificacón de la fuente.!";
                    lConexion.Cerrar();
                }
                else
                {
                    ddlPuntoIni.SelectedValue = "0";
                    ddlDepartamentoIni.SelectedValue = "0";
                    ddlDepartamentoIni_SelectedIndexChanged(null, null);
                    lConexion.Cerrar();
                    CargarDatosDet();
                    imbModFuente.Visible = false;
                    imbCrearFuente.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje= ex.Message;
            lConexion.Cerrar();
        }
        if (lblMensaje != "")
            Toastr.Error(this, lblMensaje);
    }
    //20161222 Rq112 entrega usr final
    protected void inactivaCampos()
    {
        //ddlOperador.Enabled = false; 20190822 rq054-19
        TxtDescripcion.Enabled = false;
        ddlTipo.Enabled = false;
        ddlConectado.Enabled = false;
        ddlTipoPuntoFin.Enabled = false;
        ddlPuntoFin.Enabled = false;
        ddlDepartamentoFin.Enabled = false;
        ddlCiudadFin.Enabled = false;
        ddlCmmp.Enabled = false;
    }
    protected void activaCampos()
    {
        TxtDescripcion.Enabled = true;
        ddlTipo.Enabled = true;
        ddlConectado.Enabled = true;
        ddlTipoPuntoFin.Enabled = true;
        ddlPuntoFin.Enabled = true;
        ddlDepartamentoFin.Enabled = true;
        ddlCiudadFin.Enabled = true;
        ddlCmmp.Enabled = true;
    }
}