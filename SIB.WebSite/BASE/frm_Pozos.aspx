﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_Pozos.aspx.cs"
    Inherits="BASE_frm_Pozos" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right" bgcolor="#85BF46">
                    <table border="0" cellspacing="2" cellpadding="2" align="right">
                        <tr>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_Pozos.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_Pozos.aspx?lsIndica=L">Listar</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_Pozos.aspx?lsIndica=B">Consultar</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                            </td>
                            <%--20190621 rq039-19--%>

                            <%--<td class="tv2"></td>
                            <td class="tv2">
                                <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                    Width="70%" />
                            </td>--%>
                            <td class="tv2"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
            id="tblTitulo">
            <tr>
                <td align="center" class="th3">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                </td>
            </tr>
        </table>
        <br /><br /><br /><br /><br /><%--20211119 control ptdvf mp--%>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
            id="tblCaptura">
            <tr>
                <td class="td1">Código Pozo
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtCodigoPozo" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:Label ID="LblCodigoPozo" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1" colspan="1">Descripción
                </td>
                <td class="td2" colspan="1">
                    <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
                    <%-- <asp:RegularExpressionValidator ID="RevTxtDescripcion" ControlToValidate="TxtDescripcion"
                    runat="server" ValidationExpression="^([0-9a-zA-Z]{3})([0-9a-zA-Z .-_&])*$" ErrorMessage="Valor muy corto en el Campo Descripcion"
                    ValidationGroup="comi"> * </asp:RegularExpressionValidator>--%>
                </td>
            </tr>
            <tr>
                <td class="td1" colspan="1">Tipo Campo
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="DdlTipoCampo" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1" colspan="1">campo
                <%--20160725 --%>
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="DdlCampoPto" runat="server" OnSelectedIndexChanged="DdlCampoPto_SelectedIndexChanged"
                        AutoPostBack="true">
                        <%--Ajuste evento del campo Req. 007-17 Subasta Bimestral 20170209--%>
                        <asp:ListItem Value="P">No</asp:ListItem>
                        <asp:ListItem Value="C">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20160725 --%>
            <tr>
                <td class="td1" colspan="1">Punto de Transferencia
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="ddlTrasf" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20160725 --%>
            <tr>
                <td class="td1" colspan="1">Punto Intermedio
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="ddlIntermedio" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20160725 --%>
            <tr>
                <td class="td1" colspan="1">Punto Estándar de Entrega
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="ddlEstandar" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20160725 --%>
            <tr>
                <td class="td1" colspan="1">Punto de Entrada
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="ddlEntrada" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--Campo nuevo Req. 007-17 Subasta Bimestral  20170209 --%>
            <tr runat="server" id="TrCampo" visible="false">
                <td class="td1" colspan="1">Campo en declinación
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="ddlCampoDeclinacion" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--Hasta Aqui --%>
            <%--Campo nuevo Req. 079 Mapa Interactivo  20170314 --%>
            <tr runat="server" id="TrCampo1" visible="false">
                <td class="td1" colspan="1">Pública Bec Mapa Interactivo
                </td>
                <td class="td2" colspan="1">
                    <asp:DropDownList ID="ddlPublicaMapa" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--Hasta Aqui --%>
            <%--20190306 finn rq013-19--%>
            <tr>
                <td class="td1">Departamento
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlDepartamento" runat="server" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Ciudad
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlCiudad" runat="server" OnSelectedIndexChanged="ddlCiudad_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Centro Poblado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlCentro" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Tramo
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlTramo" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Maneja telemetría
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlTelemetria" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20190306 finn rq013-19--%>
            <%--20190404 rq018-19 fase II--%>
            <tr>
                <td class="td1">Punto de formación de precios
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlPrecio" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20190621 rq039-19 --%>
            <tr>
                <td class="td1">Punto de importación
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlImporta" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20190621 rq039-19 --%>
            <tr>
                <td class="td1">Punto de Regasificación
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlRegasifica" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Estado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlEstado" runat="server">
                        <asp:ListItem Value="A">Activo</asp:ListItem>
                        <asp:ListItem Value="I">Inactivo</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="2" align="center">
                    <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                        ValidationGroup="comi" Height="40" />
                    <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                        OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                    <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                        CausesValidation="false" Height="40" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                </td>
            </tr>
        </table>
        <br />
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblBuscar" visible="false">
            <tr>
                <td class="td1">Código Punto SNT
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusCodigoPozo" runat="server" autocomplete="off"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodigoPozo" runat="server" TargetControlID="TxtBusCodigoPozo"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Descripción
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusDescripcion" runat="server" autocomplete="off"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="Table1" visible="false">
            <tr>
                <td class="td1">Código Punto SNT
                </td>
                <td class="td2">
                    <asp:TextBox ID="TextBox1" runat="server" autocomplete="off"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtBusCodigoPozo"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Descripción
                </td>
                <td class="td2">
                    <asp:TextBox ID="TextBox2" runat="server" autocomplete="off"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />
                </td>
            </tr>
        </table>
        <%--20190306 rq013-19--%>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblDetalle" visible="false">
            <tr>
                <td class="td1">Fuente
                </td>
                <td class="td2">
                    <asp:Label ID="lblFuente" runat ="server" ></asp:Label> - 
                    <asp:Label ID="lblDescripcion" runat ="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1">Potencial de producción
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlPotencial" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Campo Mayor">Campo Mayor</asp:ListItem>
                        <asp:ListItem Value="Campo Menor">Campo Menor</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Fuente Existente
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlFuente" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Campo Existente">Campo Existente</asp:ListItem>
                        <asp:ListItem Value="Campo Nuevo">Campo Nuevo</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Interconexión
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlInterconexion" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Conectado">Conectado</asp:ListItem>
                        <asp:ListItem Value="Aislado">Aislado</asp:ListItem>
                        <asp:ListItem Value="Importación">Importación</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Estado de producción
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlEstadoProd" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Pruebas Extensas">Pruebas Extensas</asp:ListItem>
                        <asp:ListItem Value="Comercial">Comercial</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Tipo de explotación
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlTipoExp" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Convencional">Convencional</asp:ListItem>
                        <asp:ListItem Value="No Convencional">No Convencional</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Destino Inyección
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlDestino" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Sistema Nacional de Transporte - SNT">Sistema Nacional de Transporte - SNT</asp:ListItem>
                        <asp:ListItem Value="Gasoducto de Conexión">Gasoducto de Conexión</asp:ListItem>
                        <asp:ListItem Value="Gasoducto Dedicado">Gasoducto Dedicado</asp:ListItem>
                        <asp:ListItem Value="Gasoducto Virtual">Gasoducto Virtual</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trMercado" runat ="server" >
                <td class="td1">Entrega Mercado Relevante
                </td>
                <td class="td2" >
                    <asp:DropDownList ID="ddlEntrega" runat="server"  OnSelectedIndexChanged="ddlEntrega_SelectedIndexChanged" AutoPostBack ="true" >
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Si">Si</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trMercado1" runat ="server" >
                <td class="td1">Mercado Relevante
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlMercado" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Región
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlRegion" runat="server">
                        <asp:ListItem Value="">Seleccione</asp:ListItem>
                        <asp:ListItem Value="Costa">Costa</asp:ListItem>
                        <asp:ListItem Value="Interior">Interior</asp:ListItem>
                        <asp:ListItem Value="Zona aislada costa">Zona aislada costa</asp:ListItem>
                        <asp:ListItem Value="Zona aislada interior">Zona aislada interior</asp:ListItem>
                        <asp:ListItem Value="No Aplica">No Aplica</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20190621 rq039-19--%>
            <tr>
                <td class="td1">Longitud
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtLongitud" runat ="server"  MaxLength="15" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <%--20190621 rq039-19--%>
            <tr>
                <td class="td1">Latitud
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtLatitud" runat ="server"  MaxLength="15" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <%--20211119 control ptdvf mp--%>
            <tr>
                <td class="td1">Control PTDVF/CIDVF en contratos de mercado primario
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlPtdvfMp" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="2" align="center">
                    <asp:ImageButton ID="imbDetalle" runat="server" ImageUrl="~/Images/Actualizar.png"
                        OnClick="imbDetalle_Click" ValidationGroup="comi" Height="40" />
                    <%--20211119 control ptdvf--%>
                    <asp:ImageButton ID="imbSalirD" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalirD_Click"
                        CausesValidation="false" Height="40" />
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
            width="80%">
            <tr>
                <td colspan="2">
                    <div>
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                            OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                            HeaderStyle-CssClass="th1" PageSize="30">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <%--20160725 se cambio campo--%>
                                <asp:BoundColumn DataField="codigo_pto_snt" HeaderText="Codigo Pto SNT" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo_campo" HeaderText="tipo campo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_Campo" HeaderText="Campo" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <%--20160725 se elimino campo--%>
                                <asp:BoundColumn DataField="ind_trasferencia" HeaderText="pto transferencia" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_intermedio" HeaderText="pto intermedio" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_estandar" HeaderText="pto estandar" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ind_entrada" HeaderText="pto entrada" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <%--Campo nuevo Req. 007-17 Subasta Bimestral 20170209--%>
                                <asp:BoundColumn DataField="campo_declinacion" HeaderText="Campo Declinación" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <%--Campo nuevo Req. 079 Mapa Interactivo 20170314 --%>
                                <asp:BoundColumn DataField="ind_pub_bec_mapa" HeaderText="Publíca Bec Mapa Interactivo"
                                    ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                                <%--20190306 rq013-19--%>
                                <asp:BoundColumn DataField="codigo_departamento" HeaderText="Código Depto." ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Código Ciudad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_centro" HeaderText="Código Centro Poblado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_centro" HeaderText="Nombre Centro Poblado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo_poz" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc. Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="maneja_telemetria" HeaderText="Maneja Telemetría" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 fin rq013-19--%>
                                <%--20190404 rq018-19 fase II--%>
                                <asp:BoundColumn DataField="formacion_precio" HeaderText="Punto de Formación d precio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2019621 rq039-19 --%>
                                <asp:BoundColumn DataField="ind_importacion" HeaderText="Punto de Importación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--2019621 rq039-19 --%>
                                <asp:BoundColumn DataField="ind_regasificacion" HeaderText="Punto de Regasificación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                                <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                                <%--20190306 rq013-19--%>
                                <asp:EditCommandColumn HeaderText="Detalle" EditText="Detalle"></asp:EditCommandColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                            <HeaderStyle CssClass="th1"></HeaderStyle>
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>