﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosEnvioCartaSuper.aspx.cs"
    Inherits="BASE_frm_ParametrosEnvioCartaSuper" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br /><br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                No. Día hábil envío e_mail Superintendencias
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNoDiaHabilEnvio" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNoDiaHabilEnvio" runat="server" ErrorMessage="Debe Ingresar el No. Día hábil envío e_mail Superintendencias"
                    ControlToValidate="TxtNoDiaHabilEnvio" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Hora envío e_mail Superintendencias
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtHoraEnvioMail" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtHoraEnvioMail" runat="server" ErrorMessage="Debe Ingresar la Hora envío e_mail Superintendencias"
                    ControlToValidate="TxtHoraEnvioMail" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevTxtHoraEnvioMail" ControlToValidate="TxtHoraEnvioMail"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ErrorMessage="Formato Incorrecto para la Hora envío e_mail Superintendencias"
                    ValidationGroup="VsParametrosBna"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Texto Correo
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtTextoCorreo" runat="server" MaxLength="2000" Width="650px" TextMode="MultiLine"
                    Rows="3"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtTextoCorreo" runat="server" ErrorMessage="Debe Ingresar el Texto Correo"
                    ControlToValidate="TxtTextoCorreo" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Nombre Firma Carta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNombreFirmaCarta" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtNombreFirmaCarta" runat="server" ErrorMessage="Debe Ingresar el Nombre Firma Carta"
                    ControlToValidate="TxtNombreFirmaCarta" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">
                Cargo Firma Carta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCargoFirmaCarta" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtCargoFirmaCarta" runat="server" ErrorMessage="Debe Ingresar el Cargo Firma Carta"
                    ControlToValidate="TxtCargoFirmaCarta" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Archivo Firma (El tamaño del archivo debe ser de 200 Ancho x 90 de Alto)
            </td>
            <td class="td2" colspan="3">
                <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" Width="500px" />
                <asp:HyperLink ID="HplArchivo" runat="server" Target="_blank">Ver Archivo</asp:HyperLink>
                <asp:HiddenField ID="hdfNomArchivo" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="td1">
                Observación Cambio
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="200" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="VsParametrosBna" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="VsParametrosBna" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="VsParametrosBna" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    </asp:Content>
