﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ExcepcionCapRemanente : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Excepciones de capacidad Remanente subasta UVLP";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarTramo(lConexion.gObjConexion, ddlBusTramo);
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddbBusOperador, "m_operador", " estado = 'A'   order by razon_social", 0, 4);
            LlenarTramo(lConexion.gObjConexion, ddlTramo);
            LlenarTramo(lConexion.gObjConexion, ddlBusTramo);
            lConexion.Cerrar();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_excepcion_ce");
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[7].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[8].Visible = (Boolean)permisos["DELETE"];
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_tramo", "@P_codigo_operador" };
        SqlDbType[] lTipoparametros = {SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0" };

        try
        {
            if (ddlBusTramo.SelectedValue != "0")
                lValorParametros[0] = ddlBusTramo.SelectedValue;
            if (ddbBusOperador.SelectedValue != "0")
                lValorParametros[1] = ddbBusOperador.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetExcepcionRemanente", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_tramo","@P_codigo_operador", "@P_capacidad_real", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
        string[] lValorParametros = { "0", "0", "0", "2" };
        lblMensaje.Text = "";
        try
        {
            if (TxtCapacidad.Text == "")
                lblMensaje.Text += "Debe digitar la capacidad remanente real<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = ddlTramo.SelectedValue ;
                lValorParametros[1] = ddlOperador.SelectedValue;
                lValorParametros[2] = TxtCapacidad.Text;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetExcepcionRemanente", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de la capacidad remanente.!";
                    lConexion.Cerrar();
                }
                else
                {
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text == "P")
            {
                try
                {
                    ddlTramo.SelectedValue = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "El tramo del registro no existe o está inactivo<br>";
                }
                try
                {
                    ddlOperador.SelectedValue = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[2].Text;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "El operador del registro no existe o está inactivo<br>";
                }
                TxtCapacidad.Text = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[9].Text;
                if (lblMensaje.Text == "")
                    Modificar();
            }
            else
                lblMensaje.Text = "No se puede modificar la capacidad remanente porque ya fue aprobada<br>";
        }
        if (((LinkButton)e.CommandSource).Text == "Eliminar")
        {
            if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[10].Text == "P")
            {
                if (this.dtgMaestro.Items[e.Item.ItemIndex].Cells[5].Text == "S")
                {
                    string[] lsNombreParametros = { "@P_codigo_tramo", "@P_codigo_operador", "@P_accion" };
                    SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char };
                    string[] lValorParametros = { this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text, this.dtgMaestro.Items[e.Item.ItemIndex].Cells[2].Text, "3" };
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetExcepcionRemanente", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Text = "Se presentó un Problema en la Eliminación de la capacidad remanente.!";
                    }
                    CargarDatos();
                    lConexion.Cerrar();
                }
                else
                    lblMensaje.Text = "El registro no ha sido modificado.!";
            }
            else
                lblMensaje.Text = "No se puede eliminar la capacidad remanente porque ya fue aprobada<br>";
        }
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarTramo(SqlConnection lConn, DropDownList lDdl)
    {
        lDdl.Items.Clear();
        SqlDataReader lLector;
        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConn, "pa_GetTramo", null, null, null);
        if (lLector.HasRows)
        {
            while (lLector.Read())
            {

                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(0).ToString();
                lItem1.Text = lLector["desc_tramo"].ToString();
                lDdl.Items.Add(lItem1);
            }
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0" };
        string lsParametros = "";

        try
        {
            if (ddlBusTramo.SelectedValue != "0")
            {
                lValorParametros[0] = ddlBusTramo.SelectedValue;
                lsParametros += " Tramo: " + ddlBusTramo.SelectedItem;
            }
            if (ddbBusOperador.SelectedValue != "0")
            {
                lValorParametros[1] = ddbBusOperador.SelectedValue;
                lsParametros += " Operador: " + ddbBusOperador.SelectedItem;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetExcepcionRemanente&nombreParametros=@P_codigo_tramo*@P_codigo_operador&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_tramo*desc_tramo*codigo_operador*nombre_operador*capacidad_real*login_usuario*fecha_hora_actual&titulo_informe=Listado de excepciones de capacidad excedentaria&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " ano_declaracion = year(getdate())";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_capacidad_remanente&procedimiento=pa_ValidarExistencia&columnas=codigo_tramo*codigo_operador*capacidad_remanente*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceLlave).ToString() + "-" + lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }

}