﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ModalidadPeriodo : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Modalidad Contractual por Periodo de Entrega";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {   
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, DdlModalidad, "m_modalidad_contractual", " estado='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlPeriodo, "m_periodos_entrega", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusModalidad, "m_modalidad_contractual", " estado='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlBusPeriodo, "m_periodos_entrega", " estado='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, DdlCampo, "m_tipo_campo", " estado='A' order by descripcion", 0, 1);
            lConexion.Cerrar();

            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_campo_modalidad");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[14].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[15].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigo.Visible = false;
        LblCodigo.Visible = true;
        LblCodigo.Text = "Automático";
        DdlPeriodo.Enabled = true;
        DdlModalidad.Enabled = true;
        DdlMercado.Enabled = true;
        DdlProducto.Enabled = true;
        DdlCampo.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            DdlPeriodo.Enabled = false;
            DdlModalidad.Enabled = false;
            DdlMercado.Enabled = false;
            DdlProducto.Enabled = false;
            DdlCampo.Enabled = false;
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_modalidad_periodo", " codigo_modalidad_periodo = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigo.Text = lLector["codigo_modalidad_periodo"].ToString();
                        TxtCodigo.Text = lLector["codigo_modalidad_periodo"].ToString();
                        DdlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                        DdlCampo.SelectedValue = lLector["tipo_campo"].ToString();
                        DdlPeriodo.SelectedValue = lLector["codigo_periodo"].ToString();
                        DdlMercado.Text = lLector["tipo_mercado"].ToString();
                        DdlProducto.Text = lLector["destino_rueda"].ToString();
                        TxtFechaInicial.Text = Convert.ToDateTime(lLector["fecha_ini_negociacion"].ToString()).ToString("yyyy/MM/dd");
                        TxtFechaFinal.Text = Convert.ToDateTime(lLector["fecha_fin_negociacion"].ToString()).ToString("yyyy/MM/dd");
                        TxtFechaMaxFin.Text = Convert.ToDateTime(lLector["fecha_max_final"].ToString()).ToString("yyyy/MM/dd");
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigo.Visible = false;
                        LblCodigo.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado. Código modalidad-periodo " + modificar.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_modalidad_periodo", "@P_codigo_modalidad", "@P_codigo_periodo" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0" };

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
            lValorParametros[1] = DdlBusModalidad.SelectedValue;
            lValorParametros[2] = DdlBusPeriodo.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetModalidadPeriodo", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_modalidad_periodo", "@P_codigo_modalidad", "@P_codigo_periodo", "@P_tipo_campo", "@P_tipo_mercado", "@P_destino_rueda", "@P_fecha_ini_negociacion", "@P_fecha_fin_negociacion", "@P_fecha_max_final", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0","0", "0", "", "", "", "","", "", "1" };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now; ;
        DateTime ldFechaF = DateTime.Now; ;
        string[] lsFecha;

        try
        {
            if (DdlPeriodo.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el periodo de entrega<br>";
            if (DdlModalidad.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar la modalidad de contrato<br>";
            if (VerificarExistencia(" codigo_periodo= " + DdlPeriodo.SelectedValue + " and codigo_modalidad=" + DdlModalidad.SelectedValue + " and tipo_mercado='" + DdlMercado.SelectedValue + "' and destino_rueda='"+ DdlProducto.SelectedValue +"' and tipo_campo =" + DdlCampo.SelectedValue ))
                lblMensaje.Text += " Ya se ha asociado la modalidad de contrato  al periodo de entrega, tipo de mercado, producto y tipo de campo<br>";
            if (TxtFechaInicial.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaInicial.Text);
                    lsFecha = TxtFechaInicial.Text.Split('/');
                    if (lsFecha[0].Length != 4)
                        lblMensaje.Text += "Formato Inválido en la Fecha Inicial. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en la Fecha Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar la Fecha Inicial. <br>";

            if (TxtFechaFinal.Text.Trim().Length > 0)
            {
                try
                {

                    ldFechaF = Convert.ToDateTime(TxtFechaFinal.Text);
                    lsFecha = TxtFechaFinal.Text.Split('/');
                    if (lsFecha[0].Length != 4)
                        lblMensaje.Text += "Formato Inválido en la Fecha Final. <br>";
                    else
                    {
                        if (TxtFechaInicial.Text.Trim().Length > 0)
                        {
                            if (ldFechaI > ldFechaF)
                                lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en la Fecha Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar la Fecha Final. <br>";

            if (TxtFechaMaxFin.Text.Trim().Length > 0)
            {
                try
                {

                    ldFechaF = Convert.ToDateTime(TxtFechaMaxFin.Text);
                    lsFecha = TxtFechaMaxFin.Text.Split('/');
                    if (lsFecha[0].Length != 4)
                        lblMensaje.Text += "Formato Inválido en la Fecha máxima de entrega final. <br>";
                    else
                    {
                        if (TxtFechaInicial.Text.Trim().Length > 0)
                        {
                            if (ldFechaI > ldFechaF)
                                lblMensaje.Text += "La Fecha máxima de entrega Final NO puede ser Menor que la Fecha Inicial. <br>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en la Fecha máxima de entrega final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar la Fecha máxima de entrega final. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = DdlModalidad.SelectedValue;
                lValorParametros[2] = DdlPeriodo.SelectedValue;
                lValorParametros[3] = DdlCampo.SelectedValue;
                lValorParametros[4] = DdlMercado.SelectedValue;
                lValorParametros[5] = DdlProducto.SelectedValue;
                lValorParametros[6] = TxtFechaInicial.Text;
                lValorParametros[7] = TxtFechaFinal.Text;
                lValorParametros[8] = TxtFechaMaxFin.Text;
                lValorParametros[9] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModalidadPeriodo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del registro.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_modalidad_periodo", "@P_codigo_modalidad", "@P_codigo_periodo","@P_tipo_campo", "@P_tipo_mercado","@P_destino_rueda", "@P_fecha_ini_negociacion", "@P_fecha_fin_negociacion", "@P_fecha_max_final", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0","0", "", "", "", "","", "", "2" };
        lblMensaje.Text = "";
        DateTime ldFechaI = DateTime.Now; ;
        DateTime ldFechaF = DateTime.Now; ;
        string[] lsFecha;

        try
        {
            if (TxtFechaInicial.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaInicial.Text);
                    lsFecha = TxtFechaInicial.Text.Split('/');
                    if (lsFecha[0].Length != 4)
                        lblMensaje.Text += "Formato Inválido en la Fecha Inicial. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en la Fecha Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar la Fecha Inicial. <br>";

            if (TxtFechaFinal.Text.Trim().Length > 0)
            {
                try
                {

                    ldFechaF = Convert.ToDateTime(TxtFechaFinal.Text);
                    lsFecha = TxtFechaFinal.Text.Split('/');
                    if (lsFecha[0].Length != 4)
                        lblMensaje.Text += "Formato Inválido en la Fecha Final. <br>";
                    else
                    {
                        if (TxtFechaInicial.Text.Trim().Length > 0)
                        {
                            if (ldFechaI > ldFechaF)
                                lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha Inicial. <br>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en la Fecha Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar la Fecha Final. <br>";

            if (TxtFechaMaxFin.Text.Trim().Length > 0)
            {
                try
                {

                    ldFechaF = Convert.ToDateTime(TxtFechaMaxFin.Text);
                    lsFecha = TxtFechaMaxFin.Text.Split('/');
                    if (lsFecha[0].Length != 4)
                        lblMensaje.Text += "Formato Inválido en la Fecha máxima de entrega final. <br>";
                    else
                    {
                        if (TxtFechaInicial.Text.Trim().Length > 0)
                        {
                            if (ldFechaI > ldFechaF)
                                lblMensaje.Text += "La Fecha máxima de entrega Final NO puede ser Menor que la Fecha Inicial. <br>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en la Fecha máxima de entrega final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe ingresar la Fecha máxima de entrega final. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigo.Text;
                lValorParametros[1] = DdlModalidad.SelectedValue;
                lValorParametros[2] = DdlPeriodo.SelectedValue;
                lValorParametros[3] = DdlCampo.SelectedValue;
                lValorParametros[4] = DdlMercado.SelectedValue;
                lValorParametros[5] = DdlProducto.SelectedValue;
                lValorParametros[6] = TxtFechaInicial.Text;
                lValorParametros[7] = TxtFechaFinal.Text;
                lValorParametros[8] = TxtFechaMaxFin.Text;
                lValorParametros[9] = ddlEstado.SelectedValue;

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetModalidadPeriodo", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización del registro.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigo.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigo.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigo.Text != "")
            manejo_bloqueo("E", LblCodigo.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_modalidad_periodo", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='dbo.m_modalidad_periodo' and llave_registro='codigo_modalidad_periodo=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_modalidad_periodo=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_modalidad_periodo";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_modalidad_periodo", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigo.Text.Trim();
                lsParametros += " Codigo Modalidad-periodo: " + TxtBusCodigo.Text;

            }
            if (DdlBusModalidad.SelectedValue != "0")
            {
                lValorParametros[1] = DdlBusModalidad.SelectedValue;
                lsParametros += " - Modalidad: " + DdlBusModalidad.SelectedItem;
            }
            if (DdlBusPeriodo.SelectedValue != "0")
            {
                lValorParametros[2] = DdlBusPeriodo.SelectedValue;
                lsParametros += " - Periodo entrega: " + DdlBusPeriodo.SelectedItem;
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetModalidadPeriodo&nombreParametros=@P_codigo_modalidad_periodo*@P_codigo_modalidad*@P_codigo_periodo&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_modalidad_periodo*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de modalidad de contrato por periodo de entrega&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_modalidad_periodo <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_modalidad_periodo&procedimiento=pa_ValidarExistencia&columnas=codigo_modalidad_periodo*codigo_modalidad*codigo_periodo*tipo_mercado*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
}