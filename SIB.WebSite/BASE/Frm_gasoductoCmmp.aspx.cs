﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace BASE
{
    public partial class Frm_gasoductoCmmp : Page
    {
        private InfoSessionVO goInfo;
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        private clConexion lConexion;

        private SqlDataReader lLector;
        private SqlDataReader lLector1;
        private string gsTabla = "t_gasoducto_cmmp";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = "CMMP por Gasoducto de Conexión";

            lConexion = new clConexion(goInfo);

            //Titulo
            Master.Titulo = "Registros Operativos";
            buttons.CrearOnclick += btnNuevo;
            buttons.FiltrarOnclick += imbConsultar_Click;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;

            if (IsPostBack) return;
            /// Activacion de los Botones
            buttons.Inicializar(ruta: gsTabla);
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador ope", " estado = 'A' and exists (select 1 from m_operador_conexion con where ope.tipo_operador = con.tipo_operador and con.estado ='A' ) order by razon_social", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlBusOperador, "m_operador ope", " estado = 'A' and exists (select 1 from m_operador_conexion con where ope.tipo_operador = con.tipo_operador and con.estado ='A' ) order by razon_social", 0, 4);
            lConexion.Cerrar();

            if (goInfo.cod_comisionista != "0")
            {
                try
                {
                    ddlOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlOperador_SelectedIndexChanged(null, null);
                    ddlBusOperador.SelectedValue = goInfo.cod_comisionista;
                    ddlBusOperador_SelectedIndexChanged(null, null);
                    ddlOperador.Enabled = false;
                    ddlBusOperador.Enabled = false;
                }
                catch (Exception ex)
                {
                }
                if (ddlOperador.SelectedValue == "0")
                {
                    var botones = new[] { EnumBotones.Ninguno };
                    buttons.Inicializar(gsTabla, botones: botones);
                    foreach (DataGridItem Grilla in dtgMaestro.Items)
                    {
                        var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                        lkbModificar.Visible = false;
                        var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                        lkbEliminar.Visible = false;

                        if (!lkbModificar.Visible && !lkbEliminar.Visible)
                            dtgMaestro.Columns[9].Visible = false;
                    }
                }
            }
            Listar();
            //Establese los permisos del sistema
            //EstablecerPermisosSistema(); //20220621 ajsute
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "t_gasoducto_cmmp");
            //20220621 ajsute
            if ((bool)permisos["UPDATE"] && (bool)permisos["DELETE"])
                dtgMaestro.Columns[9].Visible = true;
            else
            {
                if (!(bool)permisos["UPDATE"] && !(bool)permisos["DELETE"])
                    dtgMaestro.Columns[9].Visible = false;
                else
                {
                    if (!(bool)permisos["UPDATE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                            lkbModificar.Visible = false;
                        }
                    }
                    if (!(bool)permisos["DELETE"])
                    {
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            var lkbEliminar = (LinkButton)Grilla.FindControl("lkbEliminar");
                            lkbEliminar.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            CargarDatos();
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_gasoducto_cmmp", " codigo_gasoducto_cmmp= " + modificar + " ");
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            LblCodigo.Text = lLector["codigo_gasoducto_cmmp"].ToString();
                            TxtCodigo.Text = lLector["codigo_gasoducto_cmmp"].ToString();
                            TxtFecha.Text = Convert.ToDateTime(lLector["fecha_vigencia"].ToString()).ToString("yyyy/MM/dd");
                            try
                            {
                                ddlOperador.SelectedValue = lLector["codigo_operador"].ToString();
                                ddlOperador_SelectedIndexChanged(null, null);
                            }
                            catch (Exception ex)
                            {
                                lblMensaje.Append("El operador del registro no existe o está inactivo<br>"); //20190822 rq054-19 //20200727
                            }
                            try
                            {
                                ddlGasoducto.SelectedValue = lLector["codigo_gasoducto"].ToString();
                            }
                            catch (Exception ex)
                            {
                                lblMensaje.Append("El gasoducto der conexión del registro no existe o está inactivo<br>"); //20190822 rq054-19 //20200727
                            }
                            TxtCmmp.Text = lLector["cantidad_cmmp"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtCodigo.Visible = false;
                            LblCodigo.Visible = true;
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. Código CMMP Gasoducto " + modificar.ToString());//20190822 rq054-19 //20200727

                    }
                }
                catch (Exception ex)
                {

                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            if (goInfo.cod_comisionista == "0" || ddlOperador.SelectedValue != "0")
            {
                string[] lsNombreParametros = { "@P_codigo_gasoducto_cmmp", "@P_codigo_operador", "@P_codigo_gasoducto" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { "0", "0", "0" };

                try
                {
                    if (TxtBusCodigo.Text.Trim().Length > 0)
                        lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    if (ddlBusOperador.SelectedValue != "0")
                        lValorParametros[1] = ddlBusOperador.SelectedValue;
                    if (ddlBusGasoducto.SelectedValue != "0" && ddlBusGasoducto.SelectedValue != "")
                        lValorParametros[2] = ddlBusGasoducto.SelectedValue;

                    dtgMaestro.CurrentPageIndex = 0; //20200727
                    lConexion.Abrir();
                    dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetGasoductoCmmp", lsNombreParametros, lTipoparametros, lValorParametros);
                    dtgMaestro.DataBind();
                    lConexion.Cerrar();
                    EstablecerPermisosSistema(); //20220621 ajsute
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, ex.Message);
                }
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_gasoducto_cmmp", "@P_fecha_vigencia", "@P_codigo_operador", "@P_codigo_gasoducto", "@P_cantidad_cmmp", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "0", "0", "0", "", "1" };
            var lblMensaje = new StringBuilder();

            try
            {
                if (TxtFecha.Text == "")
                    lblMensaje.Append("debe seleccionar la fecha de vigencia<br>");
                if (ddlGasoducto.SelectedValue == "0" || ddlGasoducto.SelectedValue == "")
                    lblMensaje.Append("debe seleccionar el gasoducto de conexión<br>");
                if (ddlOperador.SelectedValue == "0")
                    lblMensaje.Append("debe seleccionar el operador<br>");
                //20190822 rq054-19  //20200727
                if (string.IsNullOrEmpty(lblMensaje.ToString()))
                    if (VerificarExistencia(" fecha_vigencia='" + TxtFecha.Text + "' and codigo_operador = " + ddlOperador.SelectedValue + " and codigo_gasoducto= " + ddlGasoducto.SelectedValue)) //20190822 rq054-19  //20200727
                        lblMensaje.Append(" Ya está definida la CMMP para el gasoducto de conexión, el operador y la fecha de vigencia<br>"); //20190822 rq054-19  //20200727
                if (TxtCmmp.Text == "")
                    lblMensaje.Append("debe digitar la cantidad de CMMP<br>"); //20190822 rq054-19  //20200727
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlGasoducto.SelectedValue;
                    lValorParametros[4] = TxtCmmp.Text;
                    lValorParametros[5] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoCmmp", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación de la CMMP.!"); //20190822 rq054-19  //20200727
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Listar();
                        TxtFecha.Text = "";
                        ddlGasoducto.SelectedValue = "0";
                        TxtCmmp.Text = "";
                    }
                }
                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                {
                    Toastr.Error(this, lblMensaje.ToString());
                }
            }
            catch (Exception ex)
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_codigo_gasoducto_cmmp", "@P_fecha_vigencia", "@P_codigo_operador", "@P_codigo_gasoducto", "@P_cantidad_cmmp", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            string[] lValorParametros = { "0", "", "0", "0", "0", "", "2" };
            var lblMensaje = new StringBuilder();
            try
            {
                if (TxtCmmp.Text == "")
                    lblMensaje.Append("debe digitar la cantidad de CMMP<br>"); //20190802 rq054-19 //20200727
                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = LblCodigo.Text;
                    lValorParametros[1] = TxtFecha.Text;
                    lValorParametros[2] = ddlOperador.SelectedValue;
                    lValorParametros[3] = ddlGasoducto.SelectedValue;
                    lValorParametros[4] = TxtCmmp.Text;
                    lValorParametros[5] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoCmmp", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Actualización de la CMMP.!"); //20190802 rq054-19 //20200727
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", LblCodigo.Text);
                        Listar();
                    }
                }
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", LblCodigo.Text);
                lblMensaje.Append(ex.Message);
            }
            if (lblMensaje.ToString() != "")
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (LblCodigo.Text != "")
                manejo_bloqueo("E", LblCodigo.Text);
            Listar();
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
            Modal.Cerrar(this, CrearRegistro.ID);
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            string lCodigoRegistro;
            var lblMensaje = new StringBuilder();
            if (e.CommandName.Equals("Modificar"))
            {
                mdlCrearRegistroLabel.InnerText = "Modificar";
                lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                TxtFecha.Enabled = false;
                ddlGasoducto.Enabled = false;
                ddlOperador.Enabled = false;
                Modificar(lCodigoRegistro);
            }
            if (e.CommandName.Equals("Eliminar"))
            {
                lCodigoRegistro = dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
                string[] lsNombreParametros = { "@P_codigo_gasoducto_cmmp", "@P_accion" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
                string[] lValorParametros = { "0", "3" };
                try
                {
                    lValorParametros[0] = lCodigoRegistro;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetGasoductoCmmp", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Eliminación de la CMMP.!"); //20190822 rq054-19 //20200727
                        lConexion.Cerrar();
                    }
                    else
                    {
                        lConexion.Cerrar();
                        Listar();
                    }
                }
                catch (Exception ex)
                {
                    /// Desbloquea el Registro Actualizado
                    manejo_bloqueo("E", LblCodigo.Text);
                    Toastr.Error(this, ex.Message);
                }
            }

            if (!string.IsNullOrEmpty(lblMensaje.ToString()))
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia("t_gasoducto_cmmp", lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
        {
            string lsCondicion = "nombre_tabla='t_gasoducto_cmmp' and llave_registro='codigo_gasoducto_cmmp=" + lscodigo_registro + "'";
            string lsCondicion1 = "codigo_gasoducto_cmmp=" + lscodigo_registro.ToString();
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = "t_gasoducto_cmmp";
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "t_gasoducto_cmmp", lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0", "0" };
            string lsParametros = "";

            try
            {
                if (TxtBusCodigo.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusCodigo.Text.Trim();
                    lsParametros += " Código Gasoducto CMMP: " + TxtBusCodigo.Text; //20190822 rq054-19 //20200727
                }
                if (ddlBusOperador.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusOperador.SelectedValue;
                    lsParametros += " Operador: " + ddlBusOperador.SelectedItem;
                }
                if (ddlBusGasoducto.SelectedValue != "0" && ddlBusGasoducto.SelectedValue != "")
                {
                    lValorParametros[2] = ddlBusGasoducto.SelectedValue;
                    lsParametros += " Gasoducto de conexión: " + ddlBusGasoducto.SelectedItem;
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetGasoductoCmmp&nombreParametros=@P_codigo_gasoducto_cmmp*@P_codigo_operador*@P_codigo_gasoducto&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_gasoducto_cmmp*codigo_operador*nombre_operador*codigo_gasoducto*desc_gasoducto*cantidad_cmmp*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de CMMP por gasoducto de conexión&TituloParametros=" + lsParametros);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pud0 Generar el Informe.!");//20190822 rq054-19 //20200727
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " codigo_gasoducto_cmmp <> '0'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=t_gasoducto_cmmp&procedimiento=pa_ValidarExistencia&columnas=codigo_gasoducto_cmmp*fecha_vigencia*codigo_operador*codigo_gasoducto*cantidad_cmmp*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");//20190822 rq054-19 //20200727
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString(); //20190822 rq054-19 //20200727
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles1(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            //20190822 rq054-19
            ddlGasoducto.Items.Clear();
            lConexion.Abrir();
            LlenarControles1(lConexion.gObjConexion, ddlGasoducto, "m_gasoducto_conexion gas, m_gasoducto_conexion_ope ope", "  estado = 'A' and gas.codigo_gasoducto = ope.codigo_gasoducto and ope.codigo_operador ='" + ddlOperador.SelectedValue + "'  order by descripcion", 0, 1); //20190822 rq054-19
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlBusOperador_SelectedIndexChanged(object sender, EventArgs e)
        {
            lConexion.Abrir();
            ddlBusGasoducto.Items.Clear();
            LlenarControles1(lConexion.gObjConexion, ddlBusGasoducto, "m_gasoducto_conexion gas, m_gasoducto_conexion_ope ope", "  estado = 'A' and gas.codigo_gasoducto = ope.codigo_gasoducto and ope.codigo_operador ='" + ddlBusOperador.SelectedValue + "'  order by descripcion", 0, 1); //20190822 rq054-19
            lConexion.Cerrar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }
        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            mdlCrearRegistroLabel.InnerText = "Agregar";
            LblCodigo.Text = "Automatico";
            TxtFecha.Text = string.Empty;
            ddlGasoducto.SelectedIndex = 0;
            TxtCmmp.Text = string.Empty;
            ddlEstado.SelectedIndex = 0;

            LblCodigo.Visible = true;
            imbCrear.Visible = true;
            TxtCodigo.Visible = false;
            imbActualiza.Visible = false;
            TxtFecha.Enabled = true;
            ddlGasoducto.Enabled = true;

            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListar(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Metodo del Link Consultar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Buscar();
        }
    }
}