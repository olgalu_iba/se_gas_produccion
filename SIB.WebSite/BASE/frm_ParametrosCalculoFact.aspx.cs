﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParametrosCalculoFact : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Cálculo Facturación"; //20170126 rq122 ajuste facturacion y gar s
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_parametros_calculo_fact");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[7].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[8].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoParametro.Visible = false;
        LblCodigoParametro.Visible = true;
        LblCodigoParametro.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_calculo_fact", " codigo_parametro_fac = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoParametro.Text = lLector["codigo_parametro_fac"].ToString();
                        TxtCodigoParametro.Text = lLector["codigo_parametro_fac"].ToString();
                        TxtFechaVigencia.Text = lLector["fecha_inicial_vigencia"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicial_vigencia"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicial_vigencia"].ToString().Substring(0, 2);
                        TxtPorcIncremento.Text = lLector["porc_incremento_anual"].ToString();
                        TxtValIngGestor.Text = lLector["valor_ingreso_gestor"].ToString();
                        TxtValComExito.Text = lLector["valor_comision_exito"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoParametro.Visible = false;
                        LblCodigoParametro.Visible = true;
                        TxtFechaVigencia.Enabled = false;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Código Parámetro " + modificar.ToString(); //20170126 rq122 ajuste facturacion y gar 

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_codigo_parametro_fac", "@P_fecha_inicial_vigencia" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "" };
        DateTime ldFecha;
        try
        {
            if (TxtBusCodigoParametro.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigoParametro.Text.Trim();
            if (TxtBusFechaVigencia.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaVigencia.Text.Trim());
                    lValorParametros[1] = TxtBusFechaVigencia.Text.Trim();
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Valor Inválido en Fecha de Vigencia.!"; //20170126 rq122 ajuste facturacion y gar 

                }
            }
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetParametroCalFac", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_parametro_fac", "@P_fecha_inicial_vigencia","@P_valor_ingreso_gestor","@P_porc_incremento_anual",
                                        "@P_valor_comision_exito","@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "0", "0", "", "1" };
        lblMensaje.Text = "";
        DateTime ldFecha;
        decimal ldValor;
        try
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaVigencia.Text.Trim());
                if (VerificarExistencia(" convert(varchar(10),fecha_inicial_vigencia,111) = '" + TxtFechaVigencia.Text.Trim() + "'"))
                    lblMensaje.Text += " La Fecha de Vigencia YA existe " + TxtFechaVigencia.Text + " <br>";
                else
                {
                    if (VerificarExistencia(" convert(varchar(10),fecha_inicial_vigencia,111) > '" + TxtFechaVigencia.Text.Trim() + "'"))
                        lblMensaje.Text += " La Fecha de Vigencia NO Puede ser Menor a una YA existente " + TxtFechaVigencia.Text + " <br>";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Fecha Inicial de Vigencia <br>";
            }
            /// Valor Ingreso Gestor
            try
            {
                ldValor = Convert.ToDecimal(TxtValIngGestor.Text.Trim());
                if (ldValor <= 0)
                    lblMensaje.Text += "Valor Inválido en Campo Valor Ingreso Gestor <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Valor Ingreso Gestor <br>";

            }
            /// % Incremento
            try
            {
                ldValor = Convert.ToDecimal(TxtPorcIncremento.Text.Trim());
                if (ldValor < 0)
                    lblMensaje.Text += "Valor Inválido en Campo  % Incremento Anual <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo  % Incremento Anual <br>";

            }
            /// Valor Comision Exito
            try
            {
                ldValor = Convert.ToDecimal(TxtValComExito.Text.Trim());
                if (ldValor < 0)
                    lblMensaje.Text += "Valor Inválido en Campo Valor Comisión Éxito <br>"; //20170126 rq122 ajuste facturacion y gar 
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Valor Comisión Ëxito <br>"; //20170126 rq122 ajuste facturacion y gar 

            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = TxtFechaVigencia.Text.Trim();
                lValorParametros[2] = TxtValIngGestor.Text.Trim();
                lValorParametros[3] = TxtPorcIncremento.Text.Trim();
                lValorParametros[4] = TxtValComExito.Text.Trim();
                lValorParametros[5] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametroCalFac", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del Parámetro.! " + goInfo.mensaje_error.ToString(); //20170126 rq122 ajuste facturacion y gar 
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_parametro_fac", "@P_fecha_inicial_vigencia","@P_valor_ingreso_gestor","@P_porc_incremento_anual",
                                        "@P_valor_comision_exito","@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "", "0", "0", "0", "", "2" };
        lblMensaje.Text = "";
        DateTime ldFecha;
        decimal ldValor;
        try
        {
            /// Valor Ingreso Gestor
            try
            {
                ldValor = Convert.ToDecimal(TxtValIngGestor.Text.Trim());
                if (ldValor <= 0)
                    lblMensaje.Text += "Valor Inválido en Campo Valor Ingreso Gestor <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Valor Ingreso Gestor <br>";

            }
            /// % Incremento
            try
            {
                ldValor = Convert.ToDecimal(TxtPorcIncremento.Text.Trim());
                if (ldValor < 0)
                    lblMensaje.Text += "Valor Inválido en Campo  % Incremento Anual <br>";
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo  % Incremento Anual <br>";

            }
            /// Valor Comision Exito
            try
            {
                ldValor = Convert.ToDecimal(TxtValComExito.Text.Trim());
                if (ldValor < 0)
                    lblMensaje.Text += "Valor Inválido en Campo Valor Comisión Éxito <br>"; //20170126 rq122 ajuste facturacion y gar 
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Valor Comisión Éxito <br>"; //20170126 rq122 ajuste facturacion y gar 

            }
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoParametro.Text;
                lValorParametros[1] = TxtFechaVigencia.Text.Trim();
                lValorParametros[2] = TxtValIngGestor.Text.Trim();
                lValorParametros[3] = TxtPorcIncremento.Text.Trim();
                lValorParametros[4] = TxtValComExito.Text.Trim();
                lValorParametros[5] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametroCalFac", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización del Parámetro.! " + goInfo.mensaje_error.ToString(); //20170126 rq122 ajuste facturacion y gar 
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoParametro.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoParametro.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoParametro.Text != "")
            manejo_bloqueo("E", LblCodigoParametro.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_parametros_calculo_fact", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistenciaSib", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='dbo.m_parametros_calculo_fact' and llave_registro='codigo_parametro_fac=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_parametro_fac=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_parametros_calculo_fact";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_parametros_calculo_fact", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        string[] lValorParametros = { "0", "" };
        string lsParametros = "";
        DateTime ldFecha;

        try
        {
            if (TxtBusCodigoParametro.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigoParametro.Text.Trim();
                lsParametros += " Código Pozo : " + TxtBusCodigoParametro.Text; //20170126 rq122 ajuste facturacion y gar 

            }
            if (TxtBusFechaVigencia.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtBusFechaVigencia.Text.Trim());
                    lValorParametros[1] = TxtBusFechaVigencia.Text.Trim();
                    lValorParametros[1] = TxtBusFechaVigencia.Text.Trim();
                    lsParametros += " - Fecha Vigencia: " + TxtBusFechaVigencia.Text;
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "Valor Inválido en Fecha de Vigencia.!"; //20170126 rq122 ajuste facturacion y gar 

                }
            }
            if (lblMensaje.Text == "")
            {
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetParametroCalFac&nombreParametros=@P_codigo_parametro_fac*@P_fecha_inicial_vigencia&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_pozo*descripcion*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado Parametros Calculo Facturacion&TituloParametros=" + lsParametros);
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!"; //20170126 rq122 ajuste facturacion y gar 
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_parametro_fac <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_parametros_calculo_fact&procedimiento=pa_ValidarExistencia&columnas=codigo_parametro_fac*fecha_inicial_vigencia*valor_ingreso_gestor*porc_incremento_anual*valor_comision_exito*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!"; //20170126 rq122 ajuste facturacion y gar 
        }

    }
}