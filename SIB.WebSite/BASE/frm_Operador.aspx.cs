﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;
using System.Text;
using System.Text.RegularExpressions; //20180228 rq032-17

public partial class BASE_frm_Operador : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Operador";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    SqlDataReader lLector;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipo, "m_tipos_operador", " estado = 'A' order by descripcion", 2, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoOperador, "m_tipos_operador", " estado = 'A' order by descripcion", 2, 1);
            LlenarControles(lConexion.gObjConexion, ddlDepartamento, "m_divipola", " estado = 'A' and codigo_ciudad ='0' order by nombre_ciudad", 1, 2); //20180228 rq032-17
            lConexion.Cerrar();
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_operador");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgOperador.Columns[7].Visible = (Boolean)permisos["UPDATE"];
        dtgOperador.Columns[8].Visible = (Boolean)permisos["DELETE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        //20190411 rq019-19
        lblPolitica.Text = "NO autorizado";
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoOperador.Visible = false;
        LblCodigoOperador.Visible = true;
        LblCodigoOperador.Text = "Automático";  //20170530 divipola
        TxtNoDocumento.Enabled = true;
        TxtDigitoVerificacion.Enabled = true;
        //CddlPais.SelectedValue = "169"; //20170530 divipola
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador", " codigo_operador = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoOperador.Text = lLector["codigo_operador"].ToString();
                        TxtCodigoOperador.Text = lLector["codigo_operador"].ToString();
                        ddlCodigoTipoDocumento.SelectedValue = lLector["codigo_tipo_doc"].ToString();
                        ddlTipoPersona_SelectedIndexChanged(null, null);
                        TxtNoDocumento.Text = lLector["no_documento"].ToString();
                        TxtDigitoVerificacion.Text = lLector["digito_verif"].ToString();
                        TxtRazonSocial.Text = lLector["razon_social"].ToString();
                        try
                        {
                            ddlTipo.SelectedValue = lLector["tipo_operador"].ToString();
                            ddlTipo_SelectedIndexChanged(null, null); //20180228 rq032-17
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tipo operador del registro no existe o está inactivo<br>"; //20170530 divipola
                        }
                        ddlAutoretenedor.SelectedValue = lLector["autoretenedor"].ToString();
                        ddlRegimen.SelectedValue = lLector["regimen"].ToString();
                        ddlContribuyente.SelectedValue = lLector["contribuyente"].ToString();
                        ddlGranContribuyente.SelectedValue = lLector["gran_contribuyente"].ToString();
                        TxtNombreRepresentante.Text = lLector["nombre_representante"].ToString();
                        TxtEmail.Text = lLector["e_mail"].ToString();
                        TxtEmail2.Text = lLector["e_mail2"].ToString();  //20180228 rq032-17
                        TxtEmail3.Text = lLector["e_mail3"].ToString(); //20180228 rq032-17
                        TxtDireccion.Text = lLector["direccion"].ToString();
                        TxtTelefono.Text = lLector["telefono"].ToString();
                        TxtFax.Text = lLector["fax"].ToString();
                        TxtCodigoActividad.Text = lLector["codigo_actividad"].ToString();
                        TxtBanco.Text = lLector["banco_pago"].ToString(); //20201207
                        //20201207
                        try
                        {
                            ddlTipoCta.SelectedValue = lLector["tipo_cuenta"].ToString();
                        }
                        catch (Exception ex)
                        {
                            ddlTipoCta.SelectedValue = "";
                        }
                        TxtCuenta.Text = lLector["cuenta_pago"].ToString(); //20201207
                        ddlEstado.SelectedValue = lLector["estado"].ToString();

                        ddlTipoPersona.SelectedValue = lLector["tipo_persona"].ToString();
                        if (ddlTipoPersona.SelectedValue == "N")
                        {
                            TxtNombre.Text = lLector["nombres"].ToString();
                            TxtApellido.Text = lLector["apellidos"].ToString();
                            TrJuridico.Visible = false;
                            TrNatural.Visible = true;
                        }
                        else
                        {
                            TrJuridico.Visible = true;
                            TrNatural.Visible = false;
                        }
                        ddlPosicion.SelectedValue = lLector["posicion"].ToString();
                        TxtCelular.Text = lLector["no_celular"].ToString();
                        //CddlPais.SelectedValue = lLector["codigo_pais_res"].ToString(); 20170520 divipola
                        ddlDepartamento.SelectedValue = lLector["codigo_departamento_res"].ToString();
                        ddlDepartamento_SelectedIndexChanged(null, null); //20180228 rq032-17
                        try
                        {
                            ddlCiudad.SelectedValue = lLector["codigo_ciudad_res"].ToString();
                        }
                        catch (Exception ex)
                        { }
                        ddlServPublico.SelectedValue = lLector["presta_serv_publicos"].ToString();
                        ddlServPublico_SelectedIndexChanged(null, null);
                        TxtNoRegRups.Text = lLector["no_registro_rups"].ToString();
                        if (lLector["fecha_elaboracion_reg"].ToString().Trim().Length > 10 && lLector["fecha_elaboracion_reg"].ToString().Substring(6, 4) != "1900") //20180228 rq032-17
                            TxtFechaRegRups.Text = lLector["fecha_elaboracion_reg"].ToString().Substring(6, 4) + "/" + lLector["fecha_elaboracion_reg"].ToString().Substring(3, 2) + "/" + lLector["fecha_elaboracion_reg"].ToString().Substring(0, 2);
                        else
                            TxtFechaRegRups.Text = "";
                        if (lLector["fecha_inicia_actividad"].ToString().Trim().Length > 10 && lLector["fecha_inicia_actividad"].ToString().Substring(6, 4) != "1900") //20180228 rq032-17
                            TxtFechaIniAct.Text = lLector["fecha_inicia_actividad"].ToString().Substring(6, 4) + "/" + lLector["fecha_inicia_actividad"].ToString().Substring(3, 2) + "/" + lLector["fecha_inicia_actividad"].ToString().Substring(0, 2);
                        else
                            TxtFechaIniAct.Text = "";
                        TxtDireccionWeb.Text = lLector["direccion_web"].ToString();
                        hdfEstadoAct.Value = ddlEstado.SelectedValue;
                        //20190411 rq019-19
                        if (lLector["acepto_politica"].ToString() == "S")
                            lblPolitica.Text = "SI autorizado";
                        else
                            lblPolitica.Text = "NO autorizado";

                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoOperador.Visible = false;
                        LblCodigoOperador.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado. Código Operador " + modificar.ToString(); //20170530 divipola
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_operador", "@P_nit", "@P_nombre", "@P_tipo" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "", "" };

        try
        {
            if (TxtBusNit.Text.Trim().Length > 0)
                lValorParametros[1] = TxtBusNit.Text.Trim();
            if (TxtBusRazonSocial.Text.Trim().Length > 0)
                lValorParametros[2] = TxtBusRazonSocial.Text.Trim();
            if (ddlBusTipoOperador.SelectedValue != "0")
                lValorParametros[3] = ddlBusTipoOperador.SelectedValue;

            lConexion.Abrir();
            dtgOperador.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetOperador", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgOperador.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = {"@P_codigo_operador", "@P_codigo_tipo_doc", "@P_no_documento", "@P_digito_verif", "@P_razon_social", "@P_tipo_operador",
                                        "@P_autoretenedor","@P_regimen","@P_contribuyente","@P_gran_contribuyente","@P_nombre_representante",
                                        "@P_e_mail","@P_direccion","@P_telefono","@P_fax","@P_codigo_actividad","@P_estado","@P_accion",
                                        "@P_tipo_persona","@P_nombres","@P_apellidos","@P_celular",//"@P_pais", 20170530 divipola
                                        "@P_departamento","@P_ciudad","@P_posicion",
                                        "@P_presta_serv_pub","@P_no_reg_rups","@P_fecha_reg_rups","@P_fecha_ini_activ","@P_direccion_web",
                                        "@P_e_mail2", "@P_e_mail3", //20180228 rq032-18
                                        "@P_banco_pago","@P_tipo_cuenta","@P_cuenta_pago" //20201207
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar , SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//SqlDbType.Int, 20170530 divipola
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,  //20180802 ajuste
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar, //20180228 rq032-18
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar}; //20201207
        string[] lValorParametros = { "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "1", "", "", "", "", "0", "0", "", "N", "", "", "", "", "", "", "","","" }; //20170530 divipola //20180228 rq032-18 //20201207
        lblMensaje.Text = "";
        DateTime ldFecha;

        try
        {
            if (ddlTipo.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Operador <br>";
            if (VerificarExistencia(" no_documento = '" + this.TxtNoDocumento.Text + "'"))
                lblMensaje.Text += " El Nit del Operador YA existe " + TxtNoDocumento.Text + " <br>";
            if (ddlTipoPersona.SelectedValue == "J")
            {
                if (this.TxtNoDocumento.Text != "" && this.TxtDigitoVerificacion.Text.Trim().Length != 0)
                {
                    int liDigitoVerificacion = DelegadaBase.Servicios.ValidarDigitoVerificacion(Convert.ToInt64(this.TxtNoDocumento.Text));
                    if (liDigitoVerificacion != Convert.ToInt32(TxtDigitoVerificacion.Text))
                        lblMensaje.Text += " El Dígito de Verificación no corresponde al Número del Documento. Debe ser (" + liDigitoVerificacion + ")  <br>"; //20170530 divipola
                }
                if (TxtRazonSocial.Text.Trim() == "")
                    lblMensaje.Text += " Debe Ingresar la Razón Social del Operador <br>";//20170530 divipola
            }
            else
            {
                if (TxtNombre.Text.Trim() == "")
                    lblMensaje.Text += " Debe Ingresar los Nombres del Operador <br>";
                if (TxtApellido.Text.Trim() == "")
                    lblMensaje.Text += " Debe Ingresar los Apellidos del Operador <br>";
                if (TxtDigitoVerificacion.Text.Trim() != "")
                    lblMensaje.Text += " No Debe Ingresar Dígito de Verificación para una Persona Natural <br>"; //20170530 divipola
            }
            if (ddlTipoPersona.SelectedValue.ToString() == "J" && ddlCodigoTipoDocumento.SelectedValue.ToString() != "1")
                lblMensaje.Text += " Tipo de documento no válido para Persona Jurídica (" + ddlCodigoTipoDocumento.SelectedValue.ToString() + ")  <br>"; //20170530 divipola
            if (ddlTipoPersona.SelectedValue.ToString() == "J" && TxtDigitoVerificacion.Text.Length == 0)
                lblMensaje.Text += " Debe ingresar el Dígito de Verificación para Persona Jurídica  <br>"; //20170530 divipola
            if (TxtNombreRepresentante.Text == "")
                lblMensaje.Text += " Debe ingresar el nombre del representante <br>";
            if (TxtEmail.Text == "")
                lblMensaje.Text += " Debe ingresar el E-mail de contacto<br>";
            if (TxtTelefono.Text == "")
                lblMensaje.Text += " Debe ingresar el teléfono  de contacto<br>"; //20170530 divipola
            else  //20180228 rq032-17
            {
                if (validaTel(TxtTelefono.Text) == "S")
                    lblMensaje.Text += " El teléfono de contacto no es válido <br>";
            }
            //20180228 rq032-17
            if (TxtFax.Text != "")
            {
                if (validaTel(TxtFax.Text) == "S")
                    lblMensaje.Text += " El teléfono 2 de contacto no es válido <br>";
            }
            if (TxtCodigoActividad.Text == "")
                lblMensaje.Text += " Debe ingresar el código de la actividad económica<br>";
            //20170530 divipola
            //if (ddlPais.SelectedValue == "" || ddlDepartamento.SelectedValue == "" || ddlCiudad.SelectedValue == "")
            //    lblMensaje.Text += " Debe Seleccionar la ciudad de ubicación <br>";
            if (ddlServPublico.SelectedValue == "S")
            {
                if (TxtNoRegRups.Text.Trim().Length <= 0)
                    lblMensaje.Text += " Debe ingresar el No. de Registro RUPS<br>";
                if (TxtFechaRegRups.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaRegRups.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += " Valor Inválido en Fecha de Registro del RUPS<br>";
                    }

                }
                else
                    lblMensaje.Text += " Debe ingresar la Fecha de Registro del RUPS<br>";

                if (TxtFechaIniAct.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaIniAct.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += " Valor Inválido en Fecha de Inicio Actividad<br>";
                    }

                }
                else
                    lblMensaje.Text += " Debe ingresar la Fecha de Inicio Actividad<br>";

            }
            if (ddlEstado.SelectedValue == "A")
                lblMensaje.Text += " No se puede Crear Un Operador en estado Activo, se debe crear inactivo y Luego realizar la Aprobación. <br>"; //20170530 divipola

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = ddlCodigoTipoDocumento.SelectedValue;
                lValorParametros[2] = TxtNoDocumento.Text.Trim();
                lValorParametros[3] = TxtDigitoVerificacion.Text.Trim();
                if (ddlCodigoTipoDocumento.SelectedValue == "1")
                    lValorParametros[4] = TxtRazonSocial.Text.Trim();
                else
                    lValorParametros[4] = TxtNombre.Text.Trim() + " " + TxtApellido.Text.Trim();
                lValorParametros[5] = ddlTipo.SelectedValue;
                lValorParametros[6] = ddlAutoretenedor.SelectedValue;
                lValorParametros[7] = ddlRegimen.SelectedValue;
                lValorParametros[8] = ddlContribuyente.SelectedValue;
                lValorParametros[9] = ddlGranContribuyente.SelectedValue;
                lValorParametros[10] = TxtNombreRepresentante.Text.Trim();
                lValorParametros[11] = TxtEmail.Text.Trim();
                lValorParametros[12] = TxtDireccion.Text.Trim();
                lValorParametros[13] = TxtTelefono.Text.Trim();
                lValorParametros[14] = TxtFax.Text.Trim();
                lValorParametros[15] = TxtCodigoActividad.Text.Trim();
                lValorParametros[16] = ddlEstado.SelectedValue;

                lValorParametros[18] = ddlTipoPersona.SelectedValue;
                lValorParametros[19] = TxtNombre.Text.Trim();
                lValorParametros[20] = TxtApellido.Text.Trim();
                lValorParametros[21] = TxtCelular.Text.Trim();
                //20170530 divipola
                //cambio de indices para eliminar ciudad
                //lValorParametros[22] = ddlPais.SelectedValue;
                lValorParametros[22] = ddlDepartamento.SelectedValue;
                lValorParametros[23] = ddlCiudad.SelectedValue;
                lValorParametros[24] = ddlPosicion.SelectedValue;
                if (ddlServPublico.SelectedValue == "S")
                {
                    lValorParametros[25] = ddlServPublico.SelectedValue;
                    lValorParametros[26] = TxtNoRegRups.Text.Trim();
                    lValorParametros[27] = TxtFechaRegRups.Text.Trim();
                    lValorParametros[28] = TxtFechaIniAct.Text.Trim();
                }
                lValorParametros[29] = TxtDireccionWeb.Text.Trim();
                lValorParametros[30] = TxtEmail2.Text.Trim(); //20180228 rq032-17
                lValorParametros[31] = TxtEmail3.Text.Trim(); //20180228 rq032-17
                lValorParametros[32] = TxtBanco.Text.Trim(); //20201207
                lValorParametros[33] = ddlTipoCta.SelectedValue;  //20201207
                lValorParametros[34] = TxtCuenta.Text.Trim(); //20201207
                //fin camboio de indices

                lConexion.Abrir();
                goInfo.mensaje_error = "";
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetOperador", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del  Operador.! " + goInfo.mensaje_error.ToString(); //20170530 divipola
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = {"@P_codigo_operador", "@P_codigo_tipo_doc", "@P_no_documento", "@P_digito_verif", "@P_razon_social", "@P_tipo_operador",
                                        "@P_autoretenedor","@P_regimen","@P_contribuyente","@P_gran_contribuyente","@P_nombre_representante",
                                        "@P_e_mail","@P_direccion","@P_telefono","@P_fax","@P_codigo_actividad","@P_estado","@P_accion",
                                        "@P_tipo_persona","@P_nombres","@P_apellidos","@P_celular",//"@P_pais", 20170530 divipola
                                        "@P_departamento","@P_ciudad","@P_posicion",
                                        "@P_presta_serv_pub","@P_no_reg_rups","@P_fecha_reg_rups","@P_fecha_ini_activ","@P_direccion_web",
                                        "@P_e_mail2", "@P_e_mail3",  //20180228 rq032-17 
                                        "@P_banco_pago","@P_tipo_cuenta","@P_cuenta_pago" //20201207
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar , SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, SqlDbType.Int,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//SqlDbType.Int,  20170530 divipola
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar, //20180802 ajustes
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar,SqlDbType.VarChar,//20180228 rq032-17
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar//20101207
                                      };
        string[] lValorParametros = { "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "2", "", "", "", "", "0", "0", "", "N", "", "", "", "", "", "","","","" }; //20170530 divipola //20180228 rq032-17
        lblMensaje.Text = "";
        DateTime ldFecha;
        try
        {
            /// Se comentareo la validacion del No. del documento, para permitir modificacion de los datos del Operador, en los casos de los operadores
            /// tienen dos operadores como produtor y comercializador. 2015-03-27
            //if (VerificarExistencia(" no_documento = '" + this.TxtNoDocumento.Text + "' And codigo_operador != " + LblCodigoOperador.Text))
            //    lblMensaje.Text += " El No. del Documento del Operador YA existe " + TxtNoDocumento.Text + " <br>";
            if (ddlTipoPersona.SelectedValue == "J")
            {
                if (this.TxtNoDocumento.Text != "" && this.TxtDigitoVerificacion.Text.Trim().Length != 0)
                {
                    int liDigitoVerificacion = DelegadaBase.Servicios.ValidarDigitoVerificacion(Convert.ToInt64(this.TxtNoDocumento.Text));
                    if (liDigitoVerificacion != Convert.ToInt32(TxtDigitoVerificacion.Text))
                        lblMensaje.Text += " El Dígito de Verificación no corresponde al Número del Documento. Debe ser (" + liDigitoVerificacion + ")  <br>"; //20170530 divipola
                }
                if (TxtRazonSocial.Text.Trim() == "")
                    lblMensaje.Text += " Debe Ingresar la Razón Social del Operador <br>"; //20170530 divipola
            }
            else
            {
                if (TxtNombre.Text.Trim() == "")
                    lblMensaje.Text += " Debe Ingresar los Nombres del Operador <br>";
                if (TxtApellido.Text.Trim() == "")
                    lblMensaje.Text += " Debe Ingresar los Apellidos del Operador <br>";
                if (TxtDigitoVerificacion.Text.Trim() != "")
                    lblMensaje.Text += " No Debe Ingresar Dígito de Verificación para una Persona Natural <br>"; //20170530 divipola
            }
            if (ddlTipo.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Operador <br>";
            if (ddlTipoPersona.SelectedValue.ToString() == "J" && ddlCodigoTipoDocumento.SelectedValue.ToString() != "1")
                lblMensaje.Text += " Tipo de documento no válido para Persona Jurídica " + ddlCodigoTipoDocumento.SelectedValue.ToString() + ")  <br>"; //20170530 divipola
            if (ddlTipoPersona.SelectedValue.ToString() == "J" && TxtDigitoVerificacion.Text.Length == 0)
                lblMensaje.Text += " Debe ingresar el Dígito de Verificación para Persona Jurídica  <br>"; //20170530 divipola
            if (TxtNombreRepresentante.Text == "")
                lblMensaje.Text += " Debe ingresar el nombre del representante <br>";
            if (TxtEmail.Text == "")
                lblMensaje.Text += " Debe ingresar el E-mail de contacto<br>";
            if (TxtTelefono.Text == "")
                lblMensaje.Text += " Debe ingresar el teléfono  de contacto<br>"; //20170530 divipola
            else  //20180228 rq032-17
            {
                if (validaTel(TxtTelefono.Text) == "S")
                    lblMensaje.Text += " El teléfono de contacto no es válido <br>";
            }
            //20180228 rq032-17
            if (TxtFax.Text != "")
            {
                if (validaTel(TxtFax.Text) == "S")
                    lblMensaje.Text += " El teléfono 2 de contacto no es válido <br>";
            }
            if (TxtCodigoActividad.Text == "")
                lblMensaje.Text += " Debe ingresar el código de la actividad económica<br>";

            if (ddlDepartamento.SelectedValue == "" || ddlCiudad.SelectedValue == "") //20170530 divipola
                lblMensaje.Text += " Debe Seleccionar la ciudad de ubicación <br>";

            if (ddlServPublico.SelectedValue == "S")
            {
                if (TxtNoRegRups.Text.Trim().Length <= 0)
                    lblMensaje.Text += " Debe ingresar el No. de Registro RUPS<br>";
                if (TxtFechaRegRups.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaRegRups.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += " Valor Inválido en Fecha de Registro del RUPS<br>";
                    }

                }
                else
                    lblMensaje.Text += " Debe ingresar la Fecha de Registro del RUPS<br>";

                if (TxtFechaIniAct.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaIniAct.Text.Trim());
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += " Valor Inválido en Fecha de Inicio Actividad<br>";
                    }

                }
                else
                    lblMensaje.Text += " Debe ingresar la Fecha de Inicio Actividad<br>";

            }
            if (hdfEstadoAct.Value != "A" && ddlEstado.SelectedValue == "A")
                lblMensaje.Text += " Por esta pantalla NO se permite la activación de Operadores por Favor realice la aprobación. <br>"; //20170530 divipola

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoOperador.Text;
                lValorParametros[1] = ddlCodigoTipoDocumento.SelectedValue;
                lValorParametros[2] = TxtNoDocumento.Text.Trim();
                lValorParametros[3] = TxtDigitoVerificacion.Text.Trim();
                if (ddlCodigoTipoDocumento.SelectedValue == "1")
                    lValorParametros[4] = TxtRazonSocial.Text.Trim();
                else
                    lValorParametros[4] = TxtNombre.Text.Trim() + " " + TxtApellido.Text.Trim();
                lValorParametros[5] = ddlTipo.SelectedValue;
                lValorParametros[6] = ddlAutoretenedor.SelectedValue;
                lValorParametros[7] = ddlRegimen.SelectedValue;
                lValorParametros[8] = ddlContribuyente.SelectedValue;
                lValorParametros[9] = ddlGranContribuyente.SelectedValue;
                lValorParametros[10] = TxtNombreRepresentante.Text.Trim();
                lValorParametros[11] = TxtEmail.Text.Trim();
                lValorParametros[12] = TxtDireccion.Text.Trim();
                lValorParametros[13] = TxtTelefono.Text.Trim();
                lValorParametros[14] = TxtFax.Text.Trim();
                lValorParametros[15] = TxtCodigoActividad.Text.Trim();
                lValorParametros[16] = ddlEstado.SelectedValue;

                lValorParametros[18] = ddlTipoPersona.SelectedValue;
                lValorParametros[19] = TxtNombre.Text.Trim();
                lValorParametros[20] = TxtApellido.Text.Trim();
                lValorParametros[21] = TxtCelular.Text.Trim();
                //20170530 divipola
                //cambio de indices para eliminar ciudad
                //lValorParametros[22] = ddlPais.SelectedValue;
                lValorParametros[22] = ddlDepartamento.SelectedValue;
                lValorParametros[23] = ddlCiudad.SelectedValue;
                lValorParametros[24] = ddlPosicion.SelectedValue;
                if (ddlServPublico.SelectedValue == "S")
                {
                    lValorParametros[25] = ddlServPublico.SelectedValue;
                    lValorParametros[26] = TxtNoRegRups.Text.Trim();
                    lValorParametros[27] = TxtFechaRegRups.Text.Trim();
                    lValorParametros[28] = TxtFechaIniAct.Text.Trim();
                }
                lValorParametros[29] = TxtDireccionWeb.Text.Trim();
                lValorParametros[30] = TxtEmail2.Text.Trim();  //20180228 rq032-17
                lValorParametros[31] = TxtEmail3.Text.Trim(); //20180228 rq032-17
                lValorParametros[32] = TxtBanco.Text.Trim(); //20201207
                lValorParametros[33] = ddlTipoCta.SelectedValue;  //20201207
                lValorParametros[34] = TxtCuenta.Text.Trim(); //20201207

                //fin cambio de indices
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetOperador", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización del  Operador.! " + goInfo.mensaje_error.ToString(); //20170530 divipola
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoOperador.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoOperador.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        manejo_bloqueo("E", LblCodigoOperador.Text);
        lblMensaje.Text = "";
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgOperador_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgOperador.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgOperador_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoComisionista = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoComisionista = this.dtgOperador.Items[e.Item.ItemIndex].Cells[0].Text;
            //20180228 rq032-27
            if (goInfo.codigo_grupo_usuario != 1)
            {
                ddlTipoPersona.Enabled = false;
                ddlCodigoTipoDocumento.Enabled = false;
                TxtDigitoVerificacion.Enabled = false;
                TxtNoDocumento.Enabled = false;
                TxtRazonSocial.Enabled = false;
                TxtNombre.Enabled = false;
                TxtApellido.Enabled = false;
                ddlTipo.Enabled = false;
                ddlPosicion.Enabled = false;
                ddlAutoretenedor.Enabled = false;
                ddlRegimen.Enabled = false;
                ddlContribuyente.Enabled = false;
                ddlGranContribuyente.Enabled = false;
                ddlDepartamento.Enabled = false;
                ddlCiudad.Enabled = false;
                TxtCodigoActividad.Enabled = false;
                ddlServPublico.Enabled = false;
                TxtNoRegRups.Enabled = false;
                TxtFechaRegRups.Enabled = false;
                TxtFechaIniAct.Enabled = false;
                TxtDireccionWeb.Enabled = false;
                ddlEstado.Enabled = false;
            }
            //FIn 20180228 rq032-27
            Modificar(lCodigoComisionista);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_operador", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_operador)
    {
        string lsCondicion = "nombre_tabla='m_operador' and llave_registro='codigo_operador=" + lscodigo_operador + "'";
        string lsCondicion1 = "codigo_operador=" + lscodigo_operador.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_operador";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_operador", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    ///  20170530 divipola
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_operador", "@P_nit", "@P_nombre", "@P_tipo" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
        string[] lValorParametros = { "0", "0", "", "" };
        string lsParametros = "";

        try
        {

            if (TxtBusNit.Text.Trim().Length > 0)
            {
                lValorParametros[1] = TxtBusNit.Text.Trim();
                lsParametros += " Nit Operador: " + TxtBusNit.Text;
            }
            if (TxtBusRazonSocial.Text.Trim().Length > 0)
            {
                lValorParametros[2] = TxtBusRazonSocial.Text.Trim();
                lsParametros += " - Nombre Operador: " + TxtBusRazonSocial.Text;
            }
            if (ddlBusTipoOperador.SelectedValue != "0")
            {
                lValorParametros[3] = ddlBusTipoOperador.SelectedValue;
                lsParametros += " - Tipo Operador: " + ddlBusTipoOperador.SelectedItem.ToString();
            }

            lConexion.Abrir();
            dtgExcel.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetOperador", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgExcel.DataBind();
            lConexion.Cerrar();

            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfOpeExcel" + DateTime.Now + ".xls";
            DataSet lds = new DataSet();
            SqlDataAdapter lsqldata = new SqlDataAdapter();
            decimal ldCapacidad = 0;
            StringBuilder lsb = new StringBuilder();
            ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            dtgExcel.Visible = true;
            dtgExcel.EnableViewState = false;
            lpagina.EnableEventValidation = false;
            lpagina.DesignerInitialize();
            lpagina.Controls.Add(lform);
            lform.Controls.Add(dtgExcel);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;
            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Charset = "UTF-8";
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + "Listado de Operadores" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsParametros + "</font></th><td></td></tr></table><br>");
            Response.ContentEncoding = Encoding.Default;
            Response.Write(lsb.ToString());
            dtgExcel.Visible = false;
            Response.End();
            lds.Dispose();
            lsqldata.Dispose();
            lConexion.CerrarInforme();
            dtgExcel.Visible = false;
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_operador <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_operador&procedimiento=pa_ValidarExistencia&columnas=codigo_operador*no_documento*digito_verif*tipo_operador*razon_social*direccion*telefono*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!"; //20170530 divipola
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoPersona_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTipoPersona.SelectedValue == "N")
        {
            TrJuridico.Visible = false;
            TrNatural.Visible = true;
        }
        else
        {
            TrJuridico.Visible = true;
            TrNatural.Visible = false;
            TxtNombre.Text = "";
            TxtApellido.Text = "";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlServPublico_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlServPublico.SelectedValue == "S")
        {
            TrServPub.Visible = true;
            TrServPub1.Visible = true;
        }
        else
        {
            TrServPub.Visible = false;
            TrServPub1.Visible = false;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlCiudad.Items.Clear();
        LlenarControles(lConexion.gObjConexion, ddlCiudad, "m_divipola", " estado = 'A' and codigo_departamento =" + ddlDepartamento.SelectedValue + " and codigo_centro='0' order by nombre_centro", 3, 4);
        lConexion.Cerrar();

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 20180228 rq032-17
    protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTipo.SelectedValue == "I" || ddlTipo.SelectedValue == "C")
        {
            ddlServPublico.Enabled = true;
        }
        else
        {
            ddlServPublico.Enabled = false;
            ddlServPublico.SelectedValue = "N";
            ddlServPublico_SelectedIndexChanged(null, null);
        }
        //20180228 rq032-27
        if (goInfo.codigo_grupo_usuario != 1)
            ddlServPublico.Enabled = false;
    }
    //20180228 rq032-17s
    protected string validaTel(string lsCampo)
    {
        string lsError = "N";
        Regex rgx = new Regex(@"^\([1-9]\)\d{7}$");
        string[] lsTelefono = lsCampo.Split(';');
        foreach (string lsTTel in lsTelefono)
        {
            if (!rgx.IsMatch(lsTTel))
                lsError = "S";
        }
        return lsError;

    }

}