﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ModalidadContractual.aspx.cs"
    Inherits="BASE_frm_ModalidadContractual" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_ModalidadContractual.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_ModalidadContractual.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_ModalidadContractual.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Modalidad
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoModalidad" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoModalidad" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Descripción
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
                <%-- <asp:RegularExpressionValidator ID="RevTxtDescripcion" ControlToValidate="TxtDescripcion"
                    runat="server" ValidationExpression="^([0-9a-zA-Z]{3})([0-9a-zA-Z .-_&])*$" ErrorMessage="Valor muy corto en el Campo Descripcion"
                    ValidationGroup="comi"> * </asp:RegularExpressionValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Sigla
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtSigla" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Modalidad de Contingencia
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="DdlConting" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Aplica para cálculo de porcentaje máximo de energía total demandada UVLP
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlUvlp" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--Campo Nuevo Req. 007-17 Subasta Bimestral 20170209--%>
        <tr>
            <td class="td1" colspan="1">
                Tomar en Cuenta en Selección Contratos Declaración Subasta Bimestral
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlSelecContSb" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
                <%--Campo Nuevo Req. 009-17 Indicadores Fase II 20170317 --%>
        <tr>
            <td class="td1" colspan="1">
                Tomada para Indicador MP 21
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlIndicadorMp" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--20190404 rq018-19 fase II--%>
         <tr>
            <td class="td1" colspan="1">
                Tomado para validar PTDVF/CIDVF en negociaciones de largo plazo
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlControlPtdvf" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--20211119 control ptdvf mp--%>
         <tr>
            <td class="td1" colspan="1">
                Control PTDVF/CIDVF contratos de Mercado Primario
            </td>
            <td class="td2" colspan="1">
                <asp:DropDownList ID="ddlPtdvfMp" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Código Modalidad <%--20190404 rq018-19 fase II--%>
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusModalidad" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusModalidad" runat="server" TargetControlID="TxtBusModalidad"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Descripción
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusDescripcion" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <%--20190404 rq018-19 fase II--%>
                            <asp:BoundColumn DataField="codigo_modalidad" HeaderText="Código Modalidad" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                             <%--20190404 rq018-19 fase II--%>
                            <asp:BoundColumn DataField="descripcion" HeaderText="Descripción" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="350px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sigla" HeaderText="Sigla" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ind_contingencia" HeaderText="Contingencia" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ind_porc_total_uvlp" HeaderText="aplica porcentaje UVLP" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--Campo Nuevo Req. 007-17 Subasta Bimestral 20170209--%>
                            <asp:BoundColumn DataField="ind_selec_contrato_sb" HeaderText="Tener Cuenta Selec. Contratos SB" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <%--Campo Nuevo Req. 009-17 Indicadores Fase II 20170307--%>
                            <asp:BoundColumn DataField="ind_indicador_mp_21" HeaderText="Tener Cuenta Indicador 21 MP"
                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            <%--20190404 rq018-19 fase II--%>
                            <asp:BoundColumn DataField="ind_valida_ptdvf_lp" HeaderText="Usado en control PTDVF LP"
                                ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="center">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    </asp:Content>