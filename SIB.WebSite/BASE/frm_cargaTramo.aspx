﻿

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_cargaTramo.aspx.cs"
    Inherits="BASE_frm_cargaTramo" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" Text="CARGA TRAMOS SNT" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Seleccione el Archivo:
            </td>
            <td class="td2">
                <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <br />
                <asp:Button ID="BtnCargar" runat="server" Text="Cargue Archivo" OnClick="BtnCargar_Click" /><br />
                <asp:HiddenField ID="hndID" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" class="td2">
                <asp:Label ID="ltCargaArchivo" runat="server" Width="100%" ForeColor="red"></asp:Label>
                <asp:HiddenField ID="hdfNomArchivo" runat="server" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>