﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosAdc.aspx.cs"
    Inherits="BASE_frm_ParametrosAdc" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">Días hábiles inicio ingreso información operativa gasoducto de conexión
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaIniCon" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtDiaIniCon" runat="server" TargetControlID="TxtDiaIniCon"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Días hábiles fin ingreso información operativa gasoducto de conexión
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaFinCon" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtDiaFinCon" runat="server" TargetControlID="TxtDiaFinCon"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">Días hábiles publicación información operativa gasoducto de conexión
            </td>
            <td class="td2"><%--20200925  c1 bimestral--%>
                <asp:TextBox ID="TxtDiaPubCon" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtDiaPubCon" runat="server" TargetControlID="TxtDiaPubCon"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>

        </tr>
        <%--20210317 Indicador Factura--%>
        <tr>
            <td class="td1">Días hábiles de fecha de facturación para control de indicador de oportunidad de facturación
            </td>
            <td class="td2"><%--20200925  c1 bimestral--%>
                <asp:TextBox ID="TxtDiaIndFact" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtDiaIndFact"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Días hábiles de envío de correo de ejecución de proceso de facturación
            </td>
            <td class="td2"><%--20200925  c1 bimestral--%>
                <asp:TextBox ID="TxtDiaEjeFact" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TxtDiaIndFact"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>

        <%--20210804 modiicacion ptdvf--%>
        <tr>
            <td class="td1">Fecha inicial para ingreso de PTDVF
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecIniPtdvf" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <td class="td1">Fecha final para ingreso de PTDVF
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecFinPtdvf" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <%--20210804 modiicacion ptdvf--%>
        <tr>
            <td class="td1">Fecha máxima de modificación de PTDVF
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFecModPtdvf" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
            </td>
            <%--20220202 --%>
            <td class="td1" colspan="1">Hora Envío correo verificación inyectada Vs Recibida
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraCorreoVerIny" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraCorreoVerIny" ControlToValidate="TxtHoraCorreoVerIny"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la hora del correo verificación inyectada Vs Recibida"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20220202 veriofica opertaiva--%>
        <tr>
            <td class="td1">Periodicidad envío de correo verificación inyectada Vs recibida
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPerCorreoVerIny" runat="server">
                    <asp:ListItem Value="D">Diario</asp:ListItem>
                    <asp:ListItem Value="S">Semanal</asp:ListItem>
                    <asp:ListItem Value="M">Mensual</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Día Calendario Envío de correo verificación inyectada Vs recibida
            </td>
            <td class="td2">
                <asp:TextBox ID="txtDiaHabCorreoVerIny" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebtxtDiaHabCorreoVerIny" runat="server" TargetControlID="txtDiaHabCorreoVerIny"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--20220202 veriofica opertaiva--%>
        <tr>
            <td class="td1" colspan="1">Hora Envío correo verificación Tomada Vs Entrega usuario final
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraCorreoVerTom" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revTxtHoraCorreoVerTom" ControlToValidate="TxtHoraCorreoVerTom"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora del correo verificación Tomada Vs Entrega usuario final"> * </asp:RegularExpressionValidator>
            </td>

            <td class="td1">Periodicidad envío de correo verificación Tomada Vs Entrega usuario final
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPerCorreoVerTom" runat="server">
                    <asp:ListItem Value="D">Diario</asp:ListItem>
                    <asp:ListItem Value="S">Semanal</asp:ListItem>
                    <asp:ListItem Value="M">Mensual</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Día Calendario Envío de correo verificación Tomada Vs Entrega usuario final
            </td>
            <td class="td2">
                <asp:TextBox ID="txtDiaHabCorreoVerTom" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebtxtDiaHabCorreoVerTom" runat="server" TargetControlID="txtDiaHabCorreoVerTom"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Porcentaje general de error en verificación de puntos de entrada
            </td>
            <td class="td2">
                <asp:TextBox ID="txtPorcEnt" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebtxtPorcEnt" runat="server" TargetControlID="txtPorcEnt"
                    FilterType="Custom, Numbers" ValidChars="."></cc1:FilteredTextBoxExtender>
            </td>
        </tr>

        <%--20220202 veriofica opertaiva--%>
        <tr>
            <td class="td1">Porcentaje general de error en verificación de puntos de salida
            </td>
            <td class="td2">
                <asp:TextBox ID="txtPorcSal" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebtxtPorcSal" runat="server" TargetControlID="txtPorcSal"
                    FilterType="Custom, Numbers" ValidChars="."></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Observación Cambio
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <%--20220211 contrsena--%>
        <tr>
            <td class="td1">Número de intentos fallidos para bloqueo de contraseña
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtIntCont" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtIntCont" runat="server" TargetControlID="TxtIntCont"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1">Minutos para desbloqueo automático de contraseña 
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtMinutDesb" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtMinutDesb" runat="server" TargetControlID="TxtMinutDesb"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--20220211 contrsena--%>
        <tr>
            <td class="td1">Cantidad de contraseñas historicas
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtContHist" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtContHist" runat="server" TargetControlID="TxtContHist"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <%--20220314 pareja de cargos--%>
            <td class="td1">Días hábiles antes de fin de mes para carga de tarifas de transporte
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaTarTra" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftebTxtDiaTarTra" runat="server" TargetControlID="TxtDiaTarTra"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--20220505 desistimiento--%>
        <tr>
            <td class="td1">Días hábiles para aprobar desistimiento
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDiaDesist" runat="server" MaxLength="3" Width="150px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="ftbeTxtDiaDesist" runat="server" TargetControlID="TxtDiaDesist"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsParam" runat="server" ValidationGroup="VsParametrosBna" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
