﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParamContingBilateral : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Contingencia Bilateral";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPeriodoEnt, "m_periodos_entrega", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_param_contingencia_bilateral");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[11].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[12].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoParametro.Visible = false;
        LblCodigoParametro.Visible = true;
        LblCodigoParametro.Text = "Automatico";

    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_param_contingencia_bilateral", " codigo_parametro_contingencia = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoParametro.Text = lLector["codigo_parametro_contingencia"].ToString();
                        TxtCodigoParametro.Text = lLector["codigo_parametro_contingencia"].ToString();
                        try
                        {
                            ddlTipoSubasta.SelectedValue = lLector["codigo_tipo_subasta"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "EL tipo de subasta del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlModalidad.SelectedValue = lLector["codigo_modalidad"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "La Modalidad del registro no existe o esta inactiva<br>";
                        }
                        try
                        {
                            ddlPeriodoEnt.SelectedValue = lLector["codigo_periodo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El Periodo del registro no existe o esta inactiva<br>";
                        }
                        ddlMercado.SelectedValue = lLector["tipo_mercado"].ToString();
                        ddlDestino.SelectedValue = lLector["destino_rueda"].ToString();
                        ddlMesesCompletos.SelectedValue = lLector["sumins_meses_completos"].ToString();
                        TxtFechaIniSumin.Text = lLector["fecha_ini_suministro"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_suministro"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_suministro"].ToString().Substring(0, 2);
                        TxtFechaFinSumin.Text = lLector["fecha_fin_suministro"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_suministro"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_suministro"].ToString().Substring(0, 2);
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        TxtMinDias.Text = lLector["minimo_dias_neg"].ToString();
                        ddlMesesCompletos_SelectedIndexChanged(null, null);
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoParametro.Visible = false;
                        LblCodigoParametro.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Caracteristica " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_parametro_contingencia", "@P_codigo_tipo_subasta" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0" };

        try
        {
            if (TxtBusParametro.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusParametro.Text.Trim();
            if (ddlBusTipoSubasta.SelectedValue != "0")
                lValorParametros[1] = ddlBusTipoSubasta.SelectedValue;
            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetParamContingBilateral", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_parametro_contingencia", "@P_codigo_tipo_subasta", "@P_codigo_modalidad", "@P_codigo_periodo", "@P_destino_rueda", "@P_tipo_mercado", "@P_fecha_ini_suministro", "@P_fecha_fin_suministro", "@P_sumins_meses_completos", "@P_minimo_dias_neg", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { "0", ddlTipoSubasta.SelectedValue, ddlModalidad.SelectedValue, ddlPeriodoEnt.SelectedValue, ddlDestino.SelectedValue, ddlMercado.SelectedValue, TxtFechaIniSumin.Text.Trim(), TxtFechaFinSumin.Text.Trim(), ddlMesesCompletos.SelectedValue, TxtMinDias.Text.Trim(), ddlEstado.SelectedValue, "1" };
        lblMensaje.Text = "";
        goInfo.mensaje_error = "";
        DateTime ldFecha;
        decimal ldDias = 0;

        try
        {
            if (ddlTipoSubasta.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Subasta. <br>";
            if (ddlModalidad.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar la Modalidad Contractual. <br>";
            if (ddlPeriodoEnt.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Periodo de Entrega. <br>";
            if (TxtFechaIniSumin.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Inicial de Suministro.<br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniSumin.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Fecha Inicial de Suministro.<br>";
                }
            }
            if (TxtFechaFinSumin.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Final de Contingencia.<br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFinSumin.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtFechaIniSumin.Text.Trim()))
                        lblMensaje.Text += "Fecha Final de Suministro NO puede ser menor a Fecha Inicial de Suministro.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Fecha Final de Suministro.<br>";
                }
            }
            if (ddlMesesCompletos.SelectedValue == "N")
            {
                if (TxtMinDias.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar el No Maximo de Días de Negociación.<br>";
                else
                {
                    try
                    {
                        ldDias = Convert.ToInt32(TxtMinDias.Text.Trim());
                        if (ldDias < 0)
                            lblMensaje.Text += "Valor Invalido en Campo Minimo Días de Negociación.<br>";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Invalido en Campo Minimo Días de Negociación.<br>";
                    }
                }
            }
            if (VerificarExistencia("codigo_tipo_subasta=" + ddlTipoSubasta.SelectedValue + " and tipo_mercado='" + ddlMercado.SelectedValue + "' and destino_rueda='" + ddlDestino.SelectedValue + "' and codigo_modalidad ='" + ddlModalidad.SelectedValue + "' and codigo_periodo = " + ddlPeriodoEnt.SelectedValue))
                lblMensaje.Text += " Ya se definió el Parametro para  Subasta, Destino, Tipo Mercado, Modalidad y Periodo. <br>";
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParamContingBilateral", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema al crear el Parametro.! " + goInfo.mensaje_error;
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_parametro_contingencia", "@P_codigo_tipo_subasta", "@P_codigo_modalidad", "@P_codigo_periodo", "@P_destino_rueda", "@P_tipo_mercado", "@P_fecha_ini_suministro", "@P_fecha_fin_suministro", "@P_sumins_meses_completos", "@P_minimo_dias_neg", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { LblCodigoParametro.Text, ddlTipoSubasta.SelectedValue, ddlModalidad.SelectedValue, ddlPeriodoEnt.SelectedValue, ddlDestino.SelectedValue, ddlMercado.SelectedValue, TxtFechaIniSumin.Text.Trim(), TxtFechaFinSumin.Text.Trim(), ddlMesesCompletos.SelectedValue, TxtMinDias.Text.Trim(), ddlEstado.SelectedValue, "2" };
        lblMensaje.Text = "";
        DateTime ldFecha;
        decimal ldDias = 0;
        try
        {
            if (TxtFechaIniSumin.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Inicial de Suministro.<br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaIniSumin.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Fecha Inicial de Suministro.<br>";
                }
            }
            if (TxtFechaFinSumin.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Fecha Final de Contingencia.<br>";
            else
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaFinSumin.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(TxtFechaIniSumin.Text.Trim()))
                        lblMensaje.Text += "Fecha Final de Suministro NO puede ser menor a Fecha Inicial de Suministro.<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Invalido en Campo Fecha Final de Suministro.<br>";
                }
            }
            if (ddlMesesCompletos.SelectedValue == "N")
            {
                if (TxtMinDias.Text.Trim().Length <= 0)
                    lblMensaje.Text += "Debe Ingresar el No Maximo de Días de Negociación.<br>";
                else
                {
                    try
                    {
                        ldDias = Convert.ToInt32(TxtMinDias.Text.Trim());
                        if (ldDias < 0)
                            lblMensaje.Text += "Valor Invalido en Campo Minimo Días de Negociación.<br>";
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Valor Invalido en Campo Minimo Días de Negociación.<br>";
                    }
                }
            }
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParamContingBilateral", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de los Parametros .!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoParametro.Text);
                    Listar();
                }
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoParametro.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoParametro.Text != "")
            manejo_bloqueo("E", LblCodigoParametro.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlTipoSubasta.Enabled = false;
            ddlDestino.Enabled = false;
            ddlMercado.Enabled = false;
            ddlPeriodoEnt.Enabled = false;
            ddlModalidad.Enabled = false;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_param_contingencia_bilateral", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_param_contingencia_bilateral' and llave_registro='codigo_parametro_contingencia=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_parametro_contingencia=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_param_contingencia_bilateral";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_param_contingencia_bilateral", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusParametro.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusParametro.Text.Trim();
                lsParametros += " Codigo Parametros Contingencia Bilateral: " + TxtBusParametro.Text;

            }
            if (ddlBusTipoSubasta.SelectedValue != "0")
            {
                lValorParametros[1] = ddlBusTipoSubasta.SelectedValue.Trim();
                lsParametros += " - Tipo Subasta: " + ddlBusTipoSubasta.SelectedItem.ToString();
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetParamContingBilateral&nombreParametros=@P_codigo_parametro_contingencia*@P_codigo_tipo_subasta&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_caracteristica_sub*codigo_tipo_subasta*desc_tipo_subasta*codigo_tipo_subasta*tipo_caracteristica*codigo_caracteristica*desc_caracteristica*tipo_mercado*destino_rueda*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de Parametros Contingencia Bilateral&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_caracteristica_sub <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_param_contingencia_bilateral&procedimiento=pa_ValidarExistencia&columnas=codigo_parametro_contingencia*codigo_tipo_subasta*tipo_mercado*destino_rueda*codigo_modalidad*codigo_periodo*fecha_ini_suministro*fecha_fin_suministro*sumins_meses_completos*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlMesesCompletos_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMesesCompletos.SelectedValue == "S")
        {
            TxtMinDias.Text = "0";
            TxtMinDias.Enabled = false;
        }
        else
            TxtMinDias.Enabled = true;
    }
}