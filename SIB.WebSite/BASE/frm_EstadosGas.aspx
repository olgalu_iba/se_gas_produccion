﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_EstadosGas.aspx.cs"
    Inherits="BASE_frm_EstadosGas" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_EstadosGas.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_EstadosGas.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>       
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Estado
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoEstado" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoEstado" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Destino Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoEstado" runat="server">
                    <asp:ListItem Value="I">Id</asp:ListItem>
                    <asp:ListItem Value="R">Rueda</asp:ListItem>
                    <asp:ListItem Value="P">Postura</asp:ListItem>
                    <asp:ListItem Value="C">Contratos</asp:ListItem>
                    <asp:ListItem Value="V">Verificacion</asp:ListItem>
                    <asp:ListItem Value="O">Operdor Subasta bimestral</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="S">Sarlaft</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="T">Rutas</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="M">Solicitud modificación</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="D">Modificación Cesión</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="F">Firma Digital</asp:ListItem> <%--20181210 rq046-18--%>
                    <asp:ListItem Value="E">Ejecución de contratos</asp:ListItem><%--20190322 rq018-19--%>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Sigla
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtSigla" runat="server" MaxLength="1" Width="100px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">
                Descripción
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
                <%--20190322 rq018-19--%>
                <%--<asp:RegularExpressionValidator ID="RevTxtDescripcion" ControlToValidate="TxtDescripcion"
                    runat="server" ValidationExpression="^([0-9a-zA-Z]{3})([0-9a-zA-Z .-_&])*$" ErrorMessage="Valor muy corto en el Campo Descripcion"
                    ValidationGroup="comi"> * </asp:RegularExpressionValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Tipo Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusTipoEstado" runat="server">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>  <%--20171130 rq026-17--%>
                    <asp:ListItem Value="I">Id</asp:ListItem>  <%--20171130 rq026-17--%>
                    <asp:ListItem Value="R">Rueda</asp:ListItem>  <%--20171130 rq026-17--%>
                    <asp:ListItem Value="P">Postura</asp:ListItem>  <%--20171130 rq026-17--%>
                    <asp:ListItem Value="C">Contratos</asp:ListItem>  <%--20171130 rq026-17--%>
                    <asp:ListItem Value="V">Verificacion</asp:ListItem>  <%--20171130 rq026-17--%>
                    <asp:ListItem Value="O">Operdor Subasta bimestral</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="S">Sarlaft</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="T">Rutas</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="M">Solicitud modificación</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="D">Modificación Cesión</asp:ListItem> <%--20171130 rq026-17--%>
                    <asp:ListItem Value="F">Firma Digital</asp:ListItem> <%--20181210 rq046-18--%>
                    <asp:ListItem Value="E">Ejecución de contratos</asp:ListItem><%--20190322 rq018-19--%>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_estado_gas" HeaderText="Codigo Estado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_estado" HeaderText="Tipo Estado" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="sigla_estado" HeaderText="Sigla" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="350px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>