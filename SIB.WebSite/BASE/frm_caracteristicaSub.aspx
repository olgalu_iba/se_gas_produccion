﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_caracteristicaSub.aspx.cs"
    Inherits="BASE_frm_caracteristicaSub" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_caracteristicaSub.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_caracteristicaSub.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_caracteristicaSub.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Caracteristica Subasta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoCar" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoCar" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo Característica
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoCar" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoCar_SelectedIndexChanged">
                    <asp:ListItem Value="P">Producto</asp:ListItem>
                    <asp:ListItem Value="E">Período de Entrega</asp:ListItem> <%--20170828 rq041-17--%>
                    <asp:ListItem Value="M">Modalidad Contractual</asp:ListItem>
                    <asp:ListItem Value="T">Puntos SNT (Solo Dir-Sum MS-LP)</asp:ListItem>  <%--20170828 rq041-17--%>
                    <asp:ListItem Value="C">Campo (Solo Bilateral)</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                característica
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlCaracteristica" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Destino Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlDestino" runat="server">
                    <asp:ListItem Value="G">Gas</asp:ListItem>
                    <asp:ListItem Value="T">Transporte</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo mercado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlMercado" runat="server">
                    <asp:ListItem Value="P">Primario</asp:ListItem>
                    <asp:ListItem Value="S">Secundario</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Codigo Caracteristica Subasta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCaracSub" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusCaracSub" runat="server" TargetControlID="TxtBusCaracSub"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusTipoSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tipo Caracteristica
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusTipoCar" runat="server">
                    <asp:ListItem Value="0">Seleccione</asp:ListItem>
                    <asp:ListItem Value="P">Producto</asp:ListItem>
                    <asp:ListItem Value="E">Período de Entrega</asp:ListItem> <%--20170929 rq048-17--%>
                    <asp:ListItem Value="M">Modalidad Contractual</asp:ListItem>
                    <asp:ListItem Value="T">Puntos SNT (Solo Dir Sum MS)</asp:ListItem>
                    <asp:ListItem Value="C">Campo (Solo Bilateral)</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_caracteristica_sub" HeaderText="Codigo Modalidad"
                                ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_subasta" HeaderText="cod sub" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="desc sub" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_subasta" HeaderText="cod sub" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_caracteristica" HeaderText="tpo car" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_caracteristica" HeaderText="cod car" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_caracteristica" HeaderText="desc car" ItemStyle-HorizontalAlign="left"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_mercado" HeaderText="tpo emr" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="destino_rueda" HeaderText="tpo prod" ItemStyle-HorizontalAlign="center"
                                ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>