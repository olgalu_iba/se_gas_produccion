﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_MercadoDemanda : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Tipo de Demanda por Sector de Consumo";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusDemanda, "m_tipo_demanda_atender", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusSector, "m_sector_consumo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();

            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_demanda_sector");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[8].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[9].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoSectorDemand.Visible = false;
        LblCodigoSectorDemand.Visible = true;
        LblCodigoSectorDemand.Text = "Automatico";
        ddlDemanda.Enabled = true;
        ddlSector.Enabled = true;
        ddlUso.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_demanda_sector", " codigo_demanda_sector = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoSectorDemand.Text = lLector["codigo_demanda_sector"].ToString();
                        TxtCodigoSectorDemand.Text = lLector["codigo_demanda_sector"].ToString();
                        try
                        {
                            ddlDemanda.SelectedValue = lLector["codigo_tipo_demanda"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tipo de demanda no existe o esta inactivo";
                        }
                        try
                        {
                            ddlSector.SelectedValue = lLector["codigo_sector_consumo"].ToString();
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El sector de consumo no existe o esta inactivo";
                        }
                        ddlUso.SelectedValue = lLector["ind_uso"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();

                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoSectorDemand.Visible = false;
                        LblCodigoSectorDemand.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo demanda-sector " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_demanda_sector", "@P_codigo_tipo_demanda", "@P_codigo_sector_consumo" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "0" };

        try
        {
            if (TxtBusSectorDemand.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusSectorDemand.Text.Trim();
            if (ddlBusDemanda.SelectedValue != "0")
                lValorParametros[1] = ddlBusDemanda.SelectedValue;
            if (ddlBusSector.SelectedValue != "0")
                lValorParametros[2] = ddlBusSector.SelectedValue;

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetDemandaSector", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_demanda_sector", "@P_codigo_tipo_demanda", "@P_codigo_sector_consumo","@P_estado", "@P_accion" , "@P_ind_uso"};
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { "0", "0", "0", "A", "1", "" };
        lblMensaje.Text = "";

        try
        {
            if (ddlDemanda.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de demanda a atender. <br>";
            if (ddlSector.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el sector de consumo. <br>";
            if (VerificarExistencia(" codigo_tipo_demanda = " + ddlDemanda.SelectedValue + " And codigo_sector_consumo= " + ddlSector.SelectedValue +" And ind_uso='" + ddlUso.SelectedValue + "'"))
                lblMensaje.Text += " Ya Existe un Registro para el Tipo De demanda, sector de consumo y uso<br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = ddlDemanda.SelectedValue.Trim();
                lValorParametros[2] = ddlSector.SelectedValue.Trim();
                lValorParametros[3] = ddlEstado.SelectedValue;
                lValorParametros[5] = ddlUso.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetDemandaSector", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del Tipo demanda-sector .!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_demanda_sector", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int,  SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { "0","A", "2" };
        lblMensaje.Text = "";
        try
        {
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoSectorDemand.Text;
                lValorParametros[1] = ddlEstado.SelectedValue;
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetDemandaSector", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de la demanda-sector.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoSectorDemand.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoSectorDemand.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoSectorDemand.Text != "")
            manejo_bloqueo("E", LblCodigoSectorDemand.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlDemanda.Enabled = false;
            ddlSector.Enabled = false;
            ddlUso.Enabled = false;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_demanda_sector", lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_demanda_sector' and llave_registro='codigo_demanda_sector=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_demanda_sector=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_demanda_sector";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_demanda_sector", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusSectorDemand.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusSectorDemand.Text.Trim();
                lsParametros += " Codigo Demanda-sector: " + TxtBusSectorDemand.Text;

            }
            if (ddlBusDemanda.SelectedValue != "0")
            {
                lValorParametros[1] = ddlBusSector.SelectedValue;
                lsParametros += " - Tipo demanda : " + ddlBusSector.SelectedItem.ToString();
            }
            if (ddlBusSector.SelectedValue != "0")
            {
                lValorParametros[2] = ddlBusSector.SelectedValue;
                lsParametros += " - Sector Consumo: " + ddlBusSector.SelectedItem.ToString();
            }

            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetDemandaSector&nombreParametros=@P_codigo_demanda_sector*@P_codigo_tipo_demanda*@P_codigo_sector_consumo&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_demanda_sector&titulo_informe=Listado de tipos de demanda por sector de consumo&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_demanda_sector <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_demanda_sector&procedimiento=pa_ValidarExistencia&columnas=codigo_demanda_sector*codigo_tipo_demanda*codigo_sector_consumo*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
}
