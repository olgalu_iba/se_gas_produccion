﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosTrasnporte.aspx.cs"
    Inherits="BASE_frm_ParametrosTrasnporte" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body" runat="server">
                <div class="row" id="divDatos" runat="server">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles previos a la finalización del trimestre para enviar notificación de ingreso de información CMMP de proyectos IPAT </label>
                            <asp:TextBox ID="TxtIngCmmp" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtIngCmmp" runat="server" TargetControlID="TxtIngCmmp"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles máximos previos al trimestre de negociación para ingresar información CMMP de proyectos IPAT </label>
                            <asp:TextBox ID="TxtIngIpat" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtIngIpat" runat="server" TargetControlID="TxtIngIpat"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles de inicio del trimestre de negociación para publicación de información CMMP de proyectos IPAT</label>
                            <asp:TextBox ID="TxtPubIpat" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtPubIpat" runat="server" TargetControlID="TxtPubIpat"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles para el ingreso de solicitudes de remitentes en el inicio de trimestre de negociación</label>
                            <asp:TextBox ID="TxtSolRem" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtSolRem" runat="server" TargetControlID="TxtSolRem"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles para la publicación de solicitudes de remitentes en el inicio de trimestre de negociación</label>
                            <asp:TextBox ID="TxtPubSolIni" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtPubSolIni" runat="server" TargetControlID="TxtPubSolIni"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles para la modificación de solicitudes de remitentes en el inicio de trimestre de negociación</label>
                            <asp:TextBox ID="TxtModSolRem" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="fteTxtModSolRem" runat="server" TargetControlID="TxtModSolRem"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles para la publicación de la modificación de solicitudes de remitentes en el inicio de trimestre de negociación</label>
                            <asp:TextBox ID="TxtPubSolFin" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FteTxtPubSolFin" runat="server" TargetControlID="TxtPubSolFin"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles otrosí contrato que excede capacidad contratada</label>
                            <asp:TextBox ID="TxtOtroSi" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FtebTxtOtroSi" runat="server" TargetControlID="TxtOtroSi"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil trimestre de negociación para carga de invitación pública a los remitentes interesados en capacidad asociada a ampliación</label>
                            <asp:TextBox ID="txtInvPub" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TxtOtroSi"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil del tercer mes del trimestre de negociación  para declarar las capacidades de ampliación solicitadas por remitente</label>
                            <asp:TextBox ID="txtCapAmp" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtInvPub" runat="server" TargetControlID="txtInvPub"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil del tercer mes del trimestre de negociación  para publicar las capacidades de ampliación solicitadas por remitente</label>
                            <asp:TextBox ID="txtPubAmp" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtPubAmp" runat="server" TargetControlID="txtPubAmp"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles previos  a finalización de semana para cargar cantidades  disponibles para la venta</label>
                            <asp:TextBox ID="txtCntDisp" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtCntDisp" runat="server" TargetControlID="txtCntDisp"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles previos  a finalización de semana para publicación de cantidades  disponibles para la venta</label>
                            <asp:TextBox ID="txtPubDisp" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtPubDisp" runat="server" TargetControlID="txtPubDisp"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles del trimestre de negociación para publicar la CMMP de proyectos PAG</label>
                            <asp:TextBox ID="txtPubPag" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtPubPag" runat="server" TargetControlID="txtPubPag"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles previos a la finalización del segundo mes del trimestre de negociación para cargar solicitudes de remitentes PAG</label>
                            <asp:TextBox ID="txtRemPag" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtRemPag" runat="server" TargetControlID="txtRemPag"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles del tercer mes del trimestre de negociación para asignar las solicitudes de los remitentes de proyectos PAG</label>
                            <asp:TextBox ID="txtAsigPag" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtAsigPag" runat="server" TargetControlID="txtAsigPag"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles del tercer mes del trimestre de negociacion para  publicar las asignaciones de las solicitudes de los remitentes de proyectos PAG</label>
                            <asp:TextBox ID="txtPubAsigPag" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtPubAsigPag" runat="server" TargetControlID="txtPubAsigPag"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles previos a la finalización del trimestre de negociación para registrar los contratos PAG resultantes de la asignación</label>
                            <asp:TextBox ID="txtRegPag" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtRegPag" runat="server" TargetControlID="txtRegPag"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Promedio simple del ingreso anual en pesos de la remuneración del proyecto del PAG</label>
                            <asp:TextBox ID="txtIaeCop" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtIaeCop" runat="server" TargetControlID="txtIaeCop"
                                FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Promedio simple del ingreso anual en dólares de la remuneración del proyecto del PAG</label>
                            <asp:TextBox ID="txtIaeUsd" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftentxtIaeUsd" runat="server" TargetControlID="txtIaeUsd"
                                FilterType="Custom, Numbers" ValidChars="."></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <%--Campos nuevos Req. 005-2021 20210120--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil inicial desde el comienzo del trimestre de negociación para hacer la declaración de los comercializadores</label>
                            <asp:TextBox ID="txtDiaIniComDecla" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtDiaIniComDecla" runat="server" TargetControlID="txtDiaIniComDecla"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil final desde el comienzo del trimestre de negociación para hacer la declaración de los comercializadores</label>
                            <asp:TextBox ID="txtDiaFinComDecla" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtDiaFinComDecla" runat="server" TargetControlID="txtDiaFinComDecla"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil desde el comienzo del trimestre de negociación para publicar la declaración de los comercializadores</label>
                            <asp:TextBox ID="txtDiaDesPubDecla" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtDiaDesPubDecla" runat="server" TargetControlID="txtDiaDesPubDecla"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil desde el comienzo del trimestre de negociación para habilitar los participantes de la subasta</label>
                            <asp:TextBox ID="txtDiaDesHabParti" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtDiaDesHabParti" runat="server" TargetControlID="txtDiaDesHabParti"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil desde el comienzo del trimestre de negociación para hacer la declaración de oferta</label>
                            <asp:TextBox ID="txtDiaDesDeclaOfer" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtDiaDesDeclaOfer" runat="server" TargetControlID="txtDiaDesDeclaOfer"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Hora inicial para hacer la declaración de la oferta</label>
                            <asp:TextBox ID="txtHoraIniDecOfer" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RevtxtHoraIniDecOfer" ControlToValidate="txtHoraIniDecOfer"
                                runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                                ErrorMessage="Formato Incorrecto para la Hora inicial para hacer la declaración de la oferta"> * </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Hora final para hacer la declaración de la oferta</label>
                            <asp:TextBox ID="txtHoraFinDecOfer" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RetxtHoraFinDecOfer" ControlToValidate="txtHoraFinDecOfer"
                                runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                                ErrorMessage="Formato Incorrecto para la Hora final para hacer la declaración de la oferta"> * </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil desde el comienzo del trimestre de negociación para hacer la publicación de precios de reserva</label>
                            <asp:TextBox ID="txtDiaDesPubPrec" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtDiaDesPubPrec" runat="server" TargetControlID="txtDiaDesPubPrec"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Día hábil desde el comienzo del trimestre de negociación para hacer la asignación de proyectos PAG de demanda regulada</label>
                            <asp:TextBox ID="txtDiaDesAsigProy" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtDiaDesAsigProy" runat="server" TargetControlID="txtDiaDesAsigProy"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Cantidad mínima postura subasta</label>
                            <asp:TextBox ID="txtCantMInPostSub" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FetxtCantMInPostSub" runat="server" TargetControlID="txtCantMInPostSub"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <%--Hasta Aqui--%>
                    <%--20220106 rq001-22--%>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Días hábiles del trimestre para registro de contratos</label>
                            <asp:TextBox ID="txtDiasTrimReg" runat="server" CssClass="form-control"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="ftebtxtDiasTrimReg" runat="server" TargetControlID="txtDiasTrimReg"
                                FilterType="Custom, Numbers"></ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Observación Cambio</label>
                            <asp:TextBox ID="TxtObservacion" runat="server" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="registroAdicion" tabindex="-1" role="dialog" aria-labelledby="mdlregistroAdicion" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="registroAdicionInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="lblRegistroAdicion" runat="server">Confirmar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label>Está seguro que desea actualizar los parámetros?</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button Text="Cancelar" CssClass="btn btn-secondary" OnClick="Cancel_OnClick" runat="server" />
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-primary" Text="Aceptar" OnClick="imbCrear_Click1" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>

