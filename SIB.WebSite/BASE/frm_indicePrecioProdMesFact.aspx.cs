﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Dominio;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace BASE
{
    // ReSharper disable once IdentifierTypo
    // ReSharper disable once InconsistentNaming
    public partial class frm_indicePrecioProdMesFact : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "IPP DANE- Facturación"; //20170929 rq048-17
        /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
        clConexion lConexion = null; //20200727
        private SqlDataReader lLector;
        private string gsTabla = "m_indices_prod_mes_fact";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;

            //Activacion de los Botones
            buttons.Inicializar(ruta: gsTabla);
            //buttons.CrearOnclick += btnNuevo; //20200924 ajsue componente
            buttons.FiltrarOnclick += btnConsultar_Click;
            buttons.CrearOnclick += btnNuevo;
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.ExportarPdfOnclick += ImgPdf_Click;
            lConexion = new clConexion(goInfo); //20200727

            if (IsPostBack) return;
            //Establese los permisos del sistema

            //Titulo
            Master.Titulo = "IPP-DANE";
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlBusMes, "m_mes", " 1=1", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlMes, "m_mes", " 1=1", 0, 1);
            lConexion.Cerrar();

            Inicializar();
            Listar();
            EstablecerPermisosSistema();
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        /// <summary>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }


        /// <summary>
        /// 
        /// </summary>
        private void Inicializar()
        {
            EnumBotones[] botones = { };
            botones = new[] { EnumBotones.Buscar, EnumBotones.Excel, EnumBotones.Pdf };
            TxtIndice.Enabled = false;
            ddlBusMes.Enabled = true;
            ddlEstado.Enabled = false;
            ddlMes.Enabled = false;

            // Activacion de los Botones
            buttons.Inicializar(gsTabla, botones: botones);
        }

        /// <summary>
        /// Nombre: EstablecerPermisosSistema
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
        /// Modificacion:
        /// </summary>
        private void EstablecerPermisosSistema()
        {
            var permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla );
            foreach (DataGridItem Grilla in dtgMaestro.Items)
            {
                var lkbModificar = (LinkButton)Grilla.FindControl("lkbModificar");
                lkbModificar.Visible = (bool)permisos["UPDATE"];
                
                if (!lkbModificar.Visible )
                    dtgMaestro.Columns[6].Visible = false;
            }
        }

        /// <summary>
        /// Nombre: Nuevo
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
        /// Modificacion:
        /// </summary>
        private void Nuevo()
        {
            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
            imbCrear.Visible = true;
            imbActualiza.Visible = false;
            lblTitulo.Text = lsTitulo;
            TxtAno.Enabled = true;
            ddlMes.Enabled = true;
            TxtIndice.Enabled = true;
            ddlEstado.Enabled = true;
            imbActualiza.Visible = false;
            imbCrear.Visible = true;
            TxtAno.Text = "";
            ddlMes.SelectedValue = "0";
            TxtIndice.Text = "";
        }

        /// <summary>
        /// Nombre: Listar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
        ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
        /// Modificacion:
        /// </summary>
        private void Listar()
        {
            CargarDatos();
            dtgMaestro.Visible = true;
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: Modificar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
        ///              se ingresa por el Link de Modificar.
        /// Modificacion:
        /// </summary>
        /// <param name="modificar"></param>
        private void Modificar(string modificar, string modificar1)
        {
            var lblMensaje = new StringBuilder();

            if (modificar != null && modificar != "")
            {
                try
                {
                    /// Verificar Si el Registro esta Bloqueado
                    if (!manejo_bloqueo("V", modificar, modificar1))
                    {
                        // Carga informacion de combos
                        lConexion.Abrir();
                        lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla , " ano= " + modificar + "  and mes ="+ modificar1 );
                        if (lLector.HasRows)
                        {
                            lLector.Read();
                            TxtAno.Text = lLector["ano"].ToString();
                            try
                            {
                                ddlMes.SelectedValue = lLector["mes"].ToString();
                            }
                            catch (Exception ex)
                            {
                            }
                            TxtIndice.Text = lLector["valor_indice_precios"].ToString();
                            ddlEstado.SelectedValue = lLector["estado"].ToString();
                            imbCrear.Visible = false;
                            imbActualiza.Visible = true;
                            TxtAno.Enabled = false;
                            ddlMes.Enabled = false;
                            TxtIndice.Enabled = true;
                            ddlEstado.Enabled = true;
                            Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                        /// Bloquea el Registro a Modificar
                        manejo_bloqueo("A", modificar, modificar1);
                    }
                    else
                    {
                        Listar();
                        lblMensaje.Append("No se Puede editar el Registro por que está Bloqueado. periodo: " + modificar + '/'+modificar1 );

                    }
                }
                catch (Exception ex)
                {
                    lblMensaje.Append(ex.Message);
                }
            }
            if (lblMensaje.ToString() == "")
            {
                Modal.Abrir(this, CrearRegistro.ID, CrearRegistroInside.ID);
                lblTitulo.Text = lsTitulo;
            }
            else
            {
                Toastr.Error(this, lblMensaje.ToString());
            }
        }

        /// <summary>
        /// Nombre: Buscar
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
        /// Modificacion:
        /// </summary>
        private void Buscar()
        {
            Modal.Cerrar(this, CrearRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// Nombre: CargarDatos
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
        /// Modificacion:
        /// </summary>
        private void CargarDatos()
        {
            string[] lsNombreParametros = { "@P_ano", "@P_mes"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int};
            string[] lValorParametros = { "0", "0"};

            try
            {
                if (TxtBusAno.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtBusAno.Text.Trim();
                if (ddlBusMes.SelectedValue != "0")
                    lValorParametros[1] = ddlBusMes.SelectedValue;

                lConexion.Abrir();
                dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetIndicePreMes", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgMaestro.DataBind();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            Listar();
        }

        /// <summary>
        /// Nombre: imbCrear_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Crear.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbCrear_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_ano", "@P_mes", "@P_valor_indice", "@P_estado", "@P_accion"};
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar};
            string[] lValorParametros = { "0", "0", "0", "","1" };
            var lblMensaje = new StringBuilder();
            decimal ldValor = 0;
            try
            {
                if (TxtAno.Text == "")
                    lblMensaje.Append("Debe ingresar el año<br>");
                else
                {
                    if (TxtAno.Text.Length != 4)
                        lblMensaje.Append("Año no válido<br>");
                }
                if (ddlMes.SelectedValue == "0")
                    lblMensaje.Append("Debe seleccionar el mes<br>");
                if (lblMensaje.ToString() == "")
                {
                    if (VerificarExistencia(" ano= "+TxtAno.Text +" and mes = " + ddlMes.SelectedValue ))
                        lblMensaje.Append("Ya se definió el indice de precios para el periodo seleccionado<br>");
                }
                if (TxtIndice.Text == "")
                    lblMensaje.Append("Debe digitar el indie de precios<br>");
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtIndice.Text.Trim());
                        string sOferta = ldValor.ToString();
                        int iPos = sOferta.IndexOf(".");
                        if (iPos > 0)
                            if (sOferta.Length - iPos > 3)
                                lblMensaje.Append("El inidce de precios tiene mas de 2 decimales<br>");

                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Invalido en el indice de precios<br>");
                    }
                }

                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = TxtAno.Text ;
                    lValorParametros[1] = ddlMes.SelectedValue;
                    lValorParametros[2] = TxtIndice.Text;
                    lValorParametros[3] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetIndicePreMes", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación del indice de precios.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Success(this, "Se creó correctamente el registro.!");
                        Modal.Cerrar(this, CrearRegistro.ID); 
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
        ///              en el Boton Actualizar.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbActualiza_Click(object sender, EventArgs e)
        {
            string[] lsNombreParametros = { "@P_ano", "@P_mes", "@P_valor_indice", "@P_estado", "@P_accion" };
            SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "0", "0", "0", "", "2" };
            var lblMensaje = new StringBuilder();
            decimal ldValor = 0;
            try
            {
                if (TxtIndice.Text == "")
                    lblMensaje.Append("Debe digitar el indie de precios<br>");
                else
                {
                    try
                    {
                        ldValor = Convert.ToDecimal(TxtIndice.Text.Trim());
                        string sOferta = ldValor.ToString();
                        int iPos = sOferta.IndexOf(".");
                        if (iPos > 0)
                            if (sOferta.Length - iPos > 3)
                                lblMensaje.Append("El inidce de precios tiene mas de 2 decimales<br>");

                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Append("Valor Invalido en el indice de precios<br>");
                    }
                }


                if (lblMensaje.ToString() == "")
                {
                    lValorParametros[0] = TxtAno.Text;
                    lValorParametros[1] = ddlMes.SelectedValue;
                    lValorParametros[2] = TxtIndice.Text;
                    lValorParametros[3] = ddlEstado.SelectedValue;
                    lConexion.Abrir();
                    if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetIndicePreMes", lsNombreParametros, lTipoparametros, lValorParametros))
                    {
                        lblMensaje.Append("Se presentó un Problema en la Creación del indice de precios.!");
                        lConexion.Cerrar();
                    }
                    else
                    {
                        manejo_bloqueo("E", TxtAno.Text, ddlMes.SelectedValue );
                        Toastr.Success(this, "El registro se actualizo con éxito!.");
                        Modal.Cerrar(this, CrearRegistro.ID);
                        Listar();
                    }
                }

                if (!string.IsNullOrEmpty(lblMensaje.ToString()))
                    Toastr.Error(this, lblMensaje.ToString());
            }
            catch (Exception ex)
            {
                /// Desbloquea el Registro Actualizado
                manejo_bloqueo("E", TxtAno.Text, ddlMes.SelectedValue);
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: imbActualiza_Click
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
        ///              en el Boton Salir.
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbSalir_Click(object sender, EventArgs e)
        {
            /// Desbloquea el Registro Actualizado
            if (TxtAno.Text != "")
                manejo_bloqueo("E", TxtAno.Text, ddlMes.SelectedValue);
            //Cierra el modal de Agregar
            Modal.Cerrar(this, CrearRegistro.ID);
            Listar();
        }

        /// <summary>
        /// Nombre: dtgComisionista_PageIndexChanged
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dtgMaestro.CurrentPageIndex = e.NewPageIndex;
            CargarDatos();
        }

        /// <summary>
        /// Nombre: dtgComisionista_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
        ///              Link del DataGrid.
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Modificar")
            {
                Modificar(this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text, this.dtgMaestro.Items[e.Item.ItemIndex].Cells[1].Text);
                imbActualiza.Visible = true;
                imbCrear.Visible = false;
            }
        }

        /// <summary>
        /// Nombre: VerificarExistencia
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
        ///              del codigo de la Actividad Exonomica.
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool VerificarExistencia(string lswhere)
        {
            return DelegadaBase.Servicios.ValidarExistencia(gsTabla , lswhere, goInfo);
        }

        /// <summary>
        /// Nombre: manejo_bloqueo
        /// Fecha: Agosto 15 de 2008
        /// Creador: Olga Lucia ibañez
        /// Descripcion: Metodo para validar, crear y borrar los bloqueos
        /// Modificacion:
        /// </summary>
        /// <returns></returns>
        protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro, string lscodigo_registro1)
        {
            string lsCondicion = "nombre_tabla='"+gsTabla +"' and llave_registro='ano=" + lscodigo_registro + "-mes="+ lscodigo_registro1 + "'";
            string lsCondicion1 = "ano=" + lscodigo_registro.ToString()+ "-mes=" + lscodigo_registro1;
            if (lsIndicador == "V")
            {
                return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
            }
            if (lsIndicador == "A")
            {
                a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
                lBloqueoRegistro.nombre_tabla = gsTabla ;
                lBloqueoRegistro.llave_registro = lsCondicion1;
                DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
            }
            if (lsIndicador == "E")
            {
                DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla , lsCondicion1);
            }
            return true;
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string[] lValorParametros = { "0", "0"};
            string lsParametros = "";

            try
            {
                if (TxtBusAno.Text.Trim().Length > 0)
                {
                    lValorParametros[0] = TxtBusAno.Text.Trim();
                    lsParametros += " Año: " + TxtBusAno.Text;
                }
                if (ddlBusMes.SelectedValue != "0")
                {
                    lValorParametros[1] = ddlBusMes.SelectedValue;
                    lsParametros += " Mes: " + ddlBusMes.SelectedItem.ToString();
                }
                Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetIndicePreMes&nombreParametros=@P_ano*@P_mes&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1]  + "&columnas=ano*mes*valor_indice_precios*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de indice precios al productor por mes&TituloParametros=" + lsParametros); 
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        /// <summary>
        /// Nombre: ImgPdf_Click
        /// Fecha: Agosto 15 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgPdf_Click(object sender, EventArgs e)
        {
            try
            {
                string lsCondicion = " estado = 'A'";
                Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla="+gsTabla + "&procedimiento=pa_ValidarExistencia&columnas=ano*mes*valor_indice_precios*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pudo Generar el Informe.!");
            }
        }

        ///// Eventos Nuevos para la Implementracion del UserControl

        /// <summary>
        /// Metodo del Link Nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNuevo(object sender, EventArgs e)
        {
            Nuevo();
        }

        /// <summary>
        /// Metodo del Link Listar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsulta(object sender, EventArgs e)
        {
            Listar();
        }
    }
}