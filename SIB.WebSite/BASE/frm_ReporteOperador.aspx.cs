﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ReporteOperador : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Tipos de Operador por Reporte Tablero de Control";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipoOperador, "m_tipos_operador", " estado = 'A'  order by descripcion", 2, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoOperador, "m_tipos_operador", " estado = 'A' order by descripcion", 2, 1);
            // Campo nuevo tablero de control 20160712
            LlenarControles(lConexion.gObjConexion, ddlReporte, "m_reporte_inf_ope", " 1=1 order by codigo_reporte", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusReporte, "m_reporte_inf_ope", " 1=1 order by codigo_reporte", 0, 1);
            lConexion.Cerrar();
            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_reporte_operador");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[6].Visible = (Boolean)permisos["UPDATE"];
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoOperadorRol.Visible = false;
        LblCodigoOperadorRol.Visible = true;
        LblCodigoOperadorRol.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    ddlTipoOperador.Items.Clear();
                    LlenarControles(lConexion.gObjConexion, ddlTipoOperador, "m_tipos_operador", " estado = 'A'  order by descripcion", 2, 1);
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_reporte_operador", " codigo_reporte_operador = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoOperadorRol.Text = lLector["codigo_reporte_operador"].ToString();
                        TxtCodigoOperadorRol.Text = lLector["codigo_reporte_operador"].ToString();
                        ddlReporte.SelectedValue = lLector["codigo_reporte"].ToString();
                        ddlTipoOperador.SelectedValue = lLector["sigla_tipo_operador"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        // Campo nuevo tablero de control 20160712
                        ddlAplicaTablero.SelectedValue = lLector["aplica_tablero_control"].ToString();
                        ddlTipoOperador.Enabled = false;
                        ddlReporte.Enabled = false;
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoOperadorRol.Visible = false;
                        LblCodigoOperadorRol.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Pozo " + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_reporte_operador", "@P_sigla_tipo_operador", "@P_codigo_reporte" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Char, SqlDbType.Int };
        string[] lValorParametros = { TxtBusOperadorRol.Text, ddlBusTipoOperador.SelectedValue, ddlBusReporte.SelectedValue };
        if (TxtBusOperadorRol.Text == "")
            lValorParametros[0] = "0";
        try
        {
            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetReporteOperador", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_reporte_operador", "@P_codigo_reporte", "@P_descripcion_reporte", "@P_sigla_tipo_operador", "@P_estado", "@P_accion", 
                                        "@P_aplica_tablero_control" // Campo nuevo tablero de control 20160712
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { "0", ddlReporte.SelectedValue, ddlReporte.SelectedItem.ToString(), ddlTipoOperador.SelectedValue, ddlEstado.SelectedValue, "1", 
                                      ddlAplicaTablero.SelectedValue // Campo nuevo tablero de control 20160712
                                    };
        lblMensaje.Text = "";

        try
        {
            if (ddlTipoOperador.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Operador. <br>";
            if (ddlReporte.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Operador. <br>";
            if (VerificarExistencia(" sigla_tipo_operador = '" + ddlTipoOperador.SelectedValue + "' And  codigo_reporte = '" + ddlReporte.SelectedValue + "'"))
                lblMensaje.Text += " Ya está registrado el Tipo de operador para el Reporte Seleccionado<br>";

            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetReporteOperador", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación del registro.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_reporte_operador", "@P_codigo_reporte", "@P_descripcion_reporte", "@P_sigla_tipo_operador", "@P_estado", "@P_accion",
                                         "@P_aplica_tablero_control" // Campo nuevo tablero de control 20160712
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char };
        string[] lValorParametros = { LblCodigoOperadorRol.Text, ddlReporte.SelectedValue, ddlReporte.SelectedItem.ToString(), ddlTipoOperador.SelectedValue, ddlEstado.SelectedValue, "2",
                                      ddlAplicaTablero.SelectedValue // Campo nuevo tablero de control 20160712
                                    };
        lblMensaje.Text = "";
        try
        {
            if (ddlTipoOperador.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Operador. <br>";
            if (ddlReporte.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Operador. <br>";
            if (VerificarExistencia(" codigo_reporte_operador != " + LblCodigoOperadorRol.Text + " And sigla_tipo_operador = '" + ddlTipoOperador.SelectedValue + "' And  codigo_reporte = '" + ddlReporte.SelectedValue + "'"))
                lblMensaje.Text += " Ya está registrado el Tipo de operador para el Reporte Seleccionado<br>";

            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetReporteOperador", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion del Registro.!";
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoOperadorRol.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoOperadorRol.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoOperadorRol.Text != "")
            manejo_bloqueo("E", LblCodigoOperadorRol.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_reporte_operador", lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);
        }

        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_reporte_operador' and llave_registro='codigo_reporte_operador=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_reporte_operador=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_reporte_operador";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_reporte_operador", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0", "0" };
        string lsParametros = "";

        try
        {
            if (TxtBusOperadorRol.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusOperadorRol.Text.Trim();
                lsParametros += " Codigo Reporte : " + TxtBusOperadorRol.Text;

            }
            if (ddlBusTipoOperador.SelectedValue != "0")
            {
                lValorParametros[1] = ddlBusTipoOperador.SelectedValue;
                lsParametros += " - Tipo Operador : " + ddlBusTipoOperador.SelectedItem.ToString();
            }
            if (ddlBusReporte.SelectedValue != "0")
            {
                lValorParametros[2] = ddlBusReporte.SelectedValue;
                lsParametros += " - Tipo Reporte : " + ddlBusReporte.SelectedItem.ToString();
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetReporteOperador&nombreParametros=@P_codigo_reporte_operador*@P_sigla_tipo_operador*@P_codigo_reporte&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_operador_rol&titulo_informe=Listado de Tipos de Operador por Reporte Entidades Controll&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }
    }
}