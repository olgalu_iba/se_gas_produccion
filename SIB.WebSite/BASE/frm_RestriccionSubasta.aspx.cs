﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_RestriccionSubasta : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Restricción por Subasta";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);

        if (!IsPostBack)
        {
            /// Llenar controles
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoSubasta, "m_tipos_subasta", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " codigo_tipo_subasta =0 and estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusTipoRueda, "m_tipos_rueda", " estado = 'A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlModalidad, "m_modalidad_contractual", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlTipoCampo, "m_tipo_campo", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlPeriodoEnt, "m_periodos_entrega", " estado ='A' order by descripcion", 0, 1);
            lConexion.Cerrar();


            // Carga informacion de combos
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_restriccion_subasta");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[8].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[9].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoRestric.Visible = false;
        LblCodigoRestric.Visible = true;
        LblCodigoRestric.Text = "Automatico";
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        string lsModalidad = "";
        string lsPeriodo = "";
        string lsCampo = "";
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_restriccion_subasta", " codigo_restriccion_subasta = " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoRestric.Text = lLector["codigo_restriccion_subasta"].ToString();
                        TxtCodigoRestric.Text = lLector["codigo_restriccion_subasta"].ToString();
                        try
                        {
                            ddlTipoSubasta.SelectedValue = lLector["codigo_tipo_subasta"].ToString();
                            ddlTipoSubasta_SelectedIndexChanged(null, null);
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tipo de subasta del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlTipoRueda.SelectedValue = lLector["codigo_tipo_rueda"].ToString();
                            string asdas = ddlTipoRueda.SelectedValue;
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El tipo de rueda del registro no existe o esta inactivo<br>";
                        }
                        lsModalidad = lLector["codigo_modalidad"].ToString();
                        lsPeriodo = lLector["codigo_periodo"].ToString();
                        lsCampo = lLector["codigo_tipo_campo"].ToString();
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        lLector.Close();
                        lLector.Dispose();
                        try
                        {
                            ddlModalidad.SelectedValue = lsModalidad;
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "La modalilidad de contrato del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlPeriodoEnt.SelectedValue = lsPeriodo;
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El Periodo de Entrega del registro no existe o esta inactivo<br>";
                        }
                        try
                        {
                            ddlTipoCampo.SelectedValue = lsCampo;
                            if (lsCampo == "0")
                                trCampo.Visible = false;
                            else
                                trCampo.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            lblMensaje.Text += "El Tipo de Campo del registro no existe o esta inactivo<br>";
                        }

                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoRestric.Visible = false;
                        LblCodigoRestric.Visible = true;
                        ddlTipoSubasta.Enabled = false;
                        ddlTipoRueda.Enabled = false;
                        ddlModalidad.Enabled = false;
                        ddlTipoCampo.Enabled = false;
                        ddlPeriodoEnt.Enabled = false;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado. Codigo Restricción Subasta " + modificar.ToString();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_restriccion_subasta", "@P_codigo_tipo_subasta", "@P_codigo_tipo_rueda" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", ddlBusTipoSubasta.SelectedValue, ddlBusTipoRueda.SelectedValue };

        try
        {
            if (TxtBusRestric.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusRestric.Text.Trim();

            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetRestriccionSubasta", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_restriccion_subasta", "@P_codigo_tipo_subasta", "@P_codigo_tipo_rueda", "@P_codigo_tipo_campo", "@P_codigo_modalidad", "@P_codigo_periodo", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", ddlTipoSubasta.SelectedValue, ddlTipoRueda.SelectedValue, ddlTipoCampo.SelectedValue, ddlModalidad.SelectedValue, ddlPeriodoEnt.SelectedValue, ddlEstado.SelectedValue, "1" };
        lblMensaje.Text = "";
        int liValor = 0;

        try
        {

            if (VerificarExistencia("m_restriccion_subasta", " codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue + " And codigo_tipo_rueda= '" + ddlTipoRueda.SelectedValue + "' And codigo_periodo = " + ddlPeriodoEnt.SelectedValue + " And codigo_modalidad = " + ddlModalidad.SelectedValue + " And codigo_tipo_campo = " + ddlTipoCampo.SelectedValue + " "))
                lblMensaje.Text += " El Tipo de Subasta, Mercado, Modalidad, Campo y Periodo YA esta parametrizado. <br>";
            if (ddlTipoSubasta.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Subasta. <br>";
            if (ddlTipoRueda.SelectedValue == "0" || ddlTipoRueda.SelectedValue == "")
                lblMensaje.Text += " Debe Seleccionar el Tipo de rueda. <br>";
            if (ddlModalidad.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar la Modalidad Contratual. <br>";
            if (ddlPeriodoEnt.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Periodo. <br>";
            if (trCampo.Visible == true && ddlTipoCampo.SelectedValue == "0")
                lblMensaje.Text += " Debe Seleccionar el Tipo de Campo. <br>";
            if (lblMensaje.Text == "")
            {
                if (VerificarExistencia("m_restriccion_subasta", " codigo_tipo_subasta = " + ddlTipoSubasta.SelectedValue + " And codigo_tipo_rueda= '" + ddlTipoRueda.SelectedValue + "' And codigo_periodo = " + ddlPeriodoEnt.SelectedValue + " And codigo_modalidad = " + ddlModalidad.SelectedValue + " And codigo_tipo_campo = " + ddlTipoCampo.SelectedValue + " "))
                    lblMensaje.Text += " El Tipo de Subasta, Tipo de rueda, Modalidad, Campo y Periodo YA esta parametrizado. <br>";
            }
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRestriccionSubasta", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación de la  Restricción de Subasta.!";
                    lConexion.Cerrar();
                }
                else
                    Listar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_restriccion_subasta", "@P_codigo_tipo_subasta", "@P_codigo_tipo_rueda", "@P_codigo_tipo_campo", "@P_codigo_modalidad", "@P_codigo_periodo", "@P_estado", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { LblCodigoRestric.Text, ddlTipoSubasta.SelectedValue, ddlTipoRueda.SelectedValue, ddlTipoCampo.SelectedValue, ddlModalidad.SelectedValue, ddlPeriodoEnt.SelectedValue, ddlEstado.SelectedValue, "2" };
        lblMensaje.Text = "";
        int liValor = 0;
        try
        {
            if (lblMensaje.Text == "")
            {
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetRestriccionSubasta", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de la Restricción de Subasta.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", LblCodigoRestric.Text);
                    Listar();
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", LblCodigoRestric.Text);
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoRestric.Text != "")
            manejo_bloqueo("E", LblCodigoRestric.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lsTabla, string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(lsTabla, lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_restriccion_subasta' and llave_registro='codigo_restriccion_subasta=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_restriccion_subasta=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_restriccion_subasta";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_restriccion_subasta", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusRestric.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusRestric.Text.Trim();
                lsParametros += " Codigo Restriccion Subasta : " + TxtBusRestric.Text;

            }
            if (ddlBusTipoSubasta.SelectedValue != "0")
            {
                lValorParametros[1] = ddlBusTipoSubasta.SelectedValue;
                lsParametros += " - Tipo Subasta: " + ddlBusTipoSubasta.SelectedItem.ToString();
            }
            if (ddlBusTipoRueda.SelectedValue != "")
            {
                lValorParametros[2] = ddlBusTipoRueda.SelectedValue.Trim();
                lsParametros += " - Tipo Rueda: " + ddlBusTipoRueda.SelectedItem.ToString();
            }
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetRestriccionSubasta&nombreParametros=@P_codigo_restriccion_subasta*@P_codigo_tipo_subasta*@P_codigo_tipo_rueda&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "*" + lValorParametros[2] + "&columnas=codigo_tipo_subasta&titulo_informe=Listado de Restricción de Subastas&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }


    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_restriccion_subasta <> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_restriccion_subasta&procedimiento=pa_ValidarExistencia&columnas=codigo_restriccion_subasta*codigo_tipo_subasta*tipo_mercado*codigo_modalidad*codigo_tipo_campo*codigo_periodo*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pude Generar el Informe.!";
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoSubasta_SelectedIndexChanged(object sender, EventArgs e)
    {
        lConexion.Abrir();
        ddlTipoRueda.Items.Clear();
        LlenarControles(lConexion.gObjConexion, ddlTipoRueda, "m_tipos_rueda", " estado ='A' and codigo_tipo_subasta= " + ddlTipoSubasta.SelectedValue + " order by descripcion", 0, 1);
        lConexion.Cerrar();
        if (ddlTipoSubasta.SelectedValue != "5")
            trCampo.Visible = false;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlTipoRueda_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlTipoCampo.SelectedValue = "0";
        if (VerificarExistencia("m_tipos_rueda", " codigo_tipo_subasta = 5 and codigo_tipo_rueda= " + ddlTipoRueda.SelectedValue + " and tipo_mercado='P' and destino_rueda ='G'"))
            trCampo.Visible = true;
        else
            trCampo.Visible = false;
    }
}