﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_AprobRegOpe1.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="BASE_frm_AprobRegOpe1" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <%--Mensaje--%>
            <div class="kt-portlet__body" runat="server" id="tblMensaje">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <%--Grilla--%>
            <div class="kt-portlet__body" runat="server" id="tblgrilla">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="btnAprobar" runat="server" CssClass="btn btn-success" Text="Aprobar" OnClick="btnAprobar_Click" ValidationGroup="detalle" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False"
                            Width="100%" CssClass="table-bordered" OnEditCommand="dtgMaestro_EditCommand">
                            <Columns>
                                <asp:TemplateColumn HeaderText="Apr">
                                    <ItemTemplate>
                                        <label class="kt-checkbox">
                                            <asp:CheckBox ID="ChkAprobar" runat="server" />
                                            <span></span>
                                        </label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="codigo_operador" HeaderText="Cod" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_documento" HeaderText="No. doc" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="digito_verif" HeaderText="DV" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                <asp:BoundColumn DataField="razon_social" HeaderText="Razon social" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tipo" HeaderText="tpo operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Comunicado" EditText="Comunicado" ItemStyle-HorizontalAlign="Center"></asp:EditCommandColumn>
                                <asp:EditCommandColumn HeaderText="Sarlaf" EditText="Sarlaf" ItemStyle-HorizontalAlign="Center"></asp:EditCommandColumn>
                                <asp:BoundColumn DataField="ruta_archivo_comunicado" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ruta_archivo_sarlaf" Visible="false"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar" ItemStyle-HorizontalAlign="Center"></asp:EditCommandColumn>
                                <asp:BoundColumn DataField="tiene_usuario" Visible="false"></asp:BoundColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
