﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParamGarantiaGestor.aspx.cs"
    Inherits="BASE_frm_ParamGarantiaGestor" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">Fecha de pago para carta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaPago" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaPago" runat="server" ErrorMessage="Debe ingresar la fecha de pago (carta)"
                    ControlToValidate="TxtFechaPago" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">Fecha de contratos para carta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaCont" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                
                <asp:RequiredFieldValidator ID="rfvTxtFechaCont" runat="server" ErrorMessage="Debe ingresar la fecha de selección de contratos (carta)"
                    ControlToValidate="TxtFechaCont" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha límite para constitución de garantías (carta)
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaCons" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                
                <asp:RequiredFieldValidator ID="RfvTxtFechaCons" runat="server" ErrorMessage="Debe ingresar la fecha de constitución de garantías (carta)"
                    ControlToValidate="TxtFechaCons" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
            <td class="td1">Fecha límite aprobación de garantías (carta)
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtFechaAprob" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaAprob" runat="server" ErrorMessage="Debe ingresar la fecha de aprobación de garantías (carta)"
                    ControlToValidate="TxtFechaAprob" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Fecha para liberación de garantías (carta)
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtFechaLib" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtFechaLib" runat="server" ErrorMessage="Debe ingresar la fecha de liberación de garantías (carta)"
                    ControlToValidate="TxtFechaLib" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Texto párrafo 1 de la carta
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtParraf1" runat="server" MaxLength="200" Width="800px" TextMode="MultiLine"
                    Rows="3"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtParraf1" runat="server" ErrorMessage="Debe ingresar el texto del parráfo 1 de la carta"
                    ControlToValidate="TxtParraf1" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Texto párrafo 2 de la carta
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtParraf2" runat="server" MaxLength="200" Width="800px" TextMode="MultiLine"
                    Rows="3"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtParraf2" runat="server" ErrorMessage="Debe ingresar el texto del parráfo 2 de la carta"
                    ControlToValidate="TxtParraf2" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Texto párrafo 3 de la carta
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtParraf3" runat="server" MaxLength="200" Width="800px" TextMode="MultiLine"
                    Rows="3"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtParraf3" runat="server" ErrorMessage="Debe ingresar el texto del parráfo 3 de la carta"
                    ControlToValidate="TxtParraf3" ValidationGroup="VsParametros"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="td1">Observación Cambio
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                    ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
