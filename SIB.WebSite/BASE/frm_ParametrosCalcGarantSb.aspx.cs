﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParametrosCalcGarantSb : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Cálculo Garantías Subasta Bimestral ";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string sRutaArc = ConfigurationManager.AppSettings["RutaIMG"].ToString();
    string gsTabla = "m_param_calculo_garant_sb";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    //private void EstablecerPermisosSistema()
    //{
    //    Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
    //    imbCrear.Visible = (Boolean)permisos["INSERT"];
    //    imbActualiza.Visible = (Boolean)permisos["UPDATE"];
    //}
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtPerioVentaDef.Text = lLector["precio_venta_defecto"].ToString();
                    TxtPrecioCompraDef.Text = lLector["precio_compra_defecto"].ToString();
                    TxtCantidadCompraDef.Text = lLector["cantidad_compra_defecto"].ToString();  //20161125 varibales garantias
                    TxtCantidadVentaDef.Text = lLector["cantidad_venta_defecto"].ToString();//20161125 varibales garantias
                    TxtConstante1.Text = lLector["constante1"].ToString();
                    TxtConstante2.Text = lLector["constante2"].ToString();
                    TxtConstante3.Text = lLector["constante3"].ToString();
                    TxtNoMesOyDCompra.Text = lLector["no_mes_obtener_oyd_compra"].ToString();
                    TxtNoDiasParticipa.Text = lLector["no_dias_hab_public_participa"].ToString();
                    TxtNoDiasCumpliMes1.Text = lLector["no_dias_hab_public_cumpli_mes1"].ToString();
                    TxtNoDiasCumpliMes2.Text = lLector["no_dias_hab_public_cumpli_mes2"].ToString();
                    TxtAnoIndice.Text = lLector["ano_indice_precio_oferta"].ToString();
                    TxtMesIndice.Text = lLector["mes_indice_precio_oferta"].ToString();
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado.";

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_precio_venta_defecto","@P_precio_compra_defecto","@P_cantidad_venta_defecto","@P_cantidad_compra_defecto",
                                        "@P_constante1","@P_constante2","@P_constante3","@P_no_mes_obtener_oyd_compra","@P_no_dias_hab_public_participa",
                                        "@P_no_dias_hab_public_cumpli_mes1","@P_no_dias_hab_public_cumpli_mes2","@P_ano_indice_precio_oferta","@P_mes_indice_precio_oferta",
                                        "@P_observaciones","@P_accion"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Int, SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int, SqlDbType.Int,SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int
                                      };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "", "1" };
        lblMensaje.Text = "";
        try
        {
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtPerioVentaDef.Text.Trim();
                lValorParametros[1] = TxtPrecioCompraDef.Text.Trim();
                lValorParametros[2] = TxtCantidadVentaDef.Text.Trim();
                lValorParametros[3] = TxtCantidadCompraDef.Text.Trim();
                lValorParametros[4] = TxtConstante1.Text.Trim();
                lValorParametros[5] = TxtConstante2.Text.Trim();
                lValorParametros[6] = TxtConstante3.Text.Trim();
                lValorParametros[7] = TxtNoMesOyDCompra.Text.Trim();
                lValorParametros[8] = TxtNoDiasParticipa.Text.Trim();
                lValorParametros[9] = TxtNoDiasCumpliMes1.Text.Trim();
                lValorParametros[10] = TxtNoDiasCumpliMes2.Text.Trim();
                lValorParametros[11] = TxtAnoIndice.Text.Trim();
                lValorParametros[12] = TxtMesIndice.Text.Trim();
                lValorParametros[13] = TxtObservacion.Text.Trim();
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosCalGarSb", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación de Parámetros Cálculo Garantias Sb.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");

                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_precio_venta_defecto","@P_precio_compra_defecto","@P_cantidad_venta_defecto","@P_cantidad_compra_defecto",
                                        "@P_constante1","@P_constante2","@P_constante3","@P_no_mes_obtener_oyd_compra","@P_no_dias_hab_public_participa",
                                        "@P_no_dias_hab_public_cumpli_mes1","@P_no_dias_hab_public_cumpli_mes2","@P_ano_indice_precio_oferta","@P_mes_indice_precio_oferta",
                                        "@P_observaciones","@P_accion"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Int, SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Int, SqlDbType.Int,SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.Int,SqlDbType.VarChar,SqlDbType.Int
                                      };
        string[] lValorParametros = { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "", "2" };
        lblMensaje.Text = "";
        try
        {
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtPerioVentaDef.Text.Trim();
                lValorParametros[1] = TxtPrecioCompraDef.Text.Trim();
                lValorParametros[2] = TxtCantidadVentaDef.Text.Trim();
                lValorParametros[3] = TxtCantidadCompraDef.Text.Trim();
                lValorParametros[4] = TxtConstante1.Text.Trim();
                lValorParametros[5] = TxtConstante2.Text.Trim();
                lValorParametros[6] = TxtConstante3.Text.Trim();
                lValorParametros[7] = TxtNoMesOyDCompra.Text.Trim();
                lValorParametros[8] = TxtNoDiasParticipa.Text.Trim();
                lValorParametros[9] = TxtNoDiasCumpliMes1.Text.Trim();
                lValorParametros[10] = TxtNoDiasCumpliMes2.Text.Trim();
                lValorParametros[11] = TxtAnoIndice.Text.Trim();
                lValorParametros[12] = TxtMesIndice.Text.Trim();
                lValorParametros[13] = TxtObservacion.Text.Trim();
                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosCalGarSb", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de los Parámetros Cálculo Garantias Sb .!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistenciaSib", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_param_calculo_garant_sb' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}