﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_operadorAdc.aspx.cs" EnableEventValidation="false" ValidateRequest="false" Inherits="BASE.frm_operadorAdc" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server"></asp:Label>
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Nit Operador" AssociatedControlID="TxtBusNit" runat="server" />
                            <asp:TextBox ID="TxtBusNit" runat="server" autocomplete="off" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Razón social" AssociatedControlID="TxtBusRazon" runat="server" />
                            <asp:TextBox ID="TxtBusRazon" runat="server" autocomplete="off" CssClass="form-control" />
                        </div>
                    </div>

                </div>
                <%--Grilla--%>
                <div class="table table-responsive">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DataGrid ID="dtgMaestro" Visible="False" AutoGenerateColumns="False" AllowPaging="True" Width="100%" CssClass="table-bordered" PageSize="10"
                                OnItemCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged" runat="server">
                                <Columns>
                                    <asp:BoundColumn DataField="nit_operador" HeaderText="Nit Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="razon_social" HeaderText="Razón Social" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="nombres" HeaderText="Nombres" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="apellidos" HeaderText="Nombres" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="trato" HeaderText="Trato" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="trato2" HeaderText="Trato 2" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="cargo" HeaderText="Cargo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="mail_facturacion" HeaderText="Mail facturación" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Acción" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="flaticon-more-1"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-md dropdown-menu-fit" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-226px, -34px, 0px);">
                                                    <!--begin::Nav-->
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <ul class="kt-nav">
                                                                <li class="kt-nav__item">
                                                                    <asp:LinkButton ID="lkbModificar" CssClass="kt-nav__link" CommandName="Modificar" runat="server">
                                                            <i class="kt-nav__link-icon flaticon2-contract"></i>
                                                            <span class="kt-nav__link-text">Modificar</span>
                                                                    </asp:LinkButton>
                                                                </li>

                                                            </ul>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <!--end::Nav-->
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" Position="TopAndBottom" HorizontalAlign="Center" />
                                <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                            </asp:DataGrid>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%--Modals--%>
    <%--Crear Registro--%>
    <div class="modal fade" id="CrearRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlCrearRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="CrearRegistroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlCrearRegistroLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="kt-portlet__body" runat="server">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Nombres Representante Legal" AssociatedControlID="TxtNombre" runat="server" />
                                            <asp:TextBox ID="TxtNombre" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Apellidos Representante Legal" AssociatedControlID="TxtApellido" runat="server" />
                                            <asp:TextBox ID="TxtApellido" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Trato (Señor/Señora)" AssociatedControlID="TxtTrato" runat="server" />
                                            <asp:TextBox ID="TxtTrato" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Trato 2 (Respetado/ Respetada)" AssociatedControlID="TxtTrato2" runat="server" />
                                            <asp:TextBox ID="TxtTrato2" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Cargo" AssociatedControlID="TxtCargo" runat="server" />
                                            <asp:TextBox ID="TxtCargo" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="mail facturación" AssociatedControlID="TxtMail" runat="server" />
                                            <asp:TextBox ID="TxtMail" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6 col-lg-4">
                                        <div class="form-group">
                                            <asp:Label Text="Estado" AssociatedControlID="ddlEstado" runat="server" />
                                            <asp:DropDownList ID="ddlEstado" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem Value="A">Activo</asp:ListItem>
                                                <asp:ListItem Value="I">Inactivo</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="imbSalir" runat="server" class="btn btn-secondary" Text="Cancelar" CausesValidation="false" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" OnClick="imbSalir_Click" />
                            <asp:Button ID="imbCrear" runat="server" CssClass="btn btn-success" Text="Crear" OnClick="imbCrear_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            <asp:Button ID="imbActualiza" runat="server" CssClass="btn btn-success" Text="Actualizar" OnClick="imbActualiza_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
