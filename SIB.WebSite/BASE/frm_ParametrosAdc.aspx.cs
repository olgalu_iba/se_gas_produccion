﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParametrosAdc : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Adicionales";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string gsTabla = "m_parametros_adc";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            Modificar();
        }
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtDiaIniCon.Text = lLector["dia_hab_ini_ing_conexion"].ToString();
                    TxtDiaFinCon.Text = lLector["dia_hab_fin_ing_conexion"].ToString();
                    TxtDiaPubCon.Text = lLector["dia_hab_pub_conexion"].ToString();
                    TxtDiaIndFact.Text = lLector["dias_ind_fact"].ToString(); //20210317 ind factura
                    TxtDiaEjeFact.Text = lLector["dias_alerta_fact"].ToString();//20210317 ind factura
                    ///20210804 modificacion ptdvf
                    try
                    {
                        TxtFecIniPtdvf.Text = Convert.ToDateTime(lLector["fecha_ini_ptdvf"].ToString()).ToString("yyyy/MM/dd");
                    }
                    catch (Exception ex)
                    {
                        TxtFecIniPtdvf.Text = "";
                    }
                    try
                    {
                        TxtFecFinPtdvf.Text = Convert.ToDateTime(lLector["fecha_fin_ptdvf"].ToString()).ToString("yyyy/MM/dd");
                    }
                    catch (Exception ex)
                    {
                        TxtFecFinPtdvf.Text = "";
                    }
                    try
                    {
                        TxtFecModPtdvf.Text = Convert.ToDateTime(lLector["fecha_mod_ptdvf"].ToString()).ToString("yyyy/MM/dd");
                    }
                    catch (Exception ex)
                    {
                        TxtFecModPtdvf.Text = "";
                    }
                    //////20210804 finmodificacion ptdvf
                    /////20220202 verifica operativa
                    txtPorcEnt.Text = lLector["porc_error_entrada"].ToString();
                    txtPorcSal.Text = lLector["porc_error_salida"].ToString();
                    TxtHoraCorreoVerIny.Text = lLector["hora_correo_ver_iny"].ToString();
                    try
                    {
                        ddlPerCorreoVerIny.SelectedValue = lLector["periodicidad_correo_iny"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    txtDiaHabCorreoVerIny.Text = lLector["dia_habil_correo_iny"].ToString();
                    TxtHoraCorreoVerTom.Text = lLector["hora_correo_ver_tom"].ToString();
                    try
                    {
                        ddlPerCorreoVerTom.SelectedValue = lLector["periodicidad_correo_tom"].ToString();
                    }
                    catch (Exception ex)
                    { }
                    txtDiaHabCorreoVerTom.Text = lLector["dia_habil_correo_tom"].ToString();
                    //20220202 fin verifica operativa
                    TxtIntCont.Text = lLector["int_fall_contr"].ToString(); //20220211 contraseña
                    TxtMinutDesb.Text = lLector["minut_desbl_contr"].ToString(); //20220211 contraseña
                    TxtContHist.Text = lLector["cant_contr_hist"].ToString(); //20220211 contraseña
                    TxtDiaTarTra.Text = lLector["dia_carga_tarifa"].ToString(); //20220314 tarifas de transporte
                    TxtDiaDesist.Text = lLector["dias_desistimiento"].ToString(); //20220505 desistimiento
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado.";

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_dia_hab_ini_ing_conexion", "@P_dia_hab_fin_ing_conexion", "@P_dia_hab_pub_conexion", "@P_observaciones", "@P_dias_ind_fact", "@P_dias_alerta_fact", //20210319 indicador facrtura
                         "@P_fecha_ini_ptdvf", "@P_fecha_fin_ptdvf", "@P_fecha_mod_ptdvf", //20210804 modificacion ptdvf
                         "@P_porc_error_entrada", "@P_porc_error_salida", "@P_hora_correo_ver_iny","@P_periodicidad_correo_iny","@P_dia_habil_correo_iny",//20220202 verifica operativa
                         "@P_hora_correo_ver_tom","@P_periodicidad_correo_tom","@P_dia_habil_correo_tom",//20220202 verifica operativa
                         "@P_int_fall_contr","@P_minut_desbl_contr","@P_cant_contr_hist", //20220211 contraseña
                          "@P_dia_carga_tarifa", "@P_dias_desistimiento"}; //20220314 tarifa de transporte //20220505 desistimiento
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int,  //20210319 indicador facrtura
                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,//20210804 modificacion ptdvf
                        SqlDbType.Decimal,SqlDbType.Decimal,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.Int,//20220202 verifica operativa
                        SqlDbType.Int,SqlDbType.Int,SqlDbType.Int,  //20220211 contraseña
                         SqlDbType.Int, SqlDbType.Int}; //20220314 tarifa de transporte //20220505 desistimiento
        string[] lValorParametros = { "0", "0", "0", "", "0", "0", "", "", "", "0", "0", "", "", "0", "", "", "0", "0", "0", "0", //20210319 indicador facrtura //20210804 modificacion ptdvf //20220211
                                    TxtDiaTarTra.Text, TxtDiaDesist.Text };//20220314 tarifa de transporte //20220505 desistimiento
                        
        lblMensaje.Text = "";
        int liIni = 0;
        int liFin = 0;
        int liPub = 0;

        try
        {
            if (TxtDiaIniCon.Text == "")
                lblMensaje.Text += "Debe ingresar los días hábiles inicio ingreso información operativa gasoducto de conexión. <br>";
            else
            {
                liIni = Convert.ToInt32(TxtDiaIniCon.Text);
                if (liIni <= 0)
                    lblMensaje.Text += "Los días hábiles inicio ingreso información operativa gasoducto de conexión deben ser mayores que cero<br>";
            }
            if (TxtDiaFinCon.Text == "")
                lblMensaje.Text += "Debe ingresar los días hábiles fifin ingreso información operativa gasoducto de conexión. <br>";
            else
            {
                liFin = Convert.ToInt32(TxtDiaFinCon.Text);
                if (liFin <= 0)
                    lblMensaje.Text += "Los días hábiles fin ingreso información operativa gasoducto de conexión deben ser mayores que cero<br>";
            }
            if (TxtDiaPubCon.Text == "")
                lblMensaje.Text += "Debe ingresar los días hábiles publicación información operativa gasoducto de conexión. <br>";
            else
            {
                liPub = Convert.ToInt32(TxtDiaPubCon.Text);
                if (liPub <= 0)
                    lblMensaje.Text += "Los días hábiles publicación información operativa gasoducto de conexión deben ser mayores que cero<br>";
            }
            if (lblMensaje.Text == "")
            {
                if (liIni > liFin)
                    lblMensaje.Text += "Los días hábiles inicio ingreso información operativa gasoducto de conexión deben ser menores o iguales que los días finales<br>";
                if (liFin >= liPub)
                    lblMensaje.Text += "Los días hábiles publicación información operativa gasoducto de conexión deben ser mayores que los días finales de ingreso<br>";
            }
            //20210317 indicador factrua
            if (TxtDiaIndFact.Text == "")
                lblMensaje.Text += "Debe ingresar los días hábiles de la fecha de facturación para el control del indicador. <br>";
            else
            {
                liFin = Convert.ToInt32(TxtDiaFinCon.Text);
                if (liFin <= 0)
                    lblMensaje.Text += "Los días hábiles de la fecha de facturación para el control del indicador deben ser mayores que cero<br>";
            }
            //20210317 indicador factrua
            if (TxtDiaEjeFact.Text == "")
                lblMensaje.Text += "Debe ingresar los días hábiles para envío de correo de recordatorio de proceso de facturación. <br>";
            else
            {
                liFin = Convert.ToInt32(TxtDiaEjeFact.Text);
                if (liFin <= 0)
                    lblMensaje.Text += "Los días hábiles para envío de correo de recordatorio de proceso de facturación deben ser mayores que cero<br>";
            }
            //20210804 modificacion ptdvf
            if (TxtFecIniPtdvf.Text == "")
                lblMensaje.Text += "Debe ingresar la fecha inicial de ingreso de la Ptdvf<br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFecIniPtdvf.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "La fecha inicial de ingreso de la Ptdvf no es válida<br>";
                }
            }
            //20210804 modificacion ptdvf
            if (TxtFecFinPtdvf.Text == "")
                lblMensaje.Text += "Debe ingresar la fecha final de ingreso de la Ptdvf<br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFecFinPtdvf.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "La fecha final de ingreso de la Ptdvf no es válida<br>";
                }
            }
            //20210804 modificacion ptdvf
            if (TxtFecModPtdvf.Text == "")
                lblMensaje.Text += "Debe ingresar la fecha máxima de modificación de la Ptdvf<br>";
            else
            {
                try
                {
                    Convert.ToDateTime(TxtFecModPtdvf.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "La fecha máxima de modificación de la Ptdvf no es válida<br>";
                }
            }
            //20220202 verifica operativa
            if (TxtHoraCorreoVerIny.Text == "")
                lblMensaje.Text += "Debe ingresar la hora de envío de correo de verificación inyectada Vs recibida. <br>";
            //20220202 verifica operativa
            if (txtDiaHabCorreoVerIny.Text == "")
                lblMensaje.Text += "Debe ingresar el día hábil de envío de correo de verificación inyectada Vs recibida. <br>";
            //20220202 verifica operativa
            if (TxtHoraCorreoVerTom.Text == "")
                lblMensaje.Text += "Debe ingresar la hora de envío de correo de verificación tomada Vs entrega usuario final. <br>";
            //20220202 verifica operativa
            if (txtDiaHabCorreoVerTom.Text == "")
                lblMensaje.Text += "Debe ingresar el día hábil de envío de correo de verificación tomada vs entrega usuario final. <br>";
            //20220202 verifica operativa
            if (txtPorcEnt.Text == "")
                lblMensaje.Text += "Debe ingresar el porcentaje general de error en verificación de puntos de entrada. <br>";
            else
            {
                try
                {
                    Convert.ToDecimal(txtPorcEnt.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Porcentaje general de error en verificación de puntos de entrada no válido. <br>";
                }
            }
            //20220202 verifica operativa
            if (txtPorcSal.Text == "")
                lblMensaje.Text += "Debe ingresar el porcentaje general de error en verificación de puntos de salida. <br>";
            else
            {
                try
                {
                    Convert.ToDecimal(txtPorcSal.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Porcentaje general de error en verificación de puntos de salida no válido. <br>";
                }
            }
            //20220211 contraseña
            if (TxtIntCont.Text == "" || TxtIntCont.Text == "0")
                lblMensaje.Text += "Debe ingresar el número de intentos fallidos para bloqueo de contraseña<br>";
            if (TxtMinutDesb.Text == "" || TxtMinutDesb.Text == "0")
                lblMensaje.Text += "Debe ingresar los minutos para desbloqueo automático de contraseña. <br>";
            if (TxtContHist.Text == "" || TxtContHist.Text == "0")
                lblMensaje.Text += "Debe ingresar la cantidad de contraseñas historicas. <br>";
            //20220211 frin contraseña
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";
            //20220314 tarifas de transporte
            if (TxtDiaTarTra.Text == "")
                lblMensaje.Text += " Debe digitar los días para la carga de tarifas de transporte. <br>";
            //20220505 desistimiento
            if (TxtDiaDesist.Text == "")
                lblMensaje.Text += " Debe digitar los días aprobación del desistimiento. <br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtDiaIniCon.Text.Trim();
                lValorParametros[1] = TxtDiaFinCon.Text.Trim();
                lValorParametros[2] = TxtDiaPubCon.Text.Trim();
                lValorParametros[3] = TxtObservacion.Text.Trim();
                lValorParametros[4] = TxtDiaIndFact.Text.Trim(); //20210317 ind factruacion
                lValorParametros[5] = TxtDiaEjeFact.Text.Trim();//20210317 ind factruacion
                lValorParametros[6] = TxtFecIniPtdvf.Text.Trim();//20210804 modificacion ptdvf
                lValorParametros[7] = TxtFecFinPtdvf.Text.Trim();//20210804 modificacion ptdvf
                lValorParametros[8] = TxtFecModPtdvf.Text.Trim();//20210804 modificacion ptdvf
                lValorParametros[9] = txtPorcEnt.Text.Trim();//20220202 verifca operativa
                lValorParametros[10] = txtPorcSal.Text.Trim();//20220202 verifca operativa
                lValorParametros[11] = TxtHoraCorreoVerIny.Text.Trim();//20220202 verifca operativa
                lValorParametros[12] = ddlPerCorreoVerIny.SelectedValue;//20220202 verifca operativa
                lValorParametros[13] = txtDiaHabCorreoVerIny.Text.Trim();//20220202 verifca operativa
                lValorParametros[14] = TxtHoraCorreoVerTom.Text.Trim();//20220202 verifca operativa
                lValorParametros[15] = ddlPerCorreoVerTom.SelectedValue;//20220202 verifca operativa
                lValorParametros[16] = txtDiaHabCorreoVerTom.Text.Trim();//20220202 verifca operativa
                lValorParametros[17] = TxtIntCont.Text.Trim(); //20220211 contraseña
                lValorParametros[18] = TxtMinutDesb.Text.Trim();//20220211 contraseña
                lValorParametros[19] = TxtContHist.Text.Trim();//20220211 contraseña

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosAdc", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de los Parametros adicionales.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Parametros/frm_parametros.aspx?idParametros=999"); //20200727 
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// </summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_adc' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}