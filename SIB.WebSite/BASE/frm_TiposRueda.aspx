﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_TiposRueda.aspx.cs"
    Inherits="BASE_frm_TiposRueda" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_TiposRueda.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_TiposRueda.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_TiposRueda.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=3">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">Código Tipo Rueda
            </td>
            <td class="td2" style="width: 25%;">
                <asp:TextBox ID="TxtCodigoTipoRuedas" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoTipoRueda" runat="server" Visible="False"></asp:Label>
            </td>
            <td class="td1">Tipo Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoSubasta" runat="server" OnSelectedIndexChanged="ddlTipoSubasta_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1" colspan="1">Descripción
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="500px"></asp:TextBox>
                <%-- <asp:RegularExpressionValidator ID="RevTxtDescripcion" ControlToValidate="TxtDescripcion"
                    runat="server" ValidationExpression="^([0-9a-zA-Z]{3})([0-9a-zA-Z .-_&])*$" ErrorMessage="Valor muy corto en el Campo Descripcion"
                    ValidationGroup="comi"> * </asp:RegularExpressionValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="td1">Destino Rueda
            </td>
            <td class="td2">
                <%--20201207--%>
                <asp:DropDownList ID="ddlDestino" runat="server" OnSelectedIndexChanged="ddlDestino_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="G">Gas</asp:ListItem>
                    <asp:ListItem Value="T">Transporte</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Tipo Mercado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoMercado" runat="server">
                    <asp:ListItem Value="P">Primario</asp:ListItem>
                    <asp:ListItem Value="S">Secundario</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="TrHora" runat="server" visible="false">
            <td class="td1" colspan="1">Hora Apertura Rueda
            </td>
            <td class="td2" colspan="3">
                <asp:TextBox ID="TxtHoraApertura" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraApertura" ControlToValidate="TxtHoraApertura"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de apertura de la rueda"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr id="TrModCont" runat="server" visible="false">
            <td class="td1">Permite Modificación Manual de Contratos
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlModifContratos" runat="server">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1" colspan="1">Horas Para Modificar Contratos
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniCont" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <%--20201207--%>
                <%--<asp:RequiredFieldValidator ID="RfvtxtHoraIniCont" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio de modificación de contratos"
                    ControlToValidate="txtHoraIniCont" ValidationGroup="comi"> * </asp:RequiredFieldValidator>--%>
                <asp:RegularExpressionValidator ID="revtxtHoraIniCont" ControlToValidate="txtHoraIniCont"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio de modificacion contratos"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinCont" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <%--20201207--%>
                <%--<asp:RequiredFieldValidator ID="rfvtxtHoraFinCont" runat="server" ErrorMessage="Debe Ingresar la Hora de final de modificación de contratos"
                    ControlToValidate="txtHoraFinCont" ValidationGroup="comi"> * </asp:RequiredFieldValidator>--%>
                <asp:RegularExpressionValidator ID="revtxtHoraFinCont" ControlToValidate="txtHoraFinCont"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  de modificacion contratos"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20201207--%>
        <tr runat="server" visible="false" id="TrUvcp00">
            <td class="td1" colspan="1">Tipo de rueda
            </td>
            <td class="td2" colspan="3">
                <asp:DropDownList ID="ddlTipoRuedaTra" runat="server" OnSelectedIndexChanged="ddlTipoRuedaTra_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="R">Rutas</asp:ListItem>
                    <asp:ListItem Value="T">Tramos</asp:ListItem>
                </asp:DropDownList>
            </td>

        </tr>
        <tr runat="server" visible="false" id="TrUvcp01">
            <td class="td1" colspan="1">Horas Para Declarar Información
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniDecInf" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraIniDecInf" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio de declaración de información"
                    ControlToValidate="txtHoraIniDecInf" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraIniDecInf" ControlToValidate="txtHoraIniDecInf"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio de declaracion de informacion"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinDecInf" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraFinDecInf" runat="server" ErrorMessage="Debe Ingresar la Hora final de delcaracion de informacion"
                    ControlToValidate="txtHoraFinDecInf" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraFinDecInf" ControlToValidate="txtHoraFinDecInf"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  de declaración de informacion"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Horas Para Declaración de precio de reserva y cantidad no disponible
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniPre" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraIniPre" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio de declaración de precio de reserva y cantidad no disponible"
                    ControlToValidate="txtHoraIniPre" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraIniPre" ControlToValidate="txtHoraIniPre"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio de declaracion de precio de reserva y cantidad no disponible"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinPre" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraFinPre" runat="server" ErrorMessage="Debe Ingresar la Hora final de declaracion de precio de reserva y cantidad no disponible"
                    ControlToValidate="txtHoraFinPre" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraFinPre" ControlToValidate="txtHoraFinPre"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  de declaración de precio de reserva y cantidad no disponible"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" visible="false" id="TrUvcp02">
            <td class="td1" colspan="1">Horas Para Publicación Ofertas de Venta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniPub" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraIniPub" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio de publicación de ofertaas de venta"
                    ControlToValidate="txtHoraIniPub" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniPub" ControlToValidate="txtHoraIniPub"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio de publicacion de ofertas de venta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinPub" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraFinPub" runat="server" ErrorMessage="Debe Ingresar la Hora final de publicacion de ofertas de venta"
                    ControlToValidate="txtHoraFinPub" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraFinPub" ControlToValidate="txtHoraFinPub"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  de publicacíón de ofertas de venta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Para Ingreso de ofertas de compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniComp" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rftxtHoraIniComp" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio de ingreso de posturas de compra"
                    ControlToValidate="txtHoraIniComp" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraIniComp" ControlToValidate="txtHoraIniComp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final de ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinComp" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraFinComp" runat="server" ErrorMessage="Debe Ingresar la Hora final de ingreso de posturas de compra"
                    ControlToValidate="txtHoraFinComp" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraFinComp" ControlToValidate="txtHoraFinComp"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" visible="false" id="TrUvcp03">
            <td class="td1" colspan="1">Horas Para Calce de posturas
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniCalce" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraIniCalce" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio de calce"
                    ControlToValidate="txtHoraIniCalce" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraIniCalce" ControlToValidate="txtHoraIniCalce"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio de calce"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinCalce" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvtxtHoraFinCalce" runat="server" ErrorMessage="Debe Ingresar la Hora final de calce"
                    ControlToValidate="txtHoraFinCalce" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtHoraFinCalce" ControlToValidate="txtHoraFinCalce"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final de calce de operaciones"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">No. Posturas de Compra Uselo Corto Plazo
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNoPostCompra" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtNoPostCompra" runat="server" TargetControlID="TxtNoPostCompra"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSmpsi01" visible="false">
            <td class="td1" colspan="1">No. Mes Previo Ingreso Vendedores Suministro sin Interrupciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtMesPrevVendSmpsi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtMesPrevVendSmpsi" runat="server" TargetControlID="TxtMesPrevVendSmpsi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">Tiempo Duración Ronda Suministro sin Interrupciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDuraRondaSmpsi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtDuraRondaSmpsi" runat="server" TargetControlID="TxtDuraRondaSmpsi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSmpsi02" visible="false">
            <td class="td1" colspan="1">No. Días Declara Oferta Vendedor Suministro sin Interrupciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDiasDecOfVSmpsi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtDiasDecOfVSmpsi" runat="server" TargetControlID="TxtDiasDecOfVSmpsi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">No. Días Declara Oferta Comprador Suministro sin Interrupciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDiasDecOfCSmpsi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtDiasDecOfCSmpsi" runat="server" TargetControlID="TxtDiasDecOfCSmpsi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSmpsi03" visible="false">
            <td class="td1" colspan="1">No. Horas Modifica Oferta Vendedor Suministro sin Interrupciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNHorModOfVSmpsi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtNHorModOfVSmpsi" runat="server" TargetControlID="TxtNHorModOfVSmpsi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">No. Horas Modifica Oferta Comprador Suministro sin Interrupciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNHorModOfCSmpsi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtNHorModOfCSmpsi" runat="server" TargetControlID="TxtNHorModOfCSmpsi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSmpsi04" visible="false">
            <td class="td1" colspan="1">No. Semanas Revelación de Información Suministro sin Interrupciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNoSemSmpsi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtNoSemSmpsi" runat="server" TargetControlID="TxtNoSemSmpsi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--<tr runat="server" id="TrUvlp01" visible="false">
            <td class="td1" colspan="1">
                Tiempo Duracion Ronda Uselo Largo Plazo
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDurRonUvlp" runat="server"></asp:TextBox>
                <asp:CompareValidator ID="CvTxtDurRonUvlp" runat="server" ControlToValidate="TxtDurRonUvlp"
                    Type="Integer" Operator="DataTypeCheck" ValidationGroup="comi" ErrorMessage="El Tiempo Duracion Ronda Uselo Largo Plazo deben ser un valor entero">*</asp:CompareValidator>
            </td>
        </tr>--%>
        <tr runat="server" id="TrSci01" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblDiasSCI" runat="server"></asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDiaAntCi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fteTxtDiaAntSci" runat="server" TargetControlID="TxtDiaAntCi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub01" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNoPosCCi" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtNoPosCCi" runat="server" TargetControlID="TxtNoPosCCi"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSci02" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub02" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniPubVCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniPubVCi" ControlToValidate="TxtHorIniPubVCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Publicacion Venta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub03" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinPubVCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinPubVCi" ControlToValidate="TxtHorFinPubVCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Publicacion Venta"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci03" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub04" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniPubCantDCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniPubCantDCi" ControlToValidate="TxtHorIniPubCantDCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Publicacion Cantidad Dispinible Venta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub05" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinPubCantDCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinPubCantDCi" ControlToValidate="TxtHorFinPubCantDCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Publicacion Cantidad Dispinible Venta"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci04" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub06" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniRecCantCCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniRecCantCCi" ControlToValidate="TxtHorIniRecCantCCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Recibo Cantidad Compra"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub07" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinRecCantCCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinRecCantCCi" ControlToValidate="TxtHorFinRecCantCCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Recibo Cantidad Compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci05" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub08" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorIniNegociaCi" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorIniNegociaCi" ControlToValidate="TxtHorIniNegociaCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Negociacion"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub09" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFinNegociaCi" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFinNegociaCi" ControlToValidate="TxtHorFinNegociaCi"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Negociacion"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrSci06" visible="false">
            <td class="td1" colspan="1">Minutos para modificar ofertas de venta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtMinModV" runat="server" MaxLength="5"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="cvTxtMinModV" runat="server" TargetControlID="TxtMinModV"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">Minutos para modificar ofertas de compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtMinModC" runat="server" MaxLength="5"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="cvTxtMinModC" runat="server" TargetControlID="TxtMinModC"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSci07" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub10" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtNoMesIniPer" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="CvTxtNoMesIniPer" runat="server" TargetControlID="TxtNoMesIniPer"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <%--Campos Nuevos Req. 007-17 Subasta Bimestral 20170209--%>
        <tr runat="server" id="TrSci08" visible="false">
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub11" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDiaHabDeclaraInf" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FteTxtDiaHabDeclaraInf" runat="server" TargetControlID="TxtDiaHabDeclaraInf"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
            <td class="td1" colspan="1">
                <asp:Label ID="lblSub12" runat="server"> </asp:Label>
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtDiaHabPublicaInf" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FteTxtDiaHabPublicaInf" runat="server" TargetControlID="TxtDiaHabPublicaInf"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrSci09" visible="false">
            <td class="td1" colspan="1">Hora Inicial Modificación Declaración Subasta Bimestral
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniModDecSb" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraIniModDecSb" ControlToValidate="TxtHoraIniModDecSb"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Modificación Declaración Subasta Bimestral"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Final Modificación Declaración Subasta Bimestral
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinModDecSb" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinModDecSb" ControlToValidate="TxtHoraFinModDecSb"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Modificación Declaración Subasta Bimestral"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--hasta Aqui--%>
        <tr runat="server" id="TrSnd01" visible="false">
            <td class="td1" colspan="1">Hora Inicial Ingreso Posturas Negociación Directa
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorInPosNd" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorInPosNd" ControlToValidate="TxtHorInPosNd"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial Ingreso Posturas Negociacion Directa"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Final Ingreso Posturas Negociación Directa
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHorFiPosNd" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHorFiPosNd" ControlToValidate="TxtHorFiPosNd"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final Ingreso Posturas Negociacion Directa"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP01" visible="false">
            <td class="td1" colspan="1">Hora Inicio Fase de Publicación
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniPubUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraIniPubUvlp" ControlToValidate="TxtHoraIniPubUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial de publicación"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Fin Fase de Publicación
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinPubUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinPubUvlp" ControlToValidate="TxtHoraFinPubUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final de publicación"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP02" visible="false">
            <td class="td1" colspan="1">Hora Inicio Fase de Ingreso de Posturas de Compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniCompUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RfvTxtHoraIniCompUvlp" ControlToValidate="TxtHoraIniPubUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Inicial de ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Fin Fase de Ingreso de Posturas de Compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinCompUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinCompUvlp" ControlToValidate="TxtHoraFinCompUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final de ingreso de posturas de compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP03" visible="false">
            <td class="td1" colspan="1">Hora Inicio Fase de Calce
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraIniCalceUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraIniCalceUvlp" ControlToValidate="TxtHoraIniCalceUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora de inicio de calce"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Hora Fin Fase de Calce
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="TxtHoraFinCalceUvlp" runat="server" MaxLength="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RevTxtHoraFinCalceUvlp" ControlToValidate="TxtHoraFinCalceUvlp"
                    ValidationGroup="comi" runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])"
                    ErrorMessage="Formato Incorrecto para la Hora Final de calce"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--20160921 UVLP--%>
        <tr runat="server" id="TrUVLP04" visible="false">
            <td class="td1" colspan="1">Tipo de negociación
            </td>
            <td class="td2" colspan="3">
                <asp:DropDownList ID="ddlTpoNEgUvlp" runat="server">
                    <asp:ListItem Value="P">Principal</asp:ListItem>
                    <asp:ListItem Value="D">Derivada</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--Campos Subasta Transporte Req. 005-2021 20210120--%>
        <tr runat="server" id="TrStra01" visible="false">
            <td class="td1">Tipo Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlTipoRueTra11" runat="server">
                    <asp:ListItem Value="0">Seleccione</asp:ListItem>
                    <asp:ListItem Value="1">Beneficiarios Por Rutas</asp:ListItem>
                    <asp:ListItem Value="2">Beneficiarios Por Tramos</asp:ListItem>
                    <asp:ListItem Value="3">No Beneficiarios Por rutas</asp:ListItem>
                    <asp:ListItem Value="4">No beneficiarios por Tramos</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Días hábiles desde inicio de trimestre de negociación para el desarrollo de la subasta
            </td>
            <td class="td2">
                <asp:TextBox ID="txtNoDiaInTriNeg11" runat="server"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FetxtNoDiaInTriNeg11" runat="server" TargetControlID="txtNoDiaInTriNeg11"
                    FilterType="Custom, Numbers"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr runat="server" id="TrStra02" visible="false">
            <td class="td1" colspan="1">Horas Para Publicación de oferta 
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniPubOfr11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniPubOfr11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Publicación de oferta"
                    ControlToValidate="txtHoraIniPubOfr11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniPubOfr11" ControlToValidate="txtHoraIniPubOfr11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Publicación de oferta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinPubOfr11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinPubOfr11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Publicación de oferta"
                    ControlToValidate="txtHoraFinPubOfr11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinPubOfr11" ControlToValidate="txtHoraFinPubOfr11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Publicación de oferta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Horas Para Declaración posturas de compra
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniDecPosCom11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniDecPosCom11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Declaración posturas de compra"
                    ControlToValidate="txtHoraIniDecPosCom11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniDecPosCom11" ControlToValidate="txtHoraIniDecPosCom11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Declaración posturas de compra"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinDecPosCom11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinDecPosCom11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Declaración posturas de compra"
                    ControlToValidate="txtHoraFinDecPosCom11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinDecPosCom11" ControlToValidate="txtHoraFinDecPosCom11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Declaración posturas de compra"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrStra03" visible="false">
            <td class="td1" colspan="1">Horas Para Desarrollo subasta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniDesSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniDesSub11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Desarrollo subasta"
                    ControlToValidate="txtHoraIniDesSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniDesSub11" ControlToValidate="txtHoraIniDesSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Desarrollo subasta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinDesSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinDesSub11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Desarrollo subasta"
                    ControlToValidate="txtHoraFinDesSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinDesSub11" ControlToValidate="txtHoraFinDesSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Desarrollo subasta"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Horas Para Rechazo de adjudicaciones
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniRecAdj11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniRecAdj11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Rechazo de adjudicaciones"
                    ControlToValidate="txtHoraIniRecAdj11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniRecAdj11" ControlToValidate="txtHoraIniRecAdj11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Rechazo de adjudicaciones"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinRecAdj11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinRecAdj11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Rechazo de adjudicaciones"
                    ControlToValidate="txtHoraFinRecAdj11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinRecAdj11" ControlToValidate="txtHoraFinRecAdj11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Rechazo de adjudicaciones"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr runat="server" id="TrStra04" visible="false">
            <td class="td1" colspan="1">Horas Para Publicación resultados
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniPubRes11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniPubRes11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Publicación resultados"
                    ControlToValidate="txtHoraIniPubRes11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniPubRes11" ControlToValidate="txtHoraIniPubRes11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Publicación resultados"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinPubRes11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinPubRes11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Publicación resultados"
                    ControlToValidate="txtHoraFinPubRes11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinPubRes11" ControlToValidate="txtHoraFinPubRes11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Publicación resultados"> * </asp:RegularExpressionValidator>
            </td>
            <td class="td1" colspan="1">Horas Para Finalización de la subasta
            </td>
            <td class="td2" colspan="1">
                <asp:TextBox ID="txtHoraIniFinSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraIniFinSub11" runat="server" ErrorMessage="Debe Ingresar la Hora de inicio para Finalización de la subasta"
                    ControlToValidate="txtHoraIniFinSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraIniFinSub11" ControlToValidate="txtHoraIniFinSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora de inicio para Finalización de la subasta"> * </asp:RegularExpressionValidator>
                -
                <asp:TextBox ID="txtHoraFinFinSub11" runat="server" MaxLength="5" Width="40px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvtxtHoraFinFinSub11" runat="server" ErrorMessage="Debe Ingresar la Hora final para Finalización de la subasta"
                    ControlToValidate="txtHoraFinFinSub11" ValidationGroup="comi"> * </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RevtxtHoraFinFinSub11" ControlToValidate="txtHoraFinFinSub11"
                    runat="server" ValidationExpression="^([01][0-9]|2[0-3]):([0-5][0-9])" ValidationGroup="comi"
                    ErrorMessage="Formato Incorrecto para la hora final  para Finalización de la subasta"> * </asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--Hsta Aqui--%>
        <%--Campos Nuevos Req. 007-17 Subasta Bimestral 20170209 --%>
        <tr id="TrMes" runat="server" visible="false">
            <td class="td1" rowspan="2">Meses para crear rueda
            </td>
            <td class="td2" colspan="3">
                <asp:CheckBox ID="ChkEnero" runat="server" />
                Enero
                <asp:CheckBox ID="ChkFebrero" runat="server" />
                Febrero
                <asp:CheckBox ID="ChkMarzo" runat="server" />
                Marzo
                <asp:CheckBox ID="ChkAbril" runat="server" />
                Abril
                <asp:CheckBox ID="ChkMayo" runat="server" />
                Mayo
                <asp:CheckBox ID="ChkJunio" runat="server" />
                Junio
            </td>
        </tr>
        <tr id="TrMes1" runat="server" visible="false">
            <td class="td2" colspan="3">
                <asp:CheckBox ID="ChkJulio" runat="server" />
                Julio
                <asp:CheckBox ID="ChkAgosto" runat="server" />
                Agosto
                <asp:CheckBox ID="ChkSeptiembre" runat="server" />
                Septiembre
                <asp:CheckBox ID="ChkOctubre" runat="server" />
                Octubre
                <asp:CheckBox ID="ChkNoviembre" runat="server" />
                Noviembre
                <asp:CheckBox ID="ChkDicimebre" runat="server" />
                Diciembre
            </td>
        </tr>
        <%--Hasta Aqui--%>
        <tr>
            <td class="td1">Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="td1">Crea ruedas automáticas diarias?
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlRuedaAut" runat="server" OnSelectedIndexChanged="ddlRuedaAut_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="N">No</asp:ListItem>
                    <asp:ListItem Value="S">Si</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trDia" runat="server" visible="false">
            <td class="td1">Días para crear rueda
            </td>
            <td class="td2" colspan="3">
                <asp:CheckBox ID="chkLun" runat="server" />
                Lunes
                <asp:CheckBox ID="chkMar" runat="server" />
                Martes
                <asp:CheckBox ID="chkMie" runat="server" />
                Miercoles <%--20201207--%>
                <asp:CheckBox ID="chkJue" runat="server" />
                Jueves
                <asp:CheckBox ID="chkVie" runat="server" />
                Viernes <%--20201207--%>
                <asp:CheckBox ID="chkSab" runat="server" />
                Sabado
                <asp:CheckBox ID="chkDom" runat="server" />
                Domingo
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">Código Tipo Rueda
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusTipoRueda" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Descripción
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusDescripcion" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">Destino Rueda
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusDestino" runat="server">
                    <asp:ListItem Value="S">Seleccione</asp:ListItem>
                    <asp:ListItem Value="G">Gas</asp:ListItem>
                    <asp:ListItem Value="T">Transporte</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">Tipo Subasta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlBusTipoSubasta" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_tipo_rueda" HeaderText="Codigo Tipo Rueda" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="350px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="destino_rueda" HeaderText="Destino Rueda" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="hora_apertura" HeaderText="Hora Apertura" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_mercado" HeaderText="Tipo Mercado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_tipo_subasta" HeaderText="Código Tipo Subasta"
                                ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_subasta" HeaderText="Desc. Tipo Subasta" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>