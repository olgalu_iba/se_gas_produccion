﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_rutaEsp.aspx.cs"
    Inherits="BASE_frm_rutaEsp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_rutaEsp.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_rutaEsp.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_rutaEsp.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
            </td>
        </tr>
    </table>
    <br /><br /><br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Código Ruta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoRuta" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoRuta" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Código Punto SNT inicial
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPozoIni" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Código Punto SNT final
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlPozoFin" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="P">Pendiente de Rutas</asp:ListItem>
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Código Ruta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCodigoRuta" runat="server" autocomplete="off"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodigoRuta" runat="server" TargetControlID="TxtBusCodigoRuta"
                    FilterType="Custom, Numbers">
                </cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Descripción
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusDescripcion" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_ruta_esp" HeaderText="Cd. Ruta Especial" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_pozo_ini" HeaderText="pto ini" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="codigo_pozo_fin" HeaderText="pto fin" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="350px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="costo_actual" HeaderText="costo" ItemStyle-HorizontalAlign="Right"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Tramos" EditText="Tramos"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTramo" visible="false">
        <tr>
            <td class="td1">
                Ruta Especial
            </td>
            <td class="td2" colspan="3">
                <asp:Label ID="lblCodRuta1" runat="server"></asp:Label>
                -
                <asp:Label ID="lblDescPunto" runat="server"></asp:Label>
                <asp:Label ID="lblCodPozF" runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Tramo
            </td>
            <td class="td2" colspan="3">
                <asp:DropDownList ID="ddlTramo" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrearTra" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrearTra_Click"
                    Height="40" />
                <asp:ImageButton ID="imbActualizaTra" runat="server" ImageUrl="~/Images/Actualizar.png"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalirTra" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalirTra_Click"
                    Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:DataGrid ID="dtgTramo" runat="server" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="td1"
                    ItemStyle-CssClass="td2" OnEditCommand="dtgTramo_EditCommand" HeaderStyle-CssClass="th1">
                    <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                    <ItemStyle CssClass="td2"></ItemStyle>
                    <Columns>
                        <asp:BoundColumn DataField="codigo_ruta_esp_det" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_ruta_esp" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_pozo_ini" HeaderText="Cod Ini" ItemStyle-HorizontalAlign="center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_pozo_ini" HeaderText="desc pto ini" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_pozo_fin" HeaderText="Cod Fin" ItemStyle-HorizontalAlign="center">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_pozo_fin" HeaderText="desc pto fin" ItemStyle-HorizontalAlign="Left">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="orden" HeaderText="orden" ItemStyle-HorizontalAlign="Center">
                        </asp:BoundColumn>
                        <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                    </Columns>
                    <HeaderStyle CssClass="th1"></HeaderStyle>
                </asp:DataGrid>
                <asp:HiddenField ID="hndOrden" runat="server" />
                <asp:HiddenField ID="hndPozoFin" runat="server" />
                <asp:HiddenField ID="hndPozoIni" runat="server" />
                <asp:HiddenField ID="hndCuenta" runat="server" />
            </td>
        </tr>
    </table>
    </asp:Content>
