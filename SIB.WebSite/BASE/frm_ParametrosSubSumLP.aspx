﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_ParametrosSubSumLP.aspx.cs"
    Inherits="BASE_frm_ParametrosSubSumLP" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
            id="tblTitulo">
            <tr>
                <td align="center" class="th3">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                </td>
            </tr>
        </table>
        <br /><br /><br /><br /><br />
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="100%" runat="server"
            id="tblCaptura">
            <tr>
                <td class="td1">Fecha Inicial de negociaciones de largo plazo {YYYY/MM/DD}
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaIniCont" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaIniCont" runat="server" ErrorMessage="Debe Ingresar la Fecha Inicial para ingreso de contratos de largo plazo"
                        ControlToValidate="TxtFechaIniCont" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <td class="td1">Fecha Final de negociaciones de largo plazo {YYYY/MM/DD}
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaFinCont" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaFinCont" runat="server" ErrorMessage="Debe Ingresar la Fecha Final para el ingreso de contratos de largo plazo"
                        ControlToValidate="TxtFechaFinCont" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">Mínimo de años de duración de los contratos de largo plazo
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtAnoDuracion" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtAnoDuracion" runat="server" ErrorMessage="Debe Ingresar el número mínimo de años de duración de los contratos de largo plazo"
                        ControlToValidate="TxtAnoDuracion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fteTxtAnoDuracion" runat="server" TargetControlID="TxtAnoDuracion"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Número máximo de años para fecha inicial de los contratos de largo plazo
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtAnoIniCont" runat="server" MaxLength="5" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvTxtAnoIniCont" runat="server" ErrorMessage="Debe Ingresar el Número máximo de años para la fecha inicial de los contratos de largo plazo"
                        ControlToValidate="TxtAnoIniCont" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="ftbTxtAnoIniCont" runat="server" TargetControlID="TxtAnoIniCont"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha inicial de reserva de cantidad remanente
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaIniRem" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaIniRem" runat="server" ErrorMessage="Debe Ingresar la Fecha inicial de reserva de la cantidad remanente"
                        ControlToValidate="TxtFechaIniRem" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <td class="td1">Fecha final de reserva de cantidad remanente
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaFinRem" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaFinRem" runat="server" ErrorMessage="Debe Ingresar la Fecha final de reserva de la cantidad remanente"
                        ControlToValidate="TxtFechaFinRem" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha mínina de publicación de la PTDVF/CIDVF remanente
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFechaPubRem" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtFechaPubRem" runat="server" ErrorMessage="Debe Ingresar la Fecha mímina de publicación de la PTDVF/CIDVF remanente"
                        ControlToValidate="txtFechaPubRem" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <td class="td1">Fecha de adjudicación de las cantidades reservadas PTDVF/CIDVF
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFechaAdjRes" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtFechaAdjRes" runat="server" ErrorMessage="Debe Ingresar la Fecha de adjudicación de las cantidades reservadas de PTDVF/CIDVF"
                        ControlToValidate="txtFechaAdjRes" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha cálculo de precio de operaciones de reserva
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFechaPre" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtFechaPre" runat="server" ErrorMessage="Debe Ingresar la Fecha para cálculo de precio de operaciones de reserva"
                        ControlToValidate="txtFechaPre" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <td class="td1">URL para conexión a Subasta C1 y C2
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtUrl" runat="server" MaxLength="300" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="td1">Porcentaje fijo modalidad C1
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtPorcFijoC1" runat="server" MaxLength="6" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvTxtPorcFijoC1" runat="server" ErrorMessage="Debe Ingresar el porcentaje fijo de modalidad C1"
                        ControlToValidate="TxtPorcFijoC1" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fteTxtPorcFijoC1" runat="server" TargetControlID="TxtPorcFijoC1"
                        FilterType="Custom, Numbers" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Porcentaje variable modalidad C1
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtPorcVarC1" runat="server" MaxLength="6" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtPorcVarC1" runat="server" ErrorMessage="Debe Ingresar el porcentaje variable de modalidad C1"
                        ControlToValidate="TxtPorcVarC1" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fteTxtPorcVarC1" runat="server" TargetControlID="TxtPorcVarC1"
                        FilterType="Custom, Numbers" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Porcentaje fijo modalidad C2
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtPorcFijoC2" runat="server" MaxLength="6" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtPorcFijoC2" runat="server" ErrorMessage="Debe Ingresar el porcentaje fijo de modalidad C2"
                        ControlToValidate="TxtPorcFijoC2" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fteTxtPorcFijoC2" runat="server" TargetControlID="TxtPorcFijoC2"
                        FilterType="Custom, Numbers" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Porcentaje variable modalidad C2
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtPorcVarC2" runat="server" MaxLength="6" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtPorcVarC2" runat="server" ErrorMessage="Debe Ingresar el porcentaje variable de modalidad C2"
                        ControlToValidate="TxtPorcVarC2" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fteTxtPorcVarC2" runat="server" TargetControlID="TxtPorcVarC2"
                        FilterType="Custom, Numbers" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha inicial de entrega a usuarios finales para control de reservas
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaIniCtrl" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaIniCtrl" runat="server" ErrorMessage="Debe Ingresar la Fecha inicial de entrega a usuarios finales para control de reservas"
                        ControlToValidate="TxtFechaIniCtrl" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <td class="td1">Fecha final de entrega a usuarios finales para control de reservas
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaFinCtrl" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaFinCtrl" runat="server" ErrorMessage="Debe Ingresar la Fecha final de entrega a usuarios finales para control de reservas"
                        ControlToValidate="TxtFechaFinCtrl" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">Fecha de publicación informe de la CREG de la subasta C1
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaC1" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaC1" runat="server" ErrorMessage="Debe Ingresar la Fecha de publicación del informe de la CREG de la subasta C1"
                        ControlToValidate="TxtFechaC1" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <td class="td1">Fecha de publicación informe de la CREG de la subasta C2
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaC2" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFechaC2" runat="server" ErrorMessage="Debe Ingresar la Fecha de publicación del informe de la CREG de la subasta C2"
                        ControlToValidate="TxtFechaC2" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    
                </td>
            </tr>
                        <%--20171020 rq051-17--%>
            <tr>
                <td class="td1">Valor mínimo de firmeza mínima
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFirmeMin" runat="server" MaxLength="6" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtFirmeMin" runat="server" ErrorMessage="Debe Ingresar valor mínimo de la firmeza mínima"
                        ControlToValidate="txtFirmeMin" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="ftebtxtFirmeMin" runat="server" TargetControlID="txtFirmeMin"
                        FilterType="Custom, Numbers" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="td1">Porcentaje sobre la cantidad máxima de los contratos de suministro
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtPorcCntMax" runat="server" MaxLength="6" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtPorcCntMax" runat="server" ErrorMessage="Debe Ingresar el porcentaje sobre la cantidad máxima de los contratos de suministro"
                        ControlToValidate="txtPorcCntMax" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="ftebtxtPorcCntMax" runat="server" TargetControlID="txtPorcCntMax"
                        FilterType="Custom, Numbers" ValidChars=".">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>

            <tr>
                <td class="td1">Fecha máxima de modificación de registro o eliminación de contratos de largo plazo
                </td>
                <td class="td2" > <%--20190404 rq018-19 fase II--%>
                    <asp:TextBox ID="TxtFecMaxReg" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtFecMaxReg" runat="server" ErrorMessage="Debe Ingresar la Fecha máxima de modificación de registro o eliminación de contratos de largo plazo"
                        ControlToValidate="TxtFecMaxReg" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <%--20190404 rq018-19 fase II--%>
                <td class="td1">Fecha máxima de registro de negociaciones de largo plazo
                </td>
                <%--20190404 rq018-19 fase II--%>
                <td class="td2" colspan="3">
                    <asp:TextBox ID="txtFechaMaxRegLp" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtFechaMaxRegLp" runat="server" ErrorMessage="Debe Ingresar la Fecha máxima de registro de negociaciones de largo plazo"
                        ControlToValidate="txtFechaMaxRegLp" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>

            </tr>
            <%--20190404 rq018-19 fase II--%>
            <tr>
                <td class="td1">Fecha inicial permitida para fecha inicial de operaciones demanda no regulada
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFechaIniIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtFechaIniIni" runat="server" ErrorMessage="Debe Ingresar la Fecha inicial permitida para la fecha inicial de operaciones de demanda no regulada"
                        ControlToValidate="txtFechaIniIni" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
                <td class="td1">Fecha final permitida para fecha inicial de operaciones demanda no regulada
                </td>
                <td class="td2">
                    <asp:TextBox ID="txtFechaFinIni" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtFechaFinIni" runat="server" ErrorMessage="Debe Ingresar la Fecha inicial permitida para la fecha inicial de operaciones de demanda no regulada"
                        ControlToValidate="txtFechaFinIni" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="td1">Observación Cambio
                </td>
                <td class="td2" colspan="3">
                    <asp:TextBox ID="TxtObservacion" runat="server" MaxLength="1000" Width="500px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvTxtObservacion" runat="server" ErrorMessage="Debe Ingresar la Observación del Cambio"
                        ControlToValidate="TxtObservacion" ValidationGroup="VsParametrosBna"> * </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="4" align="center">
                    <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                        Height="40" ValidationGroup="VsParametrosBna" />
                    <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                        OnClick="imbActualiza_Click" ValidationGroup="VsParametrosBna" Height="40" />
                    <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                        CausesValidation="false" Height="40" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:ValidationSummary ID="VsParam" runat="server" ValidationGroup="VsParametrosBna" />
                </td>
            </tr>
        </table>
        <br />
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
</asp:Content>