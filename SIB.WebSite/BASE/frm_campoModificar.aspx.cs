﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_campoModificar : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Campos a modificar por causa de modificación";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    SqlDataReader lLector1;

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlCausa, "m_causa_modificacion", " estado ='A' order by descripcion", 0, 1);
            LlenarControles(lConexion.gObjConexion, ddlBusCausa, "m_causa_modificacion", " estado ='A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (this.Request.QueryString["lsIndica"] != null && this.Request.QueryString["lsIndica"].ToString() != "")
            {
                lsIndica = this.Request.QueryString["lsIndica"].ToString();
            }
            if (lsIndica == null || lsIndica == "" || lsIndica == "L")
            {
                Listar();
            }
            else if (lsIndica == "N")
            {
                Nuevo();
            }
            else if (lsIndica == "B")
            {
                Buscar();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    private void EstablecerPermisosSistema()
    {
        Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, "m_campo_modificar");
        hlkNuevo.Enabled = (Boolean)permisos["INSERT"];
        hlkListar.Enabled = (Boolean)permisos["SELECT"];
        hlkBuscar.Enabled = (Boolean)permisos["SELECT"];
        dtgMaestro.Columns[5].Visible = (Boolean)permisos["UPDATE"];
        dtgMaestro.Columns[6].Visible = false;
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        tblgrilla.Visible = false;
        tblBuscar.Visible = false;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
        TxtCodigoCampo.Visible = false;
        LblCodigoCampo.Visible = true;
        LblCodigoCampo.Text = "Automático";
        ddlCausa.Enabled = true;
    }
    /// <summary>
    /// Nombre: Listar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Listar y Llama
    ///               el Metodo de obtener los Datos para Cargar el Control DataGrid
    /// Modificacion:
    /// </summary>
    private void Listar()
    {
        CargarDatos();
        tblCaptura.Visible = false;
        tblgrilla.Visible = true;
        tblBuscar.Visible = false;
        lblTitulo.Text = "Listar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar(string modificar)
    {
        if (modificar != null && modificar != "")
        {
            try
            {
                lblMensaje.Text = "";
                /// Verificar Si el Registro esta Bloqueado
                if (!manejo_bloqueo("V", modificar))
                {
                    // Carga informacion de combos
                    lConexion.Abrir();
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_campo_modificar", " codigo_campo= " + modificar + " ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        LblCodigoCampo.Text = lLector["codigo_campo"].ToString();
                        TxtCodigoCampo.Text = lLector["codigo_campo"].ToString();
                        ddlCausa.SelectedValue = lLector["codigo_causa"].ToString();
                        ddlCampo1.SelectedValue = lLector["contrato_definitivo"].ToString();
                        ddlCampo2.SelectedValue = lLector["fecha_negociacion"].ToString();
                        ddlCampo3.SelectedValue = lLector["hora_negociacion"].ToString();
                        ddlCampo4.SelectedValue = lLector["fecha_suscripcion"].ToString();
                        ddlCampo5.SelectedValue = lLector["punto_ruta"].ToString();
                        ddlCampo6.SelectedValue = lLector["codigo_modalidad"].ToString();
                        ddlCampo7.SelectedValue = lLector["periodo_entrega"].ToString();
                        ddlCampo8.SelectedValue = lLector["operador_compra"].ToString();
                        ddlCampo9.SelectedValue = lLector["operador_venta"].ToString();
                        ddlCampo10.SelectedValue = lLector["cantidad"].ToString();
                        ddlCampo11.SelectedValue = lLector["precio"].ToString();
                        ddlCampo12.SelectedValue = lLector["fecha_inicial"].ToString();
                        ddlCampo13.SelectedValue = lLector["hora_inicial"].ToString();
                        ddlCampo14.SelectedValue = lLector["fecha_final"].ToString();
                        ddlCampo15.SelectedValue = lLector["hora_final"].ToString();
                        ddlCampo16.SelectedValue = lLector["numero_años"].ToString();
                        ddlCampo17.SelectedValue = lLector["sentido_flujo"].ToString();
                        ddlCampo18.SelectedValue = lLector["presion_punto_final"].ToString();
                        ddlCampo19.SelectedValue = lLector["tipo_garantia"].ToString();
                        ddlCampo20.SelectedValue = lLector["valor_garantia"].ToString();
                        ddlCampo21.SelectedValue = lLector["fecha_pago_garantia"].ToString();
                        ddlCampo21A.SelectedValue = lLector["codigo_sector_consumo"].ToString();
                        ddlCampo21B.SelectedValue = lLector["usuario_conectado"].ToString();
                        ddlCampo22.SelectedValue = lLector["cod_mercado_relev"].ToString();
                        ddlCampo23.SelectedValue = lLector["codigo_departamento"].ToString();
                        ddlCampo24.SelectedValue = lLector["codigo_ciudad"].ToString();
                        ddlCampo25.SelectedValue = lLector["codigo_centro"].ToString();
                        ddlCampo26.SelectedValue = lLector["codigo_fuente"].ToString();
                        ddlCampo27.SelectedValue = lLector["conectado_snt"].ToString();
                        ddlCampo28.SelectedValue = lLector["ent_boca_pozo"].ToString();
                        ddlCampo29.SelectedValue = lLector["codigo_ruta_may"].ToString();
                        ddlCampo30.SelectedValue = lLector["ind_contrato_var"].ToString();
                        ddlCampo31.SelectedValue = lLector["codigo_trm"].ToString(); //20210707 trm moneda
                        ddlCampo32.SelectedValue = lLector["observacion_trm"].ToString(); //20210707 trm moneda
                        ddlCampo33.SelectedValue = lLector["tipo_moneda"].ToString(); //20210707 trm moneda
                        ddlEstado.SelectedValue = lLector["estado"].ToString();
                        imbCrear.Visible = false;
                        imbActualiza.Visible = true;
                        TxtCodigoCampo.Visible = false;
                        LblCodigoCampo.Visible = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                    /// Bloquea el Registro a Modificar
                    manejo_bloqueo("A", modificar);
                }
                else
                {
                    Listar();
                    lblMensaje.Text = "No se Puede editar el Registro por que está Bloqueado. Codigo campo a modificar" + modificar.ToString();

                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            tblgrilla.Visible = false;
            tblBuscar.Visible = false;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: Buscar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Buscar.
    /// Modificacion:
    /// </summary>
    private void Buscar()
    {
        tblCaptura.Visible = false;
        tblgrilla.Visible = false;
        tblBuscar.Visible = true;
        lblTitulo.Text = "Consultar " + lsTitulo;
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int };
        string[] lValorParametros = { "0", "0" };

        try
        {
            if (TxtBusCodigoCampo.Text.Trim().Length > 0)
                lValorParametros[0] = TxtBusCodigoCampo.Text.Trim();
            lValorParametros[1] = ddlBusCausa.SelectedValue;


            lConexion.Abrir();
            dtgMaestro.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros);
            dtgMaestro.DataBind();
            lConexion.Cerrar();
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Consulta, cuando se da Click en el Link Consultar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, ImageClickEventArgs e)
    {
        Listar();
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa", "@P_contrato_definitivo", "@P_fecha_negociacion", "@P_hora_negociacion", "@P_fecha_suscripcion", "@P_punto_ruta",
            "@P_codigo_modalidad", "@P_periodo_entrega", "@P_operador_compra", "@P_operador_venta", "@P_cantidad", "@P_precio", "@P_fecha_inicial", "@P_hora_inicial", "@P_fecha_final", "@P_hora_final",
            "@P_numero_años", "@P_sentido_flujo", "@P_presion_punto_final", "@P_tipo_garantia", "@P_valor_garantia", "@P_fecha_pago_garantia", "@P_cod_mercado_relev", "@P_codigo_departamento", "@P_codigo_ciudad",
            "@P_codigo_centro", "@P_codigo_fuente",  "@P_conectado_snt", "@P_ent_boca_pozo","@P_codigo_ruta_may","@P_ind_contrato_var", "@P_estado", "@P_accion", "@P_codigo_sector_consumo", "@P_usuario_conectado",
            "@P_codigo_trm","@P_observacion_trm","@P_tipo_moneda"}; //20210707 trm moneda
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char,  SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char,
            SqlDbType.Char, SqlDbType.Char, SqlDbType.Char }; //20210707 trm moneda
        string[] lValorParametros = { "0", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","","", "1", "","", "", "", "" }; //20210707 trm moneda
        lblMensaje.Text = "";

        try
        {
            if (ddlCausa.SelectedValue  == "")
                lblMensaje.Text += "Debe seleccionar la causa de modificación <br>";
            else 
                if(VerificarExistencia("codigo_causa =" + ddlCausa.SelectedValue ))
                    lblMensaje.Text += "Ya están definidos los campos para la causa de modificación<br>";
            if (lblMensaje.Text == "")
            {
                lValorParametros[1] = ddlCausa.Text.Trim();
                lValorParametros[2] = ddlCampo1.SelectedValue;
                lValorParametros[3] = ddlCampo2.SelectedValue;
                lValorParametros[4] = ddlCampo3.SelectedValue;
                lValorParametros[5] = ddlCampo4.SelectedValue;
                lValorParametros[6] = ddlCampo5.SelectedValue;
                lValorParametros[7] = ddlCampo6.SelectedValue;
                lValorParametros[8] = ddlCampo7.SelectedValue;
                lValorParametros[9] = ddlCampo8.SelectedValue;
                lValorParametros[10] = ddlCampo9.SelectedValue;
                lValorParametros[11] = ddlCampo10.SelectedValue;
                lValorParametros[12] = ddlCampo11.SelectedValue;
                lValorParametros[13] = ddlCampo12.SelectedValue;
                lValorParametros[14] = ddlCampo13.SelectedValue;
                lValorParametros[15] = ddlCampo14.SelectedValue;
                lValorParametros[16] = ddlCampo15.SelectedValue;
                lValorParametros[17] = ddlCampo16.SelectedValue;
                lValorParametros[18] = ddlCampo17.SelectedValue;
                lValorParametros[19] = ddlCampo18.SelectedValue;
                lValorParametros[20] = ddlCampo19.SelectedValue;
                lValorParametros[21] = ddlCampo20.SelectedValue;
                lValorParametros[22] = ddlCampo21.SelectedValue;
                lValorParametros[23] = ddlCampo22.SelectedValue;
                lValorParametros[24] = ddlCampo23.SelectedValue;
                lValorParametros[25] = ddlCampo24.SelectedValue;
                lValorParametros[26] = ddlCampo25.SelectedValue;
                lValorParametros[27] = ddlCampo26.SelectedValue;
                lValorParametros[28] = ddlCampo27.SelectedValue;
                lValorParametros[29] = ddlCampo28.SelectedValue;
                lValorParametros[30] = ddlCampo29.SelectedValue;
                lValorParametros[31] = ddlCampo30.SelectedValue;
                lValorParametros[32] = ddlEstado.SelectedValue;
                lValorParametros[34] = ddlCampo21A.SelectedValue;
                lValorParametros[35] = ddlCampo21B.SelectedValue;
                lValorParametros[36] = ddlCampo31.SelectedValue; //20210707 trm moneda
                lValorParametros[37] = ddlCampo32.SelectedValue; //20210707 trm moneda
                lValorParametros[38] = ddlCampo33.SelectedValue; //20210707 trm moneda
                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje.Text = lLector["error"].ToString();
                    
                }
                else
                {
                    lblMensaje.Text = "Registro creado correctamente";
                    Listar();
                }
                lLector.Close();
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al crear el registro. "+ex.Message;
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_campo", "@P_codigo_causa", "@P_contrato_definitivo", "@P_fecha_negociacion", "@P_hora_negociacion", "@P_fecha_suscripcion", "@P_punto_ruta",
            "@P_codigo_modalidad", "@P_periodo_entrega", "@P_operador_compra", "@P_operador_venta", "@P_cantidad", "@P_precio", "@P_fecha_inicial", "@P_hora_inicial", "@P_fecha_final", "@P_hora_final",
            "@P_numero_años", "@P_sentido_flujo", "@P_presion_punto_final", "@P_tipo_garantia", "@P_valor_garantia", "@P_fecha_pago_garantia","@P_cod_mercado_relev", "@P_codigo_departamento", "@P_codigo_ciudad",
            "@P_codigo_centro", "@P_codigo_fuente", "@P_conectado_snt", "@P_ent_boca_pozo", "@P_codigo_ruta_may","@P_ind_contrato_var", "@P_estado", "@P_accion", "@P_codigo_sector_consumo", "@P_usuario_conectado",
            "@P_codigo_trm","@P_observacion_trm","@P_tipo_moneda"}; //20210707 trm moneda
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char, SqlDbType.Char,
                SqlDbType.Char, SqlDbType.Char, SqlDbType.Char }; //20210707 trm moneda
        string[] lValorParametros = { "0", "0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "","","", "2", "","", "", "", "" }; //20210707 trm moneda

        lblMensaje.Text = "";
        try
        {
            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = LblCodigoCampo.Text;
                lValorParametros[1] = ddlCausa.Text.Trim();
                lValorParametros[2] = ddlCampo1.SelectedValue;
                lValorParametros[3] = ddlCampo2.SelectedValue;
                lValorParametros[4] = ddlCampo3.SelectedValue;
                lValorParametros[5] = ddlCampo4.SelectedValue;
                lValorParametros[6] = ddlCampo5.SelectedValue;
                lValorParametros[7] = ddlCampo6.SelectedValue;
                lValorParametros[8] = ddlCampo7.SelectedValue;
                lValorParametros[9] = ddlCampo8.SelectedValue;
                lValorParametros[10] = ddlCampo9.SelectedValue;
                lValorParametros[11] = ddlCampo10.SelectedValue;
                lValorParametros[12] = ddlCampo11.SelectedValue;
                lValorParametros[13] = ddlCampo12.SelectedValue;
                lValorParametros[14] = ddlCampo13.SelectedValue;
                lValorParametros[15] = ddlCampo14.SelectedValue;
                lValorParametros[16] = ddlCampo15.SelectedValue;
                lValorParametros[17] = ddlCampo16.SelectedValue;
                lValorParametros[18] = ddlCampo17.SelectedValue;
                lValorParametros[19] = ddlCampo18.SelectedValue;
                lValorParametros[20] = ddlCampo19.SelectedValue;
                lValorParametros[21] = ddlCampo20.SelectedValue;
                lValorParametros[22] = ddlCampo21.SelectedValue;
                lValorParametros[23] = ddlCampo22.SelectedValue;
                lValorParametros[24] = ddlCampo23.SelectedValue;
                lValorParametros[25] = ddlCampo24.SelectedValue;
                lValorParametros[26] = ddlCampo25.SelectedValue;
                lValorParametros[27] = ddlCampo26.SelectedValue;
                lValorParametros[28] = ddlCampo27.SelectedValue;
                lValorParametros[29] = ddlCampo28.SelectedValue;
                lValorParametros[30] = ddlCampo29.SelectedValue;
                lValorParametros[31] = ddlCampo30.SelectedValue;
                lValorParametros[32] = ddlEstado.SelectedValue;
                lValorParametros[34] = ddlCampo21A.SelectedValue;
                lValorParametros[35] = ddlCampo21B.SelectedValue;
                lValorParametros[36] = ddlCampo31.SelectedValue; //20210707 trm moneda
                lValorParametros[37] = ddlCampo32.SelectedValue; //20210707 trm moneda
                lValorParametros[38] = ddlCampo33.SelectedValue; //20210707 trm moneda

                SqlDataReader lLector;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCampoMod", lsNombreParametros, lTipoparametros, lValorParametros);
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lblMensaje.Text = lLector["error"].ToString();

                }
                else
                {
                    lblMensaje.Text = "Registro actualizado correctamente";
                    manejo_bloqueo("E", LblCodigoCampo.Text);
                    Listar();
                }
                lLector.Close();
                lConexion.Cerrar();
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Error al actualziar el registro. "+ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        /// Desbloquea el Registro Actualizado
        if (LblCodigoCampo.Text != "")
            manejo_bloqueo("E", LblCodigoCampo.Text);
        Listar();
    }
    /// <summary>
    /// Nombre: dtgComisionista_PageIndexChanged
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de cambio de pagina en el DataGrid
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        lblMensaje.Text = "";
        this.dtgMaestro.CurrentPageIndex = e.NewPageIndex;
        CargarDatos();

    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
    {
        string lCodigoRegistro = "";
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            lCodigoRegistro = this.dtgMaestro.Items[e.Item.ItemIndex].Cells[0].Text;
            ddlCausa.Enabled = false;
            Modificar(lCodigoRegistro);
        }
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("m_campo_modificar", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }


    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_campo_modificar' and llave_registro='codigo_campo=" + lscodigo_registro + "'";
        string lsCondicion1 = "codigo_campo=" + lscodigo_registro.ToString();
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = "m_campo_modificar";
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, "m_campo_modificar", lsCondicion1);
        }
        return true;
    }
    /// <summary>
    /// Nombre: ImgExcel_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a Excel de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgExcel_Click(object sender, ImageClickEventArgs e)
    {
        string[] lValorParametros = { "0", "" };
        string lsParametros = "";

        try
        {
            if (TxtBusCodigoCampo.Text.Trim().Length > 0)
            {
                lValorParametros[0] = TxtBusCodigoCampo.Text.Trim();
                lsParametros += " Código Campo : " + TxtBusCodigoCampo.Text;

            }
            if (ddlBusCausa.SelectedValue !="0")
            {
                lValorParametros[1] = ddlBusCausa.SelectedValue;
                lsParametros += " Causa : " + ddlBusCausa.SelectedItem;

            }
            
            Server.Transfer("../Informes/exportar_reportes.aspx?tipo_export=2&procedimiento=pa_GetCampoMod&nombreParametros=@P_codigo_campo*@P_codigo_causa&valorParametros=" + lValorParametros[0] + "*" + lValorParametros[1] + "&columnas=codigo_campo*descripcion*estado*login_usuario*fecha_hora_actual&titulo_informe=Listado de causas&TituloParametros=" + lsParametros);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
    /// <summary>
    /// Nombre: ImgPdf_Click
    /// Fecha: Agosto 15 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Genera la Exportacion a PDF de la Informacion del Maestro
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImgPdf_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            string lsCondicion = " codigo_campo<> '0'";
            Server.Execute("../Informes/exportar_pdf.aspx?tipo_export=1&nombre_tabla=m_campo_modificar&procedimiento=pa_ValidarExistencia&columnas=codigo_campo*descripcion*estado*login_usuario*fecha_hora_actual&condicion=" + lsCondicion);
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "No se Pudo Generar el Informe.!";
        }

    }
}