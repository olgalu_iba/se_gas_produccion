﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_CartasCorrepondencia.aspx.cs"
    Inherits="BASE_frm_CartasCorrepondencia" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_CartasCorrepondencia.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_CartasCorrepondencia.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_CartasCorrepondencia.aspx?lsIndica=B">Consultar</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                        <td class="tv2">
                        </td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                Width="70%" />
                        </td>
                        <td class="tv2">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor ="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Codigo Carta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoCarta" runat="server" MaxLength="3"></asp:TextBox>
                <asp:Label ID="LblCodigoCarta" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Descripción
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Destino Carta
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlDestinoCarta" runat="server">
                    <asp:ListItem Value="B" Text="Bloqueo"></asp:ListItem>
                    <asp:ListItem Value="O" Text="Orden de Giro"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Tipo Destinatario
            </td>
            <td class="style2">
                <asp:DropDownList ID="ddlTipoDestinatario" runat="server" OnSelectedIndexChanged="ddlTipoDestinatario_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="">Seleccione</asp:ListItem>
                    <asp:ListItem Value="I">Interno</asp:ListItem>
                    <asp:ListItem Value="O">Operador</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server" id="Tr01" visible="false">
            <td class="td1">
                Nombre Destinatario
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNomDestino" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="Tr02" visible="false">
            <td class="td1">
                Cargo Destinatario
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCargoDestino" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr runat="server" id="Tr03" visible="false">
            <td class="td1">
                Area Destinatario
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtAreaDestino" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Texto Carta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtTextoCarta" runat="server" Width="400px" TextMode="MultiLine"
                    Rows="2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Nombre Firma
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtNomFirma" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Cargo Firma
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCargoFirma" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Archivo Firma (El tamaño del archivo debe ser de 200 Ancho x 90 de Alto)
            </td>
            <td class="td2">
                <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" Width="250px" />
                <asp:HiddenField ID="hdfNomArchivo" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="td1">
                Estado
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                    <asp:ListItem Value="A">Activo</asp:ListItem>
                    <asp:ListItem Value="I">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                    ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                    OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                    CausesValidation="false" Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblBuscar" visible="false">
        <tr>
            <td class="td1">
                Codigo Carta
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusCarta" runat="server" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Descripcion
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtBusDescripcion" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="3" align="center">
                <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                    OnClick="imbConsultar_Click" Height="40" />
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td colspan="2">
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                        HeaderStyle-CssClass="th1" PageSize="30">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_carta" HeaderText="Codigo Carta" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="descripcion_carta" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="280px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="destino_carta" HeaderText="Destino" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tipo_destinatario" HeaderText="Tipo Destinatario" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualizacion" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass="">
                            </asp:EditCommandColumn>
                            <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>