﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_FuentePuntoSNT.aspx.cs"
    Inherits="BASE_frm_FuentePuntoSNT" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" bgcolor="#85BF46">
                <table border="0" cellspacing="2" cellpadding="2" align="right">
                    <tr>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_FuentePuntoSNT.aspx?lsIndica=L">Listar</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                        </td>
                        <td class="tv2"></td>
                        <td class="tv2">
                            <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" ForeColor="White"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
        width="80%">
        <tr>
            <td align='center'>
                <div>
                    <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                        OnEditCommand="dtgMaestro_EditCommand" HeaderStyle-CssClass="th1" PageSize="30" OnPageIndexChanged="dtgMaestro_PageIndexChanged">
                        <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                        <ItemStyle CssClass="td2"></ItemStyle>
                        <Columns>
                            <asp:BoundColumn DataField="codigo_campo" HeaderText="Código Campo / Fuente" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_campo" HeaderText="Descripción Campo / Fuente" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="350px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="desc_tipo_campo" HeaderText="Tipo de Campo" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:EditCommandColumn HeaderText="Puntos" EditText="Puntos"></asp:EditCommandColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                        <HeaderStyle CssClass="th1"></HeaderStyle>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblPuntos" visible="false">
        <tr>
            <td class="td1">Campo
            </td>
            <td class="td2" colspan="3">
                <asp:Label ID="lblCodCampo" runat="server"></asp:Label>
                -
                <asp:Label ID="lblDescCampo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="td1">Punto SNT
            </td>
            <td class="td2" colspan="3">
                <asp:DropDownList ID="ddlPunto" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="4" align="center">
                <asp:ImageButton ID="imbCrearTra" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrearTra_Click"
                    Height="40" />
                <asp:ImageButton ID="imbSalirTra" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalirTra_Click"
                    Height="40" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:DataGrid ID="dtgPuntos" runat="server" AutoGenerateColumns="False" AlternatingItemStyle-CssClass="td1"
                    ItemStyle-CssClass="td2" OnEditCommand="dtgPuntos_EditCommand" HeaderStyle-CssClass="th1">
                    <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                    <ItemStyle CssClass="td2"></ItemStyle>
                    <Columns>
                        <asp:BoundColumn DataField="codigo_fuente_campo" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_fuente" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="codigo_campo" HeaderText="Código Punto" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_punto_snt" HeaderText="Desc Punto Snt" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="300px"></asp:BoundColumn>
                        <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                    </Columns>
                    <HeaderStyle CssClass="th1"></HeaderStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
