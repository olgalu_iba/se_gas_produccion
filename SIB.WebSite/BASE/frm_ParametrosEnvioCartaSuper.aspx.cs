﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParametrosEnvioCartaSuper : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros Cartas Superintendencias";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string gsTabla = "m_parametros_cartas_super";
    string sRutaArc = ConfigurationManager.AppSettings["RutaIMG"].ToString();

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: EstablecerPermisosSistema
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para revisar los permisos del sistema y actualizar los Link y Botones de la Pagina.
    /// Modificacion:
    /// </summary>
    //private void EstablecerPermisosSistema()
    //{
    //    Hashtable permisos = DelegadaBase.Servicios.consultarPermisoMenu(goInfo, gsTabla);
    //    imbCrear.Visible = (Boolean)permisos["INSERT"];
    //    imbActualiza.Visible = (Boolean)permisos["UPDATE"];
    //}
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtNoDiaHabilEnvio.Text = lLector["no_dia_habil_envio_mail"].ToString();
                    TxtHoraEnvioMail.Text = lLector["hora_envio_mail"].ToString();
                    TxtTextoCorreo.Text = lLector["texto_correo"].ToString();
                    TxtNombreFirmaCarta.Text = lLector["nombre_firma_carta"].ToString();
                    TxtCargoFirmaCarta.Text = lLector["cargo_firma_carta"].ToString();
                    hdfNomArchivo.Value = lLector["archivo_firma_carta"].ToString();
                    HplArchivo.NavigateUrl = "../Images/" + lLector["archivo_firma_carta"].ToString();
                    TxtObservacion.Text = "";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro por que esta Bloqueado.";

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_parametro","@P_no_dia_habil_envio_mail","@P_hora_envio_mail","@P_texto_correo","@P_nombre_firma_carta","@P_cargo_firma_carta","@P_archivo_firma_carta",
                                          "@P_observacion_cambio","@P_accion"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "", "", "", "", "", "", "1" };
        lblMensaje.Text = "";
        int liValor = 0;
        string sRuta = sRutaArc;
        try
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoDiaHabilEnvio.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo No. Día hábil envío e_mail Superintendencias.<br>";
            }
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";
            if (FuArchivo.FileName.Trim().Length <= 0)
                lblMensaje.Text += "Debe Seleccionar el Archivo de la Firma.<br>";

            if (lblMensaje.Text == "")
            {
                FuArchivo.SaveAs(sRuta + FuArchivo.FileName);

                lValorParametros[1] = TxtNoDiaHabilEnvio.Text.Trim();
                lValorParametros[2] = TxtHoraEnvioMail.Text.Trim();
                lValorParametros[3] = TxtTextoCorreo.Text.Trim();
                lValorParametros[4] = TxtNombreFirmaCarta.Text.Trim();
                lValorParametros[5] = TxtCargoFirmaCarta.Text.Trim();
                lValorParametros[6] = FuArchivo.FileName.Trim();
                lValorParametros[7] = TxtObservacion.Text.Trim();

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosCartaSuper", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presento un Problema en la Creación de Parámetros.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_parametro","@P_no_dia_habil_envio_mail","@P_hora_envio_mail","@P_texto_correo","@P_nombre_firma_carta","@P_cargo_firma_carta","@P_archivo_firma_carta",
                                          "@P_observacion_cambio","@P_accion"
                                      };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", "0", "", "", "", "", "", "", "2" };
        lblMensaje.Text = "";
        int liValor = 0;
        string sRuta = sRutaArc;
        try
        {
            try
            {
                liValor = Convert.ToInt32(TxtNoDiaHabilEnvio.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Invalido en Campo No. Día hábil envío e_mail Superintendencias.<br>";
            }
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                if (FuArchivo.FileName.Trim().Length > 0)
                    FuArchivo.SaveAs(sRuta + FuArchivo.FileName);

                lValorParametros[1] = TxtNoDiaHabilEnvio.Text.Trim();
                lValorParametros[2] = TxtHoraEnvioMail.Text.Trim();
                lValorParametros[3] = TxtTextoCorreo.Text.Trim();
                lValorParametros[4] = TxtNombreFirmaCarta.Text.Trim();
                lValorParametros[5] = TxtCargoFirmaCarta.Text.Trim();
                if (FuArchivo.FileName.Trim().Length > 0)
                    lValorParametros[6] = FuArchivo.FileName.Trim();
                else
                    lValorParametros[6] = hdfNomArchivo.Value;
                lValorParametros[7] = TxtObservacion.Text.Trim();

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosCartaSuper", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presento un Problema en la Actualizacion de los Parámetros.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        //dtgComisionista.DataSource = DelegadaBase.Servicios.LlenarGrilla(lConexion.gObjConexion, "SP_GetActividadEconomica");
        //dtgComisionista.DataBind();

        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistenciaSib", lsTabla, lsCondicion);

        ListItem lItem = new ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            ListItem lItem1 = new ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            lDdl.Items.Add(lItem1);
        }
        lLector.Close();
    }
    /// <summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_cartas_super' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}