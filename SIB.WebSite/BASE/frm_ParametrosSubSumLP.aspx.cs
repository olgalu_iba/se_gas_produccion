﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class BASE_frm_ParametrosSubSumLP : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Parámetros negociaciones de largo plazo";
    /// Declara Clase conexion, para usar clases de ProcesosSql y Validaciones
    clConexion lConexion = null;
    clConexion lConexion1 = null;
    SqlDataReader lLector;
    string gsTabla = "m_parametros_sslp";

    private string lsIndica
    {
        get
        {
            if (ViewState["lsIndica"] == null)
                return "";
            else
                return (string)ViewState["lsIndica"];
        }
        set
        {
            ViewState["lsIndica"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        //Controlador util = new Controlador();
        /*  Se recibe una varibla por POST, para indicar el tipo de evento a ejecutar en la Pagina
         *   lsIndica = N -> Nuevo (Creacion)
         *   lsIndica = L -> Listar Registros (Grilla)
         *   lsIndica = M -> Modidificar
         *   lsIndica = B -> Buscar
         * */

        //Establese los permisos del sistema
        //EstablecerPermisosSistema();
        lConexion = new clConexion(goInfo);
        lConexion1 = new clConexion(goInfo);

        if (!IsPostBack)
        {
            //si la pagina fue llamada por un Hipervinculo o Redirect significa que no es postblack
            if (VerificarExistencia("1=1"))
            {
                Modificar();
            }
            else
            {
                Nuevo();
            }
        }
    }
    /// <summary>
    /// Nombre: Nuevo
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para preparar los controles de la Pagina cuando se ingresa por el Link Nuevo.
    /// Modificacion:
    /// </summary>
    private void Nuevo()
    {
        tblCaptura.Visible = true;
        imbCrear.Visible = true;
        imbActualiza.Visible = false;
        lblTitulo.Text = "Crear " + lsTitulo;
    }
    /// <summary>
    /// Nombre: Modificar
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de Actualizacion del Registro en la base de datos, cuando
    ///              se ingresa por el Link de Modificar.
    /// Modificacion:
    /// </summary>
    /// <param name="modificar"></param>
    private void Modificar()
    {
        try
        {
            lblMensaje.Text = "";
            /// Verificar Si el Registro esta Bloqueado
            if (!manejo_bloqueo("V", ""))
            {
                // Carga informacion de combos
                imbCrear.Visible = false;
                imbActualiza.Visible = true;

                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", gsTabla, " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    TxtFechaIniCont.Text = lLector["fecha_ini_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_cont"].ToString().Substring(0, 2);
                    TxtFechaFinCont.Text = lLector["fecha_fin_cont"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_cont"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_cont"].ToString().Substring(0, 2);
                    TxtAnoDuracion.Text = lLector["min_duracion_cont"].ToString();
                    TxtAnoIniCont.Text = lLector["max_año_inicio_cont"].ToString();
                    TxtFechaIniRem.Text = lLector["fecha_ini_reserva"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_reserva"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_reserva"].ToString().Substring(0, 2);
                    TxtFechaFinRem.Text = lLector["fecha_fin_reserva"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_reserva"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_reserva"].ToString().Substring(0, 2);
                    txtFechaPubRem.Text = lLector["fecha_pub_remanente"].ToString().Substring(6, 4) + "/" + lLector["fecha_pub_remanente"].ToString().Substring(3, 2) + "/" + lLector["fecha_pub_remanente"].ToString().Substring(0, 2);
                    txtFechaAdjRes.Text = lLector["fecha_adj_reserva"].ToString().Substring(6, 4) + "/" + lLector["fecha_adj_reserva"].ToString().Substring(3, 2) + "/" + lLector["fecha_adj_reserva"].ToString().Substring(0, 2);
                    txtFechaPre.Text = lLector["fecha_precio_reserva"].ToString().Substring(6, 4) + "/" + lLector["fecha_precio_reserva"].ToString().Substring(3, 2) + "/" + lLector["fecha_precio_reserva"].ToString().Substring(0, 2);
                    TxtPorcFijoC1.Text = lLector["porc_fijo_c1"].ToString();
                    TxtPorcVarC1.Text = lLector["porc_var_c1"].ToString();
                    TxtPorcFijoC2.Text = lLector["porc_fijo_c2"].ToString();
                    TxtPorcVarC2.Text = lLector["porc_var_c2"].ToString();
                    TxtUrl.Text = lLector["url_ssmp"].ToString();
                    TxtFechaIniCtrl.Text = lLector["fec_ini_ctrl_usr_fin"].ToString().Substring(6, 4) + "/" + lLector["fec_ini_ctrl_usr_fin"].ToString().Substring(3, 2) + "/" + lLector["fec_ini_ctrl_usr_fin"].ToString().Substring(0, 2);
                    TxtFechaFinCtrl.Text = lLector["fec_fin_ctrl_usr_fin"].ToString().Substring(6, 4) + "/" + lLector["fec_fin_ctrl_usr_fin"].ToString().Substring(3, 2) + "/" + lLector["fec_fin_ctrl_usr_fin"].ToString().Substring(0, 2);
                    TxtFechaC1.Text = lLector["fecha_pub_creg_c1"].ToString().Substring(6, 4) + "/" + lLector["fecha_pub_creg_c1"].ToString().Substring(3, 2) + "/" + lLector["fecha_pub_creg_c1"].ToString().Substring(0, 2);
                    TxtFechaC2.Text = lLector["fecha_pub_creg_c2"].ToString().Substring(6, 4) + "/" + lLector["fecha_pub_creg_c2"].ToString().Substring(3, 2) + "/" + lLector["fecha_pub_creg_c2"].ToString().Substring(0, 2);
                    if (lLector["fecha_max_mod_reg_lp"].ToString().Length >= 10)
                        TxtFecMaxReg.Text = lLector["fecha_max_mod_reg_lp"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_mod_reg_lp"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_mod_reg_lp"].ToString().Substring(0, 2);
                    else
                        TxtFecMaxReg.Text = "";
                    txtFirmeMin.Text = lLector["firmeza_minima"].ToString();  //20171020 rq051-17
                    txtPorcCntMax.Text = lLector["porc_max_contratos"].ToString(); //20171020 rq051-17
                    TxtObservacion.Text = "";
                    //20190404 rq018-19 fase II
                    if (lLector["fecha_max_registro"].ToString().Length >= 10)
                        txtFechaMaxRegLp.Text = lLector["fecha_max_registro"].ToString().Substring(6, 4) + "/" + lLector["fecha_max_registro"].ToString().Substring(3, 2) + "/" + lLector["fecha_max_registro"].ToString().Substring(0, 2);
                    else
                        txtFechaMaxRegLp.Text = "";
                    if (lLector["fecha_ini_no_reg"].ToString().Length >= 10)
                        txtFechaIniIni.Text = lLector["fecha_ini_no_reg"].ToString().Substring(6, 4) + "/" + lLector["fecha_ini_no_reg"].ToString().Substring(3, 2) + "/" + lLector["fecha_ini_no_reg"].ToString().Substring(0, 2);
                    else
                        txtFechaIniIni.Text = "";
                    if (lLector["fecha_fin_no_reg"].ToString().Length >= 10)
                        txtFechaFinIni.Text = lLector["fecha_fin_no_reg"].ToString().Substring(6, 4) + "/" + lLector["fecha_fin_no_reg"].ToString().Substring(3, 2) + "/" + lLector["fecha_fin_no_reg"].ToString().Substring(0, 2);
                    else
                        txtFechaFinIni.Text = "";
                    //20190404 fin rq018-19 fase II
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                /// Bloquea el Registro a Modificar
                manejo_bloqueo("A", "");
            }
            else
            {
                tblCaptura.Visible = false;
                lblMensaje.Text = "No se Puede editar el Registro porque está Bloqueado."; //20190404 rq018-19 fase II

            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
        if (lblMensaje.Text == "")
        {
            tblCaptura.Visible = true;
            lblTitulo.Text = "Modificar " + lsTitulo;
        }
    }
    /// <summary>
    /// Nombre: imbCrear_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Crear.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbCrear_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_ini_cont","@P_fecha_fin_cont","@P_min_duracion_cont","@P_max_año_inicio_cont","@P_fecha_pub_remanente","@P_fecha_ini_reserva", "@P_fecha_fin_reserva",
                                          "@p_fecha_adj_reserva","@p_fecha_precio_reserva","@P_porc_fijo_c1","@P_porc_var_c1","@P_porc_fijo_c2","@P_porc_var_c2",
                                          "@P_url_ssmp","@P_fec_ini_ctrl_usr_fin","@P_fec_fin_ctrl_usr_fin","@P_fecha_pub_creg_c1","@P_fecha_pub_creg_c2","@P_observaciones", "@P_fecha_max_mod_reg_lp",
                                        "@P_firmeza_minima", "@P_porc_max_contratos",  //20171020 rq051-17
                                        "@P_fecha_max_registro", "@P_fecha_ini_no_reg","@P_fecha_fin_no_reg"};  //20190404 rq018-19 fase II
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                         SqlDbType.Decimal, SqlDbType.Decimal, //20171020 rq051-17
                                         SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar};  //20190404 rq018-19 fase II

        string[] lValorParametros = { "", "", "0", "0", "", "", "", "", "", "0", "0", "0", "0", "", "", "", "", "", "", "", "0", "0", "", "", "" }; //20171020 rq051-17 //20190404 rq018-19 fase II
        lblMensaje.Text = "";
        string lsError;
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        decimal ldValorI = 0;
        decimal ldValorF = 0;

        try
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniCont.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial de ingreso de contratos de largo plazo.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinCont.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha Final de ingreso de contratos de largo plazo.<br>";

            }

            if (lblMensaje.Text == "")
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final de ingreso de contratos de largo plazo no puede ser menor a la Fecha Inicial.<br>";
            lsError = "";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniRem.Text.Trim());
            }
            catch (Exception ex)
            {
                lsError += "Valor Inválido en Campo Fecha Inicial de reserva de cantidad remanente.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinRem.Text.Trim());
            }
            catch (Exception ex)
            {
                lsError += "Valor Inválido en Campo Fecha Final de reserva de cantidad remanente.<br>";

            }
            if (lsError == "")
            {
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final de reserva de cantidad remanente no puede ser menor a la Fecha Inicial.<br>";
            }
            else
                lblMensaje.Text += lsError;
            try
            {
                ldFechaI = Convert.ToDateTime(txtFechaPubRem.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha mínima de publicación de la PTDVF/CIDVF remanente.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(txtFechaAdjRes.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha de adjudicación de las cantidades reservadas.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(txtFechaPre.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha de cálculo de precio de las operaciones de reserva.<br>";
            }
            lsError = "";
            if (TxtPorcFijoC1.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcFijoC1.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje fijo de modalidad C1 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI < 0 || ldValorI > 100)
                        lsError += "El porcentaje fijo de modalidad C1 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje fijo de modalidad C1 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje fijo de modalidad C1 <br>";

            if (TxtPorcVarC1.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcVarC1.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje variable de modalidad C1 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorF = Convert.ToDecimal(sOferta);
                    if (ldValorF < 0 || ldValorF > 100)
                        lsError += "El porcentaje variable de modalidad C1 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje variable de modalidad C1 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje variable de modalidad C1 <br>";

            if (lsError == "")
            {
                if ((ldValorI + ldValorF) > 100 || (ldValorI + ldValorF) < 100)
                    lblMensaje.Text += "La suma de los porcentajes fijo y variable de la modalidad C1 debe ser 100 <br>";
            }
            else
                lblMensaje.Text += lsError;

            lsError = "";
            if (TxtPorcFijoC2.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcFijoC2.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje fijo de modalidad C2 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI < 0 || ldValorI > 100)
                        lsError += "El porcentaje fijo de modalidad C2 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje fijo de modalidad C2 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje fijo de modalidad C2 <br>";

            if (TxtPorcVarC2.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcVarC2.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje variable de modalidad C2 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorF = Convert.ToDecimal(sOferta);
                    if (ldValorF < 0 || ldValorF > 100)
                        lsError += "El porcentaje variable de modalidad C2 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje variable de modalidad C2 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje variable de modalidad C2 <br>";

            if (lsError == "")
            {
                if ((ldValorI + ldValorF) > 100 || (ldValorI + ldValorF) < 100)
                    lblMensaje.Text += "La suma de los porcentajes fijo y variable de la modalidad C2 debe ser 100 <br>";
            }
            else
                lblMensaje.Text += lsError;

            if (TxtUrl.Text.Trim() == "")
                lblMensaje.Text += " Debe digitar la URL para la conexión a la subasta C1 y C2. <br>";

            lsError = "";
            if (TxtFechaIniCtrl.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha Inicial de entrega a usuarios finales para control de reservas.<br>";
            else
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIniCtrl.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en Campo Fecha Inicial de entrega a usuarios finales para control de reservas.<br>";
                }
            if (TxtFechaFinCtrl.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha Final de entrega a usuarios finales para control de reservas.<br>";
            else
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFinCtrl.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en Campo Fecha Final de entrega a usuarios finales para control de reservas.<br>";

                }
            if (lsError == "")
            {
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final de reserva de entrega a usuarios finales para control de reservas no puede ser menor a la Fecha Inicial.<br>";
            }
            else
                lblMensaje.Text += lsError;

            lsError = "";
            if (TxtFechaC1.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha de publicación del informe de la CREG de la subasta C1.<br>";
            else
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaC1.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en el Campo Fecha de publicación del informe de la CREG de la subasta C1.<br>";
                }
            if (TxtFechaC2.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha de publicación del informe de la CREG de la subasta C2.<br>";
            else
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaC2.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en el Campo Fecha de publicación del informe de la CREG de la subasta C2.<br>";
                }
            if (lsError != "")
                lblMensaje.Text += lsError;
            if (TxtFecMaxReg.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha máxima de modificación de registro o eliminación de contratos de largo plazo.<br>";
            else
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFecMaxReg.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el Campo Fecha máxima de modificación de registro o eliminación de contratos de largo plazo.<br>";
                }
            //20171020 rq051-17
            if (txtFirmeMin.Text.Trim().Length > 0)
            {
                string sOferta = txtFirmeMin.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lblMensaje.Text += "Se permiten máximo 2 decimales en el valor mínimo de firmeza mínima<br>";
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI > 999)
                        lblMensaje.Text += "El valor mínimo de firmeza mínima no es válido<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El valor mínimo de firmeza mínima no es válido<br>";
                }
            }
            else
                lblMensaje.Text += "Debe Ingresar el valor mínimo de firmeza mínima<br>";
            //20171020 rq051-17
            if (txtPorcCntMax.Text.Trim().Length > 0)
            {
                string sOferta = txtPorcCntMax.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lblMensaje.Text += "Se permiten máximo 2 decímales en el porcentaje sobre la cantidad máxima de los contratos de suministro<br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI > 100)
                        lblMensaje.Text += "El porcentaje sobre la cantidad máxima de los contratos de suministro no es válido<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El porcentaje sobre la cantidad máxima de los contratos de suministro no es válido<br>";
                }
            }
            else
                lblMensaje.Text += "Debe Ingresar el porcentaje  sobre la cantidad máxima de los contratos de suministro<br>";

            //20190404 rq018-19 fase II
            if (txtFechaMaxRegLp.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(txtFechaMaxRegLp.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha máxima de registro  de negociaciones de largo plazo. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha máxima de registro de negociaciones de largo plazo. <br>";
            if (txtFechaIniIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(txtFechaIniIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
            if (txtFechaFinIni.Text.Trim().Length > 0)
            {
                try
                {
                    if (txtFechaFinIni.Text.Trim().Length > 0)
                    {
                        ldFechaF = Convert.ToDateTime(txtFechaFinIni.Text);
                        if (ldFechaI > ldFechaF)
                            lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>";
                    }
                    else
                        lblMensaje.Text += "Debe digitar la fecha inicial antes que la final. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha final. <br>";
            //20190404 fin rq018-19 fase II
            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaIniCont.Text.Trim();
                lValorParametros[1] = TxtFechaFinCont.Text.Trim();
                lValorParametros[2] = TxtAnoDuracion.Text.Trim();
                lValorParametros[3] = TxtAnoIniCont.Text.Trim();
                lValorParametros[4] = txtFechaPubRem.Text.Trim();
                lValorParametros[5] = TxtFechaIniRem.Text.Trim();
                lValorParametros[6] = TxtFechaFinRem.Text.Trim();
                lValorParametros[7] = txtFechaAdjRes.Text.Trim();
                lValorParametros[8] = txtFechaPre.Text.Trim();
                lValorParametros[9] = TxtPorcFijoC1.Text.Trim();
                lValorParametros[10] = TxtPorcVarC1.Text.Trim();
                lValorParametros[11] = TxtPorcFijoC2.Text.Trim();
                lValorParametros[12] = TxtPorcVarC2.Text.Trim();
                lValorParametros[13] = TxtUrl.Text.Trim();
                lValorParametros[14] = TxtFechaIniCtrl.Text.Trim();
                lValorParametros[15] = TxtFechaFinCtrl.Text.Trim();
                lValorParametros[16] = TxtFechaC1.Text.Trim();
                lValorParametros[17] = TxtFechaC2.Text.Trim();
                lValorParametros[18] = TxtObservacion.Text.Trim();
                lValorParametros[19] = TxtFecMaxReg.Text.Trim();
                lValorParametros[20] = txtFirmeMin.Text.Trim(); //20171020 rq051-17
                lValorParametros[21] = txtPorcCntMax.Text.Trim(); //20171020 rq051-17 
                lValorParametros[22] = txtFechaMaxRegLp.Text.Trim(); //20190404 rq018-19 fase II
                lValorParametros[23] = txtFechaIniIni.Text.Trim(); //20190404 rq018-19 fase II
                lValorParametros[24] = txtFechaFinIni.Text.Trim(); //20190404 rq018-19 fase II

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosSslp", lsNombreParametros, lTipoparametros, lValorParametros))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Creación de Parámetros par ala negociación de largo plazo.!";
                    lConexion.Cerrar();
                }
                else
                {
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de actualizacion de la Informacion en la base de datos, cando se da click
    ///              en el Boton Actualizar.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbActualiza_Click(object sender, ImageClickEventArgs e)
    {
        string[] lsNombreParametros = { "@P_fecha_ini_cont","@P_fecha_fin_cont","@P_min_duracion_cont","@P_max_año_inicio_cont","@P_fecha_pub_remanente","@P_fecha_ini_reserva", "@P_fecha_fin_reserva",
                                          "@p_fecha_adj_reserva","@p_fecha_precio_reserva","@P_porc_fijo_c1","@P_porc_var_c1","@P_porc_fijo_c2","@P_porc_var_c2",
                                          "@P_url_ssmp","@P_fec_ini_ctrl_usr_fin","@P_fec_fin_ctrl_usr_fin","@P_fecha_pub_creg_c1","@P_fecha_pub_creg_c2","@P_observaciones", "@P_fecha_max_mod_reg_lp",
                                        "@P_firmeza_minima", "@P_porc_max_contratos",  //20171020 rq051-17
                                        "@P_fecha_max_registro", "@P_fecha_ini_no_reg","@P_fecha_fin_no_reg"};  //20190404 rq018-19 fase II
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.VarChar, SqlDbType.VarChar,SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal, SqlDbType.Decimal,
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar,
                                        SqlDbType.Decimal, SqlDbType.Decimal, //20171020 rq051-17
                                        SqlDbType.VarChar,SqlDbType.VarChar,SqlDbType.VarChar}; //20190404 rq018-19 fase II

        string[] lValorParametros = { "", "", "0", "0", "", "", "", "", "", "0", "0", "0", "0", "", "", "", "", "", "", "", "0", "0", "","",""}; //20171020 rq051-17 //20190404 rq018-19 fase II

        lblMensaje.Text = "";
        string lsError;
        DateTime ldFechaI = DateTime.Now;
        DateTime ldFechaF = DateTime.Now;
        decimal ldValorI = 0;
        decimal ldValorF = 0;

        try
        {
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniCont.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha Inicial de ingreso de contratos de largo plazo.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinCont.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en Campo Fecha Final de ingreso de contratos de largo plazo.<br>";

            }

            if (lblMensaje.Text == "")
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final de ingreso de contratos de largo plazo no puede ser menor a la Fecha Inicial.<br>";
            lsError = "";
            try
            {
                ldFechaI = Convert.ToDateTime(TxtFechaIniRem.Text.Trim());
            }
            catch (Exception ex)
            {
                lsError += "Valor Inválido en Campo Fecha Inicial de reserva de cantidad remanente.<br>";
            }
            try
            {
                ldFechaF = Convert.ToDateTime(TxtFechaFinRem.Text.Trim());
            }
            catch (Exception ex)
            {
                lsError += "Valor Inválido en Campo Fecha Final de reserva de cantidad remanente.<br>";

            }
            if (lsError == "")
            {
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final de reserva de cantidad remanente no puede ser menor a la Fecha Inicial.<br>";
            }
            else
                lblMensaje.Text += lsError;
            try
            {
                ldFechaI = Convert.ToDateTime(txtFechaPubRem.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha mínima de publicación de la PTDVF/CIDVF remanente.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(txtFechaAdjRes.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha de adjudicación de las cantidades reservadas.<br>";
            }
            try
            {
                ldFechaI = Convert.ToDateTime(txtFechaPre.Text.Trim());
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Valor Inválido en el Campo Fecha de cálculo de precio de las operaciones de reserva.<br>";
            }
            lsError = "";
            if (TxtPorcFijoC1.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcFijoC1.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje fijo de modalidad C1 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI < 0 || ldValorI > 100)
                        lsError += "El porcentaje fijo de modalidad C1 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje fijo de modalidad C1 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje fijo de modalidad C1 <br>";

            if (TxtPorcVarC1.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcVarC1.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje variable de modalidad C1 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorF = Convert.ToDecimal(sOferta);
                    if (ldValorF < 0 || ldValorF > 100)
                        lsError += "El porcentaje variable de modalidad C1 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje variable de modalidad C1 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje variable de modalidad C1 <br>";
            if (lsError == "")
            {
                if ((ldValorI + ldValorF) > 100 || (ldValorI + ldValorF) < 100)
                    lblMensaje.Text += "La suma de los porcentajes fijo y variable de la modalidad C1 debe ser 100 <br>";
            }
            else
                lblMensaje.Text += lsError;

            lsError = "";
            if (TxtPorcFijoC2.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcFijoC2.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje fijo de modalidad C2 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI < 0 || ldValorI > 100)
                        lsError += "El porcentaje fijo de modalidad C2 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje fijo de modalidad C2 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje fijo de modalidad C2 <br>";

            if (TxtPorcVarC2.Text.Trim().Length > 0)
            {
                string sOferta = TxtPorcVarC2.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lsError += "Se permiten máximo 2 decímales en el porcentaje variable de modalidad C2 <br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorF = Convert.ToDecimal(sOferta);
                    if (ldValorF < 0 || ldValorF > 100)
                        lsError += "El porcentaje variable de modalidad C2 debe ser estar entre 0 y 100<br>";
                }
                catch (Exception ex)
                {
                    lsError += "El porcentaje variable de modalidad C2 no es válido<br>";
                }
            }
            else
                lsError += "Debe Ingresar el porcentaje variable de modalidad C2 <br>";

            if (lsError == "")
            {
                if ((ldValorI + ldValorF) > 100 || (ldValorI + ldValorF) < 100)
                    lblMensaje.Text += "La suma de los porcentajes fijo y variable de la modalidad C2 debe ser 100 <br>";
            }
            else
                lblMensaje.Text += lsError;

            if (TxtUrl.Text.Trim() == "")
                lblMensaje.Text += " Debe digitar la URL para la conexión a la subasta C1 y C2. <br>";

            lsError = "";
            if (TxtFechaIniCtrl.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha Inicial de entrega a usuarios finales para control de reservas.<br>";
            else
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaIniCtrl.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en Campo Fecha Inicial de entrega a usuarios finales para control de reservas.<br>";
                }
            if (TxtFechaFinCtrl.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha Final de entrega a usuarios finales para control de reservas.<br>";
            else
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaFinCtrl.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en Campo Fecha Final de entrega a usuarios finales para control de reservas.<br>";

                }
            if (lsError == "")
            {
                if (ldFechaF < ldFechaI)
                    lblMensaje.Text += "La Fecha Final de reserva de entrega a usuarios finales para control de reservas no puede ser menor a la Fecha Inicial.<br>";
            }
            else
                lblMensaje.Text += lsError;

            lsError = "";
            if (TxtFechaC1.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha de publicación del informe de la CREG de la subasta C1.<br>";
            else
                try
                {
                    ldFechaI = Convert.ToDateTime(TxtFechaC1.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en el Campo Fecha de publicación del informe de la CREG de la subasta C1.<br>";
                }
            if (TxtFechaC2.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha de publicación del informe de la CREG de la subasta C2.<br>";
            else
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFechaC2.Text.Trim());
                }
                catch (Exception ex)
                {
                    lsError += "Valor Inválido en el Campo Fecha de publicación del informe de la CREG de la subasta C2.<br>";
                }
            if (lsError != "")
                lblMensaje.Text += lsError;

            if (TxtFecMaxReg.Text.Trim() == "")
                lsError += "Debe ingresar la Fecha máxima de modificación de registro o eliminación de contratos de largo plazo.<br>";
            else
                try
                {
                    ldFechaF = Convert.ToDateTime(TxtFecMaxReg.Text.Trim());
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el Campo Fecha máxima de modificación de registro o eliminación de contratos de largo plazo.<br>";
                }
            //20171020 rq051-17
            if (txtFirmeMin.Text.Trim().Length > 0)
            {
                string sOferta = txtFirmeMin.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lblMensaje.Text += "Se permiten máximo 2 decímales en el valor mínimo de firmeza mínima<br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI > 999)
                        lblMensaje.Text += "El valor mínimo de firmeza mínima no es válido<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El valor mínimo de firmeza mínima no es válido<br>";
                }
            }
            else
                lblMensaje.Text += "Debe Ingresar el valor mínimo de firmeza mínima<br>";
            //20171020 rq051-17
            if (txtPorcCntMax.Text.Trim().Length > 0)
            {
                string sOferta = txtPorcCntMax.Text.Replace(",", "");
                int iPos = sOferta.IndexOf(".");
                if (iPos > 0)
                    if (sOferta.Length - iPos > 3)
                        lblMensaje.Text += "Se permiten máximo 2 decímales en el porcentaje sobre la cantidad máxima de los contratos de suministro<br>"; //20190404 rq018-19 fase II
                try
                {
                    ldValorI = Convert.ToDecimal(sOferta);
                    if (ldValorI > 100)
                        lblMensaje.Text += "El porcentaje sobre la cantidad máxima de los contratos de suministro no es válido<br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "El porcentaje sobre la cantidad máxima de los contratos de suministro no es válido<br>";
                }
            }
            else
                lblMensaje.Text += "Debe Ingresar el porcentaje  sobre la cantidad máxima de los contratos de suministro<br>";

            //20190404 rq018-19 fase II
            if (txtFechaMaxRegLp.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(txtFechaMaxRegLp.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha máxima de registro  de negociaciones de largo plazo. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha máxima de registro de negociaciones de largo plazo. <br>";
            if (txtFechaIniIni.Text.Trim().Length > 0)
            {
                try
                {
                    ldFechaI = Convert.ToDateTime(txtFechaIniIni.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Inicial. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha Inicial. <br>";
            if (txtFechaFinIni.Text.Trim().Length > 0)
            {
                try
                {
                    if (txtFechaFinIni.Text.Trim().Length > 0)
                    {
                        ldFechaF = Convert.ToDateTime(txtFechaFinIni.Text);
                        if (ldFechaI > ldFechaF)
                            lblMensaje.Text += "La Fecha Final NO puede ser Menor que la Fecha de Inicial. <br>";
                    }
                    else
                        lblMensaje.Text += "Debe digitar la fecha inicial antes que la final. <br>";
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Formato Inválido en el Campo Fecha Final. <br>";
                }
            }
            else
                lblMensaje.Text += "Debe digitar la Fecha final. <br>";
            //20190404 fin rq018-19 fase II

            if (TxtObservacion.Text == "")
                lblMensaje.Text += " Debe digitar las observaciones para el registro. <br>";

            if (lblMensaje.Text == "")
            {
                lValorParametros[0] = TxtFechaIniCont.Text.Trim();
                lValorParametros[1] = TxtFechaFinCont.Text.Trim();
                lValorParametros[2] = TxtAnoDuracion.Text.Trim();
                lValorParametros[3] = TxtAnoIniCont.Text.Trim();
                lValorParametros[4] = txtFechaPubRem.Text.Trim();
                lValorParametros[5] = TxtFechaIniRem.Text.Trim();
                lValorParametros[6] = TxtFechaFinRem.Text.Trim();
                lValorParametros[7] = txtFechaAdjRes.Text.Trim();
                lValorParametros[8] = txtFechaPre.Text.Trim();
                lValorParametros[9] = TxtPorcFijoC1.Text.Trim();
                lValorParametros[10] = TxtPorcVarC1.Text.Trim();
                lValorParametros[11] = TxtPorcFijoC2.Text.Trim();
                lValorParametros[12] = TxtPorcVarC2.Text.Trim();
                lValorParametros[13] = TxtUrl.Text.Trim();
                lValorParametros[14] = TxtFechaIniCtrl.Text.Trim();
                lValorParametros[15] = TxtFechaFinCtrl.Text.Trim();
                lValorParametros[16] = TxtFechaC1.Text.Trim();
                lValorParametros[17] = TxtFechaC2.Text.Trim();
                lValorParametros[18] = TxtObservacion.Text.Trim();
                lValorParametros[19] = TxtFecMaxReg.Text.Trim();
                lValorParametros[20] = txtFirmeMin.Text.Trim();  //20171020 rq051-17
                lValorParametros[21] = txtPorcCntMax.Text.Trim(); //20171020 rq051-17
                lValorParametros[22] = txtFechaMaxRegLp.Text.Trim(); //20190404 rq018-19 fase II
                lValorParametros[23] = txtFechaIniIni.Text.Trim(); //20190404 rq018-19 fase II
                lValorParametros[24] = txtFechaFinIni.Text.Trim(); //20190404 rq018-19 fase II

                lConexion.Abrir();
                if (!DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetParametrosSslp", lsNombreParametros, lTipoparametros, lValorParametros, goInfo))
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización de los Parámetros de las negociaciones de largo plazo.!" + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    manejo_bloqueo("E", "");
                    lblMensaje.Text = "";
                    Response.Redirect("~/WebForms/Home.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            /// Desbloquea el Registro Actualizado
            manejo_bloqueo("E", "");
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Nombre: imbActualiza_Click
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar el proceso de salir de la pantalla cuando se da click
    ///              en el Boton Salir.
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbSalir_Click(object sender, ImageClickEventArgs e)
    {
        manejo_bloqueo("E", "");
        lblMensaje.Text = "";
        Response.Redirect("~/WebForms/Home.aspx");
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia(gsTabla, lswhere, goInfo);
    }
    /// </summary>
    /// Nombre: manejo_bloqueo
    /// Fecha: Agosto 15 de 2008
    /// Creador: Olga Lucia ibañez
    /// Descripcion: Metodo para validar, crear y borrar los bloqueos
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool manejo_bloqueo(string lsIndicador, string lscodigo_registro)
    {
        string lsCondicion = "nombre_tabla='m_parametros_sslp' ";
        string lsCondicion1 = "";
        if (lsIndicador == "V")
        {
            return DelegadaBase.Servicios.ValidarExistencia("a_bloqueo_registro", lsCondicion, goInfo);
        }
        if (lsIndicador == "A")
        {
            a_bloqueo_registro lBloqueoRegistro = new a_bloqueo_registro();
            lBloqueoRegistro.nombre_tabla = gsTabla;
            lBloqueoRegistro.llave_registro = lsCondicion1;
            DelegadaBase.Servicios.guardar_a_bloqueo_registro(goInfo, lBloqueoRegistro);
        }
        if (lsIndicador == "E")
        {
            DelegadaBase.Servicios.borrar_a_bloqueo_registro(goInfo, gsTabla, lsCondicion1);
        }
        return true;
    }
}