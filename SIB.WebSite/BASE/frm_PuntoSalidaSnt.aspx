﻿
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_PuntoSalidaSnt.aspx.cs"
    Inherits="BASE_frm_PuntoSalidaSnt" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right" bgcolor="#85BF46">
                    <table border="0" cellspacing="2" cellpadding="2" align="right">
                        <tr>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkNuevo" runat="server" NavigateUrl="~/BASE/frm_PuntoSalidaSnt.aspx?lsIndica=N">Nuevo</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkListar" runat="server" NavigateUrl="frm_PuntoSalidaSnt.aspx?lsIndica=L">Listar</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkBuscar" runat="server" NavigateUrl="frm_PuntoSalidaSnt.aspx?lsIndica=B">Consultar</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:HyperLink ID="hlkSalir" runat="server" NavigateUrl="~/WebForms/Parametros/frm_parametros.aspx?idParametros=999">Salir</asp:HyperLink>
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:ImageButton ID="ImgExcel" runat="server" ImageUrl="~/Images/excel.gif" OnClick="ImgExcel_Click" />
                            </td>
                            <td class="tv2"></td>
                            <td class="tv2">
                                <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/Images/Pdf.png" OnClick="ImgPdf_Click"
                                    Width="70%" />
                            </td>
                            <td class="tv2"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
            id="tblTitulo">
            <tr>
                <td align="center" class="th3">
                    <asp:Label ID="lblTitulo" runat="server" ForeColor ="White" ></asp:Label>
                </td>
            </tr>
        </table>
        <br /><br /><br />
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
            id="tblCaptura">
            <tr>
                <td class="td1">Código Punto Salida <%--20180312 rq008-18--%>
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtCodigoPunto" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:Label ID="LblCodigoPunto" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="td1" colspan="1">Descripción
                </td>
                <td class="td2" colspan="1">
                    <asp:TextBox ID="TxtDescripcion" runat="server" MaxLength="100" Width="280px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="td1">Tramo
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlTramo" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20180312 rq008-18--%>
            <tr>
                <td class="td1">departamento
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlDepto" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDepto_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20180312 rq008-18--%>
            <tr>
                <td class="td1">Ciudad
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlCiudad" runat="server" OnSelectedIndexChanged="ddlCiudad_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20190306 rq013-19--%>
            <tr>
                <td class="td1">Centro Poblado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlCentro" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="td1">Maneja telemetría
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlTelemetria" runat="server">
                        <asp:ListItem Value="N">No</asp:ListItem>
                        <asp:ListItem Value="S">Si</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--20190306 finn rq013-19--%>
            <tr>
                <td class="td1">Estado
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlEstado" runat="server">
                        <asp:ListItem Value="A">Activo</asp:ListItem>
                        <asp:ListItem Value="I">Inactivo</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="2" align="center">
                    <asp:ImageButton ID="imbCrear" runat="server" ImageUrl="~/Images/Crear.png" OnClick="imbCrear_Click"
                        ValidationGroup="comi" Height="40" />
                    <asp:ImageButton ID="imbActualiza" runat="server" ImageUrl="~/Images/Actualizar.png"
                        OnClick="imbActualiza_Click" ValidationGroup="comi" Height="40" />
                    <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClick="imbSalir_Click"
                        CausesValidation="false" Height="40" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:ValidationSummary ID="VsComisionista" runat="server" ValidationGroup="comi" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="HdnLoginCreacion" runat="server" />
        <asp:HiddenField ID="HdnFechaCreacion" runat="server" />
        <asp:HiddenField ID="HdnDepto" runat="server" />
        <%--20180312 rq008-18--%>
        <asp:HiddenField ID="HdnCiudad" runat="server" />
        <%--20180312 rq008-18--%>
        <asp:HiddenField ID="HdnCentro" runat="server" />
        <br />
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblBuscar" visible="false">
            <tr>
                <td class="td1">Código Punto Salida <%--20180312 rq008-18--%>
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusCodPunto" runat="server" autocomplete="off"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FTBETxtBusCodFase" runat="server" TargetControlID="TxtBusCodPunto"
                        FilterType="Custom, Numbers">
                    </cc1:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td class="td1">Descripción
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtBusDescripcion" runat="server" autocomplete="off"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="td1">Tramo
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusTramo" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="3" align="center">
                    <asp:ImageButton ID="imbConsultar" runat="server" ImageUrl="~/Images/Buscar.png"
                        OnClick="imbConsultar_Click" Height="40" />
                </td>
            </tr>
        </table>
        <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
            id="tblMensaje">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        <table border='0' align='center' cellspacing='3' runat="server" id="tblgrilla" visible="false"
            width="80%">
            <tr>
                <td colspan="2">
                    <div>
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                            OnEditCommand="dtgMaestro_EditCommand" OnPageIndexChanged="dtgMaestro_PageIndexChanged"
                            HeaderStyle-CssClass="th1" PageSize="30">
                            <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                            <ItemStyle CssClass="td2"></ItemStyle>
                            <Columns>
                                <%--20180312 rq008-18--%>
                                <asp:BoundColumn DataField="codigo_punto_salida" HeaderText="Código Punto Salida"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20180312 rq008-18--%>
                                <asp:BoundColumn DataField="descripcion" HeaderText="Descripción" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="250px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_tramo" HeaderText="Código Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_tramo" HeaderText="Desc. Tramo" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_depto" HeaderText="Código Depto." ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_departamento" HeaderText="Departamento" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_ciudad" HeaderText="Código Ciudad" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_ciudad" HeaderText="Municipio" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="codigo_centro" HeaderText="Código Centro Poblado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="desc_centro" HeaderText="Nombre Centro Poblado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="maneja_telemetria" HeaderText="Maneja Telemetría" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20190306 fin rq013-19--%>
                                <asp:BoundColumn DataField="estado" HeaderText="Estado" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--20180312 rq008-18--%>
                                <asp:BoundColumn DataField="login_usuario" HeaderText="Usuario Actualización" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar" HeaderStyle-CssClass=""></asp:EditCommandColumn>
                                <asp:EditCommandColumn HeaderText="Eliminar" EditText="Eliminar"></asp:EditCommandColumn>
                            </Columns>
                            <PagerStyle Mode="NumericPages" Position="TopAndBottom" />
                            <HeaderStyle CssClass="th1"></HeaderStyle>
                        </asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
</asp:Content>