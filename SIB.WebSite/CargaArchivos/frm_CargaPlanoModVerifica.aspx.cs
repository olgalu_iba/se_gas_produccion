﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaPlanoModVerifica : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            //Master.Titulo = "Subasta";

            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsCadenaArchivo = new StringBuilder();
            var lsRutaArchivo = "";
            var lsRutaArchivoU = "";
            string lsNombre;
            var lsNombreU = "";
            string[] lsErrores = { "", "" };
            string[] lsErrores1 = { "", "" };
            var oTransOK = true;
            var oCargaOK = false;
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_archivoUsuarios", "@P_ruta_ftp" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
            object[] lValorParametrosO = { "", "", strRutaFTP };

            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoContratos", CultureInfo.CurrentCulture)?.ToString());
                oTransOK = false;
            }
            if (FuArchivoUsuarios.FileName != "")
            {
                lsNombreU = DateTime.Now.Millisecond + FuArchivoUsuarios.FileName;
                try
                {
                    lsRutaArchivoU = strRutaCarga + lsNombreU;
                    FuArchivoUsuarios.SaveAs(lsRutaArchivoU);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoUsuarios", CultureInfo.CurrentCulture)?.ToString());
                    oTransOK = false;
                }
            }
            /// Realiza las Validaciones de los Archivos
            if (oTransOK)
            {
                try
                {
                    // Realiza las Validaciones de los Archivos
                    if (FuArchivo.FileName != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoContratosRequerido", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }
                    if (FuArchivoUsuarios.FileName != "")
                        lsErrores = ValidarArchivoUsuarios(lsRutaArchivoU);
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoUsuariosFinalesRequerido", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }
                    if (lsErrores[0] == "")
                    {
                        oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);
                        if (FuArchivoUsuarios.FileName != "")
                            oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivoU, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombreU, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);

                        if (oCargaOK)
                        {
                            lValorParametrosO[0] = lsNombre;
                            lValorParametrosO[1] = lsNombreU;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoVerifContHist";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                }
                            }
                            else
                            {
                                Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                    }
                    //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                    NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                    LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
                }
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 17)) //20160607 campo o fuente //20170601 rq020-17 //20170816 rq036-17
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 17 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20160607 campo o fuente  //20170601 rq020-17 //20170816 rq036-17
                    }
                    else
                    {
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Operación {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Contrato definitivo {" + oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        /// Valida Fecha de Negociacion
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(2).ToString());
                            lsFecha = oArregloLinea.GetValue(2).ToString().Split('/');
                            if (lsFecha[0].Trim().Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha de Negociación {" + oArregloLinea.GetValue(2) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de Negociación {" + oArregloLinea.GetValue(2) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Validar Codigo Punto de Entega
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(3).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto de Entrega / Ruta  {" + oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Codigo Modalidad Contractual
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(4).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Modalidad Contractual {" + oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Cantiad
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(5).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" + oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Precio
                        try
                        {
                            ldValor = Convert.ToDecimal(oArregloLinea.GetValue(6).ToString().Trim());
                            lsFecha = oArregloLinea.GetValue(6).ToString().Trim().Split('.');
                            if (lsFecha.Length > 1)
                            {
                                if (lsFecha[1].Trim().Length > 2)
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(6) + "}, debe tener máximo 2 decimales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(6) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha Inicial
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                            lsFecha = oArregloLinea.GetValue(7).ToString().Split('/');
                            if (lsFecha[0].Trim().Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha Inicial {" + oArregloLinea.GetValue(7) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de Inicial {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha Final
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(8).ToString());
                            lsFecha = oArregloLinea.GetValue(8).ToString().Split('/');
                            if (lsFecha[0].Trim().Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha Final {" + oArregloLinea.GetValue(8) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de Final {" + oArregloLinea.GetValue(8) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Sentido del Flujo
                        if (oArregloLinea.GetValue(9).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(9).ToString().Trim() != "NORMAL" && oArregloLinea.GetValue(9).ToString().Trim() != "CONTRA FLUJO")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Sentido del Flujo, valores válidos {NORMAL o CONTRA FLUJO} {" + oArregloLinea.GetValue(9) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Presion Punto Terminacion
                        if (oArregloLinea.GetValue(10).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Presión del Punto de Terminación (Valor por defecto 0) {" + oArregloLinea.GetValue(10) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            //// Validacion Nueva sentido del flujo Requerimiento Ajuste presion transporte 20151006 ///////
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            string[] lsPresion;
                            try
                            {
                                if (oArregloLinea.GetValue(10).ToString().Trim().Length > 500)
                                    lsCadenaErrores = lsCadenaErrores + "Longitud del Campo Presión Punto de Terminación {" + oArregloLinea.GetValue(10) + "} supera el máximo permitido (500 carácteres), en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                else
                                {
                                    lsPresion = oArregloLinea.GetValue(10).ToString().Trim().Split('-');
                                    foreach (string Presion in lsPresion)
                                    {
                                        try
                                        {
                                            ldValor = Convert.ToDecimal(Presion.Trim());
                                        }
                                        catch (Exception)
                                        {
                                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presión del Punto de Terminación {" + Presion.Trim() + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presión del Punto de Terminación {" + oArregloLinea.GetValue(10) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            // Valida codigo fuente 20160607
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(11).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código de la fuente {" + oArregloLinea.GetValue(11) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            // Valida el periodo de entrega
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(12).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del punto de entrega{" + oArregloLinea.GetValue(12) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// 20170601 rq020-17
                            if (oArregloLinea.GetValue(13).ToString() != "N" && oArregloLinea.GetValue(13).ToString() != "E")
                                lsCadenaErrores = lsCadenaErrores + "El campo de estado de carga {" + oArregloLinea.GetValue(13) + "} es inválido, debe ser N/E, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            /// 20170601 rq020-17
                            if (oArregloLinea.GetValue(13).ToString() != "N" && oArregloLinea.GetValue(13).ToString() != "E")
                                lsCadenaErrores = lsCadenaErrores + "El campo de estado de carga {" + oArregloLinea.GetValue(13) + "} es inválido, debe ser N/E, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(14).ToString().Trim() != "N" && oArregloLinea.GetValue(14).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el indicador de conectado al SNT {" + oArregloLinea.GetValue(14) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(15).ToString().Trim() != "N" && oArregloLinea.GetValue(15).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la entrega en boca de pozo {" + oArregloLinea.GetValue(15) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(16).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código del centro poblado {" + oArregloLinea.GetValue(16) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt64(oArregloLinea.GetValue(16).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el código del centro poblado {" + oArregloLinea.GetValue(16) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// Validacion del Archivo de Usuarios Finales
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivoUsuarios(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    /// Campop nuevo Req. 009-17 Indicadores 20170324 
                    /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                    if ((oArregloLinea.Length != 8))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 8 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>"; //rq009-17
                    }
                    else
                    {
                        /// Validar No de Registro
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Operación {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Documento Usuario Final
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el DOcumento de Usuario Final (Valor por defecto 0) {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(1).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Documento de Usuario Final {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar sector de consumo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el sector de consumo {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// Validar Codigo Punto de Salida del SNT
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto Salida SNT {" + oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// Validar Cantiad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" + oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// Validar tipo de demandan- 20151009
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el tipo de demanda{" + oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// Validar mercado relevante 
                        try
                        {
                            liValor = Convert.ToInt64(oArregloLinea.GetValue(6).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el mercado relevante {" + oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// Campop nuevo Req. 009-17 Indicadores 20170324 
                        /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad equivalente en KPCD {" + oArregloLinea.GetValue(7) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(7).ToString().Trim());
                                if (liValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad Equivalente Kpcd {" + oArregloLinea.GetValue(7) + "} no puede ser menor a 0, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad Equivalente Kpcd {" + oArregloLinea.GetValue(7) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }

                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}
