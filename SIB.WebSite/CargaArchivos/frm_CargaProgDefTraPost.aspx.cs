﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

//20190318 rq017-19

namespace CargaArchivos
{
    public partial class frm_CargaProgDefTraPost : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            //Master.Titulo = "Carga Prog De Transporte Posterior a Renominación D+1";
            //Se inicializan los botones 
            buttons.SwitchOnButton(EnumBotones.Cargue);
        }

        /// <summary>
        /// Metodo para el Cargue del Archivo
        /// Fecha: Febrero 21 de 2018
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp", "@P_codigo_operador" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", strRutaFTP, goInfo.cod_comisionista };
            string lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

            try
            {
                string lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                string[] lsErrores;
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoProgTraPost";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            var liNumeroParametros = 0;
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// Mertodo para la validacion del archivo
        /// Fecha: Febrero 21 de 2018
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                var oExpresion = new Regex(@"^([01][0-9]|2[0-3]):([0-5][0-9])"); //20190318 rq017-19
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 12))  //20190211 rq005-18 //20190502 rq024-19
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 12 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";  //20190211 rq005-18 //20190502 rq024-19
                    }
                    else
                    {
                        /// Valida numero de contrato 
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El número de la operación {" + oArregloLinea.GetValue(0) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Valida Operador comprador
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El operador compra {" + oArregloLinea.GetValue(1) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Valida punto de entrada
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El punto de entrada {" + oArregloLinea.GetValue(2) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Valida punto de salida
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El punto de salida {" + oArregloLinea.GetValue(3) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// tipo de demanda
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El tipo de demanda {" + oArregloLinea.GetValue(4) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// sector de consumo
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El sector de consumo {" + oArregloLinea.GetValue(5) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// destino
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(6).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El tipo de destino {" + oArregloLinea.GetValue(6) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Cantidad Autorizada 
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(7).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Cantidad autorizada {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Valida Equivalente en Volumen
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(8).ToString());
                            var lsValor = oArregloLinea.GetValue(8).ToString().Trim().Split('.');
                            if (lsValor.Length > 1)
                            {
                                if (lsValor[1].Trim().Length > 2)
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en equivalente en volumen {" + oArregloLinea.GetValue(8) + "}, solo debe tener máximo 2 decímales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20190211 rq005 - 18
                            }
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El equivalente en volumen  {" + oArregloLinea.GetValue(8) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Valido Indicador Desvio o Firme
                        if (oArregloLinea.GetValue(9).ToString().Trim() != "D" && oArregloLinea.GetValue(9).ToString().Trim() != "F" && oArregloLinea.GetValue(9).ToString().Trim() != "")
                            lsCadenaErrores = lsCadenaErrores + "El Indicador Desvio o Firme  {" + oArregloLinea.GetValue(9) + "} es inválido {'D' , 'F' o Vacío}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        //20190502 rq024-19
                        try
                        {
                            var lsFecha = oArregloLinea.GetValue(10).ToString().Split('/');
                            if (lsFecha[0].Length != 4 || oArregloLinea.GetValue(10).ToString().Length != 10)
                                lsCadenaErrores = lsCadenaErrores + "La fecha de recibo de nominación {" + oArregloLinea.GetValue(10) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                                Convert.ToDateTime(oArregloLinea.GetValue(10).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha de recibo de nominación {" + oArregloLinea.GetValue(10) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// 20190318 rq017-19
                        /// Valida hora de recibo                    
                        if (oArregloLinea.GetValue(11).ToString().Length != 5 || !oExpresion.IsMatch(oArregloLinea.GetValue(11).ToString()))
                            lsCadenaErrores = lsCadenaErrores + "La hora de recibo de nominación {" + oArregloLinea.GetValue(11) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }

            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}