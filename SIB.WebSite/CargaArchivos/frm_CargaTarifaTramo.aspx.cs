﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_CargaTarifaTramo : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaCarga1;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaCarga1 = ConfigurationManager.AppSettings["RutaSoporteTarifa"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Cargos de Transporte";

            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                string lsRutaArchivo;
                string lsNombre;
                string[] lsErrores = { "", "" };
                var lsCadenaArchivo = new StringBuilder();
                SqlDataReader lLector;
                var lComando = new SqlCommand();
                int liNumeroParametros;
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp", "@P_codigo_operador", "@P_resolucion_creg", "@P_nombre_diario", "@P_fecha_publicacion", "@P_nombre_archivo" };
                SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };
                object[] lValorParametrosO = { "", strRutaFTP, goInfo.cod_comisionista,TxtResolucion.Text, TxtDiario.Text, TxtFechaPub.Text, ""  };
                lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
                string lsMensaje = "";
                string lsArchivo = "";

                if (TxtResolucion.Text == "")
                    lsMensaje = "Debe digitar el número de la resolución CREG<br>";
                if (TxtDiario.Text == "")
                    lsMensaje += "Debe digitar el diario de publicación<br>";
                if (TxtFechaPub.Text == "")
                    lsMensaje += "Debe digitar la fecha de publicación<br>";
                else
                {
                    try
                    {
                        Convert.ToDateTime(TxtFechaPub.Text);
                    }
                    catch (Exception ex)
                    {
                        lsMensaje+= "La fecha de publicación no es válida<br>";
                    }
                }
                if (FuSoporte.FileName == "")
                    lsMensaje += "Debe seleccionar el archivo de soporte<br>";
                else
                {
                    if (Path.GetExtension(FuSoporte.FileName).ToUpper() != ".PDF")
                        lsMensaje += "El archivo de soporte debe ser en formato PDF<br>";
                    else
                    {
                        int liConta = 0;
                        string lsNumero = "";
                        while (lsArchivo == "" && liConta < 10)
                        {
                            lsNumero = DateTime.Now.Millisecond.ToString();
                            lsArchivo = DateTime.Now.ToString("yyyy_MM_dd") + "_" + goInfo.cod_comisionista + "_" + lsNumero.ToString() + "_" + liConta.ToString() + Path.GetExtension(FuSoporte.FileName).ToUpper();
                            if (File.Exists(strRutaCarga + lsArchivo))
                            {
                                lsArchivo = "";
                            }
                            liConta++;
                        }
                        if (lsArchivo == "")
                            lsMensaje += "No se puede cargar el archivo de soporte. Intente de nuevo<br>";
                    }
                }
                
                if (lsMensaje == "")
                {
                    lsRutaArchivo = strRutaCarga + lsNombre;
                    FuArchivo.SaveAs(lsRutaArchivo);

                    // Realiza las Validaciones de los Archivos
                    if (FuArchivo.FileName != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }

                    if (lsErrores[0] == "")
                    {
                        try
                        {
                            FuSoporte.SaveAs(strRutaCarga1 + lsArchivo);
                        }
                        catch (Exception ex)
                        { }
                        if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                        {
                            lValorParametrosO[0] = lsNombre;
                            lValorParametrosO[6] = lsArchivo;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoTarifaTramo";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                }
                            }
                            else
                            {
                                Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                    }
                }
                else
                    Toastr.Warning(this, lsMensaje);
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 7))  //202010120
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 7 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                    }
                    else
                    {
                        /// Valida codigop tramo
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del tramo {" + oArregloLinea.GetValue(0) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                        }
                        /// fecha
                        string[] lsFecha;
                        try
                        {
                            Convert.ToDateTime(oArregloLinea.GetValue(1).ToString());
                            lsFecha = oArregloLinea.GetValue(1).ToString().Split('/');
                            if (oArregloLinea.GetValue(1).ToString().Length !=10 || lsFecha.Length !=3 || lsFecha[0].Length != 4 )
                                lsCadenaErrores = lsCadenaErrores + "La fecha e vigencia inicial {" + oArregloLinea.GetValue(1) + "} es inválida.Debe tener formato {aaaa/mm/dd}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha de vigencia inicial {" + oArregloLinea.GetValue(1) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                        }
                        /// fecha fin
                        try
                        {
                            Convert.ToDateTime(oArregloLinea.GetValue(2).ToString());
                            lsFecha = oArregloLinea.GetValue(2).ToString().Split('/');
                            if (oArregloLinea.GetValue(2).ToString().Length != 10 || lsFecha.Length != 3 || lsFecha[0].Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "La fecha e vigencia final {" + oArregloLinea.GetValue(2) + "} es inválida.Debe tener formato {aaaa/mm/dd}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha de vigencia final  {" + oArregloLinea.GetValue(2).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; 
                        }
                        ///codigo pareja
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código de la pareja de cargos {" + oArregloLinea.GetValue(3).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        ///carogo fijo
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(4).ToString());
                            string[] lsValor = oArregloLinea.GetValue(4).ToString().Split('.');
                            if (lsValor.Length > 1)
                            {
                                if (lsValor[1].Trim().Length > 3)
                                    lsCadenaErrores = lsCadenaErrores + "El valor del cargo fijo {" + oArregloLinea.GetValue(4).ToString() + "} debe tener máximo 3 decímales, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El valor del cargo fijo {" + oArregloLinea.GetValue(4).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; 
                        }
                        /// cargo variabke
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(5).ToString());
                            string[] lsValor = oArregloLinea.GetValue(5).ToString().Split('.');
                            if (lsValor.Length > 1)
                            {
                                if (lsValor[1].Trim().Length > 3)
                                    lsCadenaErrores = lsCadenaErrores + "El valor del cargo variable {" + oArregloLinea.GetValue(5).ToString() + "} debe tener máximo 3 decímales, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El valor del cargo variable {" + oArregloLinea.GetValue(5).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; 
                        }
                        /// -	cargog AOM
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(6).ToString());
                            string[] lsValor = oArregloLinea.GetValue(6).ToString().Split('.');
                            if (lsValor.Length > 1)
                            {
                                if (lsValor[1].Trim().Length > 3)
                                    lsCadenaErrores = lsCadenaErrores + "El valor del cargo fijo AOM {" + oArregloLinea.GetValue(6) + "} debe tener máximo 3 decímales , en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El valor del cargo fijo AOM {" + oArregloLinea.GetValue(6) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}