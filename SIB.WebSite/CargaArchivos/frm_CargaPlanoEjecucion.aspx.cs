﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_CargaPlanoEjecucion : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Informes";

            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsRutaArchivo = "";
            var lsRutaArchivoU = "";
            var lsNombre = "";
            var lsNombreU = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            var oTransOK = true;
            bool oCargaOK;
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_archivoUsuarios", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario };

            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, $"{HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoContratos", CultureInfo.CurrentCulture)}: {ex.Message}");
                oTransOK = false;
            }
            if (FuArchivoUsuarios.FileName != "")
            {
                lsNombreU = DateTime.Now.Millisecond + FuArchivoUsuarios.FileName;
                try
                {
                    lsRutaArchivoU = strRutaCarga + lsNombreU;
                    FuArchivoUsuarios.SaveAs(lsRutaArchivoU);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, $"{HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoUsuarios", CultureInfo.CurrentCulture)}: {ex.Message}");
                    oTransOK = false;
                }
            }
            if (oTransOK)
            {
                try
                {
                    // Realiza las Validaciones de los Archivos
                    if (FuArchivo.FileName != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoEjecucionContratosRequerido", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }
                    if (lsErrores[0] == "")
                    {
                        if (FuArchivoUsuarios.FileName != "")
                            lsErrores = ValidarArchivoUsuarios(lsRutaArchivoU);
                    }
                    if (lsErrores[0] == "")
                    {
                        oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);
                        if (FuArchivoUsuarios.FileName != "")
                            oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivoU, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombreU, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);

                        if (oCargaOK)
                        {
                            lValorParametrosO[0] = lsNombre;
                            lValorParametrosO[1] = lsNombreU;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoEjecucion";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    lsCadenaArchivo.Append($"{lLector["error"]}<br>");
                                }
                            }
                            else
                            {
                                Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                    }
                    //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                    NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                    LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);

                }
                catch (Exception ex)
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
                }
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            var liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la Línea  sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if (oArregloLinea.Length != 6)  /// 20190524 rq029-19
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 6 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";  /// 20190524 rq029-19
                    }
                    else
                    {
                        /// Validar consecutivo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el consecutivo {" + oArregloLinea.GetValue(0) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// numero de oepracioon
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Operación {" + oArregloLinea.GetValue(1) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar fecha
                        /// 20190524 rq029-19
                        try
                        {
                            Convert.ToDateTime(oArregloLinea.GetValue(2).ToString());
                            if (oArregloLinea.GetValue(2).ToString().Length != 10)
                                lsCadenaErrores = lsCadenaErrores + "La fecha de gas {" + oArregloLinea.GetValue(23) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                            {
                                lsFecha = oArregloLinea.GetValue(2).ToString().Split('/');
                                if (lsFecha[0].Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "La fecha de gas {" + oArregloLinea.GetValue(2) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha del registro {" + oArregloLinea.GetValue(2) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// codigo punto
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el código del punto {" + oArregloLinea.GetValue(3) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// 20190524 rq029-19
                        /// codigo punto
                        //try
                        //{
                        //    liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString().Trim());
                        //}
                        //catch (Exception ex)
                        //{
                        //    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el tipo de demanda {" + oArregloLinea.GetValue(3).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //}
                        /// sector consumo
                        //try
                        //{
                        //    liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString().Trim());
                        //}
                        //catch (Exception ex)
                        //{
                        //    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el sector de consumo {" + oArregloLinea.GetValue(4).ToString() + "}, en la Línea  No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //}
                        /// 20190524 fin rq029-19
                        /// cantidad autoriazad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la cantidad autorizada {" + oArregloLinea.GetValue(4) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// valor factruado
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(5).ToString().Trim());
                            lsFecha = oArregloLinea.GetValue(5).ToString().Trim().Split('.');
                            if (lsFecha.Length > 1)
                            {
                                if (lsFecha[1].Trim().Length > 2)
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el valor facturado {" + oArregloLinea.GetValue(5) + "}, solo debe tener máximo 2 decimales, en la Línea  No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el valor facturado {" + oArregloLinea.GetValue(5) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// Validacion del Archivo de Usuarios Finales
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivoUsuarios(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            var liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la Línea  sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    /// Campop nuevo Req. 009-17 Indicadores 20170324 
                    /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                    if (oArregloLinea.Length != 7)
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 7 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";  /// 20190524 rq029-19
                    }
                    else
                    {
                        /// consecutivo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el consecutivo {" + oArregloLinea.GetValue(0) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// 20190524 rq029-19
                        /// codigo demanda
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el tipo de demanda {" + oArregloLinea.GetValue(1) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// sector consumo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el sector de consumo {" + oArregloLinea.GetValue(2) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// Punto de salida
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el punto de salida {" + oArregloLinea.GetValue(3) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// 20190524 fin rq029-19
                        /// Validar Documento Usuario Final
                        if (oArregloLinea.GetValue(4).ToString().Length > 15)
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Documento de Usuario Final {" + oArregloLinea.GetValue(4) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Documento de Usuario Final {" + oArregloLinea.GetValue(4) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Codigo mercado
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(5).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el mercado relevante {" + oArregloLinea.GetValue(5) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                        /// 20190524 rq029-19
                        /// canitdad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(6).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la cantidad contratada{" + oArregloLinea.GetValue(6) + "}, en la Línea  No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}