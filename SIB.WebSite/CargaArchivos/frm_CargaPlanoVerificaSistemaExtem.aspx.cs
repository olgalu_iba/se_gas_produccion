﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaPlanoVerificaSistemaExtem : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;
        private static string lsTitulo = "Archivos Verificación Contratos Sistema SEGAS Extemporánea";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                if (goInfo == null) Response.Redirect("../index.aspx");
                lConexion = new clConexion(goInfo);
                strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
                strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

                //Eventos Botones
                buttons.CargueOnclick += BtnCargar_Click;

                if (IsPostBack) return;
                //Titulo
                //Master.Titulo = "Informes";
                var lControl = new clControlUsuario();
                if (!lControl.validaAcceso(lsTitulo, goInfo.Usuario, goInfo))
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "NegacionPermisosUsuario", CultureInfo.CurrentCulture)?.ToString());
                else
                    //Se inicializan los botones 
                    buttons.SwitchOnButton(EnumBotones.Cargue);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new System.Web.UI.WebControls.ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new System.Web.UI.WebControls.ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Dispose();
            lLector.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsRutaArchivo = "";
            var lsRutaArchivoU = "";
            string lsNombre;
            var lsNombreU = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            var oTransOK = true;
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            int liNumeroParametros;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_archivoUsuarios", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario };

            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoContratos", CultureInfo.CurrentCulture)?.ToString());
                oTransOK = false;
            }
            if (FuArchivoUsuarios.FileName != "")
            {
                lsNombreU = DateTime.Now.Millisecond + FuArchivoUsuarios.FileName;
                try
                {
                    lsRutaArchivoU = strRutaCarga + lsNombreU;
                    FuArchivoUsuarios.SaveAs(lsRutaArchivoU);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoUsuarios", CultureInfo.CurrentCulture)?.ToString());
                    oTransOK = false;
                }
            }
            if (oTransOK)
            {
                try
                {
                    // Realiza las Validaciones de los Archivos
                    if (FuArchivo.FileName != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoContratosRequerido", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }
                    if (lsErrores[0] == "")
                    {
                        if (FuArchivoUsuarios.FileName != "")
                            lsErrores = ValidarArchivoUsuarios(lsRutaArchivoU);
                    }
                    if (lsErrores[0] == "")
                    {
                        bool oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);
                        if (FuArchivoUsuarios.FileName != "")
                            oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivoU, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombreU, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);

                        if (oCargaOK)
                        {
                            lValorParametrosO[0] = lsNombre;
                            lValorParametrosO[1] = lsNombreU;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoVerifContSistemaExtemp";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }
                            var lsErrorMail = "";
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    if (lLector["ind_error"].ToString() == "S")
                                        lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                    else
                                    {
                                        var mailV = new clEmail(lLector["mail"].ToString(), "Registro de contratos", lLector["Mensaje"].ToString(), "");
                                        lsErrorMail = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas") + "<br>";
                                    }
                                }
                            }
                            else
                            {
                                Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                    }
                    //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                    NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                    LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
                }
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 20)) // Fuente o campo MP 20160607  //20170816 rq036-17  //20171130 rq026-17
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 20 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; // Fuente o campo MP 20160607  //20170816 rq036-17 //20171130 rq026-17
                    }
                    else
                    {
                        /// Validar Operacion
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Operación {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Operación {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Contrato {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha de Suscripción {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(2).ToString());
                                lsFecha = oArregloLinea.GetValue(2).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha de Suscripción {" + oArregloLinea.GetValue(2) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Suscripción {" + oArregloLinea.GetValue(2) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Punto de Entrega / Tramo {" + oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto de Entrega / Tramo {" + oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Codigo Modalidad Contractual
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Modalidad Contractual {" + oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Modalidad Contractual {" + oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad / Capacidad {" + oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(5).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad / Capacidad {" + oArregloLinea.GetValue(5) + "} no puede ser 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" + oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Precio
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Precio {" + oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(6).ToString().Trim());
                                lsFecha = oArregloLinea.GetValue(6).ToString().Trim().Split('.');
                                if (lsFecha.Length > 1)
                                {
                                    if (lsFecha[1].Trim().Length > 2)
                                        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(6) + "}, solo debe tener maximo 2 decimales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                                if (ldValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(6) + "} no puede ser 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fecha Inicial
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha Inicial {" + oArregloLinea.GetValue(7) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                                lsFecha = oArregloLinea.GetValue(7).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha Inicial {" + oArregloLinea.GetValue(7) + "}, debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Inicial {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fecha Final
                        if (oArregloLinea.GetValue(8).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha Final {" + oArregloLinea.GetValue(8) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(8).ToString());
                                lsFecha = oArregloLinea.GetValue(8).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha Final {" + oArregloLinea.GetValue(8) + "}, debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Final {" + oArregloLinea.GetValue(8) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        ///// Validar Codigo Tipo Demanda
                        //if (oArregloLinea.GetValue(10).ToString().Trim().Length <= 0)
                        //    lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Codigo Tipo Demanda (Si es venta debe ingresar 0) {" + oArregloLinea.GetValue(10).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //else
                        //{
                        //    try
                        //    {
                        //        liValor = Convert.ToInt64(oArregloLinea.GetValue(10).ToString().Trim());
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Codigo Tipo Demanda {" + oArregloLinea.GetValue(10).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //    }
                        //}
                        /// Validar Sentido del Flujo
                        if (oArregloLinea.GetValue(10).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(10).ToString().Trim() != "NORMAL" && oArregloLinea.GetValue(10).ToString().Trim() != "CONTRA FLUJO")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Sentido del Flujo, valores válidos {NORMAL o CONTRA FLUJO} {" + oArregloLinea.GetValue(10) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Validar Presion Punto Terminacion
                        if (oArregloLinea.GetValue(11).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Presión Punto Terminación (Valor por defecto 0) {" + oArregloLinea.GetValue(11) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            //// Validacion Nueva sentido del flujo Requerimiento Ajuste presion transporte 20151006 ///////
                            ////////////////////////////////////////////////////////////////////////////////////////////////
                            string[] lsPresion;
                            try
                            {
                                if (oArregloLinea.GetValue(11).ToString().Trim().Length > 500)
                                    lsCadenaErrores = lsCadenaErrores + "Longitud del Campo Presión Punto de Terminación {" + oArregloLinea.GetValue(11) + "} supera el maximo permitido (500 caracteres), en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                else
                                {
                                    lsPresion = oArregloLinea.GetValue(11).ToString().Trim().Split('-');
                                    foreach (string Presion in lsPresion)
                                    {
                                        try
                                        {
                                            ldValor = Convert.ToDecimal(Presion.Trim());
                                        }
                                        catch (Exception)
                                        {
                                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presión Punto de Terminación {" + Presion.Trim() + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presión Punto de Terminación {" + oArregloLinea.GetValue(11) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida Fuente o campo MP 20160607
                            if (oArregloLinea.GetValue(12).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código de la Fuente {" + oArregloLinea.GetValue(12) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt16(oArregloLinea.GetValue(12).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Fuente {" + oArregloLinea.GetValue(12) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(13).ToString().Trim() != "N" && oArregloLinea.GetValue(13).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el indicador de conectado al SNT {" + oArregloLinea.GetValue(13) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(14).ToString().Trim() != "N" && oArregloLinea.GetValue(14).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la entrega en boca de pozo {" + oArregloLinea.GetValue(14) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            //20170816 rq036-17
                            if (oArregloLinea.GetValue(15).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código del centro poblado {" + oArregloLinea.GetValue(15) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt64(oArregloLinea.GetValue(15).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el código del centro poblado {" + oArregloLinea.GetValue(15) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(16).ToString().Trim().Length <= 0)
                                lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el periodo de entrega {" + oArregloLinea.GetValue(16) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                            {
                                try
                                {
                                    liValor = Convert.ToInt64(oArregloLinea.GetValue(16).ToString().Trim());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el periodo de entrega {" + oArregloLinea.GetValue(16) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(17).ToString().Trim() != "N" && oArregloLinea.GetValue(17).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el indicador de contrato variable {" + oArregloLinea.GetValue(17) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(18).ToString() != "0")
                            {
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(18).ToString());
                                    if (oArregloLinea.GetValue(18).ToString().Length != 5)
                                        lsCadenaErrores = lsCadenaErrores + "La hora de entrega inicial {" + oArregloLinea.GetValue(18) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La hora de entrega inicial {" + oArregloLinea.GetValue(18) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }
                            /// 20171130 rq026-17
                            if (oArregloLinea.GetValue(19).ToString() != "0")
                            {
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(18).ToString());
                                    if (oArregloLinea.GetValue(19).ToString().Length != 5)
                                        lsCadenaErrores = lsCadenaErrores + "La hora de entrega final {" + oArregloLinea.GetValue(19) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La hora de entrega final {" + oArregloLinea.GetValue(19) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// Validacion del Archivo de Usuarios Finales
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivoUsuarios(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la Línea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    /// Campop nuevo Req. 009-17 Indicadores 20170324 
                    /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                    if (oArregloLinea.Length != 8)
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 8 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";  //rq009-17
                    }
                    else
                    {
                        /// Validar Operacion
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Operación {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Operación {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Documento Usuario Final
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Documento de Usuario Final (Valor por defecto 0) {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(1).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Documento de Usuario Final {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Codigo Sector de consumo 
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Sector de Consumo {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(2).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Sector de Consumo {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Codigo Punto de Salida del SNT
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Punto Salida SNT {" + oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto Salida SNT {" + oArregloLinea.GetValue(3) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad / Capacidad {" + oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad / Capacidad {" + oArregloLinea.GetValue(4) + "} no puede ser 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" + oArregloLinea.GetValue(4) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar tipo de demanda- 20151009
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el tipo de demanda{" + oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(5).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el tipo de demanda {" + oArregloLinea.GetValue(5) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Mercado Relevante - 20160126
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Mercado Relevante {" + oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(6).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Mercado Relevante {" + oArregloLinea.GetValue(6) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Campop nuevo Req. 009-17 Indicadores 20170324 
                        /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad equivalente en KPCD {" + oArregloLinea.GetValue(7) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(7).ToString().Trim());
                                if (liValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad Equivalente Kpcd {" + oArregloLinea.GetValue(7) + "} no puede ser menor a 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad Equivalente Kpcd {" + oArregloLinea.GetValue(7) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }


                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;

        }
    }
}