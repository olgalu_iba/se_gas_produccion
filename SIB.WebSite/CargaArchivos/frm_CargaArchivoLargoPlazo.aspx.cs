﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaArchivoLargoPlazo : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Contratos";
            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsCadenaArchivo = new StringBuilder();
            string lsRutaArchivo;
            string lsNombre;
            string lsProcedimiento;
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                string[] lsErrores;
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lsProcedimiento = "pa_ValidaPlanoNegLP";
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = lsProcedimiento;
                        lComando.CommandTimeout = 3600;
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            lConexion = new clConexion(goInfo);
                            try
                            {
                                string[] lsNombreParametros = { "@P_codigo_usuario" };
                                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                                object[] lValorParametros = { goInfo.codigo_usuario };
                                lConexion.Abrir();

                                var lblMensaje = new StringBuilder();
                                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetPlanoNegoLargoPlazo", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                                if (lLector.HasRows)
                                {
                                    while (lLector.Read())
                                        lblMensaje.Append(lLector["mensaje"]);
                                    BtnAceptar.Enabled = true;
                                }
                                else
                                {
                                    lblMensaje.Append("No hay registros para cargar");
                                    BtnAceptar.Enabled = false;
                                }
                                lConexion.Cerrar();

                                parFechaLArgoPlazo.InnerHtml = lblMensaje.ToString();
                                Modal.Abrir(this, modAut.ID, modAutInside.ID, Modal.Size.Normal);
                            }
                            catch (Exception ex)
                            {
                                Toastr.Error(this, ex.Message);
                            }
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }

                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            Int64 liValor = 0;
            DateTime ldFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };
            string[] lsFecha;

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 19)) //20171130 rq026-17 //20190404 rq018-19 fase II
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 19 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20171130 rq026-17 //20190404 rq018-19 fase II
                    }
                    else
                    {
                        /// Validar Punta
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Punta {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(0).ToString().Trim() != "C" && oArregloLinea.GetValue(0).ToString().Trim() != "V")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Punta {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Destino Rueda
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Destino Rueda {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(1).ToString().Trim() != "T" && oArregloLinea.GetValue(1).ToString().Trim() != "G")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Destino Rueda {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Tipo Mercado
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Tipo Mercado {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(2).ToString().Trim() != "P" && oArregloLinea.GetValue(2).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Tipo Mercado {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Operador Punta Contraria
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del Operador Punta Contraria {" + oArregloLinea.GetValue(3) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Punto de entrega
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del punto de entrega {" + oArregloLinea.GetValue(4) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Tipo de Contrato
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del Tipo de Contrato {" + oArregloLinea.GetValue(5) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha de Negociacion
                        if (oArregloLinea.GetValue(6).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de Negociación {" + oArregloLinea.GetValue(6) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            lsFecha = oArregloLinea.GetValue(6).ToString().Split('/');
                            if (lsFecha[0].Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Negociación {" + oArregloLinea.GetValue(6) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(6).ToString());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La Fecha de Negociación {" + oArregloLinea.GetValue(6) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                        }
                        /// Valida HOra de negociación
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                            if (oArregloLinea.GetValue(7).ToString().Length != 5)
                                lsCadenaErrores = lsCadenaErrores + "La Hora de Negociación {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Hora de Negociación {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Periodo de Entrega
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(8).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del Periodo de Entrega {" + oArregloLinea.GetValue(8) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Años
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(9).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Campo años {" + oArregloLinea.GetValue(9) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha Inicial
                        if (oArregloLinea.GetValue(10).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha Inicial {" + oArregloLinea.GetValue(10) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            lsFecha = oArregloLinea.GetValue(10).ToString().Split('/');
                            if (lsFecha[0].Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "La Fecha Inicial {" + oArregloLinea.GetValue(10) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(10).ToString());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La Fecha Inicial {" + oArregloLinea.GetValue(10) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                        }
                        /// Valida Fecha final 
                        if (oArregloLinea.GetValue(11).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha Final {" + oArregloLinea.GetValue(11) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            lsFecha = oArregloLinea.GetValue(11).ToString().Split('/');
                            if (lsFecha[0].Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "La Fecha Final {" + oArregloLinea.GetValue(11) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(11).ToString());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La Fecha Final {" + oArregloLinea.GetValue(11) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                        }
                        /// Valida Precio
                        try
                        {
                            var ldValor = Convert.ToDecimal(oArregloLinea.GetValue(12).ToString());
                            var sOferta = ldValor.ToString();
                            int iPos = sOferta.IndexOf(".");
                            if (iPos > 0)
                                if (sOferta.Length - iPos > 3)
                                    lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(12) + "} solo puede tener 2 decímales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            if (Convert.ToDouble(sOferta) <= 0)
                                lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(12) + "} debe ser mayor que 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(12) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Cantidad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(13).ToString());
                            if (liValor <= 0)
                                lsCadenaErrores = lsCadenaErrores + "La Cantidad {" + oArregloLinea.GetValue(13) + "} debe ser mayor que 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Cantidad {" + oArregloLinea.GetValue(13) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha de suscripcion
                        if (oArregloLinea.GetValue(14).ToString().Length != 10)
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de suscripción {" + oArregloLinea.GetValue(14) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            lsFecha = oArregloLinea.GetValue(14).ToString().Split('/');
                            if (lsFecha[0].Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de suscripción {" + oArregloLinea.GetValue(14) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                                try
                                {
                                    ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(14).ToString());
                                }
                                catch (Exception ex)
                                {
                                    lsCadenaErrores = lsCadenaErrores + "La Fecha de suscripción {" + oArregloLinea.GetValue(14) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                        }
                        /// Valida la fuente
                        /// /// 20171130 rq026-17
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(15).ToString());
                            if (liValor <= 0)
                                lsCadenaErrores = lsCadenaErrores + "El código de la fuente {" + oArregloLinea.GetValue(15) + "} debe ser mayor que 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código de la fuente {" + oArregloLinea.GetValue(15) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida contrato definitivo
                        /// 20171130 rq026-17
                        if (oArregloLinea.GetValue(16).ToString().Length <= 0 || oArregloLinea.GetValue(16).ToString().Length > 30)
                            lsCadenaErrores = lsCadenaErrores + "El contrato definitivo {" + oArregloLinea.GetValue(16) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// Valida indiciador de contrato variable
                        /// 20171130 rq026-17
                        if (oArregloLinea.GetValue(17).ToString() != "N" && oArregloLinea.GetValue(17).ToString() != "S")
                            lsCadenaErrores = lsCadenaErrores + "El indicador de contrato variable {" + oArregloLinea.GetValue(17) + "} debe ser N o S, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// Valida tipo de demanda
                        /// /// 20190404 rq018-19 fase II
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(18).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del tipo de demanda {" + oArregloLinea.GetValue(18) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                    }

                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                bool Error = false;
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametros = { "@P_codigo_usuario", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                object[] lValorParametros = { goInfo.codigo_usuario, goInfo.cod_comisionista };
                lConexion.Abrir();
                var lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPlanoNegoLargoPlazo", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    var lsError = new StringBuilder();
                    while (lLector.Read())
                    {
                        lsError.Append($"{lLector["mensaje"]}<br>");  //rq026-17  20171130
                    }
                    Toastr.Success(this, lsError.ToString());
                }
                else
                {
                    Toastr.Info(this, "Operaciones disponibles en el módulo Registro de Contratos, para ingreso de información transaccional.!");
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                Modal.Cerrar(this, modAut.ID);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "Problemas en la Carga del Plano.!");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, modAut.ID);
        }
    }
}