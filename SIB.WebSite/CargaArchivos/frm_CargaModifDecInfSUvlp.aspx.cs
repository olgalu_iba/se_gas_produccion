﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_CargaModifDecInfSUvlp : Page
    {
        private InfoSessionVO _goInfo;
        private clConexion _lConexion;
        private string _strRutaCarga;
        private string _strRutaFtp;
        private SqlDataReader _lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            _goInfo = (InfoSessionVO)Session["infoSession"];
            if (_goInfo == null) Response.Redirect("../index.aspx");
            _lConexion = new clConexion(_goInfo);
            _strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            _strRutaFtp = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Subasta";
            //Botones
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo;
            string lsNombre;
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            int liNumeroParametros;
            _lConexion = new clConexion(_goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp", "@P_codigo_operador" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", _strRutaFtp, _goInfo.cod_comisionista };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = _strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        _lConexion.Abrir();
                        lComando.Connection = _lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        if (ddlProd.SelectedValue == "G")
                            lComando.CommandText = "pa_ValidaPlanoModifMPUvlpSum";
                        if (ddlProd.SelectedValue == "T")
                            lComando.CommandText = "pa_ValidaPlanoModifMPUvlpTra";

                        lComando.CommandTimeout = 3600;
                        for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                        {
                            lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        _lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(_goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + _goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            Int32 liNumeroLinea = 0;
            int liTotalRegistros = 0;
            string lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                // Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                if (ddlProd.SelectedValue == "G")
                {
                    while (!lLectorArchivo.EndOfStream)
                    {
                        liTotalRegistros = liTotalRegistros + 1;
                        // Obtiene la fila del Archivo
                        string lsLineaArchivo = lLectorArchivo.ReadLine();
                        //if (lsLineaArchivo.Length > 0)
                        //{
                        liNumeroLinea = liNumeroLinea + 1;
                        // Pasa la linea sepaada por Comas a un Arreglo
                        Array oArregloLinea = lsLineaArchivo.Split(',');
                        if ((oArregloLinea.Length != 8))
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 8 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        else
                        {
                            /// Validar codigo de verificacion
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El número del ID {" + oArregloLinea.GetValue(0) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida el punto de salida del SNT
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "EL punto de salida {" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad contratada
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                                if (Convert.ToInt32(oArregloLinea.GetValue(2).ToString()) < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La cantidad equivalente en kpcd {" + oArregloLinea.GetValue(2) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La cantidad equivalente en kpcd {" + oArregloLinea.GetValue(2) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad equivalente
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                                if (Convert.ToInt32(oArregloLinea.GetValue(3).ToString()) < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La cantidad equivalente en kpcd {" + oArregloLinea.GetValue(3) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La cantidad equivalente en kpcd {" + oArregloLinea.GetValue(3) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad demandada
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                                if (Convert.ToInt32(oArregloLinea.GetValue(4).ToString()) < 0)
                                    lsCadenaErrores = lsCadenaErrores + "la cantidad demandada {" + oArregloLinea.GetValue(4) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "la cantidad demandada {" + oArregloLinea.GetValue(4) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida cantidad a demandanda equivalente
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                                if (Convert.ToInt32(oArregloLinea.GetValue(5).ToString()) < 0)
                                    lsCadenaErrores = lsCadenaErrores + "la cantidad equivalente demandada {" + oArregloLinea.GetValue(5) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "la cantidad equivalente demandada {" + oArregloLinea.GetValue(5) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida la fuente
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(7).ToString());
                                if (Convert.ToInt32(oArregloLinea.GetValue(7).ToString()) < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La fuente {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fuente {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }

                        }
                    }
                    lLectorArchivo.Close();
                    lLectorArchivo.Dispose();
                }
                // Validación del archivo de declaración de información de Transporte
                if (ddlProd.SelectedValue == "T")
                {
                    while (!lLectorArchivo.EndOfStream)
                    {
                        liTotalRegistros = liTotalRegistros + 1;
                        /// Obtiene la fila del Archivo
                        string lsLineaArchivo = lLectorArchivo.ReadLine();
                        //if (lsLineaArchivo.Length > 0)
                        //{
                        liNumeroLinea = liNumeroLinea + 1;
                        /// Pasa la linea sepaada por Comas a un Arreglo
                        Array oArregloLinea = lsLineaArchivo.Split(',');
                        if ((oArregloLinea.Length != 9))
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 9 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        else
                        {
                            /// Validar codigo de verificacion
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El número del ID {" + oArregloLinea.GetValue(0) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida tramo
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del tramo {" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida porcentaje fijo de inversion
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El porcentaje del cargo fijo de inversión {" + oArregloLinea.GetValue(3) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida porcentaje variabel de inversion
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El porcentaje del cargo variable de inversión {" + oArregloLinea.GetValue(4) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// Valida la cantidad contratada
                            try
                            {
                                Convert.ToInt32(oArregloLinea.GetValue(6).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La capacidad real contratada {" + oArregloLinea.GetValue(6) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// valida el porcentaje de impuesto de trasporte
                            try
                            {
                                Convert.ToDecimal(oArregloLinea.GetValue(7).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El porcentaje de impuesto de transporte {" + oArregloLinea.GetValue(7) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            /// valida el porcentaje cuota de fomento
                            try
                            {
                                Convert.ToDecimal(oArregloLinea.GetValue(8).ToString());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El porcentaje de cuota de fomento{" + oArregloLinea.GetValue(8) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }

                        }
                    }
                    lLectorArchivo.Close();
                    lLectorArchivo.Dispose();
                }
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}