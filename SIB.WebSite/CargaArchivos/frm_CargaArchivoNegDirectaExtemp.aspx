﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaArchivoNegDirectaExtemp.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="CargaArchivos.frm_CargaArchivoNegDirectaExtemp" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="Archivo Negociación Bilateral Extemporáneo" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMensaje" Text="Archivo" AssociatedControlID="FuArchivo" runat="server" />
                            <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" EnableTheming="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label AssociatedControlID="display" runat="server">Tiempo Transcurrido</asp:Label>
                            <input type="text" name="display" id="display" size="8" value="00:00:0" class="form-control" readonly="true" clientidmode="Static" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
