﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaPlanoModContratoCesion : Page
    {
        private InfoSessionVO goInfo = null;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;
        private string sRutaArc = ConfigurationManager.AppSettings["rutaModif"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Se cargan los valores iniciales del formulario 
                CargarPagina();
                if (IsPostBack) return;
                //Se inicializan los controles para la primera ves que se ejecuta el formulario 
                InicializarPagina();
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CargarPagina()
        {
            //Se redirecciona al Login si no hay una sesión iniciada    
            var goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("~/login.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;
            cargaModificaRegistroCession.ToastrEvent += Toastr_Event;
            cargaModificaRegistroCession.ModalEvent += Modal_Event;
            cargaModificaRegistroCession.ButtonsEvent += Buttons_Event;
            cargaModificaRegistroCession.LogCargaArchivoEvent += LogCargaArchivo_Event;
        }

        /// <summary>
        /// Inicializa el contenido del formulario 
        /// </summary>
        private void InicializarPagina()
        {
            //Titulo
            //Master.Titulo = "Contratos";
            //Descripcion
            //Master.DescripcionPagina = "Registro de contratos";
            //Botones
            EnumBotones[] botones = { EnumBotones.Ninguno };
            buttons.Inicializar(botones: botones);
            cargaModificaRegistroCession.InicializarFormulario();
        }

        #region Eventos

        #region CURD Control

        /// <summary>
        /// Evento que controla el botón de cargar documentos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            cargaModificaRegistroCession.BtnCargar_Click(sender, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="botones"></param>
        protected void Buttons_Event(EnumBotones[] botones, DropDownList ddlSelect, string webController)
        {
            buttons.SwitchOnButton(botones);
        }

        #endregion CURD Control

        /// <summary>
        /// Controla los Toastrs
        /// </summary>
        protected void Toastr_Event(string message, EnumTypeToastr typeToastr)
        {
            Toastr.SelectToastr(this, message, null, typeToastr);
        }

        /// <summary>
        /// Controla el log de errores de un cargue por archivo    
        /// </summary>
        protected void LogCargaArchivo_Event(StringBuilder message)
        {
            NotificacionCargaArchivo.NotificacionCargueExitoso(this, message);
            LogCargaArchivo.DownloadBugsLog(this, message);
        }

        /// <summary>
        /// Controla la apertura de los Modals
        /// </summary>
        /// <param name="id"></param>
        /// <param name="insideId"></param>
        /// <param name="typeModal"></param>
        /// <param name="size"></param>
        protected void Modal_Event(string id, string insideId, EnumTypeModal typeModal, Modal.Size size = Modal.Size.Large)
        {
            Modal.SelectModal(this, id, insideId, typeModal, size);
        }

        #endregion  Eventos
    }
}