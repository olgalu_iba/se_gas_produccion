﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_AutCargaNegDirecta.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="CargaArchivos.CargaArchivos_frm_AutCargaNegDirecta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="tblTitulo" runat="server" Text="FECHA MAXIMA DE REGISTRO DE OPERACIONES BILATERALES" />
                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">

                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Button ID="BtnAceptar" CssClass="btn btn-success" Text="Aceptar" runat="server" OnClick="BtnAceptar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="BtnCancelar" runat="server" CssClass="btn btn-success" Text="Cancelar" OnClick="BtnCancelar_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</asp:Content>
