﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaArchivoNegDirectaExtemp : Page
    {
        private InfoSessionVO goInfo = null;
        private clConexion lConexion = null;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;
        private static string lsTitulo = "Archivo Negociación Bilateral Extemporánea";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                if (goInfo == null) Response.Redirect("../index.aspx");
                lConexion = new clConexion(goInfo);
                strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
                strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

                //Eventos Botones
                buttons.CargueOnclick += BtnCargar_Click;

                if (IsPostBack) return;
                //Titulo
                //Master.Titulo = "Registros Operativos";
                //Botones
                EnumBotones[] botones = { };

                var lControl = new clControlUsuario();
                if (!lControl.validaAcceso(lsTitulo, goInfo.Usuario, goInfo))
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "NegacionPermisosUsuario", CultureInfo.CurrentCulture)
                        ?.ToString());
                else
                    botones = new[] { EnumBotones.Cargue };
                buttons.Inicializar(botones: botones);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsRutaArchivo = "";
            var lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this,
                        HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido",
                            CultureInfo.CurrentCulture)?.ToString());
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo,
                        ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre,
                        ConfigurationManager.AppSettings["UserFtp"],
                        ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoNegoDirectaExtemp";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0;
                                liNumeroParametros <= lsNombreParametrosO.Length - 1;
                                liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros],
                                    lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            //cambio de mesanjes 20150616   
                            ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert",
                                "window.open('frm_AutCargaNegDirectaEntemp.aspx','','width=500,height=600,left=400,top=150,status=no,location=0,menubar=no,toolbar=no,resizable=no,scrollbars=1');",
                                true);
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this,
                            HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)
                                ?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo,
                        HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)
                            ?.ToString(), "Usuario : " + goInfo.nombre);
                }

                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);

                ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                Toastr.Error(this,
                    HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            var lsCadenaErrores = "";
            string[] lsFecha;
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la Línea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 25)) //20170814 rq036  //20171130 rq026-17
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 25 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<BR>"; //20170814 rq036
                    }
                    else
                    {
                        /// Validar Punta
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Punta {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20171130 rq026-17
                        else
                        {
                            if (oArregloLinea.GetValue(0).ToString().Trim() != "C" && oArregloLinea.GetValue(0).ToString().Trim() != "V")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Punta {" + oArregloLinea.GetValue(0) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Destino Rueda
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Destino Rueda {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(1).ToString().Trim() != "T" && oArregloLinea.GetValue(1).ToString().Trim() != "G")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Destino Rueda {" + oArregloLinea.GetValue(1) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Tipo Mercado
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Tipo Mercado {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(2).ToString().Trim() != "P" && oArregloLinea.GetValue(2).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Tipo Mercado {" + oArregloLinea.GetValue(2) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Operador Punta Contraria
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del Operador Punta Contraria {" + oArregloLinea.GetValue(3) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Punto de entrega
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del punto de entrerga{" + oArregloLinea.GetValue(4) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Tipo de Contrato
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del Tipo de Contrato {" + oArregloLinea.GetValue(5) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha de Negociacion
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(6).ToString());
                            lsFecha = oArregloLinea.GetValue(6).ToString().Split('/');
                            if (lsFecha[0].Trim().Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha de Negociación {" + oArregloLinea.GetValue(6) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de Negociación {" + oArregloLinea.GetValue(6) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida HOra de negociación
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                            if (oArregloLinea.GetValue(7).ToString().Length != 5)
                                lsCadenaErrores = lsCadenaErrores + "La Hora de Negociación {" + oArregloLinea.GetValue(7) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Hora de Negociación {" + oArregloLinea.GetValue(7) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Periodo de Entrega
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(8).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del Periodo de Entrega {" + oArregloLinea.GetValue(8) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Años
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(9).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Campo años {" + oArregloLinea.GetValue(9) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha Inicial
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(12).ToString());
                            lsFecha = oArregloLinea.GetValue(12).ToString().Split('/');
                            if (lsFecha[0].Trim().Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha Inicial {" + oArregloLinea.GetValue(12) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Fecha Inicial {" + oArregloLinea.GetValue(12) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Fecha final 
                        if (oArregloLinea.GetValue(13).ToString().Trim() != "")
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(13).ToString());
                                lsFecha = oArregloLinea.GetValue(13).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha Final {" + oArregloLinea.GetValue(13) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha Final {" + oArregloLinea.GetValue(13) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        /// Valida Precio
                        try
                        {
                            ldValor = Convert.ToDecimal(oArregloLinea.GetValue(14).ToString());
                            var sOferta = ldValor.ToString();
                            int iPos = sOferta.IndexOf(".");
                            if (iPos > 0)
                                if (sOferta.Length - iPos > 3)
                                    lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(14) + "} solo puede tener 2 decimales, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            if (Convert.ToDouble(sOferta) <= 0)
                                lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(14) + "} debe ser mayor que 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(14) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Cantidad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(15).ToString());
                            if (liValor <= 0)
                                lsCadenaErrores = lsCadenaErrores + "La Cantidad {" + oArregloLinea.GetValue(15) + "} debe ser mayor que 0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Cantidad {" + oArregloLinea.GetValue(15) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        //20170814 rq036
                        if (oArregloLinea.GetValue(16).ToString().Length > 8)
                            lsCadenaErrores = lsCadenaErrores + "El código del centro poblado {" + oArregloLinea.GetValue(16) + "} dbe tener máximo 8 carácteres , en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                            try
                            {
                                liValor = Convert.ToInt32(oArregloLinea.GetValue(16).ToString());
                                if (liValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "El código del centro poblado {" + oArregloLinea.GetValue(16) + "} no es válido , en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "El código del centro poblado {" + oArregloLinea.GetValue(16) + "} no es válido , en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        //20171130 rq026-17 valida fecha de suscripcion
                        if (oArregloLinea.GetValue(17).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha de Suscripción {" + oArregloLinea.GetValue(17) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(17).ToString());
                                lsFecha = oArregloLinea.GetValue(17).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Inválido en la Fecha de Suscripción {" + oArregloLinea.GetValue(17) + "},debe ser {YYYY/MM/DD}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Suscripción {" + oArregloLinea.GetValue(17) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        ///  20171130 rq026-17 Validar Sentido del Flujo
                        if (oArregloLinea.GetValue(18).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(18).ToString().Trim() != "NORMAL" && oArregloLinea.GetValue(18).ToString().Trim() != "CONTRA FLUJO")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Sentido del Flujo, valores válidos {NORMAL o CONTRA FLUJO} {" + oArregloLinea.GetValue(18) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// 20171130 rq026-17 Validar Presion Punto Terminacion
                        if (oArregloLinea.GetValue(19).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Presión Punto Terminación (Valor por defecto 0) {" + oArregloLinea.GetValue(19) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            string[] lsPresion;
                            try
                            {
                                if (oArregloLinea.GetValue(19).ToString().Trim().Length > 500)
                                    lsCadenaErrores = lsCadenaErrores + "Longitud del Campo Presión Punto de Terminación {" + oArregloLinea.GetValue(19) + "} supera el máximo permitido (500 caracteres), en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                else
                                {
                                    lsPresion = oArregloLinea.GetValue(19).ToString().Trim().Split('-');
                                    foreach (string Presion in lsPresion)
                                    {
                                        try
                                        {
                                            ldValor = Convert.ToDecimal(Presion.Trim());
                                        }
                                        catch (Exception)
                                        {
                                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presión Punto de Terminación {" + Presion.Trim() + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presión Punto de Terminación {" + oArregloLinea.GetValue(19) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// 20171130 rq026-17 Valida Fuente o campo MP 20160603
                        if (oArregloLinea.GetValue(20).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código de la Fuente {" + oArregloLinea.GetValue(20) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt16(oArregloLinea.GetValue(20).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Fuente {" + oArregloLinea.GetValue(20) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// 20171130 rq026-17 Validar No. Contrato
                        if (oArregloLinea.GetValue(21).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Contrato {" + oArregloLinea.GetValue(21) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// 20171130 rq026-17 valida tipo contrato
                        if (oArregloLinea.GetValue(22).ToString().Trim() != "N" && oArregloLinea.GetValue(22).ToString().Trim() != "S")
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el indicador de contrato variable  {" + oArregloLinea.GetValue(22) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// 20171130 rq026-17 valida conectado al snt
                        if (oArregloLinea.GetValue(23).ToString().Trim() != "N" && oArregloLinea.GetValue(23).ToString().Trim() != "S")
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el indicador de conectado al SNT {" + oArregloLinea.GetValue(23) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// 20171130 rq026-17 valida entrega en boca de pozo
                        if (oArregloLinea.GetValue(24).ToString().Trim() != "N" && oArregloLinea.GetValue(24).ToString().Trim() != "S")
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la entrega en boca de pozo {" + oArregloLinea.GetValue(24) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}