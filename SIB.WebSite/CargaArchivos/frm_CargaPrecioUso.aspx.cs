﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_CargaPrecioUso : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            //Titulo
            //Master.Titulo = "Carga Archivo Precios de Uso Gasoducto de Conexión";
            buttons.SwitchOnButton(EnumBotones.Cargue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsCadenaArchivo = new StringBuilder();
            string lsNombre;
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            int liNumeroParametros;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP };

            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                string lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                string[] lsErrores;
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoPrecioUso";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            string[] lsPrecio;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    var lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 4))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 4 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                    }
                    else
                    {
                        //fecha vigencia
                        try
                        {
                            lsFecha = oArregloLinea.GetValue(0).ToString().Split('/');
                            if (lsFecha[0].Length != 4 || oArregloLinea.GetValue(0).ToString().Length != 10)
                                lsCadenaErrores = lsCadenaErrores + "La fecha de vigencia {" + oArregloLinea.GetValue(0) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                                Convert.ToDateTime(oArregloLinea.GetValue(0).ToString());

                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha de vigencia {" + oArregloLinea.GetValue(0) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// gaosducto
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString().Trim());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en códigp del gasodcuto de conexión {" + oArregloLinea.GetValue(1) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// operacion
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString().Trim());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la operación {" + oArregloLinea.GetValue(2) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// precio uso
                        try
                        {
                            ldValor = Convert.ToDecimal(oArregloLinea.GetValue(3).ToString().Trim());
                            if (ldValor < 0)
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el precio de uso {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                            {
                                lsPrecio = oArregloLinea.GetValue(3).ToString().Split('.');
                                if (lsPrecio.Length > 1)
                                {
                                    if (lsPrecio[1].Trim().Length > 2)
                                        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el precio de uso {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                            }

                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el precio de uso {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

    }
}