﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaTrmContrato.aspx.cs"
    Inherits="CargaArchivos_frm_CargaTrmContrato" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="../css/estilo.css" rel="stylesheet" type="text/css" />
</head>
<body background="../Images/IMG_FondoGrisDeg.png">
    <form id="frmComisionista" runat="server">
    <asp:ScriptManager ID="ScriptManagerMestro" runat="server">
    </asp:ScriptManager>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblTitulo">
        <tr>
            <td align="center" class="th3">
                <asp:Label ID="lblTitulo" runat="server" Text="CARGA TASA DE CAMBIO CONTRATOS"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
        id="tblCaptura">
        <tr>
            <td class="td1">
                Seleccione el Archivo:
            </td>
            <td class="td2">
                <asp:FileUpload ID="FuArchivo" runat="server" EnableTheming="true" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="2" align="center">
                <br />
                <asp:Button ID="BtnCargar" runat="server" Text="Cargue Archivos" OnClick="BtnCargar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" /><br />
                <asp:HiddenField ID="hndID" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" class="td2">
                <asp:Label ID="ltCargaArchivo" runat="server" Width="100%" ForeColor="red"></asp:Label>
                <asp:HiddenField ID="hdfNomArchivo" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    </form>
</body>
</html>
