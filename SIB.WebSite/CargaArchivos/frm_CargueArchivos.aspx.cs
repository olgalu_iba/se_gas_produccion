﻿using System;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class CargaArchivos_frm_CargueArchivos : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Cargue Archivos Información";
    clConexion lConexion = null;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCarga"].ToString();
    string[] lsCarperta;
    string Carpeta;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lsCarperta = sRutaArc.Split('\\');
        Carpeta = lsCarperta[lsCarperta.Length - 2];

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlBusSubModulo, "m_sub_modulo_informacion", " estado = 'A' order by descripcion", 0, 1); //20181115 ajuste
            lConexion.Cerrar();
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            if (lsTabla != "m_operador")
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
            }
            else
            {
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector["codigo_operador"].ToString() + "-" + lLector["razon_social"].ToString();
            }
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        DateTime ldFecha;
        string[] lsNombreParametros = { "@P_fecha_cargue", "@P_codigo_sub_modulo" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
        string[] lValorParametros = { "", ddlBusSubModulo.SelectedValue };

        if (TxtFechaCargue.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaCargue.Text);
            }
            catch (Exception ex)
            {
                lblMensaje.Text += "Formato Inválido en el Campo Fecha Cargue. <br>";
            }
        }
        if (lblMensaje.Text == "")
        {
            try
            {
                if (TxtFechaCargue.Text.Trim().Length > 0)
                    lValorParametros[0] = TxtFechaCargue.Text.Trim();
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetCargueArchivosInformacion", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count <= 0)
                    lblMensaje.Text = "No se encontraron Registros.";
                else
                {
                    tblGrilla.Visible = true;
                    imbExcel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo Generar el Informe.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Modificar")
        {
            try
            {
                /// Llenar controles del Formulario
                ddlSubModulo.Items.Clear();
                lConexion.Abrir();
                LlenarControles(lConexion.gObjConexion, ddlSubModulo, "m_sub_modulo_informacion", " estado = 'A' order by descripcion", 0, 1); //20181115 ajuste
                lConexion.Cerrar();
                ddlSubModulo.SelectedValue = e.Item.Cells[6].Text; //20181123 rq052-18
                TxtCodigoArchivo.Text = e.Item.Cells[1].Text; //20181123 rq052-18
                TxtDescArchivo.Text = e.Item.Cells[4].Text; //20181123 rq052-18
                TxtDescMenu.Text = e.Item.Cells[8].Text; //20181123 rq052-18
                TxtOrden.Text = e.Item.Cells[5].Text; //20181123 rq052-18
                HplArchivo.NavigateUrl = "../" + Carpeta + "/" + e.Item.Cells[7].Text.Replace(@"\", "/"); ; //20181123 rq052-18
                HplArchivo.Target = "";
                tblSolicitud.Visible = true;
                tblGrilla.Visible = false;
                tblDatos.Visible = false;
                btnIngresar.Visible = false;
                btnActualizar.Visible = true;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
            }
        }
        if (((LinkButton)e.CommandSource).Text == "Ver Archivo")
        {
            try
            {
                string lsRuta = "../archivos/" + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[7].Text.Replace(@"\", "/"); //20181123 rq052-18
                if (this.dtgConsulta.Items[e.Item.ItemIndex].Cells[7].Text.Trim() != "&nbsp;" && this.dtgConsulta.Items[e.Item.ItemIndex].Cells[7].Text.Trim() != "") //20181123 rq052-18
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                else
                    lblMensaje.Text = "No ha Realizado la Carga del Archivo.!";

            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas en la Recuperación de la Información. " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Mertodo Que realiza la actualizacion de la informacion al solicitar cambio de precio.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_archivo", "@P_codigo_sub_modulo", "@P_descripcion_archivo", "@P_descripcion_menu", "@P_orden", "@P_ruta_archivo", "@P_ruta", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { TxtCodigoArchivo.Text, ddlSubModulo.SelectedValue, TxtDescArchivo.Text.ToUpper(), TxtDescMenu.Text, TxtOrden.Text, "", "", "2" };
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha;
        int liValor = 0;
        try
        {
            if (ddlSubModulo.SelectedValue == "0")
                lblMensaje.Text += "Debe Ingresar el Sub Módulo.!<br>";
            if (TxtDescArchivo.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Descripción del Archivo.!<br>";

            if (TxtDescMenu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Descripción del Menú.!<br>";
            else //20181115 ajuste
            {
                if (VerificarExistencia(" codigo_sub_modulo = " + ddlSubModulo.SelectedValue + "  and descripcion_menu = '" + this.TxtDescMenu.Text.Trim() + "' and codigo_archivo<>" + TxtCodigoArchivo.Text))
                    lblMensaje.Text += " La Descripción del Menú YA existe para el SubModulo <br>";
            }
            if (TxtOrden.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Orden.!<br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtOrden.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el Campo Orden.!<br>";
                }
            }
            if (lblMensaje.Text == "")
            {
                if (FuArchivo.FileName != "")
                {
                    try
                    {
                        if (!Directory.Exists(sRuta))
                            Directory.CreateDirectory(sRuta);
                        if (File.Exists(sRuta + "\\" + FuArchivo.FileName.ToString()))
                            lblMensaje.Text += "El archivo seleccionado ya existe<br>";
                        else
                            FuArchivo.SaveAs(sRuta + "\\" + FuArchivo.FileName);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                        lConexion.Cerrar();
                    }
                }
            }
            //else
            //  lblMensaje.Text += "Debe Seleccionar el Archivo. <br>";

            if (lblMensaje.Text == "")
            {
                if (FuArchivo.FileName != "")
                    lValorParametros[5] = lsFecha + "\\" + FuArchivo.FileName.ToString();
                lValorParametros[6] = sRutaArc;
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCargaArchivoInformacion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Actualización del Registro.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Archivo Actualizado Correctamente.!" + "');", true);
                    tblGrilla.Visible = true;
                    tblDatos.Visible = true;
                    tblSolicitud.Visible = false;
                    ddlSubModulo.SelectedValue = "0";
                    TxtDescArchivo.Text = "";
                    TxtOrden.Text = "";
                    TxtDescMenu.Text = "";
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Metodo que realiza la aprobacion por parte de la BMC del cambio de precio
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnIngresar_Click(object sender, EventArgs e)
    {
        string[] lsNombreParametros = { "@P_codigo_archivo", "@P_codigo_sub_modulo", "@P_descripcion_archivo", "@P_descripcion_menu", "@P_orden", "@P_ruta_archivo", "@P_ruta", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
        string[] lValorParametros = { "0", ddlSubModulo.SelectedValue, TxtDescArchivo.Text.ToUpper(), TxtDescMenu.Text, TxtOrden.Text, "", "", "1" };
        lblMensaje.Text = "";
        string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "-" + DateTime.Now.ToShortDateString().Substring(3, 2) + "-" + DateTime.Now.ToShortDateString().Substring(0, 2);
        string sRuta = sRutaArc + lsFecha;
        int liValor = 0;
        try
        {
            if (ddlSubModulo.SelectedValue == "0")
                lblMensaje.Text += "Debe Ingresar el Sub Módulo.!<br>";
            if (TxtDescArchivo.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Descripción del Archivo.!<br>";
            if (TxtDescMenu.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar la Descripción del Menú.!<br>";
            else //20181115 ajuste
            {
                if (VerificarExistencia(" codigo_sub_modulo = " + ddlSubModulo.SelectedValue + "  and descripcion_menu = '" + this.TxtDescMenu.Text.Trim() + "' "))
                    lblMensaje.Text += " La Descripción del Menú YA existe para el SubModulo <br>";
            }
            if (TxtOrden.Text.Trim().Length <= 0)
                lblMensaje.Text += "Debe Ingresar el Orden.!<br>";
            else
            {
                try
                {
                    liValor = Convert.ToInt32(TxtOrden.Text);
                }
                catch (Exception ex)
                {
                    lblMensaje.Text += "Valor Inválido en el Campo Orden.!<br>";
                }
            }
            if (FuArchivo.FileName == "")
                lblMensaje.Text += "Debe Seleccionar el Archivo. <br>";
            if (lblMensaje.Text == "")
            {
                if (FuArchivo.FileName != "")
                {
                    try
                    {
                        if (!Directory.Exists(sRuta))
                            Directory.CreateDirectory(sRuta);
                        if (File.Exists(sRuta + "\\" + FuArchivo.FileName.ToString()))
                            lblMensaje.Text += "El archivo seleccionado ya existe<br>";
                        else
                            FuArchivo.SaveAs(sRuta + "\\" + FuArchivo.FileName);
                    }
                    catch (Exception ex)
                    {
                        lblMensaje.Text += "Problemas en la Carga del Archivo. " + ex.Message.ToString();
                        lConexion.Cerrar();
                    }
                }
                else
                    lblMensaje.Text += "Debe Seleccionar el Archivo. <br>";
            }

            if (lblMensaje.Text == "")
            {
                if (FuArchivo.FileName != "")
                    lValorParametros[5] = lsFecha + "\\" + FuArchivo.FileName.ToString();
                lValorParametros[6] = sRutaArc;
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetCargaArchivoInformacion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presentó un Problema en la Carga del Archivo.! " + goInfo.mensaje_error.ToString();
                    lConexion.Cerrar();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Registro Cargado Correctamente.!" + "');", true);
                    tblGrilla.Visible = true;
                    tblDatos.Visible = true;
                    tblSolicitud.Visible = false;
                    ddlSubModulo.SelectedValue = "0";
                    TxtDescArchivo.Text = "";
                    TxtOrden.Text = "";
                    TxtDescMenu.Text = "";
                    CargarDatos();
                }
            }
        }
        catch (Exception ex)
        {
            lblMensaje.Text = ex.Message;
        }
    }
    /// <summary>
    /// Regresar a la pantalla anterior
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRegresar_Click(object sender, EventArgs e)
    {
        tblSolicitud.Visible = false;
        tblGrilla.Visible = true;
        tblDatos.Visible = true;
    }
    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    {
        lblMensaje.Text = "";
        try
        {
            string lsNombreArchivo = Session["login"] + "InfExcelCarInf" + DateTime.Now + ".xls";
            StringBuilder lsb = new StringBuilder();
            StringWriter lsw = new StringWriter(lsb);
            HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
            Page lpagina = new Page();
            HtmlForm lform = new HtmlForm();
            lpagina.EnableEventValidation = false;
            lpagina.Controls.Add(lform);
            dtgConsulta.EnableViewState = false;
            dtgConsulta.Columns[0].Visible = false; //20181123 rq052-18
            dtgConsulta.Columns[9].Visible = false; //20181123 rq052-18
            dtgConsulta.Columns[10].Visible = false; //20181123 rq052-18
            lform.Controls.Add(dtgConsulta);
            lpagina.RenderControl(lhtw);
            Response.Clear();

            Response.Buffer = true;
            Response.ContentType = "aplication/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
            Response.ContentEncoding = System.Text.Encoding.Default;

            Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
            Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["login"] + "</td></tr></table>");
            Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Cargue Archivos Información" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
            Response.Write(lsb.ToString());
            Response.End();
            Response.Flush();
            dtgConsulta.Columns[0].Visible = true; //20181123 rq052-18
            dtgConsulta.Columns[9].Visible = true; //20181123 rq052-18
            dtgConsulta.Columns[10].Visible = true; //20181123 rq052-18
        }
        catch (Exception ex)
        {
            lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        /// Llenar controles del Formulario
        ddlSubModulo.Items.Clear();
        lConexion.Abrir();
        LlenarControles(lConexion.gObjConexion, ddlSubModulo, "m_sub_modulo_informacion", " estado = 'A' order by descripcion", 0, 1); //20181115 ajuste
        lConexion.Cerrar();
        btnActualizar.Visible = false;
        btnIngresar.Visible = true;
        HplArchivo.Visible = false;
        tblSolicitud.Visible = true;
        tblGrilla.Visible = false;
        tblDatos.Visible = false;
        TxtCodigoArchivo.Text = "";
        ddlSubModulo.SelectedValue = "0";
        TxtDescArchivo.Text = "";
        TxtDescMenu.Text = "";
        TxtOrden.Text = "";
    }
    /// <summary>
    /// Nombre: VerificarExistencia
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para ejecutar el proceso de verificacion en la base de datos de la existencia
    ///              del codigo de la Actividad Exonomica.
    /// Modificacion:
    /// </summary>
    /// <returns></returns>
    protected bool VerificarExistencia(string lswhere)
    {
        return DelegadaBase.Servicios.ValidarExistencia("t_archivos_informacion ", lswhere, goInfo);
    }

    /// <summary>
    /// Nombre: ChkTodos_CheckedChanged
    /// Fecha: Abril 24 21 de 2012
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para chekear todos los registros
    /// Modificacion:
    /// </summary>
    protected void ChkTodos_CheckedChanged(object sender, EventArgs e)
    {
        //Selecciona o deselecciona todos los items del datagrid segun el checked del control
        CheckBox chk = (CheckBox)(sender);
        foreach (DataGridItem Grilla in this.dtgConsulta.Items)
        {
            CheckBox Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            Checkbox.Checked = chk.Checked;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        lblMensaje.Text = "";
        int liProce = 0;
        string[] lsNombreParametros = { "@P_codigo_archivo", "@P_accion" };
        SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
        String[] lValorParametros = { "0", "3" };

        lConexion.Abrir();
        // Defino la trnasacción para el manejo de la consistencia de los datos.
        foreach (DataGridItem Grilla in this.dtgConsulta.Items)
        {
            CheckBox Checkbox = null;
            Checkbox = (CheckBox)Grilla.Cells[0].Controls[1];
            if (Checkbox.Checked == true)
            {
                liProce++;
                try
                {
                    string sRuta = sRutaArc + Grilla.Cells[7].Text.Trim();  
                    if (File.Exists(sRuta))
                        File.Delete(sRuta);
                }
                catch (Exception ex)
                {
                }
                lValorParametros[0] = Grilla.Cells[1].Text.Trim();
                DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_SetCargaArchivoInformacion", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    lblMensaje.Text = "Se presento un Problema en la eliminacion del archivo  ";
                    lConexion.Cerrar();
                    break;
                }
            }
        }
        if (liProce == 0)
        {
            lblMensaje.Text = "Debe seleccionar al menos un archivo para eliminar.<br>";
            lConexion.Cerrar();
        }
        else
            CargarDatos();
    }

}