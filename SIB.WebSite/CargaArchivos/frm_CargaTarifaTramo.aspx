﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaTarifaTramo.aspx.cs" Inherits="CargaArchivos.frm_CargaTarifaTramo" MasterPageFile="~/PlantillaPrincipal.master" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="Declaración de Cargos por Tramo (Archivo Plano)" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <asp:Label Text="Resolución aprobación CREG" AssociatedControlID="TxtResolucion" runat="server" />
                            <asp:TextBox ID="TxtResolucion" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <asp:Label Text="Nombre Diario" AssociatedControlID="TxtDiario" runat="server" />
                            <asp:TextBox ID="TxtDiario" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <asp:Label Text="Fecha Publicación" AssociatedControlID="TxtFechaPub" runat="server" />
                            <asp:TextBox ID="TxtFechaPub" placeholder="yyyy/mm/dd" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <asp:Label Text="Soporte Publicación" AssociatedControlID="FuSoporte" runat="server" />
                            <asp:FileUpload ID="FuSoporte" CssClass="form-control" Width="100%" runat="server" EnableTheming="true" />
                            <asp:HiddenField ID="hndArchivo" runat="server" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Archivo" AssociatedControlID="FuArchivo" runat="server" />
                            <asp:FileUpload ID="FuArchivo" CssClass="form-control" Width="100%" runat="server" EnableTheming="true" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
