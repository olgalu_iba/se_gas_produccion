﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaPlanoVerifMay : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            //Master.Titulo = "Registros Operativos";
            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo;
            string lsNombre;
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario };

            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                string[] lsErrores;
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoContratosRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    bool oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);

                    if (oCargaOK)
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoVerifContMay";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        var lsErrorMail = "";
                        var lsMensaje = "";
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                if (lLector["ind_error"].ToString() == "S")
                                    lsCadenaArchivo.Append($"{lLector["mensaje"]}<br>");
                                else
                                {
                                    lsMensaje = lLector["Mensaje"] + "<br><br>";
                                    lsMensaje += "Cordialmente, <br><br><br>";
                                    lsMensaje += "Administrador SEGAS <br>";

                                    var mailV = new clEmail(lLector["mail"].ToString(), "Operaciones Otras Transacciones del Mercado Mayorista", lsMensaje, "");
                                    lsErrorMail = mailV.enviarMail(ConfigurationManager.AppSettings["UserSmtp"], ConfigurationManager.AppSettings["PwdSmtp"], ConfigurationManager.AppSettings["ServidorSmtp"], goInfo.Usuario, "Sistema de Gas") + "<br>";
                                }
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);

                ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 26))  //20161219 rq111 mayoristga traspaso  //20170530 divipola  //20170926 rq027-17
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 26 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20161219 rq111 mayoristga traspaso  //20170530 divipola  //20170926 rq027-17
                    }
                    else
                    {
                        /// Valida Fecha de Negociacion
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha de Negociacion {" + oArregloLinea.GetValue(0) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(0).ToString());
                                lsFecha = oArregloLinea.GetValue(0).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha de Negociacion {" + oArregloLinea.GetValue(0) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Negociacion {" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Tipo de Mercado
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Tipo de Mercado {" + oArregloLinea.GetValue(1) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(1).ToString().Trim() != "O")
                                lsCadenaErrores = lsCadenaErrores + "Valor Invalido en Tipo de Mercado {" + oArregloLinea.GetValue(1) + "},debe ser {O}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Destino del Contrato
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Destino del Contrato {" + oArregloLinea.GetValue(2) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(2).ToString().Trim() != "G" && oArregloLinea.GetValue(2).ToString().Trim() != "T" && oArregloLinea.GetValue(2).ToString().Trim() != "A") //20161219 rq111 mayorista transporte  //20170926 rq027-17
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Tipo de Contrato {" + oArregloLinea.GetValue(2) + "},debe ser {G=Gas}, {T=Transporte} o {A=Ambos}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; //20161219 rq111 mayorista transporte //20170926 rq027-17
                        }
                        /// Validar Codigo del Operador Contraparte
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Codigo del Operador Contraparte {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Codigo del Operador Contraparte {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Contrato {" + oArregloLinea.GetValue(4) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha de Suscripcion {" + oArregloLinea.GetValue(5) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(5).ToString());
                                lsFecha = oArregloLinea.GetValue(5).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha de Suscripcion {" + oArregloLinea.GetValue(5) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Suscripcion {" + oArregloLinea.GetValue(5) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Punto de Entrega / Tramo {" + oArregloLinea.GetValue(6) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(6).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto de Entrega / Tramo {" + oArregloLinea.GetValue(6) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Codigo Modalidad Contractual
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Modalidad Contractual {" + oArregloLinea.GetValue(7) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(7).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Modalidad Contractual {" + oArregloLinea.GetValue(7) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(8).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad {" + oArregloLinea.GetValue(8) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(8).ToString().Trim());
                                /// 20170926 rq027-27
                                //if (liValor <= 0)
                                //    lsCadenaErrores = lsCadenaErrores + "La Cantidad {" + oArregloLinea.GetValue(8).ToString() + "} no puder ser 0, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad {" + oArregloLinea.GetValue(8) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Precio
                        if (oArregloLinea.GetValue(9).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Precio {" + oArregloLinea.GetValue(9) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(9).ToString().Trim());
                                lsFecha = oArregloLinea.GetValue(9).ToString().Trim().Split('.');
                                if (lsFecha.Length > 1)
                                {
                                    if (lsFecha[1].Trim().Length > 2)
                                        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(9) + "}, solo debe tener maximo 2 decimales, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                                if (ldValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(9) + "} no puder ser 0, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(9) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fecha Inicial
                        if (oArregloLinea.GetValue(10).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha Inicial {" + oArregloLinea.GetValue(10) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(10).ToString());
                                lsFecha = oArregloLinea.GetValue(10).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha Inicial {" + oArregloLinea.GetValue(10) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Inicial {" + oArregloLinea.GetValue(10) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fecha Final
                        if (oArregloLinea.GetValue(11).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha Final {" + oArregloLinea.GetValue(11) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(11).ToString());
                                lsFecha = oArregloLinea.GetValue(11).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha Final {" + oArregloLinea.GetValue(11) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Final {" + oArregloLinea.GetValue(11) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Tipo de Garantia
                        if (oArregloLinea.GetValue(13).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Tipo de Garantia {" + oArregloLinea.GetValue(13) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        /// Validar Valor Garantia
                        if (oArregloLinea.GetValue(14).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Valor de la Garantia {" + oArregloLinea.GetValue(14) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(14).ToString().Trim());
                                if (ldValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "El Valor de la Garantía {" + oArregloLinea.GetValue(14) + "} no puede ser menor que 0, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el valor de la garantía {" + oArregloLinea.GetValue(14) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fecha Pago Garantia
                        if (oArregloLinea.GetValue(15).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha Pago Garantia {" + oArregloLinea.GetValue(15) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(15).ToString());
                                lsFecha = oArregloLinea.GetValue(15).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha Pago Garantia {" + oArregloLinea.GetValue(15) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Pago de Garantia {" + oArregloLinea.GetValue(15) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Codigo Mercado Relevante
                        if (oArregloLinea.GetValue(16).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Codigo del Mercado Relevante Sum Usuario NO Regulado {" + oArregloLinea.GetValue(16) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(16).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Mercado Relevante Sum. Usuario NO Regulado {" + oArregloLinea.GetValue(16) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar usuario NO Regulado con Conexion SNT
                        if (oArregloLinea.GetValue(17).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(17).ToString().Trim() != "N" && oArregloLinea.GetValue(17).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Usuario NO Regulado Con Conexion SNT, valores válidos {N o S} {" + oArregloLinea.GetValue(17) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        }
                        /// Validar Departamento Punto de Salida
                        /// 20170530 divipola
                        if (oArregloLinea.GetValue(18).ToString().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Departamento Punto de Salida {" + oArregloLinea.GetValue(18) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(18).ToString());
                                //20170530 divipola
                                if (oArregloLinea.GetValue(18).ToString() != "0" && oArregloLinea.GetValue(18).ToString().Trim().Length != 2)
                                    lsCadenaErrores = lsCadenaErrores + "La longitud del  Departamento Punto de salida {" + oArregloLinea.GetValue(18) + "}, no es válido en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Departamento Punto de Salida {" + oArregloLinea.GetValue(18) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Municipio Punto de Salida
                        /// 20170530 divipola
                        if (oArregloLinea.GetValue(19).ToString().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Municipio Punto de Salida {" + oArregloLinea.GetValue(19) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(19).ToString());
                                //20170530 divipola
                                if (oArregloLinea.GetValue(19).ToString() != "0" && oArregloLinea.GetValue(19).ToString().Trim().Length != 5)
                                    lsCadenaErrores = lsCadenaErrores + "La longitud del Municipio Punto de Salida {" + oArregloLinea.GetValue(19) + "}, no es válido en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Municipio Punto de Salida {" + oArregloLinea.GetValue(19) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Mercado Relevante Sistema de Distribucion
                        if (oArregloLinea.GetValue(20).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Mercado Relevante de Sistema de Distribucion {" + oArregloLinea.GetValue(20) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(20).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Mercado Relevante Sistema de Distribucion {" + oArregloLinea.GetValue(20) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }

                        /// Validar Id del Registro
                        if (oArregloLinea.GetValue(21).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Id del Registro {" + oArregloLinea.GetValue(21) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(21).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Id de Registro {" + oArregloLinea.GetValue(21) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Nit Usuario NO Regulado
                        if (oArregloLinea.GetValue(22).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Nit del Usuario NO Regulado {" + oArregloLinea.GetValue(21) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        /// Validar centro poblado
                        /// 20161219 rq111 mayorisya transporte
                        if (oArregloLinea.GetValue(23).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código de la ruta {" + oArregloLinea.GetValue(23) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(23).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el código de la ruta contratada{" + oArregloLinea.GetValue(23) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar centro poblado
                        /// 20170530 divipola
                        if (oArregloLinea.GetValue(24).ToString().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el centro poblado del Punto de Salida {" + oArregloLinea.GetValue(24) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(24).ToString());
                                if (oArregloLinea.GetValue(24).ToString() != "0" && oArregloLinea.GetValue(24).ToString().Trim().Length != 8)
                                    lsCadenaErrores = lsCadenaErrores + "La longitud del Centro Poblado del Punto de Salida {" + oArregloLinea.GetValue(24) + "}, no es válido en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el centro poblado del Punto de Salida {" + oArregloLinea.GetValue(24) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// 20170926 rq027-27
                        if (oArregloLinea.GetValue(25).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la capacidad de transporte {" + oArregloLinea.GetValue(25) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(25).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido la capacidad de transporte {" + oArregloLinea.GetValue(25) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}