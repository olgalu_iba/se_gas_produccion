﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaCapacTras : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            //Master.Titulo = "Registros Operativos";
            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsCadenaArchivo = new StringBuilder();
            var lsRutaArchivo = "";
            var lsNombre = "";
            string[] lsErrores = { "", "" };
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
            object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP };
            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo,
                        ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre,
                        ConfigurationManager.AppSettings["UserFtp"],
                        ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoDecTras";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0;
                                liNumeroParametros <= lsNombreParametrosO.Length - 1;
                                liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros],
                                    lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this,
                                HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga",
                                    CultureInfo.CurrentCulture)?.ToString());
                        }

                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this,
                            HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)
                                ?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo,
                        HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)
                            ?.ToString(), "Usuario : " + goInfo.nombre);
                }

                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);

                ScriptManager.RegisterStartupScript(this, GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                Toastr.Error(this,
                    HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) +
                    ex.Message);
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 5))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 5 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<BR>";
                    }
                    else
                    {
                        /// Validar Codigo del tramo 
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Tramo {" + oArregloLinea.GetValue(0) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar la capcidad firme
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la capacidad firme {" + oArregloLinea.GetValue(1) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar la capcidad disponible
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString().Trim());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la capacidad disponible primaria{" + oArregloLinea.GetValue(2) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar la fecha de inicio
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(3).ToString());
                            lsFecha = oArregloLinea.GetValue(3).ToString().Split('/');
                            if (lsFecha[0].Trim().Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha de inicio {" + oArregloLinea.GetValue(3) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Fecha de inicio {" + oArregloLinea.GetValue(3) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar la fecha de fin
                        try
                        {
                            ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(4).ToString());
                            lsFecha = oArregloLinea.GetValue(4).ToString().Split('/');
                            if (lsFecha[0].Trim().Length != 4)
                                lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha final {" + oArregloLinea.GetValue(4) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La Fecha final {" + oArregloLinea.GetValue(4) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}