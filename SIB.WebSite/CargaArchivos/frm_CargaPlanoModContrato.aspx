﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaPlanoModContrato.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="CargaArchivos.frm_CargaPlanoModContrato" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="Archivos Modificación de Contratos Registrados" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <%--Carga Modificación Contratos--%>
                <segas:CargaModContratos ID="cargaModContratos" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
