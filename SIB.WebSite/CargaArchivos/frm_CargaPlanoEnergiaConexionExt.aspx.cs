﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_CargaPlanoEnergiaConexionExt : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            //Titulo
            Master.Titulo = "Registros Operativos";

            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                string lsRutaArchivo;
                string lsNombre;
                string[] lsErrores = { "", "" };
                var lsCadenaArchivo = new StringBuilder();
                var oTransOK = true;
                var oCargaOK = true;
                SqlDataReader lLector;
                var lComando = new SqlCommand();
                var liNumeroParametros = 0;
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp" };
                SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
                object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP };
                lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                /// Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoEnergiaConexionExt";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }

                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            var liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);

            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 8))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 8 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                    }
                    else
                    {
                        //valida la fecha
                        try
                        {
                            var lsFecha = oArregloLinea.GetValue(0).ToString().Split('/');
                            if (lsFecha[0].Length != 4 || oArregloLinea.GetValue(0).ToString().Length != 10)
                                lsCadenaErrores = lsCadenaErrores + "La fecha {" + oArregloLinea.GetValue(0) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            else
                                Convert.ToDateTime(oArregloLinea.GetValue(0).ToString());

                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La fecha {" + oArregloLinea.GetValue(0) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Validar gasoducto
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del gasoducto {" + oArregloLinea.GetValue(1) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// oepacion
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El número de operación {" + oArregloLinea.GetValue(2) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Tipo entrega
                        if (oArregloLinea.GetValue(3).ToString() != "E" && oArregloLinea.GetValue(3).ToString() != "S" && oArregloLinea.GetValue(3).ToString() != "T" && oArregloLinea.GetValue(3).ToString() != "R" && oArregloLinea.GetValue(3).ToString() != "G")
                            lsCadenaErrores = lsCadenaErrores + "El tipo de entrega  {" + oArregloLinea.GetValue(3) + "} es inválido,{E/S/T/R/G} en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        /// punto
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El punto de entrega {" + oArregloLinea.GetValue(4) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// demanda
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El tipo de demanda{" + oArregloLinea.GetValue(5) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// cantidad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(6).ToString());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La cantidad {" + oArregloLinea.GetValue(6) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// kpcd
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(7).ToString());
                        }
                        catch (Exception)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El equivalente KPCD {" + oArregloLinea.GetValue(7) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}