﻿

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/PlantillaPrincipal.master" CodeFile="frm_CargueArchivos.aspx.cs"
    Inherits="CargaArchivos_frm_CargueArchivos" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table border="0" align="center" cellpadding="3" cellspacing="2" runat="server" id="tblTitulo"
            width="100%">
            <tr>
                <td align="center" class="th1" style="width: 80%;">
                    <asp:Label ID="lblTitulo" runat="server" forecolor="white"></asp:Label>
                </td>
                <td align="center" class="th1" style="width: 20%;">
                    <asp:ImageButton ID="imbExcel" runat="server" ImageUrl="~/Images/exel 2007 3D.png"
                        Height="40" OnClick="imbExcel_Click" Visible="false" />
                </td>
            </tr>
        </table>
        <br /><br /><br /><br /><br /><br /><br />
        <table id="tblDatos" runat="server" border="0" align="center" cellpadding="3" cellspacing="2"
            width="90%">
            <tr>
                <td class="td1">
                    Fecha Cargue
                </td>
                <td class="td2">
                    <asp:TextBox ID="TxtFechaCargue" placeholder="yyyy/mm/dd" Width="100%" runat="server" CssClass="form-control datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>
                </td>
                <td class="td1">
                    Sub Módulo
                </td>
                <td class="td2">
                    <asp:DropDownList ID="ddlBusSubModulo" runat="server" Width="250px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="th1" colspan="8" align="center">
                    <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click" />
                    <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" OnClick="btnNuevo_Click" />
                    <%--20181123 rq052-18--%>
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" OnClick="btnEliminar_Click"  OnClientClick ="return confirm('Está seguro de eliminar el(los) documento(s) seleccionado(s)?')"/>
                </td>
            </tr>
        </table>
    </div>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblMensaje">
        <tr>
            <td colspan="3" align="center">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" align="center" cellpadding="3" cellspacing="2" width="85%" runat="server"
        id="tblGrilla" visible="false">
        <tr>
            <td colspan="3" align="center">
                <div style="overflow: scroll; width: 1150px; height: 450px;">
                    <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" PagerStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2" HeaderStyle-CssClass="th1"
                        OnEditCommand="dtgConsulta_EditCommand">
                        <Columns>
                            <%--0--%>
                            <%--20181123 rq052-18--%>
                            <asp:TemplateColumn HeaderText="Selec">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkTodos" runat="server" OnCheckedChanged="ChkTodos_CheckedChanged"
                                        AutoPostBack="true" Text="Seleccionar Todos" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSeleccionar" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--1--%>
                            <asp:BoundColumn DataField="codigo_archivo" HeaderText="Código Archivo" ItemStyle-HorizontalAlign="Left">
                            </asp:BoundColumn>
                            <%--2--%>
                            <asp:BoundColumn DataField="fecha_cargue" HeaderText="Fecha Cargue"></asp:BoundColumn>
                            <%--3--%>
                            <asp:BoundColumn DataField="sub_modulo" HeaderText="Sub Módulo" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--4--%>
                            <asp:BoundColumn DataField="descripcion_archivo" HeaderText="Descripción Archivo"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px"></asp:BoundColumn>
                            <%--5--%>
                            <asp:BoundColumn DataField="orden" HeaderText="Orden" ItemStyle-HorizontalAlign="Center">
                            </asp:BoundColumn>
                            <%--6--%>
                            <asp:BoundColumn DataField="codigo_sub_modulo" Visible="false"></asp:BoundColumn>
                            <%--7--%>
                            <asp:BoundColumn DataField="ruta_archivo" Visible="false"></asp:BoundColumn>
                            <%--8--%>
                            <asp:BoundColumn DataField="descripcion_menu" Visible="false"></asp:BoundColumn>
                            <%--9--%>
                            <asp:EditCommandColumn HeaderText="Ver" EditText="Ver Archivo"></asp:EditCommandColumn>
                            <%--10--%>
                            <asp:EditCommandColumn HeaderText="Modificar" EditText="Modificar"></asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <table id="tblSolicitud" runat="server" border="0" align="center" cellpadding="3"
        cellspacing="2" width="80%" visible="false">
        <tr>
            <td class="td1">
                Código Archivo
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtCodigoArchivo" runat="server" ValidationGroup="detalle" Width="100px"
                    Enabled="false"></asp:TextBox>
            </td>
            <td class="td1">
                Sub Módulo
            </td>
            <td class="td2">
                <asp:DropDownList ID="ddlSubModulo" runat="server" Width="250px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Descripción Archivo
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDescArchivo" runat="server" ValidationGroup="detalle" Width="200px"
                    MaxLength="200"></asp:TextBox>
            </td>
            <td class="td1">
                Descripción Menú
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtDescMenu" runat="server" ValidationGroup="detalle" Width="200px"
                    MaxLength="200"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="td1">
                Orden
            </td>
            <td class="td2">
                <asp:TextBox ID="TxtOrden" runat="server" ValidationGroup="detalle" Width="100px"
                    MaxLength="2"></asp:TextBox>
            </td>
            <td class="td1">
                Archivo
            </td>
            <td class="td2">
                <asp:FileUpload ID="FuArchivo" runat="server" />
                <asp:HyperLink ID="HplArchivo" runat="server">Ver Archivo</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                <asp:Button ID="btnActualizar" runat="server" Text="Actualizar" OnClick="btnActualizar_Click"
                    OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                <asp:Button ID="btnRegresar" runat="server" Text="Regresar" OnClick="btnRegresar_Click" />
            </td>
        </tr>
        <tr>
            <td class="th1" colspan="6" align="center">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px"></asp:Label>
            </td>
        </tr>
    </table>
 </asp:Content>