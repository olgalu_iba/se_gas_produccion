﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class CargaArchivos_frm_AutCargaNegDirectaEntemp : System.Web.UI.Page
    {
        InfoSessionVO goInfo = null;
        clConexion lConexion = null;
        String strRutaCarga;
        String strRutaFTP;
        SqlDataReader lLector;


        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            SqlDataReader lLector;
            lblMensaje.Text = "";
            SqlCommand lComando = new SqlCommand();
            lConexion = new clConexion(goInfo);
            try
            {
                string[] lsNombreParametros = { "@P_codigo_usuario" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int };
                Object[] lValorParametros = { goInfo.codigo_usuario };
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_GetPlanoNegoDirHoraNeg", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    while (lLector.Read())
                        lblMensaje.Text += lLector["mensaje"].ToString();

                    Toastr.Warning(this, lblMensaje.Text);
                    lblMensaje.Text = "";
                    BtnAceptar.Enabled = true;
                }
                else
                {
                    Toastr.Warning(this, "No hay registros para cargar");
                    BtnAceptar.Enabled = false;
                }
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {

            }

        }
        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = "";
                SqlDataReader lLector;
                SqlCommand lComando = new SqlCommand();
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametros = { "@P_codigo_usuario", "@P_codigo_operador" };
                SqlDbType[] lTipoparametros = { SqlDbType.Int, SqlDbType.VarChar };
                Object[] lValorParametros = { goInfo.codigo_usuario, goInfo.cod_comisionista };
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetPlanoNegoDirecta", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (lLector.HasRows)
                {
                    string lsError = "";
                    while (lLector.Read())
                    {
                        lsError += lLector["mensaje"].ToString() + "\\n";  //rq026-17  20171130
                    }
                    Toastr.Warning(this, lsError);
              
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true); //rq026-17  20171130
                }
                else
                {
                    Toastr.Warning(this, "Operaciones disponibles en el módulo Registro de Contratos, para ingreso de información transaccional.!");
                
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas en la Carga del Plano.!");           
            }
        }
        protected void BtnCancelar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CLOSE_WINDOW", "window.close();", true);
        }
    }
}