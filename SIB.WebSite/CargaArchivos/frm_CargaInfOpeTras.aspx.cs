﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_CargaInfOpeTras : Page
    {
        private InfoSessionVO goInfo = null;
        private clConexion lConexion = null;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"].ToString();
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"].ToString();

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (!IsPostBack)
            {
                //Titulo
                //Master.Titulo = "Subasta";
                //Se inicializan los botones 
                EnumBotones[] botones = { EnumBotones.Cargue };
                buttons.Inicializar(botones: botones);
            }

            var liHoraAct = 0;
            var lsHoraMax = "0";
            var liValFin = 0;
            lConexion.Abrir();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
            if (lLector.HasRows)
            {
                lLector.Read();
                lsHoraMax = lLector["hora_max_declara_opera"].ToString();
            }
            lLector.Close();
            lLector.Dispose();
            var lblMensaje = new StringBuilder();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 5 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Append("El Operador no está obligado a ingresar la información operativa de energía recibida.<br>");
            }
            lLector.Close();
            lLector.Dispose();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 7 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Append("El Operador no está obligado a ingresar la información operativa de energía tomada.<br>");
            }
            lLector.Close();
            lLector.Dispose();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 4 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Append("El Operador no está obligado a ingresar la información operativa de energía de parqueo.<br>");
            }
            lLector.Close();
            lLector.Dispose();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 9 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Append("El Operador no está obligado a ingresar la información operativa de energía autorizada a transportar.<br>");
            }
            lLector.Close();
            lLector.Dispose();
            //20190306 rq013-19
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 13 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Append("El Operador no está obligado a ingresar la información operativa de energía de estación de compresión.<br>");
            }
            lLector.Close();
            lLector.Dispose();
            lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 14 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
            if (lLector.HasRows)
            {
                lblMensaje.Append("El Operador no está obligado a ingresar la información operativa de energía tomada conforme asignación.<br>");
            }
            lLector.Close();
            lLector.Dispose();
            //20190306 fin rq013-19
            lConexion.Cerrar();
            if (lblMensaje.ToString() != "")
                Toastr.Warning(this, lblMensaje.ToString());
            liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
            liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
            if (liHoraAct <= liValFin) return;
            Toastr.Error(this, "Está intentando Registrar Información Fuera del Horario.");
            buttons.SwitchOnButton(EnumBotones.Ninguno);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            string lsRutaArchivo;
            string lsNombre;
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp" ,"@P_valida" }; //20220705
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20220705
            object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP, "S" }; //20220705
            lsNombre = DateTime.Now.Millisecond.ToString() + FuArchivo.FileName.ToString();
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"].ToString() + lsNombre, ConfigurationManager.AppSettings["UserFtp"].ToString(), ConfigurationManager.AppSettings["PwdFtp"].ToString()))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoInfOpeTras";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        string lsConfirma = "N"; //20220705
                        lblMensajeConf.Text = "";//20220705
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                //20220705
                                if (lLector["ind_confirma"].ToString() == "S")
                                {
                                    lblMensajeConf.Text += lLector["Mensaje"].ToString() + "<br>";
                                    lsConfirma = "S";
                                }
                                else
                                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                            //20220705
                            if (lsConfirma == "S")
                            {
                                hdfArchivo.Value = lsNombre;
                                Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCargaInfOpe", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            var liValor = 0;
            DateTime ldFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    var lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 7)) //20190306 rq013-19
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 7 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> "; //20190306 rq013-19
                    }
                    else
                    {
                        /// Validar codigo de proceso
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del proceso {" + oArregloLinea.GetValue(0).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> ";
                        }
                        /// Valida punto de entrada/salida
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del punto de entrada/salida/Estación compresión {" + oArregloLinea.GetValue(1).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> "; //20190306 rq013-19
                        }
                        /// Valida tramo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del tramo {" + oArregloLinea.GetValue(2).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> ";
                        }
                        /// Valida el remmitente
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "el código del remitente{" + oArregloLinea.GetValue(3).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> ";
                        }
                        /// Valida el contrato
                        if (oArregloLinea.GetValue(4).ToString().Length > 50)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El número del contrato{" + oArregloLinea.GetValue(4).ToString() + "} es muy lago, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> ";
                        }
                        else  //20160803 eengia tomada
                        {
                            var oExpresion = new Regex(@"^([A-Za-z0-9]*$)");
                            if (!oExpresion.IsMatch(oArregloLinea.GetValue(4).ToString()))
                                lsCadenaErrores = lsCadenaErrores + "El número del contrato{" + oArregloLinea.GetValue(4).ToString() + "} solo debe tener letras o números, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> ";
                        }
                        /// 20190306 rq013-19
                        /// EL tipo dse operacion
                        if (oArregloLinea.GetValue(5).ToString() != "" && oArregloLinea.GetValue(5).ToString() != "G" && oArregloLinea.GetValue(5).ToString() != "E")
                        {
                            lsCadenaErrores = lsCadenaErrores + "El tipo de operación {" + oArregloLinea.GetValue(5).ToString() + "} es inválido, debe ser G/E/,n la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> ";
                        }
                        /// Valida la cantidad
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(6).ToString()); /// 20190306 rq013-19
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La cantidad {" + oArregloLinea.GetValue(6).ToString() + "} es inválida, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano <br> "; /// 20190306 rq013-19
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20220705
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_valida" }; //20220705
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20220705
            object[] lValorParametrosO = { hdfArchivo.Value, goInfo.cod_comisionista, strRutaFTP, "N" }; //20220705
            var lsCadenaArchivo = new StringBuilder();
            lConexion.Abrir();
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_ValidaPlanoInfOpeTras";
            lComando.CommandTimeout = 3600;
            if (lsNombreParametrosO != null)
            {
                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                {
                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                }
            }
            lLector = lComando.ExecuteReader();
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                }
            }
            else
            {
                Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }

    }
}