﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_CargaInfOpeUsrFin.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="CargaArchivos.frm_CargaInfOpeUsrFin" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="Archivo Información Operativa Entrega a Usuarios Finales" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label ID="lblArchivo" Text="Archivo" AssociatedControlID="FuArchivo" runat="server" />
                            <asp:FileUpload ID="FuArchivo" CssClass="form-control" runat="server" EnableTheming="true" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--20220705 pasa validaciones--%>
    <div class="modal fade" id="mdlConfirma" tabindex="-1" role="dialog" aria-labelledby="mdlConfirmaLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlConfirmaInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <div class="modal-header" style="background-color: #3E5F8A;">
                            <h5 class="modal-title" id="mdlConfirmaLabel" runat="server" style="color: white;">Confirmación</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff; opacity: 1;">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <%--20220705--%>
                            <asp:HiddenField ID="hdfArchivo" runat="server" />
                        </div>
                        <div class="modal-body" style="background-color: #D3D3D3">
                            <asp:Label ID="lblMensajeConf" runat="server" Font-Size="Large"></asp:Label>
                        </div>
                        <div class="modal-body" style="background-color: #D3D3D3">
                            <asp:Label ID="lblConforma" runat="server" Font-Size="Large">¿Desea ingresar el dato como un valor atípico?<br /><br />
                                Nota: este reporte se rige por lo establecido en la Resolución CREG 080 de 2019 “Por la cual se establecen reglas generales de comportamiento de mercado para los agentes que desarrollen las actividades de los servicios públicos domiciliarios de energía eléctrica y gas combustible
                            </asp:Label>
                        </div>
                        <div class="modal-footer" style="background-color: #3E5F8A">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <asp:Button ID="btnAceptarConf" CssClass="btn btn-primary" Text="Aceptar" OnClick="BtnAceptarConf_Click" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
