﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaPlanoVerificaExt : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            //Master.Titulo = "Registros Operativos";
            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsCadenaArchivo = new StringBuilder();
            var lsRutaArchivo = "";
            var lsRutaArchivoU = "";
            string lsNombre;
            var lsNombreU = "";
            string[] lsErrores = { "", "" };
            var oTransOK = true;
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_archivoUsuarios", "@P_codigo_operador", "@P_ruta_ftp", "@P_codigo_usuario" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.Int };
            object[] lValorParametrosO = { "", "", goInfo.cod_comisionista, strRutaFTP, goInfo.codigo_usuario };

            lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;
            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoContratos", CultureInfo.CurrentCulture)?.ToString());
                oTransOK = false;
            }
            if (FuArchivoUsuarios.FileName != "")
            {
                lsNombreU = DateTime.Now.Millisecond + FuArchivoUsuarios.FileName;
                try
                {
                    lsRutaArchivoU = strRutaCarga + lsNombreU;
                    FuArchivoUsuarios.SaveAs(lsRutaArchivoU);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivoUsuarios", CultureInfo.CurrentCulture)?.ToString());
                    oTransOK = false;
                }
            }
            if (oTransOK)
            {
                try
                {
                    // Realiza las Validaciones de los Archivos
                    if (FuArchivo.FileName != "")
                        lsErrores = ValidarArchivo(lsRutaArchivo);
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoContratosRequerido", CultureInfo.CurrentCulture)?.ToString());
                        return;
                    }
                    if (lsErrores[0] == "")
                    {
                        if (FuArchivoUsuarios.FileName != "")
                            lsErrores = ValidarArchivoUsuarios(lsRutaArchivoU);
                    }
                    if (lsErrores[0] == "")
                    {
                        bool oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);
                        if (FuArchivoUsuarios.FileName != "")
                            oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivoU, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombreU, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]);

                        if (oCargaOK)
                        {
                            lValorParametrosO[0] = lsNombre;
                            lValorParametrosO[1] = lsNombreU;
                            lConexion.Abrir();
                            lComando.Connection = lConexion.gObjConexion;
                            lComando.CommandType = CommandType.StoredProcedure;
                            lComando.CommandText = "pa_ValidaPlanoVerifContExt";
                            lComando.CommandTimeout = 3600;
                            if (lsNombreParametrosO != null)
                            {
                                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                                {
                                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                                }
                            }
                            lLector = lComando.ExecuteReader();
                            if (lLector.HasRows)
                            {
                                while (lLector.Read())
                                {
                                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                                }
                            }
                            else
                            {
                                Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                            }
                            lLector.Close();
                            lLector.Dispose();
                            lConexion.Cerrar();
                        }
                        else
                        {
                            Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                        }
                    }
                    else
                    {
                        lsCadenaArchivo.Append(lsErrores[0]);
                        DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                    }
                    //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                    NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                    LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture)?.ToString() + ex.Message);
                }
            }
        }

        /// <summary>
        /// Validacion del Archivo de Contratos
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            DateTime ldFecha;
            string[] lsFecha;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 18)) // Fuente o campo MP 20160607
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 18 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; // Fuente o campo MP 20160607
                    }
                    else
                    {
                        /// Validar No de Registro
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Registro {" + oArregloLinea.GetValue(0) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Registro {" + oArregloLinea.GetValue(0) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }

                        /// Valida Fecha de Negociacion
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha de Negociacion {" + oArregloLinea.GetValue(1) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(1).ToString());
                                lsFecha = oArregloLinea.GetValue(1).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha de Negociacion {" + oArregloLinea.GetValue(1) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Suscripcion {" + oArregloLinea.GetValue(1) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }

                        /// Valida Punta
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Punta del Contrato {" + oArregloLinea.GetValue(2) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(2).ToString().Trim() != "C" && oArregloLinea.GetValue(2).ToString().Trim() != "V")
                                lsCadenaErrores = lsCadenaErrores + "Valor Invalido en la Punta del Contrato {" + oArregloLinea.GetValue(2) + "},debe ser {C o V}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }

                        /// Valida Tipo de Mercado
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Tipo de Mercado {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(3).ToString().Trim() != "P" && oArregloLinea.GetValue(3).ToString().Trim() != "S")
                                lsCadenaErrores = lsCadenaErrores + "Valor Invalido en Tipo de Mercado {" + oArregloLinea.GetValue(3) + "},debe ser {P o S}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Valida Destino del Contrato
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Destino del Contrato {" + oArregloLinea.GetValue(4) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            if (oArregloLinea.GetValue(4).ToString().Trim() != "G" && oArregloLinea.GetValue(4).ToString().Trim() != "T")
                                lsCadenaErrores = lsCadenaErrores + "Valor Invalido en Tipo de Contrato {" + oArregloLinea.GetValue(4) + "},debe ser {G o T}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Codigo del Operador Contraparte
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Codigo del Operador Contraparte {" + oArregloLinea.GetValue(5) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(5).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Codigo del Operador Contraparte {" + oArregloLinea.GetValue(5) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar No. Contrato
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Contrato {" + oArregloLinea.GetValue(6) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                        /// Valida Fecha de Suscripcion
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha de Suscripcion {" + oArregloLinea.GetValue(7) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(7).ToString());
                                lsFecha = oArregloLinea.GetValue(7).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha de Suscripcion {" + oArregloLinea.GetValue(7) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Suscripcion {" + oArregloLinea.GetValue(7) + "} es inválido, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Codigo Punto de Entega
                        if (oArregloLinea.GetValue(8).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Punto de Entrega / Tramo {" + oArregloLinea.GetValue(8) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(8).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto de Entrega / Tramo {" + oArregloLinea.GetValue(8) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Codigo Modalidad Contractual
                        if (oArregloLinea.GetValue(9).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Modalidad Contractual {" + oArregloLinea.GetValue(9) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(9).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Modalidad Contractual {" + oArregloLinea.GetValue(9) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(10).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad / Capacidad {" + oArregloLinea.GetValue(10) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(10).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad / Capacidad {" + oArregloLinea.GetValue(10) + "} no puder ser 0, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" + oArregloLinea.GetValue(10) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Precio
                        if (oArregloLinea.GetValue(11).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Precio {" + oArregloLinea.GetValue(11) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(11).ToString().Trim());
                                lsFecha = oArregloLinea.GetValue(11).ToString().Trim().Split('.');
                                if (lsFecha.Length > 1)
                                {
                                    if (lsFecha[1].Trim().Length > 2)
                                        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(11) + "}, solo debe tener maximo 2 decimales, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }
                                if (ldValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "El Precio {" + oArregloLinea.GetValue(11) + "} no puder ser 0, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Precio {" + oArregloLinea.GetValue(11) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fecha Inicial
                        if (oArregloLinea.GetValue(12).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha Inicial {" + oArregloLinea.GetValue(12) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(12).ToString());
                                lsFecha = oArregloLinea.GetValue(12).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha Inicial {" + oArregloLinea.GetValue(12) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Inicial {" + oArregloLinea.GetValue(12) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fecha Final
                        if (oArregloLinea.GetValue(13).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Fecha Final {" + oArregloLinea.GetValue(13) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldFecha = Convert.ToDateTime(oArregloLinea.GetValue(13).ToString());
                                lsFecha = oArregloLinea.GetValue(13).ToString().Split('/');
                                if (lsFecha[0].Trim().Length != 4)
                                    lsCadenaErrores = lsCadenaErrores + "Formato Invalido en la Fecha Final {" + oArregloLinea.GetValue(13) + "},debe ser {YYYY/MM/DD}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La Fecha de Final {" + oArregloLinea.GetValue(13) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        ///// Validar Codigo Tipo Demanda
                        //if (oArregloLinea.GetValue(15).ToString().Trim().Length <= 0)
                        //    lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Codigo Tipo Demanda (Si es venta debe ingresar 0) {" + oArregloLinea.GetValue(15).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //else
                        //{
                        //    try
                        //    {
                        //        liValor = Convert.ToInt64(oArregloLinea.GetValue(15).ToString().Trim());
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Codigo Tipo Demanda {" + oArregloLinea.GetValue(15).ToString() + "}, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        //    }
                        //}
                        /// Validar Sentido del Flujo
                        if (oArregloLinea.GetValue(15).ToString().Trim().Length > 0)
                        {
                            if (oArregloLinea.GetValue(15).ToString().Trim() != "NORMAL" && oArregloLinea.GetValue(15).ToString().Trim() != "CONTRA FLUJO")
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Sentido del Flujo, valores válidos {NORMAL o CONTRA FLUJO} {" + oArregloLinea.GetValue(15) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// Validar Presion Punto Terminacion
                        if (oArregloLinea.GetValue(16).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar Presion Punto Terminacion (Valor por defecto 0) {" + oArregloLinea.GetValue(16) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                ldValor = Convert.ToDecimal(oArregloLinea.GetValue(16).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en Presion Punto de Terminacion {" + oArregloLinea.GetValue(16) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Valida Fuente o campo MP 20160607
                        if (oArregloLinea.GetValue(17).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el código de la Fuente {" + oArregloLinea.GetValue(17) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt16(oArregloLinea.GetValue(17).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Fuente {" + oArregloLinea.GetValue(17) + "}, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }

        /// <summary>
        /// Validacion del Archivo de Usuarios Finales
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivoUsuarios(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    /// Campop nuevo Req. 009-17 Indicadores 20170324 
                    /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                    if ((oArregloLinea.Length != 8))
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 8 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>"; //rq009-17
                    }
                    else
                    {
                        /// Validar No de Registro
                        if (oArregloLinea.GetValue(0).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el No. de Registro {" + oArregloLinea.GetValue(0) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(0).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el No. de Registro {" + oArregloLinea.GetValue(0) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        /// Validar Documento Usuario Final
                        if (oArregloLinea.GetValue(1).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el DOcumento de Usuario Final (Valor por defecto 0) {" + oArregloLinea.GetValue(1) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(1).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Documento de Usuario Final {" + oArregloLinea.GetValue(1) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Codigo Mercado Relevante
                        if (oArregloLinea.GetValue(2).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Mercado Relevante {" + oArregloLinea.GetValue(2) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(2).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Mercado Relevante {" + oArregloLinea.GetValue(2) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Codigo Punto de Salida del SNT
                        if (oArregloLinea.GetValue(3).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Punto Salida SNT {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(3).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el Punto Salida SNT {" + oArregloLinea.GetValue(3) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar Cantiad
                        if (oArregloLinea.GetValue(4).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad / Capacidad {" + oArregloLinea.GetValue(4) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(4).ToString().Trim());
                                if (liValor <= 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad / Capacidad {" + oArregloLinea.GetValue(5) + "} no puder ser 0, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad / Capacidad {" + oArregloLinea.GetValue(4) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar tipo de demandan- 20151009
                        if (oArregloLinea.GetValue(5).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el tipo de demanda {" + oArregloLinea.GetValue(5) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(5).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el tipo de demanda{" + oArregloLinea.GetValue(5) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Validar mercado relevante - 20160126
                        if (oArregloLinea.GetValue(6).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar el Mercado Relevante{" + oArregloLinea.GetValue(6) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(6).ToString().Trim());
                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en el mercado relevante {" + oArregloLinea.GetValue(6) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                        /// Campop nuevo Req. 009-17 Indicadores 20170324 
                        /// Cantidad Ekivalente KPCD Solo para mercado primario de Gas
                        if (oArregloLinea.GetValue(7).ToString().Trim().Length <= 0)
                            lsCadenaErrores = lsCadenaErrores + "Debe Ingresar la Cantidad equivalente en KPCD {" + oArregloLinea.GetValue(7) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                        else
                        {
                            try
                            {
                                liValor = Convert.ToInt64(oArregloLinea.GetValue(7).ToString().Trim());
                                if (liValor < 0)
                                    lsCadenaErrores = lsCadenaErrores + "La Cantidad Equivalente Kpcd {" + oArregloLinea.GetValue(7) + "} no puede ser menor a 0, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la Cantidad Equivalente Kpcd {" + oArregloLinea.GetValue(7) + "}, en la Linea No. " + liNumeroLinea + " del Archivo Plano de Usuarios<br>";
                            }
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}