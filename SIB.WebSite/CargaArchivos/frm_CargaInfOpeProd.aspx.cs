﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaInfOpeProd : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                goInfo = (InfoSessionVO)Session["infoSession"];
                if (goInfo == null) Response.Redirect("../index.aspx");
                lConexion = new clConexion(goInfo);
                strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
                strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];
                var lblMensaje = string.Empty;

                //Eventos Botones
                buttons.CargueOnclick += BtnCargar_Click;

                if (!IsPostBack)
                {
                    //Titulo
                    Master.Titulo = "Registros Operativos";
                    //Se inicializan los botones 
                    EnumBotones[] botones = { EnumBotones.Cargue };
                    buttons.Inicializar(botones: botones);
                }

                var liHoraAct = 0;
                var lsHoraMax = "0";
                var liValFin = 0;
                lConexion.Abrir();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_parametros_generales", " 1=1 ");
                if (lLector.HasRows)
                {
                    lLector.Read();
                    lsHoraMax = lLector["hora_max_declara_opera"].ToString();
                }
                lLector.Close();
                lLector.Dispose();

                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 1 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
                if (lLector.HasRows)
                {
                    lblMensaje += "El Operador no está obligado a ingresar la información operativa de energía inyectada.";
                }
                lLector.Close();
                lLector.Dispose();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 6 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
                if (lLector.HasRows)
                {
                    lblMensaje += "El Operador no está obligado a ingresar la información operativa de energía a suministrar.";
                }
                lLector.Close();
                lLector.Dispose();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 3 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
                if (lLector.HasRows)
                {
                    lblMensaje += "El Operador no está obligado a ingresar la información operativa de energía a exportar.";
                }
                lLector.Close();
                lLector.Dispose();
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 2 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
                if (lLector.HasRows)
                {
                    lblMensaje += "El Operador no está obligado a ingresar la información operativa de energía de campo aislado."; //20190306 rq013-19
                }
                lLector.Close();
                lLector.Dispose();
                //20190306 rq013-19
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 11 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
                if (lLector.HasRows)
                {
                    lblMensaje += "El Operador no está obligado a ingresar la información operativa de energía entregada a gasoducto dedicado.";
                }
                lLector.Close();
                lLector.Dispose();
                //20190306 rq013-19
                lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "m_operador_no_ing_inf_ope", " codigo_reporte = 12 and codigo_operador =" + goInfo.cod_comisionista + " and estado ='A' ");
                if (lLector.HasRows)
                {
                    lblMensaje += "El Operador no está obligado a ingresar la información operativa de energía entrega a GNC.";
                }
                lLector.Close();
                lLector.Dispose();
                lConexion.Cerrar();
                if (!string.IsNullOrEmpty(lblMensaje))
                {
                    Toastr.Warning(this, lblMensaje);
                }

                liHoraAct = ((DateTime.Now.Hour * 60) + DateTime.Now.Minute);
                liValFin = ((Convert.ToDateTime(lsHoraMax).Hour * 60) + Convert.ToDateTime(lsHoraMax).Minute);
                if (liHoraAct <= liValFin) return;
                Toastr.Warning(this, "Está intentando Registrar Información Fuera del Horario.!");
                buttons.SwitchOnButton(EnumBotones.Ninguno);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                string lsRutaArchivo;
                string lsNombre;
                string[] lsErrores = { "", "" };
                var lsCadenaArchivo = new StringBuilder();
                var oTransOK = true;
                SqlDataReader lLector;
                var lComando = new SqlCommand();
                var liNumeroParametros = 0;
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_valida" }; //20220705
                SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; //20220705
                Object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP, "S" }; //20220705
                lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lValorParametrosO[1] = goInfo.cod_comisionista;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoInfOpeProd";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        string lsConfirma = "N"; //20220705
                        lblMensajeConf.Text = "";//20220705
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                //20220705
                                if (lLector["ind_confirma"].ToString() == "S")
                                {
                                    lblMensajeConf.Text += lLector["Mensaje"].ToString()+ "<br>";
                                    lsConfirma = "S";
                                }
                                else
                                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                            //20220705
                            if (lsConfirma == "S")
                            {
                                hdfArchivo.Value = lsNombre;
                                Modal.Abrir(this, mdlConfirma.ID, mdlConfirmaInside.ID);
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCargaInfOpe", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            Int64 liValor = 0;
            DateTime ldFecha;
            var lsCadenaErrores = new StringBuilder();
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 6)) //20160809 modalidad tipo de campo //20190306 rq013-19
                    {
                        lsCadenaErrores.Append("El Número de Campos no corresponde con la estructura del Plano { 6 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"); //20160809 modalidad tipo de campo //20190306 rq013-19
                    }
                    //20190306 rq013-19
                    else
                    {
                        /// Validar codigo proceso
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores.Append("El código del proceso {" + oArregloLinea.GetValue(0) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>");
                        }
                        /// Validar punto entrega
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores.Append("El código del punto de entrega {" + oArregloLinea.GetValue(1) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>");
                        }
                        /// Valida Ingreso al SNT
                        //20200701 ajsute auditoria
                        if (oArregloLinea.GetValue(2).ToString() != "S" && oArregloLinea.GetValue(2).ToString() != "N")
                            lsCadenaErrores.Append("El indicador de ingresa al SNT {" + oArregloLinea.GetValue(2).ToString() + "} es inválido, debe ser S/N, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>");

                        /// TIpo de produccion

                        if (oArregloLinea.GetValue(3).ToString() != "N" && oArregloLinea.GetValue(3).ToString() != "I" && oArregloLinea.GetValue(3).ToString() != "0")
                            lsCadenaErrores.Append("El indicador de tipo de producción {" + oArregloLinea.GetValue(3) + "} es inválido, debe ser N/I/0, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>");
                        /// campo de proeuccion
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores.Append("La fuente {" + oArregloLinea.GetValue(4) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>");
                        }
                        /// Valida cantidad suministrada
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores.Append("La cantidad {" + oArregloLinea.GetValue(5) + "} es inválida, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>");
                        }
                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores.ToString();
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores.ToString();

            return lsCadenaRetorno;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 20220705
        protected void BtnAceptarConf_Click(object sender, EventArgs e)
        {
            Modal.Cerrar(this, mdlConfirma.ID);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp", "@P_valida" }; 
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar }; 
            Object[] lValorParametrosO = { hdfArchivo.Value , goInfo.cod_comisionista, strRutaFTP, "N" };
            var lsCadenaArchivo = new StringBuilder();
            lConexion.Abrir();
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lComando.Connection = lConexion.gObjConexion;
            lComando.CommandType = CommandType.StoredProcedure;
            lComando.CommandText = "pa_ValidaPlanoInfOpeProd";
            lComando.CommandTimeout = 3600;
            if (lsNombreParametrosO != null)
            {
                for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                {
                    lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                }
            }
            lLector = lComando.ExecuteReader();
            if (lLector.HasRows)
            {
                while (lLector.Read())
                {
                    lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                }
            }
            else
            {
                Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
            }
            lLector.Close();
            lLector.Dispose();
            lConexion.Cerrar();
        }
    }
}