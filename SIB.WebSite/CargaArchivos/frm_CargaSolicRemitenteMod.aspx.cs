﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_CargaSolicRemitenteMod : Page
    {
        private InfoSessionVO goInfo;
        private clConexion lConexion;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"];
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"];

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Carga Modificación de solicitudes de los remitentes";

            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                string lsRutaArchivo;
                string lsNombre;
                string[] lsErrores = { "", "" };
                var lsCadenaArchivo = new StringBuilder();
                SqlDataReader lLector;
                var lComando = new SqlCommand();
                int liNumeroParametros;
                lConexion = new clConexion(goInfo);
                string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp", "@P_codigo_operador" };
                SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.Int };
                object[] lValorParametrosO = { "", strRutaFTP, goInfo.cod_comisionista };
                lsNombre = DateTime.Now.Millisecond + FuArchivo.FileName;

                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }

                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"] + lsNombre, ConfigurationManager.AppSettings["UserFtp"], ConfigurationManager.AppSettings["PwdFtp"]))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoSolRemitenteMod";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            decimal ldValor = 0;
            var liTotalRegistros = 0;
            long liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 9)) 
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 9 },  en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                    }
                    else
                    {
                        /// Valida codigop tramo
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El código del tramo {" + oArregloLinea.GetValue(0) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                        }
                        /// remitente
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El remitente {" + oArregloLinea.GetValue(1) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>";
                        }
                        /// año
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(2).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El año de la información {" + oArregloLinea.GetValue(2) + "} es inválido, en la Línea No. " + liNumeroLinea + " del Archivo Plano<br>"; 
                        }
                        /// trimestre 
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(3).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El trimestre de la información {" + oArregloLinea.GetValue(3).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>"; 
                        }
                        /// capacidad
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La capacidad solicitada modalidad firme {" + oArregloLinea.GetValue(4).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// capacidad
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La capacidad solicitada modalidad firme trimestral {" + oArregloLinea.GetValue(5).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// capacidad
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(6).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La capacidad solicitada modalidad firmeza condicionada {" + oArregloLinea.GetValue(6).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// capacidad
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(7).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La capacidad solicitada modalidad opcion de compra {" + oArregloLinea.GetValue(7).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// capacidad
                        try
                        {
                            Convert.ToInt32(oArregloLinea.GetValue(8).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La capacidad solicitada modalidad continencia {" + oArregloLinea.GetValue(8).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }

                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}