﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using Segas.Web;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace CargaArchivos
{
    public partial class frm_CargaPlanoPtdvCidvNoReg : Page
    {
        private InfoSessionVO goInfo = null;
        private clConexion lConexion = null;
        private string strRutaCarga;
        private string strRutaFTP;
        private SqlDataReader lLector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            lConexion = new clConexion(goInfo);
            strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"].ToString();
            strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"].ToString();

            //Eventos Botones
            buttons.CargueOnclick += BtnCargar_Click;

            if (IsPostBack) return;
            //Titulo
            /*  Master.Titulo = "Subasta"*/

            //Se inicializan los botones 
            EnumBotones[] botones = { EnumBotones.Cargue };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCargar_Click(object sender, EventArgs e)
        {
            var lsRutaArchivo = "";
            var lsNombre = "";
            string[] lsErrores = { "", "" };
            var lsCadenaArchivo = new StringBuilder();
            var oTransOK = true;
            var oCargaOK = true;
            SqlDataReader lLector;
            var lComando = new SqlCommand();
            var liNumeroParametros = 0;
            lConexion = new clConexion(goInfo);
            string[] lsNombreParametrosO = { "@P_archivo", "@P_ruta_ftp" };
            SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.VarChar };
            object[] lValorParametrosO = { "", strRutaFTP };
            lsNombre = DateTime.Now.Millisecond.ToString() + FuArchivo.FileName.ToString();

            try
            {
                lsRutaArchivo = strRutaCarga + lsNombre;
                FuArchivo.SaveAs(lsRutaArchivo);

                // Realiza las Validaciones de los Archivos
                if (FuArchivo.FileName != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                {
                    Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ArchivoRequerido", CultureInfo.CurrentCulture)?.ToString());
                    return;
                }
                if (lsErrores[0] == "")
                {
                    if (DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"].ToString() + lsNombre, ConfigurationManager.AppSettings["UserFtp"].ToString(), ConfigurationManager.AppSettings["PwdFtp"].ToString()))
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoPtdvCidvNoReg";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                lsCadenaArchivo.Append($"{lLector["Mensaje"]}<br>");
                            }
                        }
                        else
                        {
                            Toastr.Success(this, HttpContext.GetGlobalResourceObject("AppResources", "ExitoCarga", CultureInfo.CurrentCulture)?.ToString());
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorFTP", CultureInfo.CurrentCulture)?.ToString());
                    }
                }
                else
                {
                    lsCadenaArchivo.Append(lsErrores[0]);
                    DelegadaBase.Servicios.registrarProceso(goInfo, HttpContext.GetGlobalResourceObject("AppResources", "ErrorCarga", CultureInfo.CurrentCulture)?.ToString(), "Usuario : " + goInfo.nombre);
                }
                //Se notifica al usuario el éxito de la transacción, y si esta fue negativa se descarga el log de errores
                NotificacionCargaArchivo.NotificacionCargueExitoso(this, lsCadenaArchivo);
                LogCargaArchivo.DownloadBugsLog(this, lsCadenaArchivo);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, HttpContext.GetGlobalResourceObject("AppResources", "ErrorArchivo", CultureInfo.CurrentCulture) + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsRutaArchivo"></param>
        /// <returns></returns>
        public string[] ValidarArchivo(string lsRutaArchivo)
        {
            var liNumeroLinea = 0;
            var liTotalRegistros = 0;
            var liValor = 0;
            var lsCadenaErrores = "";
            string[] lsCadenaRetorno = { "", "" };
            string[] lsDecimal;

            var lLectorArchivo = new StreamReader(lsRutaArchivo);
            try
            {
                /// Recorro el Archivo de Excel para Validarlo
                lLectorArchivo = File.OpenText(lsRutaArchivo);
                while (!lLectorArchivo.EndOfStream)
                {
                    liTotalRegistros = liTotalRegistros + 1;
                    /// Obtiene la fila del Archivo
                    string lsLineaArchivo = lLectorArchivo.ReadLine();
                    //if (lsLineaArchivo.Length > 0)
                    //{
                    liNumeroLinea = liNumeroLinea + 1;
                    /// Pasa la linea sepaada por Comas a un Arreglo
                    Array oArregloLinea = lsLineaArchivo.Split(',');
                    if ((oArregloLinea.Length != 16))//20220118 ajsute ptvf
                    {
                        lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 16 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";//20220118 ajsute ptvf
                    }
                    else
                    {
                        /// Validar Operador
                        if (oArregloLinea.GetValue(0).ToString().Length > 15)
                            lsCadenaErrores = lsCadenaErrores + "Nit del operador {" + oArregloLinea.GetValue(0).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";

                        /// Valida punto SNT
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El punto del SNT {" + oArregloLinea.GetValue(1).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida socio productor
                        if (oArregloLinea.GetValue(2).ToString().Length > 15)
                            lsCadenaErrores = lsCadenaErrores + "El nit del Socio Productor {" + oArregloLinea.GetValue(2).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        /// Valida socio productor
                        if (oArregloLinea.GetValue(3).ToString().Length > 1)
                            lsCadenaErrores = lsCadenaErrores + "La participación del estado {" + oArregloLinea.GetValue(3).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        /// Valida Ano
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(4).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Año {" + oArregloLinea.GetValue(4).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida mes
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(5).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El Mes {" + oArregloLinea.GetValue(5).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida PTDV
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(6).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El PTDV {" + oArregloLinea.GetValue(6).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida PC suminsitro interno
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(7).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de suministro de consumo interno {" + oArregloLinea.GetValue(7).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida PC exportaciones
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(8).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de exportaciones {" + oArregloLinea.GetValue(8).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida PC refineria barrancabermeja
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(9).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de Barrancabermeja {" + oArregloLinea.GetValue(9).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida PC refineria cartagena
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(10).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción comprometida de Cartagena {" + oArregloLinea.GetValue(10).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida Producción portencial del campo
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(11).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "La producción potencial del campo {" + oArregloLinea.GetValue(11).ToString() + "} es inválida, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida gas en oepración
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(12).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El gas en operación {" + oArregloLinea.GetValue(12).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida CIDV
                        try
                        {
                            liValor = Convert.ToInt32(oArregloLinea.GetValue(13).ToString());
                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El CIDV {" + oArregloLinea.GetValue(13).ToString() + "} es inválido, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// Valida poder calorifico
                        try
                        {
                            Convert.ToDecimal(oArregloLinea.GetValue(14).ToString());
                            lsDecimal = oArregloLinea.GetValue(14).ToString().Split('.');
                            if (lsDecimal.Length > 1 && lsDecimal[1].Length > 3)
                                lsCadenaErrores = lsCadenaErrores + "El Poder calorífico {" + oArregloLinea.GetValue(14).ToString() + "} debe tener máximo 3 decimales, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";

                        }
                        catch (Exception ex)
                        {
                            lsCadenaErrores = lsCadenaErrores + "El poder calorífico {" + oArregloLinea.GetValue(14).ToString() + "} es inválido, en la Linea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                        }
                        /// 20220118 ajuste ptdv
                        if (oArregloLinea.GetValue(15).ToString().Length > 0)
                        {
                            try
                            {
                                Convert.ToDateTime(oArregloLinea.GetValue(15).ToString());
                                if (oArregloLinea.GetValue(15).ToString().Length != 10)
                                    lsCadenaErrores = lsCadenaErrores + "La fecha de resolución {" + oArregloLinea.GetValue(15) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                else
                                {
                                    string[] lsFecha = oArregloLinea.GetValue(15).ToString().Split('/');
                                    if (lsFecha[0].Length != 4)
                                        lsCadenaErrores = lsCadenaErrores + "La fecha de resolución {" + oArregloLinea.GetValue(15) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                                }

                            }
                            catch (Exception ex)
                            {
                                lsCadenaErrores = lsCadenaErrores + "La fecha de resolución {" + oArregloLinea.GetValue(15) + "} es inválida, en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";
                            }
                        }
                        else
                            lsCadenaErrores = lsCadenaErrores + "Debe ingresar la fecha de resolución en la Linea No. " + liNumeroLinea + " del Archivo Plano<br>";

                    }
                }
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
            }
            catch (Exception ex)
            {
                lLectorArchivo.Close();
                lLectorArchivo.Dispose();
                lsCadenaRetorno[0] = lsCadenaErrores;
                lsCadenaRetorno[1] = "0";
                return lsCadenaRetorno;
            }
            lsCadenaRetorno[1] = liTotalRegistros.ToString();
            lsCadenaRetorno[0] = lsCadenaErrores;

            return lsCadenaRetorno;
        }
    }
}