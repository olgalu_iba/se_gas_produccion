﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;
using System.IO;

public partial class CargaArchivos_frm_CargaTrmContrato : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    clConexion lConexion = null;
    String strRutaCarga;
    String strRutaFTP;
    SqlDataReader lLector;


    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        lConexion = new clConexion(goInfo);
        strRutaCarga = ConfigurationManager.AppSettings["rutaCargaPlano"].ToString();
        strRutaFTP = ConfigurationManager.AppSettings["RutaFtp"].ToString();
    }
    protected void BtnCargar_Click(object sender, EventArgs e)
    {
        ltCargaArchivo.Text = "";
        string lsRutaArchivo = "";
        string lsNombre = "";
        string[] lsErrores = { "", "" };
        string lsCadenaArchivo = "";
        bool oTransOK = true;
        bool oCargaOK = false;
        SqlDataReader lLector;
        SqlCommand lComando = new SqlCommand();
        Int32 liNumeroParametros = 0;
        lConexion = new clConexion(goInfo);
        string[] lsNombreParametrosO = { "@P_archivo", "@P_codigo_operador", "@P_ruta_ftp" };
        SqlDbType[] lTipoparametrosO = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.VarChar };
        Object[] lValorParametrosO = { "", goInfo.cod_comisionista, strRutaFTP };

        lsNombre = DateTime.Now.Millisecond.ToString() + FuArchivo.FileName.ToString();
        try
        {
            lsRutaArchivo = strRutaCarga + lsNombre;
            FuArchivo.SaveAs(lsRutaArchivo);
        }
        catch (Exception ex)
        {
            ltCargaArchivo.Text = "Ocurrio un Problema al Cargar el Archivo. " + ex.Message;
            oTransOK = false;
        }
        /// Realiza las Validaciones de los Archivos
        if (oTransOK)
        {
            try
            {
                //trMensaje.Visible = true;
                lsCadenaArchivo = lsCadenaArchivo + "<TR><TD colspan='2'> <font color='#FF0000' size='-1'>" + "***LOG ARCHIVO DE CARGA *** </FONT> </TD></TR>";
                if (FuArchivo.FileName.ToString() != "")
                    lsErrores = ValidarArchivo(lsRutaArchivo);
                else
                    lsErrores[0] += "Debe seleccionar el archivo<br>";
                if (lsErrores[0] == "")
                {
                    oCargaOK = DelegadaBase.Servicios.put_archivo(lsRutaArchivo, ConfigurationManager.AppSettings["ServidorFtp"].ToString() + lsNombre, ConfigurationManager.AppSettings["UserFtp"].ToString(), ConfigurationManager.AppSettings["PwdFtp"].ToString());

                    if (oCargaOK)
                    {
                        lValorParametrosO[0] = lsNombre;
                        lConexion.Abrir();
                        lComando.Connection = lConexion.gObjConexion;
                        lComando.CommandType = CommandType.StoredProcedure;
                        lComando.CommandText = "pa_ValidaPlanoTrmContrato";
                        lComando.CommandTimeout = 3600;
                        if (lsNombreParametrosO != null)
                        {
                            for (liNumeroParametros = 0; liNumeroParametros <= lsNombreParametrosO.Length - 1; liNumeroParametros++)
                            {
                                lComando.Parameters.Add(lsNombreParametrosO[liNumeroParametros], lTipoparametrosO[liNumeroParametros]).Value = lValorParametrosO[liNumeroParametros];
                            }
                        }
                        lLector = lComando.ExecuteReader();
                        if (lLector.HasRows)
                        {
                            while (lLector.Read())
                            {
                                ltCargaArchivo.Text = ltCargaArchivo.Text + lLector["Mensaje"].ToString() + "<br>";
                            }
                        }
                        else
                        {
                            ltCargaArchivo.Text = "Archivo Cargado Satisfactoriamente.!";
                        }
                        lLector.Close();
                        lLector.Dispose();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        ltCargaArchivo.Text = "Se presento un Problema en el {FTP} del Archivo al Servidor.!";
                    }
                }
                else
                {
                    lsCadenaArchivo = lsCadenaArchivo + "<TR><TD colspan='2'> <font color='#FF0000' size='-1'>" + lsErrores[0] + "</FONT> </TD></TR>";
                    ltCargaArchivo.Text = ltCargaArchivo.Text + lsCadenaArchivo;
                    DelegadaBase.Servicios.registrarProceso(goInfo, "Finalizó la Carga con Errores", "Usuario : " + goInfo.nombre);
                }
            }
            catch (Exception ex)
            {
                ltCargaArchivo.Text = "Se presento un Problema Al cargar los datos del archivo plano.! <br>" + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Validacion del Archivo de Contratos
    /// </summary>
    /// <param name="lsRutaArchivo"></param>
    /// <returns></returns>
    public string[] ValidarArchivo(string lsRutaArchivo)
    {
        Int32 liNumeroLinea = 0;
        decimal ldValor = 0;
        int liTotalRegistros = 0;
        Int64 liValor = 0;
        string[] lsFecha;
        string[] lsPrecio;
        string lsCadenaErrores = "";
        string[] lsCadenaRetorno = { "", "" };

        StreamReader lLectorArchivo = new StreamReader(lsRutaArchivo);
        try
        {
            /// Recorro el Archivo de Excel para Validarlo
            lLectorArchivo = File.OpenText(lsRutaArchivo);
            while (!lLectorArchivo.EndOfStream)
            {
                liTotalRegistros = liTotalRegistros + 1;
                /// Obtiene la fila del Archivo
                string lsLineaArchivo = lLectorArchivo.ReadLine();
                //if (lsLineaArchivo.Length > 0)
                //{
                liNumeroLinea = liNumeroLinea + 1;
                /// Pasa la linea sepaada por Comas a un Arreglo
                Array oArregloLinea = lsLineaArchivo.Split(',');
                if ((oArregloLinea.Length != 4))
                {
                    lsCadenaErrores = lsCadenaErrores + "El Número de Campos no corresponde con la estructura del Plano { 4 },  en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<BR>";
                }
                else
                {
                    //contato
                    try
                    {
                        liValor = Convert.ToInt32(oArregloLinea.GetValue(0).ToString());
                    }
                    catch (Exception ex)
                    {
                        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en número de operación {" + oArregloLinea.GetValue(0).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                    /// tipo TRM
                    try
                    {
                        liValor = Convert.ToInt32(oArregloLinea.GetValue(1).ToString());
                    }
                    catch (Exception ex)
                    {
                        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en código del tipo de tasa de cambio {" + oArregloLinea.GetValue(1).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    }
                    /// moneda
                    if (oArregloLinea.GetValue(2).ToString() != "1" && oArregloLinea.GetValue(2).ToString() != "2")
                        lsCadenaErrores = lsCadenaErrores + "Valor Inválido en la moneda {" + oArregloLinea.GetValue(2).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                    /// observacion
                    if (oArregloLinea.GetValue(3).ToString().Length > 200)
                        lsCadenaErrores = lsCadenaErrores + "Valor demasiado extenso para las observaciones.{" + oArregloLinea.GetValue(3).ToString() + "}, en la Línea No. " + liNumeroLinea.ToString() + " del Archivo Plano<br>";
                }
            }
            lLectorArchivo.Close();
            lLectorArchivo.Dispose();
        }
        catch (Exception ex)
        {
            lLectorArchivo.Close();
            lLectorArchivo.Dispose();
            lsCadenaRetorno[0] = lsCadenaErrores;
            lsCadenaRetorno[1] = "0";
            return lsCadenaRetorno;
        }
        lsCadenaRetorno[1] = liTotalRegistros.ToString();
        lsCadenaRetorno[0] = lsCadenaErrores;

        return lsCadenaRetorno;
    }

}