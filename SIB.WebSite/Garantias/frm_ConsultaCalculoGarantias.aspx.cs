﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Garantias
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_ConsultaCalculoGarantias : Page
    {
        private InfoSessionVO goInfo;
        private static string lsTitulo = "Consulta Cálculo de Garantías"; //--20170126 rq122
        private clConexion lConexion;
        private SqlDataReader lLector;
        private DataSet lds = new DataSet();
        private SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += imbConsultar_Click;

            if (IsPostBack) return;
            //Titulo
            Master.Titulo = "Garantías";
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by razon_social", 2, 4);
            lConexion.Cerrar();

            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lProcedimiento"></param>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            ListItem lItem = new ListItem();
            lItem.Value = "0";
            lItem.Text = "Seleccione";
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                ListItem lItem1 = new ListItem();
                lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
                lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString();
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: lkbConsultar_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imbConsultar_Click(object sender, EventArgs e)
        {
            bool Error = false;
            DateTime ldFecha;
            string lsFecha = "";
            if (TxtPeriodo.Text.Trim().Length > 0)
            {
                try
                {
                    ldFecha = Convert.ToDateTime(TxtPeriodo.Text);
                    string anio = ldFecha.Year.ToString();
                    string mes = ldFecha.Month.ToString();

                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Formato Inválido en el Campo Periodo. <br>");
                    Error = true;
                }
            }
            if (TxtPeriodo.Text.Trim().Length <= 0 && ddlOperador.SelectedValue == "0")
            {
                Toastr.Warning(this, "Debe Ingresar algún Parámetro de búsqueda. <br>");
                Error = true;
            }
            if (!Error)
            {
                ldFecha = Convert.ToDateTime(TxtPeriodo.Text);
                string anio = ldFecha.Year.ToString();
                string mes = ldFecha.Month.ToString();
                string fecha = anio + "/" + mes + "/" + 01;

                try
                {
                    if (TxtPeriodo.Text.Trim().Length > 0)
                        //lsFecha = TxtPeriodo.Text.Substring(0, 4) + "/" + TxtPeriodo.Text.Substring(4, 2) + "/01";

                        lConexion = new clConexion(goInfo);
                    lConexion.Abrir();
                    SqlCommand lComando = new SqlCommand();
                    lComando.Connection = lConexion.gObjConexion;
                    lComando.CommandTimeout = 3600;
                    lComando.CommandType = CommandType.StoredProcedure;
                    lComando.CommandText = "pa_GetConsCalculoGarantias";
                    lComando.Parameters.Add("@P_fecha_garantia", SqlDbType.VarChar).Value = fecha.Trim();
                    lComando.Parameters.Add("@P_consitucion", SqlDbType.VarChar).Value = ddlConstitucion.SelectedValue;
                    lComando.Parameters.Add("@P_operador", SqlDbType.VarChar).Value = ddlOperador.SelectedValue;
                    lComando.ExecuteNonQuery();
                    lsqldata.SelectCommand = lComando;
                    lsqldata.Fill(lds);
                    dtgMaestro.DataSource = lds;
                    dtgMaestro.DataBind();

                    buttons.SwitchOnButton(EnumBotones.Buscar, EnumBotones.Excel);
                    lConexion.Cerrar();
                    if (dtgMaestro.Items.Count <= 0)
                    {
                        Toastr.Warning(this, "No Hay Registros para los criterios de búsqueda.!");
                        Error = true;

                    }
                    else
                    {
                        //imbExcel.Visible = true;
                        foreach (DataGridItem Grilla in dtgMaestro.Items)
                        {
                            if (Grilla.Cells[5].Text.Trim() != "0.00")
                                Grilla.Cells[7].Enabled = true;
                            else
                                Grilla.Cells[7].Enabled = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString());
                    Error = true;
                }
            }
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            string lsNombreArchivo = goInfo.Usuario.ToString() + "InfCalGarant" + DateTime.Now + ".xls";
            string lstitulo_informe = "Informe Cálculo Garantías"; //--20170126 rq122
            string lsTituloParametros = "";
            try
            {
                lsTituloParametros += "Periodo : " + TxtPeriodo.Text.Trim();
                if (ddlConstitucion.SelectedValue != "")
                    lsTituloParametros += " - Consitución: " + ddlConstitucion.SelectedItem.ToString(); //--20170126 rq122

                decimal ldCapacidad = 0;
                StringBuilder lsb = new StringBuilder();
                ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                StringWriter lsw = new StringWriter(lsb);
                HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                Page lpagina = new Page();
                HtmlForm lform = new HtmlForm();
                dtgMaestro.EnableViewState = false;
                lpagina.EnableEventValidation = false;
                lpagina.DesignerInitialize();
                lpagina.Controls.Add(lform);

                lform.Controls.Add(dtgMaestro);
                lpagina.RenderControl(lhtw);
                Response.Clear();

                Response.Buffer = true;
                Response.ContentType = "aplication/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                Response.ContentEncoding = Encoding.Default;
                Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                Response.Charset = "UTF-8";
                Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                Response.Write("<table><tr><th colspan='4' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                Response.ContentEncoding = Encoding.Default;
                Response.Write(lsb.ToString());
                Response.End();
                lds.Dispose();
                lsqldata.Dispose();
                lConexion.CerrarInforme();
            }
            catch (Exception ex)
            {

                Toastr.Warning(this, "No se Pudo Generar el Excel.!" + ex.Message.ToString());

            }
        }

        /// <summary>
        /// Nombre: dtgMaestro_EditCommand
        /// Fecha: Agosto 10 de 2008
        /// Creador: Olga Lucia Ibanez
        /// Descripcion: Metodo para activasr la pantalla de Constitucion
        /// Modificacion:
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dtgMaestro_EditCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                // Carga informacion de combos
                lConexion.Abrir();
                ddlTipoMvto.Items.Clear();
                ddlTipoGarantia.Items.Clear();
                LlenarControles(lConexion.gObjConexion, ddlTipoMvto, "m_tipos_movimiento", " estado = 'A'  And indicador_afectacion = 'E' order by descripcion", 0, 1);
                LlenarControles(lConexion.gObjConexion, ddlTipoGarantia, "m_tipos_garantia", " estado = 'A' order by descripcion", 0, 1);
                lConexion.Cerrar();

                lblTitulo.Text = "CONSTITUCIÓN GARANTÍAS"; //--20170126 rq122
                lblOperador.Text = e.Item.Cells[1].Text + "-" + e.Item.Cells[2].Text;
                hdfNoDocumento.Value = e.Item.Cells[1].Text;
                hdfValorGarantia.Value = e.Item.Cells[5].Text.Replace(",", "");
                lblValorGarantia.Text = e.Item.Cells[5].Text;
                lblFechaGarantia.Text = e.Item.Cells[0].Text;
                hdfFechaGarantia.Value = e.Item.Cells[0].Text;
                //Abre el modal de Agregar
                Modal.Abrir(this, mdlRegistro.ID, mdlRegistroInside.ID);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Problemas en la recuperacion de la Información. " + ex.Message.ToString());

            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImbCrear_Click(object sender, EventArgs e)
        {
            bool Error = false;
            string[] lsNombreParametros = { "@P_fecha_movimiento", "@P_codigo_tipo_movimiento", "@P_codigo_tipo_garantia", "@P_no_documento",
                "@P_fecha_garantia", "@P_numero_garantia","@P_fecha_vencimiento_gar","@P_valor_garantia","@P_valor_movimiento",
                "@P_detalle_movimiento","@P_reutiliza"};
            SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.Int, SqlDbType.Int, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar,
                SqlDbType.VarChar, SqlDbType.Decimal,SqlDbType.Decimal, SqlDbType.VarChar, SqlDbType.VarChar };
            string[] lValorParametros = { "", "0", "0", "", "", "", "", "0", "0", "", ddlReutiliza.SelectedValue };
            var lblMensaje = new StringBuilder();
            DateTime ldFecha;
            decimal ldValor;
            decimal ldSaldoGar = 0;
            string lsFecha = DateTime.Now.ToShortDateString().Substring(6, 4) + "/" + DateTime.Now.ToShortDateString().Substring(3, 2) + "/" + DateTime.Now.ToShortDateString().Substring(0, 2);

            try
            {
                SqlTransaction oTransaccion;
                lConexion.Abrir();

                try
                {
                    ldFecha = Convert.ToDateTime(TxtFechaMvto.Text.Trim());
                    if (ldFecha < Convert.ToDateTime(lsFecha))
                    {
                        Toastr.Warning(this, "La Fecha de Movimiento NO puede ser menor que la fecha del Sistema.<br>");
                        Error = true;
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Valor Inválido en Fecha Movimiento.<br>");
                    Error = true;
                }
                if (ddlTipoMvto.SelectedValue == "0")
                {
                    Toastr.Warning(this, "Debe seleccionar el Tipo de Movimiento.<br>");
                    Error = true;

                }
                if (ddlTipoGarantia.SelectedValue == "0")
                {
                    Toastr.Warning(this, "Debe seleccionar el Tipo de Garantía.<br>");
                    Error = true;
                }
                if (ddlReutiliza.SelectedValue == "N")
                {
                    if (DelegadaBase.Servicios.ValidarExistencia("t_cuenta_garantia", " numero_garantia= '" + TxtNoGarantia.Text.Trim() + "' and no_documento = '" + hdfNoDocumento.Value + "' ", goInfo))
                    {
                        Toastr.Warning(this, " El No. de la Garantía YA está Ingresado para el Participante.<br>");
                        Error = true;

                    }
                    if (TxtValorGarantia.Text.Trim().Length <= 0)
                    {
                        Toastr.Warning(this, "Debe Ingresar el Valor de la Garantía.<br>");
                        Error = true;
                    }
                    else
                    {
                        try
                        {
                            ldValor = Convert.ToDecimal(TxtValorGarantia.Text.Trim());
                        }
                        catch (Exception ex)
                        {
                            Toastr.Warning(this, "Valor Inválido en Valor de la Garantía.<br>");
                            Error = true;

                        }
                    }
                }
                else
                {
                    TxtValorGarantia.Text = "0";
                    lLector = DelegadaBase.Servicios.LlenarControl(lConexion.gObjConexion, "pa_ValidarExistencia", "t_cuenta_garantia", " numero_garantia= '" + TxtNoGarantia.Text.Trim() + "' and no_documento = '" + hdfNoDocumento.Value + "' ");
                    if (lLector.HasRows)
                    {
                        lLector.Read();
                        ldSaldoGar = Convert.ToDecimal(lLector["valor_saldo"].ToString());
                    }
                    else
                    {
                        Toastr.Warning(this, " El No. de la Garantía NO está Ingresada para el Participante.<br>");
                        Error = true;
                    }
                    lLector.Close();
                    lLector.Dispose();
                }
                if (TxtFechaVenc.Text.Trim().Length > 0)
                {
                    try
                    {
                        ldFecha = Convert.ToDateTime(TxtFechaVenc.Text.Trim());
                        if (ldFecha <= Convert.ToDateTime(TxtFechaMvto.Text.Trim()))
                        {
                            Toastr.Warning(this, "La Fecha de Vencimiento de la Garantía NO puede ser menor o Igual que la fecha de Movimiento.<br>");
                            Error = true;
                        }
                        if (ldFecha <= Convert.ToDateTime(lsFecha))
                        {
                            Toastr.Warning(this, "La Fecha de Vencimiento de la Garantía NO puede ser menor o Igual que la fecha del Sistema.<br>");
                            Error = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Toastr.Warning(this, "Valor Inválido en Fecha Vencimiento de la Garantía.<br>");
                        Error = true;
                    }
                }
                try
                {
                    ldValor = Convert.ToDecimal(TxtValorMvto.Text.Trim());
                    if (ldValor > Convert.ToDecimal(hdfValorGarantia.Value))
                    {
                        Toastr.Warning(this, "El Valor del Movimiento NO puede ser mayor al Valor del Saldo por Constituir.<br>");
                        Error = true;

                    }
                    if (ddlReutiliza.SelectedValue == "N")
                    {
                        if (ldValor > Convert.ToDecimal(TxtValorGarantia.Text))
                        {
                            Toastr.Warning(this, "El Valor del Movimiento NO puede ser mayor al Valor de la Garantía.<br>");
                            Error = true;
                        }
                    }
                    else
                    {
                        if (ldValor > ldSaldoGar)
                        {
                            Toastr.Warning(this, "El Valor del Movimiento NO puede ser mayor al Saldo {" + ldSaldoGar.ToString("###,###,###,###,##0.00") + "} de la Garantía.<br>", "Warning!", 50000);
                            Error = true;

                        }
                    }
                }
                catch (Exception ex)
                {
                    Toastr.Warning(this, "Valor Inválido en Valor del Movimiento.<br>");
                    Error = true;

                }
                if (!Error)
                {
                    lValorParametros[0] = TxtFechaMvto.Text.Trim();
                    lValorParametros[1] = ddlTipoMvto.SelectedValue;
                    lValorParametros[2] = ddlTipoGarantia.SelectedValue;
                    lValorParametros[3] = hdfNoDocumento.Value;
                    lValorParametros[4] = hdfFechaGarantia.Value;
                    lValorParametros[5] = TxtNoGarantia.Text.Trim();
                    lValorParametros[6] = TxtFechaVenc.Text.Trim();
                    lValorParametros[7] = TxtValorGarantia.Text.Trim();
                    lValorParametros[8] = TxtValorMvto.Text.Trim();
                    lValorParametros[9] = TxtFechaMvto.Text.Trim();
                    oTransaccion = lConexion.gObjConexion.BeginTransaction(IsolationLevel.Serializable);
                    goInfo.mensaje_error = "";
                    lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatosConTransaccion(lConexion.gObjConexion, "pa_SetMvtoGarantia", lsNombreParametros, lTipoparametros, lValorParametros, oTransaccion, goInfo);
                    if (goInfo.mensaje_error != "")
                    {
                        Toastr.Warning(this, "Se presentó un Problema en la Creación del Movimiento.! " + goInfo.mensaje_error.ToString());
                        oTransaccion.Rollback();
                        lConexion.Cerrar();
                    }
                    else
                    {
                        string lsNoMvto = "";
                        lLector.Read();
                        lsNoMvto = lLector["NoMvto"].ToString();
                        lLector.Close();
                        lLector.Dispose();
                        oTransaccion.Commit();
                        lConexion.Cerrar();
                        TxtFechaMvto.Text = "";
                        ddlTipoMvto.SelectedValue = "0";
                        ddlTipoGarantia.SelectedValue = "0";
                        TxtNoGarantia.Text = "";
                        TxtFechaVenc.Text = "";
                        TxtValorGarantia.Text = "";
                        TxtValorMvto.Text = "";
                        TxtDetalleMvto.Text = "";
                        Toastr.Success(this, "Movimiento Creado Correctamente con No. " + lsNoMvto + ".!");

                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + lblMensaje.Text + "');", true);
                        //tblDatos.Visible = true;
                        //Cerrar el modal de Agregar
                        Modal.Cerrar(this, mdlRegistro.ID);
                        imbConsultar_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImbRegresar_Click(object sender, EventArgs e)
        {
            //Cerrar el modal de Agregar
            Modal.Cerrar(this, mdlRegistro.ID);
            lblTitulo.Text = lsTitulo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlReutiliza_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReutiliza.SelectedValue == "S")
            {
                lblFechaVenc.Visible = false;
                TxtFechaVenc.Visible = false;
                lblValorGarantia.Visible = false;
                TxtValorGarantia.Visible = false;
            }
            else
            {
                lblFechaVenc.Visible = true;
                TxtFechaVenc.Visible = true;
                lblValorGarantia.Visible = true;
                TxtValorGarantia.Visible = true;
            }
        }
    }
}