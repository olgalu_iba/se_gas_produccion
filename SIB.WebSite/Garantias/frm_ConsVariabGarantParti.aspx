﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsVariabGarantParti.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Garantias.frm_ConsVariabGarantParti" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Punta" AssociatedControlID="ddlPunta" runat="server" />
                            <asp:DropDownList ID="ddlPunta" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="C" Text="Compra"></asp:ListItem>
                                <asp:ListItem Value="V" Text="Venta"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Fecha Rueda" AssociatedControlID="ddlFechaRueda" runat="server" />
                            <asp:DropDownList ID="ddlFechaRueda" runat="server" CssClass="form-control" />
                            <asp:HiddenField ID="hdfExcel" runat="server" />
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Tipo de Garantía" AssociatedControlID="ddlTipoGarantia" runat="server" />
                            <asp:DropDownList ID="ddlTipoGarantia" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="I" Text="Participación Inicial"></asp:ListItem>
                                <asp:ListItem Value="A" Text="Participación Ajustada"></asp:ListItem>
                                <asp:ListItem Value="A" Text="Participación Ajustada"></asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group" runat="server">
                            <asp:Label Text="Punto de Entrega" AssociatedControlID="ddlPuntoEnt" runat="server" />
                            <asp:DropDownList ID="ddlPuntoEnt" runat="server" Width="100%" CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div runat="server" id="tblGrilla" visible="false">
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgConsulta" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center" Width="100%" CssClass="table-bordered">
                            <Columns>
                                <%--0--%><asp:BoundColumn DataField="codigo_operador" HeaderText="Código Operador" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--1--%><asp:BoundColumn DataField="nombre_operador" HeaderText="Nombre Operador" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <%--2--%><asp:BoundColumn DataField="fecha_rueda" HeaderText="Fecha Rueda" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                                <%--3--%><asp:BoundColumn DataField="punta" HeaderText="Punta" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--4--%><asp:BoundColumn DataField="tipo_garantia" HeaderText="Tipo Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--5--%><asp:BoundColumn DataField="codigo_punto_entrega" HeaderText="Código Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--6--%><asp:BoundColumn DataField="punto_entrega" HeaderText="Punto Entrega" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--7--%><asp:BoundColumn DataField="constante_1" HeaderText="Constante 1" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--8--%><asp:BoundColumn DataField="constante_2" HeaderText="Constante 2" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <%--9--%><asp:BoundColumn DataField="precio" HeaderText="Precio" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--10--%><asp:BoundColumn DataField="valor_trm" HeaderText="Trm" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--11--%><asp:BoundColumn DataField="valor_demanda_c" HeaderText="Demanda C" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--12--%><asp:BoundColumn DataField="valor_demanda_c_p" HeaderText="Demanda C'" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--13--%><asp:BoundColumn DataField="valor_oferta_c" HeaderText="Oferta V" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--2016111--%>
                                <%--14--%><asp:BoundColumn DataField="valor_oferta_c_p" HeaderText="Oferta V'" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <%--2016111--%>
                                <%--15--%><asp:BoundColumn DataField="valor_participacion_inicial" HeaderText="Valor Participación Inicial" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                                <%--16--%><asp:BoundColumn DataField="valor_participacion_ajustada" HeaderText="Valor Participación Ajustada" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="150px"></asp:BoundColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
