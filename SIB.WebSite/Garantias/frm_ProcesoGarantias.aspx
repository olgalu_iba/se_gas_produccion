﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ProcesoGarantias.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Garantias_frm_ProcesoGarantias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
        <div class="kt-container kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <%--Head--%>
                <div class="kt-portlet__head">

                    <%--Titulo--%>
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <asp:Label ID="lblTitulo" runat="server" />

                        </h3>
                    </div>
                    <segas:CrudButton ID="buttons" runat="server" />
                </div>

                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="Name">Fecha Proceso</label> 
                                <asp:TextBox ID="TxtFechaIni" runat="server" ValidationGroup="detalle" Width="100%" CssClass="form-control"  ClientIDMode="Static"></asp:TextBox>
                            </div>
                        </div>

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                        <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>

                    </div>
                </div>
            </div>
        </div>
      
</asp:Content>
