﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsultaCalculoGarantiasCop.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master"
    Inherits="Garantias_frm_ConsultaCalculoGarantiasCop" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">

                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />

                    </h3>
                </div>

                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />

            </div>

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Periodo</label>
                            <asp:TextBox ID="TxtPeriodo" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control monthDatepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:DropDownList ID="ddlOperador" runat="server" Width="100%" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Garantias Consituidas</label>
                            <asp:DropDownList ID="ddlConstitucion" runat="server" Width="100%" CssClass="form-control">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Rueda</label>
                            <asp:TextBox ID="TxtFechaIni" runat="server" Width="100%" ValidationGroup="detalle" CssClass="form-control datepicker" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div id="tdMes01" class="form-group" runat="server">
                            <label for="Name">Número Rueda</label>
                            <asp:TextBox ID="TxtNoRueda" runat="server" Width="100%" CssClass="form-control" ValidationGroup="detalle"></asp:TextBox>

                        </div>
                    </div>
                </div>
                <table border="0" align="center" cellpadding="3" cellspacing="2" width="80%" runat="server"
                    id="tblMensaje">
                    <tr>
                        <td colspan="3" align="center">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="detalle" />
                            <asp:Label ID="lblMensaje" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div runat="server"
                    id="tblGrilla" visible="false">

                    <div class="table table-responsive" style="overflow: scroll; height: 450px;">
                        <asp:DataGrid ID="dtgMaestro" runat="server" Width="100%" CssClass="table-bordered" AutoGenerateColumns="False" AllowPaging="false"
                            PagerStyle-HorizontalAlign="Center" OnEditCommand="dtgMaestro_EditCommand">

                            <Columns>
                                <asp:BoundColumn DataField="fecha_garantia" HeaderText="Fecha Garantía" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="no_documento" HeaderText="No. Identificacion" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="120px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="nombre_participante" HeaderText="Nombre Participante"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_garantia" HeaderText="Valor Garantía" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_constitucion" HeaderText="Valor Constitución" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="valor_saldo" HeaderText="Valor Saldo" ItemStyle-HorizontalAlign="Right"
                                    DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fecha_calculo" HeaderText="Fecha Proceso" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="130px"></asp:BoundColumn>
                                <asp:EditCommandColumn HeaderText="Constituir" EditText="Constituir"></asp:EditCommandColumn>
                                <asp:EditCommandColumn HeaderText="Liberar" EditText="Liberar"></asp:EditCommandColumn>
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="tblRegConst" runat="server" border="0" align="center" cellpadding="3"
        cellspacing="2" visible="false" class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">

            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Operador</label>
                            <asp:Label ID="lblOperador" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                            <asp:HiddenField ID="hdfNoDocumento" runat="server" />
                            <asp:HiddenField ID="hdfValorGarantia" runat="server" />
                            <asp:HiddenField ID="hdfFechaGarantia" runat="server" />
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Garantía</label>
                            <asp:Label ID="lblFechaGarantia" runat="server" ForeColor="Red" Font-Bold="true"
                                Width="150px"></asp:Label>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Saldo x Constituir</label>
                            <asp:Label ID="lblValorGarantia" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Movimiento {YYYY/MM/DD}</label>
                            <asp:TextBox ID="TxtFechaMvto" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtFechaMvto" runat="server" ErrorMessage="Debe Ingresar la Fecha del Movimiento"
                                ControlToValidate="TxtFechaMvto" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo Movimiento</label>
                            <asp:DropDownList ID="ddlTipoMvto" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Tipo Garantía</label>
                            <asp:DropDownList ID="ddlTipoGarantia" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Detalle Movimiento</label>
                            <asp:TextBox ID="TxtDetalleMvto" runat="server" ValidationGroup="detalle" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtDetalleMvto" runat="server" ErrorMessage="Debe Ingresar el Detalle del Movimiento"
                                ControlToValidate="TxtDetalleMvto" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Reutiliza Garantia</label>
                            <asp:DropDownList ID="ddlReutiliza" runat="server" OnSelectedIndexChanged="ddlReutiliza_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Número Garantia</label>
                            <asp:TextBox ID="TxtNoGarantia" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtNoGarantia" runat="server" ErrorMessage="Debe Ingresar el Número de la Garantia"
                                ControlToValidate="TxtNoGarantia" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Valor Movimiento</label>
                            <asp:TextBox ID="TxtValorMvto" runat="server" ValidationGroup="detalle" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RfvTxtValorMvto" runat="server" ErrorMessage="Debe Ingresar el Valor del Movimiento"
                                ControlToValidate="TxtValorMvto" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Fecha Vencimiento {YYYY/MM/DD}</label>
                            <asp:TextBox ID="TxtFechaVenc" runat="server" ValidationGroup="detalle" Width="100px"></asp:TextBox>

                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="Name">Valor Garantia</label>
                            <asp:TextBox ID="TxtValorGarantia" runat="server" ValidationGroup="detalle" Width="150px"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="Name">Valor Garantia</label>
                        <asp:TextBox ID="TextBox3" runat="server" ValidationGroup="detalle" Width="150px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Debe Ingresar el Valor del Movimiento"
                            ControlToValidate="TxtValorMvto" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                        <asp:ImageButton ID="ImbCrear" runat="server" ImageUrl="~/Images/Crear.png" Height="40"
                            OnClick="ImbCrear_Click" OnClientClick="return confirmar();" />
                        <asp:ImageButton ID="ImbRegresar" runat="server" ImageUrl="~/Images/Regresar.png"
                            Height="40" OnClick="ImbRegresar_Click" />
                    </div>
                </div>

            </div>
        </div>
        <div runat="server"
            id="tblLiberar" visible="false">

            <asp:ImageButton ID="imbRegresar1" runat="server" ImageUrl="~/Images/Regresar.png"
                Height="40" OnClick="ImbRegresar1_Click" />
            <br />
            <br />
            <div>
                <asp:DataGrid ID="dtgLiberar" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                    PagerStyle-HorizontalAlign="Center" AlternatingItemStyle-CssClass="td1" ItemStyle-CssClass="td2"
                    HeaderStyle-CssClass="th1" OnEditCommand="dtgLiberar_EditCommand">
                    <AlternatingItemStyle CssClass="td1"></AlternatingItemStyle>
                    <ItemStyle CssClass="td2"></ItemStyle>
                    <Columns>
                        <asp:BoundColumn DataField="fecha_cons" HeaderText="Fecha Constitucion" ItemStyle-HorizontalAlign="Left"
                            DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fecha_garantia" HeaderText="Fecha Garantía" ItemStyle-HorizontalAlign="Left"
                            DataFormatString="{0:yyyy/MM/dd}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="desc_tipo_grtia" HeaderText="tpo grtia" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="120px"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numero_garantia" HeaderText="no Grtia" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="120px"></asp:BoundColumn>
                        <asp:BoundColumn DataField="no_documento" HeaderText="Nit" ItemStyle-HorizontalAlign="Left"
                            ItemStyle-Width="120px"></asp:BoundColumn>
                        <asp:BoundColumn DataField="valor_constituido" HeaderText="Valor constituido" ItemStyle-HorizontalAlign="Right"
                            DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                        <asp:BoundColumn DataField="saldo" HeaderText="saldo" ItemStyle-HorizontalAlign="Right"
                            DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Vlr Lib">
                            <ItemTemplate>
                                <asp:TextBox ID="txtValor" runat="server" Width="50px"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="FTBEtxtValor" runat="server" TargetControlID="txtValor"
                                    FilterType="Custom, Numbers" ValidChars=","></ajaxToolkit:FilteredTextBoxExtender>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:EditCommandColumn HeaderText="Liberar" EditText="Liberar"></asp:EditCommandColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
            <asp:HiddenField ID="hndFecha" runat="server" />
            <asp:HiddenField ID="hndDocumento" runat="server" />

        </div>
    </div>

</asp:Content>
