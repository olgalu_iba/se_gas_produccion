﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Segas.Web.Elements;
using SIB.Global.Negocio.Manejador;
using SIB.Global.Presentacion;

namespace Garantias
{
    /// <summary>
    /// 
    /// </summary>
    public partial class frm_ConsVariabGarantParti : Page
    {
        InfoSessionVO goInfo = null;
        static string lsTitulo = "Consulta Variables de Garantías de Participación Subasta Bimestral";
        clConexion lConexion = null;
        SqlDataReader lLector;
        DataSet lds = new DataSet();
        SqlDataAdapter lsqldata = new SqlDataAdapter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            goInfo = (InfoSessionVO)Session["infoSession"];
            if (goInfo == null) Response.Redirect("../index.aspx");
            goInfo.Programa = lsTitulo;
            lblTitulo.Text = lsTitulo.ToString();
            lConexion = new clConexion(goInfo);
            //Se agrega el comportamiento del botón
            buttons.ExportarExcelOnclick += ImgExcel_Click;
            buttons.FiltrarOnclick += btnConsultar_Click;

            if (IsPostBack) return;

            //Titulo
            Master.Titulo = "Garantias";
            //Descripcion
            //Master.DescripcionPagina = "Participación";
            //
            // Carga informacion de combos
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' order by codigo_operador", 0, 4);
            LlenarControles(lConexion.gObjConexion, ddlFechaRueda, "t_rueda", "  codigo_tipo_subasta = 6 And estado in ('F','C')  order by fecha_rueda desc", 1, 2);  //20170307 rq007-17 subasta bimestral fase II
            LlenarControles(lConexion.gObjConexion, ddlPuntoEnt, "m_pozo", " estado = 'A' order by descripcion", 0, 1);
            lConexion.Cerrar();
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);

            if (Session["tipoPerfil"].ToString() != "N") return;
            ddlOperador.SelectedValue = goInfo.cod_comisionista;
            ddlOperador.Enabled = false;
        }

        /// <summary>
        /// Nombre: LlenarControles
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para llenar los Controles DDl
        /// Modificacion:
        /// <summary>
        /// <param name="lConn"></param>
        /// <param name="lDdl"></param>
        protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, int liIndiceLlave, int liIndiceDescripcion)
        {
            SqlDataReader lLector;
            // Proceso para Cargar el DDL de ciudad
            if (lsTabla == "t_rueda")
            {
                string[] lsNombreParametrosC = { "@P_cadena" };
                SqlDbType[] lTipoparametrosC = { SqlDbType.VarChar };
                string[] lValorParametrosC = { " Select TOP 6 cast(numero_rueda as int) as numero_rueda, fecha_rueda into #tmp_rueda From t_rueda Where codigo_tipo_subasta = 6 And estado ='C'  order by fecha_rueda " +
                                               "insert into #tmp_rueda Select TOP 3 numero_rueda, fecha_rueda From t_rueda Where codigo_tipo_subasta = 6 And estado ='F' order by fecha_rueda desc " +
                                               "select * from #tmp_rueda order by fecha_rueda desc" };  //20170307 rq007-17 subasta bimestral fase II
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametrosC, lTipoparametrosC, lValorParametrosC);
            }
            else
                lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

            var lItem = new ListItem { Value = "0", Text = "Seleccione" };
            lDdl.Items.Add(lItem);

            while (lLector.Read())
            {
                var lItem1 = new ListItem();
                if (lsTabla == "m_operador")
                {
                    lItem1.Value = lLector["codigo_operador"].ToString();
                    lItem1.Text = lLector["razon_social"].ToString() + " - " + lLector["codigo_operador"].ToString();
                }
                else
                {
                    if (lsTabla == "t_rueda")
                    {
                        lItem1.Value = lLector["numero_rueda"].ToString();
                        lItem1.Text = lLector["fecha_rueda"].ToString().Substring(6, 4) + "/" + lLector["fecha_rueda"].ToString().Substring(3, 2) + "/" + lLector["fecha_rueda"].ToString().Substring(0, 2) + " - " + lLector["numero_rueda"].ToString();
                    }
                    else
                    {
                        lItem1.Value = lLector["codigo_pozo"].ToString();
                        lItem1.Text = lLector["descripcion"].ToString();
                    }
                }
                lDdl.Items.Add(lItem1);
            }
            lLector.Close();
        }

        /// <summary>
        /// Nombre: ImgExcel_Click
        /// Fecha: Agosto 11 de 2008
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Exportar la Grilla a Excel
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImgExcel_Click(object sender, EventArgs e)
        {
            if (hdfExcel.Value == "1")
            {
                string lsNombreArchivo = goInfo.Usuario.ToString() + "InfCalGarant" + DateTime.Now + ".xls";
                string lstitulo_informe = "Informe Variables de Garantías Participación Subasta Bimestral";
                string lsTituloParametros = "";
                try
                {
                    lsTituloParametros += "Fecha Rueda : " + ddlFechaRueda.SelectedValue;
                    if (ddlOperador.SelectedValue != "0")
                        lsTituloParametros += " - Operador: " + ddlOperador.SelectedItem.ToString();
                    if (ddlPunta.SelectedValue != "0")
                        lsTituloParametros += " - Punta: " + ddlPunta.SelectedItem.ToString();
                    if (ddlTipoGarantia.SelectedValue != "0")
                        lsTituloParametros += " - Tipo Garantía: " + ddlTipoGarantia.SelectedItem.ToString();
                    if (ddlPuntoEnt.SelectedValue != "0")
                        lsTituloParametros += " - Punt Enntrega: " + ddlPuntoEnt.SelectedItem.ToString();

                    decimal ldCapacidad = 0;
                    StringBuilder lsb = new StringBuilder();
                    ldCapacidad = Convert.ToDecimal(lsb.MaxCapacity);
                    StringWriter lsw = new StringWriter(lsb);
                    HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
                    Page lpagina = new Page();
                    HtmlForm lform = new HtmlForm();
                    dtgConsulta.EnableViewState = false;
                    lpagina.EnableEventValidation = false;
                    lpagina.DesignerInitialize();
                    lpagina.Controls.Add(lform);

                    lform.Controls.Add(dtgConsulta);
                    lpagina.RenderControl(lhtw);
                    Response.Clear();

                    Response.Buffer = true;
                    Response.ContentType = "aplication/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
                    Response.ContentEncoding = System.Text.Encoding.Default;
                    Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
                    Response.Charset = "UTF-8";
                    Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + goInfo.nombre.ToString() + "</td></tr></table>");
                    Response.Write("<table><tr><th colspan='5' align='left'><font face=Arial size=4>" + lstitulo_informe + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
                    Response.Write("<table><tr><th colspan='10' align='left'><font face=Arial size=2> Parametros: " + lsTituloParametros + "</font></th><td></td></tr></table><br>");
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(lsb.ToString());
                    Response.End();
                    lds.Dispose();
                    lsqldata.Dispose();
                    lConexion.CerrarInforme();
                }
                catch (Exception ex)
                {
                    Toastr.Error(this, "No se Pude Generar el Excel.!" + ex.Message);
                }
            }
            else
                Toastr.Error(this, "No se puede genrar el Excel por que no ha realizado la consulta.!");
        }

        /// <summary>
        /// Nombre: btnConsultar_Click
        /// Fecha: Septiembre 09 de 2016
        /// Creador: German Eduardo Guarnizo
        /// Descripcion: Metodo para Generar los datos del a grilla al consultar la informacion
        /// Modificacion:
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            var mensaje = new StringBuilder();
            hdfExcel.Value = "0";
            if (ddlFechaRueda.SelectedValue == "0")
            {
                mensaje.Append("Debe Seleccionar la Fecha de Rueda a Consultar.");
            }
            if (ddlOperador.SelectedValue == "0")
            {
                mensaje.Append("Debe Seleccionar la Fecha de  Rueda a Consultar.");
            }

            if (!string.IsNullOrEmpty(mensaje.ToString()))
            {
                Toastr.Warning(this, mensaje.ToString());
                return;
            }
            try
            {
                lConexion = new clConexion(goInfo);
                lConexion.Abrir();
                SqlCommand lComando = new SqlCommand();
                lComando.Connection = lConexion.gObjConexion;
                lComando.CommandTimeout = 3600;
                lComando.CommandType = CommandType.StoredProcedure;
                lComando.CommandText = "pa_GetConsVariaGarantiaPaticipaSb";
                lComando.Parameters.Add("@P_operador", SqlDbType.Int).Value = ddlOperador.SelectedValue;
                lComando.Parameters.Add("@P_punta", SqlDbType.VarChar).Value = ddlPunta.SelectedValue;
                lComando.Parameters.Add("@P_numero_rueda", SqlDbType.Int).Value = ddlFechaRueda.SelectedValue;
                lComando.Parameters.Add("@P_tipo_garantia", SqlDbType.VarChar).Value = ddlTipoGarantia.SelectedValue;
                lComando.Parameters.Add("@P_codigo_punto", SqlDbType.Int).Value = ddlPuntoEnt.SelectedValue;
                lComando.Parameters.Add("@P_tipo_perfil", SqlDbType.Char).Value = Session["tipoPerfil"].ToString();
                lComando.ExecuteNonQuery();
                lsqldata.SelectCommand = lComando;
                lsqldata.Fill(lds);
                dtgConsulta.DataSource = lds;
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count <= 0)
                {
                    Toastr.Info(this, "No Hay Registros para los criterios de búsqueda.!");
                }
                else
                {
                    tblGrilla.Visible = true;
                    hdfExcel.Value = "1";

                    //20170307 rq007-17 subasta bimestral
                    //if (Session["tipoPerfil"].ToString() != "B")
                    //{
                    //    dtgConsulta.Columns[15].Visible = false;
                    //    dtgConsulta.Columns[16].Visible = false;
                    //}

                    foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                    {
                        if (Grilla.Cells[9].Text == " 0.00")
                            Grilla.Cells[9].Text = "";
                        if (Grilla.Cells[10].Text == " 0.00")
                            Grilla.Cells[10].Text = "";
                        if (Grilla.Cells[11].Text == " 0.00")
                            Grilla.Cells[11].Text = "";
                        if (Grilla.Cells[12].Text == " 0.00")
                            Grilla.Cells[12].Text = "";
                        if (Grilla.Cells[13].Text == " 0.00")
                            Grilla.Cells[13].Text = "";
                        if (Grilla.Cells[14].Text == " 0.00")
                            Grilla.Cells[14].Text = "";
                        if (Grilla.Cells[15].Text == " 0.00")
                            Grilla.Cells[15].Text = "";
                        if (Grilla.Cells[16].Text == " 0.00")
                            Grilla.Cells[16].Text = "";
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "DetenerCrono();", true);
            }
            catch (Exception ex)
            {
                Toastr.Error(this, "No se Pude Generar el Informe.! " + ex.Message);
            }
        }
    }
}