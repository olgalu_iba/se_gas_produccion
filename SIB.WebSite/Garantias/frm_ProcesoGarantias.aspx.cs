﻿using System;
using System.Data;
using System.Data.SqlClient;
using Segas.Web.Elements;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Garantias_frm_ProcesoGarantias : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Proceso de Garantías"; //--20170126 rq122
    clConexion lConexion = null;
    SqlDataReader lLector;
    DataSet lds = new DataSet();
    SqlDataAdapter lsqldata = new SqlDataAdapter();

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        //Se agrega el comportamiento del botón
        //buttons.ExportarExcelOnclick += lkbExcel_Click;
        buttons.FiltrarOnclick += imbConsultar_Click;

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            DateTime ldfecha;
            ldfecha = DateTime.Now;
            TxtFechaIni.Text = ldfecha.Year.ToString() + "/" + ldfecha.Month.ToString().PadLeft(2,'0') + "/" + ldfecha.Day.ToString().PadLeft(2, '0');
            //TxtFecha.Enabled = false;
            //Botones
            EnumBotones[] botones = { EnumBotones.Buscar };
            buttons.Inicializar(botones: botones);
        }

    }
    /// <summary>
    /// Nombre: lkbConsultar_Click
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para Generar la Grilla del Informe de Auditoria
    /// Modificacion:
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void imbConsultar_Click(object sender, EventArgs e)
    {
        bool Error = false;
        DateTime ldFecha;
        if (TxtFechaIni.Text.Trim().Length > 0)
        {
            try
            {
                ldFecha = Convert.ToDateTime(TxtFechaIni.Text);
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "Formato Inválido en el Campo Fecha Corte. <br>", "Error!", 10000000);
                Error = true;
            }
        }
        if (TxtFechaIni.Text.Trim().Length <= 0)
        {
            Toastr.Warning(this, "Debe Ingresar algún Parámetro de búsqueda. <br>", "Error!", 10000000);
            Error = true;
        }
        if (!Error)
        {
            try
            {
                string[] lsNombreParametros = { "@P_fecha_proceso" };
                SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
                string[] lValorParametros = { TxtFechaIni.Text };
                lConexion.Abrir();
                goInfo.mensaje_error = "";
                lLector = DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetValorGarantia", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
                if (goInfo.mensaje_error != "")
                {
                    Toastr.Warning(this, "Se presentó un Problema en el Proceso de Cálculo de Garantías.! " + goInfo.mensaje_error.ToString(), "Error!", 10000000);
                   
                    lConexion.Cerrar();
                }
                else
                {
                    string Mensaje = "";
                    lLector.Read();
                    if (lLector["mensaje"].ToString() != "OK")
                        Mensaje = lLector["mensaje"].ToString();
                    else
                        Mensaje = "Proceso Ejecutado con Éxito.!"; //--20170126 rq122

                    Toastr.Info(this, Mensaje);
                    lLector.Close();
                    lLector.Dispose();
                    lConexion.Cerrar();
                }
            }
            catch (Exception ex)
            {
                Toastr.Warning(this, "No se Pudo Generar el Informe.! " + ex.Message.ToString(), "Error!", 10000000);

               
            }
        }
    }
}