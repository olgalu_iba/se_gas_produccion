﻿using System;
using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Garantias_frm_cartaGarantias : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;
    static string lsTitulo = "Generación de cartas de garantías";
    clConexion lConexion = null;
    DataSet lds = new DataSet();
    string sRutaArc = ConfigurationManager.AppSettings["rutaCargaSarlaft"].ToString();
    string[] lsCarperta;
    string Carpeta;

    protected void Page_Load(object sender, EventArgs e)
    {
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        goInfo.Programa = lsTitulo;
        lblTitulo.Text = lsTitulo.ToString();
        lConexion = new clConexion(goInfo);
        lsCarperta = sRutaArc.Split('\\');
        Carpeta = lsCarperta[lsCarperta.Length - 2];

        if (!IsPostBack)
        {
            /// Llenar controles del Formulario
            lConexion.Abrir();
            LlenarControles(lConexion.gObjConexion, ddlOperador, "m_operador", " estado = 'A' and tipo_operador in('P','I') order by razon_social", 2, 4);
            lConexion.Cerrar();
            CargarDatos();
        }
    }
    /// <summary>
    /// Nombre: LlenarControles
    /// Fecha: Agosto 11 de 2008
    /// Creador: German Eduardo Guarnizo
    /// Descripcion: Metodo para llenar los Controles DDl
    /// Modificacion:
    /// <summary>
    /// <param name="lProcedimiento"></param>
    /// <param name="lConn"></param>
    /// <param name="lDdl"></param>
    protected void LlenarControles(SqlConnection lConn, DropDownList lDdl, string lsTabla, string lsCondicion, Int32 liIndiceLlave, Int32 liIndiceDescripcion)
    {
        SqlDataReader lLector;
        // Proceso para Cargar el DDL de ciudad
        lLector = DelegadaBase.Servicios.LlenarControl(lConn, "pa_ValidarExistencia", lsTabla, lsCondicion);

        System.Web.UI.WebControls.ListItem lItem = new System.Web.UI.WebControls.ListItem();
        lItem.Value = "0";
        lItem.Text = "Seleccione";
        lDdl.Items.Add(lItem);

        while (lLector.Read())
        {
            System.Web.UI.WebControls.ListItem lItem1 = new System.Web.UI.WebControls.ListItem();
            lItem1.Value = lLector.GetValue(liIndiceLlave).ToString();
            lItem1.Text = lLector.GetValue(liIndiceDescripcion).ToString(); ;
            lDdl.Items.Add(lItem1);

        }
        lLector.Dispose();
        lLector.Close();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarDatos();
    }
    /// <summary>
    /// Nombre: CargarDatos
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para traer los datos de la base de datos y Llenar el Control DataGrid.
    /// Modificacion:
    /// </summary>
    private void CargarDatos()
    {
        lblMensaje.Text = "";
        string[] lsNombreParametros = { "@P_no_documento" };
        SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
        string[] lValorParametros = { ddlOperador.SelectedValue };

        if (lblMensaje.Text == "")
        {
            try
            {
                lConexion.Abrir();
                dtgConsulta.DataSource = DelegadaBase.Servicios.LlenarGrillaConParametros(lConexion.gObjConexion, "pa_GetOperadorFact", lsNombreParametros, lTipoparametros, lValorParametros);
                dtgConsulta.DataBind();
                lConexion.Cerrar();
                if (dtgConsulta.Items.Count <= 0)
                    lblMensaje.Text = "No se encontraron Registros.";
                else
                {
                    tblGrilla.Visible = true;
                    //imbExcel.Visible = true;
                    //HyperLink hpLink;
                    //foreach (DataGridItem Grilla in this.dtgConsulta.Items)
                    //{
                    //    string sss = Grilla.Cells[3].Text;
                    //    hpLink = (HyperLink)Grilla.Cells[6].Controls[0];
                    //    hpLink.NavigateUrl = Grilla.Cells[4].Text + Grilla.Cells[3].Text;
                    //}
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se Pudo consultar la información.! " + ex.Message.ToString();
            }
        }
    }
    /// <summary>
    /// Nombre: dtgComisionista_EditCommand
    /// Fecha: Agosto 10 de 2008
    /// Creador: Olga Lucia Ibanez
    /// Descripcion: Metodo para ejecutar los procesos de Modificacion y Eliminacin, cuando se da click en los
    ///              Link del DataGrid.
    /// Modificacion:
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dtgConsulta_EditCommand(object source, DataGridCommandEventArgs e)
    {
        lblMensaje.Text = "";
        if (((LinkButton)e.CommandSource).Text == "Carta")
        {
            try
            {
                //string lsRuta = sRutaArc + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[3].Text.Replace(@"\", "/");
                //string[] lsRutaDet;
                //int liPosition = 0;
                //lsRutaDet = sRutaArc.Split('\\');
                //liPosition = lsRutaDet.Length - 2;
                //string lsRuta = "../" + lsRutaDet[liPosition] + "/" + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[3].Text.Replace(@"\", "/");
                goInfo.mensaje_error = "";
                clCartaGarantias clCarta = new clCartaGarantias();
                string lsRuta = "../PDF/" + clCarta.generarCarta(this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text.Trim(), goInfo);
                if (goInfo.mensaje_error == "")
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "window.open('" + lsRuta + "');", true);
                else
                    lblMensaje.Text = "No se pudo generar el archivo." + goInfo.mensaje_error ;
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "No se pudo generar el archivo." + ex.Message;
            }
        }
        //if (((LinkButton)e.CommandSource).Text == "Descargar")
        //{
        //    try
        //    {
        //        string[] lsNombreParametros = { "@P_codigo_doc_sarlaft", "@P_accion" };
        //        SqlDbType[] lTipoparametros = { SqlDbType.VarChar, SqlDbType.Char };
        //        string[] lValorParametros = { this.dtgConsulta.Items[e.Item.ItemIndex].Cells[0].Text.Trim(), "E" };
        //        if (File.Exists(sRutaArc + "\\" + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[3].Text.Trim()))
        //            File.Delete(sRutaArc + "\\" + this.dtgConsulta.Items[e.Item.ItemIndex].Cells[3].Text.Trim());
        //        lConexion.Abrir();
        //        DelegadaBase.Servicios.EjecutarProcedimientoYObtenerDatos(lConexion.gObjConexion, "pa_SetDocSarlaft", lsNombreParametros, lTipoparametros, lValorParametros, goInfo);
        //        if (goInfo.mensaje_error != "")
        //        {
        //            lblMensaje.Text = "Se presentó un Problema en la eliminación del Archivo.! " + goInfo.mensaje_error.ToString();
        //            lConexion.Cerrar();
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "StartupAlert", "alert('" + "Archivo eliminado Correctamente.!" + "');", true);
        //            CargarDatos();
        //        }
        //        lConexion.Cerrar();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMensaje.Text = "Problemas al eliminar el archivo. " + ex.Message.ToString();
        //        lConexion.Cerrar();
        //    }
        //}
    }

    /// <summary>
    /// Exportacion a Excel de la Información  de la Grilla
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void imbExcel_Click(object sender, ImageClickEventArgs e)
    //{
    //    lblMensaje.Text = "";
    //    try
    //    {
    //        string lsNombreArchivo = Session["login"] + "InfDocSarlaft" + DateTime.Now + ".xls";
    //        StringBuilder lsb = new StringBuilder();
    //        StringWriter lsw = new StringWriter(lsb);
    //        HtmlTextWriter lhtw = new HtmlTextWriter(lsw);
    //        Page lpagina = new Page();
    //        HtmlForm lform = new HtmlForm();
    //        lpagina.EnableEventValidation = false;
    //        lpagina.Controls.Add(lform);
    //        dtgConsulta.EnableViewState = false;
    //        dtgConsulta.Columns[5].Visible = false;
    //        dtgConsulta.Columns[6].Visible = false;
    //        lform.Controls.Add(dtgConsulta);
    //        lpagina.RenderControl(lhtw);
    //        Response.Clear();

    //        Response.Buffer = true;
    //        Response.ContentType = "aplication/vnd.ms-excel";
    //        Response.AddHeader("Content-Type", "application/vnd.ms-excel; name=\"" + lsNombreArchivo + "\"");
    //        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lsNombreArchivo + "\"");
    //        Response.ContentEncoding = System.Text.Encoding.Default;

    //        Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title>Datos</title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n<style>\n</style>\n</head>\n<body>\n");
    //        Response.Write("<table><tr><th colspan='1' align='left'> Fecha: </th><td colspan='1' align='left'>" + DateTime.Now + "</td><th colspan='1' align='left'> Usuario: </th><td colspan='1' align='left'>" + Session["NomOperador"] + "</td></tr></table>");
    //        Response.Write("<table><tr><th colspan='3' align='left'><font face=Arial size=4>" + "Consulta Documentos Sarlaft" + "</font></th><td><font face=Arial size=5><center>" + "" + "</center></font></td></tr></table><br>");
    //        Response.Write(lsb.ToString());
    //        Response.End();
    //        Response.Flush();
    //        dtgConsulta.Columns[5].Visible = true;
    //        dtgConsulta.Columns[6].Visible = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        lblMensaje.Text = "Problemas al Consultar los Registros. " + ex.Message.ToString();
    //    }
    //}
}