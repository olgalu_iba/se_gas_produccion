﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_ConsultaCalculoGarantias.aspx.cs" MasterPageFile="~/PlantillaPrincipal.master" Inherits="Garantias.frm_ConsultaCalculoGarantias" %>

<%@ MasterType VirtualPath="~/PlantillaPrincipal.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="kt-container kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <%--Head--%>
            <div class="kt-portlet__head">
                <%--Titulo--%>
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        <asp:Label ID="lblTitulo" runat="server" />
                    </h3>
                </div>
                <%--Botones--%>
                <segas:CrudButton ID="buttons" runat="server" />
            </div>
            <%--Contenido--%>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Periodo" AssociatedControlID="TxtPeriodo" runat="server" />
                            <asp:TextBox ID="TxtPeriodo" runat="server" ValidationGroup="detalle" CssClass="form-control monthDatepicker" ClientIDMode="Static" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Operador" AssociatedControlID="ddlOperador" runat="server" />
                            <asp:DropDownList ID="ddlOperador" runat="server" CssClass="form-control selectpicker" data-live-search="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="form-group">
                            <asp:Label Text="Garantías Consituidas" AssociatedControlID="ddlConstitucion" runat="server" />
                            <asp:DropDownList ID="ddlConstitucion" runat="server" Width="100%" CssClass="form-control selectpicker" data-live-search="true">
                                <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="table table-responsive">
                        <asp:DataGrid ID="dtgMaestro" runat="server" AutoGenerateColumns="False" AllowPaging="false" PagerStyle-HorizontalAlign="Center"
                            Width="100%" CssClass="table-bordered" OnItemCommand="dtgMaestro_EditCommand">
                            <Columns>
                                <asp:BoundColumn DataField="fecha_garantia" HeaderText="Fecha Garantía" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundColumn DataField="no_documento" HeaderText="No. Identificación" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px" />
                                <%--20170126 rq122--%>
                                <asp:BoundColumn DataField="nombre_participante" HeaderText="Nombre Participante" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="200px" />
                                <asp:BoundColumn DataField="valor_garantia" HeaderText="Valor Garantía" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px" />
                                <asp:BoundColumn DataField="valor_constitucion" HeaderText="Valor Constitución" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px" />
                                <asp:BoundColumn DataField="valor_saldo" HeaderText="Valor Saldo" ItemStyle-HorizontalAlign="Right" DataFormatString="{0: ###,###,###,##0.00}" ItemStyle-Width="100px" />
                                <asp:BoundColumn DataField="fecha_calculo" HeaderText="Fecha Proceso" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="130px" />
                                <asp:EditCommandColumn HeaderText="Accion" EditText="Constituir" />
                            </Columns>
                            <HeaderStyle CssClass="theadColor" HorizontalAlign="Center" />
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Modal--%>
    <%--Registro--%>
    <div class="modal fade" id="mdlRegistro" tabindex="-1" role="dialog" aria-labelledby="mdlRegistroLabel" aria-hidden="true" clientidmode="Static" runat="server">
        <div class="modal-dialog" id="mdlRegistroInside" role="document" clientidmode="Static" runat="server">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="mdlRegistroLabel" runat="server">Agregar</h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Operador" AssociatedControlID="lblOperador" runat="server" />
                                        <asp:Label ID="lblOperador" CssClass="form-control" runat="server" ForeColor="Red" Font-Bold="true" />
                                        <asp:HiddenField ID="hdfNoDocumento" runat="server" />
                                        <asp:HiddenField ID="hdfValorGarantia" runat="server" />
                                        <asp:HiddenField ID="hdfFechaGarantia" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Fecha Garantía" AssociatedControlID="lblFechaGarantia" runat="server" />
                                        <asp:Label ID="lblFechaGarantia" placeholder="yyyy/mm/dd" CssClass="form-control datepicker" runat="server" ForeColor="Red" Font-Bold="true" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Saldo x Constituir" AssociatedControlID="lblSalCons" runat="server" />
                                        <asp:Label ID="lblSalCons" CssClass="form-control" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Fecha Movimiento" AssociatedControlID="TxtFechaMvto" runat="server" />
                                        <asp:TextBox ID="TxtFechaMvto" placeholder="yyyy/mm/dd" CssClass="form-control datepicker" runat="server" ValidationGroup="detalle" Width="100px" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Tipo Movimiento" AssociatedControlID="ddlTipoMvto" runat="server" />
                                        <asp:DropDownList ID="ddlTipoMvto" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Tipo Garantía" AssociatedControlID="ddlTipoGarantia" runat="server" />
                                        <asp:DropDownList ID="ddlTipoGarantia" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Detalle Movimiento" AssociatedControlID="TxtDetalleMvto" runat="server" />
                                        <asp:TextBox ID="TxtDetalleMvto" CssClass="form-control" runat="server" ValidationGroup="detalle" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Debe Ingresar el Detalle del Movimiento" ControlToValidate="TxtDetalleMvto" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Reutiliza Garantía" AssociatedControlID="ddlReutiliza" runat="server" />
                                        <asp:DropDownList ID="ddlReutiliza" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlReutiliza_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                            <asp:ListItem Value="S" Text="Si"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Número Garantía" AssociatedControlID="TxtNoGarantia" runat="server" />
                                        <asp:TextBox ID="TxtNoGarantia" CssClass="form-control" runat="server" ValidationGroup="detalle" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Debe Ingresar el Número de la Garantia" ControlToValidate="TxtNoGarantia" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label Text="Valor Movimiento" AssociatedControlID="ddlTipoGarantia" runat="server" />
                                        <asp:TextBox ID="TxtValorMvto" CssClass="form-control" runat="server" ValidationGroup="detalle" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Debe Ingresar el Valor del Movimiento" ControlToValidate="TxtValorMvto" ValidationGroup="detalle"> * </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblFechaVenc" Text="Fecha Vencimiento" AssociatedControlID="TxtFechaVenc" runat="server" />
                                        <asp:TextBox ID="TxtFechaVenc" placeholder="yyyy/mm/dd" CssClass="form-control datepicker" runat="server" ValidationGroup="detalle" Width="100px" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblValorGarantia" Text="Valor Garantía" AssociatedControlID="TxtValorGarantia" runat="server" />
                                        <asp:TextBox ID="TxtValorGarantia" CssClass="form-control" runat="server" ValidationGroup="detalle" />
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button Text="Cancelar" CssClass="btn btn-secondary" OnClientClick="this.disabled = true; return confirmar();" OnClick="ImbRegresar_Click" UseSubmitBehavior="false" runat="server" />
                                <asp:Button ID="ImbCrear" runat="server" CssClass="btn btn-primary" Text="Crear" OnClick="ImbCrear_Click" ValidationGroup="consulta" OnClientClick="this.disabled = true;" UseSubmitBehavior="false" />
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
