﻿// Elaboro: German Eduardo Guarnizo
// Fecha: 2016-06-25
// Funciones Que Arman las grillas de Jquery de los Inndicadores

// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador1N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess1N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador1P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess1P,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Fuente
function GetIndicador1F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess1F,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador2N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess2N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador2P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess2P,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Fuente
function GetIndicador2F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess2F,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador3N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess3N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador3P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess3P,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Fuente
function GetIndicador3F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess3F,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador4N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess4N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador4P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess4P,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Fuente
function GetIndicador4F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess4F,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador5N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess5N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador5P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess5P,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Fuente
function GetIndicador5F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess5F,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador6N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess6N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador6P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess6P,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Fuente
function GetIndicador6F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess6F,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador7N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess7N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador7P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess7P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador8N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess8N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador8F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess8F,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador9N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess9N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador9F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess9F,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador10N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess10N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador10F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess10F,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador10P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess10P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador10S(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess10S,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador11N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess11N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador11F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess11F,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador11P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess11P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador11S(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess11S,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador12N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess12N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador12P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess12P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Agregado Nacional 
function GetIndicador13N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess13N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador13P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess13P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador14(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess14,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador15(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess15,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP por Productor
function GetIndicador16(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess16,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador17N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess17N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador17P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess17P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador18N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess18N,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador18P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess18P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador19(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess19,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador20(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess20,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador21F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess21F,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador21P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess21P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador22(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess22,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador23N(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess23N,
        failure: function(response) {
            alert(response.d);
        },
        error: function(response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador23F(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess23F,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador23P(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess23P,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador23C(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess23C,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador23D(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess23D,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}
// Genera la Grilla del Indiciado MP Indicadores 17 y 18 Vista General
function GetIndicador23S(pageIndex) {
    $.ajax({
        type: "POST",
        url: "frm_ConsIndicadoresMp.aspx/GetIndicador",
        data: "{tipoIndica: '" + document.getElementById("ddlTipoIndica").value + "', AnoConsulta: " + document.getElementById("TxtAnoIndica").value + ", tipoVista: '" + document.getElementById("ddlTipoVista").value + "', AnoIni: " + document.getElementById("TxtAnoIni").value + ", MesIni: " + document.getElementById("ddlMesIni").value + ", AnoFin: " + document.getElementById("TxtAnoFin").value + ", MesFin: " + document.getElementById("ddlMesFinal").value + ", pageIndex: " + pageIndex + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess23S,
        failure: function (response) {
            alert(response.d);
        },
        error: function (response) {
            alert(response.d);
        }
    });
}

function OnSuccess1N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador1N] tr:last-child").clone(true);
    $("[id*=GvIndicador1N] tr").not($("[id*=GvIndicador1N] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador1N]").append(row1);
        row1 = $("[id*=GvIndicador1N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess1P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador1P] tr:last-child").clone(true);
    $("[id*=GvIndicador1P] tr").not($("[id*=GvIndicador1P] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador1P]").append(row1);
        row1 = $("[id*=GvIndicador1P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess1F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador1F] tr:last-child").clone(true);
    $("[id*=GvIndicador1F] tr").not($("[id*=GvIndicador1F] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador1F]").append(row1);
        row1 = $("[id*=GvIndicador1F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess2N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador2N] tr:last-child").clone(true);
    $("[id*=GvIndicador2N] tr").not($("[id*=GvIndicador2N] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("numerador2").text());
        $("td", row1).eq(4).html($(this).find("denominador1").text());
        $("td", row1).eq(5).html($(this).find("denominador2").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador2N]").append(row1);
        row1 = $("[id*=GvIndicador2N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess2P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador2P] tr:last-child").clone(true);
    $("[id*=GvIndicador2P] tr").not($("[id*=GvIndicador2P] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("nit_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("numerador2").text());
        $("td", row1).eq(6).html($(this).find("denominador1").text());
        $("td", row1).eq(7).html($(this).find("denominador2").text());
        $("td", row1).eq(8).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador2P]").append(row1);
        row1 = $("[id*=GvIndicador2P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess2F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador2F] tr:last-child").clone(true);
    $("[id*=GvIndicador2F] tr").not($("[id*=GvIndicador2F] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("numerador2").text());
        $("td", row1).eq(6).html($(this).find("denominador1").text());
        $("td", row1).eq(7).html($(this).find("denominador2").text());
        $("td", row1).eq(8).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador2F]").append(row1);
        row1 = $("[id*=GvIndicador2F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess3N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador3N] tr:last-child").clone(true);
    $("[id*=GvIndicador3N] tr").not($("[id*=GvIndicador3N] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador3N]").append(row1);
        row1 = $("[id*=GvIndicador3N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess3P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador3P] tr:last-child").clone(true);
    $("[id*=GvIndicador3P] tr").not($("[id*=GvIndicador3P] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador3P]").append(row1);
        row1 = $("[id*=GvIndicador3P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess3F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador3F] tr:last-child").clone(true);
    $("[id*=GvIndicador3F] tr").not($("[id*=GvIndicador3F] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador3F]").append(row1);
        row1 = $("[id*=GvIndicador3F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess4N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador4N] tr:last-child").clone(true);
    $("[id*=GvIndicador4N] tr").not($("[id*=GvIndicador4N] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("denominador2").text());
        $("td", row1).eq(5).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador4N]").append(row1);
        row1 = $("[id*=GvIndicador4N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess4P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador4P] tr:last-child").clone(true);
    $("[id*=GvIndicador4P] tr").not($("[id*=GvIndicador4P] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("nit_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador4P]").append(row1);
        row1 = $("[id*=GvIndicador4P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess4F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador4F] tr:last-child").clone(true);
    $("[id*=GvIndicador4F] tr").not($("[id*=GvIndicador4F] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador4F]").append(row1);
        row1 = $("[id*=GvIndicador4F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess5N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador5N] tr:last-child").clone(true);
    $("[id*=GvIndicador5N] tr").not($("[id*=GvIndicador5N] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("denominador2").text());
        $("td", row1).eq(5).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador5N]").append(row1);
        row1 = $("[id*=GvIndicador5N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess5P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador5P] tr:last-child").clone(true);
    $("[id*=GvIndicador5P] tr").not($("[id*=GvIndicador5P] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("nit_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador5P]").append(row1);
        row1 = $("[id*=GvIndicador5P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess5F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador5F] tr:last-child").clone(true);
    $("[id*=GvIndicador5F] tr").not($("[id*=GvIndicador5F] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador5F]").append(row1);
        row1 = $("[id*=GvIndicador5F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess6N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador6N] tr:last-child").clone(true);
    $("[id*=GvIndicador6N] tr").not($("[id*=GvIndicador6N] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador6N]").append(row1);
        row1 = $("[id*=GvIndicador6N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess6P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador6P] tr:last-child").clone(true);
    $("[id*=GvIndicador6P] tr").not($("[id*=GvIndicador6P] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador6P]").append(row1);
        row1 = $("[id*=GvIndicador6P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess6F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador6F] tr:last-child").clone(true);
    $("[id*=GvIndicador6F] tr").not($("[id*=GvIndicador6F] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador6F]").append(row1);
        row1 = $("[id*=GvIndicador6F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
 function OnSuccess7N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador7N] tr:last-child").clone(true);
    $("[id*=GvIndicador7N] tr").not($("[id*=GvIndicador7N] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador7N]").append(row1);
        row1 = $("[id*=GvIndicador7N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess7P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador7P] tr:last-child").clone(true);
    $("[id*=GvIndicador7P] tr").not($("[id*=GvIndicador7P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador7P]").append(row1);
        row1 = $("[id*=GvIndicador7P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess8N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador8N] tr:last-child").clone(true);
    $("[id*=GvIndicador8N] tr").not($("[id*=GvIndicador8N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("denominador2").text());
        $("td", row1).eq(5).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador8N]").append(row1);
        row1 = $("[id*=GvIndicador8N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess8F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador8F] tr:last-child").clone(true);
    $("[id*=GvIndicador8F] tr").not($("[id*=GvIndicador8F] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador8F]").append(row1);
        row1 = $("[id*=GvIndicador8F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess9N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador9N] tr:last-child").clone(true);
    $("[id*=GvIndicador9N] tr").not($("[id*=GvIndicador9N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador9N]").append(row1);
        row1 = $("[id*=GvIndicador9N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess9F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador9F] tr:last-child").clone(true);
    $("[id*=GvIndicador9F] tr").not($("[id*=GvIndicador9F] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador9F]").append(row1);
        row1 = $("[id*=GvIndicador9F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess10N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador10N] tr:last-child").clone(true);
    $("[id*=GvIndicador10N] tr").not($("[id*=GvIndicador10N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("denominador2").text());
        $("td", row1).eq(5).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador10N]").append(row1);
        row1 = $("[id*=GvIndicador10N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess10F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador10F] tr:last-child").clone(true);
    $("[id*=GvIndicador10F] tr").not($("[id*=GvIndicador10F] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador10F]").append(row1);
        row1 = $("[id*=GvIndicador10F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess10P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador10P] tr:last-child").clone(true);
    $("[id*=GvIndicador10P] tr").not($("[id*=GvIndicador10P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("nit_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador10P]").append(row1);
        row1 = $("[id*=GvIndicador10P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess10S(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador10S] tr:last-child").clone(true);
    $("[id*=GvIndicador10S] tr").not($("[id*=GvIndicador10S] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_sector").text());
        $("td", row1).eq(3).html($(this).find("desc_sector").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador10S]").append(row1);
        row1 = $("[id*=GvIndicador10S] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess11N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador11N] tr:last-child").clone(true);
    $("[id*=GvIndicador11N] tr").not($("[id*=GvIndicador11N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("denominador2").text());
        $("td", row1).eq(5).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador11N]").append(row1);
        row1 = $("[id*=GvIndicador11N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess11F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador11F] tr:last-child").clone(true);
    $("[id*=GvIndicador11F] tr").not($("[id*=GvIndicador11F] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador11F]").append(row1);
        row1 = $("[id*=GvIndicador11F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess11P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador11P] tr:last-child").clone(true);
    $("[id*=GvIndicador11P] tr").not($("[id*=GvIndicador11P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador11P]").append(row1);
        row1 = $("[id*=GvIndicador11P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess11S(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador11S] tr:last-child").clone(true);
    $("[id*=GvIndicador11S] tr").not($("[id*=GvIndicador11S] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_sector").text());
        $("td", row1).eq(3).html($(this).find("desc_sector").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador11S]").append(row1);
        row1 = $("[id*=GvIndicador11S] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};

function OnSuccess12N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador12N] tr:last-child").clone(true);
    $("[id*=GvIndicador12N] tr").not($("[id*=GvIndicador12N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("denominador2").text());
        $("td", row1).eq(5).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador12N]").append(row1);
        row1 = $("[id*=GvIndicador12N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess12P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador12P] tr:last-child").clone(true);
    $("[id*=GvIndicador12P] tr").not($("[id*=GvIndicador12P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador12P]").append(row1);
        row1 = $("[id*=GvIndicador12P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess13N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador13N] tr:last-child").clone(true);
    $("[id*=GvIndicador13N] tr").not($("[id*=GvIndicador13N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("denominador2").text());
        $("td", row1).eq(5).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador13N]").append(row1);
        row1 = $("[id*=GvIndicador13N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess13P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador13P] tr:last-child").clone(true);
    $("[id*=GvIndicador13P] tr").not($("[id*=GvIndicador13P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("denominador2").text());
        $("td", row1).eq(7).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador13P]").append(row1);
        row1 = $("[id*=GvIndicador13P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess14(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador14] tr:last-child").clone(true);
    $("[id*=GvIndicador14] tr").not($("[id*=GvIndicador14] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_mercado").text());
        $("td", row1).eq(3).html($(this).find("desc_mercado").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador14]").append(row1);
        row1 = $("[id*=GvIndicador14] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess15(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador15] tr:last-child").clone(true);
    $("[id*=GvIndicador15] tr").not($("[id*=GvIndicador15] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_mercado").text());
        $("td", row1).eq(3).html($(this).find("desc_mercado").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador15]").append(row1);
        row1 = $("[id*=GvIndicador15] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess16(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador16] tr:last-child").clone(true);
    $("[id*=GvIndicador16] tr").not($("[id*=GvIndicador16] tr:first-child")).remove();
    $.each(indicadores, function() {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(5).html($(this).find("desc_punto").text());
        $("td", row1).eq(6).html($(this).find("numerador1").text());
        $("td", row1).eq(7).html($(this).find("denominador1").text());
        $("td", row1).eq(8).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador16]").append(row1);
        row1 = $("[id*=GvIndicador16] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess17N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador17N] tr:last-child").clone(true);
    $("[id*=GvIndicador17N] tr").not($("[id*=GvIndicador17N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_modalidad").text());
        $("td", row1).eq(3).html($(this).find("desc_modalidad").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador17N]").append(row1);
        row1 = $("[id*=GvIndicador17N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess17P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador17P] tr:last-child").clone(true);
    $("[id*=GvIndicador17P] tr").not($("[id*=GvIndicador17P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("codigo_modalidad").text());
        $("td", row1).eq(5).html($(this).find("desc_modalidad").text());
        $("td", row1).eq(6).html($(this).find("numerador1").text());
        $("td", row1).eq(7).html($(this).find("denominador1").text());
        $("td", row1).eq(8).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador17P]").append(row1);
        row1 = $("[id*=GvIndicador17P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess18N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador18N] tr:last-child").clone(true);
    $("[id*=GvIndicador18N] tr").not($("[id*=GvIndicador18N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_modalidad").text());
        $("td", row1).eq(3).html($(this).find("desc_modalidad").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador18N]").append(row1);
        row1 = $("[id*=GvIndicador18N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess18P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador18P] tr:last-child").clone(true);
    $("[id*=GvIndicador18P] tr").not($("[id*=GvIndicador18P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("codigo_modalidad").text());
        $("td", row1).eq(5).html($(this).find("desc_modalidad").text());
        $("td", row1).eq(6).html($(this).find("numerador1").text());
        $("td", row1).eq(7).html($(this).find("denominador1").text());
        $("td", row1).eq(8).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador18P]").append(row1);
        row1 = $("[id*=GvIndicador18P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess19(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador19] tr:last-child").clone(true);
    $("[id*=GvIndicador19] tr").not($("[id*=GvIndicador19] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("codigo_mercado_relevante").text());
        $("td", row1).eq(5).html($(this).find("desc_mercado").text());
        $("td", row1).eq(6).html($(this).find("codigo_modalidad").text());
        $("td", row1).eq(7).html($(this).find("desc_modalidad").text());
        $("td", row1).eq(8).html($(this).find("numerador1").text());
        $("td", row1).eq(9).html($(this).find("denominador1").text());
        $("td", row1).eq(10).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador19]").append(row1);
        row1 = $("[id*=GvIndicador19] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess20(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador20] tr:last-child").clone(true);
    $("[id*=GvIndicador20] tr").not($("[id*=GvIndicador20] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(5).html($(this).find("desc_punto").text());
        $("td", row1).eq(6).html($(this).find("codigo_modalidad").text());
        $("td", row1).eq(7).html($(this).find("desc_modalidad").text());
        $("td", row1).eq(8).html($(this).find("numerador1").text());
        $("td", row1).eq(9).html($(this).find("denominador1").text());
        $("td", row1).eq(10).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador20]").append(row1);
        row1 = $("[id*=GvIndicador20] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess21F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador21F] tr:last-child").clone(true);
    $("[id*=GvIndicador21F] tr").not($("[id*=GvIndicador21F] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador21F]").append(row1);
        row1 = $("[id*=GvIndicador21F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess21P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador21P] tr:last-child").clone(true);
    $("[id*=GvIndicador21P] tr").not($("[id*=GvIndicador21P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador21P]").append(row1);
        row1 = $("[id*=GvIndicador21P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess22(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador22] tr:last-child").clone(true);
    $("[id*=GvIndicador22] tr").not($("[id*=GvIndicador22] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(5).html($(this).find("desc_punto").text());
        $("td", row1).eq(6).html($(this).find("numerador1").text());
        $("td", row1).eq(7).html($(this).find("denominador1").text());
        $("td", row1).eq(8).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador22]").append(row1);
        row1 = $("[id*=GvIndicador22] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess23N(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador23N] tr:last-child").clone(true);
    $("[id*=GvIndicador23N] tr").not($("[id*=GvIndicador23N] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("numerador1").text());
        $("td", row1).eq(3).html($(this).find("denominador1").text());
        $("td", row1).eq(4).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador23N]").append(row1);
        row1 = $("[id*=GvIndicador23N] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess23F(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador23F] tr:last-child").clone(true);
    $("[id*=GvIndicador23F] tr").not($("[id*=GvIndicador23F] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_pozo").text());
        $("td", row1).eq(3).html($(this).find("desc_punto").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador23F]").append(row1);
        row1 = $("[id*=GvIndicador23F] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess23P(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador23P] tr:last-child").clone(true);
    $("[id*=GvIndicador23P] tr").not($("[id*=GvIndicador23P] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_operador").text());
        $("td", row1).eq(3).html($(this).find("nombre_operador").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador23P]").append(row1);
        row1 = $("[id*=GvIndicador23P] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess23C(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador23C] tr:last-child").clone(true);
    $("[id*=GvIndicador23C] tr").not($("[id*=GvIndicador23C] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_modaldiad").text());
        $("td", row1).eq(3).html($(this).find("desc_modalidad").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador23C]").append(row1);
        row1 = $("[id*=GvIndicador23C] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess23D(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador23D] tr:last-child").clone(true);
    $("[id*=GvIndicador23D] tr").not($("[id*=GvIndicador23D] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_tipo_demanda").text());
        $("td", row1).eq(3).html($(this).find("desc_demanda").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador23D]").append(row1);
        row1 = $("[id*=GvIndicador23D] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
function OnSuccess23S(response) {
    var xmlDoc = $.parseXML(response.d);
    var xml = $(xmlDoc);
    var indicadores = xml.find("Indicadores");

    var row1 = $("[id*=GvIndicador23S] tr:last-child").clone(true);
    $("[id*=GvIndicador23S] tr").not($("[id*=GvIndicador23S] tr:first-child")).remove();
    $.each(indicadores, function () {
        var indicador1 = $(this);
        $("td", row1).eq(0).html($(this).find("año").text());
        $("td", row1).eq(1).html($(this).find("nombre_mes").text());
        $("td", row1).eq(2).html($(this).find("codigo_sector_consumo").text());
        $("td", row1).eq(3).html($(this).find("desc_sector").text());
        $("td", row1).eq(4).html($(this).find("numerador1").text());
        $("td", row1).eq(5).html($(this).find("denominador1").text());
        $("td", row1).eq(6).html($(this).find("valor_indicador").text());
        $("[id*=GvIndicador23S]").append(row1);
        row1 = $("[id*=GvIndicador23S] tr:last-child").clone(true);
    });
    var pager = xml.find("Pager");
    $(".Pager").ASPSnippets_Pager({
        ActiveCssClass: "current",
        PagerCssClass: "pager",
        PageIndex: parseInt(pager.find("PageIndex").text()),
        PageSize: parseInt(pager.find("PageSize").text()),
        RecordCount: parseInt(pager.find("RecordCount").text())
    });
};
