﻿var descargaArchivo = {
    txt: function descargaArchivoTxt(filename, text) {
        var re = /<br>/g;
        var arrayDeCadenas = text.replace(re, "\n");
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(arrayDeCadenas));
        element.setAttribute('download', filename);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
}