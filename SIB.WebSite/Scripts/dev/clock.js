﻿$(document).ready(function () {
    playClock();
});

function playClock() {
    $.ajax({
        type: "POST",
        url: "../WebService/Autocomplete.asmx/ObtenerHora",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (responseText) {
            var retorno = "";
            retorno = responseText.d;
            document.getElementById('lblRelog').value = retorno;
        }
    });
    //var tmpTime = document.getElementById('HdfTimeTemp');
    //if (tmpTime !== null) {
    //    var serverTime = tmpTime.value;
    //    var newDate = new Date(Date.parse(serverTime));
    //    newDate.setSeconds(newDate.getSeconds() + 1);
    //    document.getElementById('HdfTimeTemp').value = newDate;

    //    var hora = (newDate.getHours() < 10 ? '0' : '') + newDate.getHours();
    //    var minuto = (newDate.getMinutes() < 10 ? '0' : '') + newDate.getMinutes();
    //    var segundo = (newDate.getSeconds() < 10 ? '0' : '') + newDate.getSeconds();
    //    var horaImprimible = hora + ":" + minuto + ":" + segundo;

    //    document.getElementById('lblRelog').value = horaImprimible;s
    var horaImprimible = document.getElementById('lblRelog').value;
    var hour = document.getElementById('HdfTime');
    if (hour !== null)
        hour = `${hour.value}:00`;

    if (horaImprimible >= hour) {
        document.getElementById("btnClickEmulate").style.display = "contents";
        var clickButton = document.getElementById("btnClickEmulate");
        clickButton.click();
        document.getElementById("btnClickEmulate").style.display = "none";
    }
    setTimeout("playClock()", 1000);
}