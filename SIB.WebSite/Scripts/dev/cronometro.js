﻿//CRONOMETRO
var CronoID = null
var CronoEjecutandose = false
var decimas, segundos, minutos, horas
decimas = 0
segundos = 0
minutos = 0
horas = 0 
function MostrarCrono () {
  //incrementa el crono
  decimas++
  if ( decimas > 9 ) {
    decimas = 0
    segundos++
    if ( segundos > 59 ) {
      segundos = 0
      minutos++
      if ( minutos > 59 ) {
         minutos = 0
         horas++
         if ( horas > 99 ) {
           DetenerCrono()
           alert('Fin de la cuenta')
           return true
         }
      }
    }
  }
  //configura la salida
  var ValorCrono = ""
  ValorCrono = (horas < 10) ? "0" + horas : horas
  ValorCrono += (minutos < 10) ? ":0" + minutos : ":" + minutos
  ValorCrono += (segundos < 10) ? ":0" + segundos : ":" + segundos
  ValorCrono += ":" + decimas 
  document.getElementById('display').value = ValorCrono
  CronoID = setTimeout("MostrarCrono()", 100)
  CronoEjecutandose = true
  return true
}
function DetenerCrono (){
  if(CronoEjecutandose)
  clearTimeout(CronoID)
  CronoEjecutandose = false
}
