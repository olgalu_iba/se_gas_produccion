﻿var modal = {
    open: function openModal(modalId, modalInsidelId, size) {
        $(modalId).modal({ backdrop: 'static', show: true, keyboard: false });

        if (size === "Large") {
            $(modalId).addClass("bd-example-modal-xl");
            $(modalInsidelId).addClass("modal-xl");
        }
    },
    close: function closeModal(modalId) {
        $(modalId).modal('hide');
    }
}