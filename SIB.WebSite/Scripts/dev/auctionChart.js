﻿google.charts.load('current', {
    packages: ['corechart', 'scatter']
}).then(function () {
    $("#mdlCurvaOfertaDemAgre").on('shown.bs.modal', function () {
        //Se selecciona el nombre del eje según el tipo de subasta
        var lblEjeX = transporte === 'True' ? 'Capacidad (KPCD)' : 'Cantidad (MBTUD)';
        var lblEjeY = transporte === 'True' ? 'Precio (Moneda vigente/KPC)' : 'Precio (USD/MBTU)'; //20220828

        var options = {
            orientation: 'vertical',
            //width: 700,
            //height: 500,
            series: {
                0: { targetAxisIndex: 0, lineWidth: 3, pointsVisible: false },
                1: { targetAxisIndex: 0, visibleInLegend: false },
                2: { targetAxisIndex: 0, lineWidth: 3, pointsVisible: false }
            },
            title: 'Curva de Oferta y Demanda Agregada',
            hAxis: { title: lblEjeX },
            vAxes: {
                // Adds titles to each axis.
                0: { title: lblEjeY, format: '0.00', viewWindow: { min: 0 } },
                1: { title: '', viewWindow: { min: 0 } }
            },
            colors: ['#4E7E2A', '#b7b8bb', '#1B6DB6'],
            explorer: {
                actions: ['dragToZoom', 'rightClickToReset'],
                axis: 'horizontal',
                keepInBounds: true,
                maxZoomIn: 100
            }
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    });
});

google.charts.load('current', {
    packages: ['corechart', 'scatter']
}).then(function () {
    $("#mdlCurvaDem").on('shown.bs.modal', function () {
        //Se selecciona el nombre del eje según el tipo de subasta
        var lblEjeX = transporte === 'True' ? 'Cantidad (KPCD)' : 'Cantidad (MBTUD)';
        var lblEjeY = transporte === 'True' ? 'Precio (Moneda vigente/KPC)' : 'Precio (USD/MBTU)'; //20220828

        var options = {
            orientation: 'vertical',
            //width: 700,
            //height: 500,
            series: {
                0: { targetAxisIndex: 0, visibleInLegend: false },
                1: { targetAxisIndex: 0, lineWidth: 3, pointsVisible: false }
            },
            title: 'Curvas de Demanda',
            hAxis: { title: lblEjeX, viewWindow: { min: Number(inicioCoordenadaX) } },
            vAxes: {
                // Adds titles to each axis.
                0: { title: lblEjeY, format: '0.00', viewWindow: { min: inicioCoordenadaY } },
                1: { title: '' }
            },
            colors: ['#b7b8bb', '#4E7E2A']
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    });
});

var data;
var transporte;
var inicioCoordenadaX;
var inicioCoordenadaY;

function drawChartRefresh(strDatos, strTransporte, strInicioCoordenadaX, strInicioCoordenadaY) {
    inicioCoordenadaX = strInicioCoordenadaX;
    inicioCoordenadaY = strInicioCoordenadaY;
    data = google.visualization.arrayToDataTable(strDatos);
    transporte = strTransporte;
};