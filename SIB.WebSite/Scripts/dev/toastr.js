﻿var segasToastr = {
    info: function sucessToaster(mesage, title, timeOut) {
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        };
        if (timeOut === null)
            toastr.info(mesage, title);
        else
            toastr.info(mesage, title, { timeOut: `${timeOut}` });
    },
    sucess: function sucessToaster(mesage, title, timeOut) {
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        };
        if (timeOut === null)
            toastr.success(mesage, title);
        else
            toastr.success(mesage, title, { timeOut: `${timeOut}` });
    },
    warning: function warningToaster(mesage, title, timeOut) {
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        };
        if (timeOut === null)
            toastr.warning(mesage, title);
        else
            toastr.warning(mesage, title, { timeOut: `${timeOut}` });
    },
    error: function errorToaster(mesage, title, timeOut) {
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        };
        if (timeOut === null)
            toastr.error(mesage, title);
        else
            toastr.error(mesage, title, { timeOut: `${timeOut}` });
    }
}