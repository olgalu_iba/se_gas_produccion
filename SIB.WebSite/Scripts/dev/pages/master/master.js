"use strict";

/**
 * @class KApp
 */

$(document).ready(function () {

    //Se carga para que funcione cuando este dentro de un updatePanel 
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

    //Se llama a la funci�n para que se cargue cuando no este dentro de un updatePanel 
    EndRequestHandler();

    //Funcion para el manejo del datepicker y el datetimepicker
    function EndRequestHandler(sender, args) {
        $.fn.datepicker.dates['es'] = {
            days: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
            daysShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
            daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
            months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            today: "Hoy",
            clear: "Limpiar",
            format: "yyyy/mm/dd",
            titleFormat: "yyyy MM",
            weekStart: 1
        };

        $(".datepicker").datepicker({
            language: 'es',
            todayBtn: 'linked',
            autoclose: true,
            clearBtn: true
        });

        $(".monthDatepicker").datepicker({
            language: 'es',
            format: " yyyy-mm",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true
        });

        $('.datetimepicker').datetimepicker({
            language: 'es',
            format: 'hh:ii',
            todayBtn: 'linked',
            todayHighlight: true,
            pickDate: false,
            minuteStep: 1,
            pickerPosition: 'bottom-right',
            autoclose: true,
            showMeridian: true,
            startView: 1,
            maxView: 1
        });
    }
});

//Refresca los selectpicker despu�s de hacer un postback 
function pageLoad() {
    $('.selectpicker').selectpicker();
}