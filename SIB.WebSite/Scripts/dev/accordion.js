﻿var accordion = {
    select: function cardSelect(cardlId, arrayCardIdDisable, contractCardState) {

        // Elimina la clase show para contraer todo el acordeón
        arrayCardIdDisable.forEach(function (elementId) {
            $(elementId).removeClass("show");
        });
        // Se muestra la pestaña seleccionada 
        var arr = document.getElementById(cardlId.replace('#', ''));
        if (contractCardState == undefined || arr == undefined)
            return;
        if (contractCardState && !arr.classList.contains("show"))
            $(cardlId).addClass("show");
    }
}