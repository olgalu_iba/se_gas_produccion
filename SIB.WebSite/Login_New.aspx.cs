﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using Segas.Web.Elements;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class Login_New : Page
{
    clConexion lConexion = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["TxtUser"].ToString() != "" && TxtUser.Text == "")
        //{
        //    TxtUser.Text = Session["TxtUser"].ToString();
        //}
        //if (Session["TxtPassword_Old"].ToString() != "" && TxtPassword_Old.Text == "")
        //{
        //    TxtPassword_Old.Text = Session["TxtPassword_Old"].ToString();
        //}
    }

    protected void btnIngresar_Click(object sender, EventArgs e)
    {
        try
        {
            clConexion lConexion = null;
            string ObaseDatos = ConfigurationManager.AppSettings["BaseDatos"].ToString();
            string Oservidor = ConfigurationManager.AppSettings["Servidor"].ToString();
            string lsUsuarGenerico = ConfigurationManager.AppSettings["UserGenerico"].ToString();
            string lsClaveGenerico = ConfigurationManager.AppSettings["PwdGenerico"].ToString();
            string ObaseDatosPdf = "";
            string OservidorPdf = "";
            string ObaseDatosInf = ConfigurationManager.AppSettings["BaseDatosInf"].ToString(); //20211110 conexion respaldo
            string OservidorInf = ConfigurationManager.AppSettings["ServidorInf"].ToString(); //202111110 conexion respaldo
            string ObaseDatosRep = "";
            string OservidorRep = "";
            string Ousuario = Request.Form["TxtUser"].ToString();
            string Oclave = "";
            string lsClaveAnt = "";
            lsClaveAnt = Request.Form["TxtPassword_Old"].ToString().Trim();
            lsClaveAnt = DelegadaBase.Servicios.CifrarCadena(lsClaveAnt);
            //CSHA256 oSHA256 = new CSHA256();
            InfoSessionVO info = new InfoSessionVO(Oservidor, ObaseDatos, lsUsuarGenerico, lsClaveGenerico, OservidorPdf, ObaseDatosPdf, OservidorInf, ObaseDatosInf ); //20211110 conexion replica
            lConexion = new clConexion(info);

            //conectar a la base de datos sin encriptar
            try
            {
                if (!DelegadaBase.Servicios.autenticar(info))
                {
                    info.Password = Oclave;
                    if (!DelegadaBase.Servicios.autenticar(info))
                    {

                        lblMensaje.Text = "Su usuario o contraseña actual no son validos";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Error al autentica." + ex.Message;
            }

            //Actualizar el password a la nueva contraseña
            Oclave = Request.Form["TxtPassword_New"].ToString();
            //string nuevoPassword = oSHA256.SHA256(ref Oclave);
            string lsErrorCont = "";
            string nuevoPassword = "";
            try
            {
                nuevoPassword = DelegadaBase.Servicios.CifrarCadena(Oclave);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al cifrar contraseña." + ex.Message;
            }
            try
            {
                lsErrorCont = DelegadaBase.Servicios.ValidaCont(Oclave, nuevoPassword, Ousuario, "C", info);
            }
            catch (Exception ex)
            {
                lblMensaje.Text = "Problemas al Cambiar la Contrasena en el Servidor de Replica.!" + ex.Message;
            }
            if (lsErrorCont == "")
            {
                try
                {
                    //DelegadaBase.Servicios.actualizarPassword(info, nuevoPassword);
                    lConexion.Abrir();
                    //string[] lsNombreParametros = { "@P_cadena" };
                    //SqlDbType[] lTipoparametros = { SqlDbType.VarChar };
                    //string[] lValorParametros = { "ALTER LOGIN [" + Ousuario + "] WITH PASSWORD=N'" + nuevoPassword + "' " };
                    //DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametros, lTipoparametros, lValorParametros);
                    //lValorParametros[0] = "Update a_usuario SET cambio_contrasena = 'N' Where login = '" + Ousuario + "' ";
                    //DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "pa_EjecutarCadena", lsNombreParametros, lTipoparametros, lValorParametros);
                    // Actulizo el historico de contraseñas
                    string[] lsNombreParametros1 = { "@P_LoginUsuario", "@P_Contrasena_ant", "@P_Contrasena_nv", "@P_actualiza_hist" };//20220211 contraseña
                    SqlDbType[] lTipoparametros1 = { SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar, SqlDbType.VarChar };//20220211 contraseña
                    string[] lValorParametros1 = { Ousuario, lsClaveAnt, nuevoPassword, "S" }; //20220211 contraseña
                    DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "SP_UptClave", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                    lConexion.Cerrar();
                    //20211110 conexion replica
                    try
                    {
                        lValorParametros1[3] = "N";//20220211 contraseña
                        lConexion.AbrirInforme(); 
                        DelegadaBase.Servicios.EjecutarProcedimiento(lConexion.gObjConexion, "SP_UptClave", lsNombreParametros1, lTipoparametros1, lValorParametros1);
                        lConexion.CerrarInforme();
                    }
                    catch (Exception ex)
                    { }

                    
                }
                catch (Exception ex)
                {
                    lblMensaje.Text = "problemas actualizar contraseña!" + ex.Message;
                }
                //info.Password = nuevoPassword;
                if (DelegadaBase.Servicios.autenticar(info))
                {
                    //DelegadaBase.Servicios.borrar_a_bloqueo_registro_usuario(info, TxtUser.Text);
                    //Session["infoSession"] = info;
                    //DelegadaBase.Servicios.accesoSistema(info);
                    Response.Redirect("login.aspx?cambio=S");//20220211 contraseña
                }
                else
                {
                    lblMensaje.Text = "No se puede modificar el inicio de sesión '" + info.Usuario + "' porque no existe o el usuario no tiene permiso.";
                    return;
                }
            }
            else
            {
                lblMensaje.Text = lsErrorCont;
                return;
            }
        }
        catch (Exception ex)
        {
            Toastr.Error(this, "Error general." + ex.Message);
        }
    }
}
