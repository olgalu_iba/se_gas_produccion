﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using SIB.Global.Dominio;
using SIB.Global.Presentacion;
using SIB.Global.Negocio.Manejador;

public partial class frm_nuevaPagina : System.Web.UI.Page
{
    InfoSessionVO goInfo = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        string lsPagina = "";
        goInfo = (InfoSessionVO)Session["infoSession"];
        if (goInfo == null) Response.Redirect("../index.aspx");
        try
        {
            string oDestino = "'_blank'";
            if (this.Request.QueryString["pagina"] != null && this.Request.QueryString["pagina"].ToString() != "")
            {
                lsPagina = this.Request.QueryString["pagina"];
                lblUrl.Text = "<script>window.open('" + lsPagina + "?username=" + goInfo.codigo_usuario + "'," + oDestino + ",'');</script>";
                lblUrl.Visible = true;
            }
        }
        catch (Exception ex)
        {

        }
    }
}
