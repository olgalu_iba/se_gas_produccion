﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_cabecera.aspx.cs" Inherits="frm_cabecera" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/estilo.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .reloj
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
            color: #006666;
            border: 0px solid #086A6A;
            background-image: url(imagenes/fondo.png);
        }
        .mensaje
        {
            color: #FF0000;
            font-size: 12px;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>

    <script language="JavaScript">

        function cerrar() {
            res = confirm("Esta seguro que desea salir del sistema?")
            if (res) {
                parent.cerrar = 'no';
                parent.location = 'login.aspx'
            }
        }
        function runClock() {
            var timeNow = new Date();
            var hours = timeNow.getHours();
            var minutes = timeNow.getMinutes();
            var seconds = timeNow.getSeconds();
            var ampm = "";
            (seconds < 10) ? seconds = "0" + seconds : seconds;
            (minutes < 10) ? minutes = "0" + minutes : seconds;
            (hours < 12) ? ampm = "AM" : ampm = "PM";
            (hours > 12) ? hours = hours - 12 : hours;
            (hours == 0) ? hours = 12 : hours;
            var stringTime = " " + hours + ":" + minutes + ":" + seconds + " " + ampm;
            window.document.menu.clockBox.value = stringTime;
            setTimeout("runClock()", 1000);
        }
        function muestraReloj() {
            // Compruebo si se puede ejecutar el script en el navegador del usuario
            if (!document.layers && !document.all && !document.getElementById) return;
            // Obtengo la hora actual y la divido en sus partes
            var fechacompleta = new Date();
            var horas = fechacompleta.getHours();
            var minutos = fechacompleta.getMinutes();
            var segundos = fechacompleta.getSeconds();
            var mt = "AM";
            // Pongo el formato 12 horas
            if (horas > 12) {
                mt = "PM";
                horas = horas - 12;
            }
            if (horas == 0) horas = 12;
            // Pongo minutos y segundos con dos dígitos
            if (minutos <= 9) minutos = "0" + minutos;
            if (segundos <= 9) segundos = "0" + segundos;
            // En la variable 'cadenareloj' puedes cambiar los colores y el tipo de fuente
            cadenareloj = "<font size='2' face='Arial' color='navy'><b>" + horas + ":" + minutos + ":" + segundos + " " + mt + "</b></font>";
            // Escribo el reloj de una manera u otra, según el navegador del usuario
            if (document.layers) {
                document.layers.spanreloj.document.write(cadenareloj);
                document.layers.spanreloj.document.close();
            }
            else if (document.all) spanreloj.innerHTML = cadenareloj;
            else if (document.getElementById) document.getElementById("spanreloj").innerHTML = cadenareloj;
            // Ejecuto la función con un intervalo de un segundo
            setTimeout("muestraReloj()", 1000);
        }
        
    </script>

</head>
<body onload="runClock();">
    <form id="menu" runat="server">
    <table width="100%" height="0" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="middle" nowrap bgcolor="#004FA5" style="width: 80%;">
                <div align="center">
                    <font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>
                        .:: SEGAS - SISTEMA ELECTRONICO DE GAS &nbsp;.:.&nbsp;<img src="Images/bnatop.gif"
                            width="56" height="19" align="absmiddle">&nbsp;.:.&nbsp; </strong></font>
                </div>
            </td>
            <td style="width: 20%" bgcolor="#004FA5">
                <asp:Label ID="LblFecha" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="true"
                    Font-Names="Arial"></asp:Label>
                -
                <input name="clockBox" id="clockBox" type="text" class="reloj" size="8" readonly="true"
                    style="background: #004FA5; color: White;">
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <table width="100%" height="0" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 60%">
                        </td>
                        <td style="width: 40%">
                            <table width="100%" height="0" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" class="h4">
                                        Usuario:
                                        <asp:Label ID="lblUsuario" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="h4">
                                        Grupo:
                                        <asp:Label ID="lblGrupo" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="h4">
                                        Compa&ntilde;ia: Bolsa Mercantil de Colombia </a>&nbsp; <b>.:: OPERADOR:
                                            <asp:Label ID="lblComisionista" runat="server"></asp:Label></b>::.
                                        <br>
                                        Ultimo Ingreso:
                                        <asp:Label ID="lblUltimoIngreso" runat="server"></asp:Label>
                                        </span>
                                    </td>
                                    <td width="100" align="center" rowspan="3" valign="middle">
                                        <asp:ImageButton ID="imbSalir" runat="server" ImageUrl="~/Images/Salir.png" OnClientClick="cerrar();"
                                            Height="40" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
